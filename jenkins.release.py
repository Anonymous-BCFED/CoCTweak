"""
JENKINS RELEASE BUILDER

Run with:

```python
with open('jenkins.release.py') as f:
	exec(compile(f.read(), 'jenkins.release.py', 'exec'))
```
"""

import os
from pathlib import Path, PureWindowsPath
import shlex
import subprocess
import sys
from typing import List

VERSION = os.environ.get("VERSION", "ci")

print(f"sys.executable: {Path(sys.executable)}")
print(f"sys.version:")
print(sys.version)
print(f"Path.cwd(): {Path.cwd()}")
print(f"$VERSION={VERSION!r}")

CWD: Path = Path.cwd()
DEVTOOLS = CWD / "devtools"

BUILD_SH: Path = CWD / "BUILD.sh"
BUILD_CMD: Path = CWD / "BUILD.cmd"

VENV_DIR = CWD / ".venv"

# Because poetry have been sitting on #5250 for ages.
os.environ['PYTHON_KEYRING_BACKEND'] = 'keyring.backends.fail.Keyring'

def as_windows(path: Path) -> PureWindowsPath:
    return PureWindowsPath(path)


BUILD_PY = Path(DEVTOOLS / "build.py").relative_to(CWD)

CIFS_DEPLOY_PATH = PureWindowsPath("//192.168.9.5/host") / "builds" / "coctweak"
LINUX_DEPLOY_PATH = Path("/media/builds/coctweak")


def shell_call(cmd: List[str], check: bool = True) -> None:
    if os.name == "nt":
        print(f"> {' '.join(cmd)}")
    else:
        print(f"$ {shlex.join(cmd)}")
    subprocess.run(cmd, check=check)


def python_call(cmd: List[str], check: bool = True) -> None:
    shell_call([sys.executable] + cmd, check=check)


python_call(["-m", "pip", "--version"])

python_call(["-m", "pip", "install", "-U", "pip"])
python_call(["-m", "pip", "install", "-U", "poetry"])
python_call(["-m", "poetry", "env", "remove", "--all"])
python_call(["-m", "poetry", "install", "--no-root", "--no-ansi", "--no-interaction"])
if os.name == "nt":
    python_call(
        [
            "-m",
            "poetry",
            "run",
            "python",
            str(BUILD_PY),
            "--no-colors",
            "--rebuild",
            "--version=" + VERSION,
            f"--deploy-to={as_windows(CIFS_DEPLOY_PATH)}",
        ]
    )
else:
    python_call(
        [
            "-m",
            "poetry",
            "run",
            "python",
            str(BUILD_PY),
            "--no-colors",
            "--rebuild",
            "--version=" + VERSION,
            f"--deploy-to={LINUX_DEPLOY_PATH}",
        ]
    )
