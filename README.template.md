**<big>[FAQ](docs/FAQ.md)</big>**

# CoCTweak

A thorough, mod-friendly command-line-based save editor for Corruption of Champions with a multitude of console commands. Built with Python 3 and mini-amf, so it works on both Windows and Linux.

[[_TOC_]]

## A Note

As a save editor, CoCTweak is *not* supported by the developers of CoC or the game's modders.  By using CoCTweak, you can potentially break saves and trigger bugs.  **You use CoCTweak at your own risk.**

So, **don't bug CoC devs about bugs triggered by CoCTweak** in CoC.  They aren't responsible, and can't fix our problems anyway.

## Mod Support
Most development is done against the HGG mod of CoC for purely preferential reasons.

Saves are (mostly) parsed by automatically-generated code, so there (hopefully) won't be too many issues when upgrading.

**You are welcome to file issue reports and merge requests if you'd like to help out!**

<table><thead><th>Mod</th><th>Author</th><th>Notes</th></thead>
<tbody>
{%- for tr in mods.supported -%}
<tr><th>
{%- if tr[3] == null -%}
  {{- tr[0] -}}
{%- else -%}
  [{{- tr[0] -}}]({{- tr[3] -}})
{%- endif -%}
</th><td>{{ tr[1] }}</td><td>{{ tr[2] }}</td></tr>
{%- endfor -%}
</tbody></table>

### Mod Versions

The current version we support of each mod. Earlier versions may work, but this is not guaranteed. Using CoC on later versions may result in missing features or bugs.

Note: "Git Commit" is the specific state of the code in their repositories that the current version of CoCTweak was built against. This is useful for bugtracking information.

<!-- devtools/updateSupportTable.py replaces this: -->
|                                                                     Mod | Version   | Git Commit                                                                                                                                              |
|------------------------------------------------------------------------:|:----------|:--------------------------------------------------------------------------------------------------------------------------------------------------------|
|                   [**HGG**](https://gitgud.io/BelshazzarII/CoCAnon_mod) | 1.6.17.1  | [`b56b7c4ca0372455298cffe272e648eaa8caf305`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/b56b7c4ca0372455298cffe272e648eaa8caf305)              |
|    [**UEE**](https://github.com/Kitteh6660/Corruption-of-Champions-Mod) | 1.4.19    | [`54fb4a179bcd4aea1cefada8d06dc0688d297657`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/54fb4a179bcd4aea1cefada8d06dc0688d297657) |
| [**Vanilla**](https://github.com/OXOIndustries/Corruption-of-Champions) | 1.0.2     | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00)  |
<!-- devtools/updateSupportTable.py END -->

### Dropped Mods

**Support for the following mods has been dropped** for the following reasons:

<table><thead><th>Mod</th><th>Author</th><th>Reason</th></thead>
<tbody>
{%- for tr in mods.dropped -%}
<tr><th>
{%- if tr[3] == null -%}
  {{- tr[0] -}}
{%- else -%}
  [{{- tr[0] -}}]({{- tr[3] -}})
{%- endif -%}
</th><td>{{ tr[1] }}</td><td>{{ tr[2] }}</td></tr>
{%- endfor -%}
</tbody></table>

## Installing

### Binaries

Pre-compiled, binary executables are available at the GitLab [Releases]({{- repo_uri -}}/-/releases) page to make use less complicated for Windows.

Binaries are also available for Ubuntu Linux. The Ubuntu Linux binaries will *probably* work on Debian, but other distros have stuff in other locations and are not supported at this time. coctweak may or may not work on these platforms.

To install, just extract to a folder and **run in your command line console.**

### From Source
* Make sure [Python](https://python.org) &gt;=3.6 is installed.
* You'll also need [pip](https://pip.pypa.io/en/stable/installing/), if it's not already installed with Python.
* You will need `git` installed, too. [Official site](https://git-scm.com/), you may use other packaged editions, though, like TortoiseGit, SourceTree, etc.

```shell
# Check out the code to disk.
git clone https://gitlab.com/Anonymous-BCFED/CoCTweak.git coctweak
cd coctweak
# Install things we need to run
pip install -r requirements.txt
# If you intend to develop things:
pip install -r requirements-dev.txt
```

## Updating

### Binary Releases

1. Download a new Release.
1. Extract and overwrite the previous files.

### Source Code

```shell
cd coctweak
# Pull in changes from our server
git pull
# Install things we need to run
pip install -r requirements.txt
# If you intend to develop things:
pip install -r requirements-dev.txt
```

## Help output

{{ cmd([COCTWEAK, '--help'], echo=True, replacements={
    'CoCTweak Save Editor v(\\d+).(\\d+).(\\d+)-(\\d+)': 'CoCTweak Save Editor v\\1.\\2.\\3-xxxxxxxxxxxxxxxx',
    'Git commit: ([a-f0-9]{40})': 'Git commit: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    'Built at: (.+)\n': 'Built at: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n'
}) }}

## Examples

### List Saves

{{ cmd([COCTWEAK, 'ls'], echo=True, comment='List hostnames in the Shared Objects Library.') }}
{{ cmd([COCTWEAK, 'ls', 'localhost'], echo=True, comment='List all available saves in the `localhost` hostname.') }}
{{ cmd([COCTWEAK, 'ls', 'localhost', '--format=coc'], echo=True, comment='Same as above, but with the same layout as CoC\'s loading menu.') }}


### Display Save

{{ cmd([COCTWEAK, 'show', 'localhost', '5'], echo=True, comment='Display some more detailed info about localhost 5 (CoC_5.sol).') }}

### Heal Thyself

{{ cmd([COCTWEAK, 'heal', 'localhost', '1', '--all'], echo=True, comment='Heal thyself.  --all implies --fatigue, --hunger, and --lust') }}

{%- do reset_slot(1) %}

### More

To see other common commands you might run, see [the guide](docs/guide.md).

## Commands
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- backup --></inline-elem>[coctweak backup](docs/commands/backup.md)
* <inline-elem><!-- body --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; ...](docs/commands/body/index.md)
* <inline-elem><!-- changes --></inline-elem>[coctweak changes &lt;host&gt; &lt;id&gt;](docs/commands/changes.md)
* <inline-elem><!-- compare --></inline-elem>[coctweak compare &lt;host_a&gt; &lt;id_a&gt; &lt;host_b&gt; &lt;id_b&gt;](docs/commands/compare.md)
* <inline-elem><!-- copy --></inline-elem>[coctweak (copy|cp|duplicate|dupe|clone) &lt;orig_host&gt; &lt;orig_id&gt; &lt;new_host&gt; &lt;new_id&gt;](docs/commands/copy.md)
* <inline-elem><!-- export --></inline-elem>[coctweak export &lt;host&gt; &lt;id&gt; &lt;filename.yml&gt;](docs/commands/export.md)
* <inline-elem><!-- get --></inline-elem>[coctweak get &lt;host&gt; &lt;id&gt; ...](docs/commands/get/index.md)
* <inline-elem><!-- heal --></inline-elem>[coctweak heal &lt;host&gt; &lt;id&gt;](docs/commands/heal.md)
* <inline-elem><!-- import --></inline-elem>[coctweak import &lt;filename.yml&gt; &lt;host&gt; &lt;id&gt;](docs/commands/import.md)
* <inline-elem><!-- inventory --></inline-elem>[coctweak (inventory|inv) &lt;host&gt; &lt;id&gt; ...](docs/commands/inventory/index.md)
* <inline-elem><!-- keyitem --></inline-elem>[coctweak keyitem &lt;host&gt; &lt;id&gt; ...](docs/commands/keyitem/index.md)
* <inline-elem><!-- list --></inline-elem>[coctweak (list|ls) &lsqb;&lt;host&gt; &lsqb;&lt;id&gt;&rsqb;&rsqb;](docs/commands/list.md)
* <inline-elem><!-- meditate --></inline-elem>[coctweak meditate &lt;host&gt; &lt;id&gt; ...](docs/commands/meditate.md)
* <inline-elem><!-- move --></inline-elem>[coctweak (move|mv) &lt;old_host&gt; &lt;old_id&gt; &lt;new_host&gt; &lt;new_id&gt;](docs/commands/move.md)
* <inline-elem><!-- npc --></inline-elem>[coctweak npc &lt;host&gt; &lt;id&gt; &lt;npcid&gt;](docs/commands/npc/index.md)
* <inline-elem><!-- perk --></inline-elem>[coctweak perk &lt;host&gt; &lt;id&gt;](docs/commands/perk/index.md)
* <inline-elem><!-- rename --></inline-elem>[coctweak rename &lt;host&gt; &lt;old_id&gt; &lt;new_id&gt;](docs/commands/rename.md)
* <inline-elem><!-- restore --></inline-elem>[coctweak restore](docs/commands/restore.md)
* <inline-elem><!-- set --></inline-elem>[coctweak set &lt;host&gt; &lt;id&gt; ...](docs/commands/set/index.md)
* <inline-elem><!-- show --></inline-elem>[coctweak (show|info) &lt;host&gt; &lt;id&gt;](docs/commands/show.md)
* <inline-elem><!-- statuseffect --></inline-elem>[coctweak (statuseffect|sfx) &lt;host&gt; &lt;id&gt;](docs/commands/statuseffect/index.md)
