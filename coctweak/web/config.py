from typing import Any, AnyStr, Dict, List, Optional, Union


class ConfigObject:
    def __init__(self, data: Optional[Dict[str, Any]] = None) -> None:
        self._raw: Dict[str, Any] = data or {}

    def get(self, key: Union[str, List[str]], default: Optional[Any] = None) -> Any:
        if isinstance(key, str):
            key = key.split('.')
        curdict = self._raw
        lkey = len(key)
        
        assert lkey > 0, 'Key must be >= 1 in length.'

        if lkey == 1:
            return curdict.get(key, default)
        
        for k in key[:-1]:
            if k not in curdict:
                return default
            curdict = curdict[k]

        return curdict.get(key[-1], default)

    def set(self, key: Union[str, List[str]], value: Any) -> None:
        if isinstance(key, str):
            key = key.split('.')
        curdict = self._raw
        lkey = len(key)
        
        assert lkey > 0, 'Key must be >= 1 in length.'

        if lkey == 1:
            self._raw[key] = value
        
        for k in key[:-1]:
            if k not in curdict:
                curdict[k] = {}
            curdict = curdict[k]

        curdict[key[-1]] = value

class HTTPConfig(ConfigObject):
    def __init__(self, data: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(data=data)
        self.port = 8080

    def get_port(self) -> int:
        return int(self.get('port', '8080'))
    def set_port(self, value: int) -> None:
        assert value > 0 and value <= 49151
        self.set('port', value)
    port = property(get_port, set_port)

class CoCTweakConfig(ConfigObject):
    def __init__(self, data: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(data=data)
        self.http: HTTPConfig = HTTPConfig(data.get('http', {}))


