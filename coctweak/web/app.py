from coctweak.web.config import CoCTweakConfig
from typing import Any, AnyStr, Dict, List
from werkzeug.exceptions import HTTPException
from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from coctweak.web.config import CoCTweakConfig
from jinja2 import Environment as JEnv
class CoCTweakApp:
    def __init__(self, config: Dict[AnyStr, Any]) -> None:
        self.config: CoCTweakConfig = CoCTweakConfig(config)

        self.api = CoCTweakAPIRoot()
        rulemap: List[Rule] = []
        self.api.register(rulemap)
        self.map = Map(rulemap)

    def dispatch_request(self, request: Request) -> Response:
        return Response('a')

    def wsgi_app(self, environ: JEnv, start_response) -> Any:
        urls = self.map.bind_to_environ(environ)
        try:
            endpoint, args = urls.match()
        except HTTPException as e:
            return e(environ, start_response)
        start_response('200 OK', [('Content-Type', 'text/plain')])
        return [f'Rule points to {endpoint!r} with arguments {args!r}']

        request = Request(environ)
        response = self.dispatch_request(request)
        return response(environ, start_response)

    def __call__(self, environ: JEnv, start_response):
        return self.wsgi_app(environ, start_response)