import argparse

from coctweak.logsys import getLogger #isort: skip
log = getLogger(__name__)

def valid_port(inp: str):
    port = int(inp)
    if port < 1 or port > 49151:
        raise argparse.ArgumentTypeError('--port must be between 1 and 49151.')
    return port
def main():
    argp = argparse.ArgumentParser('coctweak-web')
    argp.add_argument('--port', type=valid_port, default=8000)
    args = argp.parse_args()
    