from coctweak.web.api._base import BaseAPI
class FileAPI(BaseAPI):
    BASE_PATH = '/file'
    def __init__(self, root: 'CoCTweakAPIRoot') -> None:
        super().__init__()
        self.root = root

        self.addEndpoint('/open', 'file/open', on_open_file)

    
