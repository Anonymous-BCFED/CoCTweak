from typing import Callable, Dict, List, Union
from werkzeug.routing import Rule, Map, RuleFactory, Subdomain, Submount
from werkzeug.wrappers import Request, Response
_APIHandler = Callable[[Request], Response]
class _EndpointRecord:
    def __init__(self, id: str, rule: str, handler: _APIHandler) -> None:
        self.id: str = id
        self.rule: str = rule
        self.handler: _APIHandler = handler

    def asRule(self) -> Rule:
        return Rule(self.rule, endpoint=self.id)

class BaseAPI:
    BASE_PATH: str = '/'
    def __init__(self) -> None:
        self.endpoints: Dict[str, _EndpointRecord] = {}

    def register(self) -> None:
        pass

    def getRules(self) -> List[Union[Rule, RuleFactory]]:
        o: List[Union[Rule, RuleFactory]] = []
        for epr in self.endpoints.values():
            o.append(epr.asRule())
        if self.BASE_PATH != '/':
            o = [Submount(self.BASE_PATH, o)]
        return o

    def addEndpoint(self, rule: str, id: str, handler: _APIHandler) -> None:
        self.endpoints[id] = _EndpointRecord(id, rule, handler)