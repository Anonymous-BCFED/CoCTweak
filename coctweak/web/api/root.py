class CoCTweakAPIRoot(BaseAPI):
    def __init__(self) -> None:
        super().__init__()
        self.addRule('/', 'index', self.on_index)
        self.file = FileAPI(self)