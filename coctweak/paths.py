from pathlib import Path
import shutil
import tempfile
import getpass

from coctweak.consts import VERSION

USERNAME: str = getpass.getuser()
TEMP_DIR: Path = Path(tempfile.gettempdir()) / f'coctweak-{USERNAME}' / str(VERSION)

def ensureDirExists(path: Path) -> None:
    if not path.is_dir():
        path.mkdir(parents=True)

if TEMP_DIR.parent.is_dir():
    for vdir in TEMP_DIR.parent.iterdir():
        if vdir.stem == str(VERSION):
            continue
        shutil.rmtree(vdir)
ensureDirExists(TEMP_DIR)
