##########################################################
#     WARNING: @GENERATED CODE; DO NOT EDIT BY HAND      #
#               BY devtools/buildPregex.py               #
# Pre-generated regular expression patterns for CoCTweak #
##########################################################
import re
from typing import Pattern
__ALL__ = ['REG_ANSI_ESCAPE']
__re_compile = re.compile
__re_MULTILINE = re.MULTILINE
REG_ANSI_ESCAPE: Pattern = __re_compile('\x1b(?:[@-Z\\\\-_]|\\[[0-?]*[ /]*[@-\\~])')
