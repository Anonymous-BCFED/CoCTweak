# Funny hidden gems
from typing import NamedTuple

from coctweak.urlgenerators import URLGenerator

__all__ = ['ModInfo']


class ModInfo(NamedTuple):
    id: str
    version: str
    commit: str
    urlgen: URLGenerator
