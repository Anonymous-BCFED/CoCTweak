from typing import Optional


class URLGenerator(object):
    def __init__(self, site_url: Optional[str] = ""):
        self.site_url: Optional[str] = site_url

    def load(self, data: dict) -> None:
        self.site_url = data.get("site-url", None)

    def getCommitURL(self, commit_hash: str) -> str:
        return ""

    def getSiteURL(self) -> str:
        return self.site_url

    def __str__(self) -> str:
        return f"URLGenerator({self.site_url!r})"


class GitLabURLGenerator(URLGenerator):
    def __init__(
        self,
        hostname: str = "",
        namespace: str = "",
        project: str = "",
        site_url: Optional[str] = None,
    ):
        self.hostname: str = hostname
        self.namespace: str = namespace
        self.project: str = project
        super().__init__(site_url)

    def load(self, data: dict) -> None:
        super().load(data)
        self.hostname = data["hostname"]
        self.namespace = data["namespace"]
        self.project = data["project"]

    def getSiteURL(self):
        return (
            self.site_url
            if self.site_url is not None
            else f"https://{self.hostname}/{self.namespace}/{self.project}"
        )

    def getCommitURL(self, commit_hash: str) -> str:
        return f"https://{self.hostname}/{self.namespace}/{self.project}/-/commit/{commit_hash}"

    def __str__(self):
        return f"GitLabURLGenerator({self.hostname!r}, {self.namespace!r}, {self.project!r}, {self.site_url!r})"

    def __repr__(self):
        return f"GitLabURLGenerator({self.hostname!r}, {self.namespace!r}, {self.project!r}, {self.site_url!r})"


class GitHubURLGenerator(URLGenerator):
    def __init__(
        self, namespace: str = "", project: str = "", site_url: Optional[str] = None
    ):
        self.namespace: str = namespace
        self.project: str = project
        super().__init__(site_url)

    def load(self, data: dict) -> None:
        super().load(data)
        self.namespace = data["namespace"]
        self.project = data["project"]

    def getSiteURL(self):
        return (
            self.site_url
            if self.site_url is not None
            else f"https://github.com/{self.namespace}/{self.project}"
        )

    def getCommitURL(self, commit_hash: str) -> str:
        return (
            f"https://github.com/{self.namespace}/{self.project}/commit/{commit_hash}"
        )

    def __str__(self):
        return f"GitHubURLGenerator({self.namespace!r}, {self.project!r}, {self.site_url!r})"

    def __repr__(self):
        return f"GitHubURLGenerator({self.namespace!r}, {self.project!r}, {self.site_url!r})"
