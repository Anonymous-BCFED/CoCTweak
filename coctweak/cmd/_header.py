from coctweak.consts import VERSION
from coctweak.logsys import LogWrapper
def header(args):
    if args.quiet:
        return
    log = LogWrapper()
    log.info(f'CoCTweak v{VERSION}')
    log.info('(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.')
    log.info('__________________________________________________________________________')
    log.info('')
