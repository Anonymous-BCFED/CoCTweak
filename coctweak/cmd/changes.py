import os
from coctweak.utils import execute, add_flash_host_arg
def register_parsers__changes(subp):
    p_changes = subp.add_parser('changes', help='Show diff of changes.')
    add_flash_host_arg(p_changes)
    p_changes.add_argument('id', type=str, help="ID of the save, or Main for the Permanent Object file (CoC_Main).")
    p_changes.add_argument('--since-commit', type=str, default=None, help='Commit to use as the starting point')
    p_changes.add_argument('--commits-back', type=int, default=1, help='How many commits back (default: 1)')
    p_changes.set_defaults(cmd=cmd_changes)

def cmd_changes(args):
    '''
    changes localhost 1 --since-commit=COMMIT --commits-back=2
    '''
    if args.id.lower() == 'main':
        filename = os.path.join(args.host, 'CoC_Main.sol', 'CoC_Main.yml')
    else:
        filename = os.path.join(args.host, f'CoC_{args.id}.sol', f'CoC_{args.id}.yml')
    if not os.path.isfile(os.path.join('backup', filename)):
        print(os.path.join('backup', filename), 'does not exist.')
        return
    cwd = os.getcwd()
    try:
        os.chdir('backup')
        cmd=['git', 'diff']
        cmd += ['--color=always']
        if args.since_commit is not None:
            cmd += [args.since_commit]
        else:
            cmd += ['HEAD'+('~'*max(0, args.commits_back))]
        cmd += ['--']
        cmd += [filename]
        execute(cmd)
    finally:
        os.chdir(cwd)
