import argparse
import json
import os
import sys
from typing import Any, Dict, List

from ruamel.yaml import YAML

from coctweak.cmd._header import header
from coctweak.logsys import getLogger
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.utils import add_hostid_args, yaml_set_compact

yaml = YAML(typ='rt')

log = getLogger(__name__)

def register_parsers__npc(subp: argparse.ArgumentParser, batch: bool=False):
    # This somehow works.
    p_npclist = subp.add_parser('npc-list', help='List all available NPCs.')
    add_hostid_args(p_npclist, batch)
    p_npclist.add_argument('--format', '-f', choices=['text', 'json', 'yaml'], default='text', help='What format to present the data')
    p_npclist.add_argument('--compact', '-c', action='store_true', default=False, help='Remove whitespace from non-text formats.')
    p_npclist.add_argument('--indent', '-i', type=int, default=2, help='How many spaces to indent when pretty-printing non-text data formats.')
    p_npclist.set_defaults(cmd=cmd_npclist)

    p_npc = subp.add_parser('npc', help='View and modify NPC variables, assuming it has support in coctweak.')
    add_hostid_args(p_npc, batch)
    p_npc.add_argument('npcid', type=str, help='Subject NPC.')
    p_npc.add_argument('rest', nargs=argparse.REMAINDER, help='Commands to pass to the given NPC.')
    p_npc.set_defaults(cmd=cmd_npc)

def cmd_npc(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    if args.npcid is None or args.npcid == 'list':
        print('Available NPCs:')
        for npcid in sorted(save.NPCs.keys()):
            print(f' - {npcid}')
        return

    npc = save.NPCs[args.npcid]
    argp = argparse.ArgumentParser(f'coctweak npc {args.host} {args.id} {args.npcid}', description='CoC Save Editor')
    subp = argp.add_subparsers()
    npc.register_cli(subp)
    subargs = argp.parse_args(args.rest)
    if getattr(subargs, 'npc_cmd', None) is None:
        argp.print_help()
        sys.exit(1)

    if not subargs.npc_cmd(subargs):
        # return false -> no saving needed
        return

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_npclist(args, batch_save=None):
    if batch_save is None or args.format!='text':
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet or args.format!='text')

    o: List[str] = sorted(save.NPCs.keys())
    fmt: str = args.format or 'text'
    if fmt == 'text':
        print('Available NPCs:')
        for npcid in o:
            print(f' - {npcid}')
        return
    o: Dict[str, Any] = {
        'NPCs': o
    }
    if fmt == 'json':
        print(json.dumps(o, indent=None if args.compact else args.indent, separators=(',', ':') if args.compact else (', ', ': ')))
    if fmt == 'yaml':
        if args.compact:
            yaml_set_compact(yaml)
        else:
            yaml.indent(mapping=args.indent, sequence=args.indent)
        yaml.dump(o, sys.stdout)
    return
