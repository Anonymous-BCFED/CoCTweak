import argparse
import json
import sys

from ruamel.yaml import YAML

from coctweak.cmd._header import header
from coctweak.logsys import getLogger
from coctweak.saves import loadFromSlot, saveToSlot  # NOQA
from coctweak.utils import add_hostid_args

log = getLogger(__name__)

yaml = YAML(typ='rt')

def register_parsers__perk(subp: argparse.ArgumentParser, batch: bool=False):
    p_perk = subp.add_parser('perk', help='View and modify the player\'s perks.')
    add_hostid_args(p_perk, batch)

    p_perk_subp = p_perk.add_subparsers()

    register_parsers___perk_add(p_perk_subp)
    register_parsers___perk_list(p_perk_subp)
    register_parsers___perk_remove(p_perk_subp)
    register_parsers___perk_show(p_perk_subp)

def register_parsers___perk_add(subp):
    p_add = subp.add_parser('add', help='Create a perk on the player.')
    p_add.add_argument('perk_id', type=str, help='perk ID.  See the listing of available perks for your mod.')
    p_add.add_argument('value1', type=float, default=0., help='Value 1, used as data storage in some perks.')
    p_add.add_argument('value2', type=float, default=0., help='Value 2, used as data storage in some perks.')
    p_add.add_argument('value3', type=float, default=0., help='Value 3, used as data storage in some perks.')
    p_add.add_argument('value4', type=float, default=0., help='Value 4, used as data storage in some perks.')
    p_add.set_defaults(cmd=cmd_perk_add)

def cmd_perk_add(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    perk = save.QUAD_PERK_TYPE()
    perk.id = args.perk_id
    perk.values = [args.value1, args.value2, args.value3, args.value4]
    save.perks[perk.id] = perk
    perk.displayValues()

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def register_parsers___perk_remove(subp):
    p_remove = subp.add_parser('remove')
    p_remove.add_argument('perk_id', type=str, help='Perk ID.  See the listing of available perks for your mod.')
    p_remove.set_defaults(cmd=cmd_perk_remove)

def cmd_perk_remove(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    if args.perk_id not in save.perks.keys():
        log.critical(f'Perk ID {args.perk_id!r} is not set on the player. Aborting.')
        sys.exit(1)
    log.info('Removing %s...', save.perks[args.perk_id].id)
    del save.perks[args.perk_id]
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def register_parsers___perk_list(subp):
    p_ls = subp.add_parser('list', aliases=['ls'], help='List the IDs of all active perks.')
    p_ls.add_argument('--format', '-f', type=str, choices=['ascii', 'markdown', 'json', 'yaml'], default='ascii', help='Format of display.')
    p_ls.set_defaults(cmd=cmd_perk_list)
def cmd_perk_list(args, batch_save=None):
    # DELIBERATELY uses print().
    #header(args)
    save = batch_save or loadFromSlot(args.host, args.id, True)
    if args.format in ('ascii', 'markdown') and len(save.perks.keys()) == 0:
        print('<No perks set>')

    o={}
    for perkid in sorted(save.perks.keys()):
        perk = save.perks[perkid]
        if args.format == 'ascii':
            print(f'{perk.id}')
        elif args.format == 'markdown':
            print(f' * {perk.id}')
        else:
            o[perk.id] = {'values': perk.values}
    if args.format == 'json':
        print(json.dumps(o, indent=2))
    if args.format == 'yaml':
        yaml.dump(o, sys.stdout)

def register_parsers___perk_show(subp):
    p_ls = subp.add_parser('show', help='Display all values associated with the perk and any known interpretations of their meaning.')
    p_ls.add_argument('perk_id', type=str, help='ID of the perk.  Use quotes if it has a space!')
    p_ls.set_defaults(cmd=cmd_perk_show)
def cmd_perk_show(args, batch_save=None):
    #header(args)
    save = batch_save or loadFromSlot(args.host, args.id, True)
    if args.perk_id not in save.perks:
        log.critical(f'Perk ID {args.perk_id!r} is not set on the player. Aborting.')
        sys.exit(1)
    perk = save.perks[args.perk_id]
    with log.info(f'{perk.id}:'):
        perk.longPrintValues()
