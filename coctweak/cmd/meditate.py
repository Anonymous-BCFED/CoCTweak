import argparse
import sys
from typing import Callable, List, Optional
from coctweak.cmd._header import header
from coctweak.saves import saveToSlot, loadFromSlot
from coctweak.utils import add_hostid_args
from coctweak.logsys import getLogger
log = getLogger(__name__)


def register_parsers__meditate(subp: argparse.ArgumentParser, batch: bool = False):
    p_heal = subp.add_parser('meditate', help='Simulate conducting one or more meditation sessions with Jojo.')
    
    add_hostid_args(p_heal, batch)

    p_heal.add_argument('--dry-run', dest='dry_run', action='store_true', default=False, help="Go through the motions, but do not actually save changes.")
    p_heal.add_argument('--diagnose-only', dest='diagnose_only', action='store_true', default=False, help="Don't fix anything, just show current health issues.")
    p_heal.add_argument('--count', dest='count', type=int, default=0, help="Conduct only a fixed number of sessions")
    p_heal.add_argument('--ignore-corruption', action='store_true', default=False,help="Disregard corruption value when checking whether to stop or not.")
    p_heal.add_argument('--min-corruption', type=float, default=0,help="Repeat until corruption is at or below this number.")
    p_heal.add_argument('--min-libido', type=float, default=None, help="Repeat until libido is at or below this number.")
    p_heal.set_defaults(cmd=cmd_heal)


def cmd_heal(args: argparse.Namespace, batch_save=None):
    if batch_save is None:
        header(args)
    # Idiot checks
    if args.ignore_corruption and args.min_libido is None:
        log.error('Setting --ignore-corruption without setting --min-libido will result in an infinite loop. Set --min-libido or --count instead.')
        sys.exit(1)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    with log.info('Stats:'):
        log.info(f'Corruption: {save.stats.corruption.value} (min: {save.stats.corruption.min}, max: {save.stats.corruption.max})')
        log.info(f'Lust......: {save.combat_stats.lust.value} (min: {save.combat_stats.lust.min}, max: {save.combat_stats.lust.max})')
        log.info(f'Strength..: {save.stats.strength.value} (min: {save.stats.strength.min}, max: {save.stats.strength.max})')
        log.info(f'Speed.....: {save.stats.speed.value} (min: {save.stats.speed.min}, max: {save.stats.speed.max})')
        log.info(f'Libido....: {save.stats.libido.value} (min: {save.stats.libido.min}, max: {save.stats.libido.max})')
    if args.diagnose_only:
        return

    def medit8(i: int, mincor: Optional[float] = None, minlib: Optional[float] = None) -> None:
        lus = save.combat_stats.lust.value
        cor = save.stats.corruption.value
        str_ = save.stats.strength.value
        spd = save.stats.speed.value
        lib = save.stats.libido.value
        save.meditate()
        if mincor is not None and save.stats.corruption.value < mincor:
            save.stats.corruption.value = mincor
        if minlib is not None and save.stats.libido.value < minlib:
            save.stats.corruption.value = minlib
        o: List[str] = []

        def record_change(statname: str, origval: float, afterval: float) -> None:
            if abs(origval-afterval) > .01:
                o.append(f'{statname}: {origval} -> {afterval}')
        record_change('LUS', lus, save.combat_stats.lust.value)
        record_change('COR', cor, save.stats.corruption.value)
        record_change('STR', str_, save.stats.strength.value)
        record_change('SPD', spd, save.stats.speed.value)
        record_change('LIB', lib, save.stats.libido.value)
        assert len(o)>0
        log.info(f'{i:04d}: '+', '.join(o))
    nsessions: int = 0
    if args.count > 0:
        with log.info(f'Meditating only {args.count} times:'):
            for i in range(args.count):
                medit8(i)
                nsessions+=1
    else:
        conditions: List[str] = []
        checks:List[Callable[[],bool]]=[]
        if not args.ignore_corruption and args.min_corruption is not None:
            conditions.append(f'corruption is below {args.min_corruption}')
            checks.append(lambda: round(save.stats.corruption.value) > round(args.min_corruption))
        if args.min_libido is not None:
            conditions.append(f'libido is below {args.min_libido}')
            checks.append(lambda: round(save.stats.libido.value) > round(args.min_libido))
        condstr: str = ''
        match len(conditions):
            case 1:
                condstr = conditions[0]
            case 2:
                condstr = 'either '+' and '.join(conditions)
        with log.info(f'Meditating until {condstr}:'):
            i: int = 0
            while any([x() for x in checks]):
                medit8(i)
                i += 1
                nsessions += 1
                if i==10000:
                    return
    log.info(f'After {nsessions} grueling sessions with Jojo, you feel purer.')
    if args.dry_run:
        log.warning('--dry-run enabled, so coctweak is not saving any changes.')
        return
    if batch_save is None:
        saveToSlot(args.host, args.id, save)
