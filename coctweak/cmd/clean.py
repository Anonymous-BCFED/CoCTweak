import miniamf.sol, miniamf
from typing import List
from ._header import header
from coctweak.saves import getSaveFilename, loadRawDataFromFile, saveRawDataToFile
from coctweak.utils import add_flash_host_arg, add_flash_id_arg
import argparse

from coctweak.logsys import LogWrapper
log = LogWrapper()

def register_parsers__clean(subp: argparse.ArgumentParser, batch: bool=False):
    p_clean = subp.add_parser('clean', help='Clean cruft from the save, if applicable.')
    add_flash_host_arg(p_clean)
    add_flash_id_arg(p_clean)
    p_clean.set_defaults(cmd=cmd_clean)

def delKeyIfPresent(save, key: List[str], reason: str):
    curbase = save
    for ckey in key[:-1]:
        if ckey not in curbase:
            return
        curbase = curbase[ckey]
    ckey = key[-1]
    if ckey in curbase:
        del curbase[ckey]
        log.info('Found and removed save entry %s (%s)', '/'.join(key), reason)

def cmd_clean(args, batch_save=None):
    if batch_save is None:
        header(args)
    #save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    data = loadRawDataFromFile(args.host, args.id)
    delKeyIfPresent(data, ['flags-decoded-NOT-LOADED'], 'Just used for comparing saves in CoCTweak.')
    saveRawDataToFile(args.host, args.id, data)
