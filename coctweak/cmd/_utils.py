import argparse
from typing import Optional
def buildPiercingArgs(parser: argparse.ArgumentParser, prefix: Optional[str], bodypartname: str) -> None:
    arg_prefix: str = ''
    if prefix is not None:
        arg_prefix = f'{prefix}-'
    parser.add_argument(f'--{arg_prefix}piercing-type', type=str, default=None, help=f"What kind of {bodypartname} piercing you have.")
    parser.add_argument(f'--{arg_prefix}piercing-short', type=str, default=None, help=f"Short description of {bodypartname} piercing. REQUIRED if --{arg_prefix}piercing-type is set and is not NONE.")
    parser.add_argument(f'--{arg_prefix}piercing-long', type=str, default=None, help=f"Long description of {bodypartname} piercing. REQUIRED if --{arg_prefix}piercing-type is set and is not NONE.")

def handlePiercingArgs(bp, args: argparse.Namespace, prefix: Optional[str] = None) -> None:
    prefix_namespace: str = ''
    attr_cls: str = ''
    if prefix is None:
        prefix_namespace = f''
        attr_cls = f'piercing'
    else:
        prefix_namespace = f'{prefix}_'
        attr_cls = f'{prefix}Piercing'
    args_type: str = getattr(args, f'{prefix_namespace}piercing_type')
    args_short: str = getattr(args, f'{prefix_namespace}piercing_short')
    args_long: str = getattr(args, f'{prefix_namespace}piercing_long')
    if args_type is not None:
        if args_type == 'NONE':
            setattr(bp, attr_cls, None)
        else:
            assert args_short is not None and args_short != ''
            assert args_long is not None and args_long != ''
            p = bp.TYPE_PIERCING()
            setattr(bp, attr_cls, p)
            p.type = p.ENUM_TYPES[args_type]
            p.short = args_short
            p.long = args_long
