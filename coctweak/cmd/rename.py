
import os
import shutil
import sys

from coctweak.cmd._header import header
from coctweak.logsys import getLogger
from coctweak.saves import LSO_DIR, getSaveFilename
from coctweak.utils import add_flash_host_arg, sanitizeFilename

log = getLogger(__name__)

def register_parsers__rename(subp):
    p_rename = subp.add_parser('rename', help='Rename a save (like `coctweak move`, but in the same host)')
    add_flash_host_arg(p_rename)
    p_rename.add_argument('src_id', type=int, help="ID of the original save.")
    p_rename.add_argument('dest_id', type=int, help="ID slot to rename it to. Must not exist.")
    p_rename.add_argument('--force', action='store_true', default=False, help='If dest_id exists, overwrite it.')
    p_rename.set_defaults(cmd=cmd_rename)

def cmd_rename(args):
    header(args)
    if not os.path.isdir(os.path.join(LSO_DIR, args.host)):
        log.critical('{!r} does not exist.'.format(
            os.path.join(LSO_DIR, args.host)))
        sys.exit(1)
    src_fn = getSaveFilename(args.host, args.src_id)
    if not os.path.isfile(src_fn):
        log.critical('{!r} does not exist.'.format(src_fn))
        sys.exit(1)
    dest_fn = getSaveFilename(args.host, args.dest_id)
    if not args.force and os.path.isfile(dest_fn):
        log.critical('{!r} exists. Please remove it, or use --force to overwrite it anyway.')
        sys.exit(1)
    with log.info(f'Moving {sanitizeFilename(src_fn)} to {sanitizeFilename(dest_fn)}...'):
        shutil.move(src_fn, dest_fn)
        log.info('OK!')
