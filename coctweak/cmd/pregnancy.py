import argparse
from typing import List, Tuple

from ruamel.yaml import YAML

from coctweak.cmd._header import header
from coctweak.logsys import getLogger
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.saves.pregnancystore import BasePregnancyStore
from coctweak.utils import add_hostid_args, ERoundingType, parsePropertyAdjustment  # NOQA

yaml = YAML(typ='rt')

log = getLogger(__name__)

def register_parsers__pregnancy(subp: argparse.ArgumentParser, batch: bool=False):
    p_preg = subp.add_parser('pregnancy', aliases=['preg'], help='Configure pregnancy data.')
    add_hostid_args(p_preg, batch)

    p_preg.add_argument('oriface', choices=['butt', 'vag', 'all'], help="Which hole to target")
    preg_subp = p_preg.add_subparsers()

    clearp = preg_subp.add_parser('clear', help='Clear pregnancy. WILL NOT REVERSE SIDE-EFFECTS.')
    clearp.set_defaults(cmd=cmd_preg_clear)

    showp = preg_subp.add_parser('show', help='Show all pregnancystore values.')
    showp.set_defaults(cmd=cmd_preg_show)

    adjustp = preg_subp.add_parser('adjust', help='Set or adjust values of a PregnancyStore')
    adjustp.add_argument('--type', '-t', type=str, default=None, help="Pregnancy type. See the enum for PregnancyType for your mod.")
    #adjustp.add_argument('--last-notice', '-n', type=str, default=None, help="Time that you last noticed something. You can use the adjustment sublanguage.")
    adjustp.add_argument('--incubation', '-i', type=str, default=None, help="Incubation time. You can use the adjustment sublanguage.")
    adjustp.set_defaults(cmd=cmd_preg_adjust)

def _psFromArgs(args: argparse.Namespace, save: 'BaseSave') -> List[Tuple[str, BasePregnancyStore]]:
    o: List[Tuple[str, BasePregnancyStore]] = []
    part = args.oriface
    if part in ('butt', 'all'):
        o += [('Anal', save.analPregnancy)]
    if part in ('vag', 'all'):
        o += [('Vaginal', save.vaginalPregnancy)]
    return o

def cmd_preg_show(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    for label, store in _psFromArgs(args, save):
        with log.info(f'{label} Pregnancy:'):
            store.display()


def cmd_preg_clear(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    for label, store in _psFromArgs(args, save):
        with log.info(f'{label} Pregnancy:'):
            if not store.is_pregnant:
                log.info('Skipped (Not pregnant)')
            else:
                store.reset()
                log.info('Cleared!')
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_preg_adjust(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    changes = 0
    for label, store in _psFromArgs(args, save):
        with log.info(f'{label} Pregnancy:'):
            if args.type is not None:
                before = store.type
                if args.type.isdigit():
                    store.type = store.PREGTYPE_ENUM(int(args.type))
                else:
                    store.type = store.PREGTYPE_ENUM[args.type]
                log.info(f'Type: {before.name} ({before.value}) -> {store.type.name} ({store.type.value})')
            #if args.last_notice is not None:
            #    store.last_notice = parsePropertyAdjustment(store.last_notice, args.last_notice, 0, 65535, ERoundingType.ROUND)
            if args.incubation is not None:
                before = store.incubation
                store.incubation = parsePropertyAdjustment(store.incubation, args.incubation, 0, 65535, ERoundingType.ROUND)
                log.info(f'Incubation: {before} -> {store.incubation}')
                changes += 1
            log.info('Done!')
    if batch_save is None and changes > 0:
        saveToSlot(args.host, args.id, save)
