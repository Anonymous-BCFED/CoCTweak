import argparse, sys, shlex

from .backup import register_parsers__backup
from .body import register_parsers__body
from .clean import register_parsers__clean
from .get import register_parsers__get
from .heal import register_parsers__heal
from .inventory import register_parsers__inv
from .keyitem import register_parsers__keyitem
from .npc import register_parsers__npc
from .perk import register_parsers__perk
from .pregnancy import register_parsers__pregnancy
from .restore import register_parsers__restore
from .set import register_parsers__set
from .show import register_parsers__show
from .statuseffect import register_parsers__statuseffect

from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.utils import add_flash_host_arg

def register_parsers(subp):
    register_parsers__backup(subp, True)
    register_parsers__body(subp, True)
    register_parsers__clean(subp, True)
    register_parsers__get(subp, True)
    register_parsers__heal(subp, True)
    register_parsers__inv(subp, True)
    register_parsers__keyitem(subp, True)
    register_parsers__npc(subp, True)
    register_parsers__perk(subp, True)
    register_parsers__pregnancy(subp, True)
    register_parsers__set(subp, True)
    register_parsers__show(subp, True)
    register_parsers__statuseffect(subp, True)

def register_parsers__batch(subp):
    p_batch = subp.add_parser('batch', help='Run a batched command.')
    add_flash_host_arg(p_batch)
    p_batch.add_argument('id', type=int, help="ID of the save.")
    p_batch.add_argument('filename', type=str, help='File containing commands on each line.')
    p_batch.set_defaults(cmd=cmd_batch)

def cmd_batch(args):
    from ._header import header
    header(args)

    save = loadFromSlot(args.host, args.id, args.quiet)

    argp = argparse.ArgumentParser('coctweak', description='CoC Save Editor')

    argp.add_argument('--quiet', '-q', action='store_true', default=False, help='Silence information-level messages.')
    argp.add_argument('--verbose', '-v', action='store_true', default=False, help='More verbose logging.')
    argp.add_argument('--debug', action='store_true', default=False, help='Display debug-level messages.')

    subp = argp.add_subparsers()

    register_parsers(subp)

    with open(args.filename, 'r') as f:
        for line in f:
            line = line.strip()
            if line == '':
                continue
            if line.startswith('#'):
                continue

            ns = argparse.Namespace()
            argv = shlex.split(line)
            if line == 'backup':
                argv = 'backup'
            else:
                ns = argparse.Namespace(host=args.host, id=args.id)
            args = argp.parse_args(argv, ns)
            if getattr(args, 'cmd', None) is None:
                argp.print_help()
                sys.exit(1)

            args.cmd(args, save)

    saveToSlot(args.host, args.id, save)
