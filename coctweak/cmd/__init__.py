# Remember to add to batch, too.
from .backup import register_parsers__backup
from .batch import register_parsers__batch
from .body import register_parsers__body
from .changes import register_parsers__changes
from .clean import register_parsers__clean
from .compare import register_parsers__compare
from .copy import register_parsers__copy
from .export import register_parsers__export
from .get import register_parsers__get
from .heal import register_parsers__heal
from .import_ import register_parsers__import
from .inventory import register_parsers__inv
from .keyitem import register_parsers__keyitem
from .list import register_parsers__list
from .meditate import register_parsers__meditate
from .move import register_parsers__move
from .npc import register_parsers__npc
from .perk import register_parsers__perk
from .pregnancy import register_parsers__pregnancy
from .rename import register_parsers__rename
from .restore import register_parsers__restore
from .set import register_parsers__set
from .show import register_parsers__show
from .statuseffect import register_parsers__statuseffect


def register_parsers(subp):
    register_parsers__backup(subp)
    register_parsers__batch(subp)
    register_parsers__body(subp)
    register_parsers__changes(subp)
    register_parsers__clean(subp)
    register_parsers__compare(subp)
    register_parsers__copy(subp)
    register_parsers__export(subp)
    register_parsers__get(subp)
    register_parsers__heal(subp)
    register_parsers__import(subp)
    register_parsers__inv(subp)
    register_parsers__keyitem(subp)
    register_parsers__list(subp)
    register_parsers__meditate(subp)
    register_parsers__move(subp)
    register_parsers__npc(subp)
    register_parsers__perk(subp)
    register_parsers__pregnancy(subp)
    register_parsers__rename(subp)
    register_parsers__restore(subp)
    register_parsers__set(subp)
    register_parsers__show(subp)
    register_parsers__statuseffect(subp)
