from __future__ import absolute_import

import argparse
import datetime
import filecmp
import logging
import os
from pathlib import Path
import shutil
from typing import List

import pygit2
from ruamel.yaml import YAML

from coctweak.logsys import getLogger
from coctweak.saves import loadSaveFromFile, LSO_DIR
from coctweak.utils import _do_cleanup_object, dump_to_yaml, execute

yaml = YAML(typ='rt')
log = getLogger(__name__)

COC_MAIN_FILES = [
    'CoC_Main', # HGG
    'CoC_Main-UEE', # Guess.
]
LFS_TRACK_GLOBS = [
    '*.sol', # LSOs
    '*.ssl', # Microsoft MUID...?
]
META_VERSION = '04022021' # ddmmyyyy

BACKUP_DIR = Path('backup')

def register_parsers__backup(subp: argparse.ArgumentParser, batch: bool=False):
    p_backup = subp.add_parser('backup', help='Exports all saves as YAML, and then commits the changes.')
    p_backup.add_argument('--message', type=str, default=datetime.datetime.now().strftime('%A, %B %d, %Y %I:%M:%S %p'), help='Set message.')
    p_backup.add_argument('--to-dir', type=str, default=str(BACKUP_DIR), help='Set destination directory.')
    p_backup.add_argument('--lso-dir', type=str, default=str(LSO_DIR), help='Directory in which Flash Local Shared Object files are stored.')
    p_backup.set_defaults(cmd=cmd_backup)

def remove_empty_dirs(dirname: str) -> int:
    if '.git' in dirname:
        return 0
    removed = 1
    dirs = 0
    files = 0
    while(removed > 0):
        dirs = 0
        files = 0
        removed = 0
        entries = os.listdir(dirname)
        for entryname in entries:
            entry = os.path.join(dirname, entryname)
            if os.path.isdir(entry):
                removed += remove_empty_dirs(entry)
                dirs += 1
            elif os.path.isfile(entry):
                files += 1
    if dirs + files == 0:
        log.info(f'rmdir {dirname} (empty)')
        os.rmdir(dirname)
        return 1
    return removed

def handleCopyOnly(siteid: str, namespace: str, filename: str, path: Path, prefix: Path) -> List[str]:
    outfile: Path = prefix.with_suffix(path.suffix)
    with log.info('-> %s (Raw Copy)', outfile):
        if outfile.is_file() and filecmp.cmp(path, outfile, shallow=False):
            log.warning('(Files identical, skipping copy)')
        else:
            shutil.copy(path, outfile)
    yield str(outfile)

def handleSol(siteid: str, namespace: str, filename: str, path: Path, prefix: Path) -> List[str]:
    if filename.startswith('CoC_') and filename.endswith('_backup'):
        log.info('Ignored (backup).')
        return
    if filename == 'temp-savegame':
        log.info('Ignored (temporary file).')
        return

    yield from handleCopyOnly(siteid, namespace, filename, path, prefix)

    yield from handleSOL2YAML(siteid, namespace, filename, path, prefix)

    if filename in COC_MAIN_FILES:
        #yield from handleCOCMain(siteid, namespace, filename, path, prefix)
        return

    elif filename.startswith('CoC_'):
        yield from handleCOCSave(siteid, namespace, filename, path, prefix)

def handleSOL2YAML(siteid: str, namespace: str, filename: str, path: Path, prefix: Path) -> List[str]:
    outfile: Path = prefix.with_suffix('.raw.yml')
    log.info('-> %s (Raw YAML)', outfile)
    dump_to_yaml(str(filename), str(outfile))
    yield str(outfile)

def handleCOCMain(siteid: str, namespace: str, filename: str, path: Path, prefix: Path) -> List[str]:
    outfile: Path = prefix.with_suffix('.yml')
    log.info('-> %s (Save Main)', outfile)
    #TODO
    yield str(outfile)

def handleCOCSave(siteid: str, namespace: str, filename: str, path: Path, prefix: Path) -> List[str]:
    outfile: Path = prefix.with_suffix('.yml')
    saveid: int
    try:
        saveid = int(filename[4:])
        # def loadSaveFromFile(filename: str, quiet:bool = False, loglevel_file_not_found: int=logging.CRITICAL, is_file:bool = False) -> BaseSave:
    except ValueError:
        log.error('handleCOCSave(): Unable to parse filename!')
        return
    try:
        save = loadSaveFromFile(str(path), True, loglevel_file_not_found=logging.WARNING)
        if save is None:
            log.error('handleCOCSave(): Unknown save type.')
            return

        save.loading_for_backup = True
        save.id = saveid
        save.host = siteid
        save.namespace = namespace
        save.global_data = None
        log.info(f'-> %s (%s v%s)', outfile, save.NAME, save.getModVersion())
        with open(outfile, 'w') as f:
            yaml.dump(_do_cleanup_object(save.serialize()), f)
        yield str(outfile)
    except Exception as e:
        log.exception(e)
        if outfile.is_file():
            log.error('rm %s', outfile)
            os.remove(outfile)

def cmd_backup(args, batch_save=None):
    global LSO_DIR, BACKUP_DIR
    from ._header import header
    header(args)

    LSO_DIR = Path(args.lso_dir)
    if not LSO_DIR.is_dir():
        log.critical('--lso-dir %r must be present, and not a file.', LSO_DIR)
        return

    BACKUP_DIR = Path(args.to_dir)
    if BACKUP_DIR.is_file():
        log.critical('--to-dir %r must not be a file.', BACKUP_DIR)
        return

    written = []
    meta = {}
    newmeta = {}
    metapath: Path = BACKUP_DIR / 'metadata.yml'

    loaded: bool = False
    if metapath.is_file():
        with metapath.open('r') as f:
            try:
                data: dict = yaml.load(f)
                if data['version'] == META_VERSION:
                    meta = data['meta']
                    loaded = True
            except Exception as e:
                log.exception(e)

    # Go through the Flash directory
    for siteid in os.listdir(LSO_DIR):
        sitepath = os.path.join(LSO_DIR, siteid)
        for root, _, files in os.walk(sitepath):
            for basefilename in files:
                fullpath = Path(os.path.join(root, basefilename))
                namespace = fullpath.relative_to(sitepath).parent
                ext = fullpath.suffixes[-1]
                filename = str(fullpath.name.split('.')[0]) + ''.join(fullpath.suffixes[:-1])
                destprefix = BACKUP_DIR / siteid / namespace / filename / (filename+'.xxx')
                metakey = str(siteid / namespace / filename)

                cwritten: List[str] = []

                stat = os.stat(fullpath)
                mysize = stat.st_size
                mymtime = stat.st_mtime
                theirsize: int
                theirmtime: int
                theirwritten: List[str]
                theirsize, theirmtime, theirwritten = meta.get(metakey, [-1, -1, []])


                with log.info('%s:', fullpath.relative_to(LSO_DIR)):
                    log.debug('siteid=%r, ns=%r, filename=%r', siteid, str(namespace), filename)

                    guessed_dest_file = destprefix.with_suffix(fullpath.suffix)
                    if fullpath.is_file() and guessed_dest_file.is_file() and filecmp.cmp(fullpath, destprefix.with_suffix(fullpath.suffix), shallow=False):
                        changed: bool = False
                        if theirsize == -1 or theirmtime == -1:
                            log.info('Invalid metadata.')
                            changed = True
                        else:
                            if mysize != theirsize:
                                log.info('Size changed. %r != %r', mysize, theirsize)
                                changed = True
                            if mymtime != theirmtime:
                                log.info('Modification time changed. %r != %r', mymtime, theirmtime)
                                changed = True
                        if not changed:
                            log.info('Unchanged.')
                            newmeta[metakey] = [mysize, mymtime, theirwritten]
                            written += theirwritten
                            continue

                    if not destprefix.parent.is_dir():
                        destprefix.parent.mkdir(parents=True)

                    if ext == '.sol':
                        cwritten = list(handleSol(siteid, namespace, filename, fullpath, destprefix))
                    elif ext in ('.ssl',):
                        cwritten = list(handleCopyOnly(siteid, namespace, filename, fullpath, destprefix))
                    else:
                        log.error('UNKNOWN SUFFIX %r', ext)
                newmeta[metakey] = [mysize, mymtime, cwritten]
                written += cwritten
    with metapath.open('w') as f:
        yaml.dump({'version':META_VERSION, 'meta':newmeta}, f)
    written += [str(metapath)]
    toDelete = []
    for root, _, files in os.walk(BACKUP_DIR):
        for _file in files:
            basename = os.path.basename(_file)
            if basename == 'config.yml':
                continue
            filename = os.path.join(root, _file)
            if '.git' in filename:
                continue
            if filename not in written:
                #print('  rm {}'.format(filename))
                toDelete.append(os.path.relpath(filename, start=BACKUP_DIR))
    cwd = os.getcwd()
    os.chdir(BACKUP_DIR)
    try:
        toAdd=[]
        if not os.path.isdir(os.path.join('.git')):
            execute(['git', 'init'], capture=False)
        cfgpath = Path('config.yml')
        cfg = {
            'git': {
                'user.name':  'nobody',
                'user.email': 'devnull@localhost',
            }
        }
        if not cfgpath.is_file():
            log.warning('[git config] Setting user.name to nobody, and user.email to devnull@localhost.')
            log.warning('[git config] This will prevent attaching potentially sensitive information to your CoC backups. If you plan to host the git repo remotely, though, you will have to change this.')
            log.warning('[git config] To change, edit %s.', cfgpath)
            with cfgpath.open('w') as f:
                f.write('# CoCTweak Backup Configuration File\n')
                f.write('\n')
                f.write('# git configuration\n')
                f.write('git:\n')
                f.write('  # NOTE: user.name, user.email, and user.signingkey can be used to identify you if you accidentially push this repo somewhere.\n')
                f.write('  # Therefore, they are set to meaningless values here.\n')
                f.write('  # If you DO plan on pushing this repo, change these as appropriate.\n')
                f.write('  \n')
                f.write('  # Your git username, used when making a commit. Can be anything.\n')
                f.write('  user.name:  nobody\n')
                f.write('  # Your git email, used when making a commit. Can be anything.\n')
                f.write('  user.email: devnull@localhost\n')
                f.write('  # Your GPG signing key, used when making a commit. A path to the key, or empty string.\n')
                f.write('  user.signingkey: ""\n')
                f.write('  \n')
                f.write('  # Only use this if you need it for pushing the repo.\n')
                f.write('  #core.sshCommand: ssh -i "path/to/ssh.key" -F /dev/null\n')

        with cfgpath.open('r') as f:
            cfg = yaml.load(f)
        for k,v in cfg['git'].items():
            execute(['git', 'config', '--local', k, v], capture=False)
        if not os.path.isdir(os.path.join('.git', 'lfs')):
            execute(['git', 'lfs', 'install'], capture=False)
        if not os.path.isfile('.gitattributes'):
            execute(['git', 'lfs', 'track']+LFS_TRACK_GLOBS, capture=False)
        else:
            tracked = set()
            with open('.gitattributes', 'r') as f:
                for line in f:
                    sline = line.strip()
                    if sline == '' or sline.startswith('#'):
                        continue
                    # *.sol filter=lfs diff=lfs merge=lfs -text
                    chunks = sline.split(' ')
                    if 'filter=lfs' in chunks:
                        tracked.add(chunks[0])
            addToTracking = [g for g in LFS_TRACK_GLOBS if g not in tracked]
            rmFromTracking = [g for g in tracked if g not in LFS_TRACK_GLOBS]
            if len(addToTracking) > 0:
                execute(['git', 'lfs', 'track']+addToTracking, capture=False)
            if len(rmFromTracking) > 0:
                execute(['git', 'lfs', 'untrack']+rmFromTracking, capture=False)


        with open('.gitignore', 'w') as f:
            f.write('# This file is overwritten.  Do not edit.\n')
            f.write('*.bak\n')
            f.write('*~\n')
            f.write('/config.yml\n')
        i=1
        modified = 0
        '''
        for line in execute(['git', 'status','--ignore-submodules', '--porcelain', '-uall'], capture=True, display=False).splitlines():
            linechunks=[c for c in line.split(' ') if c != '']
            #print(f'{i}: {linechunks!r}')
            if len(linechunks)>=2:
                status = linechunks[0]
                filename = linechunks[1]
                if status == '??' and (os.path.basename(filename) in ['.gitignore', '.gitattributes'] or filename[-4:] in ('.sol', '.yml')):
                    toAdd+=[filename]
                if status == 'M':
                    modified += 1
            i+=1
        '''
        repo: pygit2.Repository = pygit2.Repository('.')
        for path, status in repo.status().items():
            if (status & pygit2.GIT_STATUS_WT_NEW) > 0:
                toAdd += [path]
                print('+', path)
            elif (status & pygit2.GIT_STATUS_WT_MODIFIED) > 0:
                modified += 1
                print('M', path)
            elif (status & pygit2.GIT_STATUS_WT_DELETED) > 0:
                toDelete += [path]
                print('-', path)
        repo.free()
        if len(toAdd) > 0:
            for af in toAdd:
                # Avoids "changes staged in the index" errors
                execute(['git', 'rm', '--cache', af], capture=False, display=False)
                execute(['git', 'add', af], capture=False)
        if len(toDelete) > 0:
            execute(['git', 'rm'] + toDelete, capture=False)
        if len(toAdd) > 0 or len(toDelete) > 0 or modified > 0:
            execute(['git', 'commit', '-a', '-m', args.message], capture=False)
    finally:
        os.chdir(cwd)
    remove_empty_dirs('backup')
