import sys

from coctweak.cmd._header import header
from coctweak.cmd._utils import buildPiercingArgs, handlePiercingArgs
from coctweak.logsys import getLogger
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.utils import cmd_deprecated, display_length, ERoundingType, parsePropertyAdjustment  # NOQA

log = getLogger(__name__)
def register_parsers__body_cocks(p_body_subp):
    p_body_cocks = p_body_subp.add_parser('cocks', aliases=['cock', 'penis', 'penises'], help='Operations to perform on your cocks.')
    p_body_cocks_subp = p_body_cocks.add_subparsers()

    p_body_cocks_add = p_body_cocks_subp.add_parser('add', help='Add a cock.')
    p_body_cocks_add.add_argument('count', type=int, default=1, help="How many to add.")
    p_body_cocks_add.add_argument('--knot-multiplier', '-k', type=float, default=-1, help="The knot multiplier (W*Km in most mods).")
    p_body_cocks_add.add_argument('--length', '-l', type=float, default=0, help="The length of the cock. Will otherwise use the last cock in the collection as a template.")
    buildPiercingArgs(p_body_cocks_add, None, 'penis')
    p_body_cocks_add.add_argument('--sock', '-s', type=str, default='null', help="Sock ID (none or null for no sock)")
    p_body_cocks_add.add_argument('--type', '-t', type=str, default='', help="Cock type to use. See your mod's CockType enumeration for possible values. Will otherwise use the last cock in the collection as a template.")
    p_body_cocks_add.add_argument('--width', '-w', type=float, default=0, help="The width of the cock. Will otherwise use the last cock in the collection as a template.")
    p_body_cocks_add.set_defaults(cmd=cmd_body_cocks_add)

    p_body_cocks_remove = p_body_cocks_subp.add_parser('remove', aliases=['rm'], help='Remove a cock')
    p_body_cocks_remove.add_argument('cock_id', type=int, help="Which cock to remove (0-n).")
    p_body_cocks_remove.set_defaults(cmd=cmd_body_cocks_remove)

    p_body_cocks_set = p_body_cocks_subp.add_parser('set', help='Change a cock')
    p_body_cocks_set.add_argument('cock_id', type=int, help="Which cock to modify.")
    p_body_cocks_set.add_argument('--knot-multiplier', '-k', type=float, default=-1, help="The knot multiplier (W*Km in most mods).")
    p_body_cocks_set.add_argument('--length', '-l', type=float, default=0, help="The length of the cock in inches.")
    buildPiercingArgs(p_body_cocks_set, None, 'penis')
    p_body_cocks_set.add_argument('--sock', '-s', type=str, default='null', help="Sock ID (none or null for no sock)")
    p_body_cocks_set.add_argument('--type', '-t', type=str, default='', help="Cock type to use. See your mod's CockType enumeration for possible values")
    p_body_cocks_set.add_argument('--width', '-w', type=float, default=0, help="The width of the cock.")
    p_body_cocks_set.set_defaults(cmd=cmd_body_cocks_set)

    p_body_cocks_adjust = p_body_cocks_subp.add_parser('adjust', aliases=['adj'], help='Change a cock using the Adjustment Language.')
    p_body_cocks_adjust.add_argument('cock_id', type=int, help="Which cock to modify.")
    p_body_cocks_adjust.add_argument('--knot-multiplier', '-k', type=str, default=None, help="The knot multiplier (width * mult = knot width, in most mods).")
    p_body_cocks_adjust.add_argument('--length', '-l', type=str, default=None, help="The length of the cock in inches.")
    buildPiercingArgs(p_body_cocks_adjust, None, 'penis')
    p_body_cocks_adjust.add_argument('--sock', '-s', type=str, default=None, help="Sock ID (none or null for no sock)")
    p_body_cocks_adjust.add_argument('--type', '-t', type=str, default=None, help="Cock type to use. See your mod's CockType enumeration for possible values")
    p_body_cocks_adjust.add_argument('--width', '-w', type=str, default=None, help="The width of the cock.")
    p_body_cocks_adjust.set_defaults(cmd=cmd_body_cocks_adjust)

    p_body_cocks_average = p_body_cocks_subp.add_parser('average', aliases=['avg'], help='Average out the size of all your cocks.')
    p_body_cocks_average.add_argument('--not-length', action='store_true', default=False, help='Do not average length.')
    p_body_cocks_average.add_argument('--not-width', action='store_true', default=False, help='Do not average width.')
    p_body_cocks_average.set_defaults(cmd=cmd_body_cocks_average)

def cmd_body_cocks_add(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    lastcock = save.COCK_TYPE(save)
    if len(save.cocks) > 0:
        lastcock.deserialize(save.cocks[-1].serialize())
    else:
        lastcock.type = lastcock.COCK_TYPES.HUMAN
        lastcock.length=5.5
        lastcock.thickness=1

    newcock = save.COCK_TYPE(save)
    newcock.type = save.COCK_TYPE.COCK_TYPES[args.type] if args.type != '' else lastcock.type
    newcock.length = args.length if args.length > 0 else lastcock.length
    newcock.thickness = args.width if args.width > 0 else lastcock.thickness
    newcock.knotMultiplier = args.knot_multiplier if args.knot_multiplier > -1 else lastcock.knotMultiplier
    newcock.sock = args.sock if args.sock not in ('', 'none', 'null') else lastcock.sock
    handlePiercingArgs(newcock, args, None)
    ncocks = len(save.cocks)
    with log.info('Added:'):
        for _ in range(args.count):
            with log.info('%d:', ncocks):
                save.cocks.append(newcock)
                newcock.display()
            ncocks += 1
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_cocks_remove(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    cock = save.cocks[args.cock_id]
    with log.info('Removing:'):
        save.cocks.remove(cock)
        cock.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_cocks_set(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    cock = save.cocks[args.cock_id]
    with log.info('Before:'):
        cock.display()

    if args.type != '':
        cock.setCockType(save.COCK_TYPE.COCK_TYPES[args.type])
    if args.length > 0:
        cock.length = args.length
    if args.width > 0:
        cock.thickness = args.width
    if args.knot_multiplier > -1:
        cock.knotMultiplier = args.knot_multiplier
    if args.sock not in ('', 'none', 'null'):
        cock.sock = args.sock
    handlePiercingArgs(cock, args, None)

    with log.info('After:'):
        cock.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_cocks_adjust(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    cock = save.cocks[args.cock_id]
    with log.info('Before:'):
        cock.display()

    if args.type is not None:
        cock.setCockType(save.COCK_TYPE.COCK_TYPES[args.type])
    if args.length is not None:
        cock.length = float(parsePropertyAdjustment(cock.length, args.length, cock.getMinLength(save), cock.getMaxLength(save)))
    if args.width is not None:
        cock.thickness = float(parsePropertyAdjustment(cock.thickness, args.width, cock.getMinThickness(save), cock.getMaxThickness(save)))
    if args.knot_multiplier is not None:
        cock.knotMultiplier = float(parsePropertyAdjustment(cock.knotMultiplier, args.knot_multiplier, cock.getMinKnotMultiplier(save), cock.getMaxKnotMultiplier(save)))
    if args.sock is not None:
        cock.sock = args.sock
    handlePiercingArgs(cock, args, None)

    with log.info('After:'):
        cock.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_cocks_average(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    lt=0
    tt=0

    with log.info('Before:'):
        save.displayCocks()

    for cock in save.cocks:
        lt += cock.length
        tt += cock.thickness

    avgl = lt/len(save.cocks)
    avgt = tt/len(save.cocks)
    for cock in save.cocks:
        if not args.not_length:
            cock.length = avgl
        if not args.not_width:
            cock.thickness = avgt

    with log.info('After:'):
        save.displayCocks()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)
