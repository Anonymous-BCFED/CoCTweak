import argparse
import sys

from coctweak.cmd._header import header
from coctweak.cmd._utils import buildPiercingArgs, handlePiercingArgs
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.saves.common.piercing import BasePiercing
from coctweak.utils import bool_from_boolargs, cmd_deprecated, display_length, ERoundingType, parseBoolean, parsePropertyAdjustment  # NOQA

from coctweak.logsys import getLogger #isort: skip
log = getLogger(__name__)

def register_parsers__body_vaginas(p_body_subp):
    p_body_vaginas = p_body_subp.add_parser('vaginas', aliases=['vagina', 'vag', 'vags'], help='Operations to perform on your vaginas.')
    p_body_vaginas_subp = p_body_vaginas.add_subparsers()

    p_body_vaginas_add = p_body_vaginas_subp.add_parser('add', help='Add a vagina')
    p_body_vaginas_add.add_argument('count', type=int, default=1, help="How many to add.")
    p_body_vaginas_add.add_argument('--clit-length', '-l', type=float, default=None, help="The length of the clit. Will otherwise use the last vagina in the collection as a template.")
    buildPiercingArgs(p_body_vaginas_add, 'clit', 'clit')
    p_body_vaginas_add.add_argument('--fullness', '-f', type=float, default=None, help="The fullness of the vagina. Will otherwise use the last vagina in the collection as a template.")
    buildPiercingArgs(p_body_vaginas_add, 'labia', 'labia')
    p_body_vaginas_add.add_argument('--looseness', '-L', type=str, default=None, help="The looseness of the vagina. See the appropriate VagLooseness enum for your mod.")
    p_body_vaginas_add.add_argument('--recovery-progress', '-r', type=int, default=None, help="How much your vagina needs to recover from being stretched. Units unknown.")
    p_body_vaginas_add.add_argument('--type', '-t', type=str, default=None, help="vagina type to use. See your mod's vaginaType enumeration for possible values. Will otherwise use the last vagina in the collection as a template.")
    p_body_vaginas_add.add_argument('--virgin', type=parseBoolean, default=None, help="Whether the vagina is virgin.")
    p_body_vaginas_add.add_argument('--wetness', '-W', type=str, default=None, help="The wetness of the vagina. See the appropriate VagWetness enum for your mod.")
    p_body_vaginas_add.set_defaults(cmd=cmd_body_vaginas_add)

    p_body_vaginas_remove = p_body_vaginas_subp.add_parser('remove', aliases=['rm'], help='Remove a vagina')
    p_body_vaginas_remove.add_argument('vagina_id', type=int, help="Which vagina to remove (0-n).")
    p_body_vaginas_remove.set_defaults(cmd=cmd_body_vaginas_remove)

    p_body_vaginas_set = p_body_vaginas_subp.add_parser('set', help='Change a vagina')
    p_body_vaginas_set.add_argument('vagina_id', type=int, help="Which vagina to modify. 0-n")
    p_body_vaginas_set.add_argument('--clit-length', '-l', type=float, default=-1, help="The length of the clit. Will otherwise use the last vagina in the collection as a template.")
    buildPiercingArgs(p_body_vaginas_set, 'clit', 'clit')
    p_body_vaginas_set.add_argument('--fullness', '-f', type=float, default=-1, help="The fullness of the vagina. Will otherwise use the last vagina in the collection as a template.")
    buildPiercingArgs(p_body_vaginas_set, 'labia', 'labia')
    p_body_vaginas_set.add_argument('--looseness', '-L', type=str, default='', help="The looseness of the vagina. See the appropriate VagLooseness enum for your mod.")
    p_body_vaginas_set.add_argument('--recovery-progress', '-r', type=int, default=-1, help="How much your vagina has to recover from being stretched.  Units unknown.")
    p_body_vaginas_set.add_argument('--type', '-t', type=str, default='', help="vagina type to use. See your mod's vaginaType enumeration for possible values")
    p_body_vaginas_set.add_argument('--virgin', type=parseBoolean, default=None, help="Whether the vagina is virgin.")
    p_body_vaginas_set.add_argument('--wetness', '-W', type=str, default='', help="The wetness of the vagina. See the appropriate VagWetness enum for your mod.")
    p_body_vaginas_set.set_defaults(cmd=cmd_body_vaginas_set)

    p_body_vaginas_adjust = p_body_vaginas_subp.add_parser('adjust', aliases=['adj'], help='Change a vagina using Adjustment Language.')
    p_body_vaginas_adjust.add_argument('vagina_id', type=int, help="Which vagina to modify. 0-n")
    p_body_vaginas_adjust.add_argument('--clit-length', '-l', type=str, default=None, help="The length of the clit. Will otherwise use the last vagina in the collection as a template.")
    buildPiercingArgs(p_body_vaginas_adjust, 'clit', 'clit')
    p_body_vaginas_adjust.add_argument('--fullness', '-f', type=str, default=None, help="The fullness of the vagina. Will otherwise use the last vagina in the collection as a template.")
    buildPiercingArgs(p_body_vaginas_adjust, 'labia', 'labia')
    p_body_vaginas_adjust.add_argument('--looseness', '-L', type=str, default=None, help="The looseness of the vagina. See the appropriate VagLooseness enum for your mod.")
    p_body_vaginas_adjust.add_argument('--recovery-progress', '-r', type=str, default=None, help="How much your vagina has to recover from being stretched.  Units unknown.")
    p_body_vaginas_adjust.add_argument('--type', '-t', type=str, default=None, help="vagina type to use. See your mod's vaginaType enumeration for possible values")
    p_body_vaginas_adjust.add_argument('--virgin', type=parseBoolean, default=None, help="Whether the vagina is virgin.")
    p_body_vaginas_adjust.add_argument('--wetness', '-W', type=str, default=None, help="The wetness of the vagina. See the appropriate VagWetness enum for your mod.")
    p_body_vaginas_adjust.set_defaults(cmd=cmd_body_vaginas_adjust)

    p_body_vaginas_average = p_body_vaginas_subp.add_parser('average', aliases=['avg'], help='Average out the size of all your clits.')
    p_body_vaginas_average.set_defaults(cmd=cmd_body_vaginas_average)

def cmd_body_vaginas_add(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    lastvagina = save.VAGINA_TYPE(save)
    if len(save.vaginas) > 0:
        lastvagina.deserialize(save.vaginas[-1].serialize())
    else:
        lastvagina.type = lastvagina.ENUM_TYPES.HUMAN
        lastvagina.length = 5.5
        lastvagina.thickness = 1.0

    newvagina = save.VAGINA_TYPE(save)
    newvagina.type = save.VAGINA_TYPE.ENUM_TYPES[args.type] if args.type is not None else lastvagina.type
    newvagina.clitLength = args.clit_length if args.clit_length is not None else lastvagina.clitLength
    newvagina.fullness = args.fullness if args.fullness is not None else lastvagina.fullness
    newvagina.looseness = save.VAGINA_TYPE.ENUM_LOOSENESS[args.looseness] if args.looseness is not None else lastvagina.looseness
    newvagina.recoveryProgress = args.recovery_progress if args.recovery_progress is not None else 0
    newvagina.wetness = save.VAGINA_TYPE.ENUM_WETNESS[args.wetness] if args.wetness is not None else lastvagina.wetness

    handlePiercingArgs(newvagina, args, 'clit')
    handlePiercingArgs(newvagina, args, 'labia')

    if args.virgin is not None:
        newvagina.virgin = args.virgin

    with log.info('Added:'):
        for _ in range(args.count):
            save.vaginas.append(newvagina)
            newvagina.display()

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_vaginas_remove(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    vagina = save.vaginas[args.vagina_id]
    with log.info('Removing:'):
        save.vaginas.remove(vagina)
        vagina.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_vaginas_set(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    vagina = save.vaginas[args.vagina_id]
    with log.info('Before:'):
        vagina.display()

    if args.type != '':
        vagina.type = save.VAGINA_TYPE.ENUM_TYPES[args.type]
    if args.clit_length > -1:
        vagina.clitLength = args.clit_length
    if args.fullness > -1:
        vagina.fullness = args.fullness
    if args.recovery_progress > -1:
        vagina.recoveryProgress = args.recovery_progress

    handlePiercingArgs(vagina, args, 'clit')
    handlePiercingArgs(vagina, args, 'labia')

    if args.virgin is not None:
        vagina.virgin = args.virgin
    vagina.looseness = save.VAGINA_TYPE.ENUM_LOOSENESS[args.looseness] if args.looseness != '' else vagina.looseness
    vagina.wetness = save.VAGINA_TYPE.ENUM_WETNESS[args.wetness] if args.wetness != '' else vagina.wetness

    with log.info('After:'):
        vagina.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_vaginas_adjust(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    vagina = save.vaginas[args.vagina_id]
    with log.info('Before:'):
        vagina.display()

    if args.type is not None:
        vagina.type = save.VAGINA_TYPE.ENUM_TYPES[args.type]
    if args.clit_length is not None:
        vagina.clitLength = float(parsePropertyAdjustment(vagina.clitLength, args.clit_length, vagina.getMinClitLength(save), vagina.getMaxClitLength(save)))
    if args.fullness is not None:
        vagina.fullness = float(parsePropertyAdjustment(vagina.fullness, args.fullness, vagina.getMinFullness(save), vagina.getMaxFullness(save)))
    if args.recovery_progress is not None:
        vagina.recoveryProgress = float(parsePropertyAdjustment(vagina.recoveryProgress, args.recovery_progress, vagina.getMinRecoveryProgress(save), vagina.getMaxRecoveryProgress(save)))
    if args.looseness is not None:
        vagina.looseness = vagina.ENUM_LOOSENESS(int(parsePropertyAdjustment(vagina.looseness.value, args.looseness, vagina.getMinLooseness(save), vagina.getMaxLooseness(save), round_type=ERoundingType.ROUND)))
    if args.wetness is not None:
        vagina.wetness = vagina.ENUM_WETNESS(int(parsePropertyAdjustment(vagina.wetness.value, args.wetness, vagina.getMinWetness(save), vagina.getMaxWetness(save), round_type=ERoundingType.ROUND)))

    handlePiercingArgs(vagina, args, 'clit')
    handlePiercingArgs(vagina, args, 'labia')

    if args.virgin is not None:
        vagina.virgin = args.virgin

    with log.info('After:'):
        vagina.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_vaginas_average(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    lt=0

    with log.info('Before:'):
        save.displayVaginas()

    for vagina in save.vaginas:
        lt += vagina.clitLength

    avgl = lt/len(save.vaginas)
    for vagina in save.vaginas:
        vagina.clitLength = avgl

    with log.info('After:'):
        save.displayVaginas()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)
