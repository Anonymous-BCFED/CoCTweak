import sys
from typing import Any

from coctweak.cmd._header import header
from coctweak.logsys import getLogger
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.saves._save import SaveCaps
from coctweak.utils import cmd_deprecated, display_length, ERoundingType, parsePropertyAdjustment  # NOQA

log = getLogger(__name__)

def register_parsers__body_tail(p_body_subp):
    p_body_tail = p_body_subp.add_parser('tail', help='Operations to perform on your tail.')

    p_body_tail_subp = p_body_tail.add_subparsers()

    _register_parsers__body_tail_adjust(p_body_tail_subp)
    _register_parsers__body_tail_set(p_body_tail_subp)
    _register_parsers__body_tail_show(p_body_tail_subp)

def _register_parsers__body_tail_set(p_body_tail_subp):
    p_body_tail_set = p_body_tail_subp.add_parser('set', help='Change properties of your tail.')
    p_body_tail_set.add_argument('--type', '-t', type=str, default=None, help="Tail type to use. See your mod's Tail.as constants for possible values. (PLAIN, FUR, etc)")
    p_body_tail_set.add_argument('--venom', '-v', type=int, default=None, help="How much venom it contains.")
    p_body_tail_set.add_argument('--recharge', '-r', type=int, default=None, help="Recharge rate")
    p_body_tail_set.set_defaults(cmd=cmd_body_tail_set)

def cmd_body_tail_set(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    old: Any
    new: Any
    if args.type is not None:
        old = save.tail.type
        if args.type.isnumeric():
            new = save.tail.ENUM_TYPES(int(args.type))
        else:
            new = save.tail.ENUM_TYPES[args.type]
        save.tail.type = new
        log.info(f'Tail type changed: {old} -> {new}')
        if new.name == 'NONE':
            if args.venom is None:
                args.venom = 0
            if args.recharge is None:
                args.recharge = 0
    if args.venom is not None:
        old = save.tail.venom
        new = save.tail.venom = args.venom
        log.info(f'Tail venom changed: {old!r} -> {new!r}')
    if args.recharge is not None:
        old = save.tail.recharge
        new = save.tail.recharge = args.recharge
        log.info(f'Tail recharge changed: {old!r} -> {new!r}')
        
    save.tail.display()

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def _register_parsers__body_tail_show(p_body_tail_subp):
    p_body_tail_show = p_body_tail_subp.add_parser('show', help='Show properties of your tail.')
    p_body_tail_show.set_defaults(cmd=cmd_body_tail_show)
def cmd_body_tail_show(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    save.tail.display()


def _register_parsers__body_tail_adjust(p_body_tail_subp):
    p_body_tail_adjust = p_body_tail_subp.add_parser('adjust', aliases=['adj'], help='Adjust your tail using the adjustment system. (See docs)')
    p_body_tail_adjust.add_argument('--type', '-t', type=str, default=None, help="Tail type to use. See your mod's Tail.as constants for possible values. (PLAIN, FUR, etc) DOES NOT USE ADJUSTMENT LANGUAGE")
    p_body_tail_adjust.add_argument('--venom', '-v', type=str, default=None)
    p_body_tail_adjust.add_argument('--recharge', '-r', type=str, default=None)
    p_body_tail_adjust.set_defaults(cmd=cmd_body_tail_adjust)
def cmd_body_tail_adjust(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    old: Any
    new: Any
    if args.type is not None:
        old = save.tail.type
        if args.type.isnumeric():
            new = save.tail.ENUM_TYPES(int(args.type))
        else:
            new = save.tail.ENUM_TYPES[args.type]
        save.tail.type = new
        log.info(f'Tail type changed: {old} -> {new}')
        if new.name == 'NONE':
            if args.venom is None:
                args.venom = '=0'
            if args.recharge is None:
                args.recharge = '=0'

    if args.venom is not None:
        save.tail.venom = parsePropertyAdjustment(save.tail.venom, args.venom, 0., sys.float_info.max, ERoundingType.NONE)
    if args.recharge is not None:
        save.tail.recharge = parsePropertyAdjustment(save.tail.recharge, args.recharge, 0., sys.float_info.max, ERoundingType.NONE)

    save.tail.display()

    if batch_save is None:
        saveToSlot(args.host, args.id, save)