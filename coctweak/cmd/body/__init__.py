import argparse
import sys

from coctweak.cmd._header import header
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.utils import add_hostid_args

from .balls import register_parsers__body_balls
from .breasts import register_parsers__body_breasts
from .cocks import register_parsers__body_cocks
from .face import register_parsers__body_face
from .skin import register_parsers__body_skin
from .tail import register_parsers__body_tail
from .vaginas import register_parsers__body_vaginas


def register_parsers__body(subp: argparse.ArgumentParser, batch: bool=False):
    p_body = subp.add_parser('body', help='Change the PC\'s body')
    if not batch:
        add_hostid_args(p_body, batch)

    p_body_subp = p_body.add_subparsers()

    register_parsers__body_balls(p_body_subp)
    register_parsers__body_breasts(p_body_subp)
    register_parsers__body_cocks(p_body_subp)
    register_parsers__body_face(p_body_subp)
    register_parsers__body_skin(p_body_subp)
    register_parsers__body_tail(p_body_subp)
    register_parsers__body_vaginas(p_body_subp)