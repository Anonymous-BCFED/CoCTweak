import sys

from coctweak.cmd._header import header
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.saves._save import SaveCaps
from coctweak.utils import cmd_deprecated, display_length, ERoundingType, parsePropertyAdjustment  # NOQA
from coctweak.flash_consts import *

from coctweak.logsys import getLogger #isort: skip
log = getLogger(__name__)
def register_parsers__body_balls(p_body_subp):
    p_body_balls = p_body_subp.add_parser('balls', help='Operations to perform on your balls.')
    p_body_balls_subp = p_body_balls.add_subparsers()

    _register_parsers__body_balls_adjust(p_body_balls_subp)
    _register_parsers__body_balls_set(p_body_balls_subp)
    _register_parsers__body_balls_show(p_body_balls_subp)


def _displayNutState(save):
    if save.balls.count == 0:
        log.info('You now have no balls.')
    else:
        ballOrBalls = 'ball' if save.balls.count == 1 else 'balls'
        with log.info(f'You now have {save.balls.count} {ballOrBalls}, each {display_length(save.balls.size)} in diameter, with a cum multiplier of {save.balls.cumMultiplier}.'):
            if save.balls.count > 4 and (save.CAPABILITIES & SaveCaps.MORE_THAN_FOUR_BALLS) != SaveCaps.MORE_THAN_FOUR_BALLS:
                log.warning('Having more than 4 balls in this game may cause bugs, since it\'s impossible to get this many "naturally".')

def _register_parsers__body_balls_show(p_body_balls_subp):
    p_body_balls_show = p_body_balls_subp.add_parser('show', help='Show properties of your balls.')
    p_body_balls_show.set_defaults(cmd=cmd_body_balls_show)
def cmd_body_balls_show(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    save.balls.display()

def _register_parsers__body_balls_set(p_body_balls_subp):
    p_body_balls_set = p_body_balls_subp.add_parser('set', help='Change properties of your nuts.')
    p_body_balls_set.add_argument('--count', '-c', type=int, default=None, help="How many testicles you want")
    p_body_balls_set.add_argument('--size', '-s', type=float, default=None, help="How big each testicle is (in inches, diameter).")
    p_body_balls_set.add_argument('--mult', '-M', type=float, default=None, help="Cum multiplier, AKA virility.")
    p_body_balls_set.set_defaults(cmd=cmd_body_balls_set)
def cmd_body_balls_set(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    if args.count is not None:
        save.balls.count = max(args.count, 0)
    if args.size is not None:
        save.balls.size = max(args.size, 0)
    if args.mult is not None:
        save.balls.cumMultiplier = max(args.size, 1)

    _displayNutState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def _register_parsers__body_balls_adjust(p_body_balls_subp):
    p_body_balls_adjust = p_body_balls_subp.add_parser('adjust', aliases=['adj'], help='Adjust your nuts using the adjustment system. (See docs)')
    p_body_balls_adjust.add_argument('--count', '-c', type=str, default=None, help="How many testicles you want")
    p_body_balls_adjust.add_argument('--size', '-s', type=str, default=None, help="How big each testicle is (in inches, diameter).")
    p_body_balls_adjust.add_argument('--mult', '-M', type=str, default=None, help="Cum multiplier, AKA virility.")
    p_body_balls_adjust.set_defaults(cmd=cmd_body_balls_adjust)
def cmd_body_balls_adjust(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    if args.count is not None:
        save.balls.count = parsePropertyAdjustment(save.balls.count, args.count, 0, FL_INT_MAX, ERoundingType.ROUND)
    if args.size is not None:
        save.balls.size = parsePropertyAdjustment(save.balls.size, args.size, 0.01, FL_NUM_MAX, ERoundingType.NONE)
    if args.mult is not None:
        save.balls.cumMultiplier = parsePropertyAdjustment(save.balls.cumMultiplier, args.mult, 0.01, FL_NUM_MAX, ERoundingType.NONE)

    _displayNutState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)
