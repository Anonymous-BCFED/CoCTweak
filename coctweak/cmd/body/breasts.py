from coctweak.flash_consts import FL_INT_MAX, FL_NUM_MAX
from coctweak.cmd._header import header
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.utils import ERoundingType, parseAndClampFloat, parseAndClampInt, parsePropertyAdjustment, parseBoolean

from coctweak.logsys import getLogger #isort: skip
log = getLogger(__name__)

def register_parsers__body_breasts(p_body_subp):
    p_body_breasts = p_body_subp.add_parser('breasts', help='Operations to perform on your breasts.')
    p_body_breasts_subp = p_body_breasts.add_subparsers()

    _register_parsers__body_breasts_add(p_body_breasts_subp)
    _register_parsers__body_breasts_adjust(p_body_breasts_subp)
    _register_parsers__body_breasts_remove(p_body_breasts_subp)
    _register_parsers__body_breasts_removeall(p_body_breasts_subp)
    _register_parsers__body_breasts_set(p_body_breasts_subp)
    _register_parsers__body_breasts_show(p_body_breasts_subp)


def _displayBoobState(save):
    if len(save.breast_rows) == 0:
        log.warning('You have no breast rows.  THIS IS AN INVALID STATE IN SOME MODS, ADD A BREAST ROW AND FLATTEN IT.')
    else:
        srow = 'row' if len(save.breast_rows) == 1 else 'rows'
        row = save.getBiggestBreastRow()
        if row.fuckable:
            fuckableYesNo = 'fuckable nipples'
        else:
            fuckableYesNo = 'no fuckable nipples'
        with log.info(f'You now have {len(save.breast_rows)} breast {srow}, each containing {row.breasts} breasts {row.breastRating} in size ({row.getCupSize()}), with {row.nipplesPerBreast} nipples per breast, {row.fullness} fullness, {row.milkFullness} milk fullness, a {row.lactationMultiplier}, and {fuckableYesNo}.'):
            for i, row in enumerate(save.breast_rows):
                with log.info('Row %d:', i):
                    for k,v in row.getVars().items():
                        log.info(f'{k}: {v}')

def _register_parsers__body_breasts_show(p_body_breasts_subp):
    p_body_breasts_show = p_body_breasts_subp.add_parser('show', help='Show properties of your breasts.')
    p_body_breasts_show.set_defaults(cmd=cmd_body_breasts_show)
def cmd_body_breasts_show(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    _displayBoobState(save)

def _register_parsers__body_breasts_removeall(p_body_breasts_subp):
    p_body_breasts_removeall = p_body_breasts_subp.add_parser('remove-all', help='Remove all breast rows expect the last, and flatten it.')
    p_body_breasts_removeall.add_argument('--literally-all', action='store_true', default=False, help='Remove every breast row, period.  No flattening.')
    p_body_breasts_removeall.add_argument('--reset-to-human-male', action='store_true', default=False, help='Reset to default human male.')
    p_body_breasts_removeall.add_argument('--reset-to-human-female', action='store_true', default=False, help='Reset to default human female.')
    p_body_breasts_removeall.add_argument('--flatten', action='store_true', default=False, help='Flatten all breast rows, does not remove any.')
    p_body_breasts_removeall.set_defaults(cmd=cmd_body_breasts_removeall)

def _removeBoobsUntilSize(save, sz: int) -> None:
    assert sz >= 0
    start = len(save.breast_rows)
    while len(save.breast_rows) > sz:
        save.breast_rows.pop(0)
    log.info(f'Removed {start-len(save.breast_rows)} breast rows')

def cmd_body_breasts_removeall(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    
    if args.literally_all:
        _removeBoobsUntilSize(save, 0)
    elif args.reset_to_human_male:
        _removeBoobsUntilSize(save, 0)
        save.createBreastRow(0) # Flat
    elif args.reset_to_human_female:
        _removeBoobsUntilSize(save, 0)
        save.createBreastRow(1) # A-Cup
    elif args.flatten:
        for i, br in enumerate(save.breast_rows):
            oldsz = br.breastRating
            br.breastRating = 0
            log.info('Row %d: Size %g -> %g', i, oldsz, 0)
    else:
        raise ValueError('Specify an operation (--literally-all, --reset-to-human-male, etc')

    _displayBoobState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def _register_parsers__body_breasts_remove(p_body_breasts_subp):
    p_body_breasts_remove = p_body_breasts_subp.add_parser('remove', aliases=['rm'], help='Remove the specified breast row.')
    p_body_breasts_remove.add_argument('row', type=int, help="Which row to remove")
    p_body_breasts_remove.set_defaults(cmd=cmd_body_breasts_remove)
def cmd_body_breasts_remove(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    del save.breast_rows[args.row]

    _displayBoobState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def _register_parsers__body_breasts_add(p_body_breasts_subp):
    p_body_breasts_set = p_body_breasts_subp.add_parser('add', help='Add a number of breast rows.')
    p_body_breasts_set.add_argument('count', type=int, default=1, help="How many to add")
    p_body_breasts_set.add_argument('--breasts-per-row', '-p', type=int, default=None, help="How many boobs in this row")
    p_body_breasts_set.add_argument('--fuckable', '-F', type=bool, default=None, help="Whether the nipples can be fucked")
    p_body_breasts_set.add_argument('--fullness', '-f', type=float, default=None, help="How much cum is in your tits")
    p_body_breasts_set.add_argument('--lactation-multiplier', '-l', type=float, default=None, help="How much you lactate")
    p_body_breasts_set.add_argument('--milk-fullness', '-M', type=float, default=None, help="How much milk is in your tits")
    p_body_breasts_set.add_argument('--nipple-cocks', '-c', type=bool, default=None, help="Whether nipplecocks are present")
    p_body_breasts_set.add_argument('--nipples-per-breast', '-n', type=int, default=None, help="How many nipples per breast in this row")
    p_body_breasts_set.add_argument('--rating', '-r', type=float, default=None, help="The size of the boobs in this row")
    p_body_breasts_set.set_defaults(cmd=cmd_body_breasts_add)
def cmd_body_breasts_add(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    
    for _ in range(args.count):
        row = save.BREASTROW_TYPE(save)
        if len(save.breast_rows):
            row.cloneFrom(save.breast_rows[-1])

        save.breast_rows.append(row)

        if args.breasts_per_row is not None: 
            row.setProp('breasts', parseAndClampInt(args.breasts_per_row, 1))
        if args.rating is not None: 
            row.setProp('breastRating', parseAndClampFloat(args.rating, 0.))
        if args.nipples_per_breast is not None: 
            row.setProp('nipplesPerBreast', parseAndClampInt(args.nipples_per_breast, 0))
        if args.lactation_multiplier is not None: 
            row.setProp('lactationMultiplier', parseAndClampFloat(args.lactation_multiplier, 0.))
        if args.fullness is not None: 
            row.setProp('fullness', parseAndClampFloat(args.fullness, 0.))
        if args.milk_fullness is not None: 
            row.setProp('milkFullness', parseAndClampFloat(args.milk_fullness, 0.))
        if args.nipple_cocks is not None and hasattr(row, 'nippleCocks'): 
            row.setProp('nippleCocks', parseBoolean(args.nipple_cocks))
        if args.fuckable is not None: 
            row.setProp('fuckable', bool(args.fuckable))

    _displayBoobState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def _register_parsers__body_breasts_set(p_body_breasts_subp):
    p_body_breasts_set = p_body_breasts_subp.add_parser('set', help='Change properties of your breasts.')
    p_body_breasts_set.add_argument('row', type=int, help="Which row to modify")
    p_body_breasts_set.add_argument('--breasts-per-row', '-p', type=int, default=None, help="How many boobs in this row")
    p_body_breasts_set.add_argument('--fuckable', '-F', type=bool, default=None, help="Whether the nipples can be fucked")
    p_body_breasts_set.add_argument('--fullness', '-f', type=float, default=None, help="How much cum is in your tits")
    p_body_breasts_set.add_argument('--lactation-multiplier', '-l', type=float, default=None, help="How much you lactate")
    p_body_breasts_set.add_argument('--milk-fullness', '-M', type=float, default=None, help="How much milk is in your tits")
    p_body_breasts_set.add_argument('--nipple-cocks', '-c', type=bool, default=None, help="Whether nipplecocks are present")
    p_body_breasts_set.add_argument('--nipples-per-breast', '-n', type=int, default=None, help="How many nipples per breast in this row")
    p_body_breasts_set.add_argument('--rating', '-r', type=float, default=None, help="The size of the boobs in this row")
    p_body_breasts_set.set_defaults(cmd=cmd_body_breasts_set)
def cmd_body_breasts_set(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    
    row = save.breast_rows[args.row]

    if args.breasts_per_row is not None: 
        row.setProp('breasts', int(args.breasts_per_row))
    if args.rating is not None: 
        row.setProp('breastRating', float(args.rating))
    if args.nipples_per_breast is not None: 
        row.setProp('nipplesPerBreast', int(args.nipples_per_breast))
    if args.lactation_multiplier is not None: 
        row.setProp('lactationMultiplier', float(args.lactation_multiplier))
    if args.fullness is not None: 
        row.setProp('fullness', float(args.fullness))
    if args.milk_fullness is not None: 
        row.setProp('milkFullness', float(args.milk_fullness))
    if args.nipple_cocks is not None and hasattr(row, 'nippleCocks'): 
        row.setProp('nippleCocks', bool(args.nipple_cocks))
    if args.fuckable is not None: 
        row.setProp('fuckable', bool(args.fuckable))

    _displayBoobState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def _register_parsers__body_breasts_adjust(p_body_breasts_subp):
    p_body_breasts_adjust = p_body_breasts_subp.add_parser('adjust', aliases=['adj'], help='Adjust your boobs using the adjustment system. (See docs)')
    p_body_breasts_adjust.add_argument('row', type=int, help="Which row to modify")
    p_body_breasts_adjust.add_argument('--breasts-per-row', '-p', type=str, default=None, help="How many boobs in this row")
    p_body_breasts_adjust.add_argument('--fuckable', '-F', type=str, default=None, help="Whether the nipples can be fucked")
    p_body_breasts_adjust.add_argument('--fullness', '-f', type=str, default=None, help="How much cum is in your tits")
    p_body_breasts_adjust.add_argument('--lactation-multiplier', '-l', type=str, default=None, help="How much you lactate")
    p_body_breasts_adjust.add_argument('--milk-fullness', '-M', type=str, default=None, help="How much milk is in your tits")
    p_body_breasts_adjust.add_argument('--nipple-cocks', '-c', type=str, default=None, help="Whether nipplecocks are present")
    p_body_breasts_adjust.add_argument('--nipples-per-breast', '-n', type=str, default=None, help="How many nipples per breast in this row")
    p_body_breasts_adjust.add_argument('--rating', '-r', type=str, default=None, help="The size of the boobs in this row")
    p_body_breasts_adjust.set_defaults(cmd=cmd_body_breasts_adjust)
def cmd_body_breasts_adjust(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    
    row = save.breast_rows[args.row]

    if args.breasts_per_row is not None: 
        row.setProp('breasts', parsePropertyAdjustment(row.breasts, args.breasts_per_row, 1, FL_INT_MAX, ERoundingType.ROUND))
    if args.rating is not None: 
        row.setProp('breastRating', parsePropertyAdjustment(row.breastRating, args.rating, 0., FL_NUM_MAX, ERoundingType.NONE))
    if args.nipples_per_breast is not None: 
        row.setProp('nipplesPerBreast', parsePropertyAdjustment(row.nipplesPerBreast, args.nipples_per_breast, 0, FL_INT_MAX, ERoundingType.ROUND))
    if args.lactation_multiplier is not None: 
        row.setProp('lactationMultiplier', parsePropertyAdjustment(row.lactationMultiplier, args.lactation_multiplier, 0, FL_NUM_MAX, ERoundingType.NONE))
    if args.fullness is not None: 
        row.setProp('fullness', parsePropertyAdjustment(row.fullness, args.fullness, 0., FL_NUM_MAX, ERoundingType.NONE))
    if args.milk_fullness is not None: 
        row.setProp('milkFullness', parsePropertyAdjustment(row.milkFullness, args.milk_fullness, 0, FL_NUM_MAX, ERoundingType.NONE))
    if args.nipple_cocks is not None and hasattr(row, 'nippleCocks'): 
        row.setProp('nippleCocks', parseBoolean(args.nipple_cocks))
    if args.fuckable is not None: 
        row.setProp('fuckable', parseBoolean(args.fuckable))

    _displayBoobState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)
