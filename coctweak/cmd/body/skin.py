import sys
from typing import Any

from coctweak.cmd._header import header
from coctweak.logsys import getLogger
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.saves._save import SaveCaps
from coctweak.utils import cmd_deprecated, display_length, ERoundingType, parsePropertyAdjustment  # NOQA

log = getLogger(__name__)

def register_parsers__body_skin(p_body_subp):
    p_body_skin = p_body_subp.add_parser('skin', help='Operations to perform on your skin.')

    p_body_skin_subp = p_body_skin.add_subparsers()

    _register_parsers__body_skin_set(p_body_skin_subp)
    _register_parsers__body_skin_show(p_body_skin_subp)

def _register_parsers__body_skin_set(p_body_skin_subp):
    p_body_skin_set = p_body_skin_subp.add_parser('set', help='Change properties of your skin.')
    p_body_skin_set.add_argument('--adj',  '-a', type=str, default=None, help="Adjective.  Usually blank.")
    p_body_skin_set.add_argument('--desc', '-d', type=str, default=None, help="Skin, fur, etc.")
    p_body_skin_set.add_argument('--fur',  '-f', type=str, default=None, help="The color of your fur. Set to 'no' if you have no fur.")
    p_body_skin_set.add_argument('--tone', '-T', type=str, default=None, help="Underlying skin tone.  Not shown if you have fur.")
    p_body_skin_set.add_argument('--type', '-t', type=str, default=None, help="Skin type to use. See your mod's Skin.as constants for possible values. (PLAIN, FUR, etc)")
    p_body_skin_set.set_defaults(cmd=cmd_body_skin_set)

def cmd_body_skin_set(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    old: Any
    new: Any
    if args.type is not None:
        old = save.skin.type
        if args.type.isnumeric():
            new = save.skin.type = save.skin.ENUM_TYPES(int(args.type))
        else:
            new = save.skin.type = save.skin.ENUM_TYPES[args.type]
        log.info(f'Skin type changed: {old} -> {new}')
    if args.tone is not None:
        old = save.skin.tone
        new = save.skin.tone = args.tone
        log.info(f'Skin tone changed: {old!r} -> {new!r}')
    if args.desc is not None:
        old = save.skin.desc
        new = save.skin.desc = args.desc
        log.info(f'Skin description changed: {old!r} -> {new!r}')
    if args.adj is not None:
        old = save.skin.adj
        new = save.skin.adj = args.adj
        log.info(f'Skin adjective changed: {old!r} -> {new!r}')
    if args.fur is not None:
        old = save.skin.fur
        new = save.skin.fur = args.fur
        log.info(f'Skin fur changed: {old!r} -> {new!r}')

    save.skin.display()

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def _register_parsers__body_skin_show(p_body_skin_subp):
    p_body_skin_show = p_body_skin_subp.add_parser('show', help='Show properties of your skin.')
    p_body_skin_show.set_defaults(cmd=cmd_body_skin_show)

def cmd_body_skin_show(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    save.skin.display()

    if batch_save is None:
        saveToSlot(args.host, args.id, save)
