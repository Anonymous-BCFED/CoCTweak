import sys
from typing import Any

from coctweak.cmd._header import header
from coctweak.logsys import getLogger
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.saves._save import SaveCaps
from coctweak.utils import cmd_deprecated, display_length, ERoundingType, parsePropertyAdjustment  # NOQA

log = getLogger(__name__)

def register_parsers__body_face(p_body_subp):
    p_body_face = p_body_subp.add_parser('face', help='Operations to perform on your face.')

    p_body_face_subp = p_body_face.add_subparsers()

    _register_parsers__body_face_set(p_body_face_subp)
    _register_parsers__body_face_show(p_body_face_subp)

def _register_parsers__body_face_set(p_body_face_subp):
    p_body_face_set = p_body_face_subp.add_parser('set', help='Change properties of your face.')
    p_body_face_set.add_argument('--type', '-t', type=str, default=None, help="Face type to use. See your mod's Face.as constants for possible values. (HUMAN, HORSE, etc)")
    p_body_face_set.set_defaults(cmd=cmd_body_face_set)

def cmd_body_face_set(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    if save.face is None:
        log.error(f'Saves from {save.NAME} do not support faces.')
        return

    old: Any
    new: Any
    if args.type is not None:
        old = save.face.type
        if args.type.isnumeric():
            new = save.face.type = save.face.ENUM_TYPES(int(args.type))
        else:
            new = save.face.type = save.face.ENUM_TYPES[args.type]
        log.info(f'Face type changed: {old} -> {new}')

    save.face.display()

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def _register_parsers__body_face_show(p_body_face_subp):
    p_body_face_show = p_body_face_subp.add_parser('show', help='Show properties of your face.')
    p_body_face_show.set_defaults(cmd=cmd_body_face_show)

def cmd_body_face_show(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    save.face.display()

    if batch_save is None:
        saveToSlot(args.host, args.id, save)
