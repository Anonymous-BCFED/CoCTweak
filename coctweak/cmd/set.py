import sys, argparse
from ._header import header
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.utils import add_hostid_args

from coctweak.logsys import getLogger
log = getLogger(__name__)

def register_parsers__set(subp: argparse.ArgumentParser, batch: bool=False):

    p_set = subp.add_parser('set', help='Set a particular element of a save')
    add_hostid_args(p_set, batch)
    p_set_subp = p_set.add_subparsers()

    register_parsers__set_flag(p_set_subp)
    register_parsers__set_gems(p_set_subp)
    register_parsers__set_notes(p_set_subp)
    register_parsers__set_stat(p_set_subp)

def register_parsers__set_notes(p_set_subp):
    p_set_notes = p_set_subp.add_parser('notes', help='Set the notes for a save.')
    p_set_notes.add_argument('newnotes', type=str, help="New notes for the save.")
    p_set_notes.set_defaults(cmd=cmd_set_notes)
def cmd_set_notes(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    if not args.quiet:
        log.info(f'Notes: {save.notes!r} -> {args.newnotes!r}')
    save.notes = args.newnotes

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def register_parsers__set_stat(p_set_subp):
    p_set_stat = p_set_subp.add_parser('stat', help='Set the value for a core stat.')
    p_set_stat.add_argument('statID', type=str, help="Stat ID")
    p_set_stat.add_argument('new_value', type=float, help="New value.")
    p_set_stat.set_defaults(cmd=cmd_set_stat)
def cmd_set_stat(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    if not save.stats.has(args.statID):
        log.critical('Stat %r does not exist in %s saves.', args.statID, save.NAME)
        sys.exit(1)

    if not args.quiet:
        stat = save.stats.get(args.statID)
        log.info(f'Stat {stat.name} ({stat.id}): {stat.value!r} -> {args.new_value!r}')

    save.stats.get(args.statID).value = args.new_value

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def register_parsers__set_gems(p_set_subp):
    p_set_gems = p_set_subp.add_parser('gems', help='Set how many gems (currency) you have in your bag.')
    p_set_gems.add_argument('new_value', type=str, help="New value. +x = add x gems, -x = remove x gems")
    p_set_gems.set_defaults(cmd=cmd_set_gems)
def cmd_set_gems(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    adj = 0
    gems = ogems = save.gems
    op = ''
    if args.new_value.startswith('+'):
        op = '+'
    elif args.new_value.startswith('-'):
        op = '-'
    elif args.new_value.startswith('='):
        op = '='

    if op != '':
        args.new_value = args.new_value[1:]
    adj = int(args.new_value)

    if   op in ('', '='):
        gems = adj
        op = '='
    elif op == '+':
        gems += adj
    elif op == '-':
        gems -= adj

    if not args.quiet:
        log.info(f'Gems : {ogems!r} {op}> {gems!r}')

    save.gems = gems

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def register_parsers__set_flag(p_set_subp):
    p_set_flag = p_set_subp.add_parser('flag', help='Set the value for a flag. DO NOT TOUCH THIS UNLESS YOU KNOW WHAT YOU\'RE DOING!')
    p_set_flag.add_argument('flag_id', type=str, help="Flag ID as an integer or ENUM_NAME.")
    grpType = p_set_flag.add_mutually_exclusive_group(required=True)
    grpType.add_argument('--int', type=int, default=None, help="New value, as an integer.")
    grpType.add_argument('--float', type=float, default=None, help="New value, as a decimal.")
    grpType.add_argument('--str', type=str, default=None, help="New value, as a string.")
    p_set_flag.set_defaults(cmd=cmd_set_flag)

def cmd_set_flag(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    flag = 0
    if args.flag_id.isnumeric():
        flag = save.ENUM_FLAGS(int(args.flag_id))
    else:
        flag = save.ENUM_FLAGS[args.flag_id]
    if flag not in save.ENUM_FLAGS.__members__.values():
        log.critical(f'Specified flag {args.flag_id!r} is not a valid flag.')
        return
    val = args.int or args.float or args.str

    if not args.quiet:
        oldVal = save.flags.get(flag)
        log.info(f'{flag.name} ({flag.value}): {oldVal!r} -> {val!r}')

    save.flags.set(flag, val)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)
