import os
import shutil

from miniamf import AMF3, sol
from ruamel.yaml import YAML

from coctweak.logsys import LogWrapper
from coctweak.saves import LSO_DIR
from coctweak.utils import _do_cleanup, check_dir, sanitizeFilename

yaml = YAML(typ='rt')

log = LogWrapper()

def register_parsers__restore(subp):
    p_restore = subp.add_parser('restore', help='Restores all saves from backup/ directory.')
    p_restore.add_argument('-Y', '--yaml', action='store_true', default=False, help='Restore from YAML rather than the binary .sol files.')
    p_restore.add_argument('--clobber', action='store_true', default=False, help='Overwrite existing files.')
    p_restore.add_argument('--clean-slate', action='store_true', default=False, help='Destroy existing Macromedia Shared Objects Library, first. Irreversible.')
    p_restore.add_argument('--hostname', type=str, default=None, nargs='*', help='Which hostname(s) to restore. (default: all)')
    p_restore.add_argument('--namespace', type=str, default=None, nargs='*', help='Which namespace(s) to restore. (default: all)')
    p_restore.add_argument('--coc-save', type=int, default=None, nargs='*', help='Which CoC saves to restore. (default: all)')
    p_restore.set_defaults(cmd=cmd_restore)

def remove_empty_dirs(dirname: str) -> int:
    if '.git' in dirname:
        return 0
    removed = 1
    dirs = 0
    files = 0
    while(removed > 0):
        dirs = 0
        files = 0
        removed = 0
        entries = os.listdir(dirname)
        for entryname in entries:
            entry = os.path.join(dirname, entryname)
            if os.path.isdir(entry):
                removed += remove_empty_dirs(entry)
                dirs += 1
            elif os.path.isfile(entry):
                files += 1
    if dirs + files == 0:
        print(f'rmdir {dirname} (empty)')
        os.rmdir(dirname)
        return 1
    return removed

def cmd_restore(args):
    if args.clean_slate:
        log.warning('--clean-slate specified!')
        log.warning(f'Calling shutil.rmtree({sanitizeFilename(LSO_DIR)!r})')
        shutil.rmtree(LSO_DIR)
    target_ext = '.yml' if args.yaml else '.sol'
    sources = []
    allowed_filenames = [f'CoC_{x}' for x in args.coc_save] if args.coc_save else None

    for root, _, files in os.walk('backup'):
        for filename in files:
            abspath = os.path.join(root, filename)
            relpath = os.path.relpath(abspath, 'backup')

            pathchunks = relpath.split(os.path.sep)
            if pathchunks[0].startswith('.'):
                #print(f'Skipping {relpath}')
                continue
            if not pathchunks[-1].endswith(target_ext):
                log.info(f'Skipping {relpath}')
                continue
            #print(repr(pathchunks))
            host = pathchunks[0]
            ns = [] if len(pathchunks) <= 3 else pathchunks[1:-2]
            objid, _ = os.path.splitext(pathchunks[-2])
            spec = (host, ns, objid)
            #print(repr(spec))
            sources += [spec]
    #return
    for host, ns, objid in sources:
        nspath = os.path.join(*ns) if len(ns) > 0 else ''
        srcfile = os.path.join('backup', host, nspath, objid, objid+target_ext)
        destfile = os.path.join(LSO_DIR, host, nspath, objid+'.sol')
        if args.hostname is not None and host not in args.hostname:
            log.info('Skipping %s (hostname=%s)', srcfile, host)
            continue
        if args.namespace is not None and nspath not in args.namespace:
            log.info('Skipping %s (namespace=%s)', srcfile, nspath)
            continue
        if args.coc_save is not None and objid not in allowed_filenames:
            log.info('Skipping %s (objid=%s)', srcfile, objid)
            continue
        check_dir(os.path.dirname(destfile))

        if os.path.isfile(destfile):
            if not args.clobber:
                log.warning(f'File {sanitizeFilename(destfile)} exists already; Skipping. Use --clobber to overwrite anyway.')
                continue
            else:
                log.warning(f'Overwriting {sanitizeFilename(destfile)}.')

        with log.info(f'Saving {sanitizeFilename(destfile)}...'):
            if args.yaml:
                data = sol.SOL(objid)
                with open(srcfile, 'r') as f:
                    data.update(_do_cleanup(yaml.load(f)))
                sol.save(data, destfile, AMF3)
            else:
                shutil.copy2(srcfile, destfile)
