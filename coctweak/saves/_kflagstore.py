from typing import Any, Optional, Union, Generic, TypeVar
from ._saveobj import SaveObject
from enum import IntEnum

from coctweak.logsys import getLogger
log = getLogger(__name__)
T = TypeVar('T', bound=IntEnum)
class KFlagStore(Generic[T], SaveObject):
    def __init__(self, kflagtype: T):
        self.kflagtype: T = kflagtype
        self.flags: dict = {}

    def getValueOf(self, key: Union[T, int]) -> int:
        if isinstance(key, self.kflagtype):
            key = key.value
        return key

    def getNameOf(self, key: Union[T, int]) -> str:
        if isinstance(key, self.kflagtype):
            return key.name
        else:
            return self.kflagtype(key).name

    def get(self, key: Union[T, int], default: Any = None) -> Any:
        key = self.getValueOf(key)
        return self.flags.get(key, default)

    def set(self, key: Union[T, int], value: Any) -> None:
        key = self.getValueOf(key)
        self.flags[key] = value

    def addToValue(self, key: Union[T, int], value: Any) -> None:
        key = self.getValueOf(key)
        self.flags[key] += value
    def multiplyWithValue(self, key: Union[T, int], value: Any) -> None:
        key = self.getValueOf(key)
        self.flags[key] *= value
    def divideWithValue(self, key: Union[T, int], value: Any) -> None:
        key = self.getValueOf(key)
        self.flags[key] /= value

    def remove(self, key: Union[T, int]) -> None:
        key = self.getValueOf(key)
        del self.flags[key]

    def has(self, key: Union[T, int]) -> bool:
        key = self.getValueOf(key)
        return key in self.flags.keys()

    def serialize(self, prefix: str='flags') -> dict:
        o = {
            prefix: {},
            prefix+'-decoded-NOT-LOADED': {}
        }
        for k,v in self.flags.items():
            if v is not None and v != 0:
                o[prefix][str(k)]=v
                kname = self.kflagtype(int(k)).name
                o[prefix+'-decoded-NOT-LOADED'][f'{kname} ({k})']=v
        return o

    def serializeTo(self, data: dict, prefix: str = 'flags') -> dict:
        #print(repr(data['flags']))
        data[prefix]={}
        data[prefix+'-decoded-NOT-LOADED']={}
        for k,v in self.flags.items():
            if v is not None and v != 0:
                data[prefix][str(k)]=v
                kname = self.kflagtype(int(k)).name
                #print(f'{kname} ({k}) = {v!r}')
                data[prefix+'-decoded-NOT-LOADED'][f'{kname} ({k})']=v
        #print(repr(data['flags']))
        return data

    def deserialize(self, data: dict, prefix: str = 'flags') -> None:
        for k,v in data[prefix].items():
            self.flags[int(k)] = v
