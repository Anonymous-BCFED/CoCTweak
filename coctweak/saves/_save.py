import enum
from enum import auto, IntFlag
import json
from pathlib import Path
from typing import Any, Dict, List, Optional, Type

import miniamf
import msgpack

from coctweak.paths import TEMP_DIR
from coctweak.saves._ailment import Ailment
from coctweak.saves._appearance import IAppearance
from coctweak.saves._bonus_derived_stats import BDSController
from coctweak.saves._coctweak_data import CoCTweakData
from coctweak.saves._combatstats import BaseCombatStats
from coctweak.saves._keyitem import KeyItem
from coctweak.saves._kflagstore import KFlagStore
from coctweak.saves._npc import BaseNPC
from coctweak.saves._perk import Perk
from coctweak.saves._saveobj import SaveObject
from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._storage import GearStorage, Storage
from coctweak.saves._wearableslot import WearableSlot
from coctweak.saves.common.bodyparts import *
from coctweak.saves.common.bodyparts.bodypart import BodyPart
from coctweak.saves.common.item import BaseItem
from coctweak.saves.common.perks import *
from coctweak.saves.common.statuseffects import *
from coctweak.saves.pregnancystore import VarPregnancyStore
from coctweak.saves.vanilla.corestats import VanillaCoreStats
from coctweak.saves.vanilla.enums.kflags import VanillaKFlags
from coctweak.saves.vanilla.statuseffects import *
from coctweak.utils import sanitizeFilename, sha256sum

from coctweak.logsys import getLogger  # isort: skip
log = getLogger(__name__)


class SaveCaps(IntFlag):
    NONE = 0

    HUNGER = auto()
    DISEASES = auto()
    MORE_THAN_FOUR_BALLS = auto()
    SELF_SAVING_OBJECTS = auto()


class BaseSave(SaveObject):
    CORE_STATS_TYPE = VanillaCoreStats
    COMBAT_STATS_TYPE = BaseCombatStats
    GLOBAL_DATA_TYPE = None
    GLOBAL_DATA_FILENAME = None
    NAME = '__BASE__'
    PERK_CLASSES = {
        'Bee Ovipositor': BeeOvipositorPerk,
        'Smart': SmartPerk,
    }
    STATUS_EFFECT_CLASSES = {
        'ButtStretched': VanillaButtStretchedEffect,
        'Marble': CommonMarbleEffect,
    }
    KEYITEM_CLASSES = {
    }

    CAPABILITIES = SaveCaps.NONE

    ITEM_TYPE = BaseItem
    KEYITEM_TYPE = KeyItem
    STATUSEFFECT_TYPE = StatusEffect
    WEARABLESLOT_TYPE = WearableSlot
    QUAD_PERK_TYPE = Perk

    ANTENNAE_TYPE = None
    ARMS_TYPE = None
    ASS_TYPE = BaseAss
    BALLS_TYPE = BaseBalls  # virtual
    BREASTROW_TYPE = BaseBreastRow
    COCK_TYPE = BaseCock
    EARS_TYPE = BaseEars
    FACE_TYPE = BaseFace
    HORNS_TYPE = BaseHorns
    LOWER_BODY_TYPE = BaseLowerBody
    SKIN_TYPE = BaseSkin
    TAIL_TYPE = BaseTail
    VAGINA_TYPE = BaseVagina
    WINGS_TYPE = BaseWings

    APPEARANCE_TYPE: Type[IAppearance] = None

    BODYPART_CATS_WITH_MULTIPLES = [
        'Asses',
        'Cocks',
        'Vaginas',
    ]

    VAGINAL_PREGNANCY_TYPE: Type[VarPregnancyStore] = VarPregnancyStore
    ANAL_PREGNANCY_TYPE: Type[VarPregnancyStore] = VarPregnancyStore

    ENUM_FLAGS = VanillaKFlags

    MAX_INVENTORY_SLOTS = 10
    SLOT_QUANTITY_HARD_CAP = 10

    ALL_ITEMS: dict = None
    ITEMTYPES_FILE = None

    ALL_PERKS: dict = None
    PERKTYPES_FILE = None

    ALL_KEYITEMS: dict = None
    KEYITEMS_FILE = None

    ALL_STATUSEFFECTS: dict = None
    STATUSEFFECTTYPES_FILE = None
    STATUSEFFECTS_DIR = None

    SELF_SAVING_OBJECT_TYPES: Dict[str, SelfSavingObject] = None

    BDS_CONTROLLER = BDSController

    BAD_STATUSES = []

    DISEASES: List[Ailment] = []
    AVAILABLE_NPCS: List[Type[BaseNPC]] = []

    COCTWEAKDATATYPE = CoCTweakData

    INVENTORY_STORE = Storage
    GEAR_STORE = GearStorage

    def __init__(self):
        super().__init__()

        self._editorData: Optional[CoCTweakData] = None

        self.filename = ''
        self.host = ''
        self.id = 0

        self.version = ''

        self.saveName = ''

        self.sexOrientation = 0

        self.gems: int = 0

        self.days = 0
        self.hours = 0
        self.minutes = 0

        self.stats = self.CORE_STATS_TYPE(self)
        self.combat_stats = self.COMBAT_STATS_TYPE(self)

        self.stat_points: int = 0
        self.perk_points: int = 0

        self.notes = ''

        self.shortName = ''
        self.startingRace = 'human'

        self.flags: KFlagStore = KFlagStore(self.ENUM_FLAGS)

        self.perks = {}
        self.keyItems = {}
        self.statusEffects = {}

        self.antennae = self.ANTENNAE_TYPE(
            self) if self.ANTENNAE_TYPE else None
        self.arms = self.ARMS_TYPE(self) if self.ARMS_TYPE else None
        self.ass = self.ASS_TYPE(self)
        self.balls = self.BALLS_TYPE(self)
        self.breast_rows = [self.BREASTROW_TYPE(self)]
        self.cocks = []
        self.ears = self.EARS_TYPE(self)
        self.face = self.FACE_TYPE(self)
        self.horns = self.HORNS_TYPE(self)
        self.lowerBody = self.LOWER_BODY_TYPE(self)
        self.skin = self.SKIN_TYPE(self)
        self.tail = self.TAIL_TYPE(self)
        self.vaginas = []
        self.wings = self.WINGS_TYPE(self)

        self.global_data = None

        self.vaginalPregnancy: BasePregnancyStore = None
        self.analPregnancy: BasePregnancyStore = None
        self.initializePregnancyStores()

        # https://gitgud.io/BelshazzarII/CoCAnon_mod/blob/master/classes/classes/Saves.as: //CLOTHING/ARMOR
        self.armor: WearableSlot = self.WEARABLESLOT_TYPE(
            self, 'armor', 'Armor', 'nothing')
        self.weapon: WearableSlot = self.WEARABLESLOT_TYPE(
            self, 'weapon', 'Weapon', "Fists  ")
        # self.jewelry: WearableSlot = None
        self.shield: WearableSlot = self.WEARABLESLOT_TYPE(
            self, 'shield', 'Shield', "noshild")
        self.upperGarment: WearableSlot = self.WEARABLESLOT_TYPE(
            self, 'upperGarment', 'Upper Undergarment', "nounder")
        self.lowerGarment: WearableSlot = self.WEARABLESLOT_TYPE(
            self, 'lowerGarment', 'Lower Undergarment', "nounder")

        self.wearableslots = [self.armor, self.weapon,
                              self.shield, self.upperGarment, self.lowerGarment]

        self.bonuses = self.BDS_CONTROLLER(self)

        # classes/classes/Scenes/Inventory.as, armorRackDescription, etc
        # THIS IS A TERRIBLE SYSTEM.
        self.gearStorage: List[Storage] = [
            #                     title,         start, end, category
            self.GEAR_STORE(self, 'Weapon Rack', 0,     9,   'Weapons',       keys=[
                            'weaponrack', 'weapon rack', 'weapons']),
            self.GEAR_STORE(self, 'Armor Rack',  9,     18,  'Armors',        keys=[
                            'armorrack', 'armor rack', 'armor', 'armors']),
            self.GEAR_STORE(self, 'Jewelry Box', 18,    27,  'Jewelry',       keys=[
                            'jewelrybox', 'jewelrybox', 'jewelry']),
            self.GEAR_STORE(self, 'Dresser',     27,    36,  'Undergarments', keys=[
                            'dresser', 'clothes', 'clothing']),
            self.GEAR_STORE(self, 'Shield Rack', 36,    45,  'Shields',       keys=[
                            'shieldrack', 'shield rack', 'shield', 'shields']),
        ]

        self.chestStorage: Storage = self.INVENTORY_STORE(
            self, 'Chests', None, keys=['itemstorage', 'chest', 'chests'])
        self.chestStorage.allocate(self.MAX_INVENTORY_SLOTS)
        self.playerStorage: Storage = self.INVENTORY_STORE(
            self, 'Player', None, keys=['player', 'pc'])
        self.playerStorage.allocate(self.MAX_INVENTORY_SLOTS)

        self.campStorage: List[Storage] = self.gearStorage+[self.chestStorage]
        self.allStorage: List[Storage] = [self.playerStorage]+self.campStorage

        self.storageByName: Dict[str, Storage] = {
            s: s for s in self.allStorage}

        self.selfSavingObjects: Dict[str, SelfSavingObject] = {}
        self.NPCs: Dict[str, BaseNPC] = {}

        self.appearance: IAppearance = self.APPEARANCE_TYPE(self)

    def initializePregnancyStores(self) -> None:
        self.vaginalPregnancy = self.VAGINAL_PREGNANCY_TYPE(
            self, 'pregnancyType', 'pregnancyIncubation')
        self.analPregnancy = self.ANAL_PREGNANCY_TYPE(
            self, 'buttPregnancyType', 'buttPregnancyIncubation')

    def getAllBodyParts(self) -> List[BodyPart]:
        o: List[BodyPart] = [
            self.ass
        ]
        o += self.breast_rows
        o += self.cocks
        if self.antennae:
            o += [self.antennae]
        o += [self.arms]
        o += [self.balls]
        o += [self.ears]
        o += [self.face]
        o += [self.horns]
        o += [self.lowerBody]
        o += [self.skin]
        o += [self.tail]
        o += self.vaginas
        o += [self.wings]
        return o

    def getCoCTweakConfig(self) -> CoCTweakData:
        '''
        CoCTweak adds a hidden status effect that stores CoCTweak-related data for this save.

        As a statuseffect, the game will load it without asking too many questions.

        This method returns the parsed contents of CoCTweakData, or a creates a new one.
        '''
        if self._editorData:
            return self._editorData
        else:
            self._editorData = self.COCTWEAKDATATYPE()
            if self.flags.has(CoCTweakData.FLAG_ID):
                self._editorData.deserialize(CoCTweakData)
        return self._editorData

    @property
    def capabilities(self):
        return self.CAPABILITIES

    def hasCapability(self, cap):
        return (self.CAPABILITIES & cap) == cap

    def getFlag(self, key, default):
        log.warning("getFlag(): Deprecated")
        return self.flags.get(key, default)

    def getStatusEffect(self, seID) -> StatusEffect:
        if isinstance(seID, enum.Enum):
            seID = seID.value
        return self.statusEffects.get(seID, None)

    def addStatusEffect(self, seID, skip_meta: bool = False) -> StatusEffect:
        if isinstance(seID, enum.Enum):
            seID = seID.value
        se: StatusEffect = self.STATUS_EFFECT_CLASSES.get(
            seID, self.STATUSEFFECT_TYPE)()
        if se.id == '':
            se.id = seID
        self.statusEffects[se.id] = se
        if not skip_meta:
            se.load_from_meta(self.getStatusEffectMeta(se.id))
        return se

    def stash(self, item: BaseItem) -> GearStorage:
        for store in self.campStorage:
            if store.accepts(item):
                for slot in store.slots:
                    if slot.is_empty():
                        slot.cloneMutables(item)
                        return store
        return None

    def loadSlots(self, rawslots: List[BaseItem]) -> None:
        [self.loadSlot(x) for x in rawslots]

    def loadSlot(self, slot: BaseItem) -> None:
        slot.from_items_json(self.getItemMeta(slot.id))

    def cloneSlots(self, rawslots: List[BaseItem]) -> List[BaseItem]:
        return [self.cloneSlot(x) for x in rawslots]

    def cloneSlot(self, rawslot: BaseItem) -> BaseItem:
        o = self.ITEM_TYPE()
        o.deserialize(rawslot.serialize())
        o.from_items_json(self.getItemMeta(o.id))
        return o

    def getBodyPartsByCategory(self) -> Dict[str, List[BodyPart]]:
        o: Dict[str, List[BodyPart]] = {}
        for bp in self.getAllBodyParts():
            if bp is None:
                continue
            o[bp.CATEGORY] = o.get(bp.CATEGORY, []) + [bp]
        return o

    def display(self, **kwargs):
        filename = sanitizeFilename(self.filename)

        log.info(f'{self.host}@{self.id} - {filename}')
        log.info(f'Mod: {self.getModName()} v{self.getModVersion()}')
        log.info(f'CoC Save Version: {self.getGameSaveVersion()}')
        log.info(f'Mod Save Version: {self.getModSaveVersion()}')
        log.info(f'{self.days} days, {self.hours} hours, {self.minutes} minutes')
        self.displayPlayerLine()
        self.displayOrientation()
        self.displayBaseStats()
        self.displayCombatStats()
        for cat, bps in self.getBodyPartsByCategory().items():
            with log.info(cat):
                if cat not in self.BODYPART_CATS_WITH_MULTIPLES:
                    bps[0].display()
                else:
                    for i, bp in enumerate(bps):
                        with log.info('%d:', i):
                            bp.display()
        # self.displayAss()
        # self.displayBreasts()
        # self.displayCocks()
        # self.displayBalls()
        # self.displayVaginas()
        self.displayPregnancies()
        self.displayPerks()
        self.displayStatusEffects()
        self.displayKeyItems()
        self.displayStorage()
        # self.displayChest('Inventory', self.inventory, hide_locked=True)
        # self.displayChest('Item Storage', self.itemStorage)
        # self.displayChest('Gear Storage', self.gearStorage)
        # for title, contents in self.storageByName.items():
        #    self.displayChest(title, contents)
        # if self.global_data is not None:
        #    self.global_data.displayAchievements()
        with log.info('EditorData flag:'):
            flagID = self.COCTWEAKDATATYPE.FLAG_ID
            log.info('Flag ID: %d', flagID)
            if self.flags.has(flagID):
                edd = self.getCoCTweakConfig()
                edd.show()
                with log.info('Raw contents:'):
                    log.info(repr(self.flags.get(flagID, '{}')))
            else:
                log.info('Not present in save.')

    def displayPregnancies(self):
        if self.analPregnancy.is_pregnant or self.vaginalPregnancy.is_pregnant:
            with log.info('Pregnancies:'):
                if self.analPregnancy.is_pregnant:
                    with log.info('Anal:'):
                        self.analPregnancy.display()
                if self.vaginalPregnancy.is_pregnant:
                    with log.info('Uterine:'):
                        self.vaginalPregnancy.display()

    def displayBreasts(self):
        with log.info('Breasts:'):
            i = 0
            for breastrow in self.breast_rows:
                with log.info(f'Row {i+1}:'):
                    breastrow.display()

    def displayOrientation(self):
        mf = 'Males' if self.sexOrientation > 50 else 'Females'
        log.info(f'Sexual orientation: Likes {mf} ({self.sexOrientation:g})')

    def displayPerks(self):
        with log.info('Perks:'):
            maxlen = 0
            for perk in self.perks.values():
                maxlen = max(maxlen, len(perk.id))
            for perk in self.perks.values():
                log.info(f'* {perk.id:{maxlen}} = {perk.displayValues()}')

    def displayStorage(self):
        with log.info('Storage:'):
            for s in self.allStorage:
                s.display()

    def availableInventories(self) -> List[str]:
        return [s.keys for s in self.allStorage]

    def displayStatusEffects(self):
        with log.info('Status Effects:'):
            plist = []
            maxlen = 0
            for se in self.statusEffects.values():
                maxlen = max(maxlen, len(se.id))
            for _, se in sorted(self.statusEffects.items(), key=lambda x: x[0]):
                log.info(f'* {se.id:{maxlen}} = {se.displayValues()}')

    def displayKeyItems(self):
        with log.info('Key Items:'):
            plist = []
            maxlen = 0
            for se in self.keyItems.values():
                maxlen = max(maxlen, len(se.id))
            for _, se in sorted(self.keyItems.items(), key=lambda x: x[0]):
                log.info(f'* {se.id:{maxlen}} = {se.displayValues()}')

    def displayPlayerLine(self):
        log.info(
            f'Player: {self.shortName}, Level {self.combat_stats.level.value:g} {self.getAgeStr()} {self.getLongGender()}')

    def fmtDictStats(self, stats: dict):
        return ', '.join([f'{k}: {self.statVal2Str(v)}' for k, v in stats.items()])

    def statVal2Str(self, value):
        if isinstance(value, (float, int)):
            return f'{value:g}'
        elif isinstance(value, str):
            return value
        elif isinstance(value, tuple):
            return f'{self.statVal2Str(value[0])} / {self.statVal2Str(value[1])}'
        else:
            return str(value)

    def displayBaseStats(self):
        log.info(self.fmtDictStats(self.getBaseStatStrs()))

    def getBaseStatStrs(self):
        return {k.upper(): v.value for k, v in self.stats.allByID.items()}

    def getCombatStatStrs(self):
        exclude = ['level']
        return {v.id: v.value for v in self.combat_stats.all if v.id not in exclude}

    def displayCombatStats(self):
        log.info(self.fmtDictStats(self.getCombatStatStrs()))

    def displayCocks(self):
        if len(self.cocks) > 0:
            with log.info('Cocks:'):
                for i, cock in enumerate(self.cocks):
                    with log.info('%d:', i):
                        cock.display()

    def displayVaginas(self):
        if len(self.vaginas) > 0:
            with log.info('Vaginas:'):
                for v in self.vaginas:
                    v.display()

    def displayBalls(self):
        self.balls.display()

    def displayAss(self):
        with log.info('Ass:'):
            self.ass.display()

    def _postItemLoad(self) -> None:
        return

    def _calcCompiledDataPath(key: str, hash: str) -> Path:
        return TEMP_DIR / '{self.NAME}{key}{hash}.pkl'

    def loadItemMetadata(self) -> None:
        if self.ALL_ITEMS is None:
            if self.ITEMTYPES_FILE is not None:
                filepath = Path(self.ITEMTYPES_FILE)
                hash = sha256sum(filepath).hex()
                stem = filepath.stem.split('.')[0]
                cached = filepath.with_name(f'{stem}-cached-{hash}.pak')
                for deadcache in (set((x.absolute() for x in filepath.parent.glob('*.pak')))-set([cached.absolute()])):
                    log.info(f'cache: rm {deadcache} (Dead)')
                    deadcache.unlink()
                if cached.is_file():
                    with cached.open('rb') as f:
                        self.ALL_ITEMS = msgpack.load(f)
                else:
                    with open(self.ITEMTYPES_FILE, 'r') as f:
                        self.ALL_ITEMS = json.load(f)
                    with cached.open('wb') as f:
                        msgpack.dump(self.ALL_ITEMS, f)
                self._postItemLoad()

    def getItemMeta(self, _id) -> dict:
        if self.ITEMTYPES_FILE is None:
            return {}  # Mod does not support metadata
        else:
            self.loadItemMetadata()
        return self.ALL_ITEMS.get(_id)

    def postPerkLoad(self) -> None:
        return

    def getPerkMeta(self, _id) -> dict:
        if self.ALL_PERKS is None:
            if self.PERKTYPES_FILE is not None:
                with open(self.PERKTYPES_FILE, 'r') as f:
                    self.ALL_PERKS = json.load(f)
                self.postPerkLoad()
                # print(f'Loaded {len(self.ALL_PERKS)} perks.')
            else:
                return {}  # Mod does not support metadata
        return self.ALL_PERKS.get(_id, {})

    def postKeyItemLoad(self) -> None:
        return

    def getKeyItemMeta(self, _id) -> dict:
        if self.ALL_KEYITEMS is None:
            if self.KEYITEMS_FILE is not None:
                with open(self.KEYITEMS_FILE, 'r') as f:
                    self.ALL_KEYITEMS = json.load(f)
                self.postKeyItemLoad()
                # print(f'Loaded {len(self.ALL_PERKS)} perks.')
            else:
                return {}  # Mod does not support metadata
        return self.ALL_KEYITEMS.get(_id, {})

    def postStatusEffectLoad(self) -> None:
        return

    def getStatusEffectMeta(self, _id) -> dict:
        if self.ALL_STATUSEFFECTS is None:
            if self.STATUSEFFECTTYPES_FILE is not None:
                with open(self.STATUSEFFECTTYPES_FILE, 'r') as f:
                    self.ALL_STATUSEFFECTS = json.load(f)
                self.postStatusEffectLoad()
            else:
                return {}  # Mod does not support metadata
        return self.ALL_STATUSEFFECTS.get(_id, {})

    def createKeyItem(self, _id, values: List[int] = [0, 0, 0, 0]) -> None:
        if not self.hasKeyItem(_id):
            ki = self.keyItems[_id] = KeyItem()
            ki.id = _id
            ki.values = values

    def getOrCreateKeyItem(self, _id, values: List[int] = [0, 0, 0, 0]) -> KeyItem:
        if not self.hasKeyItem(_id):
            self.createKeyItem(_id, values)
        return self.getKeyItem(_id)

    def meditate(self) -> None:
        # [HGG]classes/classes/Scenes/NPCs/JojoScene.as: public function jojoFollowerMeditate(): void {
        # outputText("The mouse monk leads you to a quiet spot away from the portal and the two of you sit down, him cross-legged and you mimicking to the best of your ability, back to back. You close your eyes and meditate for half-an hour, centering your body and mind. Afterwards, he guides you through stretches and exercises to help keep your bodies fit and healthy.");
        # outputText("[pg]When you are done, Jojo nods to you, and climbs back onto his rock, still thinking.");
        # //Reduces lust
        # dynStats("lus", -30);
        self.combat_stats.lust.value -= 30
        # var cleanse:int = -2; //Corruption reduction - faster at high corruption
        cleanse: int = 2
        # if (player.cor > 80) cleanse -= 3;
        if self.stats.corruption.value > 80:
            cleanse += 3
        # else if (player.cor > 60) cleanse -= 2;
        if self.stats.corruption.value > 60:
            cleanse += 2
        # else if (player.cor > 40) cleanse -= 1;
        if self.stats.corruption.value > 40:
            cleanse += 2
        # dynStats("cor", cleanse - player.countCockSocks("alabaster"));
        self.stats.corruption.value -= cleanse + \
            self.countCockSocks('alabaster')
        # Let's leave these alone, for now.
        # if (player.str100 < 45) dynStats("str", 1); //Str boost to 45
        if self.stats.strength.percent < 45.:
            self.stats.strength.value += 1
        # if (player.tou100 < 45) dynStats("tou", 1); //Tou boost to 45
        if self.stats.toughness.percent < 45.:
            self.stats.toughness.value += 1
        # if (player.spe100 < 75) dynStats("spe", 1); //Speed boost to 75
        if self.stats.speed.percent < 75.:
            self.stats.speed.value += 1
        # if (player.inte100 < 80) dynStats("int", 1); //Int boost to 80
        if self.stats.intellect.percent < 80.:
            self.stats.intellect.value += 1
        # if (player.lib100 > 0) dynStats("lib", -1); //Libido lower to 15
        if self.stats.libido.percent > 0.:
            self.stats.libido.value -= 1
        # flags[kFLAGS.JOJO_LAST_MEDITATION] = game.time.days;
        # player.addStatusValue(StatusEffects.JojoMeditationCount, 1, 1);

    def heal(self, hp=True, fatigue=False, lust=False, hunger=False, diagnose_only=False) -> dict:
        stats = []
        if hp:
            stats += ['HP']
        if fatigue:
            stats += ['fatigue']
        if lust:
            stats += ['lust']
        if hunger:
            stats += ['hunger']
        return self.combat_stats.heal(stats, diagnose_only=diagnose_only)

    def get_diseases(self) -> List[Ailment]:
        # if not self.hasCapability(SaveCaps.DISEASES):
        #    log.warning(f'This save\'s mod ({self.NAME}) does not support diseases.')
        #    return None
        return self.doGetDiseases()

    def heal_diseases(self, stats: bool = False) -> List[str]:
        # if not self.hasCapability(SaveCaps.DISEASES):
        #    log.warning(f'Unable to remove diseases, as this save\'s mod ({self.NAME}) does not support them.')
        #    return None
        return self.doHealDiseases(stats)

    def doGetDiseases(self) -> List[Ailment]:
        diseases = [x() for x in self.DISEASES]
        results = []
        for p in diseases:
            if p.check(self):
                results += [p]
        return results

    def doHealDiseases(self, stats_too: bool = False) -> None:
        [x.removeFromPlayer(self, change_stats=stats_too)
         for x in self.doGetDiseases()]

    def heal_statuses(self) -> None:
        for statID in self.BAD_STATUSES:
            if statID in self.statusEffects.keys():
                with log.info('Removing status effect %r...', statID):
                    self.statusEffects[statID].preRemove()
                    del self.statusEffects[statID]

    def get_bad_statuses(self) -> List[StatusEffect]:
        o = []
        for statID in self.BAD_STATUSES:
            if statID in self.statusEffects.keys():
                o.append(statID)
        return o

    def getModName(self):
        return self.NAME

    def getModVersion(self):
        return self.version

    def getModSaveVersion(self):
        return self.flags.get(2066, -1)

    def getGameSaveVersion(self):
        # SAVE_FILE_INTEGER_FORMAT_VERSION
        return self.flags.get(1238, -1)

    def getGender(self):
        if len(self.raw['cocks']) > 0 and len(self.raw['vaginas']) > 0:
            return "H"
        elif len(self.raw['cocks']) > 0:
            return "M"
        elif len(self.raw['vaginas']) > 0:
            return "F"
        else:
            return "U"

    def getLongGender(self):
        g = self.getGender()
        if g == 'M':
            return 'Male'
        elif g == 'F':
            return 'Female'
        elif g == 'H':
            return 'Herm'
        else:
            return 'Unknown'

    def getDifficulty(self):
        return 'UNKNOWN'

    def hasPerk(self, perkID) -> bool:
        if isinstance(perkID, enum.Enum):
            perkID = perkID.value
        return perkID in self.perks

    def getPerk(self, perkID) -> Perk:
        if isinstance(perkID, enum.Enum):
            perkID = perkID.value
        if not self.hasPerk(perkID):
            return None
        p = self.perks[perkID]
        if not p._metaLoaded:
            p.load_from_meta(self.getPerkMeta(perkID))
        return p

    def hasStatusEffect(self, statusID) -> bool:
        if isinstance(statusID, enum.Enum):
            statusID = statusID.value
        return statusID in self.statusEffects

    def hasKeyItem(self, kiID) -> bool:
        if isinstance(kiID, enum.Enum):
            kiID = kiID.value
        return kiID in self.keyItems

    def getKeyItem(self, kiID) -> KeyItem:
        if isinstance(kiID, enum.Enum):
            kiID = kiID.value
        if kiID not in self.keyItems:
            return None
        ki = self.keyItems[kiID]
        if not ki._metaLoaded:
            ki.load_from_meta(self.getKeyItemMeta(kiID))
        return ki

    def getAgeStr(self):
        return 'ADULT (N/I)'

    def getPerkClassFor(self, pData: dict) -> Type[Any]:
        return self.QUAD_PERK_TYPE

    def deserialize(self, data: miniamf.sol.SOL, isFile: bool) -> None:
        self.raw = data

        self.saveName = data.name

        self.version = data['version']

        self.perk_points = self._getInt('perkPoints')
        self.stat_points = self._getInt('statPoints')

        self.stats = self.CORE_STATS_TYPE(self)
        self.stats.deserialize(data)

        self.flags = KFlagStore(self.ENUM_FLAGS)
        self.flags.deserialize(self.raw)

        self.deserializePerks(self.raw)

        self.statusEffects = {}
        for seData in self.raw['statusAffects']:  # sic
            se = self.addStatusEffect(seData[self.STATUSEFFECT_TYPE.ID_KEY])
            se.deserialize(seData)
            self.statusEffects[se.id] = se

        self.keyItems = {}
        for kiData in self.raw['keyItems']:
            ki = self.KEYITEM_CLASSES.get(
                kiData[self.KEYITEM_TYPE.ID_KEY], self.KEYITEM_TYPE)()
            ki.deserialize(kiData)
            self.keyItems[ki.id] = ki

        self.ass = self.ASS_TYPE(self)
        self.ass.deserialize(self.raw['ass'])
        self.ass.rating = self._getInt('buttRating', 0)

        self.cocks = []
        for cData in self.raw['cocks']:
            cock = self.COCK_TYPE(self)
            cock.deserialize(cData)
            self.cocks.append(cock)

        self.vaginas = []
        for vData in self.raw['vaginas']:
            v = self.VAGINA_TYPE(self)
            v.deserialize(vData)
            self.vaginas.append(v)

        self.breast_rows = []
        for brData in self.raw['breastRows']:
            breastrow = self.BREASTROW_TYPE(self)
            breastrow.deserialize(brData)
            self.breast_rows.append(breastrow)

        self.combat_stats = self.COMBAT_STATS_TYPE(self)
        self.combat_stats.deserialize(data)

        self.sexOrientation = self._getInt('sexOrientation')

        self.gems = self._getInt('gems')

        self.days = self._getInt('days')
        self.hours = self._getInt('hours')
        self.minutes = self._getInt('minutes')

        self.notes = self._getStr('notes', 'No Notes Available')

        self.shortName = self._getStr('short', '')

        self.loadInventory()
        # print('Loading itemStorage')
        # self.itemStorage = self.loadChest('itemStorage')
        # print('Loading gearStorage')
        # self.gearStorage = self.loadChest('gearStorage')

        self.startingRace = self._getStr('startingRace')

        self.vaginalPregnancy.deserializeFrom(data)
        self.analPregnancy.deserializeFrom(data)

        self.balls.deserializeFrom(data)
        self.ears.deserializeFrom(data)
        self.face.deserializeFrom(data)
        self.horns.deserializeFrom(data)
        self.lowerBody.deserializeFrom(data)
        self.skin.deserializeFrom(data)
        self.tail.deserializeFrom(data)
        self.wings.deserializeFrom(data)
        if self.ANTENNAE_TYPE:
            self.antennae.deserializeFrom(data)
        if self.ARMS_TYPE:
            self.arms.deserializeFrom(data)

        if self.hasCapability(SaveCaps.SELF_SAVING_OBJECTS):
            self.loadSelfSavingFrom(data)

        self.loadNPCs()

    def deserializePerks(self, data: dict) -> None:
        self.perks = {}
        for pData in self.raw['perks']:
            perk = self.getPerkClassFor(pData)()
            perk.deserialize(pData)
            perk.load_from_meta(self.getPerkMeta(perk.id))
            self.perks[perk.id] = perk

    def loadSelfSavingFrom(self, data: dict) -> None:
        self.selfSavingObjects = {}

    def saveSelfSavingObjects(self, data: dict) -> None:
        return

    def loadInventory(self):
        self.playerStorage.clear()
        for i in range(self.MAX_INVENTORY_SLOTS):
            key = f'itemSlot{i+1}'
            slot = self.ITEM_TYPE()
            if key in self.raw:
                slot.deserialize(self.raw[key])
            self.loadSlot(slot)
            self.playerStorage.slots.append(slot)

        for storage in self.gearStorage:
            storage.deserialize(self.raw['gearStorage'])
        self.chestStorage.deserialize(self.raw['itemStorage'])
        # self.playerStorage.deserialize(self.raw['items'])
        return

    def saveInventory(self, data: dict):
        for i in range(self.MAX_INVENTORY_SLOTS):
            key = f'itemSlot{i+1}'
            data[key] = self.playerStorage.slots[i].serialize()
        # data['gearStorage'] = []
        # [for s in self.gearStorage] s.serialize(data['gearStorage'])
        data['itemStorage'] = self.chestStorage.serialize()

    def loadChest(self, key):
        o = []
        if key in self.raw:
            data = self.raw[key]
            for itemdata in data:
                slot = self.ITEM_TYPE()
                # print(repr(itemdata))
                slot.deserialize(itemdata)
                o.append(slot)
        return o

    def saveChest(self, data: dict, key: str, slots: List[BaseItem]):
        data[key] = []
        for slot in slots:
            data[key].append(slot.serialize())

    def calcSlotLocks(self) -> int:
        # OVERRIDE IN MODS.
        return 3

    def setBackpackSize(self, backsize: int) -> None:
        if backsize != 0:
            if not self.hasKeyItem('Backpack'):
                self.createKeyItem('Backpack', [backsize, 0, 0, 0])
            else:
                self.getKeyItem('Backpack').values[0] = backsize
        else:
            ki = self.getKeyItem('Backpack')
            if ki is not None:
                del self.keyItems['Backpack']

    def getBackpackSize(self) -> int:
        ki = self.getKeyItem('Backpack')
        if ki is None:
            return 0
        return ki.values[0]

    def hasBackpack(self) -> bool:
        return self.hasKeyItem('Backpack')

    def getBackpackData(self) -> Any:
        return self.getKeyItem('Backpack').values

    def setBackpackData(self, values: Any) -> None:
        self.getKeyItem('Backpack').values = values

    def updateBackpackHack(self, unlocked: int, verbose: bool):
        hack = self.getCoCTweakConfig().backpack_hack
        if hack.is_hacked:
            if not hack.backed_up:
                with log.info('Backing up backpack, if it exists...'):
                    hack.backed_up = True
                    hack.had_backpack = self.hasBackpack()
                    hack.old_data = None
                    if hack.had_backpack:
                        hack.old_data = self.getBackpackData()
                        log.info('Old backpack size: %d',
                                 self.getBackpackSize())
                    else:
                        log.info('No backpack.')
            bpSz = hack.desired_size-unlocked
            self.setBackpackSize(bpSz)
            if verbose:
                log.info(
                    'Save has an override of %d slots set, made backpack of %d size.', hack.desired_size, bpSz)
        else:
            if hack.backed_up:
                hack.backed_up = False
                if self.had_backpack:
                    log.info('Restoring backpack from backup...')
                    self.setBackpackData(hack.old_data)
                    log.info('Backpack size: %d', self.getBackpackSize())
                else:
                    log.info('Nuking backpack since the player never had one...')
                    self.setBackpackSize(0)  # RIP
            hack.reset()  # This nukes backups!

    def _fixRemoveKey(self, data: dict, key: str, reason: str) -> None:
        if key in data:
            log.warning(f'Removing key {key!r} from save ({reason})')
            del data[key]

    def serializeEditorData(self, data: dict):
        edd = self.getCoCTweakConfig()
        # data['flags'][str(self.COCTWEAKDATATYPE.FLAG_ID)] = json.dumps(edd.serialize(), separators=(',', ':'))
        self.flags.set(self.COCTWEAKDATATYPE.FLAG_ID, json.dumps(
            edd.serialize(), separators=(',', ':')))

    def getStorageByKey(self, key: str) -> Storage:
        for s in self.allStorage:
            if key in s.keys:
                return s
        raise KeyError(key)

    def countCockSocks(self, sockID: str):
        o = 0
        for cock in self.cocks:
            if cock.sock == sockID:
                o += 1
        return o

    def loadNPCs(self) -> None:
        for npcType in self.AVAILABLE_NPCS:
            npc = npcType(self)
            self.NPCs[npc.ID] = npc
            for alias in npc.ALIASES:
                self.NPCs[alias] = npc
            npc.afterLoad()

    def saveNPCs(self) -> None:
        saved = set()
        for npc in self.NPCs.values():
            if npc.ID in saved:
                continue
            saved.add(npc.ID)
            npc.beforeSave()

    def serializePerks(self, data: dict) -> None:
        perks = []
        for p in self.perks.values():
            perks.append(p.serialize())
        data['perks'] = perks

    def serialize(self) -> dict:

        data = super().serialize()

        # Fuckups!
        self._fixRemoveKey(data, 'flags_decoded',
                           'Older version of decoded flags')
        self._fixRemoveKey(data, 'flags-decoded',
                           'Older version of decoded flags')

        self.saveNPCs()
        if self.hasCapability(SaveCaps.SELF_SAVING_OBJECTS):
            self.saveSelfSavingObjects(data)

        data.update(self.stats.serialize())
        data.update(self.combat_stats.serialize())

        data['saveName'] = self.saveName  # non-standard

        self.serializeEditorData(data)
        data.update(self.flags.serialize())

        data['cocks'] = [c.serialize() for c in self.cocks]
        data['vaginas'] = [v.serialize() for v in self.vaginas]
        data['breastRows'] = [br.serialize() for br in self.breast_rows]
        data['ass'] = self.ass.serialize()

        # self.ears.serializeTo(data)
        # self.horns.serializeTo(data)
        self.tail.serializeTo(data)
        # self.wings.serializeTo(data)
        self.skin.serializeTo(data)
        # self.lowerBody.serializeTo(data)
        self.balls.serializeTo(data)
        self.face.serializeTo(data)
        if self.antennae:
            self.antennae.serializeTo(data)
        if self.arms:
            self.arms.serializeTo(data)

        data['sexOrientation'] = self.sexOrientation

        data['gems'] = self.gems

        data['days'] = self.days
        data['hours'] = self.hours
        data['minutes'] = self.minutes

        data['notes'] = self.notes

        data['short'] = self.shortName

        data['startingRace'] = self.startingRace

        data['perkPoints'] = self.perk_points
        data['statPoints'] = self.stat_points

        selist = []
        for se in self.statusEffects.values():
            selist.append(se.serialize())
        data['statusAffects'] = selist  # sic

        self.serializePerks(data)

        keyItems = []
        ki: KeyItem
        for ki in sorted(self.keyItems.values(), key=lambda k: k.id):
            keyItems.append(ki.serialize())
        data['keyItems'] = keyItems

        self.saveInventory(data)
        # self.saveChest(data, 'itemStorage', self.itemStorage)
        # self.saveChest(data, 'gearStorage', self.gearStorage)

        self.vaginalPregnancy.serializeTo(data)
        self.analPregnancy.serializeTo(data)
        return data

    def createQuadPerk(self, pid, values, permanent: bool = False) -> Perk:
        p = self.QUAD_PERK_TYPE()
        p.id = pid
        p.values = values
        p.isPermanent = permanent
        self.perks[p.id] = p
        return p

    def createBreastRow(self, size: int = 0, nipsPerBreast: int = 1) -> BaseBreastRow:
        # from [HGG]/classes/classes/Creature.as: public function createBreastRow(size:Number = 0, nipplesPerBreast:Number = 1):Boolean { @ 7nidNM7xp52V8Kb44k0da83C1Np6x9cQmcqo3pL7Fe2olc3HcHE3ag6y9aMH5fFctl0wFfme6z06UJ1jD4dL1IIa0mcxZfWe
        br: BaseBreastRow = self.BREASTROW_TYPE(self)
        br.breastRating = size
        br.nipplesPerBreast = nipsPerBreast
        self.breast_rows.append(br)
        return br

    def getBiggestBreastRow(self) -> Optional[BaseBreastRow]:
        if len(self.breast_rows) == 0:
            return None
        largest: BaseBreastRow = None
        for row in self.breast_rows:
            if largest is None or largest.breastRating < row.breastRating:
                largest = row
        return largest
