from enum import IntEnum, auto
from typing import Optional, Any, Dict
from coctweak.saves._serialization import SerializationVersionInfo

class ESerializeAsMemberType(IntEnum):
    SINGLE_INSTANCE = auto()
    LIST_MEMBER = auto()

class SaveObject(object):
    SERIALIZATION_STAMP: Optional[SerializationVersionInfo] = None
    OBJECT_KEY: Optional[str] = None
    MEMBER_TYPE: ESerializeAsMemberType = ESerializeAsMemberType.SINGLE_INSTANCE

    def __init__(self):
        self.raw: Dict[str, Any]={}

    def _getRaw(self, key: str, default: Any, data: Optional[Dict[str, Any]] = None) -> Any:
        if data is None:
            data = self.raw
        return data.get(key, default)

    def _getBool(self, key: str, default: bool=False, data: Optional[Dict[str, Any]] = None) -> bool:
        return bool(self._getRaw(key, default, data) or default)

    def _getInt(self, key: str, default: int=0, data: Optional[Dict[str, Any]] = None) -> int:
        return int(self._getRaw(key, default, data) or default)

    def _getStr(self, key: str, default: str="", data: Optional[Dict[str, Any]] = None) -> str:
        return str(self._getRaw(key, default, data) or default)

    def _getFloat(self, key: str, default: float=0.0, data: Optional[Dict[str, Any]] = None) -> float:
        return float(self._getRaw(key, default, data) or default)

    def _getList(self, key: str, default: list=[], data: Optional[Dict[str, Any]] = None) -> list:
        return list(self._getRaw(key, default, data) or default)

    def _getDict(self, key: str, default: dict=[], data: Optional[Dict[str, Any]] = None) -> dict:
        return dict(self._getRaw(key, default, data) or default)

    def serialize(self) -> dict:
        if self.SERIALIZATION_STAMP is not None:
            return self.SERIALIZATION_STAMP.serialize(self, self.raw)
        return self.raw

    def deserialize(self, data: dict) -> None:
        self.raw = data
        if self.SERIALIZATION_STAMP is not None:
            self.SERIALIZATION_STAMP.deserialize(self, data)

    def _addSelfToRootAsListMember(self, data: dict) -> None:
        if self.OBJECT_KEY in data:
            data[self.OBJECT_KEY] += [self.serialize()]
        else:
            data[self.OBJECT_KEY] = [self.serialize()]
    def _addSelfToRootAsSingleInstance(self, data: dict) -> None:
        data[self.OBJECT_KEY] = self.serialize()
    def _addSelfToRoot(self, data: dict) -> None:
        if self.OBJECT_KEY is not None:
            if self.MEMBER_TYPE == ESerializeAsMemberType.SINGLE_INSTANCE:
                self._addSelfToRootAsSingleInstance(data)
            elif self.MEMBER_TYPE == ESerializeAsMemberType.LIST_MEMBER:
                self._addSelfToRootAsListMember(data)
            else:
                raise ValueError('self.MEMBER_TYPE', 'self.MEMBER_TYPE must be a valid ESerializeAsMemberType')

    def serializeTo(self, data: dict) -> None:
        pass

    def deserializeFrom(self, data: dict) -> None:
        pass
