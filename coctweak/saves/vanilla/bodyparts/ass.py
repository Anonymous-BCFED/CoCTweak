from coctweak.saves.vanilla.bodyparts._serialization.ass import VanillaRawAss
from coctweak.saves.vanilla.enums.ass_looseness import VanillaAssLooseness
from coctweak.saves.vanilla.enums.ass_wetness import VanillaAssWetness
from coctweak.saves.vanilla.enums.ass_ratings import VanillaAssRatings

class VanillaAss(VanillaRawAss):
    OBJECT_KEY = 'ass'
    ENUM_LOOSENESS = VanillaAssLooseness
    ENUM_WETNESS = VanillaAssWetness
    ENUM_RATINGS = VanillaAssRatings

    def deserializeFrom(self, data: dict) -> None:
        super().deserialize(data[self.OBJECT_KEY])
        super().deserializeFrom(data)

    def serializeTo(self, data: dict) -> None:
        data[self.OBJECT_KEY] = self.serialize()
        data['buttRating'] = self.buttRating

    def analCapacity(self) -> float:
        return 0. #TODO
