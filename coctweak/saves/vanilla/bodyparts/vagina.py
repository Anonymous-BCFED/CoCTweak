from coctweak.saves.vanilla.bodyparts._serialization.vagina import VanillaRawVagina
from coctweak.saves.vanilla.enums.vag_wetness import VanillaVagWetness
from coctweak.saves.vanilla.enums.vag_looseness import VanillaVagLooseness
from coctweak.saves.vanilla.enums.vag_types import VanillaVagTypes
from coctweak.saves.vanilla.piercing import VanillaPiercing
__ALL__ = ['VanillaVagina']

class VanillaVagina(VanillaRawVagina):
    TYPE_PIERCING = VanillaPiercing
    ENUM_WETNESS = VanillaVagWetness
    ENUM_LOOSENESS = VanillaVagLooseness
    ENUM_TYPES = VanillaVagTypes
