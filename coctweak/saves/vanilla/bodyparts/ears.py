from coctweak.saves.vanilla.enums.eartypes import VanillaEarType
from coctweak.saves.vanilla.piercing import VanillaPiercing
from coctweak.saves.vanilla.bodyparts._serialization.ears import VanillaRawEars

class VanillaEars(VanillaRawEars):
    ENUM_TYPES = VanillaEarType
    TYPE_PIERCING = VanillaPiercing
