from coctweak.saves.vanilla.enums.skintypes import VanillaSkinType
from coctweak.saves.vanilla.bodyparts._serialization.skin import VanillaRawSkin

class VanillaSkin(VanillaRawSkin):
    ENUM_TYPES = VanillaSkinType
