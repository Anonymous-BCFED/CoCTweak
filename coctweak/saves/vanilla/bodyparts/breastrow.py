from typing import Type
from enum import IntEnum
from coctweak.saves.vanilla.bodyparts._serialization.breastrow import VanillaRawBreastRow

class VanillaBreastRow(VanillaRawBreastRow):
    pass
