from coctweak.saves.vanilla.enums.tailtypes import VanillaTailType
from coctweak.saves.vanilla.bodyparts._serialization.tail import VanillaRawTail

class VanillaTail(VanillaRawTail):
    ENUM_TYPES = VanillaTailType
