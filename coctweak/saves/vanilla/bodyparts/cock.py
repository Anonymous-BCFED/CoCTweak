from coctweak.saves.vanilla.bodyparts._serialization.cock import VanillaRawCock
from coctweak.saves.common.bodyparts.cock import ECockTypeFlags
from coctweak.saves.vanilla.enums.cocktypes import VanillaCockTypes
from coctweak.saves.vanilla.enums.cocksocktypes import VanillaCockSockTypes
from coctweak.saves.vanilla.piercing import VanillaPiercing

class VanillaCock(VanillaRawCock):
    TYPE_PIERCING = VanillaPiercing
    COCK_TYPES = VanillaCockTypes
    SOCK_TYPES = VanillaCockSockTypes
    COCK_TYPE_FLAGS = {
        VanillaCockTypes.DOG:       ECockTypeFlags.HAS_KNOT,
    }
    def getCockTypeStr(self):
        return VanillaCockTypes(self.type).name
