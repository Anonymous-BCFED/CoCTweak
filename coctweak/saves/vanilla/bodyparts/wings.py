from coctweak.saves.vanilla.enums.wingtypes import VanillaWingType
from coctweak.saves.vanilla.bodyparts._serialization.wings import VanillaRawWings

class VanillaWings(VanillaRawWings):
    ENUM_TYPES = VanillaWingType
