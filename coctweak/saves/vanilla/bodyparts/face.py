from coctweak.saves.vanilla.enums.facetypes import VanillaFaceType
from coctweak.saves.vanilla.bodyparts._serialization.face import VanillaRawFace

class VanillaFace(VanillaRawFace):
    ENUM_TYPES = VanillaFaceType
