from coctweak.saves.vanilla.enums.horntypes import VanillaHornType
from coctweak.saves.vanilla.bodyparts._serialization.horns import VanillaRawHorns

class VanillaHorns(VanillaRawHorns):
    ENUM_TYPES = VanillaHornType
