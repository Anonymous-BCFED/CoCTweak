# @GENERATED 
from coctweak.saves.common.bodyparts.wings import BaseWings

__ALL__=['VanillaRawWings']

class VanillaRawWings(BaseWings):
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
