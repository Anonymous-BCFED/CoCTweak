# @GENERATED from coc/vanilla/classes/classes/AssClass.as
from coctweak.saves.common.bodyparts.ass import BaseAss

__ALL__=['VanillaRawAss']

class VanillaRawAss(BaseAss):
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
