# @GENERATED 
from coctweak.saves.common.bodyparts.balls import BaseBalls

__ALL__=['VanillaRawBalls']

class VanillaRawBalls(BaseBalls):
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
