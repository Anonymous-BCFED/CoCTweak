# @GENERATED 
from coctweak.saves.common.bodyparts.lowerbody import BaseLowerBody

__ALL__=['VanillaRawLowerBody']

class VanillaRawLowerBody(BaseLowerBody):
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
