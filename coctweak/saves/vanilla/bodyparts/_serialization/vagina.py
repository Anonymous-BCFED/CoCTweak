# @GENERATED from coc/vanilla/classes/classes/VaginaClass.as
from coctweak.saves.common.bodyparts.vagina import BaseVagina

__ALL__=['VanillaRawVagina']

class VanillaRawVagina(BaseVagina):
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
