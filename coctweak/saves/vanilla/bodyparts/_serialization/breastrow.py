# @GENERATED from coc/vanilla/classes/classes/BreastRowClass.as
from coctweak.saves.common.bodyparts.breastrow import BaseBreastRow

__ALL__=['VanillaRawBreastRow']

class VanillaRawBreastRow(BaseBreastRow):
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
