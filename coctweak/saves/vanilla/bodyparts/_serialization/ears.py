# @GENERATED 
from coctweak.saves.common.bodyparts.ears import BaseEars

__ALL__=['VanillaRawEars']

class VanillaRawEars(BaseEars):
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
