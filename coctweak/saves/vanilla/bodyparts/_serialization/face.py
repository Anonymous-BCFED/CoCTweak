# @GENERATED 
from coctweak.saves.common.bodyparts.face import BaseFace

__ALL__=['VanillaRawFace']

class VanillaRawFace(BaseFace):
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
