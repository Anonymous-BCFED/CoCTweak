# @GENERATED from coc/vanilla/classes/classes/Cock.as
from typing import Type
from typing import Type
from coctweak.saves.common.bodyparts.cock import BaseCock
from enum import IntEnum, Enum

__ALL__=['VanillaRawCock']

class VanillaRawCock(BaseCock):
    COCK_TYPES: Type[IntEnum] = None
    SOCK_TYPES: Type[Enum] = None
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
