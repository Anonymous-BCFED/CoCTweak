# @GENERATED 
from coctweak.saves.common.bodyparts.skin import BaseSkin

__ALL__=['VanillaRawSkin']

class VanillaRawSkin(BaseSkin):
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
