# @GENERATED 
from coctweak.saves.common.bodyparts.horns import BaseHorns

__ALL__=['VanillaRawHorns']

class VanillaRawHorns(BaseHorns):
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
