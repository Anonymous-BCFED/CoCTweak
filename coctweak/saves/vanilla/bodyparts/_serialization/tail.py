# @GENERATED 
from coctweak.saves.common.bodyparts.tail import BaseTail

__ALL__=['VanillaRawTail']

class VanillaRawTail(BaseTail):
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
