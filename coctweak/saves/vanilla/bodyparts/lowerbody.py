from coctweak.saves.vanilla.enums.lowerbodytypes import VanillaLowerBodyType
from coctweak.saves.vanilla.bodyparts._serialization.lowerbody import VanillaRawLowerBody

class VanillaLowerBody(VanillaRawLowerBody):
    ENUM_TYPES = VanillaLowerBodyType
