from coctweak.saves.pregnancystore import FlagPregnancyStore, VarPregnancyStore
from coctweak.saves.vanilla.enums.kflags import VanillaKFlags
from coctweak.saves.vanilla.enums.pregnancytypes import VanillaPregnancyType
from coctweak.saves.vanilla.enums.incubationdurations import VanillaIncubationDuration

class VanillaFlagPregnancyStore(FlagPregnancyStore[VanillaPregnancyType, VanillaIncubationDuration]):
    PREGTYPE_ENUM = VanillaPregnancyType
    INCUBATION_ENUM = VanillaIncubationDuration

class VanillaVarPregnancyStore(VarPregnancyStore[VanillaPregnancyType, VanillaIncubationDuration]):
    PREGTYPE_ENUM = VanillaPregnancyType
    INCUBATION_ENUM = VanillaIncubationDuration
