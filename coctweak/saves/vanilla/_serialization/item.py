# @GENERATED from coc/vanilla/classes/classes/ItemSlotClass.as
from coctweak.saves.common.item import BaseItem

__ALL__=['VanillaRawItem']

class VanillaRawItem(BaseItem):
    def __init__(self) -> None:
        super().__init__()
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
