from coctweak.saves._save import BaseSave
from coctweak.saves.vanilla.corestats import VanillaCoreStats
from coctweak.saves.vanilla.editordata import VanillaCoCTweakData

from coctweak.saves.vanilla.bodyparts.ass import VanillaAss
from coctweak.saves.vanilla.bodyparts.breastrow import VanillaBreastRow
from coctweak.saves.vanilla.bodyparts.cock import VanillaCock
from coctweak.saves.vanilla.bodyparts.ears import VanillaEars
from coctweak.saves.vanilla.bodyparts.face import VanillaFace
from coctweak.saves.vanilla.bodyparts.horns import VanillaHorns
from coctweak.saves.vanilla.bodyparts.lowerbody import VanillaLowerBody
from coctweak.saves.vanilla.bodyparts.skin import VanillaSkin
from coctweak.saves.vanilla.bodyparts.tail import VanillaTail
from coctweak.saves.vanilla.bodyparts.vagina import VanillaVagina
from coctweak.saves.vanilla.bodyparts.wings import VanillaWings

from coctweak.saves.vanilla.appearance import VanillaAppearance

from coctweak.saves.vanilla.pregnancystore import VanillaVarPregnancyStore

class VanillaSave(BaseSave):
    CORE_STATS_TYPE = VanillaCoreStats
    NAME = 'Vanilla'
    GLOBALDATA_TYPE = None
    COCTWEAKDATATYPE = VanillaCoCTweakData

    ASS_TYPE = VanillaAss
    BREASTROW_TYPE = VanillaBreastRow
    COCK_TYPE = VanillaCock
    EARS_TYPE = VanillaEars
    FACE_TYPE = VanillaFace
    HORNS_TYPE = VanillaHorns
    LOWER_BODY_TYPE = VanillaLowerBody
    SKIN_TYPE = VanillaSkin
    TAIL_TYPE = VanillaTail
    VAGINA_TYPE = VanillaVagina
    WINGS_TYPE = VanillaWings

    VAGINAL_PREGNANCY_TYPE = VanillaVarPregnancyStore
    ANAL_PREGNANCY_TYPE = VanillaVarPregnancyStore

    APPEARANCE_TYPE = VanillaAppearance

    def getDifficulty(self):
        return 'N/A'

    def getMaxHP(self):
        # https://gitgud.io/BelshazzarII/CoCAnon_mod/blob/master/classes/classes/Creature.as#L4709
        # max += int(tou * 2 + 50);
        m = self.stats.toughness * 2 + 50
        #if (findPerk(PerkLib.Tank) >= 0) max += 50;
        if 'Tank' in self.perks:
            m += 50
        #if (findPerk(PerkLib.Tank2) >= 0) max += Math.round(tou);
        if 'Tank 2' in self.perks:
            m += round(self.stats.toughness)
        #if (findPerk(PerkLib.Tank3) >= 0) max += level * 5;
        if 'Tank 3' in self.perks:
            m += self.level * 5

        #max += level * 15;
        m += self.level * 15
        #max += getBonusStat(BonusDerivedStats.maxHealth);
        m += self.getBonusStat('Max Health')
		#if (findPerk(PerkLib.ChiReflowDefense) >= 0) max += UmasShop.NEEDLEWORK_DEFENSE_EXTRA_HP;
        if self.perks.get('Chi Reflow - Defense', -1) >= 0:
            m += 50

        #if (jewelryEffectId == JewelryLib.MODIFIER_HP) max += jewelryEffectMagnitude;
        # Can't directly translate ATM.

        #max *= getBonusStatMultiplicative(BonusDerivedStats.maxHealth);
        #if (hasStatusEffect(St
        return m

    def serialize(self):
        data = super().serialize()
        data['version'] = self.version
        return data

    def deserialize(self, data: dict, isFile: bool) -> None:
        super().deserialize(data, isFile)
        self.version = data['version']
