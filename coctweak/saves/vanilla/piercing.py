from coctweak.saves.common.piercing import BasePiercing
from coctweak.saves.vanilla.enums.piercingtypes import VanillaPiercingType
class VanillaPiercing(BasePiercing):
    ENUM_TYPES = VanillaPiercingType
