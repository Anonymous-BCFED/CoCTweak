# @GENERATED from coc/vanilla/includes/appearanceDefs.as
from enum import IntEnum

__all__ = ['VanillaTailType']

class VanillaTailType(IntEnum):
    NONE            = 0
    HORSE           = 1
    DOG             = 2
    DEMONIC         = 3
    COW             = 4
    SPIDER_ADBOMEN  = 5
    BEE_ABDOMEN     = 6
    SHARK           = 7
    CAT             = 8
    LIZARD          = 9
    RABBIT          = 10
    HARPY           = 11
    KANGAROO        = 12
    FOX             = 13
    DRACONIC        = 14
    RACCOON         = 15
    MOUSE           = 16
    FERRET          = 17
