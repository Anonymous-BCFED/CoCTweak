# @GENERATED from enums.yml
from enum import IntEnum

__all__ = ['VanillaPiercingType']

class VanillaPiercingType(IntEnum):
    NONE        = 0
    YES         = 1
    SILVER      = 2
    UNKNOWN_03  = 3
