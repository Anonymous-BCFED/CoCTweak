# @GENERATED from coc/vanilla/includes/appearanceDefs.as
from enum import IntEnum

__all__ = ['VanillaWingType']

class VanillaWingType(IntEnum):
    NONE             = 0
    BEE_LIKE_SMALL   = 1
    BEE_LIKE_LARGE   = 2
    HARPY            = 4
    IMP              = 5
    BAT_LIKE_TINY    = 6
    BAT_LIKE_LARGE   = 7
    SHARK_FIN        = 8
    FEATHERED_LARGE  = 9
    DRACONIC_SMALL   = 10
    DRACONIC_LARGE   = 11
    GIANT_DRAGONFLY  = 12
