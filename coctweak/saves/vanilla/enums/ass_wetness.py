# @GENERATED from coc/vanilla/includes/appearanceDefs.as
from enum import IntEnum

__all__ = ['VanillaAssWetness']

class VanillaAssWetness(IntEnum):
    DRY             = 0
    NORMAL          = 1
    MOIST           = 2
    SLIMY           = 3
    DROOLING        = 4
    SLIME_DROOLING  = 5
