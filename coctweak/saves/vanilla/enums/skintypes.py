# @GENERATED from coc/vanilla/includes/appearanceDefs.as
from enum import IntEnum

__all__ = ['VanillaSkinType']

class VanillaSkinType(IntEnum):
    PLAIN      = 0
    FUR        = 1
    SCALES     = 2
    GOO        = 3
    UNDEFINED  = 4
