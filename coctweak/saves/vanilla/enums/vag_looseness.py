# @GENERATED from coc/vanilla/includes/appearanceDefs.as
from enum import IntEnum

__all__ = ['VanillaVagLooseness']

class VanillaVagLooseness(IntEnum):
    TIGHT            = 0
    NORMAL           = 1
    LOOSE            = 2
    GAPING           = 3
    GAPING_WIDE      = 4
    LEVEL_CLOWN_CAR  = 5
