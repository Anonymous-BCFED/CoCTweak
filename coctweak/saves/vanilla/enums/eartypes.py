# @GENERATED from coc/vanilla/includes/appearanceDefs.as
from enum import IntEnum

__all__ = ['VanillaEarType']

class VanillaEarType(IntEnum):
    HUMAN     = 0
    HORSE     = 1
    DOG       = 2
    COW       = 3
    ELFIN     = 4
    CAT       = 5
    LIZARD    = 6
    BUNNY     = 7
    KANGAROO  = 8
    FOX       = 9
    DRAGON    = 10
    RACCOON   = 11
    MOUSE     = 12
    FERRET    = 13
