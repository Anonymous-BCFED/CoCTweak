# @GENERATED from coc/vanilla/classes/classes/CockTypesEnum.as
from enum import IntEnum

__all__ = ['VanillaCockTypes']

class VanillaCockTypes(IntEnum):
    HUMAN      = 0
    HORSE      = 1
    DOG        = 2
    DEMON      = 3
    TENTACLE   = 4
    CAT        = 5
    LIZARD     = 6
    ANEMONE    = 7
    KANGAROO   = 8
    DRAGON     = 9
    DISPLACER  = 10
    FOX        = 11
    BEE        = 12
    UNDEFINED  = 13
