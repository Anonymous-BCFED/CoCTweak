# @GENERATED from coc/vanilla/includes/appearanceDefs.as
from enum import IntEnum

__all__ = ['VanillaLowerBodyType']

class VanillaLowerBodyType(IntEnum):
    HUMAN                  = 0
    HOOFED                 = 1
    DOG                    = 2
    NAGA                   = 3
    CENTAUR                = 4
    DEMONIC_HIGH_HEELS     = 5
    DEMONIC_CLAWS          = 6
    BEE                    = 7
    GOO                    = 8
    CAT                    = 9
    LIZARD                 = 10
    PONY                   = 11
    BUNNY                  = 12
    HARPY                  = 13
    KANGAROO               = 14
    CHITINOUS_SPIDER_LEGS  = 15
    DRIDER_LOWER_BODY      = 16
    FOX                    = 17
    DRAGON                 = 18
    RACCOON                = 19
