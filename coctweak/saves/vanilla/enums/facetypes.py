# @GENERATED from coc/vanilla/includes/appearanceDefs.as
from enum import IntEnum

__all__ = ['VanillaFaceType']

class VanillaFaceType(IntEnum):
    HUMAN         = 0
    HORSE         = 1
    DOG           = 2
    COW_MINOTAUR  = 3
    SHARK_TEETH   = 4
    SNAKE_FANGS   = 5
    CAT           = 6
    LIZARD        = 7
    BUNNY         = 8
    KANGAROO      = 9
    SPIDER_FANGS  = 10
    FOX           = 11
    DRAGON        = 12
    RACCOON_MASK  = 13
    RACCOON       = 14
    BUCKTEETH     = 15
    MOUSE         = 16
    FERRET_MASK   = 17
    FERRET        = 18
