# @GENERATED from coc/vanilla/includes/appearanceDefs.as
from enum import IntEnum

__all__ = ['VanillaVagTypes']

class VanillaVagTypes(IntEnum):
    HUMAN            = 0
    BLACK_SAND_TRAP  = 5
