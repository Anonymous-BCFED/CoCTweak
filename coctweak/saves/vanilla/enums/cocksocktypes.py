# MANUALLY made
from enum import Enum

__ALL__ = ['VanillaCockSockTypes']

class VanillaCockSockTypes(Enum):
    NONE = ''
    ## from [VANILLA]/includes/appearance.as: public function sockDescript(index:int):void @ 7zF9Kmclddue9yjeMh0aY8jEckE6nt2gJ5bl1Juftu2u29Cx4ywbicgEd5sSdnKcHt6b4dk6avFf2M0qUbQa4QK9IA2Kng1R
    WOOL        = 'wool'
    ALABASTER   = 'alabaster'
    COCKRING    = 'cockring'
    VIRIDIAN    = 'viridian'
    SCARLET     = 'scarlet'
    COBALT      = 'cobalt'
    GILDED      = 'gilded'
    AMARANTHINE = 'amaranthine'
