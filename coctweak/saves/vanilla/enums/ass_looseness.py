# @GENERATED from coc/vanilla/includes/appearanceDefs.as
from enum import IntEnum

__all__ = ['VanillaAssLooseness']

class VanillaAssLooseness(IntEnum):
    VIRGIN     = 0
    TIGHT      = 1
    NORMAL     = 2
    LOOSE      = 3
    STRETCHED  = 4
    GAPING     = 5
