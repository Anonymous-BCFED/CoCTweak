# @GENERATED from coc/vanilla/includes/appearanceDefs.as
from enum import IntEnum

__all__ = ['VanillaHornType']

class VanillaHornType(IntEnum):
    NONE                      = 0
    DEMON                     = 1
    COW_MINOTAUR              = 2
    DRACONIC_X2               = 3
    DRACONIC_X4_12_INCH_LONG  = 4
    ANTLERS                   = 5
