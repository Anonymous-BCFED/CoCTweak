# @GENERATED from coc/vanilla/includes/appearanceDefs.as
from enum import IntEnum

__all__ = ['VanillaVagWetness']

class VanillaVagWetness(IntEnum):
    DRY        = 0
    NORMAL     = 1
    WET        = 2
    SLICK      = 3
    DROOLING   = 4
    SLAVERING  = 5
