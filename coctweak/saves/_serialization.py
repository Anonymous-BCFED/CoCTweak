'''
Some mods have a serialization version attached to objects.  This class handles them.
'''
class SerializationVersionInfo(object):
    def __init__(self):
        pass

    def serialize(self, obj, data: dict) -> dict:
        return data

    def deserialize(self, obj, data: dict) -> None:
        pass
