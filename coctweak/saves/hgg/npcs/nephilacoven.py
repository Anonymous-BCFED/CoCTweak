from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class NephilaCovenNPC(BaseNPC):
    ID = 'nephilacoven'
    NAME = 'NephilaCoven'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/NephilaCovenFollowerScene.as: public class NephilaCovenFollowerScene extends NPCAwareContent { @ fIT38R7kG1xX1C84o5eGtcUgdBp4pFgOidFKbeSeeAfzj2Gv3Tg71H11a4HvcwUeLhcFP9nrbQ59Oj9GY5G64NG6Lg20I7Rn
        ## from [HGG]/classes/classes/Scenes/NPCs/NephilaCovenScene.as: public class NephilaCovenScene extends NPCAwareContent { @ 1h73GsfVu9sD4VDgeg0uX3j3b8rgMz2QpdBU7ntf6L8og4en06dcuMd7ief6ghHfGGbH6aY58DM4wLbi94kP8nKfAw9Bg0f6
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('NEPHILA_COVEN_QUEEN_CROWNED', int, 0, HGGKFlags.NEPHILA_COVEN_QUEEN_CROWNED)
        self.addKFlag('NEPHILA_MOUSE_MET', int, 0, HGGKFlags.NEPHILA_MOUSE_MET)
        self.addKFlag('NEPHILA_MOUSE_OWNED', int, 0, HGGKFlags.NEPHILA_MOUSE_OWNED)
        self.addKFlag('NEPHILA_QUEEN_ARMOR', int, 0, HGGKFlags.NEPHILA_QUEEN_ARMOR)
        self.addKFlag('NEPHILA_SECOND_ASCENSION', int, 0, HGGKFlags.NEPHILA_SECOND_ASCENSION)
        self.addKFlag('PC_MET_NEPHILA_COVEN', int, 0, HGGKFlags.PC_MET_NEPHILA_COVEN)
