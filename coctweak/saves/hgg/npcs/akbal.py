from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class AkbalNPC(BaseNPC):
    ID = 'akbal'
    NAME = 'Akbal'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/Akbal.as: public class Akbal extends Monster { @ 5aB3G4f3S66g3te6zy4yhcXi15ncio5ajfy3drXdPnc3q09hcg7bnY4Rr0zof6sfMLbDo7FmbZAdyE5Jp26h86F4ZN7T8gTV
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/AkbalScene.as: public class AkbalScene extends BaseContent implements Encounter, SelfSaving, SelfDebug { @ 46x9e81Ei60rbZl4OG4pRbf85UH3VidtPetc8XZayIaxHfYQ9Bj1ik28Oe5A39Dfix0zd0LvgS9bza4UjgY45ytd8W78cavb
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "timesRaped": [
                "Int",
                ""
            ],
            "strayCat": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('strayCat', bool, False, 'akbal', 'strayCat')
        self.addSSOProperty('timesRaped', int, 0, 'akbal', 'timesRaped')
        ##########
        # kFlags
        ##########
        self.addKFlag('AKBAL_BITCH_Q', int, 0, HGGKFlags.AKBAL_BITCH_Q)
        self.addKFlag('AKBAL_DAY_DONE', int, 0, HGGKFlags.AKBAL_DAY_DONE)
        self.addKFlag('AKBAL_FUCKERY', int, 0, HGGKFlags.AKBAL_FUCKERY)
        self.addKFlag('AKBAL_QUEST_STATUS', int, 0, HGGKFlags.AKBAL_QUEST_STATUS, bitflags={
            1: 'AKBAL_QUEST_STARTED',
            2: 'AKBAL_EVENT_GOBLIN',
            4: 'AKBAL_EVENT_ALICE',
            8: 'AKBAL_EVENT_KITSUNE1',
            16: 'AKBAL_EVENT_KITSUNE2',
            32: 'AKBAL_QUEST_BADEND',
            64: 'AKBAL_QUEST_DONE',
        })
        self.addKFlag('AKBAL_SUBMISSION_COUNTER', int, 0, HGGKFlags.AKBAL_SUBMISSION_COUNTER)
        self.addKFlag('AKBAL_SUBMISSION_STATE', int, 0, HGGKFlags.AKBAL_SUBMISSION_STATE)
        self.addKFlag('AKBAL_TIMES_BITCHED', int, 0, HGGKFlags.AKBAL_TIMES_BITCHED)
        self.addKFlag('PLAYER_RESISTED_AKBAL', int, 0, HGGKFlags.PLAYER_RESISTED_AKBAL)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('AkbalFlameDebuff', bool, False, HGGStatusLib.AkbalFlameDebuff, None)
        self.addSFXProperty('PostAkbalSubmission', bool, False, HGGStatusLib.PostAkbalSubmission, None)
        self.addSFXProperty('TrueWhispered', bool, False, HGGStatusLib.TrueWhispered, None)
