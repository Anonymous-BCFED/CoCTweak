from coctweak.saves._npc import BaseNPC, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class YaraNPC(BaseNPC):
    ID = 'yara'
    NAME = 'Yara'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/YaraPiercingStudio.as: public class YaraPiercingStudio extends TelAdreAbstractContent { @ 8PzaoD9BA2zq9UNaXK5nm2Bw9b15qHbCe3Lc6MYd5r7va3vjdpo6fp2d04p96pRdopayv73V8Xy35C3rOfWLeGnfoHefn0Tb
        super().__init__(save)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('Yara', bool, False, HGGStatusLib.Yara, None, notes="Whether or not you've met Yara before.")
