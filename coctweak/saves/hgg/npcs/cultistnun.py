from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class CultistNunNPC(BaseNPC):
    ID = 'cultistnun'
    NAME = 'CultistNun'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Lake/CultistNunScene.as: public class CultistNunScene extends AbstractLakeContent { @ 9dk3hRcCQa9ueHz42sc8v9gfeGJgi00yOdeMgbZcDRbnf5mMetM9BX5KUfmV9DB5Zag6245NbF2aOv8d1fcW0TOgDrc73eRj
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('DEFILED_NUN', int, 0, HGGKFlags.DEFILED_NUN, intbool=True)
        self.addKFlag('MET_CULTIST_NUN', int, 0, HGGKFlags.MET_CULTIST_NUN, intbool=True)
        self.addKFlag('PRAYED_WITH_NUN', int, 0, HGGKFlags.PRAYED_WITH_NUN, intbool=True)
        self.addKFlag('TOOK_NAUGHTY_HABIT', int, 0, HGGKFlags.TOOK_NAUGHTY_HABIT)
