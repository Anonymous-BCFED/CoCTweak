from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class FrogGirlNPC(BaseNPC):
    ID = 'froggirl'
    NAME = 'Frog Girl'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/FrogGirlScene.as: public class FrogGirlScene extends BaseContent implements VaginalPregnancy, AnalPregnancy, SelfSaving, SelfDebug, Encounter, TimeAwareInterface { @ 7mGebg0Ka3OqgGf6ZB5Ji9m8eNSgWTftCfil7gRbtjg5we3d3V956T1m0fUQ8bJbTE5m743qd5t4Qy3P0gEpc7JfKxcWK613
        super().__init__(save)
        #############
        # SSO Props
        #############
        self.addSSOProperty('analMad', int, 0, 'froggirl', 'analMad')
        self.addSSOProperty('angryLosses', int, 0, 'froggirl', 'angryLosses')
        self.addSSOProperty('eggCount', int, 0, 'froggirl', 'eggCount')
        self.addSSOProperty('fought', bool, False, 'froggirl', 'fought')
        self.addSSOProperty('lastEncounter', int, 0, 'froggirl', 'lastEncounter')
        self.addSSOProperty('metSuwako', bool, False, 'froggirl', 'metSuwako')
        self.addSSOProperty('pattyCake', bool, False, 'froggirl', 'pattyCake')
        self.addSSOProperty('shownKids', int, 0, 'froggirl', 'shownKids', choices={
            0: 'N/A',
            1: 'Shown',
            2: 'Accessed Menu',
        })
        self.addSSOProperty('submissive', bool, False, 'froggirl', 'submissive')
        self.addSSOProperty('taughtLesson', int, 0, 'froggirl', 'taughtLesson', choices={
            0: 'N/A',
            1: 'Taught',
            2: 'Frightened',
            3: 'Killed',
        })
        ##########
        # kFlags
        ##########
        # PregnancyStore vag incubation counter
        self.addKFlag('FROG_PREGNANCY_INCUBATION', int, 0, HGGKFlags.FROG_PREGNANCY_INCUBATION)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('FROG_PREGNANCY_TYPE', int, 0, HGGKFlags.FROG_PREGNANCY_TYPE)
        self.addKFlag('TIMES_ENCOUNTERED_FROG', int, 0, HGGKFlags.TIMES_ENCOUNTERED_FROG)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.FROG_PREGNANCY_TYPE, HGGKFlags.FROG_PREGNANCY_INCUBATION)
