from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class MaddieNPC(BaseNPC):
    ID = 'maddie'
    NAME = 'Maddie'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Maddie.as: public class Maddie extends TelAdreAbstractContent { @ aQQ9yj28Se5n0b14RGbmn8qe5p18n2cB00A47Gp18idIw9Ip9YIbbZe5gdCh2DYcWG3OlaqV8er9kQ468b712cXf4U2aq18A
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('MADDIE_STATUS', int, 0, HGGKFlags.MADDIE_STATUS, choices={
            0: 'Unmet',
            1: 'Agreed',
            -1: 'Snuck out',
            -2: 'Seen her escorted out',
            3: 'Stayed',
            4: 'Epilogued',
        })
        self.addKFlag('MINO_CHEF_EXPLAINED_INGREDIENTS', int, 0, HGGKFlags.MINO_CHEF_EXPLAINED_INGREDIENTS)
        self.addKFlag('MINO_CHEF_INTRO', int, 0, HGGKFlags.MINO_CHEF_INTRO)
