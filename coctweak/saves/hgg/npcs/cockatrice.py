from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class CockatriceNPC(BaseNPC):
    ID = 'cockatrice'
    NAME = 'Cockatrice'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/HighMountains/Cockatrice.as: public class Cockatrice extends StareMonster { @ fVKdRK1SB1QG1l48C935EcqkdpwbwEbGrfts7Pb5nEbb8cjdaVa3sb73qgj17Bla2keSU5X65iV3d13tD28PaJH7YE77k7lx
        ## from [HGG]/classes/classes/Scenes/Areas/HighMountains/CockatriceScene.as: public class CockatriceScene extends BaseContent { @ 3Rr2FD5Ac1ejdmFeif6QxbP1dwYfVKdRn4fO2dJ8fOd1e9TK1TW8UU7V5enqf3jdq9f95f2UbAq6YcbIWcqh8RJgzP8amaMP
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('BASILISK_RESISTANCE_TRACKER', int, 0, HGGKFlags.BASILISK_RESISTANCE_TRACKER, min=0, max=100, notes='How resistant you are to basilisk/cockatrice stares. Increments during combat.')
        self.addKFlag('TIMES_ENCOUNTERED_COCKATRICES', int, 0, HGGKFlags.TIMES_ENCOUNTERED_COCKATRICES)
