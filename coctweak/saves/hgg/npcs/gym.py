from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class GymNPC(BaseNPC):
    ID = 'gym'
    NAME = 'Gym'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Gym.as: public class Gym extends TelAdreAbstractContent { @ cVkcoX4wR2Kwfjo6mzcxxfd24UmeYMcCI8vW7mKeMf3T03duc1dgzp2hTftRgZPa3936qcH7eR6eTpdgg7WCh2s291aUKcDp
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('LIFETIME_GYM_MEMBER', int, 0, HGGKFlags.LIFETIME_GYM_MEMBER)
