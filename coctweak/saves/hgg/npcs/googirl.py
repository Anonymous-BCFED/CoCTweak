from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class GooGirlNPC(BaseNPC):
    ID = 'googirl'
    NAME = 'GooGirl'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Lake/GooGirlScene.as: public class GooGirlScene extends AbstractLakeContent implements Encounter { @ azM03Jgaoeh6bK3dgn3k08GDf8q8zLcX35kSexb2Gd00d2me8gv9UE0oRczte3t2o01bo2C9f83fMD8Dm3VT1Mvc37e312cD
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('GOOGIRL_BIRTHS', int, 0, HGGKFlags.GOOGIRL_BIRTHS)
        self.addKFlag('GOOGIRL_CONSECUTIVE_LOSSES', int, 0, HGGKFlags.GOOGIRL_CONSECUTIVE_LOSSES)
        self.addKFlag('GOO_TFED_MEAN', int, 0, HGGKFlags.GOO_TFED_MEAN)
        self.addKFlag('GOO_TFED_NICE', int, 0, HGGKFlags.GOO_TFED_NICE)
        self.addKFlag('PC_KNOWS_ABOUT_BLACK_EGGS', int, 0, HGGKFlags.PC_KNOWS_ABOUT_BLACK_EGGS)
        self.addKFlag('TIMES_FUCKED_NORMAL_GOOS', int, 0, HGGKFlags.TIMES_FUCKED_NORMAL_GOOS)
        self.addKFlag('TIMES_MET_OOZE', int, 0, HGGKFlags.TIMES_MET_OOZE)
        self.addKFlag('TIMES_THOUGHT_ABOUT_GOO_RECRUITMENT', int, 0, HGGKFlags.TIMES_THOUGHT_ABOUT_GOO_RECRUITMENT)
        self.addKFlag('TIMES_VALERIA_GOO_THREESOMED', int, 0, HGGKFlags.TIMES_VALERIA_GOO_THREESOMED)
