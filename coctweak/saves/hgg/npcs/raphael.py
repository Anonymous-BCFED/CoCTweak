from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class RaphaelNPC(BaseNPC):
    ID = 'raphael'
    NAME = 'Raphael'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/Raphael.as: public class Raphael extends NPCAwareContent implements TimeAwareInterface { @ 0Zz79M0DU9WD3oZbGD22h1YKbwS2jt2k35Id6tYcPVb2M5a079kdQO9pZ58BaHdgHI5cNbJE94K5CpgwleA3apa8qL3b45L4
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('FUCK_OFF_THEIVING_RAPHAEL', int, 0, HGGKFlags.FUCK_OFF_THEIVING_RAPHAEL)
        self.addKFlag('RAPHAEL_BONED', int, 0, HGGKFlags.RAPHAEL_BONED, intbool=True)
        self.addKFlag('RAPHAEL_DISGUSTED_BY_PC_APPEARANCE', int, 0, HGGKFlags.RAPHAEL_DISGUSTED_BY_PC_APPEARANCE, intbool=True)
        self.addKFlag('RAPHAEL_DRESS_GIVEN', int, 0, HGGKFlags.RAPHAEL_DRESS_GIVEN, intbool=True)
        self.addKFlag('RAPHAEL_INTELLIGENCE_TRAINING', int, 0, HGGKFlags.RAPHAEL_INTELLIGENCE_TRAINING)
        self.addKFlag('RAPHAEL_MET', int, 0, HGGKFlags.RAPHAEL_MET)
        self.addKFlag('RAPHAEL_RAPIER_TRANING', int, 0, HGGKFlags.RAPHAEL_RAPIER_TRANING)
        self.addKFlag('RAPHEAL_COUNTDOWN_TIMER', int, 0, HGGKFlags.RAPHEAL_COUNTDOWN_TIMER)
        self.addKFlag('REJECTED_RAPHAEL', int, 0, HGGKFlags.REJECTED_RAPHAEL, intbool=True)
        self.addKFlag('TIMES_ORPHANAGED_WITH_RAPHAEL', int, 0, HGGKFlags.TIMES_ORPHANAGED_WITH_RAPHAEL)
        self.addKFlag('URTA_PAID_OUT_RAPHAELS_BOUNTY', int, 0, HGGKFlags.URTA_PAID_OUT_RAPHAELS_BOUNTY, intbool=True)
