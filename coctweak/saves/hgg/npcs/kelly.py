from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class KellyNPC(BaseNPC):
    ID = 'kelly'
    NAME = 'Kelly'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Farm/Kelly.as: public class Kelly extends AbstractFarmContent implements TimeAwareInterface { @ daO59G1OD5ouerMfQh2bM3oK2uV4iy1C91dNbG94dz3XEf2e1ykbF5cr77Kv1h50UpeAQ2Cp8Ub71Ebfx4lrfP81DIdA3fiw
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('KELLY_BONUS_BOOB_ROWS', int, 0, HGGKFlags.KELLY_BONUS_BOOB_ROWS)
        self.addKFlag('KELLY_BONUS_TIT_ROWS', int, 0, HGGKFlags.KELLY_BONUS_TIT_ROWS)
        self.addKFlag('KELLY_CUNT_TYPE', int, 0, HGGKFlags.KELLY_CUNT_TYPE, choices={
            0: 'Normal',
            1: 'Horse',
        })
        self.addKFlag('KELLY_DISOBEYING_COUNTER', int, 0, HGGKFlags.KELLY_DISOBEYING_COUNTER)
        self.addKFlag('KELLY_FIRST_KID_GENDER', int, 0, HGGKFlags.KELLY_FIRST_KID_GENDER)
        self.addKFlag('KELLY_HAIR_COLOR', str, '', HGGKFlags.KELLY_HAIR_COLOR)
        self.addKFlag('KELLY_HEAT_TIME', int, 0, HGGKFlags.KELLY_HEAT_TIME)
        # PregnancyStore vag incubation counter
        self.addKFlag('KELLY_INCUBATION', int, 0, HGGKFlags.KELLY_INCUBATION)
        self.addKFlag('KELLY_KIDS', int, 0, HGGKFlags.KELLY_KIDS)
        self.addKFlag('KELLY_KIDS_MALE', int, 0, HGGKFlags.KELLY_KIDS_MALE)
        self.addKFlag('KELLY_LACTATING', None, 0, HGGKFlags.KELLY_LACTATING, unused=True)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('KELLY_PREGNANCY_TYPE', int, 0, HGGKFlags.KELLY_PREGNANCY_TYPE)
        self.addKFlag('KELLY_REWARD_COOLDOWN', int, 0, HGGKFlags.KELLY_REWARD_COOLDOWN)
        self.addKFlag('KELLY_TALK_N_HAND_TIMES', int, 0, HGGKFlags.KELLY_TALK_N_HAND_TIMES)
        self.addKFlag('KELLY_TATTOO_BUTT', int, 0, HGGKFlags.KELLY_TATTOO_BUTT)
        self.addKFlag('KELLY_TATTOO_COLLARBONE', int, 0, HGGKFlags.KELLY_TATTOO_COLLARBONE)
        self.addKFlag('KELLY_TATTOO_LOWERBACK', int, 0, HGGKFlags.KELLY_TATTOO_LOWERBACK)
        self.addKFlag('KELLY_TATTOO_SHOULDERS', int, 0, HGGKFlags.KELLY_TATTOO_SHOULDERS)
        self.addKFlag('KELLY_TIMES_APPLESAUCED', int, 0, HGGKFlags.KELLY_TIMES_APPLESAUCED)
        self.addKFlag('KELLY_TIMES_DIED_HAIR', int, 0, HGGKFlags.KELLY_TIMES_DIED_HAIR)
        self.addKFlag('KELLY_TIMES_PEPPERED', int, 0, HGGKFlags.KELLY_TIMES_PEPPERED)
        self.addKFlag('KELLY_TIMES_REWARDED', int, 0, HGGKFlags.KELLY_TIMES_REWARDED)
        self.addKFlag('KELLY_VAGINALLY_FUCKED_COUNT', int, 0, HGGKFlags.KELLY_VAGINALLY_FUCKED_COUNT)
        self.addKFlag('TIMES_PUNISHED_KELLY', int, 0, HGGKFlags.TIMES_PUNISHED_KELLY)
        self.addKFlag('TIMES_RIDDEN_KELLY_FOR_PUNISHMENT', int, 0, HGGKFlags.TIMES_RIDDEN_KELLY_FOR_PUNISHMENT)
        self.addKFlag('TIMES_RIM_JOBBED_BY_KELLY', int, 0, HGGKFlags.TIMES_RIM_JOBBED_BY_KELLY)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.KELLY_PREGNANCY_TYPE, HGGKFlags.KELLY_INCUBATION)
