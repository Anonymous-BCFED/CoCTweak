from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.common.npcmodules.renamable import RenamableNPC
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class HelSpawnNPC(BaseNPC, RenamableNPC):
    ID = 'helspawn'
    NAME = 'HelSpawn'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/HelSpawnScene.as: public class HelSpawnScene extends NPCAwareContent implements SelfSaving, SelfDebug { @ dnte5rfxZ5zM5dS4RU4Wu0ur9623Xo6J2fw0dgk4IAdJ49Bd3DzbrscOm95F4o1a7s6gN1ft8fv7Hl9JSbqBe0R2Nz9X2a9L
        super().__init__(save)
        self.setupRenamable(default='Helspawn', flag=HGGKFlags.HELSPAWN_NAME)
        #############
        # SSO Props
        #############
        '''
        {
            "footjob": [
                "Boolean",
                ""
            ],
            "sippedAnemone": [
                "Boolean",
                ""
            ],
            "wakeUp": [
                "Boolean",
                ""
            ],
            "surprised": [
                "Boolean",
                ""
            ],
            "fishing": [
                "Boolean",
                ""
            ],
            "boyfriend": [
                "Boolean",
                "Your daugther has his spiderboyfriend?"
            ],
            "alexThreesome": [
                "Boolean",
                "Have done already a threesome with your daughter and his boyfriend?"
            ],
            "alexAnalVirginity": [
                "Boolean",
                "The butt of the spider is untouched?"
            ]
        }
        '''
        self.addSSOProperty('alexAnalVirginity', bool, True, 'helspawn', 'alexAnalVirginity', notes='The butt of the spider is untouched?')
        self.addSSOProperty('alexThreesome', bool, False, 'helspawn', 'alexThreesome', notes='Have done already a threesome with your daughter and his boyfriend?')
        self.addSSOProperty('boyfriend', bool, False, 'helspawn', 'boyfriend', notes='Your daugther has his spiderboyfriend?')
        self.addSSOProperty('fishing', bool, False, 'helspawn', 'fishing')
        self.addSSOProperty('footjob', bool, False, 'helspawn', 'footjob')
        self.addSSOProperty('sippedAnemone', bool, False, 'helspawn', 'sippedAnemone')
        self.addSSOProperty('surprised', bool, False, 'helspawn', 'surprised')
        self.addSSOProperty('wakeUp', bool, False, 'helspawn', 'wakeUp')
        ##########
        # kFlags
        ##########
        self.addKFlag('HAD_FIRST_HELSPAWN_TALK', int, 0, HGGKFlags.HAD_FIRST_HELSPAWN_TALK)
        self.addKFlag('HAKON_AND_KIRI_VISIT', int, 0, HGGKFlags.HAKON_AND_KIRI_VISIT)
        self.addKFlag('HELSPAWN_AGE', int, 0, HGGKFlags.HELSPAWN_AGE)
        self.addKFlag('HELSPAWN_DADDY', int, 0, HGGKFlags.HELSPAWN_DADDY)
        self.addKFlag('HELSPAWN_DISCOVER_BOOZE', int, 0, HGGKFlags.HELSPAWN_DISCOVER_BOOZE)
        self.addKFlag('HELSPAWN_FUCK_INTERRUPTUS', int, 0, HGGKFlags.HELSPAWN_FUCK_INTERRUPTUS)
        self.addKFlag('HELSPAWN_GROWUP_COUNTER', int, 0, HGGKFlags.HELSPAWN_GROWUP_COUNTER)
        self.addKFlag('HELSPAWN_HADSEX', int, 0, HGGKFlags.HELSPAWN_HADSEX)
        self.addKFlag('HELSPAWN_INCEST', int, 0, HGGKFlags.HELSPAWN_INCEST)
        self.addKFlag('HELSPAWN_NAME', str, '', HGGKFlags.HELSPAWN_NAME)
        self.addKFlag('HELSPAWN_PERSONALITY', int, 0, HGGKFlags.HELSPAWN_PERSONALITY)
        self.addKFlag('HELSPAWN_WEAPON', str, '', HGGKFlags.HELSPAWN_WEAPON)
        self.addKFlag('PLAYERCAMEBEFORERIDING', bool, False, HGGKFlags.PLAYERCAMEBEFORERIDING)
        self.addKFlag('SPIDER_BRO_GIFT', int, 0, HGGKFlags.SPIDER_BRO_GIFT)
