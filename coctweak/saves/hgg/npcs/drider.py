from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class DriderNPC(BaseNPC):
    ID = 'drider'
    NAME = 'Drider'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Swamp/CorruptedDrider.as: public class CorruptedDrider extends AbstractSpiderMorph { @ 4WXbFY2mmfVvfEEeiH8eX84x8vD93P6bU27lcl94tn3VgfsG0pafltcPh3Tr3P87TK1gE4gTbZq7yn5BAc80fiq9ssd0TegX
        ## from [HGG]/classes/classes/Scenes/Areas/Swamp/CorruptedDriderScene.as: public class CorruptedDriderScene extends BaseContent { @ 6XPfhncLA7rSg6U8Us1pb5rX0TS5Dp5sW2v23wh0dheAabqG4Vx1hZ4pm5Tu1UjaL89Qj5x6cI7alkd2b61041lcq6aBlg37
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('TIMES_ENCOUNTERED_DRIDER', int, 0, HGGKFlags.TIMES_ENCOUNTERED_DRIDER)
