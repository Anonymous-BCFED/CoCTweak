from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class KitsuneNPC(BaseNPC):
    ID = 'kitsune'
    NAME = 'Kitsune'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/KitsuneScene.as: public class KitsuneScene extends BaseContent implements Encounter, SelfSaving, SelfDebug { @ feQ0Gk6vT8N66e4bSy7Uhgtvdv1cHjcjM9IWb6C8th59X25y5wU5wj3kIbdO4YK5UD1dt3Tz4zv4G393J8sT1ZD96Jg8f2QY
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "hadVision": [
                "Boolean",
                ""
            ],
            "statueLocation": [
                "StringList",
                "",
                [
                    {
                        "label": "",
                        "data": ""
                    },
                    {
                        "label": "desk",
                        "data": "desk"
                    },
                    {
                        "label": "bookshelf",
                        "data": "bookshelf"
                    },
                    {
                        "label": "table",
                        "data": "table"
                    },
                    {
                        "label": "nightstand",
                        "data": "nightstand"
                    },
                    {
                        "label": "dresser",
                        "data": "dresser"
                    },
                    {
                        "label": "corner",
                        "data": "corner"
                    }
                ]
            ],
            "statueDay": [
                "Int",
                ""
            ]
        }
        '''
        self.addSSOProperty('hadVision', bool, False, 'kitsune', 'hadVision')
        self.addSSOProperty('statueDay', int, 0, 'kitsune', 'statueDay')
        self.addSSOProperty('statueLocation', str, '""', 'kitsune', 'statueLocation', choices={
            '': '',
            'desk': 'desk',
            'bookshelf': 'bookshelf',
            'table': 'table',
            'nightstand': 'nightstand',
            'dresser': 'dresser',
            'corner': 'corner',
        })
        ##########
        # kFlags
        ##########
        self.addKFlag('KITSUNE_SHRINE_VISIT', int, 0, HGGKFlags.KITSUNE_SHRINE_VISIT)
        self.addKFlag('MET_KITSUNES', int, 0, HGGKFlags.MET_KITSUNES)
        self.addKFlag('REDHEAD_IS_FUTA', int, 0, HGGKFlags.REDHEAD_IS_FUTA)
        self.addKFlag('TOOK_KITSUNE_STATUE', int, 0, HGGKFlags.TOOK_KITSUNE_STATUE)
