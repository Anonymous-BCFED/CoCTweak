from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class OwcaNPC(BaseNPC):
    ID = 'owca'
    NAME = 'Owca'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Owca.as: public class Owca extends BaseContent implements SelfSaving, SelfDebug { @ eU81PAbss2ee2yp5bH36FaOpaac40Q5u8c0ugxJcB54ir7Tm5lt4Fla2Ufvzgsp9U1dkZ1ZH9adc8L9cb5bo69lgd42Rs3ic
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "rebeccKilled": [
                "Boolean",
                ""
            ],
            "lostPit": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('lostPit', bool, False, 'owca', 'lostPit')
        self.addSSOProperty('rebeccKilled', bool, False, 'owca', 'rebeccKilled')
        ##########
        # kFlags
        ##########
        self.addKFlag('DAYS_SINCE_LAST_DEMON_DEALINGS', int, 0, HGGKFlags.DAYS_SINCE_LAST_DEMON_DEALINGS)
        self.addKFlag('DECLINED_TO_VISIT_REBECCS_VILLAGE', int, 0, HGGKFlags.DECLINED_TO_VISIT_REBECCS_VILLAGE)
        self.addKFlag('OWCAS_ATTITUDE', int, 0, HGGKFlags.OWCAS_ATTITUDE, min=0, max=100)
        self.addKFlag('OWCA_ANGER_DISABLED', int, 0, HGGKFlags.OWCA_ANGER_DISABLED)
        self.addKFlag('OWCA_SACRIFICE_DISABLED', int, 0, HGGKFlags.OWCA_SACRIFICE_DISABLED)
        self.addKFlag('OWCA_UNLOCKED', int, 0, HGGKFlags.OWCA_UNLOCKED)
        self.addKFlag('REBECCS_LAST_PLEA', int, 0, HGGKFlags.REBECCS_LAST_PLEA)
        self.addKFlag('TIMES_IN_DEMON_PIT', int, 0, HGGKFlags.TIMES_IN_DEMON_PIT)
        self.addKFlag('TIMES_REFUSED_REBECCS_OFFER', int, 0, HGGKFlags.TIMES_REFUSED_REBECCS_OFFER)
        self.addKFlag('VAPULA_FOLLOWER', int, 0, HGGKFlags.VAPULA_FOLLOWER)
        self.addKFlag('VAPULA_HAREM_FUCK', int, 0, HGGKFlags.VAPULA_HAREM_FUCK)
        self.addKFlag('VAPULA_SUBMISSIVENESS', int, 0, HGGKFlags.VAPULA_SUBMISSIVENESS)
