from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class CalluNPC(BaseNPC):
    ID = 'callu'
    NAME = 'Callu'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Lake/CalluScene.as: public class CalluScene extends AbstractLakeContent implements Encounter { @ 1uhbh98rpeRV8qP2A2bsR3Vg8NY450eBW71xcZYcjN6NF0rwfZp3yu2zRepEdTLgSf5mVgWp3t2cKwfEEgnB8tJbhzblI2IB
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('MET_OTTERGIRL', int, 0, HGGKFlags.MET_OTTERGIRL)
