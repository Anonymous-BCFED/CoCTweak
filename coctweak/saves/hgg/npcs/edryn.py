from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class EdrynNPC(BaseNPC):
    ID = 'edryn'
    NAME = 'Edryn'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Edryn.as: public class Edryn extends TelAdreAbstractContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ f7nady4MU7lV24O1fA3Nu6fr1wYdidcDdaw1gCb4MKdmVdBo2uY71d5oOfund9m4Iga2R9AibTs0BUf43fCugCIgAi0S36pJ
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "boys": [
                "Int",
                ""
            ],
            "girls": [
                "Int",
                ""
            ],
            "herms": [
                "Int",
                ""
            ],
            "kidMet": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('boys', int, 0, 'edryn', 'boys')
        self.addSSOProperty('girls', int, 0, 'edryn', 'girls')
        self.addSSOProperty('herms', int, 0, 'edryn', 'herms')
        self.addSSOProperty('kidMet', bool, False, 'edryn', 'kidMet')
        ##########
        # kFlags
        ##########
        self.addKFlag('EDRYN_GIFT_COUNTER', int, 0, HGGKFlags.EDRYN_GIFT_COUNTER)
        self.addKFlag('EDRYN_NEEDS_TO_TALK_ABOUT_KID', int, 0, HGGKFlags.EDRYN_NEEDS_TO_TALK_ABOUT_KID)
        self.addKFlag('EDRYN_NEVER_SEE_AGAIN', int, 0, HGGKFlags.EDRYN_NEVER_SEE_AGAIN)
        self.addKFlag('EDRYN_NUMBER_OF_KIDS', int, 0, HGGKFlags.EDRYN_NUMBER_OF_KIDS)
        # PregnancyStore vag incubation counter
        self.addKFlag('EDRYN_PREGNANCY_INCUBATION', int, 0, HGGKFlags.EDRYN_PREGNANCY_INCUBATION)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('EDRYN_PREGNANCY_TYPE', int, 0, HGGKFlags.EDRYN_PREGNANCY_TYPE)
        self.addKFlag('EDRYN_PREGNANT_AND_NOT_TOLD_PC_YET', int, 0, HGGKFlags.EDRYN_PREGNANT_AND_NOT_TOLD_PC_YET)
        self.addKFlag('EDRYN_TIMES_HEL_THREESOMED', int, 0, HGGKFlags.EDRYN_TIMES_HEL_THREESOMED)
        self.addKFlag('TIMES_EATEN_EDRYN_PUSSY_RUT', int, 0, HGGKFlags.TIMES_EATEN_EDRYN_PUSSY_RUT)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('Edryn', bool, False, HGGStatusLib.Edryn, None)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.EDRYN_PREGNANCY_TYPE, HGGKFlags.EDRYN_PREGNANCY_INCUBATION)
