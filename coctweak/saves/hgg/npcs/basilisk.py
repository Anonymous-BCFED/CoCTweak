from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class BasiliskNPC(BaseNPC):
    ID = 'basilisk'
    NAME = 'Basilisk'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/HighMountains/Basilisk.as: public class Basilisk extends StareMonster { @ 8D43ye95kgWN4a92AW3SHe15fMbfDza3bbBi0Pkbfs60z0b4dSy9gN6KT17ogYK5bD21b8U86dz4Z6d3LatY8mq6HW7Iggx1
        ## from [HGG]/classes/classes/Scenes/Areas/HighMountains/BasiliskScene.as: public class BasiliskScene extends BaseContent { @ gF708AdT3dPE9zE9iYaJJ2y70G83KE7r779V2trbmj1XA75z9mLcV45Vbcyv0Uq4ZkdV3fTq0P2fXh0WxaXc8Rx2m46IH1Wu
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('BASILISK_RESISTANCE_TRACKER', int, 0, HGGKFlags.BASILISK_RESISTANCE_TRACKER, min=0, max=100, notes='How resistant you are to basilisk/cockatrice stares. Increments during combat.')
        self.addKFlag('TIMES_ENCOUNTERED_BASILISK', int, 0, HGGKFlags.TIMES_ENCOUNTERED_BASILISK)
