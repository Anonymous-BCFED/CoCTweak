from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class InfestedChameleonGirlNPC(BaseNPC):
    ID = 'infestedchameleongirl'
    NAME = 'InfestedChameleonGirl'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/InfestedChameleonGirl.as: public class InfestedChameleonGirl extends Monster { @ f9W0Vi8ny21odHOeNWfCq6Yr5ZA7FL8BdfrE3tv7WK2x06zGc9vgeagRY5gTgT448c6k3ftN9YRdcr0sIbs204bcRhfBtcWo
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/InfestedChameleonGirlScene.as: public class InfestedChameleonGirlScene extends BaseContent { @ 8IkeDFdyHcCG5EEgeo03wae02cYccwfkYcHf2Hz4537gQ2rd8uD8CQ9Er5Qj0NW8iF4oR6BC2eVaqc40c3Tl7kSc7E65c6oy
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('TIMES_MET_CHAMELEON', int, 0, HGGKFlags.TIMES_MET_CHAMELEON)
