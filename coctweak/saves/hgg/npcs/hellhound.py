from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class HellHoundNPC(BaseNPC):
    ID = 'hellhound'
    NAME = 'HellHound'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Mountain/HellHoundScene.as: public class HellHoundScene extends BaseContent { @ f4L7JYfXjd4lcbD5UDg3u0Jq4j08fxehobHm7epbSb4Ee0JJ8VS9SK35OeHW0dbczb7bg7VAaHA4PgdGteiv5t60ka5HH53h
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('HELLHOUND_MASTER_PROGRESS', int, 0, HGGKFlags.HELLHOUND_MASTER_PROGRESS)
