import argparse
from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from enum import Enum
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class EJojoDeadOrGone(Enum):
    ALIVE  = 0
    GONE   = 1
    DEAD   = 2
class JojoNPC(BaseNPC):
    ID = 'jojo'
    NAME = 'Jojo'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/JojoScene.as: public class JojoScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 85E5cF3jr1Ow67JdKLgEG0nUapj3XI5BL984ce94rI6KP9Ig9cS7rJ8AVc4ye2ebI5fFs27E1tM7KgaZab4jae5dGs8ZvbMs
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "forgiven": [
                "Boolean",
                "Tracks whether he has forgiven you for trying to rape him"
            ],
            "postRapeCampOffer": [
                "Boolean",
                "Tracks whether you've tried to get him to come to camp"
            ]
        }
        '''
        # Tracks whether he has forgiven you for trying to rape him
        self.addSSOProperty('forgiven', bool, False, 'jojo', 'forgiven', notes='Tracks whether he has forgiven you for trying to rape him')
        self.addSSOProperty('postRapeCampOffer', bool, False, 'jojo', 'postRapeCampOffer', notes="Tracks whether you've tried to get him to come to camp")
        ##########
        # kFlags
        ##########
        self.addKFlag('AMILY_DISCOVERED_TENTATLE_JOJO', int, 0, HGGKFlags.AMILY_DISCOVERED_TENTATLE_JOJO)
        self.addKFlag('AMILY_MET_PURE_JOJO', int, 0, HGGKFlags.AMILY_MET_PURE_JOJO)
        self.addKFlag('AMILY_PISSED_PC_CORRUPED_JOJO', int, 0, HGGKFlags.AMILY_PISSED_PC_CORRUPED_JOJO)
        self.addKFlag('AMILY_X_JOJO_COOLDOWN', int, 0, HGGKFlags.AMILY_X_JOJO_COOLDOWN)
        self.addKFlag('DISABLED_JOJO_RAPE', int, 0, HGGKFlags.DISABLED_JOJO_RAPE)
        self.addKFlag('FOLLOWER_AT_FARM_JOJO', int, 0, HGGKFlags.FOLLOWER_AT_FARM_JOJO)
        self.addKFlag('FOLLOWER_AT_FARM_JOJO_GIBS_DRAFT', int, 0, HGGKFlags.FOLLOWER_AT_FARM_JOJO_GIBS_DRAFT)
        self.addKFlag('FOLLOWER_PRODUCTION_JOJO', int, 0, HGGKFlags.FOLLOWER_PRODUCTION_JOJO)
        self.addKFlag('JOJO_ANAL_CATCH_COUNTER', int, 0, HGGKFlags.JOJO_ANAL_CATCH_COUNTER)
        self.addKFlag('JOJO_ANAL_XP', int, 0, HGGKFlags.JOJO_ANAL_XP)
        self.addKFlag('JOJO_BIMBO_STATE', int, 0, HGGKFlags.JOJO_BIMBO_STATE)
        self.addKFlag('JOJO_BLOWJOB_XP', int, 0, HGGKFlags.JOJO_BLOWJOB_XP)
        # PregnancyStore butt pregnancy type and notice counter
        self.addKFlag('JOJO_BUTT_PREGNANCY_TYPE', int, 0, HGGKFlags.JOJO_BUTT_PREGNANCY_TYPE)
        self.addKFlag('JOJO_COCK_MILKING_COOLDOWN', int, 0, HGGKFlags.JOJO_COCK_MILKING_COOLDOWN)
        self.addKFlag('JOJO_COCK_MILKING_COUNTER', int, 0, HGGKFlags.JOJO_COCK_MILKING_COUNTER)
        self.addKFlag('JOJO_DEAD_OR_GONE', int, EJojoDeadOrGone(0), HGGKFlags.JOJO_DEAD_OR_GONE, enumType=EJojoDeadOrGone, choices={
            0: 'Alive',
            1: 'Gone',
            2: 'Dead',
        })
        # PregnancyStore butt incubation counter
        self.addKFlag('JOJO_EGGCUBATE_COUNT', int, 0, HGGKFlags.JOJO_EGGCUBATE_COUNT)
        self.addKFlag('JOJO_LAST_MEDITATION', int, 0, HGGKFlags.JOJO_LAST_MEDITATION)
        self.addKFlag('JOJO_LITTERS', int, 0, HGGKFlags.JOJO_LITTERS)
        self.addKFlag('JOJO_LITTERS_AMILY_REACTION_COUNTER', int, 0, HGGKFlags.JOJO_LITTERS_AMILY_REACTION_COUNTER)
        self.addKFlag('JOJO_MOVE_IN_DISABLED', int, 0, HGGKFlags.JOJO_MOVE_IN_DISABLED)
        self.addKFlag('JOJO_RATHAZUL_INTERACTION_COUNTER', int, 0, HGGKFlags.JOJO_RATHAZUL_INTERACTION_COUNTER)
        self.addKFlag('JOJO_SEX_COUNTER', int, 0, HGGKFlags.JOJO_SEX_COUNTER)
        self.addKFlag('JOJO_STATUS', int, 0, HGGKFlags.JOJO_STATUS, notes='Number of times Jojo was raped. When this reaches 6, you can take Jojo as a slave.')
        self.addKFlag('JOJO_TATTOO_BUTT', int, 0, HGGKFlags.JOJO_TATTOO_BUTT)
        self.addKFlag('JOJO_TATTOO_COLLARBONE', int, 0, HGGKFlags.JOJO_TATTOO_COLLARBONE)
        self.addKFlag('JOJO_TATTOO_LOWERBACK', int, 0, HGGKFlags.JOJO_TATTOO_LOWERBACK)
        self.addKFlag('JOJO_TATTOO_SHOULDERS', int, 0, HGGKFlags.JOJO_TATTOO_SHOULDERS)
        self.addKFlag('JOJO_VAGINAL_CATCH_COUNTER', int, 0, HGGKFlags.JOJO_VAGINAL_CATCH_COUNTER)
        self.addKFlag('MINERVA_PURIFICATION_JOJO_TALKED', int, 0, HGGKFlags.MINERVA_PURIFICATION_JOJO_TALKED)
        self.addKFlag('TIMES_AMILY_AND_JOJO_PLAYED_TIMES', int, 0, HGGKFlags.TIMES_AMILY_AND_JOJO_PLAYED_TIMES)
        self.addKFlag('TIMES_EGGED_JOJO', int, 0, HGGKFlags.TIMES_EGGED_JOJO)
        self.addKFlag('TIMES_TALKED_WITH_JOJO', int, 0, HGGKFlags.TIMES_TALKED_WITH_JOJO)
        self.addKFlag('TIMES_TRAINED_WITH_JOJO', int, 0, HGGKFlags.TIMES_TRAINED_WITH_JOJO)
        self.addKFlag('UNLOCKED_JOJO_TRAINING', int, 0, HGGKFlags.UNLOCKED_JOJO_TRAINING)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('EverRapedJojo', bool, False, HGGStatusLib.EverRapedJojo, None)
        self.addSFXProperty('EverRapedJojo:0', int, 0, HGGStatusLib.EverRapedJojo, 0, min=0, notes='Increments every time you rape Jojo.')
        self.addSFXProperty('JojoMeditationCount', bool, False, HGGStatusLib.JojoMeditationCount, None)
        self.addSFXProperty('JojoMeditationCount:0', int, 0, HGGStatusLib.JojoMeditationCount, 0, min=0, notes='Increments every time you meditate with Jojo.')
        self.addSFXProperty('JojoNightWatch', bool, False, HGGStatusLib.JojoNightWatch, None)
        self.addSFXProperty('JojoTFOffer', bool, False, HGGStatusLib.JojoTFOffer, None)
        self.addSFXProperty('NoJojo', bool, False, HGGStatusLib.NoJojo, None)
        self.addSFXProperty('PureCampJojo', bool, False, HGGStatusLib.PureCampJojo, None)
        self.addSFXProperty('TentacleJojo', bool, False, HGGStatusLib.TentacleJojo, None)
        ###################
        # PregnancyStores
        ###################
        self.analPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.JOJO_BUTT_PREGNANCY_TYPE, HGGKFlags.JOJO_EGGCUBATE_COUNT)

    def register_cli_kill(self, subp: argparse.ArgumentParser) -> None:
        p_kill = subp.add_parser('kill', help='Kill Jojo. **THIS MAY BREAK THINGS IF HE\'S A FOLLOWER OR WHATEVER.**')
        p_kill.set_defaults(npc_cmd=self.cmd_kill)
    def cmd_kill(self, args: argparse.Namespace) -> bool:
        self.verbose_set = True
        self.JOJO_DEAD_OR_GONE = 2
        self.verbose_set = False
        log.info('Jojo marked as dead via Amily.')
        return True

    def register_cli_revive(self, subp: argparse.ArgumentParser) -> None:
        p_kill = subp.add_parser('revive', help='Un-kill Jojo.')
        p_kill.set_defaults(npc_cmd=self.cmd_revive)
    def cmd_revive(self, args: argparse.Namespace) -> bool:
        self.verbose_set = True
        self.JOJO_DEAD_OR_GONE = 0
        self.verbose_set = False
        log.info('Jojo revived.')
        return True

    def register_cli_cleanse(self, subp: argparse.ArgumentParser) -> None:
        p = subp.add_parser('cleanse', help='De-corrupt Jojo.')
        p.set_defaults(npc_cmd=self.cmd_cleanse)
    def cmd_cleanse(self, args: argparse.Namespace) -> bool:
        self.verbose_set = True
        self.JOJO_STATUS = 0
        self.verbose_set = False
        log.info('Jojo cleansed.')
        return True
