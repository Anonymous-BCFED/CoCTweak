from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class HeckelNPC(BaseNPC):
    ID = 'heckel'
    NAME = 'Heckel'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Heckel.as: public class Heckel extends TelAdreAbstractContent { @ 21tgzA1sGdazeOJ7cNcpC1uugdo4WIb6q8VS86Da140nl8kM9La7Mi7KCh1L4uH3v62OdfVJ4KUdUXekreS7eVK5Q65jQ7HM
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('MET_HECKEL', int, 0, HGGKFlags.MET_HECKEL)
        self.addKFlag('TIMES_DOMMED_HECKEL', int, 0, HGGKFlags.TIMES_DOMMED_HECKEL)
        self.addKFlag('TIMES_FUCKED_HECKEL_ANAL', int, 0, HGGKFlags.TIMES_FUCKED_HECKEL_ANAL)
        self.addKFlag('TIMES_FUCKED_HECKEL_BLOWJOB', int, 0, HGGKFlags.TIMES_FUCKED_HECKEL_BLOWJOB)
        self.addKFlag('TIMES_LOST_HECKEL_DOM_CHALLENGE', int, 0, HGGKFlags.TIMES_LOST_HECKEL_DOM_CHALLENGE)
