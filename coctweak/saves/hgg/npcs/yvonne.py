from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class YvonneNPC(BaseNPC):
    ID = 'yvonne'
    NAME = 'Yvonne'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/YvonneArmorShop.as: public class YvonneArmorShop extends Shop { @ 3Ot3bz6vwfv18pccNW2Mx0B8bkv3Rb1ED5Jk6Hz4ZIdIW79B6mY00M9P31oJgbwcI98JweWEguJ98u9DSh0z4CL9YQ1x46FD
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('YVONNE_FUCK_COUNTER', int, 0, HGGKFlags.YVONNE_FUCK_COUNTER)
