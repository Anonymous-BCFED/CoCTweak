from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class PabloNPC(BaseNPC):
    ID = 'pablo'
    NAME = 'Pablo'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/PabloScene.as: public class PabloScene extends TelAdreAbstractContent { @ eEF0ZSfLM3eRa65fDz8Xzbhm3pT4hX0Oe9lhahrbo5gzs5SEgwP4pl1qv3wm4Kj38M83acbW1dlcz80Pkf5z3qK4SZ9JS0b8
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('PABLO_AFFECTION', int, 0, HGGKFlags.PABLO_AFFECTION)
        self.addKFlag('PABLO_FREAKED_OUT_OVER_WORMS', int, 0, HGGKFlags.PABLO_FREAKED_OUT_OVER_WORMS)
        self.addKFlag('PABLO_GOT_DICKED_ANALLY', int, 0, HGGKFlags.PABLO_GOT_DICKED_ANALLY)
        self.addKFlag('PABLO_GOT_DICKED_VAGINALLY', int, 0, HGGKFlags.PABLO_GOT_DICKED_VAGINALLY)
        self.addKFlag('PABLO_MET', int, 0, HGGKFlags.PABLO_MET)
        self.addKFlag('PABLO_SECRET_LEARNED', int, 0, HGGKFlags.PABLO_SECRET_LEARNED)
        self.addKFlag('PABLO_USED_YOUR_PUSSY', int, 0, HGGKFlags.PABLO_USED_YOUR_PUSSY)
        self.addKFlag('PABLO_WORKOUT_COUNTER', int, 0, HGGKFlags.PABLO_WORKOUT_COUNTER)
