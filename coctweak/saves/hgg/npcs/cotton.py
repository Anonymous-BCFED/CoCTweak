from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class CottonNPC(BaseNPC):
    ID = 'cotton'
    NAME = 'Cotton'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Cotton.as: public class Cotton extends TelAdreAbstractContent implements TimeAwareInterface, VaginalPregnancy { @ aqOeV70LKe68cqrh09gte0moenB2zl8jMgWy9kPerX8k4fGW9LR0OcgZVdbXbCpev5aY5aKm6kk8eS1yj4XN6J711Qh2n4w4
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('COTTON_BREAKFAST_CLUB', int, 0, HGGKFlags.COTTON_BREAKFAST_CLUB)
        self.addKFlag('COTTON_CONTRACEPTION_TALK', int, 0, HGGKFlags.COTTON_CONTRACEPTION_TALK)
        self.addKFlag('COTTON_HERBS_OFF', int, 0, HGGKFlags.COTTON_HERBS_OFF)
        self.addKFlag('COTTON_KID_COUNT', int, 0, HGGKFlags.COTTON_KID_COUNT)
        self.addKFlag('COTTON_KNOCKED_UP_PC_AND_TALK_HAPPENED', int, 0, HGGKFlags.COTTON_KNOCKED_UP_PC_AND_TALK_HAPPENED)
        self.addKFlag('COTTON_MET_FUCKED', int, 0, HGGKFlags.COTTON_MET_FUCKED)
        self.addKFlag('COTTON_OLDEST_KID_AGE', int, 0, HGGKFlags.COTTON_OLDEST_KID_AGE)
        self.addKFlag('COTTON_OLDEST_KID_GENDER', int, 0, HGGKFlags.COTTON_OLDEST_KID_GENDER)
        # PregnancyStore vag incubation counter
        self.addKFlag('COTTON_PREGNANCY_INCUBATION', int, 0, HGGKFlags.COTTON_PREGNANCY_INCUBATION)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('COTTON_PREGNANCY_TYPE', int, 0, HGGKFlags.COTTON_PREGNANCY_TYPE)
        self.addKFlag('COTTON_UNUSUAL_YOGA_BOOK_TRACKER', int, 0, HGGKFlags.COTTON_UNUSUAL_YOGA_BOOK_TRACKER)
        self.addKFlag('PC_IS_A_DEADBEAT_COTTON_DAD', int, 0, HGGKFlags.PC_IS_A_DEADBEAT_COTTON_DAD)
        self.addKFlag('PC_IS_A_GOOD_COTTON_DAD', int, 0, HGGKFlags.PC_IS_A_GOOD_COTTON_DAD)
        self.addKFlag('TIMES_HAD_YOGA', int, 0, HGGKFlags.TIMES_HAD_YOGA)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.COTTON_PREGNANCY_TYPE, HGGKFlags.COTTON_PREGNANCY_INCUBATION)
