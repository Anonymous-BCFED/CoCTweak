from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class BakeryNPC(BaseNPC):
    ID = 'bakery'
    NAME = 'Bakery'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/BakeryScene.as: public class BakeryScene extends TelAdreAbstractContent { @ 9dy7kfaly0rx9jI6OxdR69Gf81Z0LX6dJ28hdRogGb2grd9ldGh7OpbrAdVX6uS15udo50Wv1rz6Dd5M8cU09oodnJ7RY2fS
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('MINOTAUR_CUM_ECLAIR_UNLOCKED', int, 0, HGGKFlags.MINOTAUR_CUM_ECLAIR_UNLOCKED)
        self.addKFlag('MINO_CHEF_BAKERY_PROC_COUNTER', int, 0, HGGKFlags.MINO_CHEF_BAKERY_PROC_COUNTER)
        self.addKFlag('MINO_CHEF_TALKED_RED_RIVER_ROOT', int, 0, HGGKFlags.MINO_CHEF_TALKED_RED_RIVER_ROOT)
        self.addKFlag('TIMES_VISITED_BAKERY', int, 0, HGGKFlags.TIMES_VISITED_BAKERY)
