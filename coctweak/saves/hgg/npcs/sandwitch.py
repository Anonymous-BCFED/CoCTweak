from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class SandWitchNPC(BaseNPC):
    ID = 'sandwitch'
    NAME = 'SandWitch'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Desert/SandWitchScene.as: public class SandWitchScene extends BaseContent implements TimeAwareInterface { @ 3RUg3F5rQcWicRHfVTgQWbEo2qY2nHd8edx94GN5at0q61Vv3K31GM6o8bazfus7AQ4JE1TE5hdgk98Wgd1V8ZpdUMe4vgrA
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('CHILD_LOSSES_SANDWITCH', int, 0, HGGKFlags.CHILD_LOSSES_SANDWITCH)
        # PregnancyStore vag incubation counter
        self.addKFlag('EGG_WITCH_COUNTER', int, 0, HGGKFlags.EGG_WITCH_COUNTER)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('EGG_WITCH_TYPE', int, 0, HGGKFlags.EGG_WITCH_TYPE)
        self.addKFlag('SANDWITCH_SERVICED', int, 0, HGGKFlags.SANDWITCH_SERVICED)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.EGG_WITCH_TYPE, HGGKFlags.EGG_WITCH_COUNTER)
