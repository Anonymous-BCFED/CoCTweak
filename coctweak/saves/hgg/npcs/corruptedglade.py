from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class CorruptedGladeNPC(BaseNPC):
    ID = 'corruptedglade'
    NAME = 'CorruptedGlade'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/CorruptedGlade.as: public class CorruptedGlade extends BaseContent implements TimeAwareInterface, Encounter { @ f7N1Iybxx4lZ3HFdZn5aR89I90P72KcQM3gK9lpcXVcMT0Sj2xY2yebRpgrZ7HU4WT8e8e320bm7ama2MdbN9yj9FTdrI1u9
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('CORRUPTED_GLADES_DESTROYED', int, 0, HGGKFlags.CORRUPTED_GLADES_DESTROYED, min=0, max=100, notes='How many glades have been destroyed. 100=Extinct.')
