from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class CinnabarNPC(BaseNPC):
    ID = 'cinnabar'
    NAME = 'Cinnabar'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/Cinnabar.as: public class Cinnabar extends BazaarAbstractContent { @ dSS3D7fkacH3b0C5eY8jC1UbcW7dn667y6aeauA0kVbLsh0qeJW21v6Tug3c6j25se92NeIWcEXgAB38ycU12uO9zP9HWg4c
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('CINNABAR_FUCKED_CORRUPT_PREVIOUSLY', int, 0, HGGKFlags.CINNABAR_FUCKED_CORRUPT_PREVIOUSLY)
        self.addKFlag('CINNABAR_HOUSE_VISITED', int, 0, HGGKFlags.CINNABAR_HOUSE_VISITED)
        self.addKFlag('CINNABAR_NUMBER_ENCOUNTERS', int, 0, HGGKFlags.CINNABAR_NUMBER_ENCOUNTERS)
        self.addKFlag('CINNABAR_NUMBER_TIMES_FUCKED', int, 0, HGGKFlags.CINNABAR_NUMBER_TIMES_FUCKED)
