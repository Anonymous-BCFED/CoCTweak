from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class LoppeNPC(BaseNPC):
    ID = 'loppe'
    NAME = 'Loppe'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Loppe.as: public class Loppe extends TelAdreAbstractContent { @ 05y2wR7ii2NK5v1aUP7uk9kygCtcHu5W34JQ1BN0fXdPK94nc1BcjaflogMydRybTy9ZX2xSg1J1Hcbua6kFauq3rBg9t6z8
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('LOPPE_DENIAL_COUNTER', int, 0, HGGKFlags.LOPPE_DENIAL_COUNTER)
        self.addKFlag('LOPPE_DISABLED', int, 0, HGGKFlags.LOPPE_DISABLED)
        self.addKFlag('LOPPE_FURRY', int, 0, HGGKFlags.LOPPE_FURRY)
        self.addKFlag('LOPPE_MET', int, 0, HGGKFlags.LOPPE_MET)
        self.addKFlag('LOPPE_TIMES_SEXED', int, 0, HGGKFlags.LOPPE_TIMES_SEXED)
        self.addKFlag('LOPPE_URTA_CHATS', int, 0, HGGKFlags.LOPPE_URTA_CHATS)
        self.addKFlag('TIMES_ASKED_LOPPE_ABOUT_LOPPE', int, 0, HGGKFlags.TIMES_ASKED_LOPPE_ABOUT_LOPPE)
