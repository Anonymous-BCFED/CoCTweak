from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class DominikaNPC(BaseNPC):
    ID = 'dominika'
    NAME = 'Dominika'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Dominika.as: public class Dominika extends TelAdreAbstractContent { @ 3kMaQsfv71b36DP4Jv7gua5vbVu0DB9Ux8y7ggz4936Bk08od1a6MY1aIgFH5gFdmj8pp8uOduJ6Cu4NpdsQe3B2QcdBvgYJ
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('DOMINIKAS_SWORD_GIVEN', int, 0, HGGKFlags.DOMINIKAS_SWORD_GIVEN)
        self.addKFlag('DOMINIKA_DRAMA', int, 0, HGGKFlags.DOMINIKA_DRAMA)
        self.addKFlag('DOMINIKA_EMBARRASSED_ABOUT_MAGIC', int, 0, HGGKFlags.DOMINIKA_EMBARRASSED_ABOUT_MAGIC)
        self.addKFlag('DOMINIKA_LAST_HYPNO_SUCK_COUNT', int, 0, HGGKFlags.DOMINIKA_LAST_HYPNO_SUCK_COUNT)
        self.addKFlag('DOMINIKA_LEARNING_COOLDOWN', int, 0, HGGKFlags.DOMINIKA_LEARNING_COOLDOWN)
        self.addKFlag('DOMINIKA_SPECIAL_FOLLOWUP', int, 0, HGGKFlags.DOMINIKA_SPECIAL_FOLLOWUP)
        self.addKFlag('DOMINIKA_STAGE', int, 0, HGGKFlags.DOMINIKA_STAGE)
        self.addKFlag('DOMINIKA_SUCKED_OFF_DOGCOCKS', int, 0, HGGKFlags.DOMINIKA_SUCKED_OFF_DOGCOCKS)
        self.addKFlag('DOMINIKA_SUCKED_OFF_HORSECOCKS', int, 0, HGGKFlags.DOMINIKA_SUCKED_OFF_HORSECOCKS)
        self.addKFlag('DOMINIKA_SUCKED_OFF_LARGE_COCKS', int, 0, HGGKFlags.DOMINIKA_SUCKED_OFF_LARGE_COCKS)
        self.addKFlag('DOMINIKA_SUCKED_OFF_SMALL_COCKS', int, 0, HGGKFlags.DOMINIKA_SUCKED_OFF_SMALL_COCKS)
        self.addKFlag('DOMINIKA_TIMES_HYPNO_BJ', int, 0, HGGKFlags.DOMINIKA_TIMES_HYPNO_BJ)
        self.addKFlag('DOMINIKA_TIMES_MULTICOCK_SLOBBERED', int, 0, HGGKFlags.DOMINIKA_TIMES_MULTICOCK_SLOBBERED)
        self.addKFlag('DOMINIKA_VAGINAL_ORAL_RECEIVED', int, 0, HGGKFlags.DOMINIKA_VAGINAL_ORAL_RECEIVED)
