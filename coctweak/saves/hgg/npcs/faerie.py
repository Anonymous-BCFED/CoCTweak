from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class FaerieNPC(BaseNPC):
    ID = 'faerie'
    NAME = 'Faerie'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/Faerie.as: public class Faerie extends BaseContent implements Encounter { @ 0QL9pJefAf3D7uE13x1yBabr6G411Hdp1blMg3V6d90Ji718eoUf6GgKi6Pe67x9DWdrm12Q1yq2Arco12z23cVfMO22821d
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('FAERIE_ENCOUNTER_DISABLED', int, 0, HGGKFlags.FAERIE_ENCOUNTER_DISABLED)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('FaerieFemFuck', bool, False, HGGStatusLib.FaerieFemFuck, None)
        self.addSFXProperty('FaerieFucked', bool, False, HGGStatusLib.FaerieFucked, None)
