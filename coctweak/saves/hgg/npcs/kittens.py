from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class KittensNPC(BaseNPC):
    ID = 'kittens'
    NAME = 'Kittens'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Kittens.as: public class Kittens extends BaseContent implements SelfSaving, SelfDebug { @ 2Id1J81YscOxh168q2fS400NdLE5OUbdkfyq3MR01vcuCgnVh1O83D9cp7A48p95FW3AvckO0dgghv1BYaRQ8KEgMBbQxbFV
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "metKittens": [
                "Boolean",
                ""
            ],
            "kittensResult": [
                "BitFlag",
                [],
                [
                    "Unused",
                    "Helped",
                    "Left",
                    "Taken",
                    "Defiant",
                    "Obedient",
                    "Watched",
                    "Spanked"
                ]
            ],
            "disabledDate": [
                "Int",
                ""
            ]
        }
        '''
        # Date of when Tel'Adre is reenabled
        self.addSSOProperty('disabledDate', int, 0, 'kittens', 'disabledDate')
        self.addSSOProperty('kittensResult', int, 0, 'kittens', 'kittensResult', notes='', bitflags={
            1: 'Unused',
            2: 'Helped',
            4: 'Left',
            8: 'Taken',
            16: 'Defiant',
            32: 'Obedient',
            64: 'Watched',
            128: 'Spanked',
        })
        self.addSSOProperty('metKittens', bool, False, 'kittens', 'metKittens')
