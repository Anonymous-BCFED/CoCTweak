from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class DullahanNPC(BaseNPC):
    ID = 'dullahan'
    NAME = 'Dullahan'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/Dullahan.as: public class Dullahan extends Monster { @ 9H55lJ7B90VN9Vg5ZkeoZdDj9iL8tY3oy4EGetqbAC91T2e04R9f688iR9ImdCM8UO6w57Jg4SGf8y43TcCJ93SaIq9VW3Vx
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/DullahanScene.as: public class DullahanScene extends BaseContent implements Encounter, SelfSaving, SelfDebug, TimeAwareInterface { @ ckXfhM6TLcdq5y41rmcAb4LbaXa8iDfqw2zCcunc0i19F4hR26f4F52sKaGR8zM7UpemwaSC8Ps6Zq4LJb64eOo7Ck4Dy9Nj
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "rimmingProgress": [
                "IntList",
                "",
                [
                    {
                        "label": "Unavailable",
                        "data": 0
                    },
                    {
                        "label": "Available",
                        "data": 1
                    },
                    {
                        "label": "Done",
                        "data": 2
                    }
                ]
            ],
            "seenBody": [
                "Boolean",
                ""
            ],
            "encounterDay": [
                "Int",
                ""
            ],
            "talkedAboutCurse": [
                "Boolean",
                ""
            ]
        }
        '''
        # 1 = available, 2 = done before
        self.addSSOProperty('rimmingProgress', int, 0, 'dullahan', 'rimmingProgress', choices={
            0: 'Unavailable',
            1: 'Available',
            2: 'Done',
        })
        self.addSSOProperty('seenBody', bool, False, 'dullahan', 'seenBody')
        self.addSSOProperty('talkedAboutCurse', bool, False, 'dullahan', 'talkedAboutCurse')
        ##########
        # kFlags
        ##########
        self.addKFlag('ACCEPTED_DULL_REQUEST', int, 0, HGGKFlags.ACCEPTED_DULL_REQUEST)
        self.addKFlag('CODEX_EPHRAIM_JOURNAL', int, 0, HGGKFlags.CODEX_EPHRAIM_JOURNAL)
        self.addKFlag('DULLAHAN_DEAD', int, 0, HGGKFlags.DULLAHAN_DEAD)
        self.addKFlag('DULLAHAN_HORSE_NAME', int, 0, HGGKFlags.DULLAHAN_HORSE_NAME)
        self.addKFlag('DULLAHAN_MET', int, 0, HGGKFlags.DULLAHAN_MET)
        self.addKFlag('DULLAHAN_RUDE', int, 0, HGGKFlags.DULLAHAN_RUDE)
        self.addKFlag('DULL_DATES', int, 0, HGGKFlags.DULL_DATES)
        self.addKFlag('TIMES_BEATEN_DULLAHAN_SPAR', int, 0, HGGKFlags.TIMES_BEATEN_DULLAHAN_SPAR)
