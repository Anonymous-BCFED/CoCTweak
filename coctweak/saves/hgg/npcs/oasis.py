from coctweak.saves._npc import BaseNPC, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class OasisNPC(BaseNPC):
    ID = 'oasis'
    NAME = 'Oasis'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Desert/Oasis.as: public class Oasis extends BaseContent { @ 7NI3HU1fhfgEcMo82sdHL7Gp4K39cX1ROd4r5LSauq8s6a53bQx91p6Rq02Pbo12LEf3Qe2kc7q4hI6fy5vfc0r7GAgM3dTN
        super().__init__(save)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('VoluntaryDemonpack:0', int, 0, HGGStatusLib.VoluntaryDemonpack, 0)
