from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class KihaNPC(BaseNPC):
    ID = 'kiha'
    NAME = 'Kiha'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/KihaFollowerScene.as: public class KihaFollowerScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ di2fGn4wy2P4cyiflH8ujaCq8ksbrPgg4bs10qO4z61LxbaucZc2Fr0Li4xI3Lb6MobCm8K87ny15j3li5ybdBgbVcat50mZ
        ## from [HGG]/classes/classes/Scenes/NPCs/KihaScene.as: public class KihaScene extends NPCAwareContent { @ 30S3njaKf59HenK4t4fhbcC242X5spdflbKR1GCg1449ScjOafI49qcgJbOc4k24AZfQH2Js11L0PEe0p6sY1cM52z0Sh2t9
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "badCookingTasted": [
                "IntList",
                "",
                [
                    {
                        "label": "0",
                        "data": 0
                    },
                    {
                        "label": "1",
                        "data": 1
                    },
                    {
                        "label": "2",
                        "data": 2
                    },
                    {
                        "label": "3",
                        "data": 3
                    }
                ]
            ],
            "flowerGifted": [
                "Boolean",
                ""
            ],
            "kidFirebreathing": [
                "Boolean",
                ""
            ]
        }
        '''
        # ```Needs documentation of what the values mean
        self.addSSOProperty('badCookingTasted', int, 0, 'kiha', 'badCookingTasted', choices={
            0: '0',
            1: '1',
            2: '2',
            3: '3',
        })
        self.addSSOProperty('flowerGifted', bool, False, 'kiha', 'flowerGifted')
        self.addSSOProperty('giftedFlower', bool, False, 'kiha', 'giftedFlower')
        self.addSSOProperty('kidFirebreathing', bool, False, 'kiha', 'kidFirebreathing')
        self.addSSOProperty('wakeUp', bool, False, 'kiha', 'wakeUp')
        ##########
        # kFlags
        ##########
        self.addKFlag('KIHA_ADMITTED_WARM_FEELINZ', int, 0, HGGKFlags.KIHA_ADMITTED_WARM_FEELINZ)
        self.addKFlag('KIHA_AFFECTION', int, 0, HGGKFlags.KIHA_AFFECTION)
        self.addKFlag('KIHA_AFFECTION_LEVEL', int, 0, HGGKFlags.KIHA_AFFECTION_LEVEL)
        self.addKFlag('KIHA_AND_HEL_WHOOPIE', int, 0, HGGKFlags.KIHA_AND_HEL_WHOOPIE)
        self.addKFlag('KIHA_CAMP_WATCH', int, 0, HGGKFlags.KIHA_CAMP_WATCH)
        self.addKFlag('KIHA_CERVIXGINITY_TAKEN', int, 0, HGGKFlags.KIHA_CERVIXGINITY_TAKEN)
        self.addKFlag('KIHA_CHILDREN_BOYS', int, 0, HGGKFlags.KIHA_CHILDREN_BOYS)
        self.addKFlag('KIHA_CHILDREN_GIRLS', int, 0, HGGKFlags.KIHA_CHILDREN_GIRLS)
        self.addKFlag('KIHA_CHILDREN_HERMS', int, 0, HGGKFlags.KIHA_CHILDREN_HERMS)
        self.addKFlag('KIHA_CHILD_MATURITY_COUNTER', int, 0, HGGKFlags.KIHA_CHILD_MATURITY_COUNTER)
        self.addKFlag('KIHA_CHOKED_OUT_PC', int, 0, HGGKFlags.KIHA_CHOKED_OUT_PC)
        self.addKFlag('KIHA_CORRUPTION_BITCH', int, 0, HGGKFlags.KIHA_CORRUPTION_BITCH)
        self.addKFlag('KIHA_DESTROYING_CORRUPTED_GLADES', int, 0, HGGKFlags.KIHA_DESTROYING_CORRUPTED_GLADES)
        self.addKFlag('KIHA_FOLLOWER', int, 0, HGGKFlags.KIHA_FOLLOWER)
        self.addKFlag('KIHA_HORSECOCK_FUCKED', int, 0, HGGKFlags.KIHA_HORSECOCK_FUCKED)
        # PregnancyStore vag incubation counter
        self.addKFlag('KIHA_INCUBATION', int, 0, HGGKFlags.KIHA_INCUBATION)
        self.addKFlag('KIHA_KILLED', int, 0, HGGKFlags.KIHA_KILLED)
        self.addKFlag('KIHA_MOVE_IN_OFFER', int, 0, HGGKFlags.KIHA_MOVE_IN_OFFER)
        self.addKFlag('KIHA_NEEDS_TO_REACT_TO_HORSECOCKING', int, 0, HGGKFlags.KIHA_NEEDS_TO_REACT_TO_HORSECOCKING)
        self.addKFlag('KIHA_NEED_SPIDER_TEXT', int, 0, HGGKFlags.KIHA_NEED_SPIDER_TEXT)
        self.addKFlag('KIHA_PREGNANCY_POTENTIAL', int, 0, HGGKFlags.KIHA_PREGNANCY_POTENTIAL)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('KIHA_PREGNANCY_TYPE', int, 0, HGGKFlags.KIHA_PREGNANCY_TYPE)
        self.addKFlag('KIHA_TALK_STAGE', int, 0, HGGKFlags.KIHA_TALK_STAGE)
        self.addKFlag('KIHA_TOLL', int, 0, HGGKFlags.KIHA_TOLL)
        self.addKFlag('KIHA_TOLL_DURATION', int, 0, HGGKFlags.KIHA_TOLL_DURATION)
        self.addKFlag('KIHA_UNDERGARMENTS', int, 0, HGGKFlags.KIHA_UNDERGARMENTS)
        self.addKFlag('PC_WIN_LAST_KIHA_FIGHT', int, 0, HGGKFlags.PC_WIN_LAST_KIHA_FIGHT)
        self.addKFlag('TIMES_KIHA_ANALED', int, 0, HGGKFlags.TIMES_KIHA_ANALED)
        self.addKFlag('TIMES_MET_KIHA', int, 0, HGGKFlags.TIMES_MET_KIHA)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.KIHA_PREGNANCY_TYPE, HGGKFlags.KIHA_INCUBATION)
