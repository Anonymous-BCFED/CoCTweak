from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class ArianNPC(BaseNPC):
    ID = 'arian'
    NAME = 'Arian'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/ArianScene.as: public class ArianScene extends NPCAwareContent implements TimeAwareInterface { @ 91C2pP2j2bvL7qZbdM7Xs7x3bK69WnddHak88hV8uWcdc8IS3MC7Ez1oVc1a4gR9Pe2HJejRcZOcD7fFj8Rw06x32l7H94CN
        '''
        DEFAULTS:
            ## from [HGG]/classes/classes/Scenes/NPCs/ArianScene.as: private function helpArianWhenYouMeetHim():void { @ d6KbZY4nt0Apaup9k88Ijgrq8zt2Ao3o9gKWdWgbK3ey7dE41x9cvv5NDc5cg6beLn9uggLy5CFbyc3T5gGfdHsgK8ao07Il
            flags[kFLAGS.ARIAN_COCK_SIZE] = 1;
            flags[kFLAGS.ARIAN_CAPACITY] = 50;
        '''
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('ARIAN_ANAL_XP', int, 0, HGGKFlags.ARIAN_ANAL_XP)
        self.addKFlag('ARIAN_ASS_CHAT', int, 0, HGGKFlags.ARIAN_ASS_CHAT)
        self.addKFlag('ARIAN_BREASTS', int, 0, HGGKFlags.ARIAN_BREASTS)
        self.addKFlag('ARIAN_CAPACITY', int, 50, HGGKFlags.ARIAN_CAPACITY)
        self.addKFlag('ARIAN_COCK_SIZE', int, 1, HGGKFlags.ARIAN_COCK_SIZE, min=1)
        self.addKFlag('ARIAN_DOUBLE_COCK', int, 0, HGGKFlags.ARIAN_DOUBLE_COCK)
        self.addKFlag('ARIAN_DOUBLE_PENETRATION_CHAT', int, 0, HGGKFlags.ARIAN_DOUBLE_PENETRATION_CHAT)
        self.addKFlag('ARIAN_EGG_CHAT', int, 0, HGGKFlags.ARIAN_EGG_CHAT)
        self.addKFlag('ARIAN_EGG_COLOR', str, '', HGGKFlags.ARIAN_EGG_COLOR)
        self.addKFlag('ARIAN_EGG_COUNTER', int, 0, HGGKFlags.ARIAN_EGG_COUNTER)
        self.addKFlag('ARIAN_EGG_EVENT', int, 0, HGGKFlags.ARIAN_EGG_EVENT, min=0, max=30, notes='Counts up to 30 at 23h each day, at which point Arian will offer to lay eggs.')
        self.addKFlag('ARIAN_FIRST_REPTILUM', int, 0, HGGKFlags.ARIAN_FIRST_REPTILUM)
        self.addKFlag('ARIAN_FOLLOWER', int, 0, HGGKFlags.ARIAN_FOLLOWER, intbool=True)
        self.addKFlag('ARIAN_HAS_BLOWN', int, 0, HGGKFlags.ARIAN_HAS_BLOWN)
        # Higher is better.
        self.addKFlag('ARIAN_HEALTH', int, 0, HGGKFlags.ARIAN_HEALTH, min=0, max=100)
        self.addKFlag('ARIAN_HERM_CHAT', int, 0, HGGKFlags.ARIAN_HERM_CHAT)
        self.addKFlag('ARIAN_LESSONS', int, 0, HGGKFlags.ARIAN_LESSONS)
        self.addKFlag('ARIAN_MORNING', int, 0, HGGKFlags.ARIAN_MORNING)
        # -1 = disabled, 1 = helped.
        self.addKFlag('ARIAN_PARK', int, 0, HGGKFlags.ARIAN_PARK, choices={
            -1: 'Disabled Arian',
            0: "Didn't meet Arian yet",
            1: 'Met Arian at the Park',
        })
        self.addKFlag('ARIAN_S_DIALOGUE', int, 0, HGGKFlags.ARIAN_S_DIALOGUE)
        self.addKFlag('ARIAN_TREATMENT', int, 0, HGGKFlags.ARIAN_TREATMENT)
        self.addKFlag('ARIAN_VAGINA', int, 0, HGGKFlags.ARIAN_VAGINA)
        self.addKFlag('ARIAN_VIRGIN', int, 0, HGGKFlags.ARIAN_VIRGIN, intbool=True)
        self.addKFlag('NOT_HELPED_ARIAN_TODAY', int, 0, HGGKFlags.NOT_HELPED_ARIAN_TODAY)
        self.addKFlag('TIMES_ARIAN_DILDOED', int, 0, HGGKFlags.TIMES_ARIAN_DILDOED)
