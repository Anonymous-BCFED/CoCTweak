from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class LumberjackNPC(BaseNPC):
    ID = 'lumberjack'
    NAME = 'Lumberjack'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/LumberjackScene.as: public class LumberjackScene  extends BaseContent implements Encounter, SelfSaving, SelfDebug { @ 6TJ1s2bZT1mB7wadn45yJdD97Pl7a02cZ9qR3in1t72jZdvRen69q7aGR1AY5ax0XdeGO88D5wD4Ho21z6D776w8Nl2C42sj
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "encountered": [
                "Boolean",
                ""
            ],
            "aggressive": [
                "Boolean",
                ""
            ],
            "freedSucc": [
                "Boolean",
                ""
            ],
            "executed": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('aggressive', bool, False, 'lumberjack', 'aggressive', notes='Whether you were aggressive towards him')
        self.addSSOProperty('encountered', bool, False, 'lumberjack', 'encountered', notes='Have you encountered him?')
        self.addSSOProperty('executed', bool, False, 'lumberjack', 'executed', notes='Did you kill him?')
        self.addSSOProperty('freedSucc', bool, False, 'lumberjack', 'freedSucc', notes='Did you free the succubus?')
