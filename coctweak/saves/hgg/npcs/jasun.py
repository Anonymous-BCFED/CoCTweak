from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class JasunNPC(BaseNPC):
    ID = 'jasun'
    NAME = 'Jasun'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Jasun.as: public class Jasun extends TelAdreAbstractContent { @ 9ur0X0cfhdz5fwzd2C1Z2dTK9EGh1A3IY4UJ9571w81b3fb53N5dgO6o11ht2vM7CH1gJ53E6bwdgN1xo1z15zy8uDgEM3y9
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('JASUN_FUCKED_COUNTER', int, 0, HGGKFlags.JASUN_FUCKED_COUNTER)
        self.addKFlag('JASUN_MET', int, 0, HGGKFlags.JASUN_MET)
        self.addKFlag('JASUN_NAME_LEARNED', int, 0, HGGKFlags.JASUN_NAME_LEARNED)
