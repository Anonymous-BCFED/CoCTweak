from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class AntsNPC(BaseNPC):
    ID = 'ants'
    ALIASES = ['phylla']
    NAME = 'Ants'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Desert/AntsScene.as: public class AntsScene extends BaseContent implements TimeAwareInterface { @ gn9es4bSqciH2H032XcHLb798H71CNbq04GP0SdgaW7it5Ex7hX9DY7HlbL85lJ35odwm5mf0CN5wpejdbYl9LQ7ttbS785x
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('ANTS_BIRTHED_FROM_LICKING', int, 0, HGGKFlags.ANTS_BIRTHED_FROM_LICKING)
        self.addKFlag('ANTS_PC_BEAT_GNOLL', int, 0, HGGKFlags.ANTS_PC_BEAT_GNOLL)
        self.addKFlag('ANTS_PC_FAILED_PHYLLA', int, 0, HGGKFlags.ANTS_PC_FAILED_PHYLLA)
        self.addKFlag('ANTS_PC_LOST_TO_GNOLL', int, 0, HGGKFlags.ANTS_PC_LOST_TO_GNOLL)
        self.addKFlag('ANT_ARENA_LOSSES', int, 0, HGGKFlags.ANT_ARENA_LOSSES)
        self.addKFlag('ANT_ARENA_WINS', int, 0, HGGKFlags.ANT_ARENA_WINS)
        self.addKFlag('ANT_COLONY_KEPT_HIDDEN', int, 0, HGGKFlags.ANT_COLONY_KEPT_HIDDEN)
        self.addKFlag('ANT_KIDS', int, 0, HGGKFlags.ANT_KIDS)
        self.addKFlag('ANT_WAIFU', int, 0, HGGKFlags.ANT_WAIFU)
        self.addKFlag('DAYS_PHYLLA_HAS_SPENT_BIRTHING', int, 0, HGGKFlags.DAYS_PHYLLA_HAS_SPENT_BIRTHING)
        self.addKFlag('DAYS_PHYLLA_IN_CAMP', int, 0, HGGKFlags.DAYS_PHYLLA_IN_CAMP)
        self.addKFlag('DIDNT_FUCK_PHYLLA_ON_RECRUITMENT', int, 0, HGGKFlags.DIDNT_FUCK_PHYLLA_ON_RECRUITMENT)
        self.addKFlag('MET_ANT_ARENA', int, 0, HGGKFlags.MET_ANT_ARENA)
        self.addKFlag('MET_ANT_ARENA_GNOLL', int, 0, HGGKFlags.MET_ANT_ARENA_GNOLL)
        self.addKFlag('PC_READY_FOR_ANT_COLONY_CHALLENGE', int, 0, HGGKFlags.PC_READY_FOR_ANT_COLONY_CHALLENGE)
        self.addKFlag('PHYLLA_BLOWJOBS', int, 0, HGGKFlags.PHYLLA_BLOWJOBS)
        self.addKFlag('PHYLLA_CAMP_VISITS', int, 0, HGGKFlags.PHYLLA_CAMP_VISITS)
        self.addKFlag('PHYLLA_CAPACITY', int, 0, HGGKFlags.PHYLLA_CAPACITY)
        self.addKFlag('PHYLLA_COOLDOWN', int, 0, HGGKFlags.PHYLLA_COOLDOWN)
        self.addKFlag('PHYLLA_DRIDER_BABIES_COUNT', int, 0, HGGKFlags.PHYLLA_DRIDER_BABIES_COUNT)
        # PregnancyStore vag incubation counter
        self.addKFlag('PHYLLA_DRIDER_INCUBATION', int, 0, HGGKFlags.PHYLLA_DRIDER_INCUBATION)
        self.addKFlag('PHYLLA_EGG_LAYING', int, 0, HGGKFlags.PHYLLA_EGG_LAYING)
        self.addKFlag('PHYLLA_FUCKS', int, 0, HGGKFlags.PHYLLA_FUCKS)
        self.addKFlag('PHYLLA_GEMS_HUNTED_TODAY', int, 0, HGGKFlags.PHYLLA_GEMS_HUNTED_TODAY)
        self.addKFlag('PHYLLA_INHERITED_KNOWLEDGE', int, 0, HGGKFlags.PHYLLA_INHERITED_KNOWLEDGE)
        self.addKFlag('PHYLLA_IZMA_TALK', int, 0, HGGKFlags.PHYLLA_IZMA_TALK)
        self.addKFlag('PHYLLA_SAVED', int, 0, HGGKFlags.PHYLLA_SAVED)
        self.addKFlag('PHYLLA_STAY_HOME', int, 0, HGGKFlags.PHYLLA_STAY_HOME)
        self.addKFlag('PHYLLA_STONES_HUNTED_TODAY', int, 0, HGGKFlags.PHYLLA_STONES_HUNTED_TODAY)
        self.addKFlag('PHYLLA_TIMES_DRIDER_EGG_LAYED', int, 0, HGGKFlags.PHYLLA_TIMES_DRIDER_EGG_LAYED)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('PHYLLA_VAGINAL_PREGNANCY_TYPE', int, 0, HGGKFlags.PHYLLA_VAGINAL_PREGNANCY_TYPE)
        self.addKFlag('TALKED_WITH_PHYLLA_ABOUT_HISTORY', int, 0, HGGKFlags.TALKED_WITH_PHYLLA_ABOUT_HISTORY)
        self.addKFlag('TIMES_CORRUPT_FEMALE_ANT_ORGY', int, 0, HGGKFlags.TIMES_CORRUPT_FEMALE_ANT_ORGY)
        self.addKFlag('TIMES_CORRUPT_MALE_ANT_ORGY', int, 0, HGGKFlags.TIMES_CORRUPT_MALE_ANT_ORGY)
        self.addKFlag('TIMES_EGG_IMPREGNATING_PHYLLA', int, 0, HGGKFlags.TIMES_EGG_IMPREGNATING_PHYLLA)
        self.addKFlag('TIMES_LINKED_BJ_SUCK', int, 0, HGGKFlags.TIMES_LINKED_BJ_SUCK)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.PHYLLA_VAGINAL_PREGNANCY_TYPE, HGGKFlags.PHYLLA_DRIDER_INCUBATION)
