from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class LiliumNPC(BaseNPC):
    ID = 'lilium'
    NAME = 'Lilium'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/Lilium.as: public class Lilium extends BazaarAbstractContent { @ 66MfxsfEp5YAaAagPj55f9kK2JVb3z4OT57s9O181f7D76mbeaz5yn5dL39xa3H844b3w2sk8rafzObNL0xQcA72943Ia3eY
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('LILIUM_TIMES_TALKED', int, 0, HGGKFlags.LILIUM_TIMES_TALKED)
