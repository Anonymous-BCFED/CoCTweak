from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class BogTempleNPC(BaseNPC):
    ID = 'bogtemple'
    NAME = 'BogTemple'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/BogTemple.as: public class BogTemple extends BaseContent implements SelfSaving, SelfDebug { @ 3O93SKdcNemG0Oj5JW4KhgjKd2Xaat0ZL4Nk1bp9iT3hegHFe5B3EE5BH3Zm0mT0ZvdA1dUD5xibWw2ge5OH9cBaeF9os1gX
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "foundTemple": [
                "Boolean"
            ],
            "shieldTaken": [
                "Boolean"
            ],
            "inspection": [
                "BitFlag",
                [
                    "What inspect options you've seen"
                ],
                [
                    "Unused",
                    "Sculpture",
                    "Altar",
                    "Statuettes",
                    "Books",
                    "Hole",
                    "Puddles",
                    "Balcony",
                    "Entrance"
                ]
            ],
            "seenBalcony": [
                "Boolean",
                "Whether you've flown up to the top"
            ],
            "excludeExplore": [
                "Int",
                "The temple cannot be encountered at this total time"
            ],
            "timesVisited": [
                "Int",
                "Total temple encounters"
            ],
            "timesPrayed": [
                "Int"
            ],
            "timesBathed": [
                "Int"
            ]
        }
        '''
        self.addSSOProperty('excludeExplore', int, 0, 'bogtemple', 'excludeExplore', notes='The temple cannot be encountered at this total time')
        self.addSSOProperty('foundTemple', bool, False, 'bogtemple', 'foundTemple')
        self.addSSOProperty('inspection', int, 0, 'bogtemple', 'inspection', notes="What inspect options you've seen", bitflags={
            1: 'Unused',
            2: 'Sculpture',
            4: 'Altar',
            8: 'Statuettes',
            16: 'Books',
            32: 'Hole',
            64: 'Puddles',
            128: 'Balcony',
            256: 'Entrance',
        })
        self.addSSOProperty('seenBalcony', bool, False, 'bogtemple', 'seenBalcony', notes="Whether you've flown up to the top")
        self.addSSOProperty('shieldTaken', bool, False, 'bogtemple', 'shieldTaken')
        self.addSSOProperty('timesBathed', int, 0, 'bogtemple', 'timesBathed')
        self.addSSOProperty('timesPrayed', int, 0, 'bogtemple', 'timesPrayed')
        self.addSSOProperty('timesVisited', int, 0, 'bogtemple', 'timesVisited', notes='Total temple encounters')
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('TempleBlessing', bool, False, HGGStatusLib.TempleBlessing, None)
