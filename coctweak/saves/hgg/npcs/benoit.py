from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class BenoitNPC(BaseNPC):
    ID = 'benoit'
    NAME = 'Benoit'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/Benoit.as: public class Benoit extends BazaarAbstractContent { @ 7ii05b3An1gPh1QghQ7gjgRvd8Z0cS2hqee57115N1f9rgez6Uq0w7e1G7GK9Q4bnZ0Je5bXfjOfui5lR7I84J5bCpgsx2cv
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('BENOIT_1', str, '', HGGKFlags.BENOIT_1, notes="The first item available for sale at Benoit's or the pawn shop.")
        self.addKFlag('BENOIT_2', str, '', HGGKFlags.BENOIT_2, notes="The second item available for sale at Benoit's or the pawn shop.")
        self.addKFlag('BENOIT_3', str, '', HGGKFlags.BENOIT_3, notes="The third item available for sale at Benoit's or the pawn shop.")
        self.addKFlag('BENOIT_AFFECTION', int, 0, HGGKFlags.BENOIT_AFFECTION, min=0, max=100)
        self.addKFlag('BENOIT_BASIL_EYES_GRANTED', int, 0, HGGKFlags.BENOIT_BASIL_EYES_GRANTED, min=0, notes="How many times you've been granted Basilisk Eyes.")
        self.addKFlag('BENOIT_BIRTH_DELAY', None, 0, HGGKFlags.BENOIT_BIRTH_DELAY, unused=True)
        self.addKFlag('BENOIT_CLOCK_ALARM', int, 0, HGGKFlags.BENOIT_CLOCK_ALARM, notes='At what time does your alarm clock go off?')
        self.addKFlag('BENOIT_CLOCK_BOUGHT', int, 0, HGGKFlags.BENOIT_CLOCK_BOUGHT, intbool=True)
        self.addKFlag('BENOIT_EGGS', int, 0, HGGKFlags.BENOIT_EGGS)
        self.addKFlag('BENOIT_EXPLAINED_SHOP', int, 0, HGGKFlags.BENOIT_EXPLAINED_SHOP, intbool=True)
        self.addKFlag('BENOIT_EYES_TALK_UNLOCKED', int, 0, HGGKFlags.BENOIT_EYES_TALK_UNLOCKED, intbool=True)
        self.addKFlag('BENOIT_GENERIC_EGGS', int, 0, HGGKFlags.BENOIT_GENERIC_EGGS)
        self.addKFlag('BENOIT_HAIRPIN_TALKED_TODAY', int, 0, HGGKFlags.BENOIT_HAIRPIN_TALKED_TODAY, intbool=True)
        self.addKFlag('BENOIT_PISTOL_BOUGHT', int, 0, HGGKFlags.BENOIT_PISTOL_BOUGHT, intbool=True)
        self.addKFlag('BENOIT_POST_FIRSTFUCK_TALK', int, 0, HGGKFlags.BENOIT_POST_FIRSTFUCK_TALK, intbool=True)
        self.addKFlag('BENOIT_STATUS', int, 0, HGGKFlags.BENOIT_STATUS, choices={
            0: 'Male',
            1: 'Female',
            2: 'Bimbo',
            3: 'Herm (only in HGG)',
        })
        self.addKFlag('BENOIT_SUGGEST_UNLOCKED', int, 0, HGGKFlags.BENOIT_SUGGEST_UNLOCKED, intbool=True)
        self.addKFlag('BENOIT_TALKED_TODAY', int, 0, HGGKFlags.BENOIT_TALKED_TODAY, intbool=True)
        self.addKFlag('BENOIT_TALKED_TO_PROPERLY', int, 0, HGGKFlags.BENOIT_TALKED_TO_PROPERLY, intbool=True)
        self.addKFlag('BENOIT_TESTED_BASILISK_WOMB', float, 0., HGGKFlags.BENOIT_TESTED_BASILISK_WOMB)
        self.addKFlag('BENOIT_TIMES_SEXED_FEMPCS', int, 0, HGGKFlags.BENOIT_TIMES_SEXED_FEMPCS)
        self.addKFlag('BENOIT_WOMB_TALK_UNLOCKED', int, 0, HGGKFlags.BENOIT_WOMB_TALK_UNLOCKED)
        self.addKFlag('BIMBO_FEMOIT_UNLOCKED', int, 0, HGGKFlags.BIMBO_FEMOIT_UNLOCKED)
        self.addKFlag('FEMOIT_EGGS', int, 0, HGGKFlags.FEMOIT_EGGS)
        self.addKFlag('FEMOIT_EGGS_LAID', int, 0, HGGKFlags.FEMOIT_EGGS_LAID, min=0)
        self.addKFlag('FEMOIT_FIRST_CLUTCH_MISSED', int, 0, HGGKFlags.FEMOIT_FIRST_CLUTCH_MISSED)
        self.addKFlag('FEMOIT_HELPED_LAY', int, 0, HGGKFlags.FEMOIT_HELPED_LAY)
        self.addKFlag('FEMOIT_INCUBATION', int, 0, HGGKFlags.FEMOIT_INCUBATION)
        self.addKFlag('FEMOIT_NEXTDAY_EVENT', int, 0, HGGKFlags.FEMOIT_NEXTDAY_EVENT)
        self.addKFlag('FEMOIT_NEXTDAY_EVENT_DONE', int, 0, HGGKFlags.FEMOIT_NEXTDAY_EVENT_DONE)
        self.addKFlag('FEMOIT_READY_FOR_EGGS', int, 0, HGGKFlags.FEMOIT_READY_FOR_EGGS)
        self.addKFlag('FEMOIT_SPOONED', int, 0, HGGKFlags.FEMOIT_SPOONED)
        self.addKFlag('FEMOIT_TALKED_TO', int, 0, HGGKFlags.FEMOIT_TALKED_TO)
        self.addKFlag('FEMOIT_UNLOCKED', int, 0, HGGKFlags.FEMOIT_UNLOCKED)
        self.addKFlag('TIMES_FUCKED_FEMOIT', int, 0, HGGKFlags.TIMES_FUCKED_FEMOIT)
        self.addKFlag('TIMES_IN_BENOITS', int, 0, HGGKFlags.TIMES_IN_BENOITS)
