from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class KaijuNPC(BaseNPC):
    ID = 'kaiju'
    NAME = 'Kaiju'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Boat/Kaiju.as: public class Kaiju extends AbstractLakeContent implements Encounter { @ 0Zd0BU3glbjf8v7blEg1h5088RS7qJ6ww0sC14F7yKfbt16DcQ4g8jeOk4g51y2fDr56lfYAd6K2ze2e34I5cy4gyZ3Hd9vg
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('KAIJU_BAD_END_COUNTER', int, 0, HGGKFlags.KAIJU_BAD_END_COUNTER, min=0, max=5, notes='Incremented by sex, decremented by talk.')
        self.addKFlag('KAIJU_COCK', int, 0, HGGKFlags.KAIJU_COCK, intbool=True)
        self.addKFlag('KAIJU_DISABLED', int, 0, HGGKFlags.KAIJU_DISABLED, intbool=True)
        self.addKFlag('KAIJU_MEETINGS', int, 0, HGGKFlags.KAIJU_MEETINGS)
        self.addKFlag('KAIJU_TALK_CYCLE', int, 0, HGGKFlags.KAIJU_TALK_CYCLE, intbool=True)
