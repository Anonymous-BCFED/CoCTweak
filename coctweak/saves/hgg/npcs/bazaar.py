from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class BazaarNPC(BaseNPC):
    ID = 'bazaar'
    NAME = 'Bazaar'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar.as: public class Bazaar extends BaseContent { @ 1gO7bY5pWeSS79h2HbgKR2dP9gB1Emg9V3HOcaF0qreMVaR2eYP1By6jg6AF3Sg1rid2g29D3MDc3i0W3egacg6fY5gFE9VD
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('BAZAAR_DEMONS_LISTENED_IN', int, 0, HGGKFlags.BAZAAR_DEMONS_LISTENED_IN, min=0)
        self.addKFlag('BAZAAR_ENCOUNTERED', int, 0, HGGKFlags.BAZAAR_ENCOUNTERED, intbool=True)
        self.addKFlag('BAZAAR_ENTERED', int, 0, HGGKFlags.BAZAAR_ENTERED, intbool=True)
