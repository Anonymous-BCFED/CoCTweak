from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class IsabellaNPC(BaseNPC):
    ID = 'isabella'
    NAME = 'Isabella'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/IsabellaFollowerScene.as: public class IsabellaFollowerScene extends NPCAwareContent { @ 66wfg67Qo38ieZf5Fy7WV0yK8Oc5Rh8MY7nC9FSgmx1L3gYS2w3bpz4hTa55gPo2BDeUtcxv4Tw6Prd39bzNfS75Y90nl4JZ
        ## from [HGG]/classes/classes/Scenes/NPCs/IsabellaScene.as: public class IsabellaScene extends NPCAwareContent implements TimeAwareInterface { @ 48LaSx0By3UG7jp0bF11If5537UcjV53E3tS5nX9gV5P32x0dUK7dv85v5SQd3P0pxdqQgqC6829Y83mMgSjcLA3ID0dDafL
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('FOLLOWER_AT_FARM_ISABELLA', int, 0, HGGKFlags.FOLLOWER_AT_FARM_ISABELLA)
        self.addKFlag('FOUND_ISABELLA_AT_FARM_TODAY', int, 0, HGGKFlags.FOUND_ISABELLA_AT_FARM_TODAY)
        self.addKFlag('ISABELLA_ACCENT_FORCED_ON', int, 0, HGGKFlags.ISABELLA_ACCENT_FORCED_ON)
        self.addKFlag('ISABELLA_ACCENT_TRAINING_COOLDOWN', int, 0, HGGKFlags.ISABELLA_ACCENT_TRAINING_COOLDOWN)
        self.addKFlag('ISABELLA_ACCENT_TRAINING_PERCENT', int, 0, HGGKFlags.ISABELLA_ACCENT_TRAINING_PERCENT)
        self.addKFlag('ISABELLA_AFFECTION', int, 0, HGGKFlags.ISABELLA_AFFECTION)
        self.addKFlag('ISABELLA_ANGRY_AT_PC_COUNTER', int, 0, HGGKFlags.ISABELLA_ANGRY_AT_PC_COUNTER)
        self.addKFlag('ISABELLA_BLOWJOBS_DISABLED', int, 0, HGGKFlags.ISABELLA_BLOWJOBS_DISABLED)
        self.addKFlag('ISABELLA_CAMP_APPROACHED', int, 0, HGGKFlags.ISABELLA_CAMP_APPROACHED)
        self.addKFlag('ISABELLA_CAMP_DISABLED', int, 0, HGGKFlags.ISABELLA_CAMP_DISABLED)
        self.addKFlag('ISABELLA_COUNTDOWN_TO_CONTRACEPTIONS', int, 0, HGGKFlags.ISABELLA_COUNTDOWN_TO_CONTRACEPTIONS)
        self.addKFlag('ISABELLA_COWMOTHER', int, 0, HGGKFlags.ISABELLA_COWMOTHER)
        self.addKFlag('ISABELLA_FOLLOWER_ACCEPTED', int, 0, HGGKFlags.ISABELLA_FOLLOWER_ACCEPTED)
        self.addKFlag('ISABELLA_MET', int, 0, HGGKFlags.ISABELLA_MET)
        self.addKFlag('ISABELLA_MET_SHORT_PC', int, 0, HGGKFlags.ISABELLA_MET_SHORT_PC)
        self.addKFlag('ISABELLA_MILKED_YET', int, 0, HGGKFlags.ISABELLA_MILKED_YET)
        self.addKFlag('ISABELLA_MILK_COOLDOWN', int, 0, HGGKFlags.ISABELLA_MILK_COOLDOWN)
        self.addKFlag('ISABELLA_MORNING_FELLATIO_COUNT', int, 0, HGGKFlags.ISABELLA_MORNING_FELLATIO_COUNT)
        self.addKFlag('ISABELLA_MURBLE_BLEH', int, 0, HGGKFlags.ISABELLA_MURBLE_BLEH)
        self.addKFlag('ISABELLA_OKAY_WITH_TALL_FOLKS', int, 0, HGGKFlags.ISABELLA_OKAY_WITH_TALL_FOLKS)
        self.addKFlag('ISABELLA_PLAINS_DISABLED', int, 0, HGGKFlags.ISABELLA_PLAINS_DISABLED)
        self.addKFlag('ISABELLA_POTENCY_STATE', int, 0, HGGKFlags.ISABELLA_POTENCY_STATE)
        self.addKFlag('ISABELLA_PREGNANCY_BOOSTED', int, 0, HGGKFlags.ISABELLA_PREGNANCY_BOOSTED)
        # PregnancyStore vag incubation counter
        self.addKFlag('ISABELLA_PREGNANCY_INCUBATION', int, 0, HGGKFlags.ISABELLA_PREGNANCY_INCUBATION)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('ISABELLA_PREGNANCY_TYPE', int, 0, HGGKFlags.ISABELLA_PREGNANCY_TYPE)
        self.addKFlag('ISABELLA_PROBOVA_BURP_COUNT', int, 0, HGGKFlags.ISABELLA_PROBOVA_BURP_COUNT)
        self.addKFlag('ISABELLA_SLEEP_RAPE_COUNTER', int, 0, HGGKFlags.ISABELLA_SLEEP_RAPE_COUNTER)
        self.addKFlag('ISABELLA_SPARRING_INTRO', int, 0, HGGKFlags.ISABELLA_SPARRING_INTRO)
        self.addKFlag('ISABELLA_TIMES_HOTDOGGED', int, 0, HGGKFlags.ISABELLA_TIMES_HOTDOGGED)
        self.addKFlag('ISABELLA_TIMES_OFFERED_FOLLOWER', int, 0, HGGKFlags.ISABELLA_TIMES_OFFERED_FOLLOWER)
        self.addKFlag('ISABELLA_TIMES_TALKED', int, 0, HGGKFlags.ISABELLA_TIMES_TALKED)
        self.addKFlag('ISABELLA_TIME_SINCE_LAST_HOTDOGGING', int, 0, HGGKFlags.ISABELLA_TIME_SINCE_LAST_HOTDOGGING)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('BurpChanged', bool, False, HGGStatusLib.BurpChanged, None)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.ISABELLA_PREGNANCY_TYPE, HGGKFlags.ISABELLA_PREGNANCY_INCUBATION)
