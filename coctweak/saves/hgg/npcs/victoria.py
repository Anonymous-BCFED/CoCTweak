from coctweak.saves._npc import BaseNPC, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class VictoriaNPC(BaseNPC):
    ID = 'victoria'
    NAME = 'Victoria'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/VictoriaTailorShop.as: public class VictoriaTailorShop extends Shop { @ ddWaw29bl7Oc4BD9so3Cycmv3pS6hIgzMfMl8x0c9faLnaX9b7vdc0aYtcYv0Pv5Z2acp9g9cmxfUagoKfOyfxxfvV642fUc
        super().__init__(save)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('Victoria', bool, False, HGGStatusLib.Victoria, None, notes="Whether you've met Victoria or not.")
