from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class TamanisDaughtersNPC(BaseNPC):
    ID = 'tamanisdaughters'
    NAME = 'TamanisDaughters'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/TamanisDaughtersScene.as: public class TamanisDaughtersScene extends BaseContent implements TimeAwareInterface { @ e7q9KsgKwcgye1o5uM9XN3qLcl851u4lReC8bAif0SbPM5hB8Eh1e64uE1HF6S2bGkaA2dXdfqa3yE9LccaddezcgT0P22l9
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('TAMANI_DAUGHTERS_PREGNANCY_COUNT', int, 0, HGGKFlags.TAMANI_DAUGHTERS_PREGNANCY_COUNT)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('TAMANI_DAUGHTERS_PREGNANCY_TYPE', int, 0, HGGKFlags.TAMANI_DAUGHTERS_PREGNANCY_TYPE)
        self.addKFlag('TAMANI_DAUGHTER_CHAIR_COUNTER', int, 0, HGGKFlags.TAMANI_DAUGHTER_CHAIR_COUNTER)
        # PregnancyStore vag incubation counter
        self.addKFlag('TAMANI_DAUGHTER_PREGGO_COUNTDOWN', int, 0, HGGKFlags.TAMANI_DAUGHTER_PREGGO_COUNTDOWN)
        self.addKFlag('TAMANI_NUMBER_OF_DAUGHTERS', int, 0, HGGKFlags.TAMANI_NUMBER_OF_DAUGHTERS)
        self.addKFlag('TIMES_ENCOUNTED_TAMANIS_DAUGHTERS', int, 0, HGGKFlags.TIMES_ENCOUNTED_TAMANIS_DAUGHTERS)
        self.addKFlag('TIMES_FUCKED_TAMANIS_DAUGHTERS', int, 0, HGGKFlags.TIMES_FUCKED_TAMANIS_DAUGHTERS)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.TAMANI_DAUGHTERS_PREGNANCY_TYPE, HGGKFlags.TAMANI_DAUGHTER_PREGGO_COUNTDOWN)
