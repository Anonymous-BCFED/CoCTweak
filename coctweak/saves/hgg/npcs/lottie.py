from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class LottieNPC(BaseNPC):
    ID = 'lottie'
    NAME = 'Lottie'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Lottie.as: public class Lottie extends TelAdreAbstractContent { @ 1Ub8ox5Ss1yFeh1bC3bQl1Yj33c0AjabjaFF8Pq4JPdiYbQf3pJ1ihglM6wV6FOd500CC9rmeFj8KydyT0VpgQi2DY71KaPf
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('LOTTIE_CHARLOTTES_WEB_EVENT', int, 0, HGGKFlags.LOTTIE_CHARLOTTES_WEB_EVENT)
        self.addKFlag('LOTTIE_CONFIDENCE', int, 0, HGGKFlags.LOTTIE_CONFIDENCE)
        self.addKFlag('LOTTIE_COWGIRL_UNLOCKED', int, 0, HGGKFlags.LOTTIE_COWGIRL_UNLOCKED)
        self.addKFlag('LOTTIE_DISABLED', int, 0, HGGKFlags.LOTTIE_DISABLED)
        self.addKFlag('LOTTIE_DOGGYSTYLE_UNLOCKED', int, 0, HGGKFlags.LOTTIE_DOGGYSTYLE_UNLOCKED)
        self.addKFlag('LOTTIE_ELLE_NTR_UNLOCKED', int, 0, HGGKFlags.LOTTIE_ELLE_NTR_UNLOCKED)
        self.addKFlag('LOTTIE_ENCOUNTER_COUNTER', int, 0, HGGKFlags.LOTTIE_ENCOUNTER_COUNTER)
        self.addKFlag('LOTTIE_ENCOURAGEMENT_THRESHOLD_30_31_UP', int, 0, HGGKFlags.LOTTIE_ENCOURAGEMENT_THRESHOLD_30_31_UP)
        self.addKFlag('LOTTIE_ENCOURAGEMENT_THRESHOLD_31_30_DOWN', int, 0, HGGKFlags.LOTTIE_ENCOURAGEMENT_THRESHOLD_31_30_DOWN)
        self.addKFlag('LOTTIE_ENCOURAGEMENT_THRESHOLD_79_80_UP', int, 0, HGGKFlags.LOTTIE_ENCOURAGEMENT_THRESHOLD_79_80_UP)
        self.addKFlag('LOTTIE_ENCOURAGEMENT_THRESHOLD_80_79_DOWN', int, 0, HGGKFlags.LOTTIE_ENCOURAGEMENT_THRESHOLD_80_79_DOWN)
        self.addKFlag('LOTTIE_FATASS_LOW_ESTEEM', int, 0, HGGKFlags.LOTTIE_FATASS_LOW_ESTEEM)
        self.addKFlag('LOTTIE_FEMDOM_UNLOCKED', int, 0, HGGKFlags.LOTTIE_FEMDOM_UNLOCKED)
        self.addKFlag('LOTTIE_FIGURE', int, 0, HGGKFlags.LOTTIE_FIGURE)
        self.addKFlag('LOTTIE_HAM_SAMMICH', int, 0, HGGKFlags.LOTTIE_HAM_SAMMICH)
        self.addKFlag('LOTTIE_LABOVA_COUNTDOWN', int, 0, HGGKFlags.LOTTIE_LABOVA_COUNTDOWN)
        self.addKFlag('LOTTIE_LAST_ENCOURAGEMENT_STATE', int, 0, HGGKFlags.LOTTIE_LAST_ENCOURAGEMENT_STATE)
        self.addKFlag('LOTTIE_LOVES_HER_BODY', int, 0, HGGKFlags.LOTTIE_LOVES_HER_BODY)
        self.addKFlag('LOTTIE_LOVING_FAT_BITCH_EVENT', int, 0, HGGKFlags.LOTTIE_LOVING_FAT_BITCH_EVENT)
        self.addKFlag('LOTTIE_MAX_GOAL_LOVE_REACHED', int, 0, HGGKFlags.LOTTIE_MAX_GOAL_LOVE_REACHED)
        self.addKFlag('LOTTIE_NEEDS_TO_TALK_ANAL', int, 0, HGGKFlags.LOTTIE_NEEDS_TO_TALK_ANAL)
        self.addKFlag('LOTTIE_NEEDS_TO_TALK_ORAL', int, 0, HGGKFlags.LOTTIE_NEEDS_TO_TALK_ORAL)
        self.addKFlag('LOTTIE_PORKED_COUNT', int, 0, HGGKFlags.LOTTIE_PORKED_COUNT)
        self.addKFlag('LOTTIE_QUICKIE_UNLOCKED', int, 0, HGGKFlags.LOTTIE_QUICKIE_UNLOCKED)
        self.addKFlag('LOTTIE_REDUCTO_USED', int, 0, HGGKFlags.LOTTIE_REDUCTO_USED)
        self.addKFlag('LOTTIE_REVERSE_COWGIRL_UNLOCKED', int, 0, HGGKFlags.LOTTIE_REVERSE_COWGIRL_UNLOCKED)
        self.addKFlag('LOTTIE_THREESOME_UNLOCKED', int, 0, HGGKFlags.LOTTIE_THREESOME_UNLOCKED)
        self.addKFlag('LOTTIE_TIMES_GIVEN_ITEMS', int, 0, HGGKFlags.LOTTIE_TIMES_GIVEN_ITEMS)
        self.addKFlag('LOTTIE_TRAINER', int, 0, HGGKFlags.LOTTIE_TRAINER)
        self.addKFlag('LOTTIE_WALKIE_UNLOCKED', int, 0, HGGKFlags.LOTTIE_WALKIE_UNLOCKED)
        self.addKFlag('LOTTIE_WEIGHT_CONCLUSION', int, 0, HGGKFlags.LOTTIE_WEIGHT_CONCLUSION)
