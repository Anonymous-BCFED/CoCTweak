from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class ErlKingNPC(BaseNPC):
    ID = 'erlking'
    NAME = 'ErlKing'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/ErlKingScene.as: public class ErlKingScene extends BaseContent implements Encounter { @ 2wNdeb8L1bE39PIdRl2vx7nPbot2pw46k9SP9he9LdfHL24t9DL9Ck2Sk67d4cH5VOd1JcjN9Jzcml97n0Kr88Z3z88xc0CB
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('ERLKING_CANE_OBTAINED', int, 0, HGGKFlags.ERLKING_CANE_OBTAINED)
        self.addKFlag('ERLKING_DISABLED', int, 0, HGGKFlags.ERLKING_DISABLED)
        self.addKFlag('TIMES_ENCOUNTERED_PRINCESS_GWYNN', int, 0, HGGKFlags.TIMES_ENCOUNTERED_PRINCESS_GWYNN)
        self.addKFlag('WILD_HUNT_ENCOUNTERS', int, 0, HGGKFlags.WILD_HUNT_ENCOUNTERS)
