from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class HellmouthNPC(BaseNPC):
    ID = 'hellmouth'
    NAME = 'Hellmouth'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/VolcanicCrag/Hellmouth.as: public class Hellmouth extends Monster { @ 5MgbRJ5OQ6Yj37HeH30d90cn457aPndig2kd2s2gNdaCA64c0dmffY2SH4mX4rt1TlgPSdQOeLD2gC3GQg0RaYX2sK2Reg0k
        ## from [HGG]/classes/classes/Scenes/Areas/VolcanicCrag/HellmouthScene.as: public class HellmouthScene extends BaseContent implements SelfSaving, SelfDebug { @ 4jCdso6cfdGZ0Gn1eEbrr1C21Zc0Mi3SU2o35qi7o45zP09J3y43BQa9ZeR40ya9Ti6Sza2C0qz1bifnU0GL7r7bes6x38T9
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "ambushed": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('ambushed', bool, False, 'hellmouth', 'ambushed')
        ##########
        # kFlags
        ##########
        self.addKFlag('HELLMOUTHS_KILLED', int, 0, HGGKFlags.HELLMOUTHS_KILLED)
        self.addKFlag('MET_HELLMOUTH', int, 0, HGGKFlags.MET_HELLMOUTH)
