from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class AnemoneNPC(BaseNPC):
    ID = 'anemone'
    NAME = 'Anemone'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/AnemoneScene.as: public class AnemoneScene extends BaseContent implements TimeAwareInterface { @ aPegw0dn134k6Lm1el5uD8ZC34w0io3zV8Pf7TX59AeY9fNO5VT7Uha4zgDJc6D5Bb9Zy95f7bB1dz6rjbxB2OJeHGcEu9Sq
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('ANEMONE_KID', int, 0, HGGKFlags.ANEMONE_KID)
        self.addKFlag('ANEMONE_WATCH', int, 0, HGGKFlags.ANEMONE_WATCH)
        self.addKFlag('ANEMONE_WEAPON_ID', str, '', HGGKFlags.ANEMONE_WEAPON_ID)
        self.addKFlag('HAD_KID_A_DREAM', int, 0, HGGKFlags.HAD_KID_A_DREAM)
        self.addKFlag('KID_A_XP', int, 0, HGGKFlags.KID_A_XP)
        self.addKFlag('KID_ITEM_FIND_HOURS', int, 0, HGGKFlags.KID_ITEM_FIND_HOURS)
        # 0 = no sitter, 1 = possible, 2 = doing dat shit
        self.addKFlag('KID_SITTER', int, 0, HGGKFlags.KID_SITTER)
        self.addKFlag('TIMES_MET_ANEMONE', int, 0, HGGKFlags.TIMES_MET_ANEMONE)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('PostAnemoneBeatdown', bool, False, HGGStatusLib.PostAnemoneBeatdown, None)
