from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class NephiliaSlimeNPC(BaseNPC):
    ID = 'nephiliaslime'
    NAME = 'NephiliaSlime'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Mountain/NephilaSlimeScene.as: public class NephilaSlimeScene extends BaseContent { @ 77d27p5pWgNS7mkaRP5O7arldY13TxdH2ff2eFucxq8lk2Gm5T08rr2VO3clcAMa6a1S86333oy8to1Of87l2gJc8N8Wg0ep
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('NEPHILA_COVEN_QUEEN_CROWNED', int, 0, HGGKFlags.NEPHILA_COVEN_QUEEN_CROWNED)
        self.addKFlag('TIMES_MET_NEPHILA', int, 0, HGGKFlags.TIMES_MET_NEPHILA)
