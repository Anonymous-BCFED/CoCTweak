from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class FarmCorruptionNPC(BaseNPC):
    ID = 'farmcorruption'
    NAME = 'FarmCorruption'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Farm/FarmCorruption.as: public class FarmCorruption extends AbstractFarmContent { @ bpw8r58O2eZTfLl4IUa7e9Tl6vEejMaHIaue3Sr1qP0p7bcq0B5b9tfyX6ewbgFfOs7Nd4ZbbAYb8Y0RA45le8x7adfpL0SK
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('FARM_CONTRACEPTIVE_STORED', int, 0, HGGKFlags.FARM_CONTRACEPTIVE_STORED)
        self.addKFlag('FARM_CORRUPTION_APPROACHED_WHITNEY', int, 0, HGGKFlags.FARM_CORRUPTION_APPROACHED_WHITNEY, intbool=True)
        self.addKFlag('FARM_CORRUPTION_BRANDING_MENU_UNLOCKED', int, 0, HGGKFlags.FARM_CORRUPTION_BRANDING_MENU_UNLOCKED, intbool=True)
        self.addKFlag('FARM_CORRUPTION_DAYS_SINCE_LAST_PAYOUT', int, 0, HGGKFlags.FARM_CORRUPTION_DAYS_SINCE_LAST_PAYOUT)
        self.addKFlag('FARM_CORRUPTION_DISABLED', int, 0, HGGKFlags.FARM_CORRUPTION_DISABLED, intbool=True)
        self.addKFlag('FARM_CORRUPTION_GEMS_WAITING', int, 0, HGGKFlags.FARM_CORRUPTION_GEMS_WAITING)
        self.addKFlag('FARM_CORRUPTION_STARTED', int, 0, HGGKFlags.FARM_CORRUPTION_STARTED, intbool=True)
        self.addKFlag('FARM_CORRUPT_PROMPT_DISPLAY', int, 0, HGGKFlags.FARM_CORRUPT_PROMPT_DISPLAY, notes='In days.')
        self.addKFlag('FARM_DISABLED', int, 0, HGGKFlags.FARM_DISABLED, intbool=True)
        self.addKFlag('FARM_EGG_COUNTDOWN', int, 0, HGGKFlags.FARM_EGG_COUNTDOWN)
        self.addKFlag('FARM_EGG_STORED', int, 0, HGGKFlags.FARM_EGG_STORED)
        self.addKFlag('FARM_INCUDRAFT_STORED', int, 0, HGGKFlags.FARM_INCUDRAFT_STORED)
        self.addKFlag('FARM_SUCCUMILK_STORED', int, 0, HGGKFlags.FARM_SUCCUMILK_STORED)
        self.addKFlag('FARM_UPGRADES_CONTRACEPTIVE', int, 0, HGGKFlags.FARM_UPGRADES_CONTRACEPTIVE, intbool=True)
        self.addKFlag('FARM_UPGRADES_MILKTANK', int, 0, HGGKFlags.FARM_UPGRADES_MILKTANK, intbool=True)
        self.addKFlag('FARM_UPGRADES_ORGYROOM', int, 0, HGGKFlags.FARM_UPGRADES_ORGYROOM, intbool=True)
        self.addKFlag('FARM_UPGRADES_REFINERY', int, 0, HGGKFlags.FARM_UPGRADES_REFINERY, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_AMILY', int, 0, HGGKFlags.FOLLOWER_AT_FARM_AMILY, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_AMILY_GIBS_MILK', int, 0, HGGKFlags.FOLLOWER_AT_FARM_AMILY_GIBS_MILK, choices={
            0: 'No',
            1: 'Yes',
            2: 'Personal service...?',
        })
        self.addKFlag('FOLLOWER_AT_FARM_BATH_GIRL', int, 0, HGGKFlags.FOLLOWER_AT_FARM_BATH_GIRL, notes='AKA Milky', intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_CERAPH', int, 0, HGGKFlags.FOLLOWER_AT_FARM_CERAPH, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_HOLLI', int, 0, HGGKFlags.FOLLOWER_AT_FARM_HOLLI, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_ISABELLA', int, 0, HGGKFlags.FOLLOWER_AT_FARM_ISABELLA, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_IZMA', int, 0, HGGKFlags.FOLLOWER_AT_FARM_IZMA, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_JOJO', int, 0, HGGKFlags.FOLLOWER_AT_FARM_JOJO, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_JOJO_GIBS_DRAFT', int, 0, HGGKFlags.FOLLOWER_AT_FARM_JOJO_GIBS_DRAFT, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_KELLY', int, 0, HGGKFlags.FOLLOWER_AT_FARM_KELLY, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_LATEXY', int, 0, HGGKFlags.FOLLOWER_AT_FARM_LATEXY, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_MARBLE', int, 0, HGGKFlags.FOLLOWER_AT_FARM_MARBLE, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_SOPHIE', int, 0, HGGKFlags.FOLLOWER_AT_FARM_SOPHIE, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_VAPULA', int, 0, HGGKFlags.FOLLOWER_AT_FARM_VAPULA, intbool=True)
        self.addKFlag('FOLLOWER_AT_FARM_VAPULA_GIBS_MILK', int, 0, HGGKFlags.FOLLOWER_AT_FARM_VAPULA_GIBS_MILK, intbool=True)
        self.addKFlag('FOLLOWER_PRODUCTION_AMILY', int, 0, HGGKFlags.FOLLOWER_PRODUCTION_AMILY, intbool=True)
        self.addKFlag('FOLLOWER_PRODUCTION_JOJO', int, 0, HGGKFlags.FOLLOWER_PRODUCTION_JOJO, intbool=True)
        self.addKFlag('FOLLOWER_PRODUCTION_SOPHIE', int, 0, HGGKFlags.FOLLOWER_PRODUCTION_SOPHIE, intbool=True)
        self.addKFlag('FOLLOWER_PRODUCTION_SOPHIE_COLORCHOICE', str, '', HGGKFlags.FOLLOWER_PRODUCTION_SOPHIE_COLORCHOICE, choices={
            'Black': 'Black',
            'Blue': 'Blue',
            'Brown': 'Brown',
            'Pink': 'Pink',
            'Purple': 'Purple',
            'White': 'White',
        })
        self.addKFlag('FOLLOWER_PRODUCTION_VAPULA', int, 0, HGGKFlags.FOLLOWER_PRODUCTION_VAPULA, intbool=True)
        self.addKFlag('QUEUE_BRANDING_AVAILABLE_TALK', int, 0, HGGKFlags.QUEUE_BRANDING_AVAILABLE_TALK)
        self.addKFlag('QUEUE_BRANDING_UPGRADE', int, 0, HGGKFlags.QUEUE_BRANDING_UPGRADE)
        self.addKFlag('QUEUE_BREASTMILKER_UPGRADE', int, 0, HGGKFlags.QUEUE_BREASTMILKER_UPGRADE)
        self.addKFlag('QUEUE_COCKMILKER_UPGRADE', int, 0, HGGKFlags.QUEUE_COCKMILKER_UPGRADE)
        self.addKFlag('QUEUE_CONTRACEPTIVE_UPGRADE', int, 0, HGGKFlags.QUEUE_CONTRACEPTIVE_UPGRADE)
        self.addKFlag('QUEUE_MILKTANK_UPGRADE', int, 0, HGGKFlags.QUEUE_MILKTANK_UPGRADE)
        self.addKFlag('QUEUE_ORGYROOM_UPGRADE', int, 0, HGGKFlags.QUEUE_ORGYROOM_UPGRADE)
        self.addKFlag('QUEUE_REFINERY_UPGRADE', int, 0, HGGKFlags.QUEUE_REFINERY_UPGRADE)
        self.addKFlag('WHITNEY_CORRUPTION', int, 0, HGGKFlags.WHITNEY_CORRUPTION)
        self.addKFlag('WHITNEY_CORRUPTION_0_30_DROP_MESSAGE', int, 0, HGGKFlags.WHITNEY_CORRUPTION_0_30_DROP_MESSAGE, intbool=True)
        self.addKFlag('WHITNEY_CORRUPTION_COMPLETE', int, 0, HGGKFlags.WHITNEY_CORRUPTION_COMPLETE, intbool=True)
        self.addKFlag('WHITNEY_CORRUPTION_HIGHEST', int, 0, HGGKFlags.WHITNEY_CORRUPTION_HIGHEST)
        self.addKFlag('WHITNEY_DEFURRED', int, 0, HGGKFlags.WHITNEY_DEFURRED, intbool=True)
        self.addKFlag('WHITNEY_DISABLED_FOR_DAY', int, 0, HGGKFlags.WHITNEY_DISABLED_FOR_DAY, choices={
            0: 'No',
            1: 'Yes',
            2: 'Followup soon',
            3: 'Defurred',
        })
        self.addKFlag('WHITNEY_DOM', int, 0, HGGKFlags.WHITNEY_DOM, intbool=True)
        self.addKFlag('WHITNEY_DOM_FIRST_PLEASURE', int, 0, HGGKFlags.WHITNEY_DOM_FIRST_PLEASURE, intbool=True)
        self.addKFlag('WHITNEY_LEAVE_0_60', int, 0, HGGKFlags.WHITNEY_LEAVE_0_60, intbool=True)
        self.addKFlag('WHITNEY_LEAVE_61_90', int, 0, HGGKFlags.WHITNEY_LEAVE_61_90, intbool=True)
        self.addKFlag('WHITNEY_MENU_31_60', int, 0, HGGKFlags.WHITNEY_MENU_31_60, intbool=True)
        self.addKFlag('WHITNEY_MENU_61_90', int, 0, HGGKFlags.WHITNEY_MENU_61_90, intbool=True)
        self.addKFlag('WHITNEY_MENU_91_119', int, 0, HGGKFlags.WHITNEY_MENU_91_119, intbool=True)
        self.addKFlag('WHITNEY_ORAL_TRAINING', int, 0, HGGKFlags.WHITNEY_ORAL_TRAINING, intbool=True)
        self.addKFlag('WHITNEY_TATTOO_BUTT', int, 0, HGGKFlags.WHITNEY_TATTOO_BUTT)
        self.addKFlag('WHITNEY_TATTOO_COLLARBONE', int, 0, HGGKFlags.WHITNEY_TATTOO_COLLARBONE)
        self.addKFlag('WHITNEY_TATTOO_LOWERBACK', int, 0, HGGKFlags.WHITNEY_TATTOO_LOWERBACK)
        self.addKFlag('WHITNEY_TATTOO_SHOULDERS', int, 0, HGGKFlags.WHITNEY_TATTOO_SHOULDERS)
