from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class MarbleNPC(BaseNPC):
    ID = 'marble'
    NAME = 'Marble'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/MarblePurification.as: public class MarblePurification extends NPCAwareContent { @ fG73MlgWC640bjog4J25ah1W4Ws9X16gY1zrbIq92dflb3At1PadQO1Lm4YQfWP6ShgHnfoTebHeYa7Qg2hZ3A5cEBfbCeIz
        ## from [HGG]/classes/classes/Scenes/NPCs/MarbleScene.as: public class MarbleScene extends NPCAwareContent implements TimeAwareInterface { @ 8r0fyR5d02scetW4FFaQ9coBaZNa6y67jcuIaAna5Ha4G4BagIa5izeOj9Zu22i6hWcxW5Lz4aMbIV8k49Kdb7a3jQekA0Vb
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('BROKE_UP_WITH_MARBLE', int, 0, HGGKFlags.BROKE_UP_WITH_MARBLE)
        self.addKFlag('FOLLOWER_AT_FARM_MARBLE', int, 0, HGGKFlags.FOLLOWER_AT_FARM_MARBLE)
        self.addKFlag('ISABELLA_MURBLE_BLEH', int, 0, HGGKFlags.ISABELLA_MURBLE_BLEH)
        self.addKFlag('IZMA_MARBLE_FREAKOUT_STATUS', int, 0, HGGKFlags.IZMA_MARBLE_FREAKOUT_STATUS)
        self.addKFlag('MARBLE_BOVA_LEVEL', int, 0, HGGKFlags.MARBLE_BOVA_LEVEL)
        self.addKFlag('MARBLE_BOYS', int, 0, HGGKFlags.MARBLE_BOYS)
        self.addKFlag('MARBLE_BREAST_SIZE', int, 0, HGGKFlags.MARBLE_BREAST_SIZE)
        self.addKFlag('MARBLE_CAMPTALK_LEVEL', int, 0, HGGKFlags.MARBLE_CAMPTALK_LEVEL)
        self.addKFlag('MARBLE_COUNTUP_TO_PURIFYING', int, 0, HGGKFlags.MARBLE_COUNTUP_TO_PURIFYING)
        self.addKFlag('MARBLE_DICK_LENGTH', int, 0, HGGKFlags.MARBLE_DICK_LENGTH)
        self.addKFlag('MARBLE_DICK_THICKNESS', int, 0, HGGKFlags.MARBLE_DICK_THICKNESS)
        self.addKFlag('MARBLE_DICK_TYPE', int, 0, HGGKFlags.MARBLE_DICK_TYPE)
        self.addKFlag('MARBLE_GROSSED_OUT_BECAUSE_WORM_INFESTATION', int, 0, HGGKFlags.MARBLE_GROSSED_OUT_BECAUSE_WORM_INFESTATION)
        self.addKFlag('MARBLE_KIDS', int, 0, HGGKFlags.MARBLE_KIDS)
        self.addKFlag('MARBLE_LEFT_OVER_CORRUPTION', int, 0, HGGKFlags.MARBLE_LEFT_OVER_CORRUPTION)
        self.addKFlag('MARBLE_LUST', int, 0, HGGKFlags.MARBLE_LUST)
        self.addKFlag('MARBLE_MILKED_BEFORE', int, 0, HGGKFlags.MARBLE_MILKED_BEFORE)
        self.addKFlag('MARBLE_NURSERY_CONSTRUCTION', int, 0, HGGKFlags.MARBLE_NURSERY_CONSTRUCTION)
        self.addKFlag('MARBLE_OR_AMILY_FIRST_FOR_FREAKOUT', int, 0, HGGKFlags.MARBLE_OR_AMILY_FIRST_FOR_FREAKOUT)
        self.addKFlag('MARBLE_PLAYED_WITH_KIDS_TODAY', int, 0, HGGKFlags.MARBLE_PLAYED_WITH_KIDS_TODAY)
        # PregnancyStore vag incubation counter
        self.addKFlag('MARBLE_PREGNANCY_INCUBATION', int, 0, HGGKFlags.MARBLE_PREGNANCY_INCUBATION)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('MARBLE_PREGNANCY_TYPE', int, 0, HGGKFlags.MARBLE_PREGNANCY_TYPE)
        self.addKFlag('MARBLE_PURIFICATION_STAGE', int, 0, HGGKFlags.MARBLE_PURIFICATION_STAGE)
        self.addKFlag('MARBLE_PURIFIED', int, 0, HGGKFlags.MARBLE_PURIFIED)
        self.addKFlag('MARBLE_RATHAZUL_COUNTER_1', int, 0, HGGKFlags.MARBLE_RATHAZUL_COUNTER_1)
        self.addKFlag('MARBLE_RATHAZUL_COUNTER_2', int, 0, HGGKFlags.MARBLE_RATHAZUL_COUNTER_2)
        self.addKFlag('MARBLE_TELADRE_STORY', int, 0, HGGKFlags.MARBLE_TELADRE_STORY)
        self.addKFlag('MARBLE_TIME_SINCE_NURSED_IN_HOURS', int, 0, HGGKFlags.MARBLE_TIME_SINCE_NURSED_IN_HOURS)
        self.addKFlag('MARBLE_WARNED_ABOUT_CORRUPTION', int, 0, HGGKFlags.MARBLE_WARNED_ABOUT_CORRUPTION)
        self.addKFlag('MARBLE_WARNING', int, 0, HGGKFlags.MARBLE_WARNING)
        self.addKFlag('MURBLE_FARM_TALK_LEVELS', int, 0, HGGKFlags.MURBLE_FARM_TALK_LEVELS)
        self.addKFlag('MURBLE_TEA_DRINKER_COUNT', int, 0, HGGKFlags.MURBLE_TEA_DRINKER_COUNT)
        self.addKFlag('TIMES_GIVEN_MARBLE_PURE_LABOVA', int, 0, HGGKFlags.TIMES_GIVEN_MARBLE_PURE_LABOVA)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('CampMarble', bool, False, HGGStatusLib.CampMarble, None, notes='Marble is at your camp.')
        self.addSFXProperty('ClaraFoughtInCamp', bool, False, HGGStatusLib.ClaraFoughtInCamp, None)
        self.addSFXProperty('FuckedMarble', bool, False, HGGStatusLib.FuckedMarble, None)
        self.addSFXProperty('MalonVisitedPostAddiction', bool, False, HGGStatusLib.MalonVisitedPostAddiction, None)
        self.addSFXProperty('Marble:0', int, 0, HGGStatusLib.Marble, 0, min=0, max=100, notes="Marble's affection towards the player.")
        self.addSFXProperty('Marble:1', int, 0, HGGStatusLib.Marble, 1, min=0, max=100, notes="Your addiction to Marble's milk.")
        self.addSFXProperty('Marble:2', bool, False, HGGStatusLib.Marble, 2, notes="Whether the player knows they're addicted to Marble's milk.")
        self.addSFXProperty('Marble:3', int, 0, HGGStatusLib.Marble, 3, notes="Marble's breast size. Depends on Marble's purity.")
        self.addSFXProperty('MarbleRapeAttempted', bool, False, HGGStatusLib.MarbleRapeAttempted, None)
        self.addSFXProperty('MarbleSpecials:0', bool, False, HGGStatusLib.MarbleSpecials, 0, notes='Whether her anus is virgin or not.')
        self.addSFXProperty('MarbleSpecials:1', int, 0, HGGStatusLib.MarbleSpecials, 1, notes='Nipple type.', choices={
            '0-1': 'Regular nipples',
            4: 'Quad-nipples',
        })
        self.addSFXProperty('MarbleSpecials:2', None, 0, HGGStatusLib.MarbleSpecials, 2, unused=True)
        self.addSFXProperty('MarbleSpecials:3', int, 0, HGGStatusLib.MarbleSpecials, 3, max=0, notes="Marble's corruption level.")
        self.addSFXProperty('NoMoreMarble', bool, False, HGGStatusLib.NoMoreMarble, None, notes='Marble no longer appears at the Farm.')
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.MARBLE_PREGNANCY_TYPE, HGGKFlags.MARBLE_PREGNANCY_INCUBATION)
