from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class HolliNPC(BaseNPC):
    ID = 'holli'
    NAME = 'Holli'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/HolliScene.as: public class HolliScene extends NPCAwareContent implements SelfSaving, SelfDebug { @ 9NhfJPa4BgDFaob33v8LP1N0gqA6PC4Y6bKX3lb1iu42Q8yj2Yt0L67Mp1or7iu62Hal99dvaOO4IM9U4bArg8AgkjeXlaGV
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "askedGlades": [
                "Boolean",
                ""
            ],
            "growingGlades": [
                "Boolean",
                ""
            ],
            "fruitsEaten": [
                "Int",
                ""
            ]
        }
        '''
        self.addSSOProperty('askedGlades', bool, False, 'holli', 'askedGlades')
        self.addSSOProperty('fruitsEaten', int, 0, 'holli', 'fruitsEaten')
        self.addSSOProperty('growingGlades', bool, False, 'holli', 'growingGlades')
        ##########
        # kFlags
        ##########
        self.addKFlag('FOLLOWER_AT_FARM_HOLLI', int, 0, HGGKFlags.FOLLOWER_AT_FARM_HOLLI)
        self.addKFlag('FOUGHT_HOLLI', int, 0, HGGKFlags.FOUGHT_HOLLI)
        self.addKFlag('HOLLI_DEFENSE_ON', int, 0, HGGKFlags.HOLLI_DEFENSE_ON)
        self.addKFlag('HOLLI_FRUIT', int, 0, HGGKFlags.HOLLI_FRUIT)
        self.addKFlag('HOLLI_FRUIT_EXPLAINED', int, 0, HGGKFlags.HOLLI_FRUIT_EXPLAINED)
        self.addKFlag('HOLLI_FUCKED_TODAY', int, 0, HGGKFlags.HOLLI_FUCKED_TODAY)
        self.addKFlag('HOLLI_SUBMISSIVE', int, 0, HGGKFlags.HOLLI_SUBMISSIVE)
        self.addKFlag('THREATENED_HOLLI', int, 0, HGGKFlags.THREATENED_HOLLI)
        self.addKFlag('TIMES_FUCKED_FLOWER', int, 0, HGGKFlags.TIMES_FUCKED_FLOWER)
        self.addKFlag('TIMES_RIDDEN_FLOWER', int, 0, HGGKFlags.TIMES_RIDDEN_FLOWER)
