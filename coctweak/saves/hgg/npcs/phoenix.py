from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class PhoenixNPC(BaseNPC):
    ID = 'phoenix'
    NAME = 'Phoenix'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/HighMountains/PhoenixScene.as: public class PhoenixScene extends BaseContent { @ 0la9wd78NaMv3zK5T7d7Ab3p4aS1ypaAX5Au1rF1oQboUfz67wU4pkaG03DO88u4Gl3nUeuIdp42Jyd7287p28A5ThaPC0ve
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('PHOENIX_ENCOUNTERED', int, 0, HGGKFlags.PHOENIX_ENCOUNTERED)
        self.addKFlag('PHOENIX_HP_LOSS_COUNTER', int, 0, HGGKFlags.PHOENIX_HP_LOSS_COUNTER)
        self.addKFlag('PHOENIX_WANKED_COUNTER', int, 0, HGGKFlags.PHOENIX_WANKED_COUNTER)
