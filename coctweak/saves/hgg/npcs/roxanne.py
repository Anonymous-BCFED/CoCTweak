from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class RoxanneNPC(BaseNPC):
    ID = 'roxanne'
    NAME = 'Roxanne'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/Roxanne.as: public class Roxanne extends BazaarAbstractContent implements TimeAwareInterface { @ cO49W33JHbaC4nL3VJ1GS5gJ2kbey9anC9q04Y0bTkfyYdwpalm9nA9XI8todSo69edEr8i9d3Y147gxEbfh4Op2sP8KEc2c
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('COUNTDOWN_TO_NIGHT_RAPE', int, 0, HGGKFlags.COUNTDOWN_TO_NIGHT_RAPE)
        self.addKFlag('ROXANNE_DRINING_CONTEST_LOST', int, 0, HGGKFlags.ROXANNE_DRINING_CONTEST_LOST)
        self.addKFlag('ROXANNE_DRINING_CONTEST_WON', int, 0, HGGKFlags.ROXANNE_DRINING_CONTEST_WON)
        self.addKFlag('ROXANNE_DRINKING_CONTEST_BONUS_SCORE', int, 0, HGGKFlags.ROXANNE_DRINKING_CONTEST_BONUS_SCORE)
        self.addKFlag('ROXANNE_DRINKING_CONTEST_LAST_WINNER', int, 0, HGGKFlags.ROXANNE_DRINKING_CONTEST_LAST_WINNER, choices={
            0: 'N/A',
            1: 'PC',
            2: 'Strahza',
        })
        self.addKFlag('ROXANNE_DRINKING_CONTEST_LOSE_ON_PURPOSE', int, 0, HGGKFlags.ROXANNE_DRINKING_CONTEST_LOSE_ON_PURPOSE)
        self.addKFlag('ROXANNE_MET', int, 0, HGGKFlags.ROXANNE_MET)
        self.addKFlag('ROXANNE_TIME_WITHOUT_SEX', int, 0, HGGKFlags.ROXANNE_TIME_WITHOUT_SEX, min=0, notes='How long has Strahza gone without sex?')
