from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class UrtaNPC(BaseNPC):
    ID = 'urta'
    NAME = 'Urta'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/UrtaHeatRut.as: public class UrtaHeatRut extends NPCAwareContent { @ 6egaqd7ck8JT6fgdYV11N0X2bBFdeD2TY0uv1YGgV42TncDmdC9baM2gn0jw4kt6nEc4ram7gi8g1g9wF5NEcjwcDF1db5Ij
        ## from [HGG]/classes/classes/Scenes/NPCs/UrtaPregs.as: public class UrtaPregs extends NPCAwareContent implements VaginalPregnancy { @ 2Fe04Te8e9Z0fQL3ASf226cl3zF1o6ef28iZbSQfy03747XZb842oL2l46Sq3A8dh65T73Uyf1H6V48Or5Js3evd0ngZZ4Se
        ## from [HGG]/classes/classes/Scenes/NPCs/UrtaScene.as: public class UrtaScene extends NPCAwareContent implements TimeAwareInterface { @ 9TlaoP2e321L99VbSV5K01tXb3sffo8Sid0B9nw2MJ8pe5lD7TDcpV1ho9rB6qveIy4pu8n20t2csz6Yk5cCeFS8wI1ej0QC
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('AMILY_NEED_TO_FREAK_ABOUT_URTA', int, 0, HGGKFlags.AMILY_NEED_TO_FREAK_ABOUT_URTA)
        self.addKFlag('AMILY_VISITING_URTA', int, 0, HGGKFlags.AMILY_VISITING_URTA)
        self.addKFlag('DISCUSSED_LUBE_SPRAY', int, 0, HGGKFlags.DISCUSSED_LUBE_SPRAY)
        self.addKFlag('DISCUSSED_URTA_ALCOHOLISM', int, 0, HGGKFlags.DISCUSSED_URTA_ALCOHOLISM)
        self.addKFlag('EDRYN_NUMBER_OF_KIDS', int, 0, HGGKFlags.EDRYN_NUMBER_OF_KIDS)
        self.addKFlag('FIRST_TIME_AT_URTA_HOUSE', int, 0, HGGKFlags.FIRST_TIME_AT_URTA_HOUSE)
        self.addKFlag('KATHERINE_URTA_AFFECTION', int, 0, HGGKFlags.KATHERINE_URTA_AFFECTION)
        self.addKFlag('KATHERINE_URTA_DATE', int, 0, HGGKFlags.KATHERINE_URTA_DATE, choices={
            'LITTLE': 0,
            'WHENEVER': 1,
            'LOTS': 2,
        })
        self.addKFlag('NEED_URTA_LETTER', int, 0, HGGKFlags.NEED_URTA_LETTER)
        self.addKFlag('PC_DISLIKES_URTA_AND_EDRYN_TOGETHER', int, 0, HGGKFlags.PC_DISLIKES_URTA_AND_EDRYN_TOGETHER)
        self.addKFlag('PC_SEEN_URTA_SEX_TOYS', int, 0, HGGKFlags.PC_SEEN_URTA_SEX_TOYS)
        self.addKFlag('TIMES_CAUGHT_URTA_WITH_SCYLLA', int, 0, HGGKFlags.TIMES_CAUGHT_URTA_WITH_SCYLLA)
        self.addKFlag('TIMES_FUCKED_URTA', int, 0, HGGKFlags.TIMES_FUCKED_URTA)
        self.addKFlag('TIMES_MASSAGED_URTA_BELLY', int, 0, HGGKFlags.TIMES_MASSAGED_URTA_BELLY)
        self.addKFlag('TIMES_NURSED_FROM_URTA', int, 0, HGGKFlags.TIMES_NURSED_FROM_URTA)
        self.addKFlag('TIMES_RUT_FUCKED_URTAS_CUNT', int, 0, HGGKFlags.TIMES_RUT_FUCKED_URTAS_CUNT)
        self.addKFlag('TIMES_URTA_BOOB_WORSHIPPED', int, 0, HGGKFlags.TIMES_URTA_BOOB_WORSHIPPED)
        self.addKFlag('URTA_AMILY_FOLLOWUP_NEEDED', int, 0, HGGKFlags.URTA_AMILY_FOLLOWUP_NEEDED)
        self.addKFlag('URTA_ANGRY_AT_PC_COUNTDOWN', int, 0, HGGKFlags.URTA_ANGRY_AT_PC_COUNTDOWN)
        self.addKFlag('URTA_BANNED_FROM_SCYLLA', int, 0, HGGKFlags.URTA_BANNED_FROM_SCYLLA)
        self.addKFlag('URTA_COMFORTABLE_WITH_OWN_BODY', int, 0, HGGKFlags.URTA_COMFORTABLE_WITH_OWN_BODY)
        self.addKFlag('URTA_CONFRONTED_SCYLLA', int, 0, HGGKFlags.URTA_CONFRONTED_SCYLLA)
        self.addKFlag('URTA_CUM_NO_CUM_DAYS', int, 0, HGGKFlags.URTA_CUM_NO_CUM_DAYS)
        self.addKFlag('URTA_DRINK_FREQUENCY', int, 0, HGGKFlags.URTA_DRINK_FREQUENCY)
        self.addKFlag('URTA_EGGS', int, 0, HGGKFlags.URTA_EGGS)
        self.addKFlag('URTA_EGG_FORCE_EVENT', int, 0, HGGKFlags.URTA_EGG_FORCE_EVENT)
        self.addKFlag('URTA_FAMILY_TALK_ATTEMPTS', int, 0, HGGKFlags.URTA_FAMILY_TALK_ATTEMPTS)
        self.addKFlag('URTA_FERTILE', int, 0, HGGKFlags.URTA_FERTILE)
        self.addKFlag('URTA_FERTILE_EGGS', int, 0, HGGKFlags.URTA_FERTILE_EGGS)
        self.addKFlag('URTA_FIRSTBORN_COCKTYPE', int, 0, HGGKFlags.URTA_FIRSTBORN_COCKTYPE)
        self.addKFlag('URTA_FIRSTBORN_GENDER', int, 0, HGGKFlags.URTA_FIRSTBORN_GENDER)
        self.addKFlag('URTA_FLATBELLY_NOTICE', int, 0, HGGKFlags.URTA_FLATBELLY_NOTICE)
        # PregnancyStore vag incubation counter
        self.addKFlag('URTA_INCUBATION', int, 0, HGGKFlags.URTA_INCUBATION)
        self.addKFlag('URTA_INCUBATION_CELEBRATION', int, 0, HGGKFlags.URTA_INCUBATION_CELEBRATION)
        self.addKFlag('URTA_KIDS_FEMALES', int, 0, HGGKFlags.URTA_KIDS_FEMALES)
        self.addKFlag('URTA_KIDS_HERMS', int, 0, HGGKFlags.URTA_KIDS_HERMS)
        self.addKFlag('URTA_KIDS_MALES', int, 0, HGGKFlags.URTA_KIDS_MALES)
        self.addKFlag('URTA_KNOWS_PC_HAS_MARBLE_FOLLOWER', int, 0, HGGKFlags.URTA_KNOWS_PC_HAS_MARBLE_FOLLOWER)
        self.addKFlag('URTA_LATESTBORN_COCKTYPE', int, 0, HGGKFlags.URTA_LATESTBORN_COCKTYPE)
        self.addKFlag('URTA_LATESTBORN_GENDER', int, 0, HGGKFlags.URTA_LATESTBORN_GENDER)
        self.addKFlag('URTA_LUNCH_PLAY', int, 0, HGGKFlags.URTA_LUNCH_PLAY)
        self.addKFlag('URTA_OPEN_ABOUT_EDRYN', int, 0, HGGKFlags.URTA_OPEN_ABOUT_EDRYN)
        self.addKFlag('URTA_PC_AFFECTION_COUNTER', int, 0, HGGKFlags.URTA_PC_AFFECTION_COUNTER)
        self.addKFlag('URTA_PC_LOVE_COUNTER', int, 0, HGGKFlags.URTA_PC_LOVE_COUNTER)
        self.addKFlag('URTA_PETPLAY_DONE', int, 0, HGGKFlags.URTA_PETPLAY_DONE)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('URTA_PREGNANCY_TYPE', int, 0, HGGKFlags.URTA_PREGNANCY_TYPE)
        self.addKFlag('URTA_PREGNANT_DELIVERY_SCENE', int, 0, HGGKFlags.URTA_PREGNANT_DELIVERY_SCENE)
        self.addKFlag('URTA_PREG_EVERYBODY', int, 0, HGGKFlags.URTA_PREG_EVERYBODY)
        self.addKFlag('URTA_QUEST_STATUS', int, 0, HGGKFlags.URTA_QUEST_STATUS)
        self.addKFlag('URTA_SCYLLA_BIG_DICK_TIMES_DONE', int, 0, HGGKFlags.URTA_SCYLLA_BIG_DICK_TIMES_DONE)
        self.addKFlag('URTA_TENTACLE_GAPED', int, 0, HGGKFlags.URTA_TENTACLE_GAPED)
        self.addKFlag('URTA_TIMES_BIRTHED', int, 0, HGGKFlags.URTA_TIMES_BIRTHED)
        self.addKFlag('URTA_TIMES_EGG_PREGGED', int, 0, HGGKFlags.URTA_TIMES_EGG_PREGGED)
        self.addKFlag('URTA_TIMES_PC_BIRTHED', int, 0, HGGKFlags.URTA_TIMES_PC_BIRTHED)
        self.addKFlag('URTA_TIME_SINCE_LAST_CAME', int, 0, HGGKFlags.URTA_TIME_SINCE_LAST_CAME)
        self.addKFlag('URTA_VIXEN_AND_CREAM_COUNTER', int, 0, HGGKFlags.URTA_VIXEN_AND_CREAM_COUNTER)
        self.addKFlag('URTA_X_RAPHAEL_HAPPENED', int, 0, HGGKFlags.URTA_X_RAPHAEL_HAPPENED)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.URTA_PREGNANCY_TYPE, HGGKFlags.URTA_INCUBATION)
