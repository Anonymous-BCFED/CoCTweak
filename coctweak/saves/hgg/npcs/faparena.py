from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class FapArenaNPC(BaseNPC):
    ID = 'faparena'
    NAME = 'FapArena'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/FapArena.as: public class FapArena extends BazaarAbstractContent { @ eKf2NIfp5dPYeKTaurbxWeZJ8Hkd5877DaDU9Wldwc9XAfVVd4wcj7bsT86T2ryb04f485Kc0WN5LK6us9bSg6g8pm0GF16i
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('FAP_ARENA_RULES_EXPLAINED', int, 0, HGGKFlags.FAP_ARENA_RULES_EXPLAINED, intbool=True)
        self.addKFlag('FAP_ARENA_SESSIONS', int, 0, HGGKFlags.FAP_ARENA_SESSIONS, min=0)
        self.addKFlag('FAP_ARENA_VICTORIES', int, 0, HGGKFlags.FAP_ARENA_VICTORIES, min=0)
