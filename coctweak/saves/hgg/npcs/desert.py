from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class DesertEncounterNPC(BaseNPC):
    ID = 'desert'
    NAME = 'Desert'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Desert.as: public class Desert extends BaseContent implements IExplorable, SelfSaving, SelfDebug { @ 4fe2e9ayd7x824v2ux0QIcdJb5XdCDbV48gQ7Lh7Mc7T8242b3hetl38XfXFeX8aiP08mdmFc2G3dJ9RI7rB7XxcYLbZD0Df
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "foundMirage": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('foundMirage', bool, False, 'desert', 'foundMirage')
        ##########
        # kFlags
        ##########
        self.addKFlag('ANTS_PC_FAILED_PHYLLA', int, 0, HGGKFlags.ANTS_PC_FAILED_PHYLLA)
        self.addKFlag('ANT_COLONY_KEPT_HIDDEN', int, 0, HGGKFlags.ANT_COLONY_KEPT_HIDDEN)
        self.addKFlag('ANT_WAIFU', int, 0, HGGKFlags.ANT_WAIFU)
        self.addKFlag('CUM_WITCHES_FIGHTABLE', int, 0, HGGKFlags.CUM_WITCHES_FIGHTABLE)
        self.addKFlag('DISCOVERED_WITCH_DUNGEON', int, 0, HGGKFlags.DISCOVERED_WITCH_DUNGEON)
        self.addKFlag('FOUND_WIZARD_STAFF', int, 0, HGGKFlags.FOUND_WIZARD_STAFF)
        self.addKFlag('SAND_WITCH_LEAVE_ME_ALONE', int, 0, HGGKFlags.SAND_WITCH_LEAVE_ME_ALONE)
        self.addKFlag('TIMES_EXPLORED_DESERT', int, 0, HGGKFlags.TIMES_EXPLORED_DESERT)
