from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class CorruptedWitchNPC(BaseNPC):
    ID = 'corruptedwitch'
    NAME = 'CorruptedWitch'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/VolcanicCrag/CorruptedWitchScene.as: public class CorruptedWitchScene extends BaseContent { @ coQ9Pw27b4AN8NA8VBcsr9so3cL85C6BB6WR8Nm18K8QzbqqfWn9ry1nP5Ai9lw7WR9aTfOd9bQ1uhd0HeCvcAV4Xy447fir
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('VOLCWITCHNUMBEROFBIRTHS', int, 0, HGGKFlags.VOLCWITCHNUMBEROFBIRTHS)
        self.addKFlag('VOLCWITCHNUMBEROFCHILDREN', int, 0, HGGKFlags.VOLCWITCHNUMBEROFCHILDREN)
