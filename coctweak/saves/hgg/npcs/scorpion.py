from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class ScorpionNPC(BaseNPC):
    ID = 'scorpion'
    NAME = 'Scorpion'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Desert/ScorpionScene.as: public class ScorpionScene extends BaseContent implements Encounter, SelfSaving, SelfDebug { @ 6dj2Xv25ugwzcreb8Id3o2790Bb4f45a345v4q1ciceCpgmkeoQ7OA8pR7xtdGY8Ce4bK1QK3j7f6BgOR8eg9j2bWc57Egf1
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "state": [
                "IntList",
                "The overall state of the scorpion",
                [
                    {
                        "label": "Disabled",
                        "data": -1
                    },
                    {
                        "label": "Unencountered",
                        "data": 0
                    },
                    {
                        "label": "Encountered",
                        "data": 1
                    }
                ]
            ],
            "fate": [
                "IntList",
                "How the encounter ended",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Killed",
                        "data": 1
                    },
                    {
                        "label": "Spared",
                        "data": 2
                    },
                    {
                        "label": "Followed You",
                        "data": 3
                    },
                    {
                        "label": "Ridden",
                        "data": 4
                    }
                ]
            ],
            "talked": [
                "Boolean",
                "Whether you tried to talk to it"
            ]
        }
        '''
        # 0 = n/a; 1 = killed; 2 = ignored; 3 = followed; 4 = ridden
        self.addSSOProperty('fate', int, 0, 'scorpion', 'fate', notes='How the encounter ended', choices={
            0: 'N/A',
            1: 'Killed',
            2: 'Spared',
            3: 'Followed You',
            4: 'Ridden',
        })
        # -1 = disabled; 0 = n/a; 1 = encountered
        self.addSSOProperty('state', int, 0, 'scorpion', 'state', notes='The overall state of the scorpion', choices={
            -1: 'Disabled',
            0: 'Unencountered',
            1: 'Encountered',
        })
        self.addSSOProperty('talked', bool, False, 'scorpion', 'talked', notes='Whether you tried to talk to it')
