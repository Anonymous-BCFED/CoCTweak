from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class FemaleSpiderNPC(BaseNPC):
    ID = 'femalespider'
    NAME = 'FemaleSpider'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Swamp/FemaleSpiderMorphScene.as: public class FemaleSpiderMorphScene extends BaseContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 7Yg1UD6xJd2T0Y729abi8dsO5K12GmchPd6scbX7m95VI4ztbBG2IBfQb7J09L78Xg6NkfPZ8Bm8UY8ycgBA6NT7ae9Cc9CF
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "spidersKilled": [
                "Int",
                ""
            ]
        }
        '''
        self.addSSOProperty('spidersKilled', int, 0, 'femalespidermorph', 'spidersKilled')
        ##########
        # kFlags
        ##########
        # PregnancyStore vag incubation counter
        self.addKFlag('FEMALE_SPIDERMORPH_PREGNANCY_INCUBATION', int, 0, HGGKFlags.FEMALE_SPIDERMORPH_PREGNANCY_INCUBATION)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('FEMALE_SPIDERMORPH_PREGNANCY_TYPE', int, 0, HGGKFlags.FEMALE_SPIDERMORPH_PREGNANCY_TYPE)
        self.addKFlag('TIMES_ENCOUNTERED_FEMALE_SPIDERMORPHS', int, 0, HGGKFlags.TIMES_ENCOUNTERED_FEMALE_SPIDERMORPHS)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.FEMALE_SPIDERMORPH_PREGNANCY_TYPE, HGGKFlags.FEMALE_SPIDERMORPH_PREGNANCY_INCUBATION)
