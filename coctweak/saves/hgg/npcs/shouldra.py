from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class ShouldraNPC(BaseNPC):
    ID = 'shouldra'
    NAME = 'Shouldra'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/Shouldra.as: public class Shouldra extends Monster { @ 8m21aQdh08mNchc7rR8jN9ozfsw6NL70S1xm7VJbKK0HsaXDgBYdDk0tndpqfME56mfKHgbF2R10lP0P8b9d3dBf3p7vh6Q8
        ## from [HGG]/classes/classes/Scenes/NPCs/ShouldraFollower.as: public class ShouldraFollower extends NPCAwareContent implements SelfSaving, SelfDebug { @ 9H66s64YPb0z26H9HIelR4lygI9dFc9KY6rYd07eib8ndfEZ11B7tM4Q93FS95C7457nE6FbcTs6BDd6556q2EW5Ppd6jdbi
        ## from [HGG]/classes/classes/Scenes/NPCs/ShouldraScene.as: public class ShouldraScene extends NPCAwareContent implements TimeAwareInterface { @ 4SJh1SaLEbZJ67GfvDflygxwcmy8iHcUcacF9sj2mlfrsdOu8s59pFaTk5SZ5dA9ohe5K1KP7HadrA0qK21tfAk3ymaoH6Sk
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "mastIntro": [
                "Boolean",
                ""
            ],
            "frustrationKnown": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('frustrationKnown', bool, False, 'shouldra', 'frustrationKnown')
        self.addSSOProperty('mastIntro', bool, False, 'shouldra', 'mastIntro')
        ##########
        # kFlags
        ##########
        self.addKFlag('GHOST_GIRL_SLIME_X_SHOULDRA_COUNTER', int, 0, HGGKFlags.GHOST_GIRL_SLIME_X_SHOULDRA_COUNTER)
        self.addKFlag('SHOULDRA_BAKERY_TIMES', int, 0, HGGKFlags.SHOULDRA_BAKERY_TIMES)
        self.addKFlag('SHOULDRA_EXGARTUAN_SPIRIT_SEX_COUNT', int, 0, HGGKFlags.SHOULDRA_EXGARTUAN_SPIRIT_SEX_COUNT)
        self.addKFlag('SHOULDRA_EXGARTUDRAMA', int, 0, HGGKFlags.SHOULDRA_EXGARTUDRAMA)
        self.addKFlag('SHOULDRA_FOLLOWER_STATE', int, 0, HGGKFlags.SHOULDRA_FOLLOWER_STATE)
        self.addKFlag('SHOULDRA_GENDERLESS_FUCK_COUNT', int, 0, HGGKFlags.SHOULDRA_GENDERLESS_FUCK_COUNT)
        self.addKFlag('SHOULDRA_HERMSEX_COUNT', int, 0, HGGKFlags.SHOULDRA_HERMSEX_COUNT)
        self.addKFlag('SHOULDRA_MAGIC_COOLDOWN', int, 0, HGGKFlags.SHOULDRA_MAGIC_COOLDOWN)
        self.addKFlag('SHOULDRA_MET_VALA', int, 0, HGGKFlags.SHOULDRA_MET_VALA)
        self.addKFlag('SHOULDRA_PALADIN_MAIDEN_COUNTDOWN', int, 0, HGGKFlags.SHOULDRA_PALADIN_MAIDEN_COUNTDOWN)
        self.addKFlag('SHOULDRA_PALADIN_MAIDEN_COUNTER', int, 0, HGGKFlags.SHOULDRA_PALADIN_MAIDEN_COUNTER)
        self.addKFlag('SHOULDRA_PENIS_DEFEAT_TIMES', int, 0, HGGKFlags.SHOULDRA_PENIS_DEFEAT_TIMES)
        self.addKFlag('SHOULDRA_PLOT_COUNTDOWN', int, 0, HGGKFlags.SHOULDRA_PLOT_COUNTDOWN)
        self.addKFlag('SHOULDRA_PLOT_LEVEL', int, 0, HGGKFlags.SHOULDRA_PLOT_LEVEL)
        self.addKFlag('SHOULDRA_SLEEP_TIMER', int, 0, HGGKFlags.SHOULDRA_SLEEP_TIMER)
        self.addKFlag('SHOULDRA_SLIME_PENOR_TIMES', int, 0, HGGKFlags.SHOULDRA_SLIME_PENOR_TIMES)
        self.addKFlag('SHOULDRA_TALK_NEEDED', int, 0, HGGKFlags.SHOULDRA_TALK_NEEDED)
        self.addKFlag('SHOULDRA_TIMES_NIGHT_RAPED_PC', int, 0, HGGKFlags.SHOULDRA_TIMES_NIGHT_RAPED_PC)
        self.addKFlag('SHOULDRA_TONGUE_LICKS_TIMES', int, 0, HGGKFlags.SHOULDRA_TONGUE_LICKS_TIMES)
        self.addKFlag('SHOULDRA_USES_YOUR_GIANT_COCK_COUNT', int, 0, HGGKFlags.SHOULDRA_USES_YOUR_GIANT_COCK_COUNT)
        self.addKFlag('SHOULDRA_VAGINAL_POSSESSIONS', int, 0, HGGKFlags.SHOULDRA_VAGINAL_POSSESSIONS)
        self.addKFlag('SHOULDRA_WORM_SCENE_COUNTER', int, 0, HGGKFlags.SHOULDRA_WORM_SCENE_COUNTER)
        self.addKFlag('SLIMEGINAED', int, 0, HGGKFlags.SLIMEGINAED)
        self.addKFlag('TIMED_SHARKGINAS', int, 0, HGGKFlags.TIMED_SHARKGINAS)
        self.addKFlag('TIMES_BEATEN_SHOULDRA', int, 0, HGGKFlags.TIMES_BEATEN_SHOULDRA)
        self.addKFlag('TIMES_MET_SHOULDRA', int, 0, HGGKFlags.TIMES_MET_SHOULDRA)
        self.addKFlag('TIMES_POSSESSED_BY_SHOULDRA', int, 0, HGGKFlags.TIMES_POSSESSED_BY_SHOULDRA)
        self.addKFlag('TIMES_SHARKPENISED', int, 0, HGGKFlags.TIMES_SHARKPENISED)
