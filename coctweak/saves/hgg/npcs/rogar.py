from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class RogarNPC(BaseNPC):
    ID = 'rogar'
    NAME = 'Rogar'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Swamp/Rogar.as: public class Rogar extends BaseContent { @ ceD8ue9GA3fy1lv1989CjgZ61Wd3do31J0Lz3F2ffh2z80TH86wega2EMgsnahc8zGfk87Kq5cIbwz3fR0PB4dlc327dw7vS
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('ROGAR_DIRT', int, 0, HGGKFlags.ROGAR_DIRT, choices={
            0: 'Player has not tongue bathed Rogar',
            1: 'Player declined',
            2: 'Player gave tongue bath',
        })
        self.addKFlag('ROGAR_DISABLED', int, 0, HGGKFlags.ROGAR_DISABLED, notes='Rogar can set this if you put him off.', intbool=True)
        self.addKFlag('ROGAR_FUCKED_TODAY', int, 0, HGGKFlags.ROGAR_FUCKED_TODAY, intbool=True)
        self.addKFlag('ROGAR_PHASE', int, 0, HGGKFlags.ROGAR_PHASE, choices={
            0: 'Unmet',
            1: 'Bath/blowjob chance',
            2: "Rogar moves to Tel'Adre",
            3: 'Rogar is Cloaked Figure at the Wet Bitch.',
            4: 'Rogar is Rogar at the Wet Bitch.',
        })
        self.addKFlag('ROGAR_WARNING', int, 0, HGGKFlags.ROGAR_WARNING, choices={
            0: 'Player perceived as masculine',
            1: 'Player perceived as feminine',
        })
