from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class SylviaNPC(BaseNPC):
    ID = 'sylvia'
    NAME = 'Sylvia'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/Sylvia.as: public class Sylvia extends Monster { @ 3Yq59l3jHcjpcOJbmB4a2ffz4wCdhofPs0r5eRY2Mx0Xg3Gr3lh3St6Q9a1Ia2b9qo14U823coL9PM7lI4pH2I35mi6d99oE
        ## from [HGG]/classes/classes/Scenes/NPCs/SylviaScene.as: public class SylviaScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface, Encounter { @ 5Sw1Oq2iUaIfcpy8M58Tt9Hn8oI0bi9O11OQ16C1ft4OD62M8vL2uZbHTefW6Od40w3qMfJG9YX0QYcTD11r5zxeqV3NPaBW
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "sylviaProgress": [
                "IntList",
                "Tracks overall scene to scene progress",
                [
                    {
                        "label": "Disabled",
                        "data": -1
                    },
                    {
                        "label": "Unencountered",
                        "data": 0
                    },
                    {
                        "label": "Encountered",
                        "data": 1
                    },
                    {
                        "label": "Learned Name",
                        "data": 2
                    },
                    {
                        "label": "Stalking",
                        "data": 3
                    },
                    {
                        "label": "Cave",
                        "data": 4
                    },
                    {
                        "label": "Capstone",
                        "data": 5
                    }
                ]
            ],
            "sylviaAffection": [
                "Int",
                "Sylvia's affection"
            ],
            "sylviaDominance": [
                "Int",
                "Sylvia's dominance over the player"
            ],
            "sylviaStalking": [
                "IntList",
                "Tracks whether you let her keep stalking you/the progression of the stalking scenes",
                [
                    {
                        "label": "Disabled",
                        "data": -1
                    },
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Enabled",
                        "data": 1
                    },
                    {
                        "label": "Encounter 1",
                        "data": 2
                    },
                    {
                        "label": "Encounter 2",
                        "data": 3
                    },
                    {
                        "label": "Encounter 3",
                        "data": 4
                    }
                ]
            ],
            "sylviaFertile": [
                "IntList",
                "Tracks whether she can get pregnant",
                [
                    {
                        "label": "Disabled",
                        "data": -1
                    },
                    {
                        "label": "Undecided",
                        "data": 0
                    },
                    {
                        "label": "Enabled",
                        "data": 1
                    }
                ]
            ],
            "sylviaCapstoneCounter": [
                "IntList",
                "Tracks progress towards capstone scene",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "First",
                        "data": 1
                    },
                    {
                        "label": "Second",
                        "data": 2
                    },
                    {
                        "label": "Third",
                        "data": 3
                    }
                ]
            ],
            "sylviaClothes": [
                "Int",
                "Tracks whether you've unlocked clothing donation/how much you've given"
            ],
            "timeSinceVisit": [
                "Int",
                "How many hours it's been since you've visited Sylvia"
            ],
            "encounterDisabled": [
                "Int",
                "Prevents encounters at this total number of hours elapsed"
            ],
            "talkedMoths": [
                "Boolean",
                "Tracks whether you've talked about moths"
            ],
            "sylviaGiftedDress": [
                "Boolean",
                "Tracks whether you made and gifted her the dress"
            ],
            "unlockedOyakodon": [
                "Boolean",
                "Tracks whether you've unlocked threesomes with Dolores"
            ]
        }
        '''
        # Gets set to current time
        self.addSSOProperty('encounterDisabled', int, 0, 'sylvia', 'encounterDisabled', notes='Prevents encounters at this total number of hours elapsed')
        self.addSSOProperty('sylviaAffection', int, 0, 'sylvia', 'sylviaAffection', notes="Sylvia's affection")
        # Tracks progress towards capstone scene
        self.addSSOProperty('sylviaCapstoneCounter', int, 0, 'sylvia', 'sylviaCapstoneCounter', notes='Tracks progress towards capstone scene', choices={
            0: 'N/A',
            1: 'First',
            2: 'Second',
            3: 'Third',
        })
        # Tracks whether you've unlocked clothing donation/how much you've given
        self.addSSOProperty('sylviaClothes', int, 0, 'sylvia', 'sylviaClothes', notes="Tracks whether you've unlocked clothing donation/how much you've given")
        self.addSSOProperty('sylviaDominance', int, 0, 'sylvia', 'sylviaDominance', notes="Sylvia's dominance over the player")
        # Tracks whether she can get pregnant
        self.addSSOProperty('sylviaFertile', int, 0, 'sylvia', 'sylviaFertile', notes='Tracks whether she can get pregnant', choices={
            -1: 'Disabled',
            0: 'Undecided',
            1: 'Enabled',
        })
        # Track whether you made and gifted her the dress
        self.addSSOProperty('sylviaGiftedDress', bool, False, 'sylvia', 'sylviaGiftedDress', notes='Tracks whether you made and gifted her the dress')
        # Tracks overall scene to scene progress
        self.addSSOProperty('sylviaProgress', int, 0, 'sylvia', 'sylviaProgress', notes='Tracks overall scene to scene progress', choices={
            -1: 'Disabled',
            0: 'Unencountered',
            1: 'Encountered',
            2: 'Learned Name',
            3: 'Stalking',
            4: 'Cave',
            5: 'Capstone',
        })
        # Tracks whether you let her keep stalking you/the progression of the stalking scenes
        self.addSSOProperty('sylviaStalking', int, 0, 'sylvia', 'sylviaStalking', notes='Tracks whether you let her keep stalking you/the progression of the stalking scenes', choices={
            -1: 'Disabled',
            0: 'N/A',
            1: 'Enabled',
            2: 'Encounter 1',
            3: 'Encounter 2',
            4: 'Encounter 3',
        })
        # Tracks whether you've talked about moths with her
        self.addSSOProperty('talkedMoths', bool, False, 'sylvia', 'talkedMoths', notes="Tracks whether you've talked about moths")
        # How many hours it's been since you've visited Sylvia
        self.addSSOProperty('timeSinceVisit', int, 0, 'sylvia', 'timeSinceVisit', notes="How many hours it's been since you've visited Sylvia")
        # Tracks whether you've unlocked threesomes with Dolores
        self.addSSOProperty('unlockedOyakodon', bool, False, 'sylvia', 'unlockedOyakodon', notes="Tracks whether you've unlocked threesomes with Dolores")
        ##########
        # kFlags
        ##########
        # PregnancyStore vag incubation counter
        self.addKFlag('SYLVIA_PREGNANCY_INCUBATION', int, 0, HGGKFlags.SYLVIA_PREGNANCY_INCUBATION)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('SYLVIA_PREGNANCY_TYPE', int, 0, HGGKFlags.SYLVIA_PREGNANCY_TYPE)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.SYLVIA_PREGNANCY_TYPE, HGGKFlags.SYLVIA_PREGNANCY_INCUBATION)
