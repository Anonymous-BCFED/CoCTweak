from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class AikoNPC(BaseNPC):
    ID = 'aiko'
    NAME = 'Aiko'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/Aiko.as: public class Aiko extends BaseKitsune { @ eYFg1V3v996TdSN85b0JVanN3uD8xQ46w9t17uh9Wme7Eb0mfHA2Q52qaeM45VAfczazz5YcdGmcLO28e0Yu5AZ5Gn0MM9hL
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/AikoScene.as: public class AikoScene extends BaseContent implements Encounter, SelfSaving, SelfDebug, TimeAwareInterface { @ 2hv2VEaPz2fie3LaPfgYLaRa8GU3xr9ujfjSfmOake3eOeoJ8ns7frfqT0T09QBcOj8HCcyp7bGeCV6Zserr5aI3Bh8xBa4j
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "akbalDone": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('akbalDone', bool, False, 'aiko', 'akbalDone')
        ##########
        # kFlags
        ##########
        self.addKFlag('AIKO_AFFECTION', int, 0, HGGKFlags.AIKO_AFFECTION)
        self.addKFlag('AIKO_APOLOGY_SINCERE', int, 0, HGGKFlags.AIKO_APOLOGY_SINCERE)
        self.addKFlag('AIKO_BALL_RETURNED', int, 0, HGGKFlags.AIKO_BALL_RETURNED)
        self.addKFlag('AIKO_BOSS_COMPLETE', int, 0, HGGKFlags.AIKO_BOSS_COMPLETE)
        self.addKFlag('AIKO_BOSS_INTRO', int, 0, HGGKFlags.AIKO_BOSS_INTRO)
        self.addKFlag('AIKO_BOSS_OUTRO', int, 0, HGGKFlags.AIKO_BOSS_OUTRO)
        self.addKFlag('AIKO_CORRUPTION', int, 0, HGGKFlags.AIKO_CORRUPTION, min=0, max=100)
        self.addKFlag('AIKO_FIGHT_WON', int, 0, HGGKFlags.AIKO_FIGHT_WON)
        self.addKFlag('AIKO_FIRST_CHOICE', int, 0, HGGKFlags.AIKO_FIRST_CHOICE)
        self.addKFlag('AIKO_HAS_FOUGHT', int, 0, HGGKFlags.AIKO_HAS_FOUGHT)
        self.addKFlag('AIKO_HOT_BLOOD', int, 0, HGGKFlags.AIKO_HOT_BLOOD)
        self.addKFlag('AIKO_RAPE', int, 0, HGGKFlags.AIKO_RAPE)
        self.addKFlag('AIKO_SEXED', int, 0, HGGKFlags.AIKO_SEXED)
        self.addKFlag('AIKO_SPAR_VICTORIES', int, 0, HGGKFlags.AIKO_SPAR_VICTORIES)
        self.addKFlag('AIKO_TALK_AIKO', int, 0, HGGKFlags.AIKO_TALK_AIKO)
        self.addKFlag('AIKO_TALK_ARCHERY', int, 0, HGGKFlags.AIKO_TALK_ARCHERY)
        self.addKFlag('AIKO_TALK_CULTURE', int, 0, HGGKFlags.AIKO_TALK_CULTURE)
        self.addKFlag('AIKO_TIMES_MET', int, 0, HGGKFlags.AIKO_TIMES_MET)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('AikoLustPrank', bool, False, HGGStatusLib.AikoLustPrank, None)
