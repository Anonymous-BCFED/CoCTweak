from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class GnollSpearThrowerNPC(BaseNPC):
    ID = 'gnollspearthrower'
    NAME = 'GnollSpearThrower'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Plains/GnollSpearThrower.as: public class GnollSpearThrower extends Monster { @ bFA7iY5Ta9TecdI6zU1sm2vzaUZ54deQ3gxM8ad4d2cqQ2He2ZafjR6cV8zA2cGeWv0CubHp4zh7Fb1ws8RMdNS1RV6HfgsJ
        ## from [HGG]/classes/classes/Scenes/Areas/Plains/GnollSpearThrowerScene.as: public class GnollSpearThrowerScene extends BaseContent { @ cky8n4aeCbd2g7tbroe36bxaa0Y7hJ2B2b2bgTq8Pk54v1W92JV4pxebxh31dPc3RXf4N4x01gr9oObKJ7xA9cn2GVenR6Fm
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('HAVE_ENCOUNTERED_GNOLL_PLAINS', int, 0, HGGKFlags.HAVE_ENCOUNTERED_GNOLL_PLAINS)
