from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class TentacleBeastNPC(BaseNPC):
    ID = 'tentaclebeast'
    NAME = 'TentacleBeast'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/TentacleBeastScene.as: public class TentacleBeastScene extends BaseContent implements SelfSaving, SelfDebug { @ 8p4fbi7xpdhZf9r402bXkeZW727daL63d5mm3QRcfzcgl7UP7b45qvf5JgV2a924IXarl3Br8bd0lvdbg5jEeg89yK1ZT9eD
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "timesLost": [
                "Int",
                ""
            ]
        }
        '''
        self.addSSOProperty('timesLost', int, 0, 'tentaclebeast', 'timesLost')
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('TentacleBadEndCounter:0', int, 0, HGGStatusLib.TentacleBadEndCounter, 0, min=0, notes="How many times you lost via lust. Triggers a bad end if you're a herm with >=50 corruption, and this value is >=3.")
