from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class AnneMarieNPC(BaseNPC):
    ID = 'annemarie'
    NAME = 'AnneMarie'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/AnneMarieScene.as: public class AnneMarieScene extends BaseContent { @ aCK7qebDa85QgJF19s0cK4vF90h8co1uFfwL4fU9uL65B1Ylce03BS7wfe0Ae685UkbL0caz4HLcYD8nCc9paUo3R4gLNenW
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('ANNEMARIE_STATUS', int, 0, HGGKFlags.ANNEMARIE_STATUS, bitflags={
            1: 'MET_ANNE',
            2: 'RUDE_TO_ANNE',
        })
