from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class ExgartuanNPC(BaseNPC):
    ID = 'exgartuan'
    NAME = 'Exgartuan'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/Exgartuan.as: public class Exgartuan extends NPCAwareContent implements TimeAwareInterface { @ 63o15P1WFau989ddY9gL82958Q78FCcFLfnF9Ukfgpd452Pd3ZN0ol0Nj0mM5mLb775ri5uM47P5Hc1Lp08r1Xe7i181vcm4
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('BOOBGARTUAN_SURPRISE_COUNT', int, 0, HGGKFlags.BOOBGARTUAN_SURPRISE_COUNT)
        self.addKFlag('EXGARTUAN_TIGHTPANTS_MASTURBATE_COUNT', int, 0, HGGKFlags.EXGARTUAN_TIGHTPANTS_MASTURBATE_COUNT)
        self.addKFlag('TIMES_AUTOFELLATIOED_EXGARTUAN', int, 0, HGGKFlags.TIMES_AUTOFELLATIOED_EXGARTUAN)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('Exgartuan', bool, False, HGGStatusLib.Exgartuan, None)
