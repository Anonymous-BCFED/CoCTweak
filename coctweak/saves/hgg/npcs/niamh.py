from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class NiamhNPC(BaseNPC):
    ID = 'niamh'
    NAME = 'Niamh'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Niamh.as: public class Niamh extends TelAdreAbstractContent implements TimeAwareInterface { @ 1Kr2Ci27t0ABa874vR8HFcyWeYT3t91pkaynd7b65T9Wz6T00L9gPXfLF3kB4Yr1Akg9X17cetk0Bjbyzftn03jgLQcwxgkJ
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('GOT_NIAMH_BEER', int, 0, HGGKFlags.GOT_NIAMH_BEER)
        self.addKFlag('MET_NIAMH', int, 0, HGGKFlags.MET_NIAMH)
        self.addKFlag('NIAMH_MOVED_OUT_COUNTER', int, 0, HGGKFlags.NIAMH_MOVED_OUT_COUNTER)
        self.addKFlag('NIAMH_SEAN_BREW_BIMBO_LIQUEUR_COUNTER', int, 0, HGGKFlags.NIAMH_SEAN_BREW_BIMBO_LIQUEUR_COUNTER)
        self.addKFlag('NIAMH_STATUS', int, 0, HGGKFlags.NIAMH_STATUS, notes='What kind of tainting was done, if any?', choices={
            0: 'None',
            1: 'Succubus Milk',
            2: 'Bimbo Liqueur',
        })
        self.addKFlag('TALKED_NIAMH', int, 0, HGGKFlags.TALKED_NIAMH)
        self.addKFlag('TIMES_NIAMH_BAZAAR_MET', int, 0, HGGKFlags.TIMES_NIAMH_BAZAAR_MET)
