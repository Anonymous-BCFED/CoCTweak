from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class RathazulNPC(BaseNPC):
    ID = 'rathazul'
    NAME = 'Rathazul'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/Rathazul.as: public class Rathazul extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface, Encounter { @ bKcgBvaJ3gva6lg2zMcYJeJNcQM2kj24B19TeIgaIG7e31dY4yM7mOcif2nJ44O6yK6Tq2J59XY5ebg2n7ljgEbfRRfjx4Dx
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "metRathazul": [
                "Boolean",
                ""
            ],
            "campFollower": [
                "Boolean",
                ""
            ],
            "campOffer": [
                "Boolean",
                ""
            ],
            "mixologyXP": [
                "Int",
                ""
            ],
            "offeredGel": [
                "Boolean",
                ""
            ],
            "offeredChitin": [
                "Boolean",
                ""
            ],
            "offeredSilk": [
                "Boolean",
                ""
            ],
            "offeredScale": [
                "Boolean",
                ""
            ],
            "offeredEbonbloom": [
                "Boolean",
                ""
            ],
            "offeredDye": [
                "Boolean",
                ""
            ],
            "offeredPhilter": [
                "Boolean",
                ""
            ],
            "offeredHoney": [
                "Boolean",
                ""
            ],
            "offeredReducto": [
                "Boolean",
                ""
            ],
            "offeredMarae": [
                "Boolean",
                ""
            ],
            "offeredGolemHeart": [
                "Boolean",
                ""
            ],
            "offeredTBark": [
                "Boolean",
                ""
            ],
            "offeredDBark": [
                "Boolean",
                ""
            ],
            "offeredTrice": [
                "Boolean",
                ""
            ],
            "offeredOculum": [
                "Boolean",
                ""
            ],
            "offeredPurify": [
                "Boolean",
                ""
            ],
            "offeredDemonTF": [
                "Boolean",
                ""
            ],
            "offeredDelight": [
                "Boolean",
                ""
            ],
            "offeredMinoCum": [
                "Boolean",
                ""
            ],
            "offeredLaBova": [
                "Boolean",
                ""
            ],
            "offeredDebimboPlayer": [
                "Boolean",
                ""
            ],
            "offeredDebimboSophie": [
                "Boolean",
                ""
            ],
            "offeredDebimboIzma": [
                "Boolean",
                ""
            ],
            "bearGifted": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('bearGifted', bool, False, 'rathazul', 'bearGifted')
        self.addSSOProperty('campFollower', bool, False, 'rathazul', 'campFollower')
        self.addSSOProperty('campOffer', bool, False, 'rathazul', 'campOffer')
        self.addSSOProperty('giftedBear', bool, False, 'rathazul', 'giftedBear')
        self.addSSOProperty('metRathazul', bool, False, 'rathazul', 'metRathazul')
        self.addSSOProperty('mixologyXP', int, 0, 'rathazul', 'mixologyXP')
        self.addSSOProperty('offeredChitin', bool, False, 'rathazul', 'offeredChitin')
        self.addSSOProperty('offeredDBark', bool, False, 'rathazul', 'offeredDBark')
        self.addSSOProperty('offeredDebimboPlayer', bool, False, 'rathazul', 'offeredDebimboPlayer')
        self.addSSOProperty('offeredDebimboSophie', bool, False, 'rathazul', 'offeredDebimboSophie')
        self.addSSOProperty('offeredDelight', bool, False, 'rathazul', 'offeredDelight')
        self.addSSOProperty('offeredDemonTF', bool, False, 'rathazul', 'offeredDemonTF')
        self.addSSOProperty('offeredDye', bool, False, 'rathazul', 'offeredDye')
        self.addSSOProperty('offeredEbonbloom', bool, False, 'rathazul', 'offeredEbonbloom')
        self.addSSOProperty('offeredGel', bool, False, 'rathazul', 'offeredGel')
        self.addSSOProperty('offeredGolemHeart', bool, False, 'rathazul', 'offeredGolemHeart')
        self.addSSOProperty('offeredHoney', bool, False, 'rathazul', 'offeredHoney')
        self.addSSOProperty('offeredLaBova', bool, False, 'rathazul', 'offeredLaBova')
        self.addSSOProperty('offeredLactaidTaurinum', bool, False, 'rathazul', 'offeredLactaidTaurinum')
        self.addSSOProperty('offeredMarae', bool, False, 'rathazul', 'offeredMarae')
        self.addSSOProperty('offeredMinoCum', bool, False, 'rathazul', 'offeredMinoCum')
        self.addSSOProperty('offeredOculum', bool, False, 'rathazul', 'offeredOculum')
        self.addSSOProperty('offeredPhilter', bool, False, 'rathazul', 'offeredPhilter')
        self.addSSOProperty('offeredPurify', bool, False, 'rathazul', 'offeredPurify')
        self.addSSOProperty('offeredReducto', bool, False, 'rathazul', 'offeredReducto')
        self.addSSOProperty('offeredScale', bool, False, 'rathazul', 'offeredScale')
        self.addSSOProperty('offeredSilk', bool, False, 'rathazul', 'offeredSilk')
        self.addSSOProperty('offeredTBark', bool, False, 'rathazul', 'offeredTBark')
        self.addSSOProperty('offeredTrice', bool, False, 'rathazul', 'offeredTrice')
        ##########
        # kFlags
        ##########
        self.addKFlag('AMILY_MET_RATHAZUL', int, 0, HGGKFlags.AMILY_MET_RATHAZUL)
        self.addKFlag('JOJO_RATHAZUL_INTERACTION_COUNTER', int, 0, HGGKFlags.JOJO_RATHAZUL_INTERACTION_COUNTER)
        self.addKFlag('MINERVA_PURIFICATION_RATHAZUL_TALKED', int, 0, HGGKFlags.MINERVA_PURIFICATION_RATHAZUL_TALKED)
        self.addKFlag('RATHAZUL_CAMP_INTERACTION_COUNTDOWN', int, 0, HGGKFlags.RATHAZUL_CAMP_INTERACTION_COUNTDOWN)
        self.addKFlag('RATHAZUL_SILK_ARMOR_COUNTDOWN', int, 0, HGGKFlags.RATHAZUL_SILK_ARMOR_COUNTDOWN)
        self.addKFlag('RATHAZUL_SILK_ARMOR_TYPE', int, 0, HGGKFlags.RATHAZUL_SILK_ARMOR_TYPE)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('DefenseCanopy', bool, False, HGGStatusLib.DefenseCanopy, None)
