from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class WormsNPC(BaseNPC):
    ID = 'worms'
    NAME = 'Worms'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Mountain/WormMass.as: public class WormMass extends Monster { @ 7ih6eV5cef7jaGOcEFcGmaYI7ml05k2wa8RA08HbqHeyT5vM96M3cW45ibbabnd7Jj6wD9h63NCaNJgFyaVb4hreMa7PO6As
        ## from [HGG]/classes/classes/Scenes/Areas/Mountain/WormsScene.as: public class WormsScene extends BaseContent { @ dUq0SYgBLdJk5b70Ht3t47kd36A2OR0yI3Nt0X71fDaQYdxJaTl2iH4ez5cp8VSbekgaCasG0NceKq8yxde748B8jG4ZYeHK
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('EVER_INFESTED', int, 0, HGGKFlags.EVER_INFESTED, intbool=True)
        self.addKFlag('WORMS_MASS_KILLED', int, 0, HGGKFlags.WORMS_MASS_KILLED, min=0, notes="How many worm masses you've murderized.")
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('InfestAttempted', bool, False, HGGStatusLib.InfestAttempted, None)
        self.addSFXProperty('MetWorms', bool, False, HGGStatusLib.MetWorms, None)
