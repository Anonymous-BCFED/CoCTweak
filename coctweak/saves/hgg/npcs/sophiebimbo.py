from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class SophieBimboNPC(BaseNPC):
    ID = 'sophiebimbo'
    NAME = 'SophieBimbo'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/SophieBimbo.as: public class SophieBimbo extends NPCAwareContent implements SelfSaving, SelfDebug { @ aBe6mRbqv1fL1zo2Qzbmb2Wr8oMdwCcSG13r3wwaQ62ql1yDegy4I6cnM89O9Lb1pe0JMg4ZcYqgXF5wEfdy8f20ND4MrerN
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "cuddlingDaughter": [
                "Boolean",
                ""
            ],
            "daughterRocked": [
                "Int",
                "Tracks the day you've rocked a harpy daughter to sleep"
            ],
            "daughterMarried": [
                "Int",
                "Tracks the most recent daughter you've done a play marriage with"
            ]
        }
        '''
        self.addSSOProperty('cuddlingDaughter', bool, False, 'sophiebimbo', 'cuddlingDaughter')
        # Tracks the most recent daughter you've done a play marriage with
        self.addSSOProperty('daughterMarried', int, 0, '', 'daughterMarried')
        # Date rocked
        self.addSSOProperty('daughterRocked', int, 0, 'sophiebimbo', 'daughterRocked', notes="Tracks the day you've rocked a harpy daughter to sleep")
        ##########
        # kFlags
        ##########
        self.addKFlag('DAUGHTER_FOUR_BIMBO', int, 0, HGGKFlags.DAUGHTER_FOUR_BIMBO)
        self.addKFlag('DAUGHTER_ONE_BIMBO', int, 0, HGGKFlags.DAUGHTER_ONE_BIMBO)
        self.addKFlag('DAUGHTER_THREE_BIMBO', int, 0, HGGKFlags.DAUGHTER_THREE_BIMBO)
        self.addKFlag('DAUGHTER_TWO_BIMBO', int, 0, HGGKFlags.DAUGHTER_TWO_BIMBO)
        self.addKFlag('FARM_EGG_COUNTDOWN', int, 0, HGGKFlags.FARM_EGG_COUNTDOWN)
        self.addKFlag('FARM_EGG_STORED', int, 0, HGGKFlags.FARM_EGG_STORED)
        self.addKFlag('FOLLOWER_AT_FARM_SOPHIE', int, 0, HGGKFlags.FOLLOWER_AT_FARM_SOPHIE)
        self.addKFlag('FOLLOWER_PRODUCTION_SOPHIE', int, 0, HGGKFlags.FOLLOWER_PRODUCTION_SOPHIE)
        self.addKFlag('FOLLOWER_PRODUCTION_SOPHIE_COLORCHOICE', str, '', HGGKFlags.FOLLOWER_PRODUCTION_SOPHIE_COLORCHOICE, choices={
            'Black': 'Black',
            'Blue': 'Blue',
            'Brown': 'Brown',
            'Pink': 'Pink',
            'Purple': 'Purple',
            'White': 'White',
            '': '',
        })
        self.addKFlag('SOPHIE_ADULT_KID_COUNT', int, 0, HGGKFlags.SOPHIE_ADULT_KID_COUNT)
        self.addKFlag('SOPHIE_BIMBO', int, 0, HGGKFlags.SOPHIE_BIMBO)
        self.addKFlag('SOPHIE_BROACHED_SLEEP_WITH', int, 0, HGGKFlags.SOPHIE_BROACHED_SLEEP_WITH)
        self.addKFlag('SOPHIE_CAMP_EGG_COUNTDOWN', int, 0, HGGKFlags.SOPHIE_CAMP_EGG_COUNTDOWN)
        self.addKFlag('SOPHIE_DAUGHTER_MATURITY_COUNTER', int, 0, HGGKFlags.SOPHIE_DAUGHTER_MATURITY_COUNTER)
        self.addKFlag('SOPHIE_DEBIMBOED', int, 0, HGGKFlags.SOPHIE_DEBIMBOED)
        self.addKFlag('SOPHIE_DISABLED_FOREVER', int, 0, HGGKFlags.SOPHIE_DISABLED_FOREVER)
        self.addKFlag('SOPHIE_FAMILY_INCEST', int, 0, HGGKFlags.SOPHIE_FAMILY_INCEST)
        self.addKFlag('SOPHIE_HEAT_COUNTER', int, 0, HGGKFlags.SOPHIE_HEAT_COUNTER)
        self.addKFlag('SOPHIE_TATTOO_BUTT', int, 0, HGGKFlags.SOPHIE_TATTOO_BUTT)
        self.addKFlag('SOPHIE_TATTOO_COLLARBONE', int, 0, HGGKFlags.SOPHIE_TATTOO_COLLARBONE)
        self.addKFlag('SOPHIE_TATTOO_LOWERBACK', int, 0, HGGKFlags.SOPHIE_TATTOO_LOWERBACK)
        self.addKFlag('SOPHIE_TATTOO_SHOULDERS', int, 0, HGGKFlags.SOPHIE_TATTOO_SHOULDERS)
        self.addKFlag('TIMES_SOPHIE_AND_IZMA_FUCKED', int, 0, HGGKFlags.TIMES_SOPHIE_AND_IZMA_FUCKED)
        self.addKFlag('TIMES_SOPHIE_HAS_DRUNK_OVI_ELIXIR', int, 0, HGGKFlags.TIMES_SOPHIE_HAS_DRUNK_OVI_ELIXIR)
        self.addKFlag('TOLD_SOPHIE_TO_IZMA', int, 0, HGGKFlags.TOLD_SOPHIE_TO_IZMA)
