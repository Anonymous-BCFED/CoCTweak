"""
This is a very complex NPC, and therefore a good test bed.
"""

from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.saves.hgg.enums.gender import HGGGender
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

import argparse
from enum import IntEnum
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib


class EAmilyFollowerType(IntEnum):
    NONE = 0
    PURE = 1
    CORRUPT = 2


class EAmilyKidMeetingType(IntEnum):
    NONE = 0
    MET = 1
    IGNORED = -1


class AmilyNPC(BaseNPC):
    ID = 'amily'
    NAME = 'Amily'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/AmilyScene.as: public class AmilyScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 8Cz8x6do2esI7oh04N1bi19i6QYfNV1mkcF24BA3eWfGC8Au6CA3DT3GA1qQbl14kydDGacB7pc5j75U8cDQdUW3YucjX4xX
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "ringType": [
                "String",
                ""
            ],
            "amilyMorning": [
                "Boolean",
                ""
            ],
            "metKids": [
                "IntList",
                "",
                [
                    {
                        "label": "",
                        "data": 0
                    },
                    {
                        "label": "Met",
                        "data": 1
                    },
                    {
                        "label": "Ignored",
                        "data": -1
                    }
                ]
            ],
            "giftedCClothes": [
                "Boolean",
                ""
            ],
            "impSkullsCount": [
                "Int",
                "Skulls that Amily has added to the wall."
            ],
            "pcKnowsAboutSkulls": [
                "Boolean",
                "Amily told player about the skulls."
            ]
        }
        '''
        self.addSSOProperty('amilyMorning', bool, False, 'amily', 'amilyMorning', notes='Wake up with Amily in the morning?')
        self.addSSOProperty('giftedCClothes', bool, False, 'amily', 'giftedCClothes', notes='Gifted comfy clothes?')
        self.addSSOProperty('impSkullsCount', int, 0, 'amily', 'impSkullsCount', notes='Skulls that Amily has added to the wall.')
        self.addSSOProperty('metKids', int, 0, 'amily', 'metKids', enumType=EAmilyKidMeetingType, notes="What kind of meeting did you have with Amily's kids?", choices={
            -1: 'Ignored',
            0: 'Did not meet kids yet',
            1: 'Met',
        })
        self.addSSOProperty('pcKnowsAboutSkulls', bool, False, 'amily', 'pcKnowsAboutSkulls', notes='Player knows about the skulls?')
        self.addSSOProperty('ringType', str, '""', 'amily', 'ringType', notes='What kind of ring Amily wears.', choices=[
            'silver',
            'gold',
            'platinum',
            'diamond',
        ])
        ##########
        # kFlags
        ##########
        #   (< 15 = low.  In between = medium. 40+= high affect)
        self.addKFlag('AMILY_AFFECTION', int, 0, HGGKFlags.AMILY_AFFECTION)
        self.addKFlag('AMILY_ALLOWS_FERTILITY', int, 0, HGGKFlags.AMILY_ALLOWS_FERTILITY)
        self.addKFlag('AMILY_ASS_SIZE', int, 0, HGGKFlags.AMILY_ASS_SIZE)
        self.addKFlag('AMILY_BIRTH_TOTAL', int, 0, HGGKFlags.AMILY_BIRTH_TOTAL)
        self.addKFlag('AMILY_BLOCK_COUNTDOWN_BECAUSE_CORRUPTED_JOJO', int, 0, HGGKFlags.AMILY_BLOCK_COUNTDOWN_BECAUSE_CORRUPTED_JOJO)
        self.addKFlag('AMILY_BULGE_PROMISE', int, 0, HGGKFlags.AMILY_BULGE_PROMISE)
        self.addKFlag('AMILY_BULGE_STRIKES', int, 0, HGGKFlags.AMILY_BULGE_STRIKES)
        # PregnancyStore butt pregnancy type and notice counter
        self.addKFlag('AMILY_BUTT_PREGNANCY_TYPE', int, 0, HGGKFlags.AMILY_BUTT_PREGNANCY_TYPE)
        self.addKFlag('AMILY_CAMP_CORRUPTION_FREAKED', int, 0, HGGKFlags.AMILY_CAMP_CORRUPTION_FREAKED, notes='Whether Amily has flipped out at you about your corruption', intbool=True)
        self.addKFlag('AMILY_CLOTHING', str, '', HGGKFlags.AMILY_CLOTHING)
        #  1=yes,2=and accepted
        self.addKFlag('AMILY_CONFESSED_LESBIAN', int, 0, HGGKFlags.AMILY_CONFESSED_LESBIAN)
        self.addKFlag('AMILY_CORRUPTION', int, 0, HGGKFlags.AMILY_CORRUPTION)
        self.addKFlag('AMILY_CORRUPT_FLIPOUT', int, 0, HGGKFlags.AMILY_CORRUPT_FLIPOUT)
        #  5-Jan
        self.addKFlag('AMILY_CUP_SIZE', int, 0, HGGKFlags.AMILY_CUP_SIZE)
        self.addKFlag('AMILY_DESTROYING_CORRUPTED_GLADES', int, 0, HGGKFlags.AMILY_DESTROYING_CORRUPTED_GLADES)
        self.addKFlag('AMILY_DRUG_MADE_COUNTER', int, 0, HGGKFlags.AMILY_DRUG_MADE_COUNTER)
        self.addKFlag('AMILY_FOLLOWER', int, 0, HGGKFlags.AMILY_FOLLOWER, notes='Whether Amily is a follower, and whether she is corrupt.', choices={
            0: 'Not follower',
            1: 'Pure, Follower',
            2: 'Corrupt Follower',
        })
        self.addKFlag('AMILY_FUCK_COUNTER', int, 0, HGGKFlags.AMILY_FUCK_COUNTER)
        #  1=freaked out
        self.addKFlag('AMILY_GROSSED_OUT_BY_WORMS', int, 0, HGGKFlags.AMILY_GROSSED_OUT_BY_WORMS)
        self.addKFlag('AMILY_HAS_BALLS_AND_SIZE', int, 0, HGGKFlags.AMILY_HAS_BALLS_AND_SIZE)
        #  1=Amily flipped out, 2=accepted as Amily's dad
        self.addKFlag('AMILY_HERM_QUEST', int, 0, HGGKFlags.AMILY_HERM_QUEST)
        self.addKFlag('AMILY_HERM_TIMES_FUCKED_BY_FEMPC', int, 0, HGGKFlags.AMILY_HERM_TIMES_FUCKED_BY_FEMPC)
        #  girly-womanly
        self.addKFlag('AMILY_HIP_RATING', int, 0, HGGKFlags.AMILY_HIP_RATING)
        self.addKFlag('AMILY_INCEST_COUNTDOWN_TIMER', int, 0, HGGKFlags.AMILY_INCEST_COUNTDOWN_TIMER)
        #    0 = not pregnant, otherwise hours till birth 168
        self.addKFlag('AMILY_INCUBATION', int, 0, HGGKFlags.AMILY_INCUBATION)
        #  1 = You turned Amily into a human and then pissed all over her happy thoughts.  She now stalks you from rooftops while buying graphite helmets, utility belts, and a sweet, jetpowered car in the theme of a rat.
        self.addKFlag('AMILY_IS_BATMAN', int, 0, HGGKFlags.AMILY_IS_BATMAN)
        self.addKFlag('AMILY_LACTATION_RATE', int, 0, HGGKFlags.AMILY_LACTATION_RATE)
        #   (0 = not met, 1 = met)
        self.addKFlag('AMILY_MET', int, 0, HGGKFlags.AMILY_MET)
        self.addKFlag('AMILY_MET_AS', int, HGGGender(0), HGGKFlags.AMILY_MET_AS, enumType=HGGGender)
        self.addKFlag('AMILY_MET_PURE_JOJO', int, 0, HGGKFlags.AMILY_MET_PURE_JOJO)
        self.addKFlag('AMILY_NEED_TO_FREAK_ABOUT_URTA', int, 0, HGGKFlags.AMILY_NEED_TO_FREAK_ABOUT_URTA)
        #  0.3-4
        self.addKFlag('AMILY_NIPPLE_LENGTH', float, 0.3, HGGKFlags.AMILY_NIPPLE_LENGTH)
        #  1 = Amily is no longer a flea-ridden furry who stinks up your carpet.
        self.addKFlag('AMILY_NOT_FURRY', int, 0, HGGKFlags.AMILY_NOT_FURRY)
        #  1 = Offered to defurry Amily
        self.addKFlag('AMILY_OFFERED_DEFURRY', int, 0, HGGKFlags.AMILY_OFFERED_DEFURRY)
        #   (1 = true, 0 = not yet)
        self.addKFlag('AMILY_OFFER_ACCEPTED', int, 0, HGGKFlags.AMILY_OFFER_ACCEPTED)
        self.addKFlag('AMILY_OVIPOSITED_COUNT', int, 0, HGGKFlags.AMILY_OVIPOSITED_COUNT)
        # PregnancyStore butt incubation counter
        self.addKFlag('AMILY_OVIPOSITED_COUNTDOWN', int, 0, HGGKFlags.AMILY_OVIPOSITED_COUNTDOWN)
        self.addKFlag('AMILY_OVIPOSITION_UNLOCKED', int, 0, HGGKFlags.AMILY_OVIPOSITION_UNLOCKED)
        self.addKFlag('AMILY_OWNS_BIKINI', int, 0, HGGKFlags.AMILY_OWNS_BIKINI)
        self.addKFlag('AMILY_PC_GENDER', int, 0, HGGKFlags.AMILY_PC_GENDER)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('AMILY_PREGNANCY_TYPE', int, 0, HGGKFlags.AMILY_PREGNANCY_TYPE)
        self.addKFlag('AMILY_SPAR_WITH_PURE_JOJO', int, 0, HGGKFlags.AMILY_SPAR_WITH_PURE_JOJO)
        self.addKFlag('AMILY_TATTOO_BUTT', int, 0, HGGKFlags.AMILY_TATTOO_BUTT)
        self.addKFlag('AMILY_TATTOO_COLLARBONE', int, 0, HGGKFlags.AMILY_TATTOO_COLLARBONE)
        self.addKFlag('AMILY_TATTOO_LOWERBACK', int, 0, HGGKFlags.AMILY_TATTOO_LOWERBACK)
        self.addKFlag('AMILY_TATTOO_SHOULDERS', int, 0, HGGKFlags.AMILY_TATTOO_SHOULDERS)
        self.addKFlag('AMILY_TIMES_BUTTFUCKED_PC', int, 0, HGGKFlags.AMILY_TIMES_BUTTFUCKED_PC)
        self.addKFlag('AMILY_TIMES_FUCKED_FEMPC', int, 0, HGGKFlags.AMILY_TIMES_FUCKED_FEMPC)
        self.addKFlag('AMILY_TIMES_SWIMFUCKED', int, 0, HGGKFlags.AMILY_TIMES_SWIMFUCKED)
        #  0 = Amily doesn't know 'bout fuckflowers; 1 = Amily saw tree but you enhanced your calm and kept silent; 2 = HOLLI DIES and Amily comes back, but SHE WILL REMEMBER THIS, you monster. (See BATMAN FLIPOUT above)
        self.addKFlag('AMILY_TREE_FLIPOUT', int, 0, HGGKFlags.AMILY_TREE_FLIPOUT)
        self.addKFlag('AMILY_VAGINAL_WETNESS', int, 0, HGGKFlags.AMILY_VAGINAL_WETNESS)
        self.addKFlag('AMILY_VILLAGE_ACCESSIBLE', int, 0, HGGKFlags.AMILY_VILLAGE_ACCESSIBLE)
        #  1=true,44=village button
        self.addKFlag('AMILY_VILLAGE_ENCOUNTERS_DISABLED', int, 0, HGGKFlags.AMILY_VILLAGE_ENCOUNTERS_DISABLED)
        self.addKFlag('AMILY_VISITING_URTA', int, 0, HGGKFlags.AMILY_VISITING_URTA)
        self.addKFlag('AMILY_WAIT_FOR_PC_FIX_JOJO', int, 0, HGGKFlags.AMILY_WAIT_FOR_PC_FIX_JOJO)
        self.addKFlag('AMILY_WANG_GIRTH', int, 0, HGGKFlags.AMILY_WANG_GIRTH)
        self.addKFlag('AMILY_WANG_LENGTH', int, 0, HGGKFlags.AMILY_WANG_LENGTH)
        self.addKFlag('AMILY_X_IZMA_POTION_3SOME', int, 0, HGGKFlags.AMILY_X_IZMA_POTION_3SOME)
        self.addKFlag('AMILY_X_JOJO_COOLDOWN', int, 0, HGGKFlags.AMILY_X_JOJO_COOLDOWN)
        self.addKFlag('FOLLOWER_AT_FARM_AMILY', int, 0, HGGKFlags.FOLLOWER_AT_FARM_AMILY)
        self.addKFlag('FOLLOWER_AT_FARM_AMILY_GIBS_MILK', int, 0, HGGKFlags.FOLLOWER_AT_FARM_AMILY_GIBS_MILK)
        self.addKFlag('FOLLOWER_PRODUCTION_AMILY', int, 0, HGGKFlags.FOLLOWER_PRODUCTION_AMILY)
        self.addKFlag('GIVEN_AMILY_NURSE_OUTFIT', int, 0, HGGKFlags.GIVEN_AMILY_NURSE_OUTFIT)
        self.addKFlag('IZMA_AMILY_FREAKOUT_STATUS', int, 0, HGGKFlags.IZMA_AMILY_FREAKOUT_STATUS)
        self.addKFlag('MARBLE_OR_AMILY_FIRST_FOR_FREAKOUT', int, 0, HGGKFlags.MARBLE_OR_AMILY_FIRST_FOR_FREAKOUT)
        #  1=yes,2=finished
        self.addKFlag('PC_PENDING_PREGGERS', int, 0, HGGKFlags.PC_PENDING_PREGGERS)
        self.addKFlag('PC_TIMES_BIRTHED_AMILYKIDS', int, 0, HGGKFlags.PC_TIMES_BIRTHED_AMILYKIDS)
        self.addKFlag('TIMES_FUCKED_AMILYBUTT', int, 0, HGGKFlags.TIMES_FUCKED_AMILYBUTT)
        ###################
        # PregnancyStores
        ###################
        self.analPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.AMILY_BUTT_PREGNANCY_TYPE, HGGKFlags.AMILY_OVIPOSITED_COUNTDOWN)
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.AMILY_PREGNANCY_TYPE, HGGKFlags.AMILY_INCUBATION)

    def register_cli_enable_encounters(self, subp: argparse._SubParsersAction) -> None:
        p = subp.add_parser(
            "enable-encounters",
            help="Re-enable village encounters if AMILY_VILLAGE_ENCOUNTERS_DISABLED somehow got set. WILL REMOVE HER AS A FOLLOWER",
        )
        p.set_defaults(npc_cmd=self.cmd_enable_encounters)

    def cmd_enable_encounters(self, args: argparse.Namespace) -> bool:
        self.verbose_set = True
        self.AMILY_VILLAGE_ENCOUNTERS_DISABLED = 0
        self.AMILY_VILLAGE_ACCESSIBLE = 1
        self.AMILY_FOLLOWER = 0
        self.AMILY_CORRUPT_FLIPOUT = 0
        self.verbose_set=False
        return True

    def register_cli_make_follower(self, subp: argparse.ArgumentParser) -> None:
        p_make_follower = subp.add_parser(
            "make-follower", help="Make Amily a camp follower."
        )
        p_make_follower.add_argument(
            "--corrupt",
            "-c",
            action="store_true",
            default=False,
            help="Adds her as a corrupt follower.",
        )
        p_make_follower.set_defaults(npc_cmd=self.cmd_make_follower)

    def cmd_make_follower(self, args: argparse.Namespace) -> bool:
        self.verbose_set = True
        if args.corrupt:
            ## from [HGG]/classes/classes/Scenes/NPCs/AmilyScene.as: private function rapeCorruptAmily4Epilogue():void { @ 5MUddR1nkfVu90fd0663E5jo0Dv1ln7u80lZa3a200ggG6066ng9Utg5g39O3hTa9U4mA6uvfo93yk2E1cbt2AfdYW1D048J
            # //Add corrupted Amily flag here
            # flags[kFLAGS.AMILY_FOLLOWER] = 2;
            self.AMILY_FOLLOWER = EAmilyFollowerType.CORRUPT
            # //Set other flags if Amily is moving in for the first time
            # if (flags[kFLAGS.AMILY_CAMP_CORRUPTION_FREAKED] == 0) {
            if self.AMILY_CAMP_CORRUPTION_FREAKED == 0:
                self.AMILY_CUP_SIZE = 5
                self.AMILY_NIPPLE_LENGTH = 0.5
                self.AMILY_HIP_RATING = 12
                self.AMILY_ASS_SIZE = 12
                self.AMILY_VAGINAL_WETNESS = 1
                self.AMILY_CLOTHING = "sexy rags"
        else:
            ## from [HGG]/classes/classes/Scenes/NPCs/AmilyScene.as: private function thisFunctionProbablySucksTooOhYeahAmilyFunction():void { @ 1WA2Ea6W11aKedU5n80Pf9RwbNS22A3yS7k66eK2RweA57RtdNE8GG7E13Jmge24ZT7To9w0dOQexR8QM6swdvi2uaeYhcjq
            self.AMILY_FOLLOWER = EAmilyFollowerType.PURE
            self.AMILY_CUP_SIZE = 1
            self.AMILY_NIPPLE_LENGTH = 0.3
            self.AMILY_HIP_RATING = 6
            self.AMILY_ASS_SIZE = 6
            self.AMILY_VAGINAL_WETNESS = 1
            self.AMILY_CLOTHING = "rags"
        # //if marble is there, tag it for freak-out
        # if (player.hasStatusEffect(StatusEffects.CampMarble)) {
        #    flags[kFLAGS.MARBLE_OR_AMILY_FIRST_FOR_FREAKOUT] = 1;
        # }
        # else flags[kFLAGS.MARBLE_OR_AMILY_FIRST_FOR_FREAKOUT] = 2;
        if self.save.getStatusEffect(HGGStatusLib.CampMarble) is not None:
            self.MARBLE_OR_AMILY_FIRST_FOR_FREAKOUT = 1
        else:
            self.MARBLE_OR_AMILY_FIRST_FOR_FREAKOUT = 2
        # NOTE: This is only used in the pure camp join, for some reason.
        if not args.corrupt:
            # //if Izma is there, tag for freak-out!
            # if (flags[kFLAGS.IZMA_FOLLOWER_STATUS] == 1) {
            #    flags[kFLAGS.IZMA_AMILY_FREAKOUT_STATUS] = 1;
            # }
            if self.save.flags.get(HGGKFlags.IZMA_FOLLOWER_STATUS, 0) == 1:
                self.IZMA_AMILY_FREAKOUT_STATUS = 1
        # //Disable Amily encounters in the village!
        self.AMILY_VILLAGE_ENCOUNTERS_DISABLED = 1

        self.verbose_set = False
        return True
