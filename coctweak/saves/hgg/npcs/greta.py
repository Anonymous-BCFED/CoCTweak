from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class GretaNPC(BaseNPC):
    ID = 'greta'
    NAME = 'Greta'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/GretasGarments.as: public class GretasGarments extends BazaarAbstractContent { @ bfR7WE4L33AT4Q91043Clcvo0pGgV28OB7kg8hG5Ld7Qc6oy4dJ4Otaf40JfeGodXf85u7gEcVhaANaZY4O11664gPfue0DZ
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('FOUND_SOCKS', float, 0., HGGKFlags.FOUND_SOCKS, notes='Whether Greta told you about her socks.', choices={
            0.0: 'No',
            0.5: 'In narrative',
            1.0: 'Talked about, but low stock',
            2.0: 'In stock',
        })
        self.addKFlag('OWN_MAIDEN_BIKINI', int, 0, HGGKFlags.OWN_MAIDEN_BIKINI, intbool=True)
        self.addKFlag('SOCKS_BOUGHT', int, 0, HGGKFlags.SOCKS_BOUGHT, min=0)
        self.addKFlag('SOCK_COUNTER', int, 0, HGGKFlags.SOCK_COUNTER, min=0, max=24, notes='Ticks down from 24 when you are first introduced to the concept of cock socks.')
        self.addKFlag('SOCK_HOLDING', int, 0, HGGKFlags.SOCK_HOLDING, readonly=True, notes="Which cock sock you are looking at in Greta's shop. Useless to modify.")
        self.addKFlag('TOOK_NAUGHTY_HABIT', int, 0, HGGKFlags.TOOK_NAUGHTY_HABIT)
