import argparse
from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from enum import IntEnum
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class ECeraphRapePlayState(IntEnum):
    NOT_DONE = 0
    IMPS_YES = 1
    IMPS_NO = 2

class CeraphNPC(BaseNPC):
    ID = 'ceraph'
    NAME = 'Ceraph'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/CeraphFollowerScene.as: public class CeraphFollowerScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 8d70krg2DdeCdap5V0bmH6dH9utfus9TDga0gD62xI62rgmwfAZ31Z9fD53s2Mm8OM13C8S1h2kd1f2vGe1I8X5h014T3df9
        ## from [HGG]/classes/classes/Scenes/NPCs/CeraphScene.as: public class CeraphScene extends NPCAwareContent { @ 8ra28bb85dtsgYdaNwgRs4Ag19w1aPgkW0Sj9xGaz31GT9Z40vNe3yggMd8odOZ3MPdAYfe3fneaj29I7gz78If7LTdEmaIy
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "rapePlay": [
                "IntList",
                "",
                [
                    {
                        "label": "Never done",
                        "data": 0
                    },
                    {
                        "label": "Yes Imps",
                        "data": 1
                    },
                    {
                        "label": "No Imps",
                        "data": 2
                    }
                ]
            ],
            "roleplayAsTellyCount": [
                "Int",
                "Times roleplayed as Telly"
            ]
        }
        '''
        # 0 = never done, 1 = continued impregnation, 2 = impregnation disabled
        self.addSSOProperty('rapePlay', int, 0, 'ceraph', 'rapePlay', enumType=ECeraphRapePlayState, notes='How rape play was handled', choices={
            0: 'Never done',
            1: 'Yes Imps',
            2: 'No Imps',
        })
        self.addSSOProperty('roleplayAsTellyCount', int, 0, 'ceraph', 'roleplayAsTellyCount', notes='Times roleplayed as Telly')
        ##########
        # kFlags
        ##########
        self.addKFlag('CERAPH_BEATEN_AND_RAPED_COUNTER', int, 0, HGGKFlags.CERAPH_BEATEN_AND_RAPED_COUNTER)
        self.addKFlag('CERAPH_BELLYBUTTON_PIERCING', int, 0, HGGKFlags.CERAPH_BELLYBUTTON_PIERCING)
        self.addKFlag('CERAPH_DICKS_OWNED', int, 0, HGGKFlags.CERAPH_DICKS_OWNED)
        self.addKFlag('CERAPH_FAUX_CORRUPTION_DISABLED', int, 0, HGGKFlags.CERAPH_FAUX_CORRUPTION_DISABLED)
        self.addKFlag('CERAPH_GENITAL_STEAL_CHOICE', int, 0, HGGKFlags.CERAPH_GENITAL_STEAL_CHOICE)
        self.addKFlag('CERAPH_HIDING_DICK', int, 0, HGGKFlags.CERAPH_HIDING_DICK)
        self.addKFlag('CERAPH_KILLED', int, 0, HGGKFlags.CERAPH_KILLED)
        self.addKFlag('CERAPH_LICKED_YOU_OUT', int, 0, HGGKFlags.CERAPH_LICKED_YOU_OUT)
        self.addKFlag('CERAPH_PUNISHED', int, 0, HGGKFlags.CERAPH_PUNISHED)
        self.addKFlag('CERAPH_PUSSIES_OWNED', int, 0, HGGKFlags.CERAPH_PUSSIES_OWNED)
        self.addKFlag('CERAPH_ROLEPLAY_AS_DOMINIKA_COUNT', int, 0, HGGKFlags.CERAPH_ROLEPLAY_AS_DOMINIKA_COUNT)
        self.addKFlag('CERAPH_SWEETIE_NO_RP_DONE', int, 0, HGGKFlags.CERAPH_SWEETIE_NO_RP_DONE)
        self.addKFlag('CERAPH_SWEETIE_NO_RP_UTTER_MOO_DISABLED', int, 0, HGGKFlags.CERAPH_SWEETIE_NO_RP_UTTER_MOO_DISABLED)
        self.addKFlag('CERAPH_TITS_OWNED', int, 0, HGGKFlags.CERAPH_TITS_OWNED)
        self.addKFlag('CERAPH_TOKEN', int, 0, HGGKFlags.CERAPH_TOKEN)
        self.addKFlag('FOLLOWER_AT_FARM_CERAPH', int, 0, HGGKFlags.FOLLOWER_AT_FARM_CERAPH)
        self.addKFlag('LIDDELLIUM_CERAPH_FLAG', int, 0, HGGKFlags.LIDDELLIUM_CERAPH_FLAG)
        self.addKFlag('PC_MET_CERAPH', int, 0, HGGKFlags.PC_MET_CERAPH)
        self.addKFlag('TIMES_CERAPH_PORTAL_FUCKED', int, 0, HGGKFlags.TIMES_CERAPH_PORTAL_FUCKED)

    def register_cli_kill(self, subp: argparse.ArgumentParser) -> None:
        p_kill = subp.add_parser('kill', help='Kill Ceraph. **THIS MAY BREAK THINGS IF SHE\'S A FOLLOWER OR WHATEVER.**')
        p_kill.set_defaults(npc_cmd=self.cmd_kill)

    def register_cli_revive(self, subp: argparse.ArgumentParser) -> None:
        p_kill = subp.add_parser('revive', help='Un-kill Ceraph.')
        p_kill.set_defaults(npc_cmd=self.cmd_revive)

    def cmd_kill(self, args: argparse.Namespace) -> bool:
        self.verbose_set = True
        self.CERAPH_KILLED = 1
        self.verbose_set = False
        log.info('Ceraph marked as dead.')
        return True

    def cmd_revive(self, args: argparse.Namespace) -> bool:
        self.verbose_set = True
        self.CERAPH_KILLED = 0
        self.verbose_set = False
        log.info('Ceraph marked as not dead.')
        return True
