from coctweak.saves._npc import BaseNPC, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class SwordInStoneNPC(BaseNPC):
    ID = 'swordinstone'
    NAME = 'SwordInStone'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Lake/SwordInStone.as: public class SwordInStone extends AbstractLakeContent implements Encounter { @ gRPegD0hL8A8edn7Dv4FucY83sobLz5AM3gX6gxeRD48caD19ec1rO3RAfep0rv5YNdhs1ze2dy4W71C8cHD6i35Kt4cWbPG
        super().__init__(save)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('BSwordBroken', bool, False, HGGStatusLib.BSwordBroken, None)
        self.addSFXProperty('TookBlessedSword', bool, False, HGGStatusLib.TookBlessedSword, None)
