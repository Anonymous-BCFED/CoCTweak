from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class RubiNPC(BaseNPC):
    ID = 'rubi'
    NAME = 'Rubi'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Rubi.as: public class Rubi extends TelAdreAbstractContent implements SelfSaving, SelfDebug { @ 6WOdtCbpIb6Zaci5GqaHu9tsbZP6L22iqbbV9dP5yP7I6eBm2R0azj71D6nt5CMh122SOcET8os3g26FX14b33j2TA5q4fIK
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "hadSex": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('hadSex', bool, False, 'rubi', 'hadSex')
        ##########
        # kFlags
        ##########
        self.addKFlag('RUBIS_HOUSE_FIRST_TIME', int, 0, HGGKFlags.RUBIS_HOUSE_FIRST_TIME)
        self.addKFlag('RUBI_ADMITTED_GENDER', int, 0, HGGKFlags.RUBI_ADMITTED_GENDER)
        self.addKFlag('RUBI_AFFECTION', int, 0, HGGKFlags.RUBI_AFFECTION)
        self.addKFlag('RUBI_BALLS_TYPE', int, 0, HGGKFlags.RUBI_BALLS_TYPE)
        self.addKFlag('RUBI_BAR_CONFESSION', int, 0, HGGKFlags.RUBI_BAR_CONFESSION)
        self.addKFlag('RUBI_BIMBO', int, 0, HGGKFlags.RUBI_BIMBO, intbool=True)
        self.addKFlag('RUBI_BIMBO_MINIDRESS', int, 0, HGGKFlags.RUBI_BIMBO_MINIDRESS)
        self.addKFlag('RUBI_BLU_BALLS', int, 0, HGGKFlags.RUBI_BLU_BALLS)
        self.addKFlag('RUBI_BODYSUIT', int, 0, HGGKFlags.RUBI_BODYSUIT)
        self.addKFlag('RUBI_BONDAGE_STRAPS', int, 0, HGGKFlags.RUBI_BONDAGE_STRAPS)
        self.addKFlag('RUBI_BREAST_SIZE', int, 0, HGGKFlags.RUBI_BREAST_SIZE)
        self.addKFlag('RUBI_COCK_SIZE', int, 0, HGGKFlags.RUBI_COCK_SIZE)
        self.addKFlag('RUBI_COCK_TYPE', int, 0, HGGKFlags.RUBI_COCK_TYPE)
        self.addKFlag('RUBI_CUNTTYPE', int, 0, HGGKFlags.RUBI_CUNTTYPE)
        self.addKFlag('RUBI_DEBIMBO', int, 0, HGGKFlags.RUBI_DEBIMBO, unused=True, intbool=True)
        self.addKFlag('RUBI_DISABLED', int, 0, HGGKFlags.RUBI_DISABLED)
        self.addKFlag('RUBI_EAR_TYPE', int, 0, HGGKFlags.RUBI_EAR_TYPE)
        self.addKFlag('RUBI_EYE_TYPE', int, 0, HGGKFlags.RUBI_EYE_TYPE)
        self.addKFlag('RUBI_FANCY_CONFESSION', int, 0, HGGKFlags.RUBI_FANCY_CONFESSION)
        self.addKFlag('RUBI_FEET', int, 0, HGGKFlags.RUBI_FEET)
        self.addKFlag('RUBI_FETISH_CLOTHES', int, 0, HGGKFlags.RUBI_FETISH_CLOTHES)
        self.addKFlag('RUBI_GOT_BIMBO_SKIRT', int, 0, HGGKFlags.RUBI_GOT_BIMBO_SKIRT)
        self.addKFlag('RUBI_GREEN_ADVENTURER', int, 0, HGGKFlags.RUBI_GREEN_ADVENTURER)
        self.addKFlag('RUBI_HAIR', int, 0, HGGKFlags.RUBI_HAIR)
        self.addKFlag('RUBI_HAIR_LENGTH', int, 0, HGGKFlags.RUBI_HAIR_LENGTH)
        self.addKFlag('RUBI_HANDS', int, 0, HGGKFlags.RUBI_HANDS)
        self.addKFlag('RUBI_HORNTYPE', int, 0, HGGKFlags.RUBI_HORNTYPE)
        self.addKFlag('RUBI_ICECREAM_CONFESSION', int, 0, HGGKFlags.RUBI_ICECREAM_CONFESSION)
        self.addKFlag('RUBI_INCUBUS_PROGRESS', int, 0, HGGKFlags.RUBI_INCUBUS_PROGRESS)
        self.addKFlag('RUBI_INQUISITORS_CORSET', int, 0, HGGKFlags.RUBI_INQUISITORS_CORSET)
        self.addKFlag('RUBI_INTRODUCED', int, 0, HGGKFlags.RUBI_INTRODUCED)
        self.addKFlag('RUBI_LONGDRESS', int, 0, HGGKFlags.RUBI_LONGDRESS)
        self.addKFlag('RUBI_LOWERBODY', int, 0, HGGKFlags.RUBI_LOWERBODY)
        self.addKFlag('RUBI_NIPPLE_TYPE', int, 0, HGGKFlags.RUBI_NIPPLE_TYPE)
        self.addKFlag('RUBI_NO_CUNT', int, 0, HGGKFlags.RUBI_NO_CUNT)
        self.addKFlag('RUBI_NURSE_CLOTHES', int, 0, HGGKFlags.RUBI_NURSE_CLOTHES)
        self.addKFlag('RUBI_ORGASM_DENIAL', int, 0, HGGKFlags.RUBI_ORGASM_DENIAL)
        self.addKFlag('RUBI_PROFIT', int, 0, HGGKFlags.RUBI_PROFIT)
        self.addKFlag('RUBI_PROSTITUTION', int, 0, HGGKFlags.RUBI_PROSTITUTION)
        self.addKFlag('RUBI_SETUP', int, 0, HGGKFlags.RUBI_SETUP)
        self.addKFlag('RUBI_SHE', int, 0, HGGKFlags.RUBI_SHE)
        self.addKFlag('RUBI_SKIN', int, 0, HGGKFlags.RUBI_SKIN)
        self.addKFlag('RUBI_SUITCLOTHES', int, 0, HGGKFlags.RUBI_SUITCLOTHES)
        self.addKFlag('RUBI_SWIMWEAR', int, 0, HGGKFlags.RUBI_SWIMWEAR)
        self.addKFlag('RUBI_TIGHT_PANTS', int, 0, HGGKFlags.RUBI_TIGHT_PANTS)
        self.addKFlag('RUBI_TIMES_ANALLY_TRAINED', int, 0, HGGKFlags.RUBI_TIMES_ANALLY_TRAINED)
        self.addKFlag('RUBI_TIMES_GIVEN_AN_ITEM', int, 0, HGGKFlags.RUBI_TIMES_GIVEN_AN_ITEM)
        self.addKFlag('RUBI_TUBE_TOP', int, 0, HGGKFlags.RUBI_TUBE_TOP)
        self.addKFlag('RUBI_WHISKERS', int, 0, HGGKFlags.RUBI_WHISKERS)
        self.addKFlag('TIMES_DISCUSSED_RUBIS_IDENTITY', int, 0, HGGKFlags.TIMES_DISCUSSED_RUBIS_IDENTITY)
        self.addKFlag('TIMES_RUBI_DATED', int, 0, HGGKFlags.TIMES_RUBI_DATED)
        self.addKFlag('TIMES_RUBI_MASSAGED', int, 0, HGGKFlags.TIMES_RUBI_MASSAGED)
