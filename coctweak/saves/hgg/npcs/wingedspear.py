from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class WingedSpearNPC(BaseNPC):
    ID = 'wingedspear'
    NAME = 'WingedSpear'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/HighMountains/WingedSpearScene.as: public class WingedSpearScene extends BaseContent implements Encounter, SelfSaving, SelfDebug { @ 2hv0J53Hqd2wcAK9f2aIe4OYbuf7SGbzf4uSfoz3n4aEG0xmaCg3L6d2SfNU11sdbO3LV59Q019eT56152GF2qa7Ey2Dw8r0
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "encountered": [
                "Boolean",
                ""
            ],
            "examined": [
                "Boolean",
                ""
            ],
            "takenSpear": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('encountered', bool, False, 'wingedspear', 'encountered')
        self.addSSOProperty('examined', bool, False, 'wingedspear', 'examined')
        self.addSSOProperty('takenSpear', bool, False, 'wingedspear', 'takenSpear')
