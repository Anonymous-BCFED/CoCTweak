from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class UmaNPC(BaseNPC):
    ID = 'uma'
    NAME = 'Uma'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/UmasShop.as: public class UmasShop extends TelAdreAbstractContent { @ daI9bs1SEgfufxggW397Y8Bz5RlcFs6hN6Hs8BMaHkeg31Md9KrbUB2VCdZka8g4BB1FSgsC6WY3os5uI6xZ0vp9O7cpf8qU
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('LOPPE_PC_MET_UMA', int, 0, HGGKFlags.LOPPE_PC_MET_UMA)
        self.addKFlag('UMA_TIMES_ACUPUNCTURE_UNDO', int, 0, HGGKFlags.UMA_TIMES_ACUPUNCTURE_UNDO)
        self.addKFlag('UMA_TIMES_MASSAGED', int, 0, HGGKFlags.UMA_TIMES_MASSAGED)
        self.addKFlag('UMA_TIMES_SUCKED_YOU', int, 0, HGGKFlags.UMA_TIMES_SUCKED_YOU)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('UmasMassage', bool, False, HGGStatusLib.UmasMassage, None)
