from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class YamataNPC(BaseNPC):
    ID = 'yamata'
    NAME = 'Yamata'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/Yamata.as: public class Yamata extends BaseKitsune { @ 1Kz1vS3ca28y4dD5vB2xG70y2dX62N22weErfv95ZQ5w7asqfyreuT9Do9vK9cqawNcmxfMK2OO3lJfsoaOndyT1TD0x62Yg
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('YAMATA_MASOCHIST', int, 0, HGGKFlags.YAMATA_MASOCHIST, min=0, max=100, notes='Incremented whenever Yamata inflicts pain upon the player. Never decremented in basegame.')
