from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class SalonNPC(BaseNPC):
    ID = 'salon'
    NAME = 'Salon'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Mountain/Salon.as: public class Salon extends BaseContent implements TimeAwareInterface { @ aFa7ov92EaUf23A3XbcZG2ZP11Agrn1xrb1mdeGfXx426bWY7aV6hW06f9HzcvZfG15LvdAf7bh5bb1x5bWsaps089e70eBB
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('HAIR_GROWTH_STOPPED_BECAUSE_LIZARD', int, 0, HGGKFlags.HAIR_GROWTH_STOPPED_BECAUSE_LIZARD, notes='Your hair no longer grows because you are a lizard.', intbool=True)
        self.addKFlag('LYNNETTE_ANNOUNCED_APPROVAL', int, 0, HGGKFlags.LYNNETTE_ANNOUNCED_APPROVAL, notes='Lynette approved of your cum production.', intbool=True)
        self.addKFlag('LYNNETTE_APPROVAL', int, 0, HGGKFlags.LYNNETTE_APPROVAL, min=-100, max=100, notes="Lynette's approval rating of you.")
        self.addKFlag('LYNNETTE_BABY_COUNT', int, 0, HGGKFlags.LYNNETTE_BABY_COUNT, notes='How many babies you and Lynnette have produced.')
        self.addKFlag('LYNNETTE_CARRYING_COUNT', int, 0, HGGKFlags.LYNNETTE_CARRYING_COUNT, min=0, notes='How many babies Lynnette is currently carrying.')
        self.addKFlag('LYNNETTE_FUCK_COUNTER', int, 0, HGGKFlags.LYNNETTE_FUCK_COUNTER, min=0, notes="How many times you've fucked Lynnette.")
        self.addKFlag('LYNNETTE_MET_UNPREGNANT', int, 0, HGGKFlags.LYNNETTE_MET_UNPREGNANT, notes="Whether you've met Lynnette when she's not pregnant.", intbool=True)
        self.addKFlag('LYNNETTE_PREGNANCY_CYCLE', int, 0, HGGKFlags.LYNNETTE_PREGNANCY_CYCLE, notes='Which stage in the pregnancy cycle Lynnette is in.', choices={
            '0-3': 'Not pregnant',
            '4-6': 'Pregnant',
        })
        self.addKFlag('SALON_MINOCUM_UNLOCKED', int, 0, HGGKFlags.SALON_MINOCUM_UNLOCKED)
        self.addKFlag('SALON_PAID', int, 0, HGGKFlags.SALON_PAID, notes='You paid the Salon.', intbool=True)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('HairdresserMeeting', bool, False, HGGStatusLib.HairdresserMeeting, None, notes="Whether you've found the Salon or not.")
