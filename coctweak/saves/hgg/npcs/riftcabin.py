from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class RiftCabinNPC(BaseNPC):
    ID = 'riftcabin'
    NAME = 'RiftCabin'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/GlacialRift/RiftCabinScene.as: public class RiftCabinScene extends BaseContent implements Encounter, SelfSaving, SelfDebug { @ 0jAdqF6rNbeCfm60S4dBM8cW0sL7smbu5dQw9363MA5ia33A9pC0vncELgT84b24Lbe3M2pg4FmdxYcCSaBb1p48goeSXeTQ
        super().__init__(save)
        #############
        # SSO Props
        #############
        self.addSSOProperty('bloomersBurned', bool, False, 'riftCabin', 'bloomersBurned', notes='Whether you burnt the bloomers.')
        self.addSSOProperty('encountered', bool, False, 'riftCabin', 'encountered', notes='Whether the cabin was encountered yet or not.')
        self.addSSOProperty('loliVersion', bool, False, 'riftCabin', 'loliVersion', notes='Did you encounter the loli version of the scene?')
        self.addSSOProperty('orgasmType', str, '""', 'riftCabin', 'orgasmType', notes='If you orgasmed, what organ produced it?', choices={
            '': 'N/A',
            'Dick': 'Penis',
            'Vaginal': 'Vagina',
        })
        self.addSSOProperty('pictureBurned', bool, False, 'riftCabin', 'pictureBurned', notes='Whether you burnt the picture.')
        self.addSSOProperty('tookBloomers', bool, False, 'riftCabin', 'tookBloomers', notes='Whether you stole the bloomers.')
