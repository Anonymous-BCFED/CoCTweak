from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class TamaniNPC(BaseNPC):
    ID = 'tamani'
    NAME = 'Tamani'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/TamaniScene.as: public class TamaniScene extends BaseContent implements SelfSaving, SelfDebug, TimeAwareInterface, Encounter { @ 45Zc2T6hGauF3DV2ZY8Kw1w73cbf0P6qge47dMccVZa7NdxQbOgdJme0m0rk6KZ4Mc87rdqmczO5kQ4Qm7CrcBt2ml66X4b2
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "timesRidden": [
                "Int",
                ""
            ]
        }
        '''
        self.addSSOProperty('timesRidden', int, 0, 'tamani', 'timesRidden')
        ##########
        # kFlags
        ##########
        self.addKFlag('TAMANI_BAD_ENDED', int, 0, HGGKFlags.TAMANI_BAD_ENDED)
        self.addKFlag('TAMANI_DAUGHTER_PREGGO_COUNTDOWN', int, 0, HGGKFlags.TAMANI_DAUGHTER_PREGGO_COUNTDOWN)
        self.addKFlag('TAMANI_DEFEAT_COUNTER', int, 0, HGGKFlags.TAMANI_DEFEAT_COUNTER)
        self.addKFlag('TAMANI_MET', int, 0, HGGKFlags.TAMANI_MET)
        self.addKFlag('TAMANI_NUMBER_OF_DAUGHTERS', int, 0, HGGKFlags.TAMANI_NUMBER_OF_DAUGHTERS)
        self.addKFlag('TAMANI_PREGNANCY_COUNT', int, 0, HGGKFlags.TAMANI_PREGNANCY_COUNT)
        # PregnancyStore vag incubation counter
        self.addKFlag('TAMANI_PREGNANCY_INCUBATION', int, 0, HGGKFlags.TAMANI_PREGNANCY_INCUBATION)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('TAMANI_PREGNANCY_TYPE', int, 0, HGGKFlags.TAMANI_PREGNANCY_TYPE)
        self.addKFlag('TAMANI_TIMES_HYPNOTISED', int, 0, HGGKFlags.TAMANI_TIMES_HYPNOTISED)
        self.addKFlag('TAMANI_TIMES_IMPREGNATED', int, 0, HGGKFlags.TAMANI_TIMES_IMPREGNATED)
        self.addKFlag('TAMANI_TIME_OUT', int, 0, HGGKFlags.TAMANI_TIME_OUT)
        self.addKFlag('TIMES_OVIPOSITED_TAMANI', int, 0, HGGKFlags.TIMES_OVIPOSITED_TAMANI)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.TAMANI_PREGNANCY_TYPE, HGGKFlags.TAMANI_PREGNANCY_INCUBATION)
