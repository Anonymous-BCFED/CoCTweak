from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class SatyrNPC(BaseNPC):
    ID = 'satyr'
    NAME = 'Satyr'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Plains/SatyrScene.as: public class SatyrScene extends BaseContent { @ 2mk7E6c8veDT9hUc3qdfF4ad9i17ag1owaq3fVC3wj33hc5N8T65pZ4Sf09WcOU4IRcUQbd83fdc5k5TS6QS7iBfDifDT2VY
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('SATYR_KIDS', int, 0, HGGKFlags.SATYR_KIDS)
