from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class MinotaurNPC(BaseNPC):
    ID = 'minotaur'
    NAME = 'Minotaur'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Mountain/MinotaurScene.as: public class MinotaurScene extends BaseContent { @ cjl0gYeXse3Kd1M7SN8OCeGI6u03JUgb9ffQaXt6d6fOF9pe1AV1q38eh2YG6xi9kwfiefHu58gg3IdD8eEpef7emT4jW5g9
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('MINOTAUR_CUM_ADDICTION_STATE', int, 0, HGGKFlags.MINOTAUR_CUM_ADDICTION_STATE)
        self.addKFlag('MINOTAUR_CUM_ADDICTION_TRACKER', int, 0, HGGKFlags.MINOTAUR_CUM_ADDICTION_TRACKER)
        self.addKFlag('MINOTAUR_CUM_REALLY_ADDICTED_STATE', int, 0, HGGKFlags.MINOTAUR_CUM_REALLY_ADDICTED_STATE)
        self.addKFlag('MINOTAUR_CUM_RESISTANCE_TRACKER', int, 0, HGGKFlags.MINOTAUR_CUM_RESISTANCE_TRACKER)
        self.addKFlag('MINOTAUR_SONS_CUM_REPEAT_COOLDOWN', int, 0, HGGKFlags.MINOTAUR_SONS_CUM_REPEAT_COOLDOWN)
        self.addKFlag('TIMES_MINO_MUTUAL', int, 0, HGGKFlags.TIMES_MINO_MUTUAL)
        self.addKFlag('TIME_SINCE_LAST_CONSUMED_MINOTAUR_CUM', int, 0, HGGKFlags.TIME_SINCE_LAST_CONSUMED_MINOTAUR_CUM)
