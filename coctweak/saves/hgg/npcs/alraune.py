from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class AlrauneNPC(BaseNPC):
    ID = 'alraune'
    NAME = 'Alraune'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Swamp/Alraune.as: public class Alraune extends Monster { @ eU86svaMDc6ychf5lM43J6U0864dL0eftgyN5LVc037le9ZzbGU5IO1Bibnn3MBgUI76R1NO9AB9dFgDJ4QgfI87Yga5N4A2
        ## from [HGG]/classes/classes/Scenes/Areas/Swamp/AlrauneScene.as: public class AlrauneScene extends BaseContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 3we34MeDJ6rm8LWgKl9xvcfTfP45oa8zw2zzeZX79Tdc28eT9ma3cY0E0azC8RVasNduDgfCgfx6j57KJetd7f3cMk3s13nm
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "metAlraune": [
                "Boolean",
                ""
            ],
            "eatenAss": [
                "Boolean",
                ""
            ],
            "questAsked": [
                "BitFlag",
                [],
                [
                    "Unused",
                    "Amily",
                    "Arian",
                    "Jojo",
                    "Kiha",
                    "Kitsune",
                    "Rathazul",
                    "Shouldra",
                    "Holli",
                    "Clue"
                ]
            ],
            "vineTimer": [
                "Int",
                ""
            ],
            "alrauneKilled": [
                "Int",
                ""
            ],
            "vinesTaken": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('alrauneKilled', int, 0, 'alraune', 'alrauneKilled', notes="How many Alraune you've killed thus far.")
        self.addSSOProperty('eatenAss', bool, False, 'alraune', 'eatenAss')
        self.addSSOProperty('metAlraune', bool, False, 'alraune', 'metAlraune')
        self.addSSOProperty('questAsked', int, 0, 'alraune', 'questAsked', notes='Which stages have you done while trying to figure out how to remove the vines.', bitflags={
            1: 'Unused',
            2: 'Amily',
            4: 'Arian',
            8: 'Jojo',
            16: 'Kiha',
            32: 'Kitsune',
            64: 'Rathazul',
            128: 'Shouldra',
            256: 'Holli',
            512: 'Clue',
        })
        self.addSSOProperty('vineTimer', int, 0, 'alraune', 'vineTimer')
        self.addSSOProperty('vinesTaken', bool, False, 'alraune', 'vinesTaken')
