from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class LibraryNPC(BaseNPC):
    ID = 'library'
    NAME = 'Library'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Library.as: public class Library extends TelAdreAbstractContent { @ dcCe3n8FVec74d8bIz6nA3eq19Q9CF18o6vH61L3Osen306892MfUfbyOdFi36w00P9fd5iR4mr2GI4p59Mv1B4b3PfBlgz2
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('MALI_TAKEN_BLADE', int, 0, HGGKFlags.MALI_TAKEN_BLADE)
        self.addKFlag('TIMES_BEEN_TO_LIBRARY', int, 0, HGGKFlags.TIMES_BEEN_TO_LIBRARY)
        self.addKFlag('TIMES_VISITED_MALI', int, 0, HGGKFlags.TIMES_VISITED_MALI)
