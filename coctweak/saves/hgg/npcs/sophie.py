from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class SophieNPC(BaseNPC):
    ID = 'sophie'
    NAME = 'Sophie'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/Sophie.as: public class Sophie extends Harpy { @ asA2Ty9fH5rzgp1c509ELd0q1101Lm7p4ggLfU158ZdX3gu0eIlaa89s96hlaXe491a2xgc26V5dI4cWy6T53GJ0QN67Qaoj
        ## from [HGG]/classes/classes/Scenes/NPCs/SophieFollowerScene.as: public class SophieFollowerScene extends NPCAwareContent { @ 9FM0Zh02vdYJ8REf3F55j5r5aN18vw90n59o4Z24lodof02138F3LI2L8etagwj3kW8FwfIH6NA5KB1Qv9Xcg3Df511wu2le
        ## from [HGG]/classes/classes/Scenes/NPCs/SophieScene.as: public class SophieScene extends BaseContent implements TimeAwareInterface { @ 8edbReeUS6Vr30J2sW2Hfg1490B3CmbCmcSVfi7edw9Wj9W79QY99kbDq7kedOU0Gdgr89j72VceAi9Ei3Ar0tC0u270hefp
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('BREASTFEAD_SOPHIE_COUNTER', int, 0, HGGKFlags.BREASTFEAD_SOPHIE_COUNTER)
        self.addKFlag('DAUGHTER_FOUR_BIMBO', int, 0, HGGKFlags.DAUGHTER_FOUR_BIMBO)
        self.addKFlag('DAUGHTER_ONE_BIMBO', int, 0, HGGKFlags.DAUGHTER_ONE_BIMBO)
        self.addKFlag('DAUGHTER_THREE_BIMBO', int, 0, HGGKFlags.DAUGHTER_THREE_BIMBO)
        self.addKFlag('DAUGHTER_TWO_BIMBO', int, 0, HGGKFlags.DAUGHTER_TWO_BIMBO)
        self.addKFlag('FARM_EGG_COUNTDOWN', int, 0, HGGKFlags.FARM_EGG_COUNTDOWN)
        self.addKFlag('FARM_EGG_STORED', int, 0, HGGKFlags.FARM_EGG_STORED)
        self.addKFlag('FOLLOWER_AT_FARM_SOPHIE', int, 0, HGGKFlags.FOLLOWER_AT_FARM_SOPHIE)
        self.addKFlag('FOLLOWER_PRODUCTION_SOPHIE', int, 0, HGGKFlags.FOLLOWER_PRODUCTION_SOPHIE)
        self.addKFlag('FOLLOWER_PRODUCTION_SOPHIE_COLORCHOICE', str, '', HGGKFlags.FOLLOWER_PRODUCTION_SOPHIE_COLORCHOICE, choices={
            'Black': 'Black',
            'Blue': 'Blue',
            'Brown': 'Brown',
            'Pink': 'Pink',
            'Purple': 'Purple',
            'White': 'White',
            '': '',
        })
        self.addKFlag('FUCKED_SOPHIE_COUNTER', int, 0, HGGKFlags.FUCKED_SOPHIE_COUNTER)
        self.addKFlag('MET_SOPHIE_COUNTER', int, 0, HGGKFlags.MET_SOPHIE_COUNTER)
        self.addKFlag('NO_PURE_SOPHIE_RECRUITMENT', int, 0, HGGKFlags.NO_PURE_SOPHIE_RECRUITMENT)
        self.addKFlag('SOPHIES_DAUGHTERS_DEBIMBOED', int, 0, HGGKFlags.SOPHIES_DAUGHTERS_DEBIMBOED)
        self.addKFlag('SOPHIE_ADULT_KID_COUNT', int, 0, HGGKFlags.SOPHIE_ADULT_KID_COUNT)
        self.addKFlag('SOPHIE_ANGRY_AT_PC_COUNTER', int, 0, HGGKFlags.SOPHIE_ANGRY_AT_PC_COUNTER)
        self.addKFlag('SOPHIE_BIMBO', int, 0, HGGKFlags.SOPHIE_BIMBO)
        self.addKFlag('SOPHIE_CAMP_EGG_COUNTDOWN', int, 0, HGGKFlags.SOPHIE_CAMP_EGG_COUNTDOWN)
        self.addKFlag('SOPHIE_DAUGHTER_MATURITY_COUNTER', int, 0, HGGKFlags.SOPHIE_DAUGHTER_MATURITY_COUNTER)
        self.addKFlag('SOPHIE_DEBIMBOED', int, 0, HGGKFlags.SOPHIE_DEBIMBOED)
        self.addKFlag('SOPHIE_DISABLED_FOREVER', int, 0, HGGKFlags.SOPHIE_DISABLED_FOREVER)
        self.addKFlag('SOPHIE_EGGS_LAID', int, 0, HGGKFlags.SOPHIE_EGGS_LAID)
        self.addKFlag('SOPHIE_FAMILY_INCEST', int, 0, HGGKFlags.SOPHIE_FAMILY_INCEST)
        self.addKFlag('SOPHIE_FOLLOWER_IRRITATION', int, 0, HGGKFlags.SOPHIE_FOLLOWER_IRRITATION)
        self.addKFlag('SOPHIE_FOLLOWER_PROGRESS', int, 0, HGGKFlags.SOPHIE_FOLLOWER_PROGRESS)
        self.addKFlag('SOPHIE_HEAT_COUNTER', int, 0, HGGKFlags.SOPHIE_HEAT_COUNTER)
        # PregnancyStore vag incubation counter
        self.addKFlag('SOPHIE_INCUBATION', int, 0, HGGKFlags.SOPHIE_INCUBATION)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('SOPHIE_PREGNANCY_TYPE', int, 0, HGGKFlags.SOPHIE_PREGNANCY_TYPE)
        self.addKFlag('SOPHIE_RECRUITED_PURE', int, 0, HGGKFlags.SOPHIE_RECRUITED_PURE)
        self.addKFlag('SOPHIE_TATTOO_BUTT', int, 0, HGGKFlags.SOPHIE_TATTOO_BUTT)
        self.addKFlag('SOPHIE_TATTOO_COLLARBONE', int, 0, HGGKFlags.SOPHIE_TATTOO_COLLARBONE)
        self.addKFlag('SOPHIE_TATTOO_LOWERBACK', int, 0, HGGKFlags.SOPHIE_TATTOO_LOWERBACK)
        self.addKFlag('SOPHIE_TATTOO_SHOULDERS', int, 0, HGGKFlags.SOPHIE_TATTOO_SHOULDERS)
        self.addKFlag('TIMES_FUCKED_SOPHIE_LESBIAN', int, 0, HGGKFlags.TIMES_FUCKED_SOPHIE_LESBIAN)
        self.addKFlag('TIMES_MORNING_SOPHIE_FEMDOMMED', int, 0, HGGKFlags.TIMES_MORNING_SOPHIE_FEMDOMMED)
        self.addKFlag('TIMES_PISSED_OFF_SOPHIE_COUNTER', int, 0, HGGKFlags.TIMES_PISSED_OFF_SOPHIE_COUNTER)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.SOPHIE_PREGNANCY_TYPE, HGGKFlags.SOPHIE_INCUBATION)
