from coctweak.saves._npc import BaseNPC, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class FetishCultistNPC(BaseNPC):
    ID = 'fetishcultist'
    NAME = 'FetishCultist'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Lake/FetishCultistScene.as: public class FetishCultistScene extends AbstractLakeContent implements Encounter { @ 8rrgqd0bg0f7f0Ta4a33Bh1B1ll8ur8aR42ea1821A6tO1DOgK4c6cdCKaDMceL1YsbQ51bq8jh1e08Vc2VU8B68r8bIXe7c
        super().__init__(save)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('FetishOn', bool, False, HGGStatusLib.FetishOn, None, notes='Whether Fetish Cultists are available to be encountered.')
