from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class MinervaNPC(BaseNPC):
    ID = 'minerva'
    NAME = 'Minerva'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/HighMountains/MinervaPurification.as: public class MinervaPurification extends BaseContent { @ 7EW1mNgW49FxaBleUj99U2KdePT396cmZ2M7fsjbFD2Xw5zmcpE9pJ6dR3XhdTyc44cBidBZ5bsdlK9Qufp67Ur3zS1OzcpE
        ## from [HGG]/classes/classes/Scenes/Areas/HighMountains/MinervaScene.as: public class MinervaScene extends BaseContent implements TimeAwareInterface { @ ccodVzgbcdzW9HH1Jybbs8p6a9HaJy5VH8xN30Ef8zey6fNn0EGaAL5a7aAi8TP7rc9kCeB7gIJ5Vp3wH3BCdjc3Pd6i81E7
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('MET_MINERVA', int, 0, HGGKFlags.MET_MINERVA)
        self.addKFlag('MINERVA_BACKSTORY', int, 0, HGGKFlags.MINERVA_BACKSTORY, notes='How much of her backstory she has shared.', choices={
            0: 'None',
            1: 'Part 1 only',
            2: 'Up to Part 2',
        })
        self.addKFlag('MINERVA_BACKSTORY_LEARNED', int, 0, HGGKFlags.MINERVA_BACKSTORY_LEARNED, notes="Whether you have learned ALL of Minerva's backstory.", choices={
            0: 'No',
            1: 'Yes',
        })
        self.addKFlag('MINERVA_CHILDREN', int, 0, HGGKFlags.MINERVA_CHILDREN)
        self.addKFlag('MINERVA_CORRUPTION_PROGRESS', int, 0, HGGKFlags.MINERVA_CORRUPTION_PROGRESS)
        self.addKFlag('MINERVA_LEZZES_OUT', int, 0, HGGKFlags.MINERVA_LEZZES_OUT)
        self.addKFlag('MINERVA_LOVE', int, 0, HGGKFlags.MINERVA_LOVE)
        # PregnancyStore vag incubation counter
        self.addKFlag('MINERVA_PREGNANCY_INCUBATION', int, 0, HGGKFlags.MINERVA_PREGNANCY_INCUBATION)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('MINERVA_PREGNANCY_TYPE', int, 0, HGGKFlags.MINERVA_PREGNANCY_TYPE)
        self.addKFlag('MINERVA_PURIFICATION_JOJO_TALKED', int, 0, HGGKFlags.MINERVA_PURIFICATION_JOJO_TALKED)
        self.addKFlag('MINERVA_PURIFICATION_MARAE_TALKED', int, 0, HGGKFlags.MINERVA_PURIFICATION_MARAE_TALKED)
        self.addKFlag('MINERVA_PURIFICATION_PROGRESS', int, 0, HGGKFlags.MINERVA_PURIFICATION_PROGRESS)
        self.addKFlag('MINERVA_PURIFICATION_RATHAZUL_TALKED', int, 0, HGGKFlags.MINERVA_PURIFICATION_RATHAZUL_TALKED)
        self.addKFlag('MINERVA_TOWER_REPAIRED', int, 0, HGGKFlags.MINERVA_TOWER_REPAIRED)
        self.addKFlag('MINERVA_TOWER_TREE', int, 0, HGGKFlags.MINERVA_TOWER_TREE)
        self.addKFlag('TIMES_BIRTHED_SHARPIES', int, 0, HGGKFlags.TIMES_BIRTHED_SHARPIES)
        self.addKFlag('TIMES_BUTTFUCKED_MINERVA', int, 0, HGGKFlags.TIMES_BUTTFUCKED_MINERVA)
        self.addKFlag('TIMES_MINERVA_COWGIRLED', int, 0, HGGKFlags.TIMES_MINERVA_COWGIRLED)
        self.addKFlag('TIMES_MINERVA_LAPSEXED', int, 0, HGGKFlags.TIMES_MINERVA_LAPSEXED)
        self.addKFlag('TIMES_MINERVA_SEXED', int, 0, HGGKFlags.TIMES_MINERVA_SEXED)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.MINERVA_PREGNANCY_TYPE, HGGKFlags.MINERVA_PREGNANCY_INCUBATION)
