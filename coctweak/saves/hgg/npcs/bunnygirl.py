from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class BunnyGirlNPC(BaseNPC):
    ID = 'bunnygirl'
    NAME = 'BunnyGirl'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Plains/BunnyGirl.as: public class BunnyGirl extends BaseContent { @ 59f4MxfQ23lefld8ntcVtfhx67872v8yX99kb2S6Mm8e3aUCaiTgw5eFR8Yv6MZcmMbEb7go4wXfRS7eQ9uP9dY3yG2oq8gD
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('MET_BUNBUN', int, 0, HGGKFlags.MET_BUNBUN)
