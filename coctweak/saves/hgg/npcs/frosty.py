from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class FrostyNPC(BaseNPC):
    ID = 'frosty'
    NAME = 'Frosty'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Frosty.as: public class Frosty extends TelAdreAbstractContent { @ ciXdp71rTePneYZaHB8kC01rbLw0F98fp2mndD5ed4g8M13c9JH9wL1SZbEZ3y3eno5P22qleidck8gGK3RY3hP70aeYDeuI
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('FROSTY_POINTS', int, 0, HGGKFlags.FROSTY_POINTS)
        self.addKFlag('MET_FROSTY', int, 0, HGGKFlags.MET_FROSTY)
        self.addKFlag('SEEN_GENDERLESS_FROSTY_REJECTION', int, 0, HGGKFlags.SEEN_GENDERLESS_FROSTY_REJECTION)
        self.addKFlag('SIGNED_FROSTYS_CONTRACT', int, 0, HGGKFlags.SIGNED_FROSTYS_CONTRACT)
