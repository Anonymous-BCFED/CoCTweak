from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class IfrisNPC(BaseNPC):
    ID = 'ifris'
    NAME = 'Ifris'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Ifris.as: public class Ifris extends TelAdreAbstractContent { @ cau24Pe3g3iG9SAgo20beb785dN6KRcuS1exbG1eSC5tK4dM9uP1cB5N4ftk7ll2VifNd6BM8ui0zTgA1eyw6uxbDNasQa7j
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('IFRIS_SHOWED_OFF', int, 0, HGGKFlags.IFRIS_SHOWED_OFF)
        self.addKFlag('MET_IFRIS', int, 0, HGGKFlags.MET_IFRIS)
        self.addKFlag('TIMES_FUCKED_IFRIS_BLOWJOB', int, 0, HGGKFlags.TIMES_FUCKED_IFRIS_BLOWJOB)
        self.addKFlag('TIMES_FUCKED_IFRIS_LICKED', int, 0, HGGKFlags.TIMES_FUCKED_IFRIS_LICKED)
