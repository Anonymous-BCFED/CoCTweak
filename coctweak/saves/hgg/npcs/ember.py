from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class EmberNPC(BaseNPC):
    ID = 'ember'
    NAME = 'Ember'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/Ember.as: public class Ember extends Monster { @ dtg1dUab39y6dkk1vDd9xanKgF13vn5fR04I2q9fXEb3E6r63OqcsS99IfuJbtY6eh30PfegbBTe8tbQA3yO9YR82w1DC6QX
        ## from [HGG]/classes/classes/Scenes/NPCs/EmberScene.as: public class EmberScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface, VaginalPregnancy { @ c3o44hfob56M4Y9c8f3MhglJdjjfPL0Sxabh0V103vdNs49Y7nD26oexndmk8dggnKd97fHw0eRg2B6uw0iJb3Ggm9dQt9F4
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "flowerExplained": [
                "Boolean",
                ""
            ],
            "newbornGender": [
                "IntList",
                "",
                [
                    {
                        "label": "None",
                        "data": 0
                    },
                    {
                        "label": "Male",
                        "data": 1
                    },
                    {
                        "label": "Female",
                        "data": 2
                    },
                    {
                        "label": "Herm",
                        "data": 3
                    }
                ]
            ],
            "birthTime": [
                "Int",
                ""
            ],
            "learnedFeeding": [
                "Boolean",
                ""
            ],
            "eggArray": [
                "Array",
                "",
                [
                    "int",
                    ""
                ]
            ],
            "hatchedToday": [
                "Int",
                ""
            ],
            "tuckedToday": [
                "Int",
                ""
            ]
        }
        '''
        self.addSSOProperty('birthTime', int, 0, 'ember', 'birthTime')
        self.addSSOProperty('eggArray', list, [], 'ember', 'eggArray')
        self.addSSOProperty('flowerExplained', bool, False, 'ember', 'flowerExplained')
        self.addSSOProperty('hatchedToday', int, 0, 'ember', 'hatchedToday')
        self.addSSOProperty('learnedFeeding', bool, False, 'ember', 'learnedFeeding')
        self.addSSOProperty('newbornGender', int, 0, 'ember', 'newbornGender', choices={
            0: 'None',
            1: 'Male',
            2: 'Female',
            3: 'Herm',
        })
        self.addSSOProperty('tuckedToday', int, 0, 'ember', 'tuckedToday')
        ##########
        # kFlags
        ##########
        # Cooldown for ember TFs
        self.addKFlag('DRANK_EMBER_BLOOD_TODAY', int, 0, HGGKFlags.DRANK_EMBER_BLOOD_TODAY)
        # PC Smash!? ember's egg?
        self.addKFlag('EGG_BROKEN', int, 0, HGGKFlags.EGG_BROKEN)
        # : Pretty obvious
        self.addKFlag('EMBER_AFFECTION', int, 0, HGGKFlags.EMBER_AFFECTION)
        #  0 for normal Ember, 1 for child Ember.
        self.addKFlag('EMBER_AGE', int, 0, HGGKFlags.EMBER_AGE)
        self.addKFlag('EMBER_BITCHES_ABOUT_PREGNANT_PC', int, 0, HGGKFlags.EMBER_BITCHES_ABOUT_PREGNANT_PC)
        self.addKFlag('EMBER_CHILDREN_FEMALES', int, 0, HGGKFlags.EMBER_CHILDREN_FEMALES)
        self.addKFlag('EMBER_CHILDREN_HERMS', int, 0, HGGKFlags.EMBER_CHILDREN_HERMS)
        self.addKFlag('EMBER_CHILDREN_MALES', int, 0, HGGKFlags.EMBER_CHILDREN_MALES)
        # Controls Ember's current corruption levels, only default/dragon-girl Ember uses this. (Default starting value = 50)
        self.addKFlag('EMBER_COR', int, 0, HGGKFlags.EMBER_COR)
        #  Used to trigger minotaurJizzFreakout()
        self.addKFlag('EMBER_CURRENTLY_FREAKING_ABOUT_MINOCUM', int, 0, HGGKFlags.EMBER_CURRENTLY_FREAKING_ABOUT_MINOCUM)
        self.addKFlag('EMBER_EGGS', int, 0, HGGKFlags.EMBER_EGGS)
        # This controls when it's time to hatch. Every item use and every time you use the egg as a masturbation aid, this will be incremented. Threshold for birthing is 5, but the birthing process can only be triggered when using as a masturbatory aid. This is done to allow players the chance to modify Ember before actually hatching.
        self.addKFlag('EMBER_EGG_FLUID_COUNT', int, 0, HGGKFlags.EMBER_EGG_FLUID_COUNT)
        #  1 for male, 2 for female, 3 for herm. This also controls the egg's shell color.
        self.addKFlag('EMBER_GENDER', int, 0, HGGKFlags.EMBER_GENDER)
        # 0 for no hair, 1 for hair, 2 for mane.
        self.addKFlag('EMBER_HAIR', int, 0, HGGKFlags.EMBER_HAIR)
        # : is ember hatched? 1 = true
        self.addKFlag('EMBER_HATCHED', int, 0, HGGKFlags.EMBER_HATCHED)
        self.addKFlag('EMBER_HERM_EGGS', int, 0, HGGKFlags.EMBER_HERM_EGGS)
        # PregnancyStore vag incubation counter
        self.addKFlag('EMBER_INCUBATION', int, 0, HGGKFlags.EMBER_INCUBATION)
        # Dragon-girl Ember can have either an internal sheath to keep [Ember eir] dick in or have it be more human-like. 0 = internal, 1 = external.
        self.addKFlag('EMBER_INTERNAL_DICK', int, 0, HGGKFlags.EMBER_INTERNAL_DICK)
        # Has the PC masturbated on the egg yet? Needed to hatcH!
        self.addKFlag('EMBER_JACKED_ON', int, 0, HGGKFlags.EMBER_JACKED_ON)
        self.addKFlag('EMBER_LUST_BITCHING_COUNTER', int, 0, HGGKFlags.EMBER_LUST_BITCHING_COUNTER)
        # 0 for no lactation, 1 for lactating.
        self.addKFlag('EMBER_MILK', int, 0, HGGKFlags.EMBER_MILK)
        self.addKFlag('EMBER_MORNING', int, 0, HGGKFlags.EMBER_MORNING)
        # 0 for no egg laying, 1 for egg laying.
        self.addKFlag('EMBER_OVIPOSITION', int, 0, HGGKFlags.EMBER_OVIPOSITION)
        # Used to trigger emberBitchesAboutPCBeingFullOfEggs()
        self.addKFlag('EMBER_OVI_BITCHED_YET', int, 0, HGGKFlags.EMBER_OVI_BITCHED_YET)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('EMBER_PREGNANCY_TYPE', int, 0, HGGKFlags.EMBER_PREGNANCY_TYPE)
        self.addKFlag('EMBER_PREGNANT_TALK', int, 0, HGGKFlags.EMBER_PREGNANT_TALK)
        self.addKFlag('EMBER_PUSSY_FUCK_COUNT', int, 0, HGGKFlags.EMBER_PUSSY_FUCK_COUNT)
        # 0 for anthro Ember, 1 for dragon-girl Ember. (You might want to control this with the Type flag since only default Embers use this variable.)
        self.addKFlag('EMBER_ROUNDFACE', int, 0, HGGKFlags.EMBER_ROUNDFACE)
        self.addKFlag('EMBER_SPAR_VICTORIES', int, 0, HGGKFlags.EMBER_SPAR_VICTORIES)
        self.addKFlag('EMBER_TALKS_TO_PC_ABOUT_PC_MOTHERING_DRAGONS', int, 0, HGGKFlags.EMBER_TALKS_TO_PC_ABOUT_PC_MOTHERING_DRAGONS)
        self.addKFlag('GIFTED_FLOWER', int, 0, HGGKFlags.GIFTED_FLOWER)
        self.addKFlag('TIMES_BUTTFUCKED_EMBER', int, 0, HGGKFlags.TIMES_BUTTFUCKED_EMBER)
        self.addKFlag('TIMES_EMBER_LUSTY_FUCKED', int, 0, HGGKFlags.TIMES_EMBER_LUSTY_FUCKED)
        self.addKFlag('TIMES_EQUIPPED_EMBER_SHIELD', int, 0, HGGKFlags.TIMES_EQUIPPED_EMBER_SHIELD)
        # Times stumbled into ze egg.
        self.addKFlag('TIMES_FOUND_EMBERS_EGG', int, 0, HGGKFlags.TIMES_FOUND_EMBERS_EGG)
        self.addKFlag('TIMES_SLEPT_WITH_EMBER', int, 0, HGGKFlags.TIMES_SLEPT_WITH_EMBER)
        # PC Take ember's egg home?
        self.addKFlag('TOOK_EMBER_EGG', int, 0, HGGKFlags.TOOK_EMBER_EGG)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('EmberFuckCooldown', bool, False, HGGStatusLib.EmberFuckCooldown, None)
        self.addSFXProperty('EmberNapping', bool, False, HGGStatusLib.EmberNapping, None)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.EMBER_PREGNANCY_TYPE, HGGKFlags.EMBER_INCUBATION)
