from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class DemonFistFighterNPC(BaseNPC):
    ID = 'demonfistfighter'
    NAME = 'DemonFistFighter'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/DemonFistFighter.as: public class DemonFistFighter extends Monster { @ 5U957g7bN1P45ppc4k9frbgA5WcepV6ikdghe4v48l2Fh4oG3Q7ctv3BR2YR8gt1OPgPcdAB9f31sqeBCfwQcjQ5Kc8Xig04
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/DemonFistFighterScene.as: public class DemonFistFighterScene extends BaseContent implements SelfSaving, TimeAwareInterface { @ eCt7Tz4fI18d6sef0o5dddEg4HPcC63LuciW1TL3J9bs44Sb88h8Xq28E7kRcTbfDO2ht1LV1Tn4Jp6uU7XO9v47IB2zZcuG
        super().__init__(save)
        #############
        # SSO Props
        #############
        self.addSSOProperty('beatDemonfist', bool, False, 'demonfistFighter', 'beatDemonfist', notes="Whether you've defeated Demonfist.")
        self.addSSOProperty('consecutiveLosses', int, 0, 'demonfistFighter', 'consecutiveLosses')
        self.addSSOProperty('demonfistTimeAway', int, 0, 'demonfistFighter', 'demonfistTimeAway')
        self.addSSOProperty('learnedOfDemonFist', bool, False, 'demonfistFighter', 'learnedOfDemonFist')
        self.addSSOProperty('newRulesExplained', bool, True, 'demonfistFighter', 'newRulesExplained')
        self.addSSOProperty('playerName', str, '""', 'demonfistFighter', 'playerName', notes='Your ring-name.')
        self.addSSOProperty('shookDogHand', bool, False, 'demonfistFighter', 'shookDogHand')
        self.addSSOProperty('timesBrokenRules', int, 0, 'demonfistFighter', 'timesBrokenRules')
        self.addSSOProperty('timesFoughtDog', int, 0, 'demonfistFighter', 'timesFoughtDog')
        self.addSSOProperty('timesLost', int, 0, 'demonfistFighter', 'timesLost')
        self.addSSOProperty('timesWon', int, 0, 'demonfistFighter', 'timesWon')
