from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class EssrayleNPC(BaseNPC):
    ID = 'essrayle'
    NAME = 'Essrayle'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/Essrayle.as: public class Essrayle extends BaseContent { @ 3978fb0y09KCblje1pduleXAecV8Oa0p75oqf6GcIF0etehecQPesk9Jy6JN2q12evcD2ght1nGfps9euemD0P17ay2xFdV9
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('ACCEPTED_ESSY_FIRST_MEETING', int, 0, HGGKFlags.ACCEPTED_ESSY_FIRST_MEETING)
        self.addKFlag('ESSRAYLE_ESCAPED_DUNGEON', int, 0, HGGKFlags.ESSRAYLE_ESCAPED_DUNGEON)
        self.addKFlag('ESSY_DUNGEON_FUCKED', int, 0, HGGKFlags.ESSY_DUNGEON_FUCKED)
        self.addKFlag('ESSY_MET_IN_DUNGEON', int, 0, HGGKFlags.ESSY_MET_IN_DUNGEON)
        self.addKFlag('MET_ESSY', int, 0, HGGKFlags.MET_ESSY)
        self.addKFlag('TOLD_MOTHER_TO_RELEASE_ESSY', int, 0, HGGKFlags.TOLD_MOTHER_TO_RELEASE_ESSY)
        self.addKFlag('TURNED_DOWN_ESSY_FIRST_MEETING', int, 0, HGGKFlags.TURNED_DOWN_ESSY_FIRST_MEETING)
