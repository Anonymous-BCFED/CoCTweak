from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class PhoukaNPC(BaseNPC):
    ID = 'phouka'
    NAME = 'Phouka'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/Phouka.as: public class Phouka extends Monster { @ 1ew0gadBUcNj9Xh7AI1VJ8kX8m9dRI0Dp0v08Fj4481Mh7wP9aI5e68nK5NXfcC2zQ09f76nbEch1H28d0Judpd96i9vZ4bp
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/PhoukaScene.as: public class PhoukaScene extends BaseContent implements TimeAwareInterface, VaginalPregnancy { @ 95AfAWd8zdml8Dy1Gv6fq75QaV4cMLdMmeRuaxh8mQ9oqc5H5AO6rWblf7qN3LFcDJ7oNdMt7q0766ghy4Cbfew5xP0QR2LY
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('PHOUKA_ENCOUNTER_STATUS', int, 0, HGGKFlags.PHOUKA_ENCOUNTER_STATUS, choices={
            0: 'Not encountered',
            1: 'Encountered',
        })
        self.addKFlag('PHOUKA_LORE', int, 0, HGGKFlags.PHOUKA_LORE, notes='Whether you know what to call them.', intbool=True)
        self.addKFlag('TREACLE_MINE_YEAR_DONE', int, 0, HGGKFlags.TREACLE_MINE_YEAR_DONE, readonly=True, notes="Whether you've done the treacle mine.  Value is a year.")
