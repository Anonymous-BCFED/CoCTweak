from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class AmarokNPC(BaseNPC):
    ID = 'amarok'
    NAME = 'Amarok'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/GlacialRift/AmarokScene.as: public class AmarokScene extends BaseContent { @ 6m1gpOahRdf8gyCaEQ0SKcWu3X2fwc4irgDG9NFah3ey3eFq4FyaRK1RV3qPc9LfSP6TedID4Eqd8CbxpbBZ8jE4kg7wEaPM
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('AMAROK_LOSSES', int, 0, HGGKFlags.AMAROK_LOSSES)
