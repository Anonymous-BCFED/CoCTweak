from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.enums.gender import HGGGender
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class IzumiNPC(BaseNPC):
    ID = 'izumi'
    NAME = 'Izumi'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/HighMountains/IzumiScene.as: public class IzumiScene extends BaseContent implements SelfSaving, SelfDebug { @ eigaoOewzc2KfS5fOh3JjezMfg33Yx4k478Z74R7oaa628RCfj0g9ha102kz3PccTP51p3nUaHJcyG9JI81naPkb8z0o1azh
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "contestStage": [
                "NumberList",
                "",
                [
                    {
                        "label": "",
                        "data": 0
                    },
                    {
                        "label": "Failed 1",
                        "data": 0.5
                    },
                    {
                        "label": "Cleared 1",
                        "data": 1
                    },
                    {
                        "label": "Failed 2",
                        "data": 1.5
                    },
                    {
                        "label": "Cleared 2",
                        "data": 2
                    },
                    {
                        "label": "Failed 3",
                        "data": 2.5
                    },
                    {
                        "label": "Cleared 3",
                        "data": 3
                    }
                ]
            ]
        }
        '''
        self.addSSOProperty('contestStage', float, 0, 'izumi', 'contestStage', choices={
            0: '',
            0.5: 'Failed 1',
            1: 'Cleared 1',
            1.5: 'Failed 2',
            2: 'Cleared 2',
            2.5: 'Failed 3',
            3: 'Cleared 3',
        })
        ##########
        # kFlags
        ##########
        self.addKFlag('IZUMI_LAST_ENCOUNTER', int, 0, HGGKFlags.IZUMI_LAST_ENCOUNTER, notes='What you did during your last encounter with Izumi.', choices={
            0: 'N/A (No encounter yet)',
            1: 'Izumi won the fight.',
            2: 'You left.',
            3: 'Izumi lost the fight.',
        })
        self.addKFlag('IZUMI_MET', int, 0, HGGKFlags.IZUMI_MET, notes='Whether you met Izumi or not.', choices={
            0: 'No',
            1: 'Yes',
        })
        self.addKFlag('IZUMI_SEEN_PC_GENDER', int, 0, HGGKFlags.IZUMI_SEEN_PC_GENDER, enumType=HGGGender, notes="Izumi's seen your true gender.")
        self.addKFlag('IZUMI_TIMES_GRABBED_THE_HORN', int, 0, HGGKFlags.IZUMI_TIMES_GRABBED_THE_HORN, notes='The number of times you beat Izumi in combat.')
        self.addKFlag('IZUMI_TIMES_SUBMITTED', int, 0, HGGKFlags.IZUMI_TIMES_SUBMITTED, notes="How many times you've lost to Izumi and submitted to her.")
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('IzumisPipeSmoke', bool, False, HGGStatusLib.IzumisPipeSmoke, None)
