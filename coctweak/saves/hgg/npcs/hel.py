from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class HelNPC(BaseNPC):
    ID = 'hel'
    NAME = 'Hel'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/HelFollower.as: public class HelFollower extends NPCAwareContent { @ 2Xeee07qw350eLd16t9UD5P84kMayJ0tU5cUbFs0wE2yd8lO8HOf0x8wyalQb2TbfCeJydPmbNf83K7oY2bieeV4aXbXbeOr
        ## from [HGG]/classes/classes/Scenes/NPCs/HelScene.as: public class HelScene extends NPCAwareContent implements TimeAwareInterface { @ atF6yJ9uJ5UJ7MlcI1fyL50SgrHbecgRi1sT9ej5ma5es8CoeTLgLg49eb4o2in2zl87mgPWfPi47hc5s5216Y6e78gfU8go
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('FOLLOWER_HEL_TALKS', int, 0, HGGKFlags.FOLLOWER_HEL_TALKS)
        self.addKFlag('HELIA_ANAL_TRAINING', int, 0, HGGKFlags.HELIA_ANAL_TRAINING)
        self.addKFlag('HELIA_ANAL_TRAINING_OFFERED', int, 0, HGGKFlags.HELIA_ANAL_TRAINING_OFFERED)
        self.addKFlag('HELIA_BDAY_DRINKS', int, 0, HGGKFlags.HELIA_BDAY_DRINKS)
        self.addKFlag('HELIA_BDAY_FOX_TWINS', int, 0, HGGKFlags.HELIA_BDAY_FOX_TWINS)
        self.addKFlag('HELIA_BDAY_HAKON_AND_KIRI', int, 0, HGGKFlags.HELIA_BDAY_HAKON_AND_KIRI)
        self.addKFlag('HELIA_BDAY_PHOENIXES', int, 0, HGGKFlags.HELIA_BDAY_PHOENIXES)
        self.addKFlag('HELIA_BIRTHDAY_OFFERED', int, 0, HGGKFlags.HELIA_BIRTHDAY_OFFERED)
        self.addKFlag('HELIA_FOLLOWER_DISABLED', int, 0, HGGKFlags.HELIA_FOLLOWER_DISABLED)
        self.addKFlag('HELIA_KIDS_CHAT', int, 0, HGGKFlags.HELIA_KIDS_CHAT)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('HELIA_PREGNANCY_TYPE', int, 0, HGGKFlags.HELIA_PREGNANCY_TYPE)
        self.addKFlag('HELIA_SPAR_VICTORIES', int, 0, HGGKFlags.HELIA_SPAR_VICTORIES)
        self.addKFlag('HELIA_TALK_SEVEN', int, 0, HGGKFlags.HELIA_TALK_SEVEN)
        self.addKFlag('HEL_AFFECTION', int, 0, HGGKFlags.HEL_AFFECTION)
        self.addKFlag('HEL_AFFECTION_FOLLOWER', int, 0, HGGKFlags.HEL_AFFECTION_FOLLOWER)
        self.addKFlag('HEL_BONUS_POINTS', int, 0, HGGKFlags.HEL_BONUS_POINTS)
        self.addKFlag('HEL_CAN_SWIM', int, 0, HGGKFlags.HEL_CAN_SWIM)
        self.addKFlag('HEL_EDRYN_OFFER', int, 0, HGGKFlags.HEL_EDRYN_OFFER)
        self.addKFlag('HEL_FOLLOWER_LEVEL', int, 0, HGGKFlags.HEL_FOLLOWER_LEVEL)
        self.addKFlag('HEL_FOXY_FOURSOME_WARNED', int, 0, HGGKFlags.HEL_FOXY_FOURSOME_WARNED)
        self.addKFlag('HEL_FUCKBUDDY', int, 0, HGGKFlags.HEL_FUCKBUDDY)
        self.addKFlag('HEL_FUCK_COUNTER', int, 0, HGGKFlags.HEL_FUCK_COUNTER)
        self.addKFlag('HEL_GUARDING', int, 0, HGGKFlags.HEL_GUARDING)
        self.addKFlag('HEL_HARPY_QUEEN_DEFEATED', int, 0, HGGKFlags.HEL_HARPY_QUEEN_DEFEATED)
        self.addKFlag('HEL_INTROS_LEVEL', int, 0, HGGKFlags.HEL_INTROS_LEVEL)
        self.addKFlag('HEL_ISABELLA_THREESOME_ENABLED', int, 0, HGGKFlags.HEL_ISABELLA_THREESOME_ENABLED)
        self.addKFlag('HEL_LOVE', int, 0, HGGKFlags.HEL_LOVE)
        self.addKFlag('HEL_NTR_TRACKER', int, 0, HGGKFlags.HEL_NTR_TRACKER)
        # PregnancyStore vag incubation counter
        self.addKFlag('HEL_PREGNANCY_INCUBATION', int, 0, HGGKFlags.HEL_PREGNANCY_INCUBATION)
        self.addKFlag('HEL_RAPED_TODAY', int, 0, HGGKFlags.HEL_RAPED_TODAY)
        self.addKFlag('HEL_TALKED_ABOUT_ATTACKING_YOU', int, 0, HGGKFlags.HEL_TALKED_ABOUT_ATTACKING_YOU)
        self.addKFlag('HEL_TALKED_ABOUT_BERSERKING', int, 0, HGGKFlags.HEL_TALKED_ABOUT_BERSERKING)
        self.addKFlag('HEL_TALKED_ABOUT_HER', int, 0, HGGKFlags.HEL_TALKED_ABOUT_HER)
        self.addKFlag('HEL_TALK_EIGHT', int, 0, HGGKFlags.HEL_TALK_EIGHT)
        self.addKFlag('HEL_TIMES_ENCOUNTERED', int, 0, HGGKFlags.HEL_TIMES_ENCOUNTERED)
        self.addKFlag('KEEP_HELIA_AND_SOPHIE', int, 0, HGGKFlags.KEEP_HELIA_AND_SOPHIE)
        self.addKFlag('PC_PROMISED_HEL_MONOGAMY_FUCKS', int, 0, HGGKFlags.PC_PROMISED_HEL_MONOGAMY_FUCKS)
        self.addKFlag('TIMES_HELIA_DOUBLE_DONGED', int, 0, HGGKFlags.TIMES_HELIA_DOUBLE_DONGED)
        self.addKFlag('URTA_MET_HEL', int, 0, HGGKFlags.URTA_MET_HEL)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.HELIA_PREGNANCY_TYPE, HGGKFlags.HEL_PREGNANCY_INCUBATION)
