from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class ScyllaNPC(BaseNPC):
    ID = 'scylla'
    NAME = 'Scylla'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Scylla.as: public class Scylla extends TelAdreAbstractContent { @ gmZ4IncFmcbK4Ts3tf3r71N92V13jU8Yz1aY3MrfML7lz0TN3NT8dR5YucOo5nd34iegdbqUbtNexB0mA3pi2fDgO92b7gkY
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('FED_SCYLLA_TODAY', int, 0, HGGKFlags.FED_SCYLLA_TODAY)
        self.addKFlag('KATHERINE_MET_SCYLLA', int, 0, HGGKFlags.KATHERINE_MET_SCYLLA)
        self.addKFlag('NUMBER_OF_TIMES_MET_SCYLLA', int, 0, HGGKFlags.NUMBER_OF_TIMES_MET_SCYLLA)
        self.addKFlag('SCYLLA_CUM_THERAPY_TIMES', int, 0, HGGKFlags.SCYLLA_CUM_THERAPY_TIMES)
        self.addKFlag('SCYLLA_FURRY_FOURSOME_COUNT', int, 0, HGGKFlags.SCYLLA_FURRY_FOURSOME_COUNT)
        self.addKFlag('SCYLLA_MILK_THERAPY_TIMES', int, 0, HGGKFlags.SCYLLA_MILK_THERAPY_TIMES)
        self.addKFlag('SCYLLA_SEX_THERAPY_TIMES', int, 0, HGGKFlags.SCYLLA_SEX_THERAPY_TIMES)
        self.addKFlag('SCYLLA_SMALLCOCK_INTRO', int, 0, HGGKFlags.SCYLLA_SMALLCOCK_INTRO)
        self.addKFlag('SCYLLA_TIMES_SHARED_IN_ADDICT_GROUP', int, 0, HGGKFlags.SCYLLA_TIMES_SHARED_IN_ADDICT_GROUP)
        self.addKFlag('TIMES_ADVANTAGED_SCYLLA_MULTICOCKS', int, 0, HGGKFlags.TIMES_ADVANTAGED_SCYLLA_MULTICOCKS)
        self.addKFlag('TIMES_CAUGHT_URTA_WITH_SCYLLA', int, 0, HGGKFlags.TIMES_CAUGHT_URTA_WITH_SCYLLA)
        self.addKFlag('TIMES_MET_SCYLLA_IN_ADDICTION_GROUP', int, 0, HGGKFlags.TIMES_MET_SCYLLA_IN_ADDICTION_GROUP)
        self.addKFlag('TIMES_SCYLLA_ADDICT_GROUP_EXPLOITED', int, 0, HGGKFlags.TIMES_SCYLLA_ADDICT_GROUP_EXPLOITED)
        self.addKFlag('URTA_BANNED_FROM_SCYLLA', int, 0, HGGKFlags.URTA_BANNED_FROM_SCYLLA)
        self.addKFlag('URTA_CONFRONTED_SCYLLA', int, 0, HGGKFlags.URTA_CONFRONTED_SCYLLA)
