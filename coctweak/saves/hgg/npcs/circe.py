from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class CirceNPC(BaseNPC):
    ID = 'circe'
    NAME = 'Circe'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/VolcanicCrag/CorruptedCoven.as: public class CorruptedCoven extends BaseContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 7SKd2Jff5fXCezZ7mkdEk6GT9EW7PqgNm7DjcjZ1tqabv5eR7ICgeQ7nT234faL4fy4itcJV0X336Y8Mv3LleOT4WNay8diS
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "metCirceAsGrossInsectPerson": [
                "Boolean",
                ""
            ],
            "sippedWineWithCirce": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('metCirceAsGrossInsectPerson', bool, False, 'circe', 'metCirceAsGrossInsectPerson')
        self.addSSOProperty('sippedWineWithCirce', bool, False, 'circe', 'sippedWineWithCirce')
        ##########
        # kFlags
        ##########
        self.addKFlag('CORRUPTED_COVEN_CIRCE_CHIT_CHAT_COOLDOWN', int, 0, HGGKFlags.CORRUPTED_COVEN_CIRCE_CHIT_CHAT_COOLDOWN)
        self.addKFlag('CORRUPTED_COVEN_CIRCE_CHIT_CHAT_COUNT', int, 0, HGGKFlags.CORRUPTED_COVEN_CIRCE_CHIT_CHAT_COUNT)
        self.addKFlag('CORR_WITCH_COVEN', int, 0, HGGKFlags.CORR_WITCH_COVEN, bitflags={
            1: 'MET_CIRCE',
            2: 'TOLD_NAME',
            4: 'TALKED_MARAE_BLESS',
            8: 'TALKED_FERA_BLESS',
            16: 'TALKED_JEREMIAH_WEAPONS',
            32: 'TALKED_LAURENTIUS_INCIDENT',
            64: 'TALKED_MANOR_DULLAHAN',
            128: 'TALKED_CORRWITCH_HEX',
            256: 'TALKED_TELADRE_WIZARDS',
            512: 'TALKED_GARGOYLE',
            1024: 'TALKED_SANDWITCH_BLESS',
            2048: 'TALKED_KITSUNE_ENLIGHTEMENT',
            4096: 'TALKED_DOMINIKA_SWORD',
            8192: 'TALKED_NAMELESS_HORRORD3_DISCOVERED',
            16384: 'CAUGHT_ALCHEMY',
            32768: 'GOT_RING_OF_ETHEREAL_TEARING',
            65536: 'GOT_MAJOR_RING_OF_ACCURACY',
            131072: 'GOT_SPECTRE_RING',
            262144: 'BROUGHT_JEREMIAH_BACK',
            524288: 'HAD_SEX_ONCE',
            1048576: 'BROUGHT_DULLAHAN',
            2097152: 'RETURNED_DULLAHAN_POSTDREAM',
            4194304: 'TALKED_BEAUTIFUL_SWORD',
        })
