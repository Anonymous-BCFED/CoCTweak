from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class BrookeNPC(BaseNPC):
    ID = 'brooke'
    NAME = 'Brooke'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Brooke.as: public class Brooke extends TelAdreAbstractContent { @ ae7b0p1099Rk1Pu7Wmfw89y02qH4evc8ggnE9SteY9fplb1Qbn3eM88Kq4GodZLfUud8va53cWd0LPfWteqO3ucdjMfjW9pH
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('BROOKE_AFFECTION', int, 0, HGGKFlags.BROOKE_AFFECTION)
        self.addKFlag('BROOKE_AND_HECKEL_3SOME', int, 0, HGGKFlags.BROOKE_AND_HECKEL_3SOME)
        self.addKFlag('BROOKE_GRUMPS_ABOUT_TA', int, 0, HGGKFlags.BROOKE_GRUMPS_ABOUT_TA)
        self.addKFlag('BROOKE_MEDIUM_SCENE', int, 0, HGGKFlags.BROOKE_MEDIUM_SCENE)
        self.addKFlag('BROOKE_MET', int, 0, HGGKFlags.BROOKE_MET)
        self.addKFlag('BROOKE_MET_TODAY', int, 0, HGGKFlags.BROOKE_MET_TODAY)
        self.addKFlag('BROOKE_SHOWERED_WITH', int, 0, HGGKFlags.BROOKE_SHOWERED_WITH)
        self.addKFlag('TIMES_IN_BROOKES_BUTT', int, 0, HGGKFlags.TIMES_IN_BROOKES_BUTT)
