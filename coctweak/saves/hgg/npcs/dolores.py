from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class DoloresNPC(BaseNPC):
    ID = 'dolores'
    NAME = 'Dolores'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/MothCave/DoloresScene.as: public class DoloresScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 3gndv0a1QgEradrfvkg5H4xFe0B1NX0vigbegz1dO558w6uhdHj9aGed77Am8Tl9cZ9lT0ivc2f7iF4GO0wB83B4K4dLo049
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "doloresProgress": [
                "IntList",
                "Tracks Dolores's overall progress",
                [
                    {
                        "label": "Unborn",
                        "data": 0
                    },
                    {
                        "label": "Born",
                        "data": 1
                    },
                    {
                        "label": "Post-birth",
                        "data": 2
                    },
                    {
                        "label": "Toys",
                        "data": 3
                    },
                    {
                        "label": "Talking",
                        "data": 4
                    },
                    {
                        "label": "Magic",
                        "data": 5
                    },
                    {
                        "label": "Ran Away",
                        "data": 6
                    },
                    {
                        "label": "Cocooned",
                        "data": 7
                    },
                    {
                        "label": "In Cocoon",
                        "data": 8
                    },
                    {
                        "label": "Hatched",
                        "data": 9
                    },
                    {
                        "label": "Wings",
                        "data": 10
                    },
                    {
                        "label": "Concerns",
                        "data": 11
                    },
                    {
                        "label": "Summoning",
                        "data": 12
                    },
                    {
                        "label": "Debriefed",
                        "data": 13
                    },
                    {
                        "label": "Sketched",
                        "data": 14
                    },
                    {
                        "label": "Tapestry",
                        "data": 15
                    },
                    {
                        "label": "HikkiQuest",
                        "data": 16
                    },
                    {
                        "label": "HikkiDone",
                        "data": 17
                    }
                ]
            ],
            "doloresTimeSinceEvent": [
                "Int",
                "Tracks how many hours it's been since an event has fired"
            ],
            "doloresDecision": [
                "IntList",
                "Tracks whether you let her keep the book",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Kept",
                        "data": 1
                    },
                    {
                        "label": "Taken",
                        "data": 2
                    },
                    {
                        "label": "Waiting",
                        "data": 3
                    }
                ]
            ],
            "doloresAmbitions": [
                "IntList",
                "Tracks some progress that's parallel to the main one",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Magic Talk",
                        "data": 1
                    },
                    {
                        "label": "Ambitions",
                        "data": 2
                    }
                ]
            ],
            "doloresFinal": [
                "Int",
                "Tracks results of her final scene"
            ],
            "hikkiQuest": [
                "BitFlag",
                [
                    "Tracks progress on her personal quest"
                ],
                [
                    "Camp",
                    "Marielle",
                    "Bazaar",
                    "Library",
                    "Circe",
                    "Sex",
                    "Finished",
                    "Bought Robe",
                    "Freed Her",
                    "Solved Riddle",
                    "Killed Demon"
                ]
            ],
            "doloresSex": [
                "IntList",
                "Tracks whether you've unlocked sex and/or comforted her",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Unlocked",
                        "data": 1
                    },
                    {
                        "label": "Comforted",
                        "data": 2
                    },
                    {
                        "label": "Uncomforted",
                        "data": 3
                    },
                    {
                        "label": "Comforted Sex",
                        "data": 4
                    }
                ]
            ],
            "doloresTimesLeft": [
                "Int",
                "Tracks how many times you've been a deadbeat [dad]"
            ],
            "doloresAngry": [
                "Boolean",
                "Tracks whether you've upset her somehow, disabling her for the current encounter"
            ],
            "doloresBlowjob": [
                "Boolean",
                "Tracks whether you've been blown"
            ],
            "doloresBooks": [
                "Boolean",
                "Tracks whether you've brought her books from the manor"
            ]
        }
        '''
        # tracks some progress that's parallel to the main one
        self.addSSOProperty('doloresAmbitions', int, 0, 'dolores', 'doloresAmbitions', notes="Tracks some progress that's parallel to the main one", choices={
            0: 'N/A',
            1: 'Magic Talk',
            2: 'Ambitions',
        })
        # tracks whether you've upset her
        self.addSSOProperty('doloresAngry', bool, False, 'dolores', 'doloresAngry', notes="Tracks whether you've upset her somehow, disabling her for the current encounter")
        # tracks whether you've been blown
        self.addSSOProperty('doloresBlowjob', bool, False, 'dolores', 'doloresBlowjob', notes="Tracks whether you've been blown")
        # tracks whether you've brought her books from the manor
        self.addSSOProperty('doloresBooks', bool, False, 'dolores', 'doloresBooks', notes="Tracks whether you've brought her books from the manor")
        # tracks whether you let her keep the book
        self.addSSOProperty('doloresDecision', int, 0, 'dolores', 'doloresDecision', notes='Tracks whether you let her keep the book', choices={
            0: 'N/A',
            1: 'Kept',
            2: 'Taken',
            3: 'Waiting',
        })
        # tracks results of her final scene
        self.addSSOProperty('doloresFinal', int, 0, 'dolores', 'doloresFinal', notes='Tracks results of her final scene')
        # 1 = alive, 2-5 = childhood events, 6-8 = cocoon events, 9+ = teen, 16 = hikkiquest, 17 = done (for now)
        self.addSSOProperty('doloresProgress', int, 0, 'dolores', 'doloresProgress', notes="Tracks Dolores's overall progress", choices={
            0: 'Unborn',
            1: 'Born',
            2: 'Post-birth',
            3: 'Toys',
            4: 'Talking',
            5: 'Magic',
            6: 'Ran Away',
            7: 'Cocooned',
            8: 'In Cocoon',
            9: 'Hatched',
            10: 'Wings',
            11: 'Concerns',
            12: 'Summoning',
            13: 'Debriefed',
            14: 'Sketched',
            15: 'Tapestry',
            16: 'HikkiQuest',
            17: 'HikkiDone',
        })
        # tracks whether you've unlocked sex and/or comforted her
        self.addSSOProperty('doloresSex', int, 0, 'dolores', 'doloresSex', notes="Tracks whether you've unlocked sex and/or comforted her", choices={
            0: 'N/A',
            1: 'Unlocked',
            2: 'Comforted',
            3: 'Uncomforted',
            4: 'Comforted Sex',
        })
        # tracks how many hours it's been since an event has fired
        self.addSSOProperty('doloresTimeSinceEvent', int, 0, 'dolores', 'doloresTimeSinceEvent', notes="Tracks how many hours it's been since an event has fired")
        # tracks how many times you've been a deadbeat [dad]
        self.addSSOProperty('doloresTimesLeft', int, 0, 'dolores', 'doloresTimesLeft', notes="Tracks how many times you've been a deadbeat [dad]")
        # tracks progress on her personal questsomehow
        self.addSSOProperty('hikkiQuest', int, 0, 'dolores', 'hikkiQuest', notes='Tracks progress on her personal quest')
