from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class SlipperySqueezeNPC(BaseNPC):
    ID = 'slipperysqueeze'
    NAME = 'SlipperySqueeze'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/SlipperySqueeze.as: public class SlipperySqueeze extends BazaarAbstractContent { @ gWygSZfHH5Am4MM33YarV2NKg5p2w67ej0DN3NL3X2dAK41E50e4dKd9cdiJ848doy8rI6vCfSfdjifvP5kZ6944IY7d90m6
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('BAZAAR_SLIPPERY_SQUEEZE_VISITED', int, 0, HGGKFlags.BAZAAR_SLIPPERY_SQUEEZE_VISITED, intbool=True)
        self.addKFlag('JOEY_BIG_BALLS_COUNTER', int, 0, HGGKFlags.JOEY_BIG_BALLS_COUNTER)
        self.addKFlag('JOEY_OFFERED_MILKER', int, 0, HGGKFlags.JOEY_OFFERED_MILKER, intbool=True)
        self.addKFlag('JOEY_TOLD_TO_MASTURBATE_COUNTER', int, 0, HGGKFlags.JOEY_TOLD_TO_MASTURBATE_COUNTER)
