from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class VolcanicGolemNPC(BaseNPC):
    ID = 'volcanicgolem'
    NAME = 'VolcanicGolem'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/VolcanicCrag/VolcanicGolem.as: public class VolcanicGolem extends Monster { @ gxyati0wZ9Tv8e3e4p6Ywe5w9THfEi1gbe0T42G4gT4Bn8lD3i5bjs4Ns802cBZ74Idzv2oQbyDcbEg5U2J05pm7D93Lb7Fc
        ## from [HGG]/classes/classes/Scenes/Areas/VolcanicCrag/VolcanicGolemScene.as: public class VolcanicGolemScene extends BaseContent { @ foI2tC546fSc7NJffmctL3dub7U47R52lflu9AWdvkbnI4Fk5XXf3h6fT0fh1L96sw8iPb7b1bXeBh5I73vjffm2791iHasX
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('DESTROYEDVOLCANICGOLEM', int, 0, HGGKFlags.DESTROYEDVOLCANICGOLEM)
        self.addKFlag('METVOLCANICGOLEM', int, 0, HGGKFlags.METVOLCANICGOLEM)
        self.addKFlag('VOLCANICGOLEMHP', int, 0, HGGKFlags.VOLCANICGOLEMHP)
        self.addKFlag('VOLCANICGOLEMSHIELDHP', int, 0, HGGKFlags.VOLCANICGOLEMSHIELDHP)
