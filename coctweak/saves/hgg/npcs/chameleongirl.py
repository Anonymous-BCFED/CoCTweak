from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class ChameleonGirlNPC(BaseNPC):
    ID = 'chameleongirl'
    NAME = 'ChameleonGirl'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/ChameleonGirl.as: public class ChameleonGirl extends Monster { @ 5sEdjV5Asabb8GEa0N6SW9PB3p5g5kcY5dSb3sD2E18sv5Wf14C3XN7eT0Mb3HCcBGcODc4tfxvea27W1fmIdfw5nOfNtgFg
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/ChameleonGirlScene.as: public class ChameleonGirlScene extends BaseContent { @ fxp1Hug1McvNa4t5O81IqaG66YJaQP9ui2myeDTc6o6WlcvecIdflQd0W9WMd8B5fga2913757ucjEgrO9Led6x8Lm0eQcaR
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('TIMES_MET_CHAMELEON', int, 0, HGGKFlags.TIMES_MET_CHAMELEON)
