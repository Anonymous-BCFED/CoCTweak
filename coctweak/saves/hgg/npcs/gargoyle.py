from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.common.npcmodules.renamable import RenamableNPC
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class GargoyleNPC(BaseNPC, RenamableNPC):
    ID = 'gargoyle'
    NAME = 'Gargoyle'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Explore/Gargoyle.as: public class Gargoyle extends BaseContent { @ cfM1I95V46fCdAG6U90pqffvb1s6DsfWJfPt59FceOd7Pf4R5wu0Og1iR7Px6p4ebh8av4Ta5Z66RW1ZI5eqgNhenn6OL7Tm
        super().__init__(save)
        self.setupRenamable(default='Gargoyle', flag=HGGKFlags.GAR_NAME)
        ##########
        # kFlags
        ##########
        self.addKFlag('FOUND_CATHEDRAL', int, 0, HGGKFlags.FOUND_CATHEDRAL)
        self.addKFlag('GAR_CATHEDRAL', int, 0, HGGKFlags.GAR_CATHEDRAL)
        self.addKFlag('GAR_CONFIDENCE', int, 0, HGGKFlags.GAR_CONFIDENCE)
        self.addKFlag('GAR_HISTORY', int, 0, HGGKFlags.GAR_HISTORY)
        self.addKFlag('GAR_LOVER_CHOICE', int, 0, HGGKFlags.GAR_LOVER_CHOICE)
        self.addKFlag('GAR_LOVER_TALKS', int, 0, HGGKFlags.GAR_LOVER_TALKS)
        self.addKFlag('GAR_NAME', str, '', HGGKFlags.GAR_NAME)
        self.addKFlag('GAR_NAME_TALKS', int, 0, HGGKFlags.GAR_NAME_TALKS)
        self.addKFlag('KINKY_RITUALS_SPOKEN', int, 0, HGGKFlags.KINKY_RITUALS_SPOKEN)
        self.addKFlag('LEWDED_THE_GARGOYLE', int, 0, HGGKFlags.LEWDED_THE_GARGOYLE)
        self.addKFlag('RITUAL_INTRO', int, 0, HGGKFlags.RITUAL_INTRO)
