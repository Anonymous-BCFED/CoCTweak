from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class SexMachineNPC(BaseNPC):
    ID = 'sexmachine'
    NAME = 'SexMachine'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/SexMachine.as: public class SexMachine extends TelAdreAbstractContent { @ 1rvclraB028HePr3Dz7yoaDg8DX1KodUkfdj6C4fWG8nM7DUc6O6JUazrayT3Y76sP0Yd3ng0lD7kt3Fh1ejdnQ3aZd5S84l
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('DISABLED_SEX_MACHINE', int, 0, HGGKFlags.DISABLED_SEX_MACHINE)
        self.addKFlag('GYM_MACHINE_STATUS', int, 0, HGGKFlags.GYM_MACHINE_STATUS, notes='Gym sex machine status.', choices={
            0: 'Unmet',
            1: "You've seen it",
            2: "You've used it",
        })
        self.addKFlag('TIMES_USED_SEX_MACHINE_AS_EUNUCH', int, 0, HGGKFlags.TIMES_USED_SEX_MACHINE_AS_EUNUCH)
        self.addKFlag('TIMES_USED_SEX_MACHINE_AS_FEMALE', int, 0, HGGKFlags.TIMES_USED_SEX_MACHINE_AS_FEMALE)
        self.addKFlag('TIMES_USED_SEX_MACHINE_AS_HERM', int, 0, HGGKFlags.TIMES_USED_SEX_MACHINE_AS_HERM)
        self.addKFlag('TIMES_USED_SEX_MACHINE_AS_MALE', int, 0, HGGKFlags.TIMES_USED_SEX_MACHINE_AS_MALE)
