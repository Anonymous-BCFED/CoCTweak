from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.common.npcmodules.renamable import RenamableNPC
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class CampNPC(BaseNPC, RenamableNPC):
    ID = 'camp'
    NAME = 'Camp'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Camp.as: public class Camp extends NPCAwareContent implements SelfSaving, SelfDebug { @ aTq6IR2rr5Zhbsz7DYbbN3yf0TadI50i51z69VI3Wx9WQ1v9gkV8YifPR6NYd3KaPc5tKem4e1C43cfnE7Bn40egpF8ng3UO
        super().__init__(save)
        self.setupRenamable(default='Training Dummy', ssoname='camp', ssoprop='dummyName')
        #############
        # SSO Props
        #############
        '''
        {
            "skyDamage": [
                "Int",
                "Amount of damage done to the sky"
            ],
            "dummyBuilt": [
                "Boolean",
                "Is the dummy built?"
            ],
            "dummyGender": [
                "IntList",
                "The dummy's gender",
                [
                    {
                        "label": "None",
                        "data": 0
                    },
                    {
                        "label": "Male",
                        "data": 1
                    },
                    {
                        "label": "Female",
                        "data": 2
                    },
                    {
                        "label": "Herm",
                        "data": 3
                    }
                ]
            ],
            "dummyName": [
                "String",
                ""
            ],
            "storageMoveAll": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('dummyBuilt', bool, False, 'camp', 'dummyBuilt', notes='Is the dummy built?')
        self.addSSOProperty('dummyGender', int, 0, 'camp', 'dummyGender', notes="The dummy's gender", choices={
            0: 'None',
            1: 'Male',
            2: 'Female',
            3: 'Herm',
        })
        self.addSSOProperty('dummyName', str, '""', 'camp', 'dummyName')
        self.addSSOProperty('skyDamage', int, 0, 'camp', 'skyDamage', notes='Amount of damage done to the sky')
        self.addSSOProperty('storageMoveAll', bool, False, 'camp', 'storageMoveAll')
        ##########
        # kFlags
        ##########
        self.addKFlag('CAMP_BUILT_CABIN', int, 0, HGGKFlags.CAMP_BUILT_CABIN)
        self.addKFlag('CAMP_CABIN_FURNITURE_BED', int, 0, HGGKFlags.CAMP_CABIN_FURNITURE_BED)
        self.addKFlag('CAMP_CABIN_FURNITURE_BOOKSHELF', int, 0, HGGKFlags.CAMP_CABIN_FURNITURE_BOOKSHELF)
        self.addKFlag('CAMP_CABIN_FURNITURE_CHAIR1', int, 0, HGGKFlags.CAMP_CABIN_FURNITURE_CHAIR1)
        self.addKFlag('CAMP_CABIN_FURNITURE_CHAIR2', int, 0, HGGKFlags.CAMP_CABIN_FURNITURE_CHAIR2)
        self.addKFlag('CAMP_CABIN_FURNITURE_DESK', int, 0, HGGKFlags.CAMP_CABIN_FURNITURE_DESK)
        self.addKFlag('CAMP_CABIN_FURNITURE_DESKCHAIR', int, 0, HGGKFlags.CAMP_CABIN_FURNITURE_DESKCHAIR)
        self.addKFlag('CAMP_CABIN_FURNITURE_DRESSER', int, 0, HGGKFlags.CAMP_CABIN_FURNITURE_DRESSER)
        self.addKFlag('CAMP_CABIN_FURNITURE_NIGHTSTAND', int, 0, HGGKFlags.CAMP_CABIN_FURNITURE_NIGHTSTAND)
        self.addKFlag('CAMP_CABIN_FURNITURE_TABLE', int, 0, HGGKFlags.CAMP_CABIN_FURNITURE_TABLE)
        self.addKFlag('CAMP_CABIN_PROGRESS', int, 0, HGGKFlags.CAMP_CABIN_PROGRESS)
        self.addKFlag('CAMP_CABIN_STONE_RESOURCES', int, 0, HGGKFlags.CAMP_CABIN_STONE_RESOURCES)
        self.addKFlag('CAMP_CABIN_WOOD_RESOURCES', int, 0, HGGKFlags.CAMP_CABIN_WOOD_RESOURCES)
        self.addKFlag('CAMP_PORTAL_PROGRESS', int, 0, HGGKFlags.CAMP_PORTAL_PROGRESS)
        self.addKFlag('CAMP_WALL_GATE', int, 0, HGGKFlags.CAMP_WALL_GATE)
        self.addKFlag('CAMP_WALL_PROGRESS', int, 0, HGGKFlags.CAMP_WALL_PROGRESS)
        self.addKFlag('CAMP_WALL_SKULLS', int, 0, HGGKFlags.CAMP_WALL_SKULLS)
        self.addKFlag('CAMP_WALL_STATUES', int, 0, HGGKFlags.CAMP_WALL_STATUES)
