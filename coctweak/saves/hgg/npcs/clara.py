from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class ClaraNPC(BaseNPC):
    ID = 'clara'
    NAME = 'Clara'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/Clara.as: public class Clara extends Monster { @ 6dA0Coc1c8tafUC1bdaoFcW9cuE2Th83m6lw6Z09TF6BPbF5aOG41Xa6EgNn9FVadecNM3zX19mcFNcvVg5961h01gcbQ1kg
        ## from [HGG]/classes/classes/Scenes/NPCs/MarblePurification.as: public class MarblePurification extends NPCAwareContent { @ fG73MlgWC640bjog4J25ah1W4Ws9X16gY1zrbIq92dflb3At1PadQO1Lm4YQfWP6ShgHnfoTebHeYa7Qg2hZ3A5cEBfbCeIz
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('CLARA_IMPRISONED', int, 0, HGGKFlags.CLARA_IMPRISONED)
        self.addKFlag('CLARA_PURIFIED', int, 0, HGGKFlags.CLARA_PURIFIED)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('ClaraCombatRounds', bool, False, HGGStatusLib.ClaraCombatRounds, None)
        self.addSFXProperty('ClaraFoughtInCamp', bool, False, HGGStatusLib.ClaraFoughtInCamp, None)
