from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class IzmaNPC(BaseNPC):
    ID = 'izma'
    NAME = 'Izma'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/IzmaScene.as: public class IzmaScene extends NPCAwareContent implements TimeAwareInterface, SelfSaving, SelfDebug, Encounter { @ 2wHgfH02J98EbqabtEaob9lWgrpeWHewOeXU0mR81BcHBayM9MB4rGcxF97Ig3veWb8p949N5GkbUtgHT45QeL62sy6wUfyv
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "sharkgirlsDeflowered": [
                "Int",
                ""
            ],
            "tigersharksDeflowered": [
                "Int",
                ""
            ],
            "daysSinceAneFight": [
                "Int",
                ""
            ],
            "tonguedButt": [
                "Boolean",
                ""
            ],
            "kidDick": [
                "Boolean",
                ""
            ],
            "lezDemonstration": [
                "Boolean",
                ""
            ],
            "sparred": [
                "Boolean",
                ""
            ],
            "previousVictoryTeased": [
                "Boolean",
                ""
            ],
            "lastNightmare": [
                "Number",
                ""
            ],
            "izmaMorning": [
                "Boolean",
                ""
            ],
            "gaveBooks": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('daysSinceAneFight', int, 0, 'izma', 'daysSinceAneFight')
        self.addSSOProperty('gaveBooks', bool, False, 'izma', 'gaveBooks')
        self.addSSOProperty('izmaMorning', bool, False, 'izma', 'izmaMorning')
        self.addSSOProperty('kidDick', bool, False, 'izma', 'kidDick')
        self.addSSOProperty('lastNightmare', float, 0, 'izma', 'lastNightmare')
        self.addSSOProperty('lezDemonstration', bool, False, 'izma', 'lezDemonstration')
        self.addSSOProperty('previousVictoryTeased', bool, False, 'izma', 'previousVictoryTeased')
        self.addSSOProperty('sharkgirlsDeflowered', int, 0, 'izma', 'sharkgirlsDeflowered')
        self.addSSOProperty('sparred', bool, False, 'izma', 'sparred')
        self.addSSOProperty('tigersharksDeflowered', int, 0, 'izma', 'tigersharksDeflowered')
        self.addSSOProperty('tonguedButt', bool, False, 'izma', 'tonguedButt')
        ##########
        # kFlags
        ##########
        self.addKFlag('ASKED_IZMA_ABOUT_WANG_REMOVAL', int, 0, HGGKFlags.ASKED_IZMA_ABOUT_WANG_REMOVAL)
        self.addKFlag('FOLLOWER_AT_FARM_IZMA', int, 0, HGGKFlags.FOLLOWER_AT_FARM_IZMA)
        self.addKFlag('IZMA_AMILY_FREAKOUT_STATUS', int, 0, HGGKFlags.IZMA_AMILY_FREAKOUT_STATUS)
        self.addKFlag('IZMA_BROFIED', int, 0, HGGKFlags.IZMA_BROFIED)
        self.addKFlag('IZMA_CHILDREN_SHARKGIRLS', int, 0, HGGKFlags.IZMA_CHILDREN_SHARKGIRLS)
        self.addKFlag('IZMA_CHILDREN_TIGERSHARKS', int, 0, HGGKFlags.IZMA_CHILDREN_TIGERSHARKS)
        self.addKFlag('IZMA_ENCOUNTER_COUNTER', int, 0, HGGKFlags.IZMA_ENCOUNTER_COUNTER)
        self.addKFlag('IZMA_FEEDING_VALERIA', int, 0, HGGKFlags.IZMA_FEEDING_VALERIA)
        self.addKFlag('IZMA_FOLLOWER_STATUS', int, 0, HGGKFlags.IZMA_FOLLOWER_STATUS)
        self.addKFlag('IZMA_GLOVES_TAKEN', int, 0, HGGKFlags.IZMA_GLOVES_TAKEN)
        # PregnancyStore vag incubation counter
        self.addKFlag('IZMA_INCUBATION', int, 0, HGGKFlags.IZMA_INCUBATION)
        self.addKFlag('IZMA_KIDS_IN_WILD', int, 0, HGGKFlags.IZMA_KIDS_IN_WILD)
        self.addKFlag('IZMA_MARBLE_FREAKOUT_STATUS', int, 0, HGGKFlags.IZMA_MARBLE_FREAKOUT_STATUS)
        self.addKFlag('IZMA_NO_COCK', int, 0, HGGKFlags.IZMA_NO_COCK)
        self.addKFlag('IZMA_PREGNANCY_DISCUSSED', int, 0, HGGKFlags.IZMA_PREGNANCY_DISCUSSED)
        self.addKFlag('IZMA_PREGNANCY_ENABLED', int, 0, HGGKFlags.IZMA_PREGNANCY_ENABLED)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('IZMA_PREGNANCY_TYPE', int, 0, HGGKFlags.IZMA_PREGNANCY_TYPE)
        self.addKFlag('IZMA_TALKED_AT_LAKE', int, 0, HGGKFlags.IZMA_TALKED_AT_LAKE)
        self.addKFlag('IZMA_TALK_LEVEL', int, 0, HGGKFlags.IZMA_TALK_LEVEL)
        self.addKFlag('IZMA_TIGERSHARK_TOOTH_COUNTDOWN', int, 0, HGGKFlags.IZMA_TIGERSHARK_TOOTH_COUNTDOWN)
        self.addKFlag('IZMA_TIMES_FOUGHT_AND_WON', int, 0, HGGKFlags.IZMA_TIMES_FOUGHT_AND_WON)
        self.addKFlag('IZMA_TIME_TILL_NEW_BOOK_AVAILABLE', int, 0, HGGKFlags.IZMA_TIME_TILL_NEW_BOOK_AVAILABLE)
        self.addKFlag('IZMA_WORMS_SCARED', int, 0, HGGKFlags.IZMA_WORMS_SCARED)
        self.addKFlag('IZMA_X_LATEXY_DISABLED', int, 0, HGGKFlags.IZMA_X_LATEXY_DISABLED)
        self.addKFlag('TIMES_IZMA_DOMMED_LATEXY', int, 0, HGGKFlags.TIMES_IZMA_DOMMED_LATEXY)
        self.addKFlag('TIMES_SOPHIE_AND_IZMA_FUCKED', int, 0, HGGKFlags.TIMES_SOPHIE_AND_IZMA_FUCKED)
        self.addKFlag('TOLD_SOPHIE_TO_IZMA', int, 0, HGGKFlags.TOLD_SOPHIE_TO_IZMA)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('Goojob', bool, False, HGGStatusLib.Goojob, None)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.IZMA_PREGNANCY_TYPE, HGGKFlags.IZMA_INCUBATION)
