# This is a very complicated NPC.
# Do not fuck with this file unless you know what you're doing.
from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.breastcupsizes import HGGBreastCupSizes
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.breaststore import HGGBreastStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class KatherineNPC(BaseNPC):
    ID = 'katherine'
    ALIASES = ['kath']
    NAME = 'Katherine'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Katherine.as: public class Katherine extends TelAdreAbstractContent implements TimeAwareInterface { @ 6ly5U56RD7ru632e5m6kr5aZfzq8fodBp2dZ9IA355fPT4FM1TMd2t5ptarkfyYgwP6lx0D26YlcsI5vpcvrekpbmc2qa3Ud
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/KatherineEmployment.as: public class KatherineEmployment extends TelAdreAbstractContent { @ ele7MOgrCdix9Ugf6seur9k2amA0l2gRG6ejazogjP733gPdaWV0nqaujc532gz2OxaKLd5ecVwfU9dTs0Mm0xk4FW1fXcFX
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/KatherineThreesome.as: public class KatherineThreesome extends TelAdreAbstractContent { @ ctc9D6bQb3jI9WxdqX5MffDra2zcDdfhF2qL1KVeHr9oo5lE7bn9AG52wbeUgFz2Du3x27Nn5566XH6bB4HM5Fv03w86i051
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('KATHERINE_AMILY_AFFECTION', int, 0, HGGKFlags.KATHERINE_AMILY_AFFECTION)
        self.addKFlag('KATHERINE_BALL_SIZE', int, 0, HGGKFlags.KATHERINE_BALL_SIZE)
        # BreastStore data
        self.addKFlag('KATHERINE_BREAST_SIZE', str, '1^1^2^0^0^0^0^0^0', HGGKFlags.KATHERINE_BREAST_SIZE, readonly=True, notes='Stored breast data. See `npc ... katherine breasts --help` for ways to change this.')
        self.addKFlag('KATHERINE_CLOTHES', int, 0, HGGKFlags.KATHERINE_CLOTHES, notes='Clothing Kath currently owns', bitflags={
            0: 'TATTERED',
            1: 'UNIFORM',
            2: 'C_CLOTH',
            4: 'BODYSUIT',
            8: 'B_DRESS',
            16: 'SS_ROBE',
            32: 'TUBETOP',
            64: 'NURSECL',
        })
        self.addKFlag('KATHERINE_CLOTHES_PREF', int, 0, HGGKFlags.KATHERINE_CLOTHES_PREF, notes='Clothing player prefers.  Negative = forced by player.', choices={
            0: 'TATTERED',
            1: 'UNIFORM',
            2: 'C_CLOTH',
            4: 'BODYSUIT',
            8: 'B_DRESS',
            16: 'SS_ROBE',
            32: 'TUBETOP',
            64: 'NURSECL',
        })
        self.addKFlag('KATHERINE_CLOTHES_WORN', int, 0, HGGKFlags.KATHERINE_CLOTHES_WORN, notes='Clothing Kath is wearing right now.', choices={
            0: 'TATTERED',
            1: 'UNIFORM',
            2: 'C_CLOTH',
            4: 'BODYSUIT',
            8: 'B_DRESS',
            16: 'SS_ROBE',
            32: 'TUBETOP',
            64: 'NURSECL',
        })
        self.addKFlag('KATHERINE_COTTON_AFFECTION', int, 0, HGGKFlags.KATHERINE_COTTON_AFFECTION)
        self.addKFlag('KATHERINE_DICK_COUNT', int, 0, HGGKFlags.KATHERINE_DICK_COUNT)
        self.addKFlag('KATHERINE_DICK_FORM', int, 0, HGGKFlags.KATHERINE_DICK_FORM)
        self.addKFlag('KATHERINE_DICK_LENGTH', int, 0, HGGKFlags.KATHERINE_DICK_LENGTH)
        self.addKFlag('KATHERINE_EDRYN_AFFECTION', int, 0, HGGKFlags.KATHERINE_EDRYN_AFFECTION)
        self.addKFlag('KATHERINE_HAIR_COLOR', str, '', HGGKFlags.KATHERINE_HAIR_COLOR)
        self.addKFlag('KATHERINE_HELIA_AFFECTION', int, 0, HGGKFlags.KATHERINE_HELIA_AFFECTION)
        self.addKFlag('KATHERINE_HOURS_SINCE_CUM', int, 0, HGGKFlags.KATHERINE_HOURS_SINCE_CUM)
        self.addKFlag('KATHERINE_IS_CAT_GIRL', int, 0, HGGKFlags.KATHERINE_IS_CAT_GIRL)
        self.addKFlag('KATHERINE_KNOT_THICKNESS', int, 0, HGGKFlags.KATHERINE_KNOT_THICKNESS)
        self.addKFlag('KATHERINE_LOCATION', int, 0, HGGKFlags.KATHERINE_LOCATION, notes='Where is the kitty right now?', choices={
            0: 'STREETS',
            1: 'KATHS_APT',
            2: 'URTAS_APT',
            3: 'URTAS_HOME',
            4: 'BAR',
            5: 'BAR_DRUNK',
            6: 'BAR_URTA_REFUSED',
            7: 'LAKE',
            8: 'DESERT',
        })
        self.addKFlag('KATHERINE_MET_SCYLLA', int, 0, HGGKFlags.KATHERINE_MET_SCYLLA)
        self.addKFlag('KATHERINE_MILK_OPTIONS', int, 0, HGGKFlags.KATHERINE_MILK_OPTIONS, notes='Milk-related flags.', bitflags={
            1: 'TOLD_PLAYER_SHES_DRY',
            2: 'SHARE_WITH_FRIENDS',
            4: 'SHARE_WITH_OLD_GANG',
            8: 'SHARE_WITH_HELENA',
            16: 'SHARE_WITH_URTA',
            32: 'SHARE_WITH_VALA',
        })
        self.addKFlag('KATHERINE_RANDOM_RECRUITMENT_DISABLED', int, 0, HGGKFlags.KATHERINE_RANDOM_RECRUITMENT_DISABLED)
        self.addKFlag('KATHERINE_SEXUAL_EXPERIENCE', int, 0, HGGKFlags.KATHERINE_SEXUAL_EXPERIENCE, notes='Sexual experience flags.', bitflags={
            1: 'RECEIVE_ANAL',
            2: 'RECEIVE_SUCK_N_FUCK',
            4: 'RECEIVE_ORAL',
            8: 'GIVE_VAGINAL',
            16: 'GIVE_ANAL',
            32: 'GIVE_DOUBLE_PEN',
            64: 'GIVE_SUCK_N_FUCK',
            128: 'DOUBLE_HELIX',
            256: 'BATH',
            512: 'BEDROOM_BONDAGE',
            1024: 'DRUNK_BAR_FUCK',
        })
        self.addKFlag('KATHERINE_SUB_FLAGS', int, 0, HGGKFlags.KATHERINE_SUB_FLAGS, notes='Sub experience flags.', bitflags={
            1: 'HIGH_CUM',
            2: 'BED_BOND',
            4: 'ORGASM_DENIAL',
            8: 'CALL_MASTER',
            16: 'GROW_KNOT',
            32: 'CAT_DICK',
            64: 'GROW_BIG_BOOBS',
            128: 'GROW_BIG_DICK',
            256: 'CAT_GIRL',
            512: 'HIGH_LACTATION',
            1024: 'REM_BALLS',
            2048: 'GROW_BIG_BALLS',
            4096: 'PUBLIC_EXHIBITION',
            8192: 'REM_COCK',
            16384: 'START_LACTATION',
        })
        self.addKFlag('KATHERINE_TIMES_SEXED', int, 0, HGGKFlags.KATHERINE_TIMES_SEXED)
        self.addKFlag('KATHERINE_TRAINING', int, 0, HGGKFlags.KATHERINE_TRAINING)
        self.addKFlag('KATHERINE_UNLOCKED', int, 0, HGGKFlags.KATHERINE_UNLOCKED)
        self.addKFlag('KATHERINE_URTA_AFFECTION', int, 0, HGGKFlags.KATHERINE_URTA_AFFECTION)
        self.addKFlag('KATHERINE_URTA_DATE', int, 0, HGGKFlags.KATHERINE_URTA_DATE, notes='Urta dating urgency.', choices={
            0: 'LITTLE',
            1: 'WHENEVER',
            2: 'LOTS',
        })
        self.addKFlag('KATHERINE_URTA_TIMES_SEX', int, 0, HGGKFlags.KATHERINE_URTA_TIMES_SEX)
        self.addKFlag('KATHERINE_VALA_AFFECTION', int, 0, HGGKFlags.KATHERINE_VALA_AFFECTION)
        self.addKFlag('KATHERINE_VALA_DATE', int, 0, HGGKFlags.KATHERINE_VALA_DATE, notes='Vala dating urgency.', choices={
            0: 'LITTLE',
            1: 'WHENEVER',
            2: 'LOTS',
        })
        self.addKFlag('KATHERINE_VALA_TIMES_SEX', int, 0, HGGKFlags.KATHERINE_VALA_TIMES_SEX)
        ################
        # BreastStores
        ################
        self.breasts = HGGBreastStore(save, False, HGGKFlags.KATHERINE_BREAST_SIZE)

    def show(self, properties) -> None:
        super().show(properties)
        with log.info('Breasts:'):
            self.breasts.display()
            log.info(self.breastAppearance())

    def breastAppearance(self) -> str:
        cream = ', laden with cream,' if self.breasts.milkIsFull() else ''
        suffix = ". Her nipples stand at attention, ready for milking" if self.breasts.milkIsOverflowing() else ''
        return f"Two {self.breasts.adj()} {self.breasts.cup()} breasts{cream} sit on her chest{suffix}."

    def reset_breasts(self) -> bool:
        chunks = '1^1^2^0^0^0^0^0^0'.split('^')
        self.breasts.rows = int(chunks[1])
        self.breasts.cupSize = int(chunks[2])
        self.breasts.lactationLevel = int(chunks[3])
        self.breasts.nippleLength = float(chunks[4])
        self.breasts.fullness = int(chunks[5])
        self.breasts.timesMilked = int(chunks[6])
        self.breasts.preventLactationIncrease = int(chunks[7])
        self.breasts.preventLactationDecrease = int(chunks[8])
        return True