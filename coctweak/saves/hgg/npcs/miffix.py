from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class MiffixNPC(BaseNPC):
    ID = 'miffix'
    NAME = 'Miffix'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/MiffixScene.as: public class MiffixScene extends BaseContent implements SelfSaving, TimeAwareInterface { @ 5DveVq6OA8AE7Wlg4Weh20jXg93g89gqHeiu3ft7tQcnf8LU83bgmbbLBa3T6sBc0AcKog0y2qC6O9doua9obj22Mm1h92ln
        super().__init__(save)
        #############
        # SSO Props
        #############
        self.addSSOProperty('beatDemonfist', None, False, 'miffix', 'beatDemonfist', unused=True, notes='Someone did some crappy copypasta.')
        self.addSSOProperty('donatedToMiffix', int, 0, 'miffix', 'donatedToMiffix')
        self.addSSOProperty('killedMiffix', bool, False, 'miffix', 'killedMiffix')
        self.addSSOProperty('learnedOfDemonFist', None, False, 'miffix', 'learnedOfDemonFist', unused=True, notes='Someone did some crappy copypasta.')
        self.addSSOProperty('metMiffixPost', bool, False, 'miffix', 'metMiffixPost')
        self.addSSOProperty('playerName', None, "", 'miffix', 'playerName', unused=True, notes='Someone did some crappy copypasta.')
        self.addSSOProperty('timeUntilLegsBroken', int, 0, 'miffix', 'timeUntilLegsBroken')
        self.addSSOProperty('timeUntilPlan', int, 0, 'miffix', 'timeUntilPlan')
        self.addSSOProperty('timesLost', None, 0, 'miffix', 'timesLost', unused=True, notes='Someone did some crappy copypasta.')
        self.addSSOProperty('toldPlan', bool, False, 'miffix', 'toldPlan')
