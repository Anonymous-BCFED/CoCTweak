from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class MaraeNPC(BaseNPC):
    ID = 'marae'
    NAME = 'Marae'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Boat/Marae.as: public class Marae extends Monster { @ fBy4EC1dI3gpetZezQ5JrdYqbgk8yf11kfJD0PYf6Z8WrgbV5J4c0vgbyayb0RL5vKccX3QR7R13KZ5YN0S88zP4UV5Wh2Rk
        ## from [HGG]/classes/classes/Scenes/Places/Boat/MaraeScene.as: public class MaraeScene extends AbstractBoatContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 1gFbPxbWYdLXgrGf6wa1Y2iyfvg5tnbrW1bOcd09LOfVJbYXaVU8jq8FgcbQ3I3dsMcbn7a5djE6280Ip3jX1sM0R5gQ60rl
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "feraMet": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addSSOProperty('feraMet', bool, False, 'marae', 'feraMet')
        ##########
        # kFlags
        ##########
        self.addKFlag('CORRUPTED_MARAE_KILLED', int, 0, HGGKFlags.CORRUPTED_MARAE_KILLED)
        self.addKFlag('CORRUPT_MARAE_FOLLOWUP_ENCOUNTER_STATE', int, 0, HGGKFlags.CORRUPT_MARAE_FOLLOWUP_ENCOUNTER_STATE)
        self.addKFlag('FUCK_FLOWER_GROWTH_COUNTER', int, 0, HGGKFlags.FUCK_FLOWER_GROWTH_COUNTER)
        self.addKFlag('FUCK_FLOWER_KILLED', int, 0, HGGKFlags.FUCK_FLOWER_KILLED)
        self.addKFlag('FUCK_FLOWER_LEVEL', int, 0, HGGKFlags.FUCK_FLOWER_LEVEL)
        self.addKFlag('MARAE_QUEST_COMPLETE', int, 0, HGGKFlags.MARAE_QUEST_COMPLETE)
        self.addKFlag('MARAE_QUEST_START', int, 0, HGGKFlags.MARAE_QUEST_START)
        self.addKFlag('MET_MARAE', int, 0, HGGKFlags.MET_MARAE)
        self.addKFlag('MET_MARAE_CORRUPTED', int, 0, HGGKFlags.MET_MARAE_CORRUPTED)
        self.addKFlag('PURE_MARAE_ENDGAME', int, 0, HGGKFlags.PURE_MARAE_ENDGAME)
