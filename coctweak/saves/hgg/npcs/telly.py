from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class TellyNPC(BaseNPC):
    ID = 'telly'
    NAME = 'Telly'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/Telly.as: public class Telly extends BazaarAbstractContent implements SelfSaving, SelfDebug { @ 1rt4K10OF6DPbHd5flbJb0yxe6FeXi4i2c83a8Y91g2o33xUd88aT64No3wIf3ZgcKdrh33T3vL61Zdplg265x56Ef1J5cJb
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "debugSortOrder": [
                "tellyGenesis",
                "noncommiTelly",
                "tellyCommute",
                "tellyTubby",
                "tellyGraph",
                "tellyCardiogram",
                "tellyTimer",
                "tellyGram",
                "tellyCommand",
                "tellyPlasmed",
                "vesTelly",
                "tasTelly",
                "experimenTelly",
                "tellyComL",
                "tellyComB",
                "tellyComK",
                "tellyComP",
                "tellyComA",
                "tellyComH",
                "tellyComD",
                "tellyOphile"
            ],
            "tellyGenesis": [
                "Boolean",
                "Met Telly in the bazaar"
            ],
            "noncommiTelly": [
                "Boolean",
                "Seen Telly's wagon while exploring"
            ],
            "tellyCommute": [
                "Boolean",
                "Met Telly while exploring"
            ],
            "tellyTubby": [
                "String",
                "Facepaint color"
            ],
            "tellyGraph": [
                "String",
                "Facepaint type"
            ],
            "tellyCardiogram": [
                "Int",
                "Tracks what Telly is doing in the shop"
            ],
            "tellyTimer": [
                "Int",
                "Time of last random rolls"
            ],
            "tellyGram": [
                "Int",
                "Number of times chatted per visit"
            ],
            "tellyCommand": [
                "Int",
                "Tracks the day that Telly is disabled for"
            ],
            "tellyPlasmed": [
                "Boolean",
                "Tracks if you've bought candy"
            ],
            "vesTelly": [
                "Int",
                "Tracks if you've hugged Telly this hour"
            ],
            "tasTelly": [
                "Int",
                "Tracks this stupid fucking peach"
            ],
            "experimenTelly": [
                "Int",
                "Tracks Drake's Heart stuff"
            ],
            "tellyComL": [
                "Boolean",
                "Asked about selling Liddellium"
            ],
            "tellyComB": [
                "Boolean",
                "Gave bear"
            ],
            "tellyComK": [
                "Boolean",
                "Kitsuned fluffy tail"
            ],
            "tellyComP": [
                "Boolean",
                "Gave peach"
            ],
            "tellyComA": [
                "Boolean",
                "Gave abyssal shard"
            ],
            "tellyComH": [
                "Boolean",
                "Unlocked hugging"
            ],
            "tellyComD": [
                "Boolean",
                "Gave Drake's Heart"
            ],
            "tellyOphile": [
                "Object",
                "Tracks which chats you've seen",
                {
                    "debugSortOrder": [
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6",
                        "7",
                        "8",
                        "9",
                        "10",
                        "11",
                        "12",
                        "13",
                        "14",
                        "15",
                        "16",
                        "17",
                        "18",
                        "19",
                        "20",
                        "21",
                        "22",
                        "23"
                    ],
                    "1": [
                        "boolean",
                        ""
                    ],
                    "2": [
                        "boolean",
                        ""
                    ],
                    "3": [
                        "boolean",
                        ""
                    ],
                    "4": [
                        "boolean",
                        ""
                    ],
                    "5": [
                        "boolean",
                        ""
                    ],
                    "6": [
                        "boolean",
                        ""
                    ],
                    "7": [
                        "boolean",
                        ""
                    ],
                    "8": [
                        "boolean",
                        ""
                    ],
                    "9": [
                        "boolean",
                        ""
                    ],
                    "10": [
                        "boolean",
                        ""
                    ],
                    "11": [
                        "boolean",
                        ""
                    ],
                    "12": [
                        "boolean",
                        ""
                    ],
                    "13": [
                        "boolean",
                        ""
                    ],
                    "14": [
                        "boolean",
                        ""
                    ],
                    "15": [
                        "boolean",
                        ""
                    ],
                    "16": [
                        "boolean",
                        ""
                    ],
                    "17": [
                        "boolean",
                        ""
                    ],
                    "18": [
                        "boolean",
                        ""
                    ],
                    "19": [
                        "boolean",
                        ""
                    ],
                    "20": [
                        "boolean",
                        ""
                    ],
                    "21": [
                        "boolean",
                        ""
                    ],
                    "22": [
                        "boolean",
                        ""
                    ],
                    "23": [
                        "boolean",
                        ""
                    ]
                }
            ]
        }
        '''
        self.addSSOProperty('experimenTelly', int, 0, '', 'experimenTelly', notes="Tracks Drake's Heart stuff")
        self.addSSOProperty('tasTelly', int, 0, '', 'tasTelly', notes='Tracks this stupid fucking peach')
        self.addSSOProperty('tellyCardiogram', int, 0, '', 'tellyCardiogram', notes='Tracks what Telly is doing in the shop')
        self.addSSOProperty('tellyComA', bool, False, '', 'tellyComA', notes='Gave abyssal shard')
        self.addSSOProperty('tellyComB', bool, False, '', 'tellyComB', notes='Gave bear')
        self.addSSOProperty('tellyComD', bool, False, '', 'tellyComD', notes="Gave Drake's Heart")
        self.addSSOProperty('tellyComH', bool, False, '', 'tellyComH', notes='Unlocked hugging')
        self.addSSOProperty('tellyComK', bool, False, '', 'tellyComK', notes='Kitsuned fluffy tail')
        self.addSSOProperty('tellyComL', bool, False, '', 'tellyComL', notes='Asked about selling Liddellium')
        self.addSSOProperty('tellyComP', bool, False, '', 'tellyComP', notes='Gave peach')
        self.addSSOProperty('tellyCommand', int, 0, '', 'tellyCommand', notes='Tracks if Telly is disabled for the day (by setting it to the current day)')
        self.addSSOProperty('tellyGenesis', bool, False, '', 'tellyGenesis', notes='met Telly')
        self.addSSOProperty('tellyGram', int, 0, '', 'tellyGram', notes='Number of times chatted per visit')
        self.addSSOProperty('tellyGraph', str, 'butterfly', '', 'tellyGraph', notes='Facepaint type')
        self.addSSOProperty('tellyOphile', dict, {1: False, 2: False, 3: False, 4: False, 5: False, 6: False, 7: False, 8: False, 9: False, 10: False, 11: False, 12: False, 13: False, 14: False, 15: False, 16: False, 17: False, 18: False, 19: False, 20: False, 21: False, 22: False}, '', 'tellyOphile', readonly=True, notes="Chats you've had with her")
        self.addSSOProperty('tellyPlasmed', bool, False, '', 'tellyPlasmed', notes="Tracks if you've bought candy")
        self.addSSOProperty('tellyTimer', int, 0, '', 'tellyTimer', notes='Time of last random rolls')
        self.addSSOProperty('tellyTubby', str, 'purple', '', 'tellyTubby', notes='Facepaint color')
        self.addSSOProperty('vesTelly', int, 0, '', 'vesTelly', notes="Tracks if you've hugged Telly this hour")
