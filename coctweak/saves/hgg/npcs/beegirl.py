from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from typing import Dict
from enum import IntEnum
#from construct import BitStruct, Enum, BitsInteger, Int16ub, Bit, Bitwise, Container
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

import argparse


class BeeGirlConversations(IntEnum):
    NONE = 0
    MAIDEN_1 = 1
    MAIDEN_2 = 2
    MAIDEN_3 = 3
    MAIDEN_4 = 4

class BeeGirlAttitude(IntEnum):
    NONE = 0
    BEE_GIRL_TALKED = 1 # Replaces the old bee progress flag
    BEE_GIRL_TALKED_AND_LEFT = 2 # Refusing to take her eggs leads to conversation
    BEE_GIRL_TALKED_AND_LEFT_TWICE = 3
    BEE_GIRL_PLAYER_AFRAID = 4
    BEE_GIRL_PLAYER_VOLUNTARY_EGGING = 5 # End of the afraid chain, from now on player gets egged when they meet her
    BEE_GIRL_PLAYER_DISGUSTED = 6
    BEE_GIRL_PLAYER_DUTY = 7

# BeeGirlStatus = BitStruct(
#     'bad_end_warning' / Bit,
#     'conversation' / Enum(BitsInteger(15), BeeGirlConversations), #0x7FFF0000
#     'attitude' / Enum(BitsInteger(16), BeeGirlAttitude) #0x0000FFFF
# )

class BeeGirlNPC(BaseNPC):
    ID = 'beegirl'
    NAME = 'BeeGirl'

    ## from [HGG]/classes/classes/Scenes/Areas/Forest/BeeGirlScene.as: private static const BEE_GIRL_CONVERSATION:int = 0x7FFF0000; //Used to track conversations when the player is a female bee morph (Highest bit is for the bad end warning) @ 1qxejp2BzduHauI7Gl9bPejbgma0xiamZ0zT9i5ek6dVgcqG4AS9Ikf2K3HYgaNc4n7QK9sg9SHchRdiP5wQaoo3du4H9ctU
    BEE_GIRL_CONVERSATION: int = 0x7FFF0000
    ## from [HGG]/classes/classes/Scenes/Areas/Forest/BeeGirlScene.as: private static const BEE_GIRL_ATTITUDE:int = 0x0000FFFF; @ cWE3E7ey3grX05Z64bfrNgJk5KM3eE1Jx03BaxG0gW0MC35j1PEdlL7yj1sP00T4yw5ZPgEIgag1nT1NSdj90731HR0a3790
    BEE_GIRL_ATTITUDE: int = 0x0000FFFF
    ## from [HGG]/classes/classes/Scenes/Areas/Forest/BeeGirlScene.as: private function get badEndWarning():Boolean { @ 6Ig1EHa200RFgSS2Sv8mKaxidE6eFO3Yq32lgyScDr5bDg4bevv3H402R0EAfTpcxkgy63xG3kragv3pVgTo8X8eMsdWYbFR
    BEE_GIRL_BITFLAG_BAD_END_WARNING: int = 0x80000000

    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/BeeGirlScene.as: public class BeeGirlScene extends BaseContent implements SelfSaving, SelfDebug { @ cRZ9Rk0ktbBe3kJc9Xed44HF16U2z0793c6gfOJfNMcxdaWd2KOeNW4N31ug03U8T5gAMc1f8fv3Nr4yee33aib2M90WD9yf
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "timesEgged": [
                "Int",
                ""
            ]
        }
        '''
        self.addSSOProperty('timesEgged', int, 0, 'beegirl', 'timesEgged')
        ##########
        # kFlags
        ##########
        self.addKFlag('BEE_GIRL_COMBAT_LOSSES', int, 0, HGGKFlags.BEE_GIRL_COMBAT_LOSSES)
        self.addKFlag('BEE_GIRL_COMBAT_WINS_WITHOUT_RAPE', int, 0, HGGKFlags.BEE_GIRL_COMBAT_WINS_WITHOUT_RAPE)
        self.addKFlag('BEE_GIRL_COMBAT_WINS_WITH_RAPE', int, 0, HGGKFlags.BEE_GIRL_COMBAT_WINS_WITH_RAPE)
        self.addKFlag('BEE_GIRL_STATUS', int, 0, HGGKFlags.BEE_GIRL_STATUS, readonly=True, notes='See npc ... beegirl --help for ways to change this.')

        #self._parsedBeeGirlStatus: Container = None
        
    # def afterLoad(self) -> None:
    #     self._parsedBeeGirlStatus = BeeGirlStatus.parse(self.BEE_GIRL_STATUS.to_bytes(4, 'big'))
    # def beforeSave(self) -> None:
    #     data = BeeGirlStatus.build(self._parsedBeeGirlStatus)
    #     value = int.from_bytes(data, 'big', signed=True)
    #     self.BEE_GIRL_STATUS = value

    ## from [HGG]/classes/classes/Scenes/Areas/Forest/BeeGirlScene.as: private function get attitude():int { @ 2Y19gQ92sfvMf1Egcf2hv6cldza7cr42c3qm20x9y41879Bv9qLese7W44AH8W476H273bT3gXo2MG9uD7sd1BDbh08KZaxp
    @property
    def attitude(self) -> int:
        return (self.BEE_GIRL_STATUS & self.BEE_GIRL_ATTITUDE)
        #return self._parsedBeeGirlStatus.attitude.value
    ## from [HGG]/classes/classes/Scenes/Areas/Forest/BeeGirlScene.as: private function set attitude(value:int):void { @ 7191P9bzd1K54W0ari3ZI0gtaSX2Fj0yceSC5hyao19gr65x1077qi9wgcs02Yd2GWdw24x9cwo1kq8ymaWH8sSesp5cheui
    @attitude.setter
    def set_attitude(self, value: int) -> None:
        self.BEE_GIRL_STATUS = (self.BEE_GIRL_STATUS & self.BEE_GIRL_ATTITUDE) | value
        #self._parsedBeeGirlStatus.attitude = value

    ## from [HGG]/classes/classes/Scenes/Areas/Forest/BeeGirlScene.as: private function get badEndWarning():Boolean { @ 6Ig1EHa200RFgSS2Sv8mKaxidE6eFO3Yq32lgyScDr5bDg4bevv3H402R0EAfTpcxkgy63xG3kragv3pVgTo8X8eMsdWYbFR
    @property
    def bad_end_warning(self) -> bool:
        # return (flags[kFLAGS.BEE_GIRL_STATUS] & 0x80000000) != 0;
        return (self.BEE_GIRL_STATUS & self.BEE_GIRL_BITFLAG_BAD_END_WARNING) == self.BEE_GIRL_BITFLAG_BAD_END_WARNING
        #return self._parsedBeeGirlStatus.bad_end_warning == 1
    ## from [HGG]/classes/classes/Scenes/Areas/Forest/BeeGirlScene.as: private function set badEndWarning(value:Boolean):void { @ bkye0h7pZ0wz8fK4Wfajq4Bm8xC3QhfEk29L6Le5xbdFD3Gd8bw5Kw284eVwghQ9Rb1ef7GH7rqb3Vbs04lRbc00oO9n4e5h
    @bad_end_warning.setter
    def set_bad_end_warning(self, value: bool) -> None:
        # Original code:
        #   flags[kFLAGS.BEE_GIRL_STATUS] = (flags[kFLAGS.BEE_GIRL_STATUS] & (BEE_GIRL_ATTITUDE | BEE_GIRL_CONVERSATION)) + (value ? 0x80000000 : 0);
        # OR we could just not be insane.
        if value:
           self.BEE_GIRL_STATUS |= self.BEE_GIRL_BITFLAG_BAD_END_WARNING
        else:
           self.BEE_GIRL_STATUS &= ~self.BEE_GIRL_BITFLAG_BAD_END_WARNING
        #self._parsedBeeGirlStatus.bad_end_warning = 1 if value else 0

    ## from [HGG]/classes/classes/Scenes/Areas/Forest/BeeGirlScene.as: private function get conversation():int { @ gaC6INe9D0nqcpugSb4qVf1UeE48C0b7MgMNeY4fzB8PT9pafQQdaucSTeM662Y2C99WB7g99Ke8dpbPCdng6Bp9y76sL9DB
    @property
    def conversation(self) -> int:
        return (self.BEE_GIRL_STATUS & self.BEE_GIRL_CONVERSATION) >> 16
        #return self._parsedBeeGirlStatus.conversation.value
    ## from [HGG]/classes/classes/Scenes/Areas/Forest/BeeGirlScene.as: private function set conversation(value:int):void { @ 9IF9kw4S44trfwEcfHcoq3l256k1MQ6cBfpX2jFe0j18Gecz8o01jO14l0fl7eyf9Q6Kv9Tz7jweWv0HwgkW6ow24x5OG0dJ
    @conversation.setter
    def set_conversation(self, value: int) -> None:
        self.BEE_GIRL_STATUS = (self.BEE_GIRL_STATUS & self.BEE_GIRL_CONVERSATION) | (value << 16)
        #self._parsedBeeGirlStatus.conversation = value

    def register_cli_attitude(self, subp: argparse.ArgumentParser) -> None:
        p_attitude = subp.add_parser('attitude', help='View or Change the bits of BEE_GIRL_STATUS relating to attitude.')
        p_attitude.add_argument('value', type=str, default=None, nargs='?', help='Value to change attitude to.')
        p_attitude.set_defaults(npc_cmd=self.cmd_attitude)

    def cmd_attitude(self, args: argparse.Namespace):
        value = self.BEE_GIRL_STATUS & self.BEE_GIRL_ATTITUDE
        if args.value is None:
            with log.info('Current value:'):
                attitude_flag_name: str
                try:
                    attitude_flag_name = BeeGirlAttitude(value).name
                except:
                    attitude_flag_name = '[UNKNOWN/INVALID]'
                log.info(f'{value} (0x{value:08X}) - {attitude_flag_name}')
            with log.info('Possible values:'):
                for attitude in BeeGirlAttitude:
                    log.info(f'- {attitude.name} ({attitude.value})')
            return False
        else:
            with log.info('Old value:'):
                attitude_flag_name: str
                try:
                    attitude_flag_name = BeeGirlAttitude(value).name
                except:
                    attitude_flag_name = '[UNKNOWN/INVALID]'
                log.info(f'{value} (0x{value:08X}) - {attitude_flag_name}')
            try:
                value = int(args.value)
            except ValueError:
                value = BeeGirlAttitude[args.value.upper()].value
            self.BEE_GIRL_STATUS = (self.BEE_GIRL_STATUS & self.BEE_GIRL_ATTITUDE) | value
            value = self.BEE_GIRL_STATUS & self.BEE_GIRL_ATTITUDE
            with log.info('New value:'):
                attitude_flag_name: str
                try:
                    attitude_flag_name = BeeGirlAttitude(value).name
                except:
                    attitude_flag_name = '[UNKNOWN/INVALID]'
                log.info(f'{value} (0x{value:08X}) - {attitude_flag_name}')
            return True

    def register_cli_conversation(self, subp: argparse.ArgumentParser) -> None:
        p_conversation = subp.add_parser('conversation', help='View or Change the bits of BEE_GIRL_STATUS relating to conversation.')
        p_conversation.add_argument('value', type=str, default=None, nargs='?', help='Value to change conversation to.')
        p_conversation.set_defaults(npc_cmd=self.cmd_conversation)

    def cmd_conversation(self, args: argparse.Namespace):
        value = int(self._parsedBeeGirlStatus.conversation)
        if args.value is None:
            with log.info('Current value:'):
                conversation_name: str
                try:
                    conversation_name = BeeGirlConversations(value).name
                except:
                    conversation_name = '[UNKNOWN/INVALID]'
                log.info(f'{value} (0x{value:08X}) - {conversation_name}')
            with log.info('Possible values:'):
                for attitude in BeeGirlConversations:
                    log.info(f'- {attitude.name} ({attitude.value})')
            return False
        else:
            with log.info('Old value:'):
                conversation_name: str
                try:
                    conversation_name = BeeGirlConversations(value).name
                except:
                    conversation_name = '[UNKNOWN/INVALID]'
                log.info(f'{value} (0x{value:08X}) - {conversation_name}')
            try:
                value = int(args.value)
            except ValueError:
                value = BeeGirlConversations[args.value.upper()].value
            self._parsedBeeGirlStatus.conversation = value
            value = int(self._parsedBeeGirlStatus.conversation)
            with log.info('New value:'):
                conversation_name: str
                try:
                    conversation_name = BeeGirlConversations(value).name
                except:
                    conversation_name = '[UNKNOWN/INVALID]'
                log.info(f'{value} (0x{value:08X}) - {conversation_name}')
            return True


    def show(self, properties):
        super().show(properties)
        with log.info('BEE_GIRL_STATUS:'):
            log.info(f'Decimal:  {self.BEE_GIRL_STATUS:d}')
            log.info(f'Hex:    0x{self.BEE_GIRL_STATUS:08x}')
            log.info(f'Binary: 0b{self.BEE_GIRL_STATUS:032b}')
            # status = BeeGirlStatus.parse(self.BEE_GIRL_STATUS.to_bytes(4, 'big'))
            # log.info(f'struct: {status}')
            with log.info(f'& BEE_GIRL_ATTITUDE (0x{self.BEE_GIRL_ATTITUDE:08x})'):
                masked = self.BEE_GIRL_STATUS & self.BEE_GIRL_ATTITUDE
                name: str
                try:
                    name = BeeGirlAttitude(masked).name
                except:
                    name = '???'
                log.info(f'Name:      {name}')
                log.info(f'Decimal:   {masked}')
                log.info(f'Hex:     0x{masked:04x}')
                log.info(f'Binary:  0b{masked:016b}')
            with log.info(f'& BEE_GIRL_CONVERSATION (0x{self.BEE_GIRL_CONVERSATION:08x})'):
                masked = (self.BEE_GIRL_STATUS & self.BEE_GIRL_CONVERSATION)
                shifted = masked >> 16
                name: str
                try:
                    name = BeeGirlConversations(shifted).name
                except:
                    name = '???'
                log.info(f'Name:      {name}')
                log.info(f'Decimal:   {masked: <10}                      (shifted: {shifted})')
                log.info(f'Hex:     0x{masked:08x}                        (shifted: 0x{shifted:04x})')
                log.info(f'Binary:  0b{masked:031b} (shifted: 0b{shifted:015b})')
            with log.info(f'& BAD END WARNING BITFLAG (0x{self.BEE_GIRL_BITFLAG_BAD_END_WARNING:08x})'):
                masked = (self.BEE_GIRL_STATUS & self.BEE_GIRL_BITFLAG_BAD_END_WARNING)
                shifted = masked >> 31
                #log.info(f'Decimal:   2147483648                       (shifted: 1)')
                #log.info(f'Hex:     0x80000000                         (shifted: 0x1)')
                #log.info(f'Binary:  0b10000000000000000000000000000000 (shifted: 0b1)')
                log.info(f'Decimal:   {masked: <10}                       (shifted: {shifted})')
                log.info(f'Hex:     0x{masked:08x}                         (shifted: 0x{shifted:01x})')
                log.info(f'Binary:  0b{masked:032b} (shifted: 0b{shifted:01b})')
