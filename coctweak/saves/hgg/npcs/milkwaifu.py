from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCSelfSaveProperty
from coctweak.saves.common.npcmodules.renamable import RenamableNPC
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class MilkWaifuNPC(BaseNPC, RenamableNPC):
    ID = 'milkwaifu'
    NAME = 'Milky'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/MilkWaifu.as: public class MilkWaifu extends NPCAwareContent implements SelfSaving, SelfDebug { @ 6rOgRz0aBbw09PCc3C7Lf1d3biB06B8tR8IXgYCfj99JK1Sn9QF1LP8CW82e7oYebL4KL0VOgk2bI4bxh1Oe6KP0skf4F6gw
        super().__init__(save)
        self.setupRenamable(default='Milky', flag=HGGKFlags.MILK_NAME)
        #############
        # SSO Props
        #############
        '''
        {
            "talkedSlut": [
                "IntList",
                "",
                [
                    {
                        "label": "0",
                        "data": 0
                    },
                    {
                        "label": "1",
                        "data": 1
                    },
                    {
                        "label": "2",
                        "data": 2
                    },
                    {
                        "label": "3",
                        "data": 3
                    }
                ]
            ]
        }
        '''
        self.addSSOProperty('talkedSlut', int, 0, 'milkWaifu', 'talkedSlut', choices={
            0: '0',
            1: '1',
            2: '2',
            3: '3',
        })
        ##########
        # kFlags
        ##########
        self.addKFlag('FOLLOWER_AT_FARM_BATH_GIRL', int, 0, HGGKFlags.FOLLOWER_AT_FARM_BATH_GIRL)
        self.addKFlag('MILKY_TATTOO_BUTT', int, 0, HGGKFlags.MILKY_TATTOO_BUTT)
        self.addKFlag('MILKY_TATTOO_COLLARBONE', int, 0, HGGKFlags.MILKY_TATTOO_COLLARBONE)
        self.addKFlag('MILKY_TATTOO_LOWERBACK', int, 0, HGGKFlags.MILKY_TATTOO_LOWERBACK)
        self.addKFlag('MILKY_TATTOO_SHOULDERS', int, 0, HGGKFlags.MILKY_TATTOO_SHOULDERS)
        self.addKFlag('MILK_NAME', int, 0, HGGKFlags.MILK_NAME)
        self.addKFlag('MILK_SIZE', int, 0, HGGKFlags.MILK_SIZE)
