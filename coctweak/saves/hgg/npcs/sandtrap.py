from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class SandTrapNPC(BaseNPC):
    ID = 'sandtrap'
    NAME = 'SandTrap'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Desert/SandTrapScene.as: public class SandTrapScene extends BaseContent { @ cUs9X12ao71Z0DI4hKcJ3fiob8kabD3tggPU8Oq4jgbXJ2DM67a0mueMd9yudEa8OncpJ47k4NM1TD7Mh09mfRRb8Ff6k2gh
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('SANDTRAP_LOSS_REPEATS', int, 0, HGGKFlags.SANDTRAP_LOSS_REPEATS)
        self.addKFlag('SANDTRAP_NAGA_3SOME', int, 0, HGGKFlags.SANDTRAP_NAGA_3SOME)
        self.addKFlag('TIMES_ENCOUNTERED_SAND_TRAPS', int, 0, HGGKFlags.TIMES_ENCOUNTERED_SAND_TRAPS)
