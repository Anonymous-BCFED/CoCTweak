from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class MarielleNPC(BaseNPC):
    ID = 'marielle'
    NAME = 'Marielle'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/Marielle.as: public class Marielle extends BaseContent implements SelfSaving, SelfDebug { @ c4oajk4w47wD88Z1rm6Pxg1ee4405P5i1ddPcFW5aH9NefUyacK0do81P30heSIdsW1tCbfK0TJ4oc4qo2O258PeBgfuEdMB
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "state": [
                "IntList",
                "Tracks overall state",
                [
                    {
                        "label": "Abandoned",
                        "data": -2
                    },
                    {
                        "label": "Dead",
                        "data": -1
                    },
                    {
                        "label": "Unmet",
                        "data": 0
                    },
                    {
                        "label": "Met",
                        "data": 1
                    }
                ]
            ],
            "extorted": [
                "IntList",
                "Tracks what you extorted out of her",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Gems",
                        "data": 1
                    },
                    {
                        "label": "Sword",
                        "data": 2
                    },
                    {
                        "label": "Sex",
                        "data": 3
                    },
                    {
                        "label": "Eyeball",
                        "data": 4
                    }
                ]
            ],
            "insulted": [
                "IntList",
                "Tracks if you've insulted her father",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Insulted",
                        "data": 1
                    },
                    {
                        "label": "Apologized",
                        "data": 2
                    },
                    {
                        "label": "Double Insult",
                        "data": 3
                    },
                    {
                        "label": "Tried Apology",
                        "data": 4
                    }
                ]
            ],
            "rose": [
                "IntList",
                "Tracks rose quest",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Started",
                        "data": 1
                    },
                    {
                        "label": "Gave Rose",
                        "data": 2
                    }
                ]
            ],
            "fainted": [
                "IntList",
                "Tracks whether she's fainted in the bath",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Fainted",
                        "data": 1
                    },
                    {
                        "label": "Alt Intro",
                        "data": 2
                    }
                ]
            ],
            "kissDenied": [
                "IntList",
                "Tracks whether/why she's declined to kiss you",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Dick",
                        "data": 1
                    },
                    {
                        "label": "Genderless",
                        "data": 2
                    },
                    {
                        "label": "Both",
                        "data": 3
                    }
                ]
            ],
            "taurDecision": [
                "IntList",
                "Tracks what you said to her about taur rides",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Normal",
                        "data": 1
                    },
                    {
                        "label": "Fetish",
                        "data": 2
                    },
                    {
                        "label": "Disabled",
                        "data": 3
                    }
                ]
            ],
            "handHeld": [
                "IntList",
                "Tracks what kinds of handholding you've done",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Normal",
                        "data": 1
                    },
                    {
                        "label": "Detached",
                        "data": 2
                    },
                    {
                        "label": "Both",
                        "data": 3
                    }
                ]
            ],
            "talks": [
                "BitFlag",
                [
                    "Tracks individual talk options"
                ],
                [
                    "Collapse",
                    "Undeath",
                    "Dressmaking",
                    "Cart",
                    "Old World",
                    "Her Likes",
                    "Family",
                    "Loneliness",
                    "Sex",
                    "Commando",
                    "Father",
                    "Background",
                    "Fondness",
                    "Her Death",
                    "His Death",
                    "Necromancy",
                    "Sex II",
                    "Her Death II"
                ]
            ],
            "itemsBought": [
                "BitFlag",
                [
                    "Tracks one-time shop items"
                ],
                [
                    "Long Dress",
                    "Cheer Outfit",
                    "Black Cloak"
                ]
            ],
            "modeled": [
                "BitFlag",
                [
                    "Tracks individual items modeled"
                ],
                [
                    "Long Dress",
                    "Maid Dress",
                    "Butler Suit",
                    "Kimono",
                    "China Dress",
                    "Turtleneck",
                    "Ballet Dress"
                ]
            ],
            "moneySpent": [
                "Int"
            ],
            "commissionCount": [
                "Int",
                "Tracks total number of commissions"
            ],
            "timesKissed": [
                "Int"
            ],
            "timesPatted": [
                "Int"
            ],
            "timesHandHeld": [
                "Int"
            ],
            "timesSexed": [
                "Int"
            ],
            "timesYurid": [
                "Int",
                "Tracks the number of times you've had sex with only a vagina"
            ],
            "timesDicked": [
                "Int"
            ],
            "timesInButt": [
                "Int"
            ],
            "visitTime": [
                "Int",
                "Tracks the time the visit started"
            ],
            "openDate": [
                "Int",
                "Tracks the day she opened her boutique"
            ],
            "reenabled": [
                "Int",
                "Tracks the time when she's able to be encountered again after being disabled"
            ],
            "commissionTime": [
                "Int",
                "Tracks the time when her commission will be done"
            ],
            "seenNaked": [
                "Boolean"
            ],
            "askedModeling": [
                "Boolean"
            ],
            "mockedDress": [
                "Boolean",
                "Tracks whether you've mocked her in the ballet dress"
            ],
            "askedCommission": [
                "Boolean"
            ],
            "footstuff": [
                "Boolean",
                "Tracks whether you've participated in any variety of footfaggotry"
            ],
            "bathed": [
                "Boolean"
            ],
            "breastfed": [
                "Boolean"
            ],
            "seenMothSilk": [
                "Boolean",
                "Tracks whether you've shown her Dolores' silk"
            ],
            "seenSpiderSilk": [
                "Boolean",
                "Tracks whether you've shown her spider silk"
            ],
            "commission": [
                "IntList",
                "Tracks what item is being commissioned",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Moth Dress",
                        "data": 1
                    },
                    {
                        "label": "Sylvia Dress",
                        "data": 2
                    },
                    {
                        "label": "Moth Panties",
                        "data": 4
                    },
                    {
                        "label": "Moth Bedding",
                        "data": 8
                    },
                    {
                        "label": "SS Dress",
                        "data": 3
                    },
                    {
                        "label": "SS Bra",
                        "data": 5
                    },
                    {
                        "label": "SS Panties",
                        "data": 6
                    },
                    {
                        "label": "SS Underwear",
                        "data": 7
                    },
                    {
                        "label": "SS Bedding",
                        "data": 9
                    }
                ]
            ],
            "lastBoot": [
                "Int",
                "Tracks which blurb was last used to kick you out for time"
            ],
            "lastSleep": [
                "Int"
            ]
        }
        '''
        self.addSSOProperty('askedCommission', bool, False, 'marielle', 'askedCommission')
        self.addSSOProperty('askedModeling', bool, False, 'marielle', 'askedModeling')
        self.addSSOProperty('bathed', bool, False, 'marielle', 'bathed')
        self.addSSOProperty('breastfed', bool, False, 'marielle', 'breastfed')
        # tracks item commissioned
        self.addSSOProperty('commission', int, 0, 'marielle', 'commission', notes='Tracks what item is being commissioned', choices={
            0: 'N/A',
            1: 'Moth Dress',
            2: 'Sylvia Dress',
            4: 'Moth Panties',
            8: 'Moth Bedding',
            3: 'SS Dress',
            5: 'SS Bra',
            6: 'SS Panties',
            7: 'SS Underwear',
            9: 'SS Bedding',
        })
        self.addSSOProperty('commissionCount', int, 0, 'marielle', 'commissionCount', notes='Tracks total number of commissions')
        # tracks time when her commission will be done
        self.addSSOProperty('commissionTime', int, 0, 'marielle', 'commissionTime', notes='Tracks the time when her commission will be done')
        # 0 = n/a; 1 = gems; 2 = sword; 3 = sex; 4 = eyeball
        self.addSSOProperty('extorted', int, 0, 'marielle', 'extorted', notes='Tracks what you extorted out of her', choices={
            0: 'N/A',
            1: 'Gems',
            2: 'Sword',
            3: 'Sex',
            4: 'Eyeball',
        })
        # 0 = n/a; 1 = fainted; 2 = intro seen
        self.addSSOProperty('fainted', int, 0, 'marielle', 'fainted', notes="Tracks whether she's fainted in the bath", choices={
            0: 'N/A',
            1: 'Fainted',
            2: 'Alt Intro',
        })
        self.addSSOProperty('footstuff', bool, False, 'marielle', 'footstuff', notes="Tracks whether you've participated in any variety of footfaggotry")
        # 0 = n/a; 1 = normal; 2 = detached; 3 = both
        self.addSSOProperty('handHeld', int, 0, 'marielle', 'handHeld', notes="Tracks what kinds of handholding you've done", choices={
            0: 'N/A',
            1: 'Normal',
            2: 'Detached',
            3: 'Both',
        })
        # 0 = n/a; 1 = insulted; 2 = apologized; 3 = double insulted; 4 = attempted apology
        self.addSSOProperty('insulted', int, 0, 'marielle', 'insulted', notes="Tracks if you've insulted her father", choices={
            0: 'N/A',
            1: 'Insulted',
            2: 'Apologized',
            3: 'Double Insult',
            4: 'Tried Apology',
        })
        # tracks one-time shop items
        self.addSSOProperty('itemsBought', int, 0, 'marielle', 'itemsBought', notes='Tracks one-time shop items')
        # 0 = n/a; 1 = denied for dick; 2 = denied for genderless; 3 = both
        self.addSSOProperty('kissDenied', int, 0, 'marielle', 'kissDenied', notes="Tracks whether/why she's declined to kiss you", choices={
            0: 'N/A',
            1: 'Dick',
            2: 'Genderless',
            3: 'Both',
        })
        # tracks which blurb was last used
        self.addSSOProperty('lastBoot', int, 0, 'marielle', 'lastBoot', notes='Tracks which blurb was last used to kick you out for time')
        # same as above
        self.addSSOProperty('lastSleep', int, 0, 'marielle', 'lastSleep')
        self.addSSOProperty('mockedDress', bool, False, 'marielle', 'mockedDress', notes="Tracks whether you've mocked her in the ballet dress")
        # tracks individual items modeled
        self.addSSOProperty('modeled', int, 0, 'marielle', 'modeled', notes='Tracks individual items modeled')
        self.addSSOProperty('moneySpent', int, 0, 'marielle', 'moneySpent')
        # tracks the second encounter by date
        self.addSSOProperty('openDate', int, 0, 'marielle', 'openDate', notes='Tracks the day she opened her boutique')
        # tracks day when she gets re-enabled
        self.addSSOProperty('reenabled', int, 0, 'marielle', 'reenabled', notes="Tracks the time when she's able to be encountered again after being disabled")
        # 0 = n/a; 1 = started; 2 = gave her rose
        self.addSSOProperty('rose', int, 0, 'marielle', 'rose', notes='Tracks rose quest', choices={
            0: 'N/A',
            1: 'Started',
            2: 'Gave Rose',
        })
        self.addSSOProperty('seenMothSilk', bool, False, 'marielle', 'seenMothSilk', notes="Tracks whether you've shown her Dolores' silk")
        self.addSSOProperty('seenNaked', bool, False, 'marielle', 'seenNaked')
        self.addSSOProperty('seenSpiderSilk', bool, False, 'marielle', 'seenSpiderSilk', notes="Tracks whether you've shown her spider silk")
        # -2 = abandoned; -1 = dead; 0 = unencountered; 1 = encounterable;
        self.addSSOProperty('state', int, 0, 'marielle', 'state', notes='Tracks overall state', choices={
            -2: 'Abandoned',
            -1: 'Dead',
            0: 'Unmet',
            1: 'Met',
        })
        # tracks individual talk options
        self.addSSOProperty('talks', int, 0, 'marielle', 'talks', notes='Tracks individual talk options')
        # 0 = n/a; 1 = normal; 2 = fetish; 3 = disable
        self.addSSOProperty('taurDecision', int, 0, 'marielle', 'taurDecision', notes='Tracks what you said to her about taur rides', choices={
            0: 'N/A',
            1: 'Normal',
            2: 'Fetish',
            3: 'Disabled',
        })
        self.addSSOProperty('timesDicked', int, 0, 'marielle', 'timesDicked')
        self.addSSOProperty('timesHandHeld', int, 0, 'marielle', 'timesHandHeld')
        self.addSSOProperty('timesInButt', int, 0, 'marielle', 'timesInButt')
        self.addSSOProperty('timesKissed', int, 0, 'marielle', 'timesKissed')
        self.addSSOProperty('timesPatted', int, 0, 'marielle', 'timesPatted')
        self.addSSOProperty('timesSexed', int, 0, 'marielle', 'timesSexed')
        self.addSSOProperty('timesYurid', int, 0, 'marielle', 'timesYurid', notes="Tracks the number of times you've had sex with only a vagina")
        # tracks how long you've been there in a given encounter
        self.addSSOProperty('visitTime', int, 0, 'marielle', 'visitTime', notes='Tracks the time the visit started')
