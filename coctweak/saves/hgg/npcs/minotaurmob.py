from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class MinotaurMobNPC(BaseNPC):
    ID = 'minotaurmob'
    NAME = 'MinotaurMob'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/HighMountains/MinotaurMobScene.as: public class MinotaurMobScene extends BaseContent implements TimeAwareInterface { @ cL31eS5tcdLObAF3UX5G11KddmB9tP12s2bo6ml2tW9BB2t25Be4Jg8SYguw64NddDeW8gdT2Pu5Zd0PQaFa5kh72GfYD5x0
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('ADULT_MINOTAUR_OFFSPRINGS', int, 0, HGGKFlags.ADULT_MINOTAUR_OFFSPRINGS)
        self.addKFlag('MINOTAUR_SONS_GROWUP_COUNTER', int, 0, HGGKFlags.MINOTAUR_SONS_GROWUP_COUNTER)
        self.addKFlag('MINOTAUR_SONS_PENDING', int, 0, HGGKFlags.MINOTAUR_SONS_PENDING)
        self.addKFlag('TIMES_ENCOUNTERED_MINOTAUR_MOB', int, 0, HGGKFlags.TIMES_ENCOUNTERED_MINOTAUR_MOB)
