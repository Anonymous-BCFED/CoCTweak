from coctweak.saves._npc import BaseNPC, NPCSelfSaveProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.gender import HGGGender
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class IvorySuccubusNPC(BaseNPC):
    ID = 'ivorysuccubus'
    NAME = 'Ivory Succubus'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Monsters/IvorySuccubus.as: public class IvorySuccubus extends Monster { @ cLD5VYfFJbNh7AlgdO5eU4kna4ddQq3Je7wX1fPce2d8ae2qgO1bbk6dX47HeIY5wB6SLagqfLH2k82zd2pPewH07Hd0TeWZ
        ## from [HGG]/classes/classes/Scenes/Monsters/IvorySuccubusScene.as: public class IvorySuccubusScene extends BaseContent implements SelfSaving, SelfDebug { @ eZZeTh5J26kc6rI0vTgwV5VzeBk0G76MLah5ak9ahIdFP1iOctt8pI0ZnbFLgQfd4r2I332Hcye42m1Io4Cs2n41iE7KMc9H
        super().__init__(save)
        #############
        # SSO Props
        #############
        '''
        {
            "met": [
                "Boolean",
                ""
            ],
            "deadHookers": [
                "Int",
                ""
            ],
            "timesLost": [
                "Int",
                ""
            ]
        }
        '''
        self.addSSOProperty('deadHookers', int, 0, 'ivory succubus', 'deadHookers', notes='How many succubi you have killed')
        self.addSSOProperty('met', bool, False, 'ivory succubus', 'met', notes='Have you met one of these creatures before?')
        self.addSSOProperty('timesLost', int, 0, 'ivory succubus', 'timesLost', notes='How many times you lost to a succubi')
