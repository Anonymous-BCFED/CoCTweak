from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.cocktypes import HGGCockTypes
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class LatexGirlNPC(BaseNPC):
    ID = 'latexgirl'
    NAME = 'LatexGirl'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/LatexGirl.as: public class LatexGirl extends NPCAwareContent { @ fP2a3Fa4a9Ry3OYccO8BO8ao6eaeyJ9ZLadT5Pr5guazS0mOb3ncG34CM1Jl5wt0ArePF1YhfUsa0J2g46re0V9gAA5jQefw
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('FOLLOWER_AT_FARM_LATEXY', int, 0, HGGKFlags.FOLLOWER_AT_FARM_LATEXY)
        self.addKFlag('GOO_DICK_LENGTH', int, 0, HGGKFlags.GOO_DICK_LENGTH)
        self.addKFlag('GOO_DICK_TYPE', int, HGGCockTypes(0), HGGKFlags.GOO_DICK_TYPE, enumType=HGGCockTypes)
        self.addKFlag('GOO_EYES', str, '', HGGKFlags.GOO_EYES)
        self.addKFlag('GOO_FLUID_AMOUNT', int, 0, HGGKFlags.GOO_FLUID_AMOUNT)
        self.addKFlag('GOO_HAPPINESS', int, 0, HGGKFlags.GOO_HAPPINESS)
        self.addKFlag('GOO_INDIRECT_FED', int, 0, HGGKFlags.GOO_INDIRECT_FED)
        self.addKFlag('GOO_NAME', str, '', HGGKFlags.GOO_NAME)
        self.addKFlag('GOO_NIPPLE_TYPE', int, 0, HGGKFlags.GOO_NIPPLE_TYPE)
        self.addKFlag('GOO_OBEDIENCE', int, 0, HGGKFlags.GOO_OBEDIENCE)
        self.addKFlag('GOO_PREFERRED_TIT_SIZE', int, 0, HGGKFlags.GOO_PREFERRED_TIT_SIZE)
        self.addKFlag('GOO_SLAVE_RECRUITED', int, 0, HGGKFlags.GOO_SLAVE_RECRUITED)
        self.addKFlag('GOO_TFED_MEAN', int, 0, HGGKFlags.GOO_TFED_MEAN)
        self.addKFlag('GOO_TFED_NICE', int, 0, HGGKFlags.GOO_TFED_NICE)
        self.addKFlag('GOO_TOSSED_AFTER_NAMING', int, 0, HGGKFlags.GOO_TOSSED_AFTER_NAMING)
        self.addKFlag('LATEX_GOO_TIMES_FEMDOMMED_BY_PC', int, 0, HGGKFlags.LATEX_GOO_TIMES_FEMDOMMED_BY_PC)
        self.addKFlag('TIMES_FED_LATEXY_MINO_CUM', int, 0, HGGKFlags.TIMES_FED_LATEXY_MINO_CUM)
