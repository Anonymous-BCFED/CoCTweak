from coctweak.saves._npc import BaseNPC, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class WandererNPC(BaseNPC):
    ID = 'wanderer'
    NAME = 'Wanderer'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Desert/Wanderer.as: public class Wanderer extends BaseContent { @ 6cJgRKfpi99cbL41rsdkE8DKeyO9VdcFTeA8bCf8du4gb0w3bsq6FCcjo28d8cy0jaaiB6YZ1SG0rK0uz4PTgx65IE2Hm1eC
        super().__init__(save)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('MeetWanderer', bool, False, HGGStatusLib.MeetWanderer, None)
        self.addSFXProperty('WandererDemon', bool, False, HGGStatusLib.WandererDemon, None)
        self.addSFXProperty('WandererHuman', bool, False, HGGStatusLib.WandererHuman, None)
