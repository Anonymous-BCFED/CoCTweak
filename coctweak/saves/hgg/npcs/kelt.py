from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class KeltNPC(BaseNPC):
    ID = 'kelt'
    NAME = 'Kelt'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/Farm/Kelt.as: public class Kelt extends Monster { @ 5iu7L5cLafwx2yj8F29QSdDA23528f4ZicrD4733p94rc7id77rczD1TG5sr1NTeUPdtS9zJ1JR7lW9pd8zLfCggho0IUbmq
        ## from [HGG]/classes/classes/Scenes/Places/Farm/KeltScene.as: public class KeltScene extends AbstractFarmContent { @ 5NMbPZgsD48j9ku21q3MQcxc1queQjfVCdrS6mDdLx2i59an6Qx0ct34cban23w61ba4UebYbJq6eEc8a9a97BZbbR3HT8KE
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('KELT_BREAK_LEVEL', int, 0, HGGKFlags.KELT_BREAK_LEVEL, min=0, max=4, notes='[0,4], higher being more broken.')
        self.addKFlag('KELT_KILLED', int, 0, HGGKFlags.KELT_KILLED, intbool=True)
        self.addKFlag('KELT_KILL_PLAN', int, 0, HGGKFlags.KELT_KILL_PLAN, intbool=True)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('BlowjobOn', bool, False, HGGStatusLib.BlowjobOn, None, notes='Kelt cum addict')
        self.addSFXProperty('Kelt:0', int, 0, HGGStatusLib.Kelt, 0, notes='Archery')
        self.addSFXProperty('Kelt:1', int, 0, HGGStatusLib.Kelt, 1, notes='Submissiveness')
        self.addSFXProperty('Kelt:2', int, 0, HGGStatusLib.Kelt, 2, notes='Encounter count')
        self.addSFXProperty('Kelt:3', None, 0, HGGStatusLib.Kelt, 3, unused=True)
        self.addSFXProperty('KeltBJ', bool, False, HGGStatusLib.KeltBJ, None, notes='Kelt requires a blowjob.')
        self.addSFXProperty('KeltBadEndWarning', bool, False, HGGStatusLib.KeltBadEndWarning, None)
        self.addSFXProperty('KeltOff', bool, False, HGGStatusLib.KeltOff, None, notes='Kelt disabled.')
        self.addSFXProperty('NakedOn', bool, False, HGGStatusLib.NakedOn, None, notes='Kelt requires you to be naked.')
