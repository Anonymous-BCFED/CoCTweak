from coctweak.saves._npc import BaseNPC, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class NagaNPC(BaseNPC):
    ID = 'naga'
    NAME = 'Naga'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Desert/Naga.as: public class Naga extends Monster { @ 0nkaca6ce3V84S8eBObQHaum4J2fJQ0R80h83uLgrl69pauo0IKbEq9RZ7cj70w9r1awq7otelObUF3Wl84D8006FyfhO4oc
        ## from [HGG]/classes/classes/Scenes/Areas/Desert/NagaScene.as: public class NagaScene extends BaseContent { @ 3PT9909jG3rI2XxclbcSN3vMbXN7TL8sV4mJ1mfdxT9BD68rfT2eVN8O32da0yz9at7GE4Xtd1ndJH7pI3zo6RV1MU1WK0HT
        super().__init__(save)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('MeanToNaga', bool, False, HGGStatusLib.MeanToNaga, None, notes="You've been an asshole to Nagas.")
        self.addSFXProperty('Naga:0', int, 0, HGGStatusLib.Naga, 0, intbool=True, notes='Last Naga you met was when you WERE a Naga.')
        self.addSFXProperty('Naga:1', int, 0, HGGStatusLib.Naga, 1, intbool=True, notes='You met a Naga as a Naga at some point.')
        self.addSFXProperty('Naga:2', None, 0, HGGStatusLib.Naga, 2, unused=True)
        self.addSFXProperty('Naga:3', None, 0, HGGStatusLib.Naga, 3, unused=True)
