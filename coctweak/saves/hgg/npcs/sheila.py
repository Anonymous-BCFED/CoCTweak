from coctweak.saves._npc import BaseNPC, NPCKFlagProperty, NPCStatusEffectProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class SheilaNPC(BaseNPC):
    ID = 'sheila'
    NAME = 'Sheila'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/NPCs/Sheila.as: public class Sheila extends Monster { @ 6iS2rt4F92pDevh0MO5ZLaCQ5Ttgus0SHbdHgA2g1f52ac4TbS8fOn2cRbXRgwV7oxgMzch59U3cJR7kV0hD4ou88q4kNgkF
        ## from [HGG]/classes/classes/Scenes/NPCs/SheilaScene.as: public class SheilaScene extends NPCAwareContent implements TimeAwareInterface { @ fLJ3gqekxenT8Q16Vn6cCgYW1uE6o9351gqE2nda0b5vd0v33Mj3pJ5OUgy9dDu9ZV1r787f5VL93Hbj481m3lO04x1KM6lN
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('SHEILA_CITE', int, 0, HGGKFlags.SHEILA_CITE)
        self.addKFlag('SHEILA_CLOCK', int, 0, HGGKFlags.SHEILA_CLOCK)
        self.addKFlag('SHEILA_CORRUPTION', int, 0, HGGKFlags.SHEILA_CORRUPTION)
        self.addKFlag('SHEILA_DEMON', int, 0, HGGKFlags.SHEILA_DEMON)
        self.addKFlag('SHEILA_DISABLED', int, 0, HGGKFlags.SHEILA_DISABLED)
        self.addKFlag('SHEILA_IMPS', int, 0, HGGKFlags.SHEILA_IMPS)
        self.addKFlag('SHEILA_JOEYS', int, 0, HGGKFlags.SHEILA_JOEYS)
        # PregnancyStore vag incubation counter
        self.addKFlag('SHEILA_PREGNANCY_INCUBATION', int, 0, HGGKFlags.SHEILA_PREGNANCY_INCUBATION)
        # PregnancyStore vag pregnancy type and notice counter
        self.addKFlag('SHEILA_PREGNANCY_TYPE', int, 0, HGGKFlags.SHEILA_PREGNANCY_TYPE)
        self.addKFlag('SHEILA_XP', int, 0, HGGKFlags.SHEILA_XP)
        ##################
        # Status Effects
        ##################
        self.addSFXProperty('SheilaOil', bool, False, HGGStatusLib.SheilaOil, None)
        ###################
        # PregnancyStores
        ###################
        self.vaginalPregnancy = HGGFlagPregnancyStore(save, HGGKFlags.SHEILA_PREGNANCY_TYPE, HGGKFlags.SHEILA_PREGNANCY_INCUBATION)
