from coctweak.saves._npc import BaseNPC, NPCKFlagProperty
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)
class AuntNancyNPC(BaseNPC):
    ID = 'auntnancy'
    NAME = 'AuntNancy'
    def __init__(self, save) -> None:
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/AuntNancy.as: public class AuntNancy extends TelAdreAbstractContent { @ fWxbrk4WT44ZakCbaS7Ce5bk79gc9Mep13rYfXngYC19WaX539CdGRbDIb0jamt5geeIS0XZgUq45D9cj8dB4se2eX4wq7vy
        super().__init__(save)
        ##########
        # kFlags
        ##########
        self.addKFlag('NANCY_MET', int, 0, HGGKFlags.NANCY_MET)
        self.addKFlag('NANCY_RELATIONSHIP_LEVEL', int, 0, HGGKFlags.NANCY_RELATIONSHIP_LEVEL)
        self.addKFlag('NANCY_TIMES_BONED', int, 0, HGGKFlags.NANCY_TIMES_BONED)
        self.addKFlag('NANCY_TIMES_TALKED', int, 0, HGGKFlags.NANCY_TIMES_TALKED)
