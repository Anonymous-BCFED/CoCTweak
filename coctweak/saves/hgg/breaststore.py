from enum import IntEnum
from typing import List, Dict
from coctweak.saves._breaststore import BaseSingleFlagBreastStore
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.breastcupsizes import HGGBreastCupSizes
from coctweak.saves.hgg.enums.lactationlevels import HGGLactationLevels
from coctweak.utils import rand
class HGGBreastStore(BaseSingleFlagBreastStore):
    ENUM_CUPSIZE = HGGBreastCupSizes
    ENUM_LACTATIONLEVEL = HGGLactationLevels

    GLOBAL_MIN_CUP_SIZE = HGGBreastCupSizes.FLAT
    GLOBAL_MAX_CUP_SIZE = HGGBreastCupSizes.ZZZ_LARGE

    GLOBAL_MIN_LACTATION_LEVEL = HGGLactationLevels.DISABLED
    GLOBAL_MAX_LACTATION_LEVEL = HGGLactationLevels.EPIC

    ## from [HGG]/classes/classes/BreastStore.as: private static const LACTATION_BOOST:Array = [0, 0, 2, 3, 6, 9, 17]; //Disabled, None, Light, Moderate, Strong, Heavy, Epic @ 0xU4d4552gsh79D6wu1e28MEbLO57x4mm5NLbDSfLy4qq8HkatzaJ7gNq3WL76J12PcezdDe4hm123dN62wl5pd29950d83D
    LACTATION_BOOST:List[int] = [0, 0, 2, 3, 6, 9, 17]

    ADJTABLE: Dict[HGGBreastCupSizes, str] = {
    ## from [HGG]/classes/classes/BreastStore.as: public function adj():String { @ gfy66Md6ndKncqb3sE3Mg8NkbQp7xC1DL6Sp4uYaLy46LeXmaS02G8eTAe6N1Iv9Uve7T3awdVF9X5gqI1aIefm5oR5S38uu
    #switch (_cupSize) {
        #case BreastCup.FLAT:
        #   return "non-existent";
        HGGBreastCupSizes.FLAT: 'non-existent',

        #case BreastCup.A:
        #	return "small";
        HGGBreastCupSizes.A: 'small',

        #case BreastCup.B:
        #case BreastCup.C:
        #	return "palmable";
        HGGBreastCupSizes.B: 'palmable',
        HGGBreastCupSizes.C: 'palmable',

        #case BreastCup.D:
        #case BreastCup.DD:
        #case BreastCup.DD_BIG:
        #	return "sizeable";
        HGGBreastCupSizes.D: 'sizeable', #[sic]
        HGGBreastCupSizes.DD: 'sizeable', #[sic]
        HGGBreastCupSizes.DD_BIG: 'sizeable', #[sic]

        #case BreastCup.E:
        #case BreastCup.E_BIG:
        #case BreastCup.EE:
        #case BreastCup.EE_BIG:
        #case BreastCup.F:
        #case BreastCup.F_BIG:
        #case BreastCup.FF:
        #case BreastCup.FF_BIG:
        #   return "huge";
        HGGBreastCupSizes.E: 'huge',
        HGGBreastCupSizes.E_BIG: 'huge',
        HGGBreastCupSizes.EE: 'huge',
        HGGBreastCupSizes.EE_BIG: 'huge',
        HGGBreastCupSizes.F: 'huge',
        HGGBreastCupSizes.F_BIG: 'huge',
        HGGBreastCupSizes.FF: 'huge',
        HGGBreastCupSizes.FF_BIG: 'huge',

        #case BreastCup.G:
        #case BreastCup.G_BIG:
        #case BreastCup.GG:
        #case BreastCup.GG_BIG:
        #case BreastCup.H:
        #case BreastCup.H_BIG:
        #case BreastCup.HH:
        #case BreastCup.HH_BIG:
        #case BreastCup.I:
        #case BreastCup.I_BIG:
        #case BreastCup.II:
        #case BreastCup.II_BIG:
        #    return "gigantic";
        HGGBreastCupSizes.G: 'gigantic',
        HGGBreastCupSizes.G_BIG: 'gigantic',
        HGGBreastCupSizes.GG: 'gigantic',
        HGGBreastCupSizes.GG_BIG: 'gigantic',
        HGGBreastCupSizes.H: 'gigantic',
        HGGBreastCupSizes.H_BIG: 'gigantic',
        HGGBreastCupSizes.HH: 'gigantic',
        HGGBreastCupSizes.HH_BIG: 'gigantic',
        HGGBreastCupSizes.I: 'gigantic',
        HGGBreastCupSizes.I_BIG: 'gigantic',
        HGGBreastCupSizes.II: 'gigantic',
        HGGBreastCupSizes.II_BIG: 'gigantic',

        #case BreastCup.J:
        #case BreastCup.J_BIG:
        #case BreastCup.JJ:
        #case BreastCup.JJ_BIG:
        #case BreastCup.K:
        #case BreastCup.K_BIG:
        #case BreastCup.KK:
        #case BreastCup.KK_BIG:
        #case BreastCup.L:
        #case BreastCup.L_BIG:
        #case BreastCup.LL:
        #case BreastCup.LL_BIG:
        #case BreastCup.M:
        #case BreastCup.M_BIG:
        #case BreastCup.MM:
        #case BreastCup.MM_BIG:
        #case BreastCup.MMM:
        #case BreastCup.MMM_LARGE:
        #    return "mammoth";
        HGGBreastCupSizes.J: 'mammoth',
        HGGBreastCupSizes.J_BIG: 'mammoth',
        HGGBreastCupSizes.JJ: 'mammoth',
        HGGBreastCupSizes.JJ_BIG: 'mammoth',
        HGGBreastCupSizes.K: 'mammoth',
        HGGBreastCupSizes.K_BIG: 'mammoth',
        HGGBreastCupSizes.KK: 'mammoth',
        HGGBreastCupSizes.KK_BIG: 'mammoth',
        HGGBreastCupSizes.L: 'mammoth',
        HGGBreastCupSizes.L_BIG: 'mammoth',
        HGGBreastCupSizes.LL: 'mammoth',
        HGGBreastCupSizes.LL_BIG: 'mammoth',
        HGGBreastCupSizes.M: 'mammoth',
        HGGBreastCupSizes.M_BIG: 'mammoth',
        HGGBreastCupSizes.MM: 'mammoth',
        HGGBreastCupSizes.MM_BIG: 'mammoth',
        HGGBreastCupSizes.MMM: 'mammoth',
        HGGBreastCupSizes.MMM_LARGE: 'mammoth',
    }


    def breastDescript(self) -> str:
        size = self.cupSize.value
        lactation = self.lactationLevel.value

        ## from [HGG]/classes/classes/BreastStore.as: public static function breastDescript(size:int, lactation:Number = 0):String { @ 4cZgPt9E0f1J2DC11B7M9f265Nc8CR87p3Fc4fw2OU8OW9Va0mn9weaSDdcE2jw6kZ0M5663gUj9d11SUa5L3Lh0Wre5MeGu
        #if (size < 1) return "flat breasts";
        if size < 1:
            return 'flat breasts'
        #var descript:String = (rand(2) === 0 ? Appearance.breastSize(size) : ""); //Add a description of the breast size 50% of the time
        descript: str = self.save.appearance.breastSize(size) if rand(2) == 0 else ''
        #switch (rand(10)) {
        caseVal: int = rand(10)
        #case 1:
        if caseVal == 1:
            #if (lactation > 2) return descript + "milk-udders";
            return descript + 'milk-udders'
            #break;
        #case 2:
        elif caseVal == 2:
            #if (lactation > 1.5) descript += "milky ";
            if lactation > 1.5:
                descript += 'milky '
            #if (size > 4) return descript + "tits";
            if size > 4:
                return descript + 'tits'
            #break;
        #case 4:
        #case 5:
        #case 6:
        elif caseVal in (4,5,6):
            #return descript + "tits";
            return descript + 'tits'
        #case 7:
        elif caseVal == 7:
            #if (lactation >= 2.5) return descript + "udders";
            if lactation >= 2.5:
                return descript + 'udders'
            #if (lactation >= 1) descript += "milk ";
            if lactation >= 1:
                descript += 'milk '
            #return descript + "jugs";
            return descript + 'jugs'
        #case 8:
        elif caseVal == 8:
            #if (size > 6) return descript + "love-pillows";
            if size > 6:
                return descript + 'love-pillows'
            #return descript + "boobs";
            return descript + 'boobs'
        #case 9:
        elif caseVal == 9:
            #if (size > 6) return descript + "tits";
            if size > 6:
                return descript + 'tits'
            #break;
        #default:
        #return descript + "breasts"
        return descript + 'breasts'

    def adj(self) -> str:
        size = self.cupSize.value
        ## from [HGG]/classes/classes/BreastStore.as: public function adj():String { @ gfy66Md6ndKncqb3sE3Mg8NkbQp7xC1DL6Sp4uYaLy46LeXmaS02G8eTAe6N1Iv9Uve7T3awdVF9X5gqI1aIefm5oR5S38uu
        # See ADJTABLE for case table.
        if self.cupSize in self.ADJTABLE:
            return self.ADJTABLE[self.cupSize]
        else:
            return 'titanic'

    def canTitFuck(self) -> bool:
        ## from [HGG]/classes/classes/BreastStore.as: public function canTitFuck():Boolean { @ 8dl9bO2Zhfi63iY5BO1p5f8saHd91k78qcxQgu56aT8Xr4wqb0ucrR8Ap5Pdcs59TH1fT23Y8qwe2BfWic5q6jI5LQ20oaPF
        #return _cupSize >= BreastCup.C;
        return self.cupSize >= HGGBreastCupSizes.C

    def cup(self) -> str:
        ## from [HGG]/classes/classes/BreastStore.as: public function cup():String { @ dY29Dob597Z09ULcx1bjF0Zn6GXf3L46vdn4eQk578gHXaUJdoZ1qYe4d0i65YucFP0XPd0b0Yvg9VaKvafg1NsasnclQdiZ
        #return Appearance.breastCup(_cupSize);
        return self.save.appearance.breastCup(self._cupSize)

    def description(self, useAdj: bool = False) -> str:
        ## from [HGG]/classes/classes/BreastStore.as: public function description(useAdj:Boolean = false, isMale:Boolean = false):String { @ bjkbE67OT8Wg6vJ6x6e6G8R8asD2Se2t9eF34Ay22z3wmfo77kB13Jan64T78YrcxM9FL8KIahMfuF15igUW2S5aev5Ti4Y6
        #if (_cupSize === BreastCup.FLAT) return "flat" + (isMale ? " manly," : "") + " chest";
        if self.cupSize == HGGBreastCupSizes.FLAT:
            adjs = ['flat']
            if self.isMale:
                adjs += ['manly']
            return ', '.join(adjs)+' chest'
        else:
            #return (useAdj ? adj() + " " : "") + cup() + " breasts";
            adjs = []
            if useAdj:
                adjs += [self.adj()]
            adjs += [self.cup()]
            return ' '.join(adjs) + ' breasts'

    # why
    def breastDesc(self) -> str:
        ## from [HGG]/classes/classes/BreastStore.as: public function breastDesc():String { @ 6ZM96A4Fb6IFeO48Kjaiw3E172T4jkgaLdIOgYUaeo3Cv8JV0hL3uSggffiw6fLcG9gCacMHd0v1ei0QM85Wc7S9GIgGbaf8
        #return breastDescript(cupSize, 0.5 * lactationLevel);
        return self.breastDescript(self._cupSize, self._lactationLevel * 0.5)

    def hasBreasts(self) -> bool:
        ## from [HGG]/classes/classes/BreastStore.as: public function hasBreasts():Boolean { @ gA25fNdKL8ak2SDcej37Yc5l2UwcIT4O13ye5NL2mAb1k3JhfCr8Qj1al9EMaDq5FJeEy4Tf7XMf4eaNb1Xs50I3YzeQt8mW
        #return _cupSize !== BreastCup.FLAT;
        return self.cupSize > HGGBreastCupSizes.FLAT

    def isLactating(self) -> bool:
        ## from [HGG]/classes/classes/BreastStore.as: public function lactating():Boolean { @ g7NcaR6971V1ggY1oBfjZ4wn1czaJJ3hoaNO3oRcBr0GR0pDf9FdYj2vK5yD03336OgCndQ72Gd8Qq3Zi4ISad07Msdbu14d
        #return _lactation >= LACTATION_LIGHT;
        return self.lactationLevel >= HGGLactationLevels.LIGHT


    def milkIsFull(self) -> bool:
        ## from [HGG]/classes/classes/BreastStore.as: public function milkIsFull():Boolean { @ 0jYejv8se99Sb8Y2aN16WdoGcZvg5EgJR6VL2Fz1DJ3k121w7M969P0zg9WlfTv6Qj7w22n438k0sx7Bz5AidCk2PZcMt5mV
        #return (_lactation <= LACTATION_NONE ? 0 : _fullness >= 50);
        return self.lactationLevel > HGGLactationLevels.NONE and self.fullness >= 50

    def milkIsOverflowing(self) -> bool:
        ## from [HGG]/classes/classes/BreastStore.as: public function milkIsOverflowing():Boolean { @ b7faoD7D5gLobOPaMa84q1CG8h94kO9DE41X6ace9tes696O9Tr6mr3A66qzgKF2az07J1h7buIdfRcdo63N6Ma52fbiw1mi
        #return (_lactation <= LACTATION_NONE ? 0 : _fullness >= 60 + 5 * LACTATION_BOOST[_lactation]); //Probably pretty desperate to be milked by this point
        return self.lactationLevel > HGGLactationLevels.NONE and self.fullness >= 60 + 5 * self.LACTATION_BOOST[self.lactationLevel.value]

    def milkQuantity(self) -> float:
        #//At fullness == 50 the maximum amount of milk is produced. When overfull, lactation level is reduced and fullness drops to 50.
        #//So a higher lactationLevel means more milk is produced and the breasts can stay full without drying up for longer. Will always return 0 if not lactating
        ## from [HGG]/classes/classes/BreastStore.as: public function milkQuantity():Number { @ 01896CfSkg513VGc8wa147WV7BNayv5f3f5Yb5AdyD4mm8dh9CT5Hd099aVv9jj1iS7mndVt8P53ezeXI9uD3kd29W5yD1LT

        #if (_lactation <= LACTATION_NONE) return 0;
        if self.lactationLevel <= HGGLactationLevels.NONE:
            return 0
        #return 0.01 * Math.max(100, 2 * _fullness) * Number(20 * _rows * _cupSize * (_lactation - 1));
        return 0.01 * max(100, 2 * self.fullness) * float(20 * self.rows * self.cupSize.value * (self._lactationLevel - 1))

    def nippleDescript(self, tiny: str = 'tiny', small: str='prominent', large:str='large', huge:str='elongated', massive:str='massive') -> bool:
        ## from [HGG]/classes/classes/BreastStore.as: public function nippleDescript(tiny:String = "tiny", small:String = "prominent", large:String = "large", huge:String = "elongated", massive:String = "massive"):String { @ fwf1ha7aD2k53Ph7yf5Hwdtq4wkcu1fsL4MT9yr7qgdhRdXHepA8Jp8Mt80u27F1B9f9P0A66Go5F47fE1gmdaGcNwgVG686
        #if (_nippleLength < 3) return tiny;
        if self.nippleLength < 3:
            return tiny
        #if (_nippleLength < 10) return small;
        if self.nippleLength < 10:
            return small
        #if (_nippleLength < 20) return large;
        if self.nippleLength < 20:
            return large
        #if (_nippleLength < 32) return huge;
        if self.nippleLength < 32:
            return huge
        return massive

    def reset(self) -> None:
        super().reset()
        self.rows = 1
        self.cupSize = 0
        self.lactationLevel = 0
        self.nippleLength = 0
        self.fullness = 0
        self.timesMilked = 0
        self.preventLactationIncrease = 0
        self.preventLactationDecrease = 0
        