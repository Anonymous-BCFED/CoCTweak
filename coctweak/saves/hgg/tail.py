from coctweak.saves.hgg.enums.tailtypes import HGGTailType
from coctweak.saves._tail import BaseTail

class HGGTail(BaseTail):
    ENUM_TYPES = HGGTailType
