from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class OwcaSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('owca')
        ## from [HGG]/classes/classes/Scenes/Places/Owca.as: public class Owca extends BaseContent implements SelfSaving, SelfDebug { @ eU81PAbss2ee2yp5bH36FaOpaac40Q5u8c0ugxJcB54ir7Tm5lt4Fla2Ufvzgsp9U1dkZ1ZH9adc8L9cb5bo69lgd42Rs3ic
        '''
        {
            "rebeccKilled": [
                "Boolean",
                ""
            ],
            "lostPit": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('lostPit', False, '')
        self.addBoolean('rebeccKilled', False, '')
