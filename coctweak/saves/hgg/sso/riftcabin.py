from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class RiftCabinSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('riftCabin')
        ## from [HGG]/classes/classes/Scenes/Areas/GlacialRift/RiftCabinScene.as: public class RiftCabinScene extends BaseContent implements Encounter, SelfSaving, SelfDebug { @ 0jAdqF6rNbeCfm60S4dBM8cW0sL7smbu5dQw9363MA5ia33A9pC0vncELgT84b24Lbe3M2pg4FmdxYcCSaBb1p48goeSXeTQ
        self.addBoolean('bloomersBurned', False, 'Whether you burnt the bloomers.')
        self.addBoolean('encountered', False, 'Whether the cabin was encountered yet or not.')
        self.addBoolean('loliVersion', False, 'Did you encounter the loli version of the scene?')
        self.addChoices('orgasmType', '""', 'If you orgasmed, what organ produced it?', {
            '': 'N/A',
            'Dick': 'Penis',
            'Vaginal': 'Vagina',
        })
        self.addBoolean('pictureBurned', False, 'Whether you burnt the picture.')
        self.addBoolean('tookBloomers', False, 'Whether you stole the bloomers.')
