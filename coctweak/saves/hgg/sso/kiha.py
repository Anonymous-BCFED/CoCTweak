from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class KihaSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('kiha')
        ## from [HGG]/classes/classes/Scenes/NPCs/KihaFollowerScene.as: public class KihaFollowerScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ di2fGn4wy2P4cyiflH8ujaCq8ksbrPgg4bs10qO4z61LxbaucZc2Fr0Li4xI3Lb6MobCm8K87ny15j3li5ybdBgbVcat50mZ
        ## from [HGG]/classes/classes/Scenes/NPCs/KihaScene.as: public class KihaScene extends NPCAwareContent { @ 30S3njaKf59HenK4t4fhbcC242X5spdflbKR1GCg1449ScjOafI49qcgJbOc4k24AZfQH2Js11L0PEe0p6sY1cM52z0Sh2t9
        '''
        {
            "badCookingTasted": [
                "IntList",
                "",
                [
                    {
                        "label": "0",
                        "data": 0
                    },
                    {
                        "label": "1",
                        "data": 1
                    },
                    {
                        "label": "2",
                        "data": 2
                    },
                    {
                        "label": "3",
                        "data": 3
                    }
                ]
            ],
            "flowerGifted": [
                "Boolean",
                ""
            ],
            "kidFirebreathing": [
                "Boolean",
                ""
            ]
        }
        '''
        # ```Needs documentation of what the values mean
        self.addChoices('badCookingTasted', 0, '', {
            0: '0',
            1: '1',
            2: '2',
            3: '3',
        })
        self.addBoolean('flowerGifted', False, '')
        self.addBoolean('giftedFlower', False, '')
        self.addBoolean('kidFirebreathing', False, '')
        self.addBoolean('wakeUp', False, '')
