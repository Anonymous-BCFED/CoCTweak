from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class ScorpionSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('scorpion')
        ## from [HGG]/classes/classes/Scenes/Areas/Desert/ScorpionScene.as: public class ScorpionScene extends BaseContent implements Encounter, SelfSaving, SelfDebug { @ 6dj2Xv25ugwzcreb8Id3o2790Bb4f45a345v4q1ciceCpgmkeoQ7OA8pR7xtdGY8Ce4bK1QK3j7f6BgOR8eg9j2bWc57Egf1
        '''
        {
            "state": [
                "IntList",
                "The overall state of the scorpion",
                [
                    {
                        "label": "Disabled",
                        "data": -1
                    },
                    {
                        "label": "Unencountered",
                        "data": 0
                    },
                    {
                        "label": "Encountered",
                        "data": 1
                    }
                ]
            ],
            "fate": [
                "IntList",
                "How the encounter ended",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Killed",
                        "data": 1
                    },
                    {
                        "label": "Spared",
                        "data": 2
                    },
                    {
                        "label": "Followed You",
                        "data": 3
                    },
                    {
                        "label": "Ridden",
                        "data": 4
                    }
                ]
            ],
            "talked": [
                "Boolean",
                "Whether you tried to talk to it"
            ]
        }
        '''
        # 0 = n/a; 1 = killed; 2 = ignored; 3 = followed; 4 = ridden
        self.addChoices('fate', 0, 'How the encounter ended', {
            0: 'N/A',
            1: 'Killed',
            2: 'Spared',
            3: 'Followed You',
            4: 'Ridden',
        })
        # -1 = disabled; 0 = n/a; 1 = encountered
        self.addChoices('state', 0, 'The overall state of the scorpion', {
            -1: 'Disabled',
            0: 'Unencountered',
            1: 'Encountered',
        })
        self.addBoolean('talked', False, 'Whether you tried to talk to it')
