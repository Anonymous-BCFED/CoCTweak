from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class IvorySuccubusSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('ivory succubus')
        ## from [HGG]/classes/classes/Scenes/Monsters/IvorySuccubus.as: public class IvorySuccubus extends Monster { @ cLD5VYfFJbNh7AlgdO5eU4kna4ddQq3Je7wX1fPce2d8ae2qgO1bbk6dX47HeIY5wB6SLagqfLH2k82zd2pPewH07Hd0TeWZ
        ## from [HGG]/classes/classes/Scenes/Monsters/IvorySuccubusScene.as: public class IvorySuccubusScene extends BaseContent implements SelfSaving, SelfDebug { @ eZZeTh5J26kc6rI0vTgwV5VzeBk0G76MLah5ak9ahIdFP1iOctt8pI0ZnbFLgQfd4r2I332Hcye42m1Io4Cs2n41iE7KMc9H
        '''
        {
            "met": [
                "Boolean",
                ""
            ],
            "deadHookers": [
                "Int",
                ""
            ],
            "timesLost": [
                "Int",
                ""
            ]
        }
        '''
        self.addInt('deadHookers', 0, 'How many succubi you have killed')
        self.addBoolean('met', False, 'Have you met one of these creatures before?')
        self.addInt('timesLost', 0, 'How many times you lost to a succubi')
