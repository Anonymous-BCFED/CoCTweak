from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class AkbalSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('akbal')
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/Akbal.as: public class Akbal extends Monster { @ 5aB3G4f3S66g3te6zy4yhcXi15ncio5ajfy3drXdPnc3q09hcg7bnY4Rr0zof6sfMLbDo7FmbZAdyE5Jp26h86F4ZN7T8gTV
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/AkbalScene.as: public class AkbalScene extends BaseContent implements Encounter, SelfSaving, SelfDebug { @ 46x9e81Ei60rbZl4OG4pRbf85UH3VidtPetc8XZayIaxHfYQ9Bj1ik28Oe5A39Dfix0zd0LvgS9bza4UjgY45ytd8W78cavb
        '''
        {
            "timesRaped": [
                "Int",
                ""
            ],
            "strayCat": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('strayCat', False, '')
        self.addInt('timesRaped', 0, '')
