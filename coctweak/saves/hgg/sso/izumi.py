from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class IzumiSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('izumi')
        ## from [HGG]/classes/classes/Scenes/Areas/HighMountains/IzumiScene.as: public class IzumiScene extends BaseContent implements SelfSaving, SelfDebug { @ eigaoOewzc2KfS5fOh3JjezMfg33Yx4k478Z74R7oaa628RCfj0g9ha102kz3PccTP51p3nUaHJcyG9JI81naPkb8z0o1azh
        '''
        {
            "contestStage": [
                "NumberList",
                "",
                [
                    {
                        "label": "",
                        "data": 0
                    },
                    {
                        "label": "Failed 1",
                        "data": 0.5
                    },
                    {
                        "label": "Cleared 1",
                        "data": 1
                    },
                    {
                        "label": "Failed 2",
                        "data": 1.5
                    },
                    {
                        "label": "Cleared 2",
                        "data": 2
                    },
                    {
                        "label": "Failed 3",
                        "data": 2.5
                    },
                    {
                        "label": "Cleared 3",
                        "data": 3
                    }
                ]
            ]
        }
        '''
        self.addChoices('contestStage', 0, '', {
            0: '',
            0.5: 'Failed 1',
            1: 'Cleared 1',
            1.5: 'Failed 2',
            2: 'Cleared 2',
            2.5: 'Failed 3',
            3: 'Cleared 3',
        })
