from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class BogTempleSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('bogtemple')
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/BogTemple.as: public class BogTemple extends BaseContent implements SelfSaving, SelfDebug { @ 3O93SKdcNemG0Oj5JW4KhgjKd2Xaat0ZL4Nk1bp9iT3hegHFe5B3EE5BH3Zm0mT0ZvdA1dUD5xibWw2ge5OH9cBaeF9os1gX
        '''
        {
            "foundTemple": [
                "Boolean"
            ],
            "shieldTaken": [
                "Boolean"
            ],
            "inspection": [
                "BitFlag",
                [
                    "What inspect options you've seen"
                ],
                [
                    "Unused",
                    "Sculpture",
                    "Altar",
                    "Statuettes",
                    "Books",
                    "Hole",
                    "Puddles",
                    "Balcony",
                    "Entrance"
                ]
            ],
            "seenBalcony": [
                "Boolean",
                "Whether you've flown up to the top"
            ],
            "excludeExplore": [
                "Int",
                "The temple cannot be encountered at this total time"
            ],
            "timesVisited": [
                "Int",
                "Total temple encounters"
            ],
            "timesPrayed": [
                "Int"
            ],
            "timesBathed": [
                "Int"
            ]
        }
        '''
        self.addInt('excludeExplore', 0, 'The temple cannot be encountered at this total time')
        self.addBoolean('foundTemple', False, '')
        self.addInt('inspection', 0, "What inspect options you've seen", bitflags={
            1: 'Unused',
            2: 'Sculpture',
            4: 'Altar',
            8: 'Statuettes',
            16: 'Books',
            32: 'Hole',
            64: 'Puddles',
            128: 'Balcony',
            256: 'Entrance',
        })
        self.addBoolean('seenBalcony', False, "Whether you've flown up to the top")
        self.addBoolean('shieldTaken', False, '')
        self.addInt('timesBathed', 0, '')
        self.addInt('timesPrayed', 0, '')
        self.addInt('timesVisited', 0, 'Total temple encounters')
