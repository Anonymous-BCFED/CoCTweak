from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class RathazulSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('rathazul')
        ## from [HGG]/classes/classes/Scenes/NPCs/Rathazul.as: public class Rathazul extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface, Encounter { @ bKcgBvaJ3gva6lg2zMcYJeJNcQM2kj24B19TeIgaIG7e31dY4yM7mOcif2nJ44O6yK6Tq2J59XY5ebg2n7ljgEbfRRfjx4Dx
        '''
        {
            "metRathazul": [
                "Boolean",
                ""
            ],
            "campFollower": [
                "Boolean",
                ""
            ],
            "campOffer": [
                "Boolean",
                ""
            ],
            "mixologyXP": [
                "Int",
                ""
            ],
            "offeredGel": [
                "Boolean",
                ""
            ],
            "offeredChitin": [
                "Boolean",
                ""
            ],
            "offeredSilk": [
                "Boolean",
                ""
            ],
            "offeredScale": [
                "Boolean",
                ""
            ],
            "offeredEbonbloom": [
                "Boolean",
                ""
            ],
            "offeredDye": [
                "Boolean",
                ""
            ],
            "offeredPhilter": [
                "Boolean",
                ""
            ],
            "offeredHoney": [
                "Boolean",
                ""
            ],
            "offeredReducto": [
                "Boolean",
                ""
            ],
            "offeredMarae": [
                "Boolean",
                ""
            ],
            "offeredGolemHeart": [
                "Boolean",
                ""
            ],
            "offeredTBark": [
                "Boolean",
                ""
            ],
            "offeredDBark": [
                "Boolean",
                ""
            ],
            "offeredTrice": [
                "Boolean",
                ""
            ],
            "offeredOculum": [
                "Boolean",
                ""
            ],
            "offeredPurify": [
                "Boolean",
                ""
            ],
            "offeredDemonTF": [
                "Boolean",
                ""
            ],
            "offeredDelight": [
                "Boolean",
                ""
            ],
            "offeredMinoCum": [
                "Boolean",
                ""
            ],
            "offeredLaBova": [
                "Boolean",
                ""
            ],
            "offeredDebimboPlayer": [
                "Boolean",
                ""
            ],
            "offeredDebimboSophie": [
                "Boolean",
                ""
            ],
            "offeredDebimboIzma": [
                "Boolean",
                ""
            ],
            "bearGifted": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('bearGifted', False, '')
        self.addBoolean('campFollower', False, '')
        self.addBoolean('campOffer', False, '')
        self.addBoolean('giftedBear', False, '')
        self.addBoolean('metRathazul', False, '')
        self.addInt('mixologyXP', 0, '')
        self.addBoolean('offeredChitin', False, '')
        self.addBoolean('offeredDBark', False, '')
        self.addBoolean('offeredDebimboPlayer', False, '')
        self.addBoolean('offeredDebimboSophie', False, '')
        self.addBoolean('offeredDelight', False, '')
        self.addBoolean('offeredDemonTF', False, '')
        self.addBoolean('offeredDye', False, '')
        self.addBoolean('offeredEbonbloom', False, '')
        self.addBoolean('offeredGel', False, '')
        self.addBoolean('offeredGolemHeart', False, '')
        self.addBoolean('offeredHoney', False, '')
        self.addBoolean('offeredLaBova', False, '')
        self.addBoolean('offeredLactaidTaurinum', False, '')
        self.addBoolean('offeredMarae', False, '')
        self.addBoolean('offeredMinoCum', False, '')
        self.addBoolean('offeredOculum', False, '')
        self.addBoolean('offeredPhilter', False, '')
        self.addBoolean('offeredPurify', False, '')
        self.addBoolean('offeredReducto', False, '')
        self.addBoolean('offeredScale', False, '')
        self.addBoolean('offeredSilk', False, '')
        self.addBoolean('offeredTBark', False, '')
        self.addBoolean('offeredTrice', False, '')
