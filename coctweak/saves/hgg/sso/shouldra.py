from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class ShouldraSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('shouldra')
        ## from [HGG]/classes/classes/Scenes/NPCs/Shouldra.as: public class Shouldra extends Monster { @ 8m21aQdh08mNchc7rR8jN9ozfsw6NL70S1xm7VJbKK0HsaXDgBYdDk0tndpqfME56mfKHgbF2R10lP0P8b9d3dBf3p7vh6Q8
        ## from [HGG]/classes/classes/Scenes/NPCs/ShouldraFollower.as: public class ShouldraFollower extends NPCAwareContent implements SelfSaving, SelfDebug { @ 9H66s64YPb0z26H9HIelR4lygI9dFc9KY6rYd07eib8ndfEZ11B7tM4Q93FS95C7457nE6FbcTs6BDd6556q2EW5Ppd6jdbi
        ## from [HGG]/classes/classes/Scenes/NPCs/ShouldraScene.as: public class ShouldraScene extends NPCAwareContent implements TimeAwareInterface { @ 4SJh1SaLEbZJ67GfvDflygxwcmy8iHcUcacF9sj2mlfrsdOu8s59pFaTk5SZ5dA9ohe5K1KP7HadrA0qK21tfAk3ymaoH6Sk
        '''
        {
            "mastIntro": [
                "Boolean",
                ""
            ],
            "frustrationKnown": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('frustrationKnown', False, '')
        self.addBoolean('mastIntro', False, '')
