from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class LumberjackSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('lumberjack')
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/LumberjackScene.as: public class LumberjackScene  extends BaseContent implements Encounter, SelfSaving, SelfDebug { @ 6TJ1s2bZT1mB7wadn45yJdD97Pl7a02cZ9qR3in1t72jZdvRen69q7aGR1AY5ax0XdeGO88D5wD4Ho21z6D776w8Nl2C42sj
        '''
        {
            "encountered": [
                "Boolean",
                ""
            ],
            "aggressive": [
                "Boolean",
                ""
            ],
            "freedSucc": [
                "Boolean",
                ""
            ],
            "executed": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('aggressive', False, 'Whether you were aggressive towards him')
        self.addBoolean('encountered', False, 'Have you encountered him?')
        self.addBoolean('executed', False, 'Did you kill him?')
        self.addBoolean('freedSucc', False, 'Did you free the succubus?')
