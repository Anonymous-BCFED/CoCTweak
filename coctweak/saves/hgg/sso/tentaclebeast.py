from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class TentacleBeastSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('tentaclebeast')
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/TentacleBeastScene.as: public class TentacleBeastScene extends BaseContent implements SelfSaving, SelfDebug { @ 8p4fbi7xpdhZf9r402bXkeZW727daL63d5mm3QRcfzcgl7UP7b45qvf5JgV2a924IXarl3Br8bd0lvdbg5jEeg89yK1ZT9eD
        '''
        {
            "timesLost": [
                "Int",
                ""
            ]
        }
        '''
        self.addInt('timesLost', 0, '')
