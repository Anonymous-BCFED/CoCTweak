from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class BeeGirlSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('beegirl')
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/BeeGirlScene.as: public class BeeGirlScene extends BaseContent implements SelfSaving, SelfDebug { @ cRZ9Rk0ktbBe3kJc9Xed44HF16U2z0793c6gfOJfNMcxdaWd2KOeNW4N31ug03U8T5gAMc1f8fv3Nr4yee33aib2M90WD9yf
        '''
        {
            "timesEgged": [
                "Int",
                ""
            ]
        }
        '''
        self.addInt('timesEgged', 0, '')
