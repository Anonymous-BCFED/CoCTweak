from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class MaraeSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('marae')
        ## from [HGG]/classes/classes/Scenes/Places/Boat/Marae.as: public class Marae extends Monster { @ fBy4EC1dI3gpetZezQ5JrdYqbgk8yf11kfJD0PYf6Z8WrgbV5J4c0vgbyayb0RL5vKccX3QR7R13KZ5YN0S88zP4UV5Wh2Rk
        ## from [HGG]/classes/classes/Scenes/Places/Boat/MaraeScene.as: public class MaraeScene extends AbstractBoatContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 1gFbPxbWYdLXgrGf6wa1Y2iyfvg5tnbrW1bOcd09LOfVJbYXaVU8jq8FgcbQ3I3dsMcbn7a5djE6280Ip3jX1sM0R5gQ60rl
        '''
        {
            "feraMet": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('feraMet', False, '')
