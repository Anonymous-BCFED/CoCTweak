from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class RubiSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('rubi')
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Rubi.as: public class Rubi extends TelAdreAbstractContent implements SelfSaving, SelfDebug { @ 6WOdtCbpIb6Zaci5GqaHu9tsbZP6L22iqbbV9dP5yP7I6eBm2R0azj71D6nt5CMh122SOcET8os3g26FX14b33j2TA5q4fIK
        '''
        {
            "hadSex": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('hadSex', False, '')
