from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class DesertEncounterSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('desert')
        ## from [HGG]/classes/classes/Scenes/Areas/Desert.as: public class Desert extends BaseContent implements IExplorable, SelfSaving, SelfDebug { @ 4fe2e9ayd7x824v2ux0QIcdJb5XdCDbV48gQ7Lh7Mc7T8242b3hetl38XfXFeX8aiP08mdmFc2G3dJ9RI7rB7XxcYLbZD0Df
        '''
        {
            "foundMirage": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('foundMirage', False, '')
