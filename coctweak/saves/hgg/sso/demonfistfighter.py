from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class DemonFistFighterSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('demonfistFighter')
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/DemonFistFighter.as: public class DemonFistFighter extends Monster { @ 5U957g7bN1P45ppc4k9frbgA5WcepV6ikdghe4v48l2Fh4oG3Q7ctv3BR2YR8gt1OPgPcdAB9f31sqeBCfwQcjQ5Kc8Xig04
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/DemonFistFighterScene.as: public class DemonFistFighterScene extends BaseContent implements SelfSaving, TimeAwareInterface { @ eCt7Tz4fI18d6sef0o5dddEg4HPcC63LuciW1TL3J9bs44Sb88h8Xq28E7kRcTbfDO2ht1LV1Tn4Jp6uU7XO9v47IB2zZcuG
        self.addBoolean('beatDemonfist', False, "Whether you've defeated Demonfist.")
        self.addInt('consecutiveLosses', 0, '')
        self.addInt('demonfistTimeAway', 0, '')
        self.addBoolean('learnedOfDemonFist', False, '')
        self.addBoolean('newRulesExplained', True, '')
        self.addStr('playerName', '""', 'Your ring-name.')
        self.addBoolean('shookDogHand', False, '')
        self.addInt('timesBrokenRules', 0, '')
        self.addInt('timesFoughtDog', 0, '')
        self.addInt('timesLost', 0, '')
        self.addInt('timesWon', 0, '')
