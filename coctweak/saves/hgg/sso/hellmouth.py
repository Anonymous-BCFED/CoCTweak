from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class HellmouthSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('hellmouth')
        ## from [HGG]/classes/classes/Scenes/Areas/VolcanicCrag/Hellmouth.as: public class Hellmouth extends Monster { @ 5MgbRJ5OQ6Yj37HeH30d90cn457aPndig2kd2s2gNdaCA64c0dmffY2SH4mX4rt1TlgPSdQOeLD2gC3GQg0RaYX2sK2Reg0k
        ## from [HGG]/classes/classes/Scenes/Areas/VolcanicCrag/HellmouthScene.as: public class HellmouthScene extends BaseContent implements SelfSaving, SelfDebug { @ 4jCdso6cfdGZ0Gn1eEbrr1C21Zc0Mi3SU2o35qi7o45zP09J3y43BQa9ZeR40ya9Ti6Sza2C0qz1bifnU0GL7r7bes6x38T9
        '''
        {
            "ambushed": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('ambushed', False, '')
