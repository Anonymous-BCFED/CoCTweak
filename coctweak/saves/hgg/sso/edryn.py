from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class EdrynSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('edryn')
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Edryn.as: public class Edryn extends TelAdreAbstractContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ f7nady4MU7lV24O1fA3Nu6fr1wYdidcDdaw1gCb4MKdmVdBo2uY71d5oOfund9m4Iga2R9AibTs0BUf43fCugCIgAi0S36pJ
        '''
        {
            "boys": [
                "Int",
                ""
            ],
            "girls": [
                "Int",
                ""
            ],
            "herms": [
                "Int",
                ""
            ],
            "kidMet": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addInt('boys', 0, '')
        self.addInt('girls', 0, '')
        self.addInt('herms', 0, '')
        self.addBoolean('kidMet', False, '')
