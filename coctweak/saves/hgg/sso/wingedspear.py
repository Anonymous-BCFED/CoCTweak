from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class WingedSpearSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('wingedspear')
        ## from [HGG]/classes/classes/Scenes/Areas/HighMountains/WingedSpearScene.as: public class WingedSpearScene extends BaseContent implements Encounter, SelfSaving, SelfDebug { @ 2hv0J53Hqd2wcAK9f2aIe4OYbuf7SGbzf4uSfoz3n4aEG0xmaCg3L6d2SfNU11sdbO3LV59Q019eT56152GF2qa7Ey2Dw8r0
        '''
        {
            "encountered": [
                "Boolean",
                ""
            ],
            "examined": [
                "Boolean",
                ""
            ],
            "takenSpear": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('encountered', False, '')
        self.addBoolean('examined', False, '')
        self.addBoolean('takenSpear', False, '')
