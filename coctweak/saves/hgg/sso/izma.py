from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class IzmaSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('izma')
        ## from [HGG]/classes/classes/Scenes/NPCs/IzmaScene.as: public class IzmaScene extends NPCAwareContent implements TimeAwareInterface, SelfSaving, SelfDebug, Encounter { @ 2wHgfH02J98EbqabtEaob9lWgrpeWHewOeXU0mR81BcHBayM9MB4rGcxF97Ig3veWb8p949N5GkbUtgHT45QeL62sy6wUfyv
        '''
        {
            "sharkgirlsDeflowered": [
                "Int",
                ""
            ],
            "tigersharksDeflowered": [
                "Int",
                ""
            ],
            "daysSinceAneFight": [
                "Int",
                ""
            ],
            "tonguedButt": [
                "Boolean",
                ""
            ],
            "kidDick": [
                "Boolean",
                ""
            ],
            "lezDemonstration": [
                "Boolean",
                ""
            ],
            "sparred": [
                "Boolean",
                ""
            ],
            "previousVictoryTeased": [
                "Boolean",
                ""
            ],
            "lastNightmare": [
                "Number",
                ""
            ],
            "izmaMorning": [
                "Boolean",
                ""
            ],
            "gaveBooks": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addInt('daysSinceAneFight', 0, '')
        self.addBoolean('gaveBooks', False, '')
        self.addBoolean('izmaMorning', False, '')
        self.addBoolean('kidDick', False, '')
        self.addFloat('lastNightmare', 0, '')
        self.addBoolean('lezDemonstration', False, '')
        self.addBoolean('previousVictoryTeased', False, '')
        self.addInt('sharkgirlsDeflowered', 0, '')
        self.addBoolean('sparred', False, '')
        self.addInt('tigersharksDeflowered', 0, '')
        self.addBoolean('tonguedButt', False, '')
