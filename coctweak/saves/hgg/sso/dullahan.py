from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class DullahanSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('dullahan')
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/Dullahan.as: public class Dullahan extends Monster { @ 9H55lJ7B90VN9Vg5ZkeoZdDj9iL8tY3oy4EGetqbAC91T2e04R9f688iR9ImdCM8UO6w57Jg4SGf8y43TcCJ93SaIq9VW3Vx
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/DullahanScene.as: public class DullahanScene extends BaseContent implements Encounter, SelfSaving, SelfDebug, TimeAwareInterface { @ ckXfhM6TLcdq5y41rmcAb4LbaXa8iDfqw2zCcunc0i19F4hR26f4F52sKaGR8zM7UpemwaSC8Ps6Zq4LJb64eOo7Ck4Dy9Nj
        '''
        {
            "rimmingProgress": [
                "IntList",
                "",
                [
                    {
                        "label": "Unavailable",
                        "data": 0
                    },
                    {
                        "label": "Available",
                        "data": 1
                    },
                    {
                        "label": "Done",
                        "data": 2
                    }
                ]
            ],
            "seenBody": [
                "Boolean",
                ""
            ],
            "encounterDay": [
                "Int",
                ""
            ],
            "talkedAboutCurse": [
                "Boolean",
                ""
            ]
        }
        '''
        # 1 = available, 2 = done before
        self.addChoices('rimmingProgress', 0, '', {
            0: 'Unavailable',
            1: 'Available',
            2: 'Done',
        })
        self.addBoolean('seenBody', False, '')
        self.addBoolean('talkedAboutCurse', False, '')
