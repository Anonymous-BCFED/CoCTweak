from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class SophieBimboSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('sophiebimbo')
        ## from [HGG]/classes/classes/Scenes/NPCs/SophieBimbo.as: public class SophieBimbo extends NPCAwareContent implements SelfSaving, SelfDebug { @ aBe6mRbqv1fL1zo2Qzbmb2Wr8oMdwCcSG13r3wwaQ62ql1yDegy4I6cnM89O9Lb1pe0JMg4ZcYqgXF5wEfdy8f20ND4MrerN
        '''
        {
            "cuddlingDaughter": [
                "Boolean",
                ""
            ],
            "daughterRocked": [
                "Int",
                "Tracks the day you've rocked a harpy daughter to sleep"
            ],
            "daughterMarried": [
                "Int",
                "Tracks the most recent daughter you've done a play marriage with"
            ]
        }
        '''
        self.addBoolean('cuddlingDaughter', False, '')
        # Tracks the most recent daughter you've done a play marriage with
        self.addInt('daughterMarried', 0, '')
        # Date rocked
        self.addInt('daughterRocked', 0, "Tracks the day you've rocked a harpy daughter to sleep")
