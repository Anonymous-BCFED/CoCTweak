from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
from enum import IntEnum
class EmberNewbornGender(IntEnum):
    NONE = 0
    MALE = 1
    FEMALE = 2
    HERM = 3

class EmberSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('ember')
        ## from [HGG]/classes/classes/Scenes/NPCs/Ember.as: public class Ember extends Monster { @ dtg1dUab39y6dkk1vDd9xanKgF13vn5fR04I2q9fXEb3E6r63OqcsS99IfuJbtY6eh30PfegbBTe8tbQA3yO9YR82w1DC6QX
        ## from [HGG]/classes/classes/Scenes/NPCs/EmberScene.as: public class EmberScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface, VaginalPregnancy { @ c3o44hfob56M4Y9c8f3MhglJdjjfPL0Sxabh0V103vdNs49Y7nD26oexndmk8dggnKd97fHw0eRg2B6uw0iJb3Ggm9dQt9F4
        '''
        {
            "flowerExplained": [
                "Boolean",
                ""
            ],
            "newbornGender": [
                "IntList",
                "",
                [
                    {
                        "label": "None",
                        "data": 0
                    },
                    {
                        "label": "Male",
                        "data": 1
                    },
                    {
                        "label": "Female",
                        "data": 2
                    },
                    {
                        "label": "Herm",
                        "data": 3
                    }
                ]
            ],
            "birthTime": [
                "Int",
                ""
            ],
            "learnedFeeding": [
                "Boolean",
                ""
            ],
            "eggArray": [
                "Array",
                "",
                [
                    "int",
                    ""
                ]
            ],
            "hatchedToday": [
                "Int",
                ""
            ],
            "tuckedToday": [
                "Int",
                ""
            ]
        }
        '''
        self.addInt('birthTime', 0, 'When child was born')
        self.addArray('eggArray', [], 'Eggs')
        self.addBoolean('flowerExplained', False, 'Whether the flower was explained')
        self.addInt('hatchedToday', 0, 'How many kids were hatched today')
        self.addBoolean('learnedFeeding', False, 'Whether Ember learned to feed or not')
        self.addChoices('newbornGender', 0, 'Gender of the newborn.', {
            0: 'None',
            1: 'Male',
            2: 'Female',
            3: 'Herm',
        })
        self.addInt('tuckedToday', 0, 'How many kids were tucked in today')
