from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class CampSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('camp')
        ## from [HGG]/classes/classes/Scenes/Camp.as: public class Camp extends NPCAwareContent implements SelfSaving, SelfDebug { @ aTq6IR2rr5Zhbsz7DYbbN3yf0TadI50i51z69VI3Wx9WQ1v9gkV8YifPR6NYd3KaPc5tKem4e1C43cfnE7Bn40egpF8ng3UO
        '''
        {
            "skyDamage": [
                "Int",
                "Amount of damage done to the sky"
            ],
            "dummyBuilt": [
                "Boolean",
                "Is the dummy built?"
            ],
            "dummyGender": [
                "IntList",
                "The dummy's gender",
                [
                    {
                        "label": "None",
                        "data": 0
                    },
                    {
                        "label": "Male",
                        "data": 1
                    },
                    {
                        "label": "Female",
                        "data": 2
                    },
                    {
                        "label": "Herm",
                        "data": 3
                    }
                ]
            ],
            "dummyName": [
                "String",
                ""
            ],
            "storageMoveAll": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('dummyBuilt', False, 'Is the dummy built?')
        self.addChoices('dummyGender', 0, "The dummy's gender", {
            0: 'None',
            1: 'Male',
            2: 'Female',
            3: 'Herm',
        })
        self.addStr('dummyName', '""', '')
        self.addInt('skyDamage', 0, 'Amount of damage done to the sky')
        self.addBoolean('storageMoveAll', False, '')
