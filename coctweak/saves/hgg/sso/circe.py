from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class CirceSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('circe')
        ## from [HGG]/classes/classes/Scenes/Areas/VolcanicCrag/CorruptedCoven.as: public class CorruptedCoven extends BaseContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 7SKd2Jff5fXCezZ7mkdEk6GT9EW7PqgNm7DjcjZ1tqabv5eR7ICgeQ7nT234faL4fy4itcJV0X336Y8Mv3LleOT4WNay8diS
        '''
        {
            "metCirceAsGrossInsectPerson": [
                "Boolean",
                ""
            ],
            "sippedWineWithCirce": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('metCirceAsGrossInsectPerson', False, '')
        self.addBoolean('sippedWineWithCirce', False, '')
