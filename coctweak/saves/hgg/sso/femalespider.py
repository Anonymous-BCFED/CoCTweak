from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class FemaleSpiderSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('femalespidermorph')
        ## from [HGG]/classes/classes/Scenes/Areas/Swamp/FemaleSpiderMorphScene.as: public class FemaleSpiderMorphScene extends BaseContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 7Yg1UD6xJd2T0Y729abi8dsO5K12GmchPd6scbX7m95VI4ztbBG2IBfQb7J09L78Xg6NkfPZ8Bm8UY8ycgBA6NT7ae9Cc9CF
        '''
        {
            "spidersKilled": [
                "Int",
                ""
            ]
        }
        '''
        self.addInt('spidersKilled', 0, '')
