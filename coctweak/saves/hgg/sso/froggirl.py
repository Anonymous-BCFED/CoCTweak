from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class FrogGirlSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('froggirl')
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/FrogGirlScene.as: public class FrogGirlScene extends BaseContent implements VaginalPregnancy, AnalPregnancy, SelfSaving, SelfDebug, Encounter, TimeAwareInterface { @ 7mGebg0Ka3OqgGf6ZB5Ji9m8eNSgWTftCfil7gRbtjg5we3d3V956T1m0fUQ8bJbTE5m743qd5t4Qy3P0gEpc7JfKxcWK613
        self.addInt('analMad', 0, '')
        self.addInt('angryLosses', 0, '')
        self.addInt('eggCount', 0, '')
        self.addBoolean('fought', False, '')
        self.addInt('lastEncounter', 0, '')
        self.addBoolean('metSuwako', False, '')
        self.addBoolean('pattyCake', False, '')
        self.addChoices('shownKids', 0, '', {
            0: 'N/A',
            1: 'Shown',
            2: 'Accessed Menu',
        })
        self.addBoolean('submissive', False, '')
        self.addChoices('taughtLesson', 0, '', {
            0: 'N/A',
            1: 'Taught',
            2: 'Frightened',
            3: 'Killed',
        })
