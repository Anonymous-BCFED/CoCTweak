from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
from enum import IntEnum
class AmilyMetKids(IntEnum):
    NOT_MET = 0
    MET = 1
    IGNORED = -1
class AmilySSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('amily')
        ## from [HGG]/classes/classes/Scenes/NPCs/AmilyScene.as: public class AmilyScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 8Cz8x6do2esI7oh04N1bi19i6QYfNV1mkcF24BA3eWfGC8Au6CA3DT3GA1qQbl14kydDGacB7pc5j75U8cDQdUW3YucjX4xX
        '''
        {
            "ringType": [
                "String",
                ""
            ],
            "amilyMorning": [
                "Boolean",
                ""
            ],
            "metKids": [
                "IntList",
                "",
                [
                    {
                        "label": "",
                        "data": 0
                    },
                    {
                        "label": "Met",
                        "data": 1
                    },
                    {
                        "label": "Ignored",
                        "data": -1
                    }
                ]
            ],
            "giftedCClothes": [
                "Boolean",
                ""
            ],
            "impSkullsCount": [
                "Int",
                "Skulls that Amily has added to the wall."
            ],
            "pcKnowsAboutSkulls": [
                "Boolean",
                "Amily told player about the skulls."
            ]
        }
        '''
        self.addBoolean('amilyMorning', False, 'Wake up with Amily in the morning?')
        self.addBoolean('giftedCClothes', False, 'Gifted comfy clothes?')
        self.addInt('impSkullsCount', 0, 'Skulls that Amily has added to the wall.')
        self.addChoices('metKids', 0, "What kind of meeting did you have with Amily's kids?", {
            -1: 'Ignored',
            0: 'Did not meet kids yet',
            1: 'Met',
        })
        self.addBoolean('pcKnowsAboutSkulls', False, 'Player knows about the skulls?')
        self.addChoices('ringType', '""', 'What kind of ring Amily wears.', {
            'silver': 'silver',
            'gold': 'gold',
            'platinum': 'platinum',
            'diamond': 'diamond',
        })
