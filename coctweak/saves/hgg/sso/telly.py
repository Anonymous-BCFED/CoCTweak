from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class TellySSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 3))
    def __init__(self) -> None:
        super().__init__('telly')
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/Telly.as: public class Telly extends BazaarAbstractContent implements SelfSaving, SelfDebug { @ 1rt4K10OF6DPbHd5flbJb0yxe6FeXi4i2c83a8Y91g2o33xUd88aT64No3wIf3ZgcKdrh33T3vL61Zdplg265x56Ef1J5cJb
        '''
        {
            "debugSortOrder": [
                "tellyGenesis",
                "noncommiTelly",
                "tellyCommute",
                "tellyTubby",
                "tellyGraph",
                "tellyCardiogram",
                "tellyTimer",
                "tellyGram",
                "tellyCommand",
                "tellyPlasmed",
                "vesTelly",
                "tasTelly",
                "experimenTelly",
                "tellyComL",
                "tellyComB",
                "tellyComK",
                "tellyComP",
                "tellyComA",
                "tellyComH",
                "tellyComD",
                "tellyOphile"
            ],
            "tellyGenesis": [
                "Boolean",
                "Met Telly in the bazaar"
            ],
            "noncommiTelly": [
                "Boolean",
                "Seen Telly's wagon while exploring"
            ],
            "tellyCommute": [
                "Boolean",
                "Met Telly while exploring"
            ],
            "tellyTubby": [
                "String",
                "Facepaint color"
            ],
            "tellyGraph": [
                "String",
                "Facepaint type"
            ],
            "tellyCardiogram": [
                "Int",
                "Tracks what Telly is doing in the shop"
            ],
            "tellyTimer": [
                "Int",
                "Time of last random rolls"
            ],
            "tellyGram": [
                "Int",
                "Number of times chatted per visit"
            ],
            "tellyCommand": [
                "Int",
                "Tracks the day that Telly is disabled for"
            ],
            "tellyPlasmed": [
                "Boolean",
                "Tracks if you've bought candy"
            ],
            "vesTelly": [
                "Int",
                "Tracks if you've hugged Telly this hour"
            ],
            "tasTelly": [
                "Int",
                "Tracks this stupid fucking peach"
            ],
            "experimenTelly": [
                "Int",
                "Tracks Drake's Heart stuff"
            ],
            "tellyComL": [
                "Boolean",
                "Asked about selling Liddellium"
            ],
            "tellyComB": [
                "Boolean",
                "Gave bear"
            ],
            "tellyComK": [
                "Boolean",
                "Kitsuned fluffy tail"
            ],
            "tellyComP": [
                "Boolean",
                "Gave peach"
            ],
            "tellyComA": [
                "Boolean",
                "Gave abyssal shard"
            ],
            "tellyComH": [
                "Boolean",
                "Unlocked hugging"
            ],
            "tellyComD": [
                "Boolean",
                "Gave Drake's Heart"
            ],
            "tellyOphile": [
                "Object",
                "Tracks which chats you've seen",
                {
                    "debugSortOrder": [
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6",
                        "7",
                        "8",
                        "9",
                        "10",
                        "11",
                        "12",
                        "13",
                        "14",
                        "15",
                        "16",
                        "17",
                        "18",
                        "19",
                        "20",
                        "21",
                        "22",
                        "23"
                    ],
                    "1": [
                        "boolean",
                        ""
                    ],
                    "2": [
                        "boolean",
                        ""
                    ],
                    "3": [
                        "boolean",
                        ""
                    ],
                    "4": [
                        "boolean",
                        ""
                    ],
                    "5": [
                        "boolean",
                        ""
                    ],
                    "6": [
                        "boolean",
                        ""
                    ],
                    "7": [
                        "boolean",
                        ""
                    ],
                    "8": [
                        "boolean",
                        ""
                    ],
                    "9": [
                        "boolean",
                        ""
                    ],
                    "10": [
                        "boolean",
                        ""
                    ],
                    "11": [
                        "boolean",
                        ""
                    ],
                    "12": [
                        "boolean",
                        ""
                    ],
                    "13": [
                        "boolean",
                        ""
                    ],
                    "14": [
                        "boolean",
                        ""
                    ],
                    "15": [
                        "boolean",
                        ""
                    ],
                    "16": [
                        "boolean",
                        ""
                    ],
                    "17": [
                        "boolean",
                        ""
                    ],
                    "18": [
                        "boolean",
                        ""
                    ],
                    "19": [
                        "boolean",
                        ""
                    ],
                    "20": [
                        "boolean",
                        ""
                    ],
                    "21": [
                        "boolean",
                        ""
                    ],
                    "22": [
                        "boolean",
                        ""
                    ],
                    "23": [
                        "boolean",
                        ""
                    ]
                }
            ]
        }
        '''
        self.addInt('experimenTelly', 0, "Tracks Drake's Heart stuff")
        self.addInt('tasTelly', 0, 'Tracks this stupid fucking peach')
        self.addInt('tellyCardiogram', 0, 'Tracks what Telly is doing in the shop')
        self.addBoolean('tellyComA', False, 'Gave abyssal shard')
        self.addBoolean('tellyComB', False, 'Gave bear')
        self.addBoolean('tellyComD', False, "Gave Drake's Heart")
        self.addBoolean('tellyComH', False, 'Unlocked hugging')
        self.addBoolean('tellyComK', False, 'Kitsuned fluffy tail')
        self.addBoolean('tellyComL', False, 'Asked about selling Liddellium')
        self.addBoolean('tellyComP', False, 'Gave peach')
        self.addInt('tellyCommand', 0, 'Tracks if Telly is disabled for the day (by setting it to the current day)')
        self.addBoolean('tellyGenesis', False, 'met Telly')
        self.addInt('tellyGram', 0, 'Number of times chatted per visit')
        self.addStr('tellyGraph', 'butterfly', 'Facepaint type')
        self.addObject('tellyOphile', {1: False, 2: False, 3: False, 4: False, 5: False, 6: False, 7: False, 8: False, 9: False, 10: False, 11: False, 12: False, 13: False, 14: False, 15: False, 16: False, 17: False, 18: False, 19: False, 20: False, 21: False, 22: False}, "Chats you've had with her")
        self.addBoolean('tellyPlasmed', False, "Tracks if you've bought candy")
        self.addInt('tellyTimer', 0, 'Time of last random rolls')
        self.addStr('tellyTubby', 'purple', 'Facepaint color')
        self.addInt('vesTelly', 0, "Tracks if you've hugged Telly this hour")
