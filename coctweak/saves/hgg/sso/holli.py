from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class HolliSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('holli')
        ## from [HGG]/classes/classes/Scenes/NPCs/HolliScene.as: public class HolliScene extends NPCAwareContent implements SelfSaving, SelfDebug { @ 9NhfJPa4BgDFaob33v8LP1N0gqA6PC4Y6bKX3lb1iu42Q8yj2Yt0L67Mp1or7iu62Hal99dvaOO4IM9U4bArg8AgkjeXlaGV
        '''
        {
            "askedGlades": [
                "Boolean",
                ""
            ],
            "growingGlades": [
                "Boolean",
                ""
            ],
            "fruitsEaten": [
                "Int",
                ""
            ]
        }
        '''
        self.addBoolean('askedGlades', False, '')
        self.addInt('fruitsEaten', 0, '')
        self.addBoolean('growingGlades', False, '')
