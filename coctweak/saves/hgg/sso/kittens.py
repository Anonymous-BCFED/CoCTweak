from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class KittensSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('kittens')
        ## from [HGG]/classes/classes/Scenes/Places/TelAdre/Kittens.as: public class Kittens extends BaseContent implements SelfSaving, SelfDebug { @ 2Id1J81YscOxh168q2fS400NdLE5OUbdkfyq3MR01vcuCgnVh1O83D9cp7A48p95FW3AvckO0dgghv1BYaRQ8KEgMBbQxbFV
        '''
        {
            "metKittens": [
                "Boolean",
                ""
            ],
            "kittensResult": [
                "BitFlag",
                [],
                [
                    "Unused",
                    "Helped",
                    "Left",
                    "Taken",
                    "Defiant",
                    "Obedient",
                    "Watched",
                    "Spanked"
                ]
            ],
            "disabledDate": [
                "Int",
                ""
            ]
        }
        '''
        # Date of when Tel'Adre is reenabled
        self.addInt('disabledDate', 0, '')
        self.addInt('kittensResult', 0, '', bitflags={
            1: 'Unused',
            2: 'Helped',
            4: 'Left',
            8: 'Taken',
            16: 'Defiant',
            32: 'Obedient',
            64: 'Watched',
            128: 'Spanked',
        })
        self.addBoolean('metKittens', False, '')
