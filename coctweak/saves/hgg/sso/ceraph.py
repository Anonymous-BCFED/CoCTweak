from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
from enum import IntEnum
class ECeraphRapePlay(IntEnum):
    NONE = 0
    IMPS_YES = 1
    IMPS_NO = 2
class CeraphSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('ceraph')
        ## from [HGG]/classes/classes/Scenes/NPCs/CeraphFollowerScene.as: public class CeraphFollowerScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 8d70krg2DdeCdap5V0bmH6dH9utfus9TDga0gD62xI62rgmwfAZ31Z9fD53s2Mm8OM13C8S1h2kd1f2vGe1I8X5h014T3df9
        ## from [HGG]/classes/classes/Scenes/NPCs/CeraphScene.as: public class CeraphScene extends NPCAwareContent { @ 8ra28bb85dtsgYdaNwgRs4Ag19w1aPgkW0Sj9xGaz31GT9Z40vNe3yggMd8odOZ3MPdAYfe3fneaj29I7gz78If7LTdEmaIy
        '''
        {
            "rapePlay": [
                "IntList",
                "",
                [
                    {
                        "label": "Never done",
                        "data": 0
                    },
                    {
                        "label": "Yes Imps",
                        "data": 1
                    },
                    {
                        "label": "No Imps",
                        "data": 2
                    }
                ]
            ],
            "roleplayAsTellyCount": [
                "Int",
                "Times roleplayed as Telly"
            ]
        }
        '''
        # 0 = never done, 1 = continued impregnation, 2 = impregnation disabled
        self.addChoices('rapePlay', 0, 'How rape play was handled', {
            0: 'Never done',
            1: 'Yes Imps',
            2: 'No Imps',
        })
        self.addInt('roleplayAsTellyCount', 0, 'Times roleplayed as Telly')
