from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class JojoSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 2))
    def __init__(self) -> None:
        super().__init__('jojo')
        ## from [HGG]/classes/classes/Scenes/NPCs/JojoScene.as: public class JojoScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 85E5cF3jr1Ow67JdKLgEG0nUapj3XI5BL984ce94rI6KP9Ig9cS7rJ8AVc4ye2ebI5fFs27E1tM7KgaZab4jae5dGs8ZvbMs
        '''
        {
            "forgiven": [
                "Boolean",
                "Tracks whether he has forgiven you for trying to rape him"
            ],
            "postRapeCampOffer": [
                "Boolean",
                "Tracks whether you've tried to get him to come to camp"
            ]
        }
        '''
        # Tracks whether he has forgiven you for trying to rape him
        self.addBoolean('forgiven', False, 'Tracks whether he has forgiven you for trying to rape him')
        self.addBoolean('postRapeCampOffer', False, "Tracks whether you've tried to get him to come to camp")
