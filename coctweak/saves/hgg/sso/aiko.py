from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class AikoSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('aiko')
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/Aiko.as: public class Aiko extends BaseKitsune { @ eYFg1V3v996TdSN85b0JVanN3uD8xQ46w9t17uh9Wme7Eb0mfHA2Q52qaeM45VAfczazz5YcdGmcLO28e0Yu5AZ5Gn0MM9hL
        ## from [HGG]/classes/classes/Scenes/Areas/Forest/AikoScene.as: public class AikoScene extends BaseContent implements Encounter, SelfSaving, SelfDebug, TimeAwareInterface { @ 2hv2VEaPz2fie3LaPfgYLaRa8GU3xr9ujfjSfmOake3eOeoJ8ns7frfqT0T09QBcOj8HCcyp7bGeCV6Zserr5aI3Bh8xBa4j
        '''
        {
            "akbalDone": [
                "Boolean",
                ""
            ]
        }
        '''
        self.addBoolean('akbalDone', False, '')
