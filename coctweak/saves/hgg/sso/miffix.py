from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class MiffixSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('miffix')
        ## from [HGG]/classes/classes/Scenes/Places/Bazaar/MiffixScene.as: public class MiffixScene extends BaseContent implements SelfSaving, TimeAwareInterface { @ 5DveVq6OA8AE7Wlg4Weh20jXg93g89gqHeiu3ft7tQcnf8LU83bgmbbLBa3T6sBc0AcKog0y2qC6O9doua9obj22Mm1h92ln
        self.addUnused('beatDemonfist', False, 'Someone did some crappy copypasta.')
        self.addInt('donatedToMiffix', 0, '')
        self.addBoolean('killedMiffix', False, '')
        self.addUnused('learnedOfDemonFist', False, 'Someone did some crappy copypasta.')
        self.addBoolean('metMiffixPost', False, '')
        self.addUnused('playerName', "", 'Someone did some crappy copypasta.')
        self.addInt('timeUntilLegsBroken', 0, '')
        self.addInt('timeUntilPlan', 0, '')
        self.addUnused('timesLost', 0, 'Someone did some crappy copypasta.')
        self.addBoolean('toldPlan', False, '')
