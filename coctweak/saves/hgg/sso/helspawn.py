from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class HelSpawnSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('helspawn')
        ## from [HGG]/classes/classes/Scenes/NPCs/HelSpawnScene.as: public class HelSpawnScene extends NPCAwareContent implements SelfSaving, SelfDebug { @ dnte5rfxZ5zM5dS4RU4Wu0ur9623Xo6J2fw0dgk4IAdJ49Bd3DzbrscOm95F4o1a7s6gN1ft8fv7Hl9JSbqBe0R2Nz9X2a9L
        '''
        {
            "footjob": [
                "Boolean",
                ""
            ],
            "sippedAnemone": [
                "Boolean",
                ""
            ],
            "wakeUp": [
                "Boolean",
                ""
            ],
            "surprised": [
                "Boolean",
                ""
            ],
            "fishing": [
                "Boolean",
                ""
            ],
            "boyfriend": [
                "Boolean",
                "Your daugther has his spiderboyfriend?"
            ],
            "alexThreesome": [
                "Boolean",
                "Have done already a threesome with your daughter and his boyfriend?"
            ],
            "alexAnalVirginity": [
                "Boolean",
                "The butt of the spider is untouched?"
            ]
        }
        '''
        self.addBoolean('alexAnalVirginity', True, 'The butt of the spider is untouched?')
        self.addBoolean('alexThreesome', False, 'Have done already a threesome with your daughter and his boyfriend?')
        self.addBoolean('boyfriend', False, 'Your daugther has his spiderboyfriend?')
        self.addBoolean('fishing', False, '')
        self.addBoolean('footjob', False, '')
        self.addBoolean('sippedAnemone', False, '')
        self.addBoolean('surprised', False, '')
        self.addBoolean('wakeUp', False, '')
