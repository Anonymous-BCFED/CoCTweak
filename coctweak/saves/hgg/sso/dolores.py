from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class DoloresSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('dolores')
        ## from [HGG]/classes/classes/Scenes/Places/MothCave/DoloresScene.as: public class DoloresScene extends NPCAwareContent implements SelfSaving, SelfDebug, TimeAwareInterface { @ 3gndv0a1QgEradrfvkg5H4xFe0B1NX0vigbegz1dO558w6uhdHj9aGed77Am8Tl9cZ9lT0ivc2f7iF4GO0wB83B4K4dLo049
        '''
        {
            "doloresProgress": [
                "IntList",
                "Tracks Dolores's overall progress",
                [
                    {
                        "label": "Unborn",
                        "data": 0
                    },
                    {
                        "label": "Born",
                        "data": 1
                    },
                    {
                        "label": "Post-birth",
                        "data": 2
                    },
                    {
                        "label": "Toys",
                        "data": 3
                    },
                    {
                        "label": "Talking",
                        "data": 4
                    },
                    {
                        "label": "Magic",
                        "data": 5
                    },
                    {
                        "label": "Ran Away",
                        "data": 6
                    },
                    {
                        "label": "Cocooned",
                        "data": 7
                    },
                    {
                        "label": "In Cocoon",
                        "data": 8
                    },
                    {
                        "label": "Hatched",
                        "data": 9
                    },
                    {
                        "label": "Wings",
                        "data": 10
                    },
                    {
                        "label": "Concerns",
                        "data": 11
                    },
                    {
                        "label": "Summoning",
                        "data": 12
                    },
                    {
                        "label": "Debriefed",
                        "data": 13
                    },
                    {
                        "label": "Sketched",
                        "data": 14
                    },
                    {
                        "label": "Tapestry",
                        "data": 15
                    },
                    {
                        "label": "HikkiQuest",
                        "data": 16
                    },
                    {
                        "label": "HikkiDone",
                        "data": 17
                    }
                ]
            ],
            "doloresTimeSinceEvent": [
                "Int",
                "Tracks how many hours it's been since an event has fired"
            ],
            "doloresDecision": [
                "IntList",
                "Tracks whether you let her keep the book",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Kept",
                        "data": 1
                    },
                    {
                        "label": "Taken",
                        "data": 2
                    },
                    {
                        "label": "Waiting",
                        "data": 3
                    }
                ]
            ],
            "doloresAmbitions": [
                "IntList",
                "Tracks some progress that's parallel to the main one",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Magic Talk",
                        "data": 1
                    },
                    {
                        "label": "Ambitions",
                        "data": 2
                    }
                ]
            ],
            "doloresFinal": [
                "Int",
                "Tracks results of her final scene"
            ],
            "hikkiQuest": [
                "BitFlag",
                [
                    "Tracks progress on her personal quest"
                ],
                [
                    "Camp",
                    "Marielle",
                    "Bazaar",
                    "Library",
                    "Circe",
                    "Sex",
                    "Finished",
                    "Bought Robe",
                    "Freed Her",
                    "Solved Riddle",
                    "Killed Demon"
                ]
            ],
            "doloresSex": [
                "IntList",
                "Tracks whether you've unlocked sex and/or comforted her",
                [
                    {
                        "label": "N/A",
                        "data": 0
                    },
                    {
                        "label": "Unlocked",
                        "data": 1
                    },
                    {
                        "label": "Comforted",
                        "data": 2
                    },
                    {
                        "label": "Uncomforted",
                        "data": 3
                    },
                    {
                        "label": "Comforted Sex",
                        "data": 4
                    }
                ]
            ],
            "doloresTimesLeft": [
                "Int",
                "Tracks how many times you've been a deadbeat [dad]"
            ],
            "doloresAngry": [
                "Boolean",
                "Tracks whether you've upset her somehow, disabling her for the current encounter"
            ],
            "doloresBlowjob": [
                "Boolean",
                "Tracks whether you've been blown"
            ],
            "doloresBooks": [
                "Boolean",
                "Tracks whether you've brought her books from the manor"
            ]
        }
        '''
        # tracks some progress that's parallel to the main one
        self.addChoices('doloresAmbitions', 0, "Tracks some progress that's parallel to the main one", {
            0: 'N/A',
            1: 'Magic Talk',
            2: 'Ambitions',
        })
        # tracks whether you've upset her
        self.addBoolean('doloresAngry', False, "Tracks whether you've upset her somehow, disabling her for the current encounter")
        # tracks whether you've been blown
        self.addBoolean('doloresBlowjob', False, "Tracks whether you've been blown")
        # tracks whether you've brought her books from the manor
        self.addBoolean('doloresBooks', False, "Tracks whether you've brought her books from the manor")
        # tracks whether you let her keep the book
        self.addChoices('doloresDecision', 0, 'Tracks whether you let her keep the book', {
            0: 'N/A',
            1: 'Kept',
            2: 'Taken',
            3: 'Waiting',
        })
        # tracks results of her final scene
        self.addInt('doloresFinal', 0, 'Tracks results of her final scene')
        # 1 = alive, 2-5 = childhood events, 6-8 = cocoon events, 9+ = teen, 16 = hikkiquest, 17 = done (for now)
        self.addChoices('doloresProgress', 0, "Tracks Dolores's overall progress", {
            0: 'Unborn',
            1: 'Born',
            2: 'Post-birth',
            3: 'Toys',
            4: 'Talking',
            5: 'Magic',
            6: 'Ran Away',
            7: 'Cocooned',
            8: 'In Cocoon',
            9: 'Hatched',
            10: 'Wings',
            11: 'Concerns',
            12: 'Summoning',
            13: 'Debriefed',
            14: 'Sketched',
            15: 'Tapestry',
            16: 'HikkiQuest',
            17: 'HikkiDone',
        })
        # tracks whether you've unlocked sex and/or comforted her
        self.addChoices('doloresSex', 0, "Tracks whether you've unlocked sex and/or comforted her", {
            0: 'N/A',
            1: 'Unlocked',
            2: 'Comforted',
            3: 'Uncomforted',
            4: 'Comforted Sex',
        })
        # tracks how many hours it's been since an event has fired
        self.addInt('doloresTimeSinceEvent', 0, "Tracks how many hours it's been since an event has fired")
        # tracks how many times you've been a deadbeat [dad]
        self.addInt('doloresTimesLeft', 0, "Tracks how many times you've been a deadbeat [dad]")
        # tracks progress on her personal questsomehow
        self.addInt('hikkiQuest', 0, 'Tracks progress on her personal quest')
