from coctweak.saves._selfsaving import SelfSavingObject
from coctweak.saves.hgg.serialization import HGGSerializationVersion
class MilkWaifuSSO(SelfSavingObject):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self) -> None:
        super().__init__('milkWaifu')
        ## from [HGG]/classes/classes/Scenes/NPCs/MilkWaifu.as: public class MilkWaifu extends NPCAwareContent implements SelfSaving, SelfDebug { @ 6rOgRz0aBbw09PCc3C7Lf1d3biB06B8tR8IXgYCfj99JK1Sn9QF1LP8CW82e7oYebL4KL0VOgk2bI4bxh1Oe6KP0skf4F6gw
        '''
        {
            "talkedSlut": [
                "IntList",
                "",
                [
                    {
                        "label": "0",
                        "data": 0
                    },
                    {
                        "label": "1",
                        "data": 1
                    },
                    {
                        "label": "2",
                        "data": 2
                    },
                    {
                        "label": "3",
                        "data": 3
                    }
                ]
            ]
        }
        '''
        self.addChoices('talkedSlut', 0, '', {
            0: '0',
            1: '1',
            2: '2',
            3: '3',
        })
