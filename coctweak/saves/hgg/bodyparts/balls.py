from coctweak.saves.hgg.bodyparts._serialization.balls import HGGRawBalls
from typing import List, Optional
import random
from typing import Dict
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.enums.skintypes import HGGSkinType

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class HGGBalls(HGGRawBalls):
    def getDisplayText(self) -> str:
        return super().getDisplayText() + f', {self.hoursSinceCum} hours since last orgasm'

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Hours Since Cum'] = self.hoursSinceCum
        return o

    def getLongDesc(self, forcedSize: bool=True, plural: Optional[bool]=None, inclArticle: bool=False) -> str:
        save = self.save
        ## from [HGG]/classes/classes/Appearance.as: public static function ballsDescription(i_forcedSize:Boolean, i_plural:Boolean, i_creature:Creature, i_withArticle:Boolean = false):String { @ 308c5x4qq93E9kMgo84tefTGaz54XjcFiblmgI273mdyO9CwbFR2CR9Xu7Q60Xi1qM5Mx3D83BV9uJdvjaOt4yden159i5IK
        # This is pretty poorly written, so I'll be making some changes.

        if plural is None:
            plural = self.count > 1

        a: str = '' # consonant form
        an: str = '' # vowel form

        if inclArticle:
            a: str = 'a '
            an: str = 'an '

        #if (i_creature.balls === 0) return "prostate";
        if self.count == 0:
            return a+'prostate'

        #var description:String = "";
        description: str = ''
        #var options:Array;
        options: List[str] = []

        #if (i_plural && (!i_creature.hasStatusEffect(StatusEffects.Uniball))) {
        if plural and (not save.hasStatusEffect(HGGStatusLib.Uniball)):
            #if (i_creature.balls === 1) {
            if self.count == 1:
                #if (i_withArticle) {
                #    options = ["a single", "a solitary", "a lone", "an individual"];
                #}
                #else {
                #    options = ["single", "solitary", "lone", "individual"];
                #}
                #description += randomChoice(options);
                description += random.choice([
                    a+'single',
                    a+'solitary',
                    a+'lone',
                    an+'individual',
                ])
            #}
            #else if (i_creature.balls === 2) {
            elif self.count == 2:
                #if (i_withArticle) {
                #    options = ["a pair of", "two", "a duo of"];
                #}
                #else {
                #    options = ["pair of", "two", "duo of"];
                #}
                #description += randomChoice(options);
                description += random.choice([
                    a+'pair of',
                    'two',
                    a+'duo of',
                ])
            #}
            #else if (i_creature.balls === 3) {
            elif self.count == 3:
                #options = ["three", "triple"];
                #(i_withArticle) ? options.push("a trio of") : options.push("trio of");
                #description += randomChoice(options);
                description += random.choice([
                    'three',
                    'triple',
                    a+'trio of',
                ])
            #}
            #else if (i_creature.balls === 4) {
            elif self.count == 4:
                #options = ["four", "quadruple"];
                #(i_withArticle) ? options.push("a quartette of") : options.push("quartette of");
                #description += randomChoice(options);
                description += random.choice([
                    'four',
                    'quadruple',
                    a+'quartette of',
                ])
            #}
            #else {
            else:
                #if (i_withArticle) {
                #    options = ["a multitude of", "many", "a large handful of"];
                #}
                #else {
                #    options = ["multitude of", "many", "large handful of"];
                #}
                #description += randomChoice(options);
                description += random.choice([
                    a+'multitude of',
                    'many',
                    a+'large handful of',
                ])
            #}
        #}
        #//size!
        #if (rand(3) <= 1 || i_forcedSize) {
        if forcedSize or random.randint(0, 3) <= 1:
            #if (description) description += " ";
            if description != '':
                description += ' '

            #if (i_creature.ballSize >= 18) description += "hideously swollen and oversized";
            if self.size >= 18:
                description += 'hideously swollen and oversized'
            #else if (i_creature.ballSize >= 15) description += "beachball-sized";
            elif self.size >= 15:
                description += 'beachball-sized'
            #else if (i_creature.ballSize >= 12) description += "watermelon-sized";
            elif self.size >= 12:
                description += 'watermelon-sized'
            #else if (i_creature.ballSize >= 9) description += "basketball-sized";
            elif self.size >= 9:
                description += 'basketball-sized'
            #else if (i_creature.ballSize >= 7) description += "soccerball-sized";
            elif self.size >= 7:
                description += 'soccerball-sized'
            #else if (i_creature.ballSize >= 5) description += "cantaloupe-sized";
            elif self.size >= 5:
                description += 'cantaloupe-sized'
            #else if (i_creature.ballSize >= 4) description += "grapefruit-sized";
            elif self.size >= 4:
                description += 'grapefruit-sized'
            #else if (i_creature.ballSize >= 3) description += "apple-sized";
            elif self.size >= 3:
                description += 'apple-sized'
            #else if (i_creature.ballSize >= 2) description += "baseball-sized";
            elif self.size >= 2:
                description += 'baseball-sized'
            #else if (i_creature.ballSize > 1) description += "large";
            elif self.size > 1:
                description += 'large'
            #else if (i_creature.ballSize < 0.5) description += "tiny";
            elif self.size < 0.5:
                description += 'tiny'
            #else if (i_creature.ballSize < 1) description += "small";
            elif self.size < 1:
                description += 'small'
        #}
        #//UNIBALL
        #if (i_creature.hasStatusEffect(StatusEffects.Uniball)) {
        if save.hasStatusEffect(HGGStatusLib.Uniball):
            #if (description) description += " ";
            if description != '':
                description += ' '
            #options = ["tightly-compressed", "snug", "cute", "pleasantly squeezed", "compressed-together"];
            #description += randomChoice(options);
            description += random.choice([
                "tightly-compressed",
                "snug",
                "cute",
                "pleasantly squeezed",
                "compressed-together",
            ])
        #}
        #//Descriptive
        #if (i_creature.hoursSinceCum >= 48 && rand(2) === 0 && !i_forcedSize) {
        if self.hoursSinceCum >= 48 and (forcedSize or random.randint(0, 2) == 0):
            #if (description) description += " ";
            if description != '':
                description += ' '
            #options = ["overflowing", "swollen", "cum-engorged"];
            #description += randomChoice(options);
            description += random.choice([
                "overflowing",
                "swollen",
                "cum-engorged"
            ])
        #}
        #//lusty
        #if (i_creature.lust100 > 90 && (description === "") && rand(2) === 0 && !i_forcedSize) {
        if save.combat_stats.lust.percent > 90 and description == '' and (forcedSize or random.randint(0, 2) == 0):
            #options = ["eager", "full", "needy", "desperate", "throbbing", "heated", "trembling", "quivering", "quaking"];
            #description += randomChoice(options);
            description += random.choice([
                "eager",
                "full",
                "needy",
                "desperate",
                "throbbing",
                "heated",
                "trembling",
                "quivering",
                "quaking",
            ])
        #}
        #//Slimy skin
        #if (i_creature.hasGooSkin()) {
        if save.skin.type == HGGSkinType.GOO:
            #if (description) description += " ";
            if description != '':
                description += ' '
            #options = ["goopy", "gooey", "slimy"];
            #description += randomChoice(options);
            description += random.choice([
                "goopy",
                "gooey",
                "slimy",
            ])
        #}
        #if (description) description += " ";
        if description != '' and description[-1] != ' ':
            description += ' '

        #options = ["nut", "gonad", "teste", "testicle", "testicle", "ball", "ball", "ball"];
        #description += randomChoice(options);
        description += random.choice([
            "nut",
            "gonad",
            "teste",
            "testicle",
            "testicle",
            "ball",
            "ball",
            "ball",
        ])
        #if (i_plural) description += "s";
        if plural:
            description += 's'

        #return description;
        return description
