# @GENERATED from coc/hgg/classes/classes/BodyParts/Skin.as
from coctweak.saves.common.bodyparts.skin import BaseSkin

__ALL__=['HGGRawSkin']

class HGGRawSkin(BaseSkin):
    PLAIN: int = 0
    FUR: int = 0
    LIZARD_SCALES: int = 0
    GOO: int = 0
    UNDEFINED: int = 0
    DRAGON_SCALES: int = 0
    FISH_SCALES: int = 0
    WOOL: int = 0
    FEATHERED: int = 0
    BARK: int = 0
    STALK: int = 0
    WOODEN: int = 0
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
