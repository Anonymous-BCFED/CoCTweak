# @GENERATED from coc/hgg/classes/classes/BodyParts/Wings.as
from coctweak.saves.common.bodyparts.wings import BaseWings

__ALL__=['HGGRawWings']

class HGGRawWings(BaseWings):
    NONE: int = 0
    BEE_LIKE_SMALL: int = 0
    BEE_LIKE_LARGE: int = 0
    HARPY: int = 0
    IMP: int = 0
    BAT_LIKE_TINY: int = 0
    BAT_LIKE_LARGE: int = 0
    SHARK_FIN: int = 0
    FEATHERED_LARGE: int = 0
    DRACONIC_SMALL: int = 0
    DRACONIC_LARGE: int = 0
    GIANT_DRAGONFLY: int = 0
    IMP_LARGE: int = 0
    FAERIE_SMALL: int = 0
    FAERIE_LARGE: int = 0
    WOODEN: int = 0
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
