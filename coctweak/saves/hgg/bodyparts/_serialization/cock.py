# @GENERATED from coc/hgg/classes/classes/Cock.as
from coctweak.saves.common.bodyparts.cock import BaseCock
from coctweak.saves.hgg.serialization import HGGSerializationVersion

__ALL__=['HGGRawCock']

class HGGRawCock(BaseCock):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    OBJECT_NOT_FOUND: int = 0
    MAX_LENGTH: float = 0.0
    MAX_THICKNESS: float = 0.0
    KNOTMULTIPLIER_NO_KNOT: float = 0.0
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
