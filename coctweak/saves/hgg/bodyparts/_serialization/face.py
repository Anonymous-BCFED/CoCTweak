# @GENERATED from coc/hgg/classes/classes/BodyParts/Face.as
from coctweak.saves.common.bodyparts.face import BaseFace

__ALL__=['HGGRawFace']

class HGGRawFace(BaseFace):
    HUMAN: int = 0
    HORSE: int = 0
    DOG: int = 0
    COW_MINOTAUR: int = 0
    SHARK_TEETH: int = 0
    SNAKE_FANGS: int = 0
    CATGIRL: int = 0
    LIZARD: int = 0
    BUNNY: int = 0
    KANGAROO: int = 0
    SPIDER_FANGS: int = 0
    FOX: int = 0
    DRAGON: int = 0
    RACCOON_MASK: int = 0
    RACCOON: int = 0
    BUCKTEETH: int = 0
    MOUSE: int = 0
    FERRET_MASK: int = 0
    FERRET: int = 0
    PIG: int = 0
    BOAR: int = 0
    RHINO: int = 0
    ECHIDNA: int = 0
    DEER: int = 0
    WOLF: int = 0
    COCKATRICE: int = 0
    BEAK: int = 0
    RED_PANDA: int = 0
    CAT: int = 0
    GNOLL: int = 0
    def __init__(self, save) -> None:
        super().__init__(save)
        #self.creature: Any = 'None'
    def serialize(self) -> dict:
        data = super().serialize()
        #data["_creature"] = self.creature
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        #self.creature = data.get("_creature", 'None')
