# @GENERATED 
from coctweak.saves.common.bodyparts.balls import BaseBalls

__ALL__=['HGGRawBalls']

class HGGRawBalls(BaseBalls):
    def __init__(self, save) -> None:
        super().__init__(save)
        self.hoursSinceCum: int = 0
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
        data["hoursSinceCum"] = self.hoursSinceCum
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
        self.hoursSinceCum = self._getInt("hoursSinceCum", 0, data)
