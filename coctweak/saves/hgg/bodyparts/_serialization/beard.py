# @GENERATED from coc/hgg/classes/classes/BodyParts/Beard.as
from coctweak.saves.common.bodyparts.bodypart import BodyPart

__ALL__=['HGGRawBeard']

class HGGRawBeard(BodyPart):
    NORMAL: int = 0
    GOATEE: int = 0
    CLEANCUT: int = 0
    MOUNTAINMAN: int = 0
    def __init__(self, save) -> None:
        super().__init__(save)
        self.style: float = 0.0
        self.length: float = 0.0
    def serialize(self) -> dict:
        data = super().serialize()
        data["style"] = self.style
        data["length"] = self.length
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.style = self._getFloat("style", 0.0)
        self.length = self._getFloat("length", 0.0)
