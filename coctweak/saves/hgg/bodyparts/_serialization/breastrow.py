# @GENERATED from coc/hgg/classes/classes/BreastRow.as
from coctweak.saves.common.bodyparts.breastrow import BaseBreastRow

__ALL__=['HGGRawBreastRow']

class HGGRawBreastRow(BaseBreastRow):
    def __init__(self, save) -> None:
        super().__init__(save)
        self.nippleCocks: bool = False
    def serialize(self) -> dict:
        data = super().serialize()
        data["nippleCocks"] = self.nippleCocks
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.nippleCocks = self._getBool("nippleCocks", False)
