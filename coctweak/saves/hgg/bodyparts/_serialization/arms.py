# @GENERATED from coc/hgg/classes/classes/BodyParts/Arms.as
from coctweak.saves.common.bodyparts.arms import BaseArms

__ALL__=['HGGRawArms']

class HGGRawArms(BaseArms):
    HUMAN: int = 0
    HARPY: int = 0
    SPIDER: int = 0
    BEE: int = 0
    PREDATOR: int = 0
    SALAMANDER: int = 0
    WOLF: int = 0
    COCKATRICE: int = 0
    RED_PANDA: int = 0
    FERRET: int = 0
    CAT: int = 0
    DOG: int = 0
    FOX: int = 0
    DRAGON: int = 0
    LIZARD: int = 0
    GNOLL: int = 0
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
