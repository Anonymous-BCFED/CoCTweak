# @GENERATED from coc/hgg/classes/classes/BodyParts/Horns.as
from coctweak.saves.common.bodyparts.horns import BaseHorns

__ALL__=['HGGRawHorns']

class HGGRawHorns(BaseHorns):
    NONE: int = 0
    DEMON: int = 0
    COW_MINOTAUR: int = 0
    DRACONIC_X2: int = 0
    DRACONIC_X4_12_INCH_LONG: int = 0
    ANTLERS: int = 0
    GOAT: int = 0
    UNICORN: int = 0
    RHINO: int = 0
    SHEEP: int = 0
    RAM: int = 0
    IMP: int = 0
    WOODEN: int = 0
    def __init__(self, save) -> None:
        super().__init__(save)
        self.value: float = 0.0
    def serialize(self) -> dict:
        data = super().serialize()
        data["value"] = self.value
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.value = self._getFloat("value", 0.0)
