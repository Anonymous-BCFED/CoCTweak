from coctweak.saves.hgg.bodyparts._serialization.antennae import HGGRawAntennae
from coctweak.saves.hgg.enums.antennaetypes import HGGAntennaeTypes

class HGGAntennae(HGGRawAntennae):
    ENUM_TYPES = HGGAntennaeTypes
