import random
from typing import List, Optional

from coctweak.saves.hgg.bodyparts._serialization.beard import HGGRawBeard
from coctweak.saves.hgg.enums.beard_types import HGGBeardTypes
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class HGGBeard(HGGRawBeard):
    def getDisplayText(self) -> str:
        return super().getDisplayText()

    def displayVars(self) -> None:
        super().displayVars()
        log.info('Style: %s', self.style.name)
        log.info('Length: %s', self.length)
