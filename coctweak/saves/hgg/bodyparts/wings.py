from coctweak.saves.hgg.enums.wingtypes import HGGWingType
from coctweak.saves.hgg.bodyparts._serialization.wings import HGGRawWings

class HGGWings(HGGRawWings):
    ENUM_TYPES = HGGWingType
