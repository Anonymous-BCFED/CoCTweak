import pprint

from coctweak.saves.common.bodyparts.cock import ECockTypeFlags
from coctweak.saves.hgg.bodyparts._serialization.cock import HGGRawCock
from coctweak.saves.hgg.enums.cocksocktypes import HGGCockSockTypes
from coctweak.saves.hgg.enums.cocktypes import HGGCockTypes
from coctweak.saves.hgg.piercing import HGGPiercing

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class HGGCock(HGGRawCock):
    TYPE_PIERCING = HGGPiercing
    COCK_TYPES = HGGCockTypes
    SOCK_TYPES = HGGCockSockTypes
    
    # HAS_KNOT:
    ## from [HGG]/classes/classes/lists/GenitalLists.as: public static const KNOTTED_COCKS:Vector.<CockTypesEnum> @ 6nh7AwfuC82La8hahp8gD16ddtu60C9Ff6Fk63f5oJ7tOb79gw71mHc8zfrx2xDanWbhV77O2Hm2VP9fCc2J2sl2p7ayO0LY
    ## from [HGG]/classes/classes/Cock.as: public function hasSheath():Boolean { @ 3Ey9ed2B02dEbIvg26bHb0OLf9S4kC2Yjagf5qWgAS44j6C48QdfTt9Rv45mgAs51l4Ze6B7gad7Ny9NkbQX5NneJvgYMcpc
    COCK_TYPE_FLAGS = {
        HGGCockTypes.AVIAN:                               ECockTypeFlags.HAS_SHEATH,
        HGGCockTypes.CAT:                                 ECockTypeFlags.HAS_SHEATH,
        HGGCockTypes.DISPLACER: ECockTypeFlags.HAS_KNOT | ECockTypeFlags.HAS_SHEATH,
        HGGCockTypes.DOG:       ECockTypeFlags.HAS_KNOT | ECockTypeFlags.HAS_SHEATH,
        HGGCockTypes.DRAGON:    ECockTypeFlags.HAS_KNOT,
        HGGCockTypes.ECHIDNA:                             ECockTypeFlags.HAS_SHEATH,
        HGGCockTypes.FERRET:                              ECockTypeFlags.HAS_SHEATH,
        HGGCockTypes.FOX:       ECockTypeFlags.HAS_KNOT | ECockTypeFlags.HAS_SHEATH,
        HGGCockTypes.HORSE:                               ECockTypeFlags.HAS_SHEATH,
        HGGCockTypes.KANGAROO:                            ECockTypeFlags.HAS_SHEATH,
        HGGCockTypes.RED_PANDA:                           ECockTypeFlags.HAS_SHEATH,
        HGGCockTypes.WOLF:      ECockTypeFlags.HAS_KNOT | ECockTypeFlags.HAS_SHEATH,
    }
    ## from [HGG]/classes/classes/Cock.as: public static const MAX_LENGTH:Number @ 7Os2Zxdag5lZ3fAbmWbOtbRv4bIdIv52H8PAaVidNk0nM4MtcEn8po7nodR95bidUleaoeqScjt7CkdFr0DJ7YqgpD4hSedu
    MAX_LENGTH = 9999.9
    ## from [HGG]/classes/classes/Cock.as: public static const MAX_THICKNESS:Number @ 2aW3jY3wZgNTd3E6CcbIs0EwgOPbbHdGD74rddI8G3caj67Weie82T9SCdG763WdxaeZlcXWaXTaRd0ole3m02E8fT6Ok1nc
    MAX_THICKNESS = 999.9
    ## from [HGG]/classes/classes/Cock.as: public static const KNOTMULTIPLIER_NO_KNOT:Number @ 7QC2lo7Z5aW16D42m59Xucp91ege4qbmt66I5PY8xa4Pwc4ngj2fVB2yscOQ9Vf3XteOIatpbbb2Kp1YP6ZDd1feWTaHW2P3
    MIN_KNOT_MULTIPLIER = 1.

    def getCockTypeStr(self):
        return HGGCockTypes(self.type).name

    def getKnotDiameter(self) -> float:
        return self.knotMultiplier * self.thickness

    def deserialize(self, data):
        super().deserialize(data)
        if not self.hasTypeFlag(ECockTypeFlags.HAS_KNOT) and self.knotMultiplier > 1:
            with log.warning('A cock has been loaded that does not support knots, yet erroneously has a knotMultiplier of %r. Knot will be ignored. Raw data:'):
                pprint.pprint(self.raw)

    def hasKnot(self) -> bool:
        return self.hasTypeFlag(ECockTypeFlags.HAS_KNOT) and self.knotMultiplier > 1

    def hasSheath(self) -> bool:
        return self.hasTypeFlag(ECockTypeFlags.HAS_SHEATH)

    def setCockType(self, newtype: HGGCockTypes) -> None:
        super().setCockType(newtype)
        if not self.hasTypeFlag(ECockTypeFlags.HAS_KNOT) and self.knotMultiplier > 1:
            log.warning('Specified cock type does not support knots, removing its size data.')
            self.knotMultiplier = self.MIN_KNOT_MULTIPLIER
