from coctweak.saves.hgg.enums.skintypes import HGGSkinType
from coctweak.saves.hgg.bodyparts._serialization.skin import HGGRawSkin

class HGGSkin(HGGRawSkin):
    ENUM_TYPES = HGGSkinType
