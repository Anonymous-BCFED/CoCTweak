from coctweak.saves.hgg.bodyparts._serialization.vagina import HGGRawVagina
from coctweak.saves.hgg.piercing import HGGPiercing
from coctweak.saves.hgg.enums.vag_wetness import HGGVagWetness
from coctweak.saves.hgg.enums.vag_looseness import HGGVagLooseness
from coctweak.saves.hgg.enums.vag_types import HGGVagTypes

__ALL__ = ['HGGVagina']

class HGGVagina(HGGRawVagina):
    ENUM_WETNESS = HGGVagWetness
    ENUM_LOOSENESS = HGGVagLooseness
    ENUM_TYPES = HGGVagTypes
    TYPE_PIERCING = HGGPiercing

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.clitPiercing = self.TYPE_PIERCING.SimpleLoadFrom(data, 'clit')
        self.labiaPiercing = self.TYPE_PIERCING.SimpleLoadFrom(data, 'labia')

    def serialize(self) -> dict:
        data = super().serialize()
        (self.clitPiercing if self.clitPiercing is not None else self.TYPE_PIERCING()).simpleSerializeTo(data, 'clit')
        (self.labiaPiercing if self.labiaPiercing is not None else self.TYPE_PIERCING()).simpleSerializeTo(data, 'labia')
        return data
