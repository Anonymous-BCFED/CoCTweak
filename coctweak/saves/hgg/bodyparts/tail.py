from coctweak.saves.hgg.enums.tailtypes import HGGTailType
from coctweak.saves.hgg.bodyparts._serialization.tail import HGGRawTail

class HGGTail(HGGRawTail):
    ENUM_TYPES = HGGTailType
