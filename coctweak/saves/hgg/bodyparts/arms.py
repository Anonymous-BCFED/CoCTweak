from coctweak.saves.hgg.bodyparts._serialization.arms import HGGRawArms
from coctweak.saves.hgg.enums.armtypes import HGGArmTypes

class HGGArms(HGGRawArms):
    ENUM_TYPES = HGGArmTypes
