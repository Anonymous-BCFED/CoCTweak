from coctweak.saves.hgg.enums.facetypes import HGGFaceType
from coctweak.saves.hgg.bodyparts._serialization.face import HGGRawFace
import random

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class HGGFace(HGGRawFace):
    ENUM_TYPES = HGGFaceType

    ## from [HGG]/classes/classes/lists/BodyPartLists.as: public static const MUZZLES:Array @ 7KX51Cd0zcnteBleei4QIbsnaUX1QZ4RU3Jp91feHg9Dc5Fs2um533b67e3V73VaDX4sUfAMgw1bV5gNHemj7AB4aE5HIeRH
    MUZZLES = (
        HGGFaceType.HORSE,
        HGGFaceType.DOG,
        HGGFaceType.CAT,
        HGGFaceType.LIZARD,
        HGGFaceType.KANGAROO,
        HGGFaceType.FOX,
        HGGFaceType.DRAGON,
        HGGFaceType.RHINO,
        HGGFaceType.ECHIDNA,
        HGGFaceType.DEER,
        HGGFaceType.WOLF,
        HGGFaceType.RED_PANDA,
    )

    def display(self) -> None:
        super().display()

    def getDisplayText(self) -> str:
        return super().getDisplayText() + f', {self.type.name}'

    def displayVars(self) -> None:
        super().displayVars()
        log.info('type: %s (%d)', self.type.name, self.type.value)

    def hasBeard(self) -> bool:
        return self.save.beard.length > 0

    def hasMuzzle(self) -> bool:
    	#public function hasMuzzle():Boolean {
    	#	return BodyPartLists.MUZZLES.indexOf(face.type) !== -1;
    	#}
        return self.type in self.MUZZLES

    def _faceDesc(self) -> str:
        save = self.save
        femininity: float = save.combat_stats.femininity.value
        ##from [HGG]/classes/classes/Character.as: public function faceDesc():String {

        #var faceo:String = "";
        faceo: str = ''
        #//0-10
        #if (femininity < 10) {
        if femininity < 10:
            #faceo = "a square chin";
            faceo = 'a square chin'
            #if (!hasBeard()) faceo += " and chiseled jawline";
            if not save.hasBeard():
                faceo += ' and chiseled jawline'
            #else faceo += ", chiseled jawline, and " + beardDesc();
            else:
                faceo += ', chiseled jawline, and beard'
            return faceo
        #}
        #//10+ -20
        #else if (femininity < 20) {
        elif femininity < 20:
            #faceo = "a rugged looking " + faceDescript() + " ";
            faceo = f'a rugged looking {self._faceDescript(save)} '
            #if (hasBeard()) faceo += "and " + beardDesc();
            if save.hasBeard():
                faceo += 'and beard'
            #faceo += " that's surely handsome";
            faceo += ' that\'s surely handsome'
            return faceo
        #}
        #//21-28
        #else if (femininity < 28) faceo = "a well-defined jawline and a fairly masculine profile";
        elif femininity < 28:
            return 'a well-defined jawline and a fairly masculine profile'
        #//28+-35
        #else if (femininity < 35) faceo = "a somewhat masculine, angular jawline";
        elif femininity < 35:
            return 'a somewhat masculine, angular jawline'
        #//35-45
        #else if (femininity < 45) faceo = "the barest hint of masculinity on its features";
        elif femininity < 45:
            return 'the barest hint of masculinity on its features'
        #//45-55
        #else if (femininity <= 55) faceo = "an androgynous set of features that would look normal on a male or female";
        elif femininity <= 55:
            return 'an androgynous set of features that would look normal on a male or female'
        #//55+-65
        #else if (femininity <= 65) faceo = "a tiny touch of femininity to it, with gentle curves";
        elif femininity <= 65:
            return 'a tiny touch of femininity to it, with gentle curves'
        #//65+-72
        #else if (femininity <= 72) faceo = "a nice set of cheekbones and lips that have the barest hint of pout";
        elif femininity <= 72:
            return 'a nice set of cheekbones and lips that have the barest hint of pout'
        #//72+-80
        #else if (femininity <= 80) faceo = "a beautiful, feminine shapeliness that's sure to draw the attention of males";
        elif femininity <= 80:
            return "a beautiful, feminine shapeliness that's sure to draw the attention of males"
        #//81-90
        #else if (femininity <= 90) faceo = "a gorgeous profile with full lips, a button nose, and noticeable eyelashes";
        elif femininity <= 90:
            return 'a gorgeous profile with full lips, a button nose, and noticeable eyelashes'
        #//91-100
        #else faceo = "a jaw-droppingly feminine shape with full, pouting lips, an adorable nose, and long, beautiful eyelashes";
        else:
            return 'a jaw-droppingly feminine shape with full, pouting lips, an adorable nose, and long, beautiful eyelashes'

    def _rollForOneOutOf(self, v: int) -> bool:
        # return int(Math.random() * 3) == 0
        return int(random.random() * v) == 0

    def _faceDescript(self) -> str:
        save = self.save
        ## from [HGG]/classes/classes/Character.as: public function faceDescript():String { @ ffP1BvbY340cgMZ0VQ3CX4q130SaeU4PEazX13MbD2es53Y6769c289Oo7Xt8024dZ2GdbWMco0bOX0wC7l0fyDdh63rG3qt
        #var stringo:String = "";
        o: str = ''
        #if (face.type == Face.HUMAN) return "face";
        if self.type == HGGFaceType.HUMAN:
            return 'face'
        #if (hasMuzzle()) {
        if self.hasMuzzle(save):
            #if (int(Math.random() * 3) == 0 && face.type == Face.HORSE) stringo = "long ";
            if self._rollForOneOutOf(3) and self.type == HGGFaceType.HORSE:
                o = "long "
            #if (int(Math.random() * 3) == 0 && face.type == Face.CAT) stringo = "feline ";
            if self._rollForOneOutOf(3) and self.type == HGGFaceType.CAT:
                o = "feline "
            #if (int(Math.random() * 3) == 0 && face.type == Face.RHINO) stringo = "rhino ";
            if self._rollForOneOutOf(3) and self.type == HGGFaceType.RHINO:
                o = "rhino "
            #if (int(Math.random() * 3) == 0 && (face.type == Face.LIZARD || face.type == Face.DRAGON)) stringo = "reptilian ";
            if self._rollForOneOutOf(3) and self.type in (HGGFaceType.LIZARD, HGGFaceType.DRAGON):
                o = "reptilian "
            #if (int(Math.random() * 3) == 0 && face.type == Face.WOLF) stringo = "canine ";
            if self._rollForOneOutOf(3) and self.type == HGGFaceType.WOLF:
                o = "canine "

            # Nuitka doesn't support Python 3.10 yet, so no match here.
            '''
            switch (rand(3)) {
                case 0:
                    return stringo + "muzzle";
                case 1:
                    return stringo + "snout";
                case 2:
                    return stringo + "face";
                default:
                    return stringo + "face";
            }
            '''
            return o + random.choice(['muzzle', 'snout', 'face'])
        #}
        #//3 - cowface
        #if (face.type == Face.COW_MINOTAUR) {
        if self.type == HGGFaceType.COW_MINOTAUR:
            #if (Math.floor(Math.random() * 4) == 0) stringo = "bovine ";
            if self._rollForOneOutOf(4):
                o = 'bovine '
            #if (int(Math.random() * 2) == 0) return "muzzle";
            #return stringo + "face";
            return o + random.choice(['muzzle', 'face'])
        #}
        #//4 - sharkface-teeth
        #if (face.type == Face.SHARK_TEETH) {
        if self.type == HGGFaceType.SHARK_TEETH:
            #if (Math.floor(Math.random() * 4) == 0) stringo = "angular ";
            if self._rollForOneOutOf(4):
                o = 'angular '
            #return stringo + "face";
            return o + 'face'
        #}
        #if (face.type == Face.PIG || face.type == Face.BOAR) {
        if self.type in (HGGFaceType.PIG, HGGFaceType.BOAR):
            #if (Math.floor(Math.random() * 4) == 0) stringo = (face.type == Face.PIG ? "pig" : "boar") + "-like ";
            if self._rollForOneOutOf(4):
                o = ('pig' if self.face == HGGFaceType.PIG else 'boar')+'-like '
            #if (Math.floor(Math.random() * 4) == 0) return stringo + "snout";
            #return stringo + "face";
            return o + random.choice(['snout', 'face'])
        #}
        #return "face";
        return 'face'
