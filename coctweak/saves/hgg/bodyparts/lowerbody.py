from coctweak.saves.hgg.enums.lowerbodytypes import HGGLowerBodyType
from coctweak.saves.hgg.bodyparts._serialization.lowerbody import HGGRawLowerBody

class HGGLowerBody(HGGRawLowerBody):
    ENUM_TYPES = HGGLowerBodyType

    def isDrider(self) -> bool:
        ## from [HGG]/classes/classes/Creature.as: public function isDrider():Boolean { @ 8S36xvdVj9ht1tne4rcaV2iZ097fLIgDD9QH0069Xf5ng1LXdRp6547n16QQ6LP0po17rbAIg7K1EqdXXfqn1l01wX85bbVq
        # return lowerBody.type == LowerBody.DRIDER;
        return self.type == HGGLowerBody.DRIDER

    def isTaur(self) -> bool:
        ## from [HGG]/classes/classes/Creature.as: public function isTaur():Boolean { @ aB1bZF1KW7NTaNYgl0fjl7Cz2R26Ix0YN7yi46hb6BbXecqU3cigdrdR799N24a9Ruflddf7dTyc9J9Rl3j18X992Q95a0wS
        #return lowerBody.legCount > 2 && !isDrider(); // driders have genitals on their human part, unlike usual taurs... this is actually bad way to check, but too many places to fix just now
        return self.count > 2 and not self.isDrider()