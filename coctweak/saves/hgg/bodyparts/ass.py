from typing import Dict
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.enums.perklib import HGGPerkLib
from coctweak.saves.hgg.hggutils import HGGUtils
from coctweak.saves.hgg.bodyparts._serialization.ass import HGGRawAss
from coctweak.saves.hgg.enums.ass_looseness import HGGAssLooseness
from coctweak.saves.hgg.enums.ass_wetness import HGGAssWetness
from coctweak.saves.hgg.enums.ass_ratings import HGGAssRatings

class HGGAss(HGGRawAss):
    OBJECT_KEY = 'ass'
    ENUM_LOOSENESS = HGGAssLooseness
    ENUM_WETNESS = HGGAssWetness
    ENUM_RATINGS = HGGAssRatings

    def deserializeFrom(self, data: dict) -> None:
        super().deserialize(data[self.OBJECT_KEY])
        super().deserializeFrom(data)

    def serializeTo(self, data: dict) -> None:
        data[self.OBJECT_KEY] = self.serialize()
        data['buttRating'] = self.buttRating

    def getHelpers(self) -> Dict[str, str]:
        o = super().getHelpers()
        o['Rating'] = self.buttRating
        return o

    def analCapacity(self) -> float:
        ## from [HGG]/classes/classes/Creature.as: public function analCapacity():Number { @ 4sOgGI58IaYmgrzeD8gzUfF00DeeUs8JHass5OS2jF6L3fP04bFep3boq3Ccfzw8y6afM1jm7PP4rmamS5CAdbceXE2ksf4i
        bonus: float = 0.
        # var bonus:Number = 0;
        # //Centaurs = +30 capacity
        # if (isTaur()) bonus = 30;
        if self.save.lowerBody.isTaur():
            bonus += 30.
        # if (hasPerk(PerkLib.HistorySlut)) bonus += 20;
        if self.save.hasPerk(HGGPerkLib.HistorySlut):
            bonus += 20.
        # if (hasPerk(PerkLib.Cornucopia)) bonus += 30;
        if self.save.hasPerk(HGGPerkLib.Cornucopia):
            bonus += 30.
        # if (hasPerk(PerkLib.OneTrackMind)) bonus += 10;
        if self.save.hasPerk(HGGPerkLib.OneTrackMind):
            bonus += 10.
        # if (ass.analWetness > 0) bonus += 15;
        if self.wetness > 0:
            bonus += 15.
        # return ((bonus + statusEffectv1(StatusEffects.BonusACapacity) + 6 * ass.analLooseness * ass.analLooseness) * (1 + ass.analWetness / 10));
        bonusAnalCapacity = 0.
        if (bac := self.save.getStatusEffect(HGGStatusLib.BonusACapacity)) is not None:
            bonusAnalCapacity = float(bac.value[0])
        return ((bonus+bonusAnalCapacity+6.*self.looseness*self.looseness)*(1+self.wetness/10))