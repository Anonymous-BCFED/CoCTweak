from coctweak.saves.hgg.enums.eartypes import HGGEarType
from coctweak.saves.hgg.piercing import HGGPiercing
from coctweak.saves.hgg.bodyparts._serialization.ears import HGGRawEars

class HGGEars(HGGRawEars):
    ENUM_TYPES = HGGEarType
    TYPE_PIERCING = HGGPiercing
