from typing import Type
from enum import IntEnum
from coctweak.saves.hgg.bodyparts._serialization.breastrow import HGGRawBreastRow

class HGGBreastRow(HGGRawBreastRow):
    def fixForMen(self, verbose: bool = False) -> None:
        super().fixForMen(verbose)
        '''
        self.setProps({
            'nippleCocks': False,
        }, quiet=not verbose)
        '''

    def reset(self, verbose: bool = False) -> None:
        self.setProps({
            'nippleCocks': False,
        }, quiet=not verbose)
