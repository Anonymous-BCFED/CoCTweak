from coctweak.saves.hgg.enums.horntypes import HGGHornType
from coctweak.saves.hgg.bodyparts._serialization.horns import HGGRawHorns

class HGGHorns(HGGRawHorns):
    ENUM_TYPES = HGGHornType
