from coctweak.saves.hgg.enums.skintypes import HGGSkinType
from coctweak.saves._skin import BaseSkin

class HGGSkin(BaseSkin):
    ENUM_TYPES = HGGSkinType
