from coctweak.saves._appearance import IAppearance
from typing import Union, List
from enum import IntEnum
import random, math
from coctweak.utils import clamp

class HGGAppearance(IAppearance):
    #public static const BREAST_CUP_NAMES:Array = ["flat",//0
    BREAST_CUP_NAMES: List[str] = [
        "flat",# 0
        "A-cup", "B-cup", "C-cup", "D-cup", "DD-cup", "big DD-cup", "E-cup", "big E-cup", "EE-cup",#  1-9
        "big EE-cup", "F-cup", "big F-cup", "FF-cup", "big FF-cup", "G-cup", "big G-cup", "GG-cup", "big GG-cup", "H-cup",# 10-19
        "big H-cup", "HH-cup", "big HH-cup", "HHH-cup", "I-cup", "big I-cup", "II-cup", "big II-cup", "J-cup", "big J-cup",# 20-29
        "JJ-cup", "big JJ-cup", "K-cup", "big K-cup", "KK-cup", "big KK-cup", "L-cup", "big L-cup", "LL-cup", "big LL-cup",# 30-39
        "M-cup", "big M-cup", "MM-cup", "big MM-cup", "MMM-cup", "large MMM-cup", "N-cup", "large N-cup", "NN-cup", "large NN-cup",# 40-49
        "O-cup", "large O-cup", "OO-cup", "large OO-cup", "P-cup", "large P-cup", "PP-cup", "large PP-cup", "Q-cup", "large Q-cup",# 50-59
        "QQ-cup", "large QQ-cup", "R-cup", "large R-cup", "RR-cup", "large RR-cup", "S-cup", "large S-cup", "SS-cup", "large SS-cup",# 60-69
        "T-cup", "large T-cup", "TT-cup", "large TT-cup", "U-cup", "large U-cup", "UU-cup", "large UU-cup", "V-cup", "large V-cup",# 70-79
        "VV-cup", "large VV-cup", "W-cup", "large W-cup", "WW-cup", "large WW-cup", "X-cup", "large X-cup", "XX-cup", "large XX-cup",# 80-89
        "Y-cup", "large Y-cup", "YY-cup", "large YY-cup", "Z-cup", "large Z-cup", "ZZ-cup", "large ZZ-cup", "ZZZ-cup", "large ZZZ-cup",# 90-99
        # HYPER ZONE
        "hyper A-cup", "hyper B-cup", "hyper C-cup", "hyper D-cup", "hyper DD-cup", "hyper big DD-cup", "hyper E-cup", "hyper big E-cup", "hyper EE-cup",# 100-109
        "hyper big EE-cup", "hyper F-cup", "hyper big F-cup", "hyper FF-cup", "hyper big FF-cup", "hyper G-cup", "hyper big G-cup", "hyper GG-cup", "hyper big GG-cup", "hyper H-cup",# 110-119
        "hyper big H-cup", "hyper HH-cup", "hyper big HH-cup", "hyper HHH-cup", "hyper I-cup", "hyper big I-cup", "hyper II-cup", "hyper big II-cup", "hyper J-cup", "hyper big J-cup",# 120-129
        "hyper JJ-cup", "hyper big JJ-cup", "hyper K-cup", "hyper big K-cup", "hyper KK-cup", "hyper big KK-cup", "hyper L-cup", "hyper big L-cup", "hyper LL-cup", "hyper big LL-cup",# 130-139
        "hyper M-cup", "hyper big M-cup", "hyper MM-cup", "hyper big MM-cup", "hyper MMM-cup", "hyper large MMM-cup", "hyper N-cup", "hyper large N-cup", "hyper NN-cup", "hyper large NN-cup",# 140-149
        "hyper O-cup", "hyper large O-cup", "hyper OO-cup", "hyper large OO-cup", "hyper P-cup", "hyper large P-cup", "hyper PP-cup", "hyper large PP-cup", "hyper Q-cup", "hyper large Q-cup",# 150-159
        "hyper QQ-cup", "hyper large QQ-cup", "hyper R-cup", "hyper large R-cup", "hyper RR-cup", "hyper large RR-cup", "hyper S-cup", "hyper large S-cup", "hyper SS-cup", "hyper large SS-cup",# 160-169
        "hyper T-cup", "hyper large T-cup", "hyper TT-cup", "hyper large TT-cup", "hyper U-cup", "hyper large U-cup", "hyper UU-cup", "hyper large UU-cup", "hyper V-cup", "hyper large V-cup",# 170-179
        "hyper VV-cup", "hyper large VV-cup", "hyper W-cup", "hyper large W-cup", "hyper WW-cup", "hyper large WW-cup", "hyper X-cup", "hyper large X-cup", "hyper XX-cup", "hyper large XX-cup",# 180-189
        "hyper Y-cup", "hyper large Y-cup", "hyper YY-cup", "hyper large YY-cup", "hyper Z-cup", "hyper large Z-cup", "hyper ZZ-cup", "hyper large ZZ-cup", "hyper ZZZ-cup", "hyper large ZZZ-cup",# 190-199
        "jacques00-cup"
    ]
    def __init__(self, save) -> None:
        super().__init__(save)

    def breastCup(self, _cupSize: Union[int, IntEnum]) -> str:
        cupSize: int = _cupSize.value if isinstance(_cupSize, IntEnum) else _cupSize

        ## from [HGG]/classes/classes/Appearance.as: public static function breastCup(size:Number):String { @ 5Oyeb05FScBk1OU5I00YR5sj4Ql9a315k8VHd7K0yFaXU2os3r35ndfCa3za7281kkbyLfMq8TkdR0dkcbHP5TS62JgSkfmV
        #return BREAST_CUP_NAMES[Math.min(Math.floor(size), BREAST_CUP_NAMES.length - 1)];
        idx: int = clamp(math.floor(cupSize), 0, len(self.BREAST_CUP_NAMES)-1)
        return self.BREAST_CUP_NAMES[idx]

    def breastSize(self, _cupSize: Union[int, IntEnum]) -> str:
        cupSize: int = _cupSize.value if isinstance(_cupSize, IntEnum) else _cupSize

        ## from [HGG]/classes/classes/Appearance.as: public static function breastSize(val:Number):String { @ fpBgF5d4f7CS9L35BH1pk1Sm39M0OQfugdy4ek89m3ekqflbfteb3faZRegj6OQg7S7xjbfK64l2EJ9XucJy8aj54RceLb9E
        #var descript:String = "";
        descript: str = ''

        #//Catch all for dudes.
        #if (val < 1) return "manly ";
        if cupSize < 1:
            return 'manly '
        #//Small - A->B
        #if (val <= 2) {
        #    descript += randomChoice("palmable ", "tight ", "perky ", "baseball-sized ");
        #}
        if cupSize <= 2:
            descript = random.choice(['palmable', 'tight', 'perky', 'baseball-sized'])
        #//C-D
        #else if (val <= 4) {
        #	descript += randomChoice("nice ", "hand-filling ", "well-rounded ", "supple ", "softball-sized ");
        #}
        elif cupSize <= 4:
            descript = random.choice(['nice', 'hand-filling', 'well-rounded', 'supple', 'softball-sized'])
        #//DD->big EE
        #else if (val < 11) {
        #	descript += randomChoice("big ", "large ", "pillowy ", "jiggly ", "volleyball-sized ");
        #}
        elif cupSize < 11:
            descript = random.choice(['big', 'large', 'pillowy', 'jiggly', 'volleyball-sized'])
        #//F->big FF
        #else if (val < 15) {
        #	descript += randomChoice("soccerball-sized ", "hand-overflowing ", "generous ", "jiggling ");
        #}
        elif cupSize < 16:
            descript = random.choice(['soccerball-sized', 'hand-overflowing', 'generous', 'jiggling'])
        #//G -> HHH
        #else if (val < 24) {
        #	descript += randomChoice("basketball-sized ", "whorish ", "cushiony ", "wobbling ");
        #}
        elif cupSize < 24:
            descript = random.choice(['basketball-sized', 'whorish', 'cushiony', 'wobbling'])
        #//I -> KK
        #else if (val < 35) {
        #	descript += randomChoice("massive motherly ", "luscious ", "smothering ", "prodigious ");
        #}
        elif cupSize < 35:
            descript = random.choice(['massive motherly', 'luscious', 'smothering', 'prodigious'])
        #//K- > MMM+
        #else if (val < 100) {
        #	descript += randomChoice("mountainous ", "monumental ", "back-breaking ", "exercise-ball-sized ", "immense ");
        #}
        elif cupSize < 100:
            descript = random.choice(['mountainous', 'monumental', 'back-breaking', 'exercise-ball-sized', 'immense'])
        #//Hyper sizes
        #else {
        #	descript += randomChoice("ludicrously-sized ", "hideously large ", "absurdly large ", "back-breaking ", "colossal ", "immense ");
        #}
        else:
            descript = random.choice(['ludicrously-sized', 'hideously large', 'absurdly large', 'back-breaking', 'colossal', 'immense'])
        return descript + ' '
