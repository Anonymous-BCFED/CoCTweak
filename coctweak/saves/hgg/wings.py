from coctweak.saves.hgg.enums.wingtypes import HGGWingType
from coctweak.saves._wings import BaseWings

class HGGWings(BaseWings):
    ENUM_TYPES = HGGWingType
