from coctweak.saves._globaldata import GlobalData
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.achievements import HGGAchievements

class HGGGlobalData(GlobalData):
    FLAGS_ENUM = HGGKFlags
    ACHIEVEMENTS_ENUM = HGGAchievements
    PERM_OBJ_VERSION = 1039900 # https://gitgud.io/BelshazzarII/CoCAnon_mod/blob/8eca94cb38b24605e2590fd717048fdb77aa999b/classes/classes/Saves.as#L631
    MIN_PERM_OBJECT_ID = 1039900
    MAX_PERM_OBJECT_ID = 1039900
