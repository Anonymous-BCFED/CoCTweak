from coctweak.saves.hgg.enums.lowerbodytypes import HGGLowerBodyType
from coctweak.saves._lowerbody import BaseLowerBody

class HGGLowerBody(BaseLowerBody):
    ENUM_TYPES = HGGLowerBodyType
