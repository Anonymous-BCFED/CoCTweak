from typing import Dict, Any
from .baseperk import HGGBasePerk

class HGGAdvancedPerk(HGGBasePerk):
    def __init__(self) -> None:
        super().__init__()
        self.content: Dict[str, Any] = {}

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.content = data['content']

    def serialize(self, use_overhauled_schema: bool) -> dict:
        data: dict = super().serialize(True)
        data['content'] = self.content
        return data
