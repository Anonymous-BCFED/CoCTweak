from coctweak.saves.pregnancystore import FlagPregnancyStore, VarPregnancyStore
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.pregnancytypes import HGGPregnancyType
from coctweak.saves.hgg.enums.incubationdurations import HGGIncubationDuration

class HGGFlagPregnancyStore(FlagPregnancyStore[HGGPregnancyType, HGGIncubationDuration]):
    PREGTYPE_ENUM = HGGPregnancyType
    INCUBATION_ENUM = HGGIncubationDuration

class HGGVarPregnancyStore(VarPregnancyStore[HGGPregnancyType, HGGIncubationDuration]):
    PREGTYPE_ENUM = HGGPregnancyType
    INCUBATION_ENUM = HGGIncubationDuration
