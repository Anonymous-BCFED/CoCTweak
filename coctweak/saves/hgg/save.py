from enum import IntEnum
from typing import List, Any, Type

import miniamf

from coctweak.saves.hgg.enums.age import HGGAges
from coctweak.saves.hgg.enums.itemlib import HGGItemLib
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.perklib import HGGPerkLib

from coctweak.saves._save import SaveCaps, BaseSave
from coctweak.saves._storage import Storage
from coctweak.saves._wearableslot import WearableSlot

from coctweak.saves.hgg.advancedperk import HGGAdvancedPerk
from coctweak.saves.hgg.ailments import WormsParasite, EelParasite, SlugParasite, NephilaParasite, MinotaurCumAddiction, MarbleMilkAddiction
from coctweak.saves.hgg.appearance import HGGAppearance
from coctweak.saves.hgg.combatstats import HGGCombatStats
from coctweak.saves.hgg.corestats import HGGCoreStats
from coctweak.saves.hgg.editordata import HGGCoCTweakData
from coctweak.saves.hgg.globaldata import HGGGlobalData
from coctweak.saves.hgg.item import HGGItem
from coctweak.saves.hgg.oldperk import HGGOldPerk
from coctweak.saves.hgg.wearableslot import HGGWearableSlot

from coctweak.saves.hgg.bodyparts.ass import HGGAss
from coctweak.saves.hgg.bodyparts.balls import HGGBalls
from coctweak.saves.hgg.bodyparts.breastrow import HGGBreastRow
from coctweak.saves.hgg.bodyparts.cock import HGGCock
from coctweak.saves.hgg.bodyparts.ears import HGGEars
from coctweak.saves.hgg.bodyparts.face import HGGFace
from coctweak.saves.hgg.bodyparts.horns import HGGHorns
from coctweak.saves.hgg.bodyparts.lowerbody import HGGLowerBody
from coctweak.saves.hgg.bodyparts.skin import HGGSkin
from coctweak.saves.hgg.bodyparts.tail import HGGTail
from coctweak.saves.hgg.bodyparts.vagina import HGGVagina
from coctweak.saves.hgg.bodyparts.wings import HGGWings
from coctweak.saves.hgg.bodyparts.antennae import HGGAntennae
from coctweak.saves.hgg.bodyparts.arms import HGGArms

from coctweak.saves.hgg.pregnancystore import HGGVarPregnancyStore

from coctweak.saves.hgg.npcs import AVAILABLE_NPC_CLASSES
from coctweak.saves.hgg.sso import AVAILABLE_SSO_CLASSES

from coctweak.logsys import getLogger #isort: skip
log = getLogger(__name__)

class HGGSave(BaseSave):
    NAME = 'HGG'

    GLOBAL_DATA_TYPE = HGGGlobalData
    GLOBAL_DATA_FILENAME = 'CoC_Main'

    CORE_STATS_TYPE = HGGCoreStats
    COMBAT_STATS_TYPE = HGGCombatStats

    ANTENNAE_TYPE = HGGAntennae
    ARMS_TYPE = HGGArms
    ASS_TYPE = HGGAss
    BALLS_TYPE = HGGBalls
    BREASTROW_TYPE = HGGBreastRow
    COCK_TYPE = HGGCock
    EARS_TYPE = HGGEars
    FACE_TYPE = HGGFace
    HORNS_TYPE = HGGHorns
    LOWER_BODY_TYPE = HGGLowerBody
    SKIN_TYPE = HGGSkin
    TAIL_TYPE = HGGTail
    VAGINA_TYPE = HGGVagina
    WINGS_TYPE = HGGWings

    ITEM_TYPE = HGGItem

    ENUM_FLAGS = HGGKFlags

    ITEMTYPES_FILE = 'data/hgg/items-merged.min.json'
    PERKTYPES_FILE = 'data/hgg/perks-merged.min.json'
    STATUSEFFECTTYPES_FILE = 'data/hgg/statuseffects-merged.min.json'
    MAX_INVENTORY_SLOTS = 10

    WEARABLESLOT_TYPE = HGGWearableSlot

    CAPABILITIES = SaveCaps.HUNGER | SaveCaps.DISEASES | SaveCaps.SELF_SAVING_OBJECTS
    DISEASES = [WormsParasite, EelParasite, SlugParasite, NephilaParasite, MinotaurCumAddiction, MarbleMilkAddiction]

    BAD_STATUS_FLAG = 'monster-dispellable'

    COCTWEAKDATATYPE = HGGCoCTweakData

    AVAILABLE_NPCS = AVAILABLE_NPC_CLASSES
    SELF_SAVING_OBJECT_TYPES = AVAILABLE_SSO_CLASSES

    VAGINAL_PREGNANCY_TYPE = HGGVarPregnancyStore
    ANAL_PREGNANCY_TYPE = HGGVarPregnancyStore

    APPEARANCE_TYPE = HGGAppearance

    QUAD_PERK_TYPE = HGGOldPerk

    def __init__(self):
        super().__init__()

        self.age: HGGAges = HGGAges.ADULT

        self.jewelry: WearableSlot = self.WEARABLESLOT_TYPE(self, 'jewelry', 'Jewelry', "nothing")
        self.wearableslots += [self.jewelry]

        # DEPRECATED.  Will be removed if the branch is merged.
        self.uses_overhauled_perks: bool = False

        self.startingAge: int = 0 # ?

    def postStatusEffectLoad(self) -> None:
        self.BAD_STATUSES = [x['id'] for k, x in self.ALL_STATUSEFFECTS.items() if self.BAD_STATUS_FLAG in x['flags']]
        log.debug(f'Loaded {len(self.BAD_STATUSES)} BAD_STATUSES ({self.BAD_STATUS_FLAG})')

    def unlockAllSlots(self, storage:Storage) -> None:
        for x in storage.slots:
            x.unlocked = True

    def lockAllSlots(self, storage:Storage) -> None:
        for x in storage.slots:
            x.unlocked = False

    def deserialize(self, data: miniamf.sol.SOL, isFile: bool) -> None:
        super().deserialize(data,isFile)
        self.startingAge = self._getInt('startingAge', 0, data)

    def serialize(self) -> dict:
        data = super().serialize()
        data['startingAge'] = self.startingAge
        return data

    def calcSlotLocks(self, verbose: bool=False) -> int:
        if verbose: 
            log.info('Recalculating inventory slot locks...').indent()
        ## from [HGG]/classes/classes/Scenes/Inventory.as: public function getMaxSlots():int { @ bbQ4NTf5e7xxgigeiU6lF8AJ9YI9qE9UH5tj2o0cjO2mVdHk3iMeOg6jIebweKH21O4Ys1bmfuBfo04apa9rdWegK8axM4U2
        #var slots:int = 3;
        unlocked: int = 3
        if self.hasPerk(HGGPerkLib.StrongBack):
            unlocked += 1
        if self.hasPerk(HGGPerkLib.StrongBack2):
            unlocked += 1
        if self.shield.id == HGGItemLib.CLKSHLD.value:
            unlocked += 2

        self.updateBackpackHack(unlocked, verbose)

        if self.hasKeyItem('Backpack'):
            unlocked += self.getKeyItem('Backpack').values[0]
        if verbose: log.info('Calculated unlocked slot count: %d', unlocked)
        unlocked = min(max(unlocked, 3), self.MAX_INVENTORY_SLOTS)
        if verbose: log.info('Capped unlocked slot count: %d', unlocked).dedent()
        return unlocked

    def calcFixedInventorySlots(self) -> int:
        ## from [HGG]/classes/classes/Scenes/Inventory.as: public function fixStorage():int { @ gM33mGdEH1pVaeGahfd5SeHU1SI6JW6Bk6OG90M2c7eo41vY14SdjJ89Dfgj49cgeNf7S47f5WZ03Nga581EfJV4lUfQnbzl
        fixedStorage: int = 4
        if self.hasKeyItem('Camp - Chest'):
            fixedStorage += 6
        if self.hasKeyItem('Camp - Murky Chest'):
            fixedStorage += 4
        if self.hasKeyItem('Camp - Ornate Chest'):
            fixedStorage += 4
        return fixedStorage

    def unlockNumSlots(self, storage: Storage, n: int, warn_on_changes: bool = False) -> None:
        for i in range(len(storage.slots)):
            x = storage.slots[i]
            u = x.unlocked
            x.unlocked = n > 0
            if u != x.unlocked:
                status = 'unlocked' if u else 'locked'
                if warn_on_changes:
                    log.warning('Inventory slot #%d was incorrectly %s. Fixed.', i+1, status)
            n -= 1

    def getModName(self):
        if self.flags.get(HGGKFlags.MOD_SAVE_VERSION, 0) < 15:
            return 'Revamp'
        return 'HGG'

    def getModVersion(self):
        if self.version == None:
            return 'null (BUG)'
        elif self.version.startswith('hgg '):
            return self.version[4:]
        else:
            return self.version

    def getAgeStr(self):
        return HGGAges(self.age).name

    def deserialize(self, data: dict, isFile: bool) -> None:
        super().deserialize(data, isFile)
        for gs in self.gearStorage:
            self.unlockAllSlots(gs)
        self.age = self._getInt('age')
        self.unlockNumSlots(self.playerStorage, self.calcSlotLocks(), False)
        self.unlockNumSlots(self.chestStorage, self.calcFixedInventorySlots(), False)

    def serialize(self):
        self.lockAllSlots(self.chestStorage)
        for gs in self.gearStorage:
            self.lockAllSlots(gs)
        #self.unlockNumSlots(self.playerStorage, self.calcSlotLocks(), True)
        self.lockAllSlots(self.playerStorage)

        data = super().serialize()
        data['age'] = self.age
        return data


    def getDifficulty(self):
        difficulty = self.flags.get(HGGKFlags.GAME_DIFFICULTY)
        ezmode = self.flags.get(HGGKFlags.EASY_MODE_ENABLE_FLAG) == 1
        if difficulty is None or difficulty == 0:
            return 'Easy' if ezmode else 'Normal'
        elif difficulty == 1:
            return 'Hard'
        elif difficulty == 2:
            return 'Nightmare'
        elif difficulty == 3:
            return 'EXTREME'

    def countCocksOfType(self, typeID: IntEnum):
        o = 0
        tid = typeID.value
        for cock in self.raw['cocks']:
            if cock.get('cockType') == tid:
                o += 1
        return o

    def loadInventory(self):
        for storage in self.gearStorage:
            storage.deserialize(self.raw['gearStorage'])
        self.chestStorage.deserialize(self.raw['itemStorage'])
        self.playerStorage.deserialize(self.raw['items'])
        return

    def saveInventory(self, data: dict):
        # BUG: HGG currently locks all slots in the player's inventory.
        self.lockAllSlots(self.playerStorage)

        for storage in self.gearStorage:
            storage.serialize(data['gearStorage'])
        data['itemStorage'] = self.chestStorage.serialize()
        data['items'] = self.playerStorage.serialize()
        return

    def postLoadInventory(self):
        # Make sure we've unlocked what needs to be unlocked.
        # The game currently sets all slots to locked on save, which makes me ask why they store the variable at all.
        self.unlockNumSlots(self.playerStorage, self.calcSlotLocks(), False)

    def loadSelfSavingFrom(self, data: dict) -> None:
        super().loadSelfSavingFrom(data)
        for k, clsType in self.SELF_SAVING_OBJECT_TYPES.items():
            ssoData = data['selfSaving'].get(k)
            if ssoData is None:
                continue
            sso = clsType()
            sso.deserialize(ssoData)
            self.selfSavingObjects[k] = sso

    def getPerkClassFor(self, pData: dict) -> Type[Any]:
        return HGGOldPerk if 'values' in pData or 'value1' in pData else HGGAdvancedPerk

    def serializePerks(self, data: dict) -> None:
        perks = []
        for p in self.perks.values():
            perks.append(p.serialize(self.uses_overhauled_perks))
        data['perks'] = perks

    def deserializePerks(self, data: dict) -> None:
        super().deserializePerks(data)
        self.uses_overhauled_perks = any([x.uses_overhauled_schema for x in self.perks.values()])
