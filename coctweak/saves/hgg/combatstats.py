import math
from typing import List
from coctweak.saves._combatstats import BaseCombatStats
from coctweak.saves.hgg.enums.age import HGGAges
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves._stat import Stat, StatHealType
from coctweak.saves.hgg.enums.jewelrymodifiers import HGGJewelryModifiers
from coctweak.saves.hgg.enums.bonus_derived_stat_ids import HGGBonusDerivedStatIDs
from coctweak.saves.hgg.enums.itemlib import HGGItemLib
from coctweak.saves.hgg.enums.perklib import HGGPerkLib
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.hggutils import HGGUtils
from coctweak.saves.hgg.racescores import HGGRaceScores
from coctweak.logsys import getLogger

log = getLogger(__name__)


class HGGCombatStats(BaseCombatStats):

    # from [HGG]/classes/classes/Scenes/Places/TelAdre/UmasShop.as: public static const NEEDLEWORK_DEFENSE_EXTRA_HP:int @ dTk9wE4ta9IA5gpdN437NgCZ02lcC73wVeofg9M07x1D2bKG4eOgdK3va2bWgP5aKw80U8i2dcWas47hy0b8fQfbpufcneRQ
    NEEDLEWORK_DEFENSE_EXTRA_HP: int = 50

    def __init__(self, save: 'HGGSave') -> None:
        self.hunger: Stat = None
        super().__init__(save)

    def init(self) -> None:
        super().init()
        self.hunger = self._add(Stat('hunger', 'Hunger', 0.0, lambda: 0, self._get_hunger_max, healtype=StatHealType.MAX))

    def _get_hunger_max(self) -> float:
        # from [HGG]/classes/classes/Character.as: public function maxHunger():Number { @ 0435pW33v0nybd60Q72Hcfp047o0Cg6coeTZ7Se2WPeiQfgy4rI9Ly3EV9AQ6a1bMkeFccuIeyn0Wg2d02ue63oeHX4Ay5cz
        return 100.

    def _get_ovi_egg_count(self) -> int:
        '''
        Ovipositor egg count.
        '''
        # from [HGG]/classes/classes/Creature.as: public function eggs():int { @ gXbfem9ecfQVaCp7mq2hU3ov4pv9sv8bp5HTfXX48r9984d726IbpJ1yufln9hm9tV6Lae900aq8Bp1sF5UqbnuehC9DrdVR
        spider_ovi = self.save.getPerk(HGGPerkLib.SpiderOvipositor)
        bee_ovi = self.save.getPerk(HGGPerkLib.SpiderOvipositor)
        # if (!hasPerk(PerkLib.SpiderOvipositor) && !hasPerk(PerkLib.BeeOvipositor)) return -1;
        if spider_ovi is None and bee_ovi is None:
            return -1
        # else if (hasPerk(PerkLib.SpiderOvipositor)) return perkv1(PerkLib.SpiderOvipositor);
        elif spider_ovi is not None:
            return spider_ovi.values[0]
        # else return perkv1(PerkLib.BeeOvipositor);
        return bee_ovi.values[0]

    def _get_max_lust(self) -> float:
        # from [HGG]/classes/classes/Creature.as: public function maxLust():Number { @ 4Ca8bM7PY4rp6ETgvG0Sq8zEefOabc6dd8ENfrp3FwcTw4oC5EP2bobnXbhX8qL5HK0i512l7YA2lo2MacwtfTa0Gog4qfeK
        # var max:Number = 100;
        _max: float = 100.

        def addIfPerksPresent(perkList: List[HGGPerkLib], toAdd: float) -> None:
            nonlocal _max
            if any((self.save.hasPerk(x) for x in perkList)):
                _max += toAdd
        # if (this == game.player && game.player.demonScore() >= 4) max += 20;
        # I skip the player check, since it's always true.
        if HGGRaceScores.getDemonScore(self.save) >= 4:
            _max += 20
        # if (hasPerk(PerkLib.ImprovedSelfControl)) max += 20;
        addIfPerksPresent([HGGPerkLib.ImprovedSelfControl], 20)
        # if (hasPerk(PerkLib.ImprovedSelfControl2)) max += 20;
        addIfPerksPresent([HGGPerkLib.ImprovedSelfControl2], 20)
        # if (hasPerk(PerkLib.BroBody) || hasPerk(PerkLib.BimboBody) || hasPerk(PerkLib.FutaForm)) max += 20;
        addIfPerksPresent(
            [HGGPerkLib.BroBody, HGGPerkLib.BimboBody, HGGPerkLib.FutaForm], 20)
        # if (hasPerk(PerkLib.OmnibusGift)) max += 15;
        addIfPerksPresent([HGGPerkLib.OmnibusGift], 15)
        # if (hasPerk(PerkLib.AscensionDesires)) max += perkv1(PerkLib.AscensionDesires) * 5;
        if self.save.hasPerk(HGGPerkLib.AscensionDesires):
            _max += self.save.getPerk(HGGPerkLib.AscensionDesires).values[0] * 5
        # if (hasPerk(PerkLib.NephilaArchQueen)) max += 40;
        addIfPerksPresent([HGGPerkLib.NephilaArchQueen], 40)
        # if (max > 999) max = 999;
        _max = min(_max, 999)
        # return max;
        return _max

    def _get_min_lust_soft_cap(self, raw: float) -> float:
        # from [HGG]/classes/classes/Player.as: private function minLustSoftCap(raw:Number):Number { @ 3uh0BA2PL31k6wnfmy0bM3kubK71kq2Fk26j0ga6s36Flgj7aPE1qgf2XeuP9zW5ee3z6dn54pY3yx9Oi4Mo45M86x34F8pr
        # var result:Number = 0;
        result: float = 0
        # while (raw > 20) {
        while raw > 20:
            # //Take 20 of the current value and add it to the total.
            # raw -= 20;
            raw -= 20
            # result += 20;
            result += 20
            # //Reduce any remaining value by 10%.
            # raw *= 0.9;
            raw *= 0.9

        # //Add any leftover value that's less than 20
        # result += raw;
        # return result;
        return result + raw

    def _get_lust_min(self) -> float:
        # from [HGG]/classes/classes/Player.as: public override function minLust():Number { @ eVp4oN23i247azq5u1eeUft09SV6hdcWS75xdTifxI2hNb8R2qy6M16u4fU68oBazT6H224edRd9W77VY7E7bWe0dKamJ2Pj
        # var min:Number = 0;
        _min: float = 0
        # var minCap:Number = maxLust();
        minCap: float = self._get_max_lust()
        # var minMin:Number = 0;
        minMin: float = 0
        # var count:int = 0;
        count: int = 0

        # //Adds to your min lust, and keeps track of how many different modifiers you have.
        # function addMin(value:Number):void {
        #    min += value;
        #    count++;
        # }
        def addMin(value: float) -> None:
            nonlocal _min, count
            _min += value
            count += 1

        # //Add up min lust modifiers. Order doesn't matter.
        # min += getBonusStat(BonusDerivedStats.minLust);
        _min += self.save.bonuses.get(HGGBonusDerivedStatIDs.minLust)

        # count += countBonusStat(BonusDerivedStats.minLust);
        count += self.save.bonuses.count(HGGBonusDerivedStatIDs.minLust)

        eggs = self._get_ovi_egg_count()

        # if (eggs() >= 20) addMin(10);
        if eggs >= 20:
            addMin(10)
        # if (eggs() >= 40) addMin(10)
        if eggs >= 40:
            addMin(10)
        # if (hasStatusEffect(StatusEffects.AnemoneArousal)) addMin(30)
        if self.save.hasStatusEffect(HGGStatusLib.AnemoneArousal):
            addMin(30)
        # if (hasStatusEffect(StatusEffects.ParasiteSlug)) addMin(10)
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteSlug):
            addMin(10)
        # if (hasStatusEffect(StatusEffects.ParasiteEelNeedCum)) addMin(5 * statusEffectv3(StatusEffects.ParasiteEelNeedCum));
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteEelNeedCum):
            addMin(
                5 * self.save.getStatusEffect(HGGStatusLib.ParasiteEelNeedCum).values[2])
        # if (hasStatusEffect(StatusEffects.ParasiteNephilaNeedCum)) addMin(5 * statusEffectv3(StatusEffects.ParasiteNephilaNeedCum));
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum):
            addMin(
                5 * self.save.getStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum).values[2])
        # if (hasStatusEffect(StatusEffects.BimboChampagne) || hasPerk(PerkLib.BimboBody) || hasPerk(PerkLib.BroBody) || hasPerk(PerkLib.FutaForm)) {
        if self.save.hasStatusEffect(HGGStatusLib.BimboChampagne) or any((self.save.hasPerk(x) for x in (HGGPerkLib.BimboBody, HGGPerkLib.BroBody, HGGPerkLib.FutaForm))):
            # addMin(40)
            addMin(40)

        # if (flags[kFLAGS.SHOULDRA_SLEEP_TIMER] <= -168 && flags[kFLAGS.URTA_QUEST_STATUS] != 0.75) {
        if self.save.flags.get(HGGKFlags.SHOULDRA_SLEEP_TIMER, 0) <= -168 and self.save.flags.get(HGGKFlags.URTA_QUEST_STATUS, 0) != 0.75:
            # addMin(20)
            addMin(20)
            # if (flags[kFLAGS.SHOULDRA_SLEEP_TIMER] <= -216) addMin(30)
            if self.save.flags.get(HGGKFlags.SHOULDRA_SLEEP_TIMER, 0) <= -216:
                addMin(30)

        # //Add up minimum min lust modifiers: these put a lower bound on your min lust. The highest value is used, so order doesn't matter.
        # if (armorName == "lusty maiden's armor") minMin = Math.max(minMin, 30);
        if self.save.armor.id == HGGItemLib.LMARMOR:
            minMin = max(minMin, 30)
        # if (armorName == "tentacled bark armor") minMin = Math.max(minMin, 20);
        if self.save.armor.id == HGGItemLib.TBARMOR:
            minMin = max(minMin, 20)
        # if (hasStatusEffect(StatusEffects.Luststick)) minMin = Math.max(minMin, 50);
        if self.save.hasStatusEffect(HGGStatusLib.LustStick):
            minMin = max(minMin, 50)
        # if (hasStatusEffect(StatusEffects.Infested))  minMin = Math.max(minMin, 50);
        if self.save.hasStatusEffect(HGGStatusLib.Infested):
            minMin = max(minMin, 50)
        # if (hasStatusEffect(StatusEffects.ParasiteSlugReproduction)) minMin = Math.max(minMin, 80);
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteSlugReproduction):
            minMin = max(minMin, 80)
        # if (hasPerk(PerkLib.ParasiteMusk)) minMin = Math.max(minMin, 50);
        if self.save.hasPerk(HGGPerkLib.ParasiteMusk):
            minMin = max(minMin, 50)

        # //Apply soft caps only if you have more than one modifier
        # if (count > 1) min = minLustSoftCap(min);
        if count > 1:
            _min = self._get_min_lust_soft_cap(_min)

        # //Cold Blooded is unique, apply it separately after soft caps
        # if (hasPerk(PerkLib.ColdBlooded)) {
        if self.save.hasPerk(HGGPerkLib.ColdBlooded):
            # //Reduce min lust by up to 20, but don't reduce it below 20
            # if (min > 20) min -= Math.min(20, min-20);
            if _min > 20:
                _min -= min(20, _min-20)
            # minCap -= 20;
            minCap -= 20

        # min = boundInt(minMin, round(min), minCap)
        # return min;
        return HGGUtils.boundInt(minMin, round(_min), minCap)

    def _get_hp_max_unmod(self) -> float:
        # from [HGG]/classes/classes/Creature.as: public function maxHPUnmodified():Number {
        # var max:Number = 0;
        max_: float = 0
        # max += int(tou * 2 + 50);
        max_ += int((self.save.stats.toughness.value * 2.) + 50.)
        # if (hasPerk(PerkLib.Tank)) max += 50;
        if self.save.hasPerk(HGGPerkLib.Tank):
            max_ += 50
        # if (hasPerk(PerkLib.Tank2)) max += Math.round(tou);
        if self.save.hasPerk(HGGPerkLib.Tank2):
            max_ += round(self.save.stats.toughness.value)
        # if (hasPerk(PerkLib.Tank3)) max += level * 5;
        if self.save.hasPerk(HGGPerkLib.Tank3):
            max_ += self.level.value * 5
        # max += level * 15;
        max_ += self.level.value * 15
        # max += getBonusStat(BonusDerivedStats.maxHealth);
        max_ += self.save.bonuses.get(HGGBonusDerivedStatIDs.maxHealth)
        # if (hasPerk(PerkLib.ChiReflowDefense)) max += UmasShop.NEEDLEWORK_DEFENSE_EXTRA_HP;
        if self.save.hasPerk(HGGPerkLib.ChiReflowDefense):
            max_ += self.NEEDLEWORK_DEFENSE_EXTRA_HP

        # if (jewelryEffectId == JewelryLib.MODIFIER_HP) max += jewelryEffectMagnitude;
        if self.save.jewelry.effect.id == HGGJewelryModifiers.HP:
            max_ += self.save.jewelry.effect.magnitude
        # max *= 1 + (countCockSocks("green") * 0.02);
        max_ *= 1 + (self.save.countCockSocks('green') * 0.02)

        # max *= getBonusStatMultiplicative(BonusDerivedStats.maxHealth);
        max_ *= self.save.bonuses.getMultiplicative(
            HGGBonusDerivedStatIDs.maxHealth)
        # if (hasStatusEffect(StatusEffects.Soulburst)) max = max / Math.pow(2, statusEffectv1(StatusEffects.Soulburst));
        if self.save.hasStatusEffect(HGGStatusLib.Soulburst):
            max_ /= pow(2,
                        self.save.statusEffects[HGGStatusLib.Soulburst].values[0])
        # return max;
        return max_

    def _get_hp_max(self) -> float:
        # from [HGG]/classes/classes/Creature.as: public function maxHP():Number { @ 2Rz7mffCO6vUdkI4aGeZ52HDfm92xpc862Sn79R7yMeHTeLEbqM2mE8VF0729V5g1n3mK6mk7rX3yA20tbDZ3lP7q8cTS9Bl
        # var max:Number = maxHPUnmodified();
        m: float = self._get_hp_max_unmod()

        # if (hasStatusEffect(StatusEffects.Overhealing)) max += statusEffectv1(StatusEffects.Overhealing);
        if self.save.hasStatusEffect(HGGStatusLib.Overhealing):
            m += self.save.statusEffects[HGGStatusLib.Overhealing].values[0]

        # max = Math.round(max);
        # if (max > 9999) max = 9999;
        # return max;
        return min(round(m), 9999)

    def _get_max_fatigue(self) -> float:
        # from [HGG]/classes/classes/Creature.as: public function maxFatigue():Number { @ 3a68iQ1hvcUU0Hx3l0dK1flUblM6C801h6KH2vt6L9c7tc9324W8BHduM0iNffBb9o2dy9iEgim8dU76Zdxf1Ou8re0V5fCe
        # var max:Number = 100;
        max_: float = 100.
        # max += getBonusStat(BonusDerivedStats.fatigueMax);
        max_ += self.save.bonuses.get(HGGBonusDerivedStatIDs.fatigueMax)
        # if (max > 999) max = 999;
        if max_ > 999.:
            max_ = 999.
        # return max;
        return max_
