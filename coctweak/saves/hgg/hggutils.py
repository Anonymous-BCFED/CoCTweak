from coctweak.saves.hgg.enums.wingtypes import HGGWingType
from coctweak.saves.hgg.enums.skintypes import HGGSkinType
class HGGUtils(object):
    @staticmethod
    def boundInt(_min, value, _max):
        return min(max(_min, value), _max)

    @staticmethod
    def hasBatlikeWings(save: 'HGGSave') -> bool:
        ## from [HGG]/classes/classes/PlayerHelper.as: public function hasBatLikeWings(large:Boolean = false):Boolean { @ 8Xs1gO1JlciGbCRefJaho1tJeEvdRvgQd3lUbdkcGf9wKfBJffT0Eq0Wk4I40qmban5lK0nx2eG7FL8Xy4va8rweoo0RV73P
        return save.wings.type in (HGGWingType.BAT_LIKE_TINY, HGGWingType.BAT_LIKE_LARGE)

    @staticmethod
    def hasPlainSkin(save: 'HGGSave') -> bool:
        ## from [HGG]/classes/classes/Creature.as: public function hasPlainSkin():Boolean { @ ezz0Tzd2Y3pU4469Zfe4B1JRgfV65NcTI9BkbAh3ucaWa4LPgXV4gYa7Y3VIgdY2mQ9kg5rp9NB6xldifbUR2gU6SCeYUf6l
		#return skin.type == Skin.PLAIN;
        return save.skin.type == HGGSkinType.PLAIN

    @staticmethod
    def hasBeard(save: 'HGGSave') -> bool:
        ## from [HGG]/classes/classes/Character.as: public function hasBeard():Boolean { @ bL46bQ6mNf7Tds2aBq4JV6mhebt8tWfNK0Wc68W8RbcWgaSN0EUbOr3RV8MY92q88EgfZ3Io1yZ6gfgZI7mc3ltb4o2rO0ZK
        #return beard.length > 0;
        return save.beard.length > 0
