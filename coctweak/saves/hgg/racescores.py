import typing
from coctweak.saves.hgg.enums.eartypes import HGGEarType
from coctweak.saves.hgg.enums.wingtypes import HGGWingType
from coctweak.saves.hgg.enums.facetypes import HGGFaceType
from coctweak.saves.hgg.enums.cocktypes import HGGCockTypes
from coctweak.saves.hgg.enums.horntypes import HGGHornType
from coctweak.saves.hgg.enums.tailtypes import HGGTailType
from coctweak.saves.hgg.enums.lowerbodytypes import HGGLowerBodyType
from coctweak.saves.hgg.hggutils import HGGUtils
if typing.TYPE_CHECKING:
    from coctweak.saves.hgg.save import HGGSave
class HGGRaceScores(object):
    @staticmethod
    def getDemonScore(save: 'HGGSave') -> int:
        ## from [HGG]/classes/classes/Player.as: public function demonScore():Number { @ 7Rb09Ycm42cH1afcNTfi775t6S3bkaeRh6k28VyawTcf62dEfJhcMifhigQC0zxbZn4Rvgk36cVgRp5PP0s24NVbel2Bb6gD
        #var demonCounter:Number = 0;
        count: int = 0
        #if (horns.type == Horns.DEMON && horns.value > 0) demonCounter++;
        if save.horns.type == HGGHornType.DEMON and save.horns.value > 0:
            count += 1
        #if (horns.type == Horns.DEMON && horns.value > 4) demonCounter++;
        if save.horns.type == HGGHornType.DEMON and save.horns.value > 4:
            count += 1
        #if (tail.type == Tail.DEMONIC) demonCounter++;
        if save.tail.type == HGGTailType.DEMONIC:
            count += 1
        #if (hasBatLikeWings()) demonCounter++;
        if HGGUtils.hasBatlikeWings(save):
            count += 1
        #if (hasPlainSkin() && cor > 50) demonCounter++;
        if HGGUtils.hasPlainSkin(save) and save.stats.corruption.value > 50:
            count += 1
        #if (face.type == Face.HUMAN && cor > 50) demonCounter++;
        if save.face.type == HGGFaceType.HUMAN and save.stats.corruption.value > 50:
            count += 1
        #if (lowerBody.type == LowerBody.DEMONIC_HIGH_HEELS || lowerBody.type == LowerBody.DEMONIC_CLAWS) demonCounter++;
        if save.lowerBody.type in (HGGLowerBodyType.DEMONIC_HIGH_HEELS, HGGLowerBodyType.DEMONIC_CLAWS):
            count += 1
        #if (countCocksOfType(CockTypesEnum.DEMON) > 0) demonCounter++;
        if save.countCocksOfType(HGGCockTypes.DEMON) > 0:
            count += 1
        #return demonCounter;
        return count
