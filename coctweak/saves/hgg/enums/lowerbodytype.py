# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/BodyParts/LowerBody.as
from enum import IntEnum

__ALL__ = ['HGGLowerBodyType']

class HGGLowerBodyType(IntEnum):
    HUMAN                  = 0
    HOOFED                 = 1
    DOG                    = 2
    NAGA                   = 3
    CENTAUR                = 4
    DEMONIC_HIGH_HEELS     = 5
    DEMONIC_CLAWS          = 6
    BEE                    = 7
    GOO                    = 8
    CAT                    = 9
    LIZARD                 = 10
    PONY                   = 11
    BUNNY                  = 12
    HARPY                  = 13
    KANGAROO               = 14
    CHITINOUS_SPIDER_LEGS  = 15
    DRIDER                 = 16
    FOX                    = 17
    DRAGON                 = 18
    RACCOON                = 19
    FERRET                 = 20
    CLOVEN_HOOFED          = 21
    RHINO                  = 22
    ECHIDNA                = 23
    DEERTAUR               = 24
    SALAMANDER             = 25
    WOLF                   = 26
    IMP                    = 27
    COCKATRICE             = 28
    RED_PANDA              = 29
    ROOT_LEGS              = 30
    GNOLL                  = 31
