# @GENERATED from enums.yml
from enum import IntFlag

__all__ = ['EHGGPerkFlags']

class EHGGPerkFlags(IntFlag):
    NONE       = 0
    PERMANENT  = 1
