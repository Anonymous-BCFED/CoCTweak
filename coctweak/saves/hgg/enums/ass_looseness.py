# @GENERATED from coc/hgg/classes/classes/Ass.as
from enum import IntEnum

__all__ = ['HGGAssLooseness']

class HGGAssLooseness(IntEnum):
    VIRGIN     = 0
    TIGHT      = 1
    NORMAL     = 2
    LOOSE      = 3
    STRETCHED  = 4
    GAPING     = 5
