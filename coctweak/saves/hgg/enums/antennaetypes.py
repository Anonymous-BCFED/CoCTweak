# @GENERATED from coc/hgg/classes/classes/BodyParts/Antennae.as
from enum import IntEnum

__all__ = ['HGGAntennaeTypes']

class HGGAntennaeTypes(IntEnum):
    NONE        = 0
    BEE         = 2
    COCKATRICE  = 3
