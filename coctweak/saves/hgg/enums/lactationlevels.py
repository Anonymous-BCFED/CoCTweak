# @GENERATED from coc/hgg/classes/classes/BreastStore.as
from enum import IntEnum

__all__ = ['HGGLactationLevels']

class HGGLactationLevels(IntEnum):
    DISABLED  = 0
    NONE      = 1
    LIGHT     = 2
    MODERATE  = 3
    STRONG    = 4
    HEAVY     = 5
    EPIC      = 6
