# @GENERATED from coc/hgg/classes/classes/BodyParts/Tail.as
from enum import IntEnum

__all__ = ['HGGTailType']

class HGGTailType(IntEnum):
    NONE            = 0
    HORSE           = 1
    DOG             = 2
    DEMONIC         = 3
    COW             = 4
    SPIDER_ABDOMEN  = 5
    BEE_ABDOMEN     = 6
    SHARK           = 7
    CAT             = 8
    LIZARD          = 9
    RABBIT          = 10
    HARPY           = 11
    KANGAROO        = 12
    FOX             = 13
    DRACONIC        = 14
    RACCOON         = 15
    MOUSE           = 16
    FERRET          = 17
    PIG             = 19
    SCORPION        = 20
    GOAT            = 21
    RHINO           = 22
    ECHIDNA         = 23
    DEER            = 24
    SALAMANDER      = 25
    WOLF            = 26
    SHEEP           = 27
    IMP             = 28
    COCKATRICE      = 29
    RED_PANDA       = 30
    GNOLL           = 31
