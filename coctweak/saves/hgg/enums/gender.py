# @GENERATED from coc/hgg/classes/classes/lists/Gender.as
from enum import IntEnum

__all__ = ['HGGGender']

class HGGGender(IntEnum):
    ANY     = -1
    NONE    = 0
    MALE    = 1
    FEMALE  = 2
    HERM    = 3
