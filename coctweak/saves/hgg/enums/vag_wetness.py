# @GENERATED from coc/hgg/classes/classes/Vagina.as
from enum import IntEnum

__all__ = ['HGGVagWetness']

class HGGVagWetness(IntEnum):
    DRY        = 0
    NORMAL     = 1
    WET        = 2
    SLICK      = 3
    DROOLING   = 4
    SLAVERING  = 5
