# @GENERATED from coc/hgg/classes/classes/Items/JewelryLib.as
from enum import IntEnum

__all__ = ['HGGJewelryModifiers']

class HGGJewelryModifiers(IntEnum):
    NONE           = 0
    MINIMUM_LUST   = 1
    FERTILITY      = 2
    CRITICAL       = 3
    REGENERATION   = 4
    HP             = 5
    ATTACK_POWER   = 6
    SPELL_POWER    = 7
    PURITY         = 8
    CORRUPTION     = 9
    FLAMESPIRIT    = 10
    ACCURACY       = 11
    ETHEREALBLEED  = 12
    SPECTRE        = 13
    FRENZY         = 14
