# @GENERATED from coc/hgg/classes/classes/Vagina.as
from enum import IntEnum

__all__ = ['HGGVagLooseness']

class HGGVagLooseness(IntEnum):
    TIGHT            = 0
    NORMAL           = 1
    LOOSE            = 2
    GAPING           = 3
    GAPING_WIDE      = 4
    LEVEL_CLOWN_CAR  = 5
