# @GENERATED from coc/hgg/classes/classes/BodyParts/Beard.as
from enum import IntEnum

__all__ = ['HGGBeardTypes']

class HGGBeardTypes(IntEnum):
    NORMAL       = 0
    GOATEE       = 1
    CLEANCUT     = 2
    MOUNTAINMAN  = 3
