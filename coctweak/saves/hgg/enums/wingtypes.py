# @GENERATED from coc/hgg/classes/classes/BodyParts/Wings.as
from enum import IntEnum

__all__ = ['HGGWingType']

class HGGWingType(IntEnum):
    NONE             = 0
    BEE_LIKE_SMALL   = 1
    BEE_LIKE_LARGE   = 2
    HARPY            = 4
    IMP              = 5
    BAT_LIKE_TINY    = 6
    BAT_LIKE_LARGE   = 7
    SHARK_FIN        = 8
    FEATHERED_LARGE  = 9
    DRACONIC_SMALL   = 10
    DRACONIC_LARGE   = 11
    GIANT_DRAGONFLY  = 12
    IMP_LARGE        = 13
    FAERIE_SMALL     = 14
    FAERIE_LARGE     = 15
    WOODEN           = 16
