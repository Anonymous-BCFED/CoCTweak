# @GENERATED from coc/hgg/classes/classes/Ass.as
from enum import IntEnum

__all__ = ['HGGAssWetness']

class HGGAssWetness(IntEnum):
    DRY             = 0
    NORMAL          = 1
    MOIST           = 2
    SLIMY           = 3
    DROOLING        = 4
    SLIME_DROOLING  = 5
