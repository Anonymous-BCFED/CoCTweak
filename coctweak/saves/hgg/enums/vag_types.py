# @GENERATED from coc/hgg/classes/classes/Vagina.as
from enum import IntEnum

__all__ = ['HGGVagTypes']

class HGGVagTypes(IntEnum):
    HUMAN            = 0
    EQUINE           = 1
    BLACK_SAND_TRAP  = 5
