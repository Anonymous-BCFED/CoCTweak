# @GENERATED from coc/hgg/classes/classes/PregnancyStore.as
from enum import IntEnum

__all__ = ['HGGIncubationDuration']

class HGGIncubationDuration(IntEnum):
    IMP                  = 432
    MINOTAUR             = 432
    MOUSE                = 350
    OVIELIXIR_EGGS       = 50
    HELL_HOUND           = 352
    CENTAUR              = 420
    CORRWITCH            = 240
    MARBLE               = 368
    BUNNY_BABY           = 200
    BUNNY_EGGS           = 808
    ANEMONE              = 256
    IZMA                 = 300
    SPIDER               = 400
    BASILISK             = 250
    COCKATRICE           = 225
    DRIDER               = 400
    GOO_GIRL             = 85
    EMBER                = 336
    SATYR                = 160
    COTTON               = 350
    URTA                 = 515
    SAND_WITCH           = 360
    FROG_GIRL            = 30
    FAERIE               = 200
    BEE                  = 48
    SANDTRAP             = 42
    HARPY                = 168
    SHIELA               = 72
    SALAMANDER           = 336
    MINERVA              = 216
    PHOENIX              = 168
    KIHA                 = 336
    ISABELLA             = 2160
    TENTACLE_BEAST_SEED  = 120
    SYLVIA               = 336
    NONE                 = 0
