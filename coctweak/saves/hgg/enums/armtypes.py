# @GENERATED from coc/hgg/classes/classes/BodyParts/Arms.as
from enum import IntEnum

__all__ = ['HGGArmTypes']

class HGGArmTypes(IntEnum):
    HUMAN       = 0
    HARPY       = 1
    SPIDER      = 2
    BEE         = 3
    PREDATOR    = 4
    SALAMANDER  = 5
    WOLF        = 6
    COCKATRICE  = 7
    RED_PANDA   = 8
    FERRET      = 9
    CAT         = 10
    DOG         = 11
    FOX         = 12
    DRAGON      = 13
    LIZARD      = 14
    GNOLL       = 15
