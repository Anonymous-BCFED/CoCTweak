# @GENERATED from coc/hgg/classes/classes/lists/Age.as
from enum import IntEnum

__all__ = ['HGGAges']

class HGGAges(IntEnum):
    ADULT    = 0
    CHILD    = 1
    TEEN     = 2
    ELDER    = 3
    #invalid literal for int() with base 10: '["adult", "child", "teenager", "elder"]'
    #strings    = ["adult", "child", "teenager", "elder"]
