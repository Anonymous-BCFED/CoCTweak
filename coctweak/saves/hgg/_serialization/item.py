# @GENERATED from coc/hgg/classes/classes/ItemSlot.as
from coctweak.saves.common.item import BaseItem

__ALL__=['HGGRawItem']

class HGGRawItem(BaseItem):
    def __init__(self) -> None:
        super().__init__()
        self.damage: int = 0
    def serialize(self) -> dict:
        data = super().serialize()
        data["damage"] = self.damage
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.damage = self._getInt("damage", 0)
