from coctweak.saves.hgg.enums.perkflags import EHGGPerkFlags
from typing import Any, List
from coctweak.saves._saveobj import SaveObject

from coctweak.logsys import getLogger
log = getLogger(__name__)
class HGGBasePerkMeta(object):
    FLAG_EXPLAINS = {
        'merge-flags': None,
        'fully-mapped': 'Full support by CoCTweak',
        'values-unused': 'Values Unused',
        'coctweak-internal': 'COCTWEAK INTERNAL USE ONLY'
    }
    def __init__(self) -> None:
        self._metaLoaded:bool = False
        self.description: str = ''
        self.flags: List[str] = []
        self.bonusStats:dict = {}

        self.meanings = None

    def load_from_meta(self, data: dict):
        self.flags = data.get('flags', [])
        self.description = data.get('desc', None)
        self.bonusStats = data.get('bonusStats', {})
        self._metaLoaded = True

    def longPrintValues(self, perk: 'HGGBasePerk') -> None:
        with log.info('Description:'):
            if self.description is not None:
                log.info(self.description)
        with log.info('Flags:'):
            for f in sorted(map(lambda x: f'{self.FLAG_EXPLAINS.get(x, "UNKNOWN")} ({x})', self.flags)):
                if 'coctweak' in f:
                    log.warning(f)
                else:
                    log.info(f)
        if self.bonusStats is not None:
            with log.info('Bonus Stats:'):
                for k, v in self.bonusStats.items():
                    log.info(f'{k}: {v!r}')

class HGGBasePerk(object):
    def __init__(self) -> None:
        super().__init__()
        self.id: str = ''

        self.name:str = ''
        self.keepOnAscension:bool = False

        self.uses_overhauled_schema:bool = False

        self.flags: EHGGPerkFlags = EHGGPerkFlags.NONE

        self.meta: HGGBasePerkMeta

    def deserialize(self, data: dict) -> None:
        assert 'id' in data or 'perkName' in data, 'Malformed perk. '+repr(data)
        self.id = data['id'] or data['perkName']
        self.flags = EHGGPerkFlags(data.get('flags', 0))

        self.uses_overhauled_perks = 'values' in data or 'content' in data

    def serialize(self, use_overhauled_schema:bool) -> dict:
        o: dict = {
            'id': self.id
        }
        if use_overhauled_schema:
            o['flags'] = self.flags.value

        return o

    @property
    def isPermanent(self) -> bool:
        return (self.flags & EHGGPerkFlags.PERMANENT) == EHGGPerkFlags.PERMANENT

    @isPermanent.setter
    def set_isPermanent(self, value: bool) -> None:
        if value:
            self.flags |= EHGGPerkFlags.PERMANENT
        else:
            self.flags &= ~EHGGPerkFlags.PERMANENT

    def load_from_meta(self, data) -> None:
        self.meta.load_from_meta(data)

    def longPrintValues(self) -> None:
        self.meta.longPrintValues(self)

    def displayValues(self):
        return repr(self.values)

    def __str__(self):
        return f'{self.id!r}: {self.values!r}'
