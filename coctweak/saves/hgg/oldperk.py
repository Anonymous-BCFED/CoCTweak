from typing import List
from .baseperk import HGGBasePerk, HGGBasePerkMeta

from coctweak.logsys import getLogger
log = getLogger(__name__)
class HGGOldPerkMeta(HGGBasePerkMeta):
    def __init__(self) -> None:
        super().__init__()
        self.meanings: List[str] = [f'Value {i+1}' for i in range(4)]

    def load_from_meta(self, data: dict) -> None:
        self.meanings = data.get('values', [f'Value {i+1}' for i in range(4)])
        super().load_from_meta(data)

    def longPrintValues(self, perk: HGGBasePerk) -> None:
        super().longPrintValues(perk)
        with log.info('Values:'):
            for i in range(4):
                meaning = self.meanings[i] if i < len(self.meanings) else None
                if meaning is None and 'fully-mapped' in self.flags:
                    meaning = 'Unused'
                if meaning is not None:
                    log.info(f'value{i+1} ({meaning}): {perk.values[i]!r}')
                else:
                    log.info(f'value{i+1}: {perk.values[i]!r}')
class HGGOldPerk(HGGBasePerk):
    def __init__(self) -> None:
        super().__init__()
        self.meta = HGGOldPerkMeta()

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        if 'values' in data:
            self.values = data['values'][0:4]
            self.uses_overhauled_perks = True
        else:
            # Ascension locks-in some perks. They use v4 as the flag.
            if data['value4'] > 0:
                self.isPermanent = True
                #data['value4'] = 0

            self.values = [data['value'+str(i+1)] for i in range(4)]
            self.uses_overhauled_perks = False

    def serialize(self, use_overhauled_schema: bool) -> dict:
        data: dict = super().serialize(use_overhauled_schema)

        values = self.values[0:4]

        # Deprecated - Dec 19 2020 (See https://gitgud.io/BelshazzarII/CoCAnon_mod/-/merge_requests/81)
        if self.isPermanent:
            values[3] = 1
        if not use_overhauled_schema:
            for i in range(4):
                data[f'value{i+1}']=values[i]
        else:
            data['values'] = values
        return data
