from coctweak.saves.common.piercing import BasePiercing
from coctweak.saves.hgg.enums.piercingtypes import HGGPiercingType
class HGGPiercing(BasePiercing):
    ENUM_TYPES = HGGPiercingType
