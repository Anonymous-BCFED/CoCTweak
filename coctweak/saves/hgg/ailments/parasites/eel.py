from coctweak.saves._ailment import Parasite
from typing import List, Dict, Any
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._perk import Perk

from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.enums.perklib import HGGPerkLib
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.pregnancytypes import HGGPregnancyType
from coctweak.logsys import getLogger
log = getLogger(__name__)
class EelParasite(Parasite):
    ID = 'eelparasite'
    NAME: str = 'Eel'

    STATUS_EFFECTS: List[str] = [
        HGGStatusLib.ParasiteEel.value,
        HGGStatusLib.ParasiteEelNeedCum.value,
        HGGStatusLib.ParasiteEelReproduction.value]
    PERKS: List[str] = [HGGPerkLib.ParasiteQueen.value]
    KFLAGS: List[int] = []

    def addToPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/InfestedChameleonGirlScene.as: private function eelInfest():void { @ 02kcYU5GQ7i51bY6ZdcDBghacf78uD90P0uI2uE83o7eDaclapyaatf9rb8CcSaanceIA3fCbaLe5I1vi0RmfiC9qLbRZg6x
        #player.createStatusEffect(StatusEffects.ParasiteEel, 1, 0, 0, 0);
        self.addStatusEffect(HGGStatusLib.ParasiteEel, [1, 0, 0, 0])
        #if (player.hasStatusEffect(StatusEffects.ParasiteNephila)) {
        #outputText("[pg]The slimes living inside you are effortlessly killed by the new parasite.");
        #player.removeStatusEffect(StatusEffects.ParasiteNephila);
        #player.removeStatusEffect(StatusEffects.ParasiteNephilaNeedCum);
        #flags[kFLAGS.NEPHILA_COVEN_QUEEN_CROWNED] = 0;
        #player.removePerk(PerkLib.NephilaQueen);
        #player.removePerk(PerkLib.NephilaArchQueen);
        #}

    def removeFromPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/InfestedChameleonGirlScene.as: public function parasiteUpdate():Boolean { @ bffasR20g10h63u5vg3Jy6zV0LR4IZffg71i4rZcEM5E9cis7699RxaHv3yh1Oifzm7F6dpa2Nbeqz0pE25K22KcTc21ufnY

        #outputText("[pg]Without a womb to house the parasites, they wither and die. You wonder how your body will get rid of them, but remember you just lost a womb without any pain or side effects; they'll disappear, somehow.");
        #player.removeStatusEffect(StatusEffects.ParasiteEel);
        self.removeStatusEffect(save, HGGStatusLib.ParasiteEel)
        #player.removeStatusEffect(StatusEffects.ParasiteEelNeedCum);
        self.removeStatusEffect(save, HGGStatusLib.ParasiteEelNeedCum)
        #if (player.hasPerk(PerkLib.ParasiteQueen)) player.removePerk(PerkLib.ParasiteQueen);
        self.removePerk(save, HGGPerkLib.ParasiteQueen)
        #retorno = true;


        if self.save.pregnancy.type == HGGPregnancyType.WORM_STUFFED.value:
            # Let's also clear the pregnancy.
            self.removeStatusEffect(HGGStatusLib.WormPlugged)
            with log.info('Clearing pregnancy...'):
                self.save.pregnancy.clear()

        # I noticed this was sloppily left in place. - A
        if save.hasStatusEffect(HGGStatusLib.ParasiteEelReproduction):
            self.removeStatusEffect(save, HGGStatusLib.ParasiteEelReproduction)
