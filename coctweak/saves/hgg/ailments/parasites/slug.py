from coctweak.saves._ailment import Parasite
from typing import List, Dict, Any
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._perk import Perk

from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.enums.perklib import HGGPerkLib
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.pregnancytypes import HGGPregnancyType
from coctweak.logsys import getLogger
log = getLogger(__name__)
class SlugParasite(Parasite):
    ID: str = 'slugparasite'
    NAME: str = 'Slug'
    AFFLICTED: str = 'Infected'

    STATUS_EFFECTS: List[str] = [
        HGGStatusLib.ParasiteSlug.value,
        HGGStatusLib.ParasiteSlugMatureDay.value,
        HGGStatusLib.ParasiteSlugMusk.value,
        HGGStatusLib.ParasiteSlugReproduction.value]
    PERKS: List[str] = []
    KFLAGS: List[int] = [
        # This is used to detect whether you had them ever.
        #HGGKFlags.HADSLUGPARASITE.value,
    ]


    def addToPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Bog/ChameleonGirl.as: override public function won(hpVictory:Boolean, pcCameWorms:Boolean = false):void { @ a4z6RweR2e1F3kWgI62SB5cM8AB2H4dSjcHI0Op6FG4Ym1iwapj6j4b8vdeEcgXaE71EbddUcC77HZ2Vn3iN88m6f2bfC5cK
        #//40% chance of getting infected by the bog parasite if fully worms are on. 20% if worms are "half", none if worms are off, the player doesn't have a penis or if he's already infested by something.
        #if (player.hasCock() && !player.hasStatusEffect(StatusEffects.ParasiteSlug) && !player.hasPerk(PerkLib.ParasiteMusk) && !player.hasStatusEffect(StatusEffects.Infested) && randomChance(game.parasiteRating*20)) {
        #player.createStatusEffect(StatusEffects.ParasiteSlug, 72, 0, 0, 0);
        self.createStatusEffect(HGGStatusLib.ParasiteSlug, [72, 0, 0, 0])
        #}

    def removeFromPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        # This is how Jojo does it. Quick and easy.
        ## from [HGG]/classes/classes/Scenes/NPCs/JojoScene.as: public function wormRemoval():void { @ 3Ia7C2eAu3Qa8513L1a0deg33vMd8m3CI0EU9SWaqVdn0f1I3IcaI0fU31Px1bS1To1sl4PIgfPcHr7TEff6c7Ogiqa2F6AB

        # Prose, graphics.
        #jojoSprite();
        #clearOutput();
        #outputText("[say: Excellent, young one,] Jojo continues. [say: Your dedication to purification is admirable. Relax and know that the parasites will leave you soon.][pg]");
        #outputText("Jojo gets up and walks over to a backpack hidden in the bushes. He removes a lacquered box. He removes and combines a rather noxious combination of herbs, oils and other concoctions into a mortar and grinds it with a pestle. After a few minutes, he ignites the mixture and uses a feathered fan to blow the fumes over you. The smell of the mix is nauseating and repugnant. Your stomach turns and you fight the urge to vomit. Eventually, you are no longer able to resist and you purge yourself onto the ground. Cramping from your vomiting fits, you wrack with discomfort, which slowly builds to genuine pain.");
        #if (player.hasStatusEffect(StatusEffects.Infested)) outputText(" As the pain sets in, you feel a stirring deep in your crotch. The worms inside you are stirring and thus are compelling another unwanted orgasm. Unable to control your body, your cock explodes, launching cum and worms everywhere. Jojo begins fanning faster as he sees the worms leave your body.[pg]");
        #outputText("[say: Further endurance is needed, young one,] Jojo says. [say: The root of your problem must leave before you may pursue further purification. Healing is always twice as uncomfortable as the illness requiring attention.][pg]");
        #outputText("Your body cramps up as you feel the fat worm struggle. You feel it pushing up your urethra, fighting to escape your fumigated body. The worm rapidly peeks from the end of your penis. With expedience, Jojo quickly grabs the worm and pulls it out of you, triggering one last orgasm. The monk casts the fat worm to the ground and strikes it dead with his staff.[pg]");
        #outputText("[say: The culprit has been exorcised and will no longer trouble you. Rest here for a while and join me in some meditation to heal your exhausted body and soul.][pg]");
        #outputText("Being too tired for anything else, you join Jojo in meditation, which does much to relive you of your former woes.");

        if change_stats:
            #//Infestation removed. HP reduced to 50% of MAX. Sensitivity reduced by -25 or reduced to 10, which ever is the smaller reduction.
            #//Infestation purged. Hit Points reduced to 10% of MAX. Corruption -20.
            #if (player.HP > int(player.maxHP() * .5)) player.HP = int(player.maxHP() * .5);
            if save.combat_stats.hp.value > int(save.combat_stats.hp.max * 0.5):
                save.combat_stats.hp.value = int(save.combat_stats.hp.max * 0.5)

            #player.damageHunger(30);
            save.combat_stats.hunger.value -= 30
            # I think this is a bug. Probably meant to write  += 11.  I'll check with OCA. - A
            #player.sens = 11;
            save.stats.sensitivity.value = 11

        #player.removeStatusEffect(StatusEffects.ParasiteSlug);
        self.removeStatusEffect(HGGStatusLib.ParasiteSlug)
        #player.removeStatusEffect(StatusEffects.Infested);
        #player.removePerk(PerkLib.ParasiteMusk);
        self.removePerk(HGGPerkLib.ParasiteMusk)
        #player.removeStatusEffect(StatusEffects.ParasiteSlugReproduction);
        self.removeStatusEffect(HGGStatusLib.ParasiteSlugReproduction)
        #dynStats("sens=", Math.max(player.sens - 25, 10), "lus", -99, "cor", -15);
        if change_stats:
            save.stats.sensitivity.value = max(save.stats.sensitivity.value - 25., 10.)
            save.combat_stats.lust.value -= 99
            save.stats.corruption.value -= 15
        #player.orgasm('Generic');
        #doNext(camp.returnToCampUseOneHour);

        # Bug - Not removed
        self.removeStatusEffect(HGGStatusLib.ParasiteSlugMatureDay)

    @property
    def infestationTime(self) -> int:
        sfx = self.save.getStatusEffect(HGGStatusLib.ParasiteSlug)
        if sfx is None:
            return -1
        else:
            return sfx.values[0]
    def isReadyToReproduce(self) -> bool:
        return self.save.hasStatusEffect(HGGStatusLib.ParasiteSlugReproduction)
    def isProducingMusk(self) -> bool:
        return self.save.hasPerk(HGGPerkLib.ParasiteMusk)
    def daysUntilMature(self) -> int:
        sfx = self.save.getStatusEffect(HGGStatusLib.ParasiteSlugMatureDay)
        if sfx is None:
            return -1
        return sfx.values[0]

    def displayState(self) -> None:
        log.info(f'Hours until fully infected: {self.infestationTime}')
        log.info(f'Ready to reproduce: {self.isReadyToReproduce()!r}')
        log.info(f'Producing Musk: {self.isProducingMusk()!r}')
        if self.isProducingMusk():
            log.info(f'Days until mature: {self.daysUntilMature()!r}')
