from .worms import WormsParasite
from .eel import EelParasite
from .slug import SlugParasite
from .nephila import NephilaParasite

__ALL__ = ['WormsParasite', 'EelParasite', 'SlugParasite', 'NephilaParasite']
