from coctweak.saves._ailment import Parasite
from typing import List, Dict, Any, Optional, Union
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._perk import Perk

from enum import IntEnum

from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.enums.perklib import HGGPerkLib
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.pregnancytypes import HGGPregnancyType
from coctweak.logsys import getLogger
log = getLogger(__name__)

class ENephilaCumType(IntEnum):
    ## from [HGG]/classes/classes/PlayerInfo.as: switch (player.statusEffectv2(StatusEffects.ParasiteNephilaNeedCum)) { @ 88ldE076510k1M40buakYf135gn5C79HZ1Rwcui1nSczMbJOgWf7Jj3wb3bL05VeItdPn8BL3rSebZ7ko1jvgFY8Tc4VKdEE
    NONE = 0
    #case 1:
    #statEffects += "Nephila parasites hunger for " + Math.round(player.statusEffectv3(StatusEffects.ParasiteNephilaNeedCum)) + " liters of <b>imp</b> cum.\n";
    #break;
    IMP = 1
    #case 4:
    #statEffects += "Nephila parasites hunger for " + Math.round(player.statusEffectv3(StatusEffects.ParasiteNephilaNeedCum)) + " liters of <b>mouse</b> cum.\n";
    #break;
    MOUSE = 4
    #case 2:
    #statEffects += "Nephila parasites hunger for " + Math.round(player.statusEffectv3(StatusEffects.ParasiteNephilaNeedCum)) + " liters of <b>minotaur</b> cum.\n";
    #break;
    MINOTAUR = 2
    #case 10:
    #statEffects += "Nephila parasites hunger for " + Math.round(player.statusEffectv3(StatusEffects.ParasiteNephilaNeedCum)) + " liters of <b>anemone</b> cum.\n";
    #break;
    ANEMONE = 10

class NephilaParasite(Parasite):
    ID: str = 'nephilaparasite'
    NAME: str = 'Nephila'

    STATUS_EFFECTS: List[str] = [
        HGGStatusLib.ParasiteNephila.value,
        HGGStatusLib.ParasiteNephilaNeedCum.value
    ]
    PERKS: List[str] = [
        HGGPerkLib.NephilaQueen.value,
        HGGPerkLib.NephilaArchQueen.value,
    ]
    KFLAGS: List[int] = [
        HGGKFlags.PARASITE_NEPHILA_DAYDONE.value,
        HGGKFlags.NEPHILA_COVEN_QUEEN_CROWNED.value,
    ]

    def addToPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Mountain/NephilaSlimeScene.as: private function nephilaInfest():void { @ dSAaSi5TMdbI6Gi35V9uR9Qt09D6Rd7rQh0Hc8u1Uk6zs6zegBa9AGg9i8XZ7IE09Vc9I1B10whfvybL84TLg26bIX2X8aWL
		#player.createStatusEffect(StatusEffects.ParasiteNephila, 1, 0, 0, 0);
        self.save.addStatusEffect(HGGStatusLib.ParasiteNephila, [1, 0, 0, 0])

    def removeFromPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Mountain/NephilaSlimeScene.as: public function nephilaUpdate():Boolean { @ elj1AO0tw7rNeLd50D1rZfDb4Y64kZ4vc8m6fZE7B7ciB0X750TeMq0z27XP5R7979bqR88DgW4dYp1XHfd0bqheqkgZEf7Q
        super().removeFromPlayer(save, change_stats)

    def setAmount(self, value: int) -> None:
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteNephila):
            self.save.getStatusEffect(HGGStatusLib.ParasiteNephila).values[0] = value
    def getAmount(self) -> Optional[int]:
        if not self.save.hasStatusEffect(HGGStatusLib.ParasiteNephila):
            return None
        return int(self.save.getStatusEffect(HGGStatusLib.ParasiteNephila).values[0])

    def getReproductionTime(self) -> Optional[int]:
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteNephila):
            return int(self.save.getStatusEffect(HGGStatusLib.ParasiteNephila).values[1])
        else:
            return None
    def setReproductionTime(self, value: int) -> None:
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteNephila):
            self.save.getStatusEffect(HGGStatusLib.ParasiteNephila).values[1] = value

    def getLitersOfDesiredCum(self) -> Optional[int]:
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum):
            return int(self.save.getStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum).values[0])
        else:
            return None
    def setLitersOfDesiredCum(self, value: int) -> None:
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum):
            self.save.getStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum).values[0] = value

    def getTypeOfDesiredCum(self) -> Optional[ENephilaCumType]:
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum):
            return int(self.save.getStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum).values[1])
        else:
            return None
    def setTypeOfDesiredCum(self, value: Optional[Union[int, ENephilaCumType]]) -> None:
        if value is None:
            if self.hasStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum):
                del self.save.statusEffects[HGGStatusLib.ParasiteNephilaNeedCum.value]
        else:
            if not self.save.hasStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum):
                self.save.addStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum)
            self.save.getStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum).values[1] = value


    def getStats(self) -> List[str]:
        o = []
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteNephila):
            o += [f'{self.getAmount()} slime(s) present']
            o += [f'repro at day {self.getReproductionTime()}']
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum):
            o += [f'hungry for {self.getLitersOfDesiredCum()}L of {self.getTypeOfDesiredCum().name} cum']
        #o += [f'Values: {se.values!r}']
        #o += [f'dataStore: {se.dataStore!r}']
        return o

    def displayState(self) -> None:
        for stat in self.getStats():
            log.info(stat)

    def __str__(self) -> str:
        stats = ', '.join(self.getStats())
        return f'{self.NAME} (VAGINAL) - {stats}'
