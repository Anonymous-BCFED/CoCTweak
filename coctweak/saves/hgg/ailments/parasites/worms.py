from coctweak.saves._ailment import Parasite
from typing import List, Dict, Any
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._perk import Perk

from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.pregnancytypes import HGGPregnancyType
from coctweak.logsys import getLogger
log = getLogger(__name__)

class WormsParasite(Parasite):
    ID: str = 'wormsparasite'
    NAME: str = 'Worms'

    STATUS_EFFECTS: List[str] = [HGGStatusLib.WormPlugged.value, HGGStatusLib.Infested.value]
    PERKS: List[str] = []
    KFLAGS: List[int] = [HGGKFlags.PLAYER_PREGGO_WITH_WORMS]

    def addToPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        ## from [HGG]/classes/classes/Scenes/Areas/Mountain/WormsScene.as: public function infest1():void { @ 1kQ5sq8KqcuvgL4arM5Q238XfUl5aq5cl3lfe6N93w5FF0wu0yu7vo68BeLt9GZgYcdZwbCj6Su36v3Cb9BX0hUcJf25c5BV
        # spriteSelect(SpriteDb.s_dickworms);
		# outputText("Trapped within the mass of worms, you are utterly helpless. The constant moving all over your body provides naught but unwanted stimulation. Your cock, not knowing any better, springs to attention, creating a peak in the mass. The worms immediately recognize what has happened to you. One particularly fat worm finds itself perched on top of your dick's throbbing glans. You feel it prodding about your urethral opening and come to a horrible realization that your precious penis is completely vulnerable to thousands of creatures capable of invading your body! Before you can react or curse your fate, the fat worm quickly forces open your urethra and begins to push its way inside your dick![pg]");
		# outputText("Crying out in shock, you feel the fat worm push its way, inch by inch, into your urethra. Your nerves light up like a Christmas tree as each individual cell tells you of the creature's presence and movement deeper into your body. The fat beast easily finds its way into your prostate and settles within the organ. As it settles, it begins flailing inside your sex. The sensations shift from shock to grotesque pleasure as your body only senses the stimulation conductive to orgasmic response. Your groin cramps and bloats quickly by the torrent of semen building within you and the invader's presence. Obviously sensitive to your fluids, you feel the worm thrash around some more, causing your body to respond by making more semen. The flopping creature quickly erodes any orgasmic discipline you are capable of and with a great shrill cry, you force lances of cum into the air, launching goo and worms alike in a sick display of forced pleasure. After you empty your body of spunk, the remaining worms become hyperaggressive.[pg]");
		# outputText("Excited by the feel of your fluids on them, many smaller worms push their way into your penis. Your cock distends as the worms fight to get inside you and to the source of the milk that has so excited them. Your prostate quickly fills up with the squirming creatures. The discomfort in your bloated bludgeon and the ceaseless stimulation of your organs causes your body to produce more cum. However, you find yourself unable to climax as the invaders rest inside your body submerged in your salty lust. The rest of the colony disperses, having accomplished its true goal of infesting your body.[pg]");
        if change_stats:
            # if (player.cor < 25) {
            if save.stats.corruption.value > 25:
                # +1 corruption, and then hard-set it to 25. WTF.
                # This whole logic block is a big wat. - A
                #dynStats("cor", 1);
                #player.cor = 25;
                log.info('Setting corruption to 25...')
                save.stats.corruption.value = 25
            #}
        # //trace("GET INFESTED HERE")
        # if (!player.hasStatusEffect(StatusEffects.Infested)) {
        if not save.hasStatusEffect(HGGStatusLib.Infested):
			# if (flags[kFLAGS.EVER_INFESTED] < 1) flags[kFLAGS.EVER_INFESTED] = 1;
            self.setKFlag(HGGKFlags.EVER_INFESTED, 1)
			# player.createStatusEffect(StatusEffects.Infested, 0, 0, 0, 0);
            self.addStatusEffect(HGGStatusLib.Infested, [0, 0, 0, 0])
			# if (player.hasPerk(PerkLib.ParasiteMusk) || player.hasStatusEffect(StatusEffects.ParasiteSlug)) {
            if save.hasPerk(HGGPerkLib.ParasiteMusk) or save.hasStatusEffect(HGGStatusLib.ParasiteSlug):
				# outputText("The aggressive worms have certainly destroyed the parasite that lived inside your prostate.[pg]");
				# player.removePerk(PerkLib.ParasiteMusk);
                self.removePerk(HGGPerkLib.ParasiteMusk)
				# player.removeStatusEffect(StatusEffects.ParasiteSlug);
                self.removeStatusEffect(HGGStatusLib.ParasiteSlug)
				# player.removeStatusEffect(StatusEffects.ParasiteSlugReproduction);
                self.removeStatusEffect(HGGStatusLib.ParasiteSlugReproduction)
			# }
			# dynStats("cor", 0);
		# }
		# combat.cleanupAfterCombat();
        

    def removeFromPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        # This is how Jojo does it. Quick and easy.
        ## from [HGG]/classes/classes/Scenes/NPCs/JojoScene.as: public function wormRemoval():void { @ 3Ia7C2eAu3Qa8513L1a0deg33vMd8m3CI0EU9SWaqVdn0f1I3IcaI0fU31Px1bS1To1sl4PIgfPcHr7TEff6c7Ogiqa2F6AB

        # Skipping prose...

        if change_stats:
            #//Infestation removed. HP reduced to 50% of MAX. Sensitivity reduced by -25 or reduced to 10, which ever is the smaller reduction.
            #//Infestation purged. Hit Points reduced to 10% of MAX. Corruption -20.
            #if (player.HP > int(player.maxHP() * .5)) player.HP = int(player.maxHP() * .5);
            halfMax = int(self.save.combat_stats.hp.max * 0.5)
            if self.save.combat_stats.hp.value > halfMax:
                self.save.combat_stats.hp.value = float(halfMax)
            #player.damageHunger(30);
            self.save.combat_stats.hunger.value -= 30
        #player.removeStatusEffect(StatusEffects.ParasiteSlug);
        #player.removeStatusEffect(StatusEffects.Infested);
        self.removeStatusEffect(HGGStatusLib.Infested)
        #player.removePerk(PerkLib.ParasiteMusk);
        #player.removeStatusEffect(StatusEffects.ParasiteSlugReproduction);
        if change_stats:
            #dynStats("sens=", Math.max(player.sens - 25, 10), "lus", -99, "cor", -15);
            self.save.stats.sensitivity.value = max(self.save.stats.sensitivity.value - 25., 10.)
            self.save.stats.lust.value -= 99
            self.save.stats.corruption.value -= 15
        #player.orgasm('Generic');
        #doNext(camp.returnToCampUseOneHour);

        if self.save.pregnancy.type == HGGPregnancyType.WORM_STUFFED.value:
            # Let's also clear the pregnancy.
            self.removeStatusEffect(HGGStatusLib.WormPlugged)
            with log.info('Clearing pregnancy...'):
                self.save.pregnancy.clear()

    def getOrifaces(self) -> List[str]:
        o = []
        if self.save.hasStatusEffect(HGGStatusLib.WormPlugged):
            o.append('VAGINAL')
        if self.save.hasStatusEffect(HGGStatusLib.Infested):
            o.append('PENILE')
        return o

    def getStats(self) -> List[str]:
        o = []
        if self.save.hasStatusEffect(HGGStatusLib.WormPlugged):
            o += [f'{self.save.pregnancy.incubation}h uterine incubation left']
        if self.save.hasStatusEffect(HGGStatusLib.Infested):
            se = self.save.getStatusEffect(HGGStatusLib.Infested)
            o += [f'stage {se.values[0]+1} penile infection']
        #o += [f'Values: {se.values!r}']
        #o += [f'dataStore: {se.dataStore!r}']
        return o

    def displayState(self) -> None:
        for stat in self.getStats():
            log.info(stat)

    def __str__(self) -> str:
        orifaces = ', '.join(self.getOrifaces())
        stats = ', '.join(self.getStats())
        return f'{self.NAME} ({orifaces}) - {stats}'
