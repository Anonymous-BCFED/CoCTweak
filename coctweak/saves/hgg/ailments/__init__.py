from .addictions import MinotaurCumAddiction, MarbleMilkAddiction
from .parasites import EelParasite, SlugParasite, WormsParasite, NephilaParasite

__ALL__ = [
    'EelParasite',
    'NephilaParasite',
    'SlugParasite',
    'WormsParasite',

    'MarbleMilkAddiction',
    'MinotaurCumAddiction',
]
