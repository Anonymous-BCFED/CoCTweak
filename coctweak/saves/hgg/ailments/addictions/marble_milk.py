from coctweak.saves._ailment import Addiction
from typing import List, Dict, Any
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._perk import Perk
from coctweak.utils import clamp

from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.enums.perklib import HGGPerkLib
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.pregnancytypes import HGGPregnancyType
from coctweak.logsys import getLogger
log = getLogger(__name__)
class MarbleMilkAddiction(Addiction):
    ID = 'marblemilkaddiction'
    NAME: str = 'Marble\'s Milk'

    STATUS_EFFECTS: List[str] = [HGGStatusLib.MarbleWithdrawl.value]
    PERKS: List[str] = [HGGPerkLib.MarblesMilk.value]
    KFLAGS: List[int] = []

    def addToPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        return

    def removeFromPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        self.removePerk(HGGPerkLib.MarblesMilk)
        self.removeStatusEffect(HGGStatusLib.MarbleWithdrawl)
        marble = self.save.getStatusEffect(HGGStatusLib.Marble)
        oldval = marble.values[1]
        newval = 0
        marble.values[1] = newval
        log.info('SFX Marble[1] (marbleAddiction): {oldval} -> {newval}')
