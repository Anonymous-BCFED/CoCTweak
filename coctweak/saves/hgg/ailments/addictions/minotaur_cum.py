from coctweak.saves._ailment import Addiction
from typing import List, Dict, Any
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._perk import Perk
from coctweak.utils import clamp

from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.enums.perklib import HGGPerkLib
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.pregnancytypes import HGGPregnancyType
from coctweak.logsys import getLogger
log = getLogger(__name__)
class MinotaurCumAddiction(Addiction):
    ID = 'minotaurcumaddiction'
    NAME: str = 'Minotaur Cum'

    STATUS_EFFECTS: List[str] = []
    PERKS: List[str] = [HGGPerkLib.MinotaurCumAddict.value]
    KFLAGS: List[int] = [
        HGGKFlags.MINOTAUR_CUM_ADDICTION_TRACKER.value,
        HGGKFlags.MINOTAUR_CUM_ADDICTION_STATE.value,
        HGGKFlags.MINOTAUR_CUM_REALLY_ADDICTED_STATE.value,
    ]

    def addToPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        #Simulate consuming taur cum

        raw: float = kwargs.get('consumed', 2)

        ##from [HGG]/classes/classes/Player.as: public function minoCumAddiction(raw:Number = 10):void {//Increment minotaur cum intake count
        intake_count: int = int(save.flags.get(HGGKFlags.MINOTAUR_CUM_INTAKE_COUNT))
        tracker: int = int(save.flags.get(HGGKFlags.MINOTAUR_CUM_ADDICTION_TRACKER))
        state: int = int(save.flags.get(HGGKFlags.MINOTAUR_CUM_ADDICTION_STATE))
        last_consumed: int = int(save.flags.get(HGGKFlags.TIME_SINCE_LAST_CONSUMED_MINOTAUR_CUM))

        #flags[kFLAGS.MINOTAUR_CUM_INTAKE_COUNT]++;
        intake_count += 1

        #//Fix if variables go out of range.
        #if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] < 0) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] = 0;
        #if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] < 0) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] = 0;
        #if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] > 120) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] = 120;
        tracker = clamp(tracker, 0, 120)
        state = max(state, 0)

        #//Turn off withdrawal
        #//if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] > 1) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] = 1;
        #//Reset counter
        #flags[kFLAGS.TIME_SINCE_LAST_CONSUMED_MINOTAUR_CUM] = 0;
        last_consumed = 0

        #if (!game.addictionEnabled) { //Disables addiction if set to OFF.
        #    flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] = 0;
        #    flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] = 0;
        #    return;
        #}
        #//If highly addicted, rises slower
        #if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] >= 60 && raw > 0) raw /= 2;
        if tracker >= 60 and raw > 0:
            raw /= 2
        #if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] >= 80 && raw > 0) raw /= 2;
        if tracker >= 80 and raw > 0:
            raw /= 2
        #if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] >= 90 && raw > 0) raw /= 2;
        if tracker >= 90 and raw > 0:
            raw /= 2
        #if (hasPerk(PerkLib.MinotaurCumResistance)) raw *= 0;
        if tracker >= 60 and raw > 0:
            raw = 0
        #//If in withdrawal, readdiction is potent!
        #if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] == 3 && raw > 0) raw += 10;
        if state == 3 and raw > 0:
            raw += 10
        #if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] == 2 && raw > 0) raw += 5;
        if state == 2 and raw > 0:
            raw += 5
        #raw = Math.round(raw * 100) / 100;
        raw = round(raw * 100) / 100
        #//PUT SOME CAPS ON DAT' SHIT
        #if (raw > 50) raw = 50;
        #if (raw < -50) raw = -50;
        raw = clamp(raw, -50, 50)
        #flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] += raw;
        tracker += raw
        #//Recheck to make sure shit didn't break
        #if (hasPerk(PerkLib.MinotaurCumResistance)) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] = 0; //Never get addicted!
        if save.hasPerk(HGGPerkLib.MinotaurCumResistance):
            log.warning(f'Perk {HGGPerkLib.MinotaurCumResistance.value} is present. You will be unable to get addicted until the perk is removed.')
            tracker = 0
        #if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] > 120) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] = 120;
        #if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] < 0) flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] = 0;
        tracker = clamp(tracker, 0, 120)

        #self.setKFlag(HGGKFlags.MINOTAUR_CUM_ADDICTION_TRACKER, kwargs.pop('tracker_value', 60)) # Min 50
        #self.setKFlag(HGGKFlags.MINOTAUR_CUM_ADDICTION_STATE, 1)
        self.setKFlag(HGGKFlags.MINOTAUR_CUM_ADDICTION_STATE, state)
        self.setKFlag(HGGKFlags.MINOTAUR_CUM_ADDICTION_TRACKER, tracker)
        self.setKFlag(HGGKFlags.MINOTAUR_CUM_INTAKE_COUNT, intake_count)
        self.setKFlag(HGGKFlags.TIME_SINCE_LAST_CONSUMED_MINOTAUR_CUM, last_consumed)

    def removeFromPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        ##from [HGG]/classes/classes/Scenes/Areas/Mountain/MinotaurScene.as: if (flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] < 50) {
        #flags[kFLAGS.MINOTAUR_CUM_ADDICTION_TRACKER] = 0;
        self.setKFlag(HGGKFlags.MINOTAUR_CUM_ADDICTION_TRACKER, 0)

        #flags[kFLAGS.MINOTAUR_CUM_ADDICTION_STATE] = 0;
        self.setKFlag(HGGKFlags.MINOTAUR_CUM_ADDICTION_STATE, 0)

        self.setKFlag(HGGKFlags.MINOTAUR_CUM_REALLY_ADDICTED_STATE, 0)

        self.removePerk(HGGPerkLib.MinotaurCumAddict)
