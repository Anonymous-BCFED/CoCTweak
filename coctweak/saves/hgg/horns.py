from coctweak.saves.hgg.enums.horntypes import HGGHornType
from coctweak.saves._horns import BaseHorns

class HGGHorns(BaseHorns):
    ENUM_TYPES = HGGHornType
