from coctweak.saves._saveobj import SaveObject
from coctweak.saves.common.upgrade import UpgradeCollection
from coctweak.saves.common.upgrades.coctweakdata import ALL_UPGRADES
from typing import List, Any, Optional, Tuple

from coctweak.logsys import getLogger
log = getLogger(__name__)

class BackpackHack(SaveObject):
    VERSION = 1

    def __init__(self):
        super().__init__()
        self.is_hacked: bool = False
        self.backed_up: Optional[bool] = None
        self.had_backpack: Optional[bool] = None
        self.old_data: Optional[List[Any]] = None
        self.desired_size: Optional[int] = None

    def reset(self) -> None:
        self.is_hacked = False
        self.desired_size = None
        self.backed_up = None
        self.had_backpack = None
        self.old_data = None

    def setHacked(self, newsz: int) -> None:
        if not self.backed_up:
            self.reset()
        self.is_hacked = True
        self.desired_size = newsz

    def unhack(self) -> None:
        self.is_hacked = False

    def serialize(self) -> dict:
        data = {}
        if self.is_hacked:
            data['is-hacked'] = True
            if self.desired_size is not None:
                data['desired-size'] = self.desired_size
                if self.backed_up:
                    # This just tells the game to backup the backpack.
                    data['backed_up']    = self.backed_up
                if self.is_hacked:
                    data['had-backpack'] = self.had_backpack
                    data['old-data']     = self.old_data
        return data

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.is_hacked = self._getBool('is-hacked', False)
        self.desired_size = self._getInt('desired-size', None)
        if self.desired_size is not None:
            self.is_hacked = self._getBool('backed-up', False)
            if self.is_hacked:
                self.had_backpack = self._getBool('had-backpack', False)
                self.old_data = self._getRaw('old-data', None)

    def show(self) -> None:
        with log.info('Backpack hack:'):
            log.info('Is Hacked: %s', 'Y' if self.is_hacked else 'N')
            if self.is_hacked:
                log.info('Had Backpack: %s', 'Y' if self.had_backpack else 'N')
                log.info('Backed-up backpack size: %s', 'N/A (None)' if self.old_data is None else self.old_data[0])
                log.info('Desired inventory size: %d', self.desired_size)

SCHEMA_UPDATES = UpgradeCollection('CoCTweak Data', ALL_UPGRADES)
class CoCTweakData(SaveObject):
    '''
    This all gets crammed into a flag.
    '''
    VERSION = 2
    FLAG_ID = 0 # kFlag ID, set by subclasses.
    def __init__(self):
        super().__init__()
        self.backpack_hack: BackpackHack = BackpackHack()

    def deserialize(self, data) -> None:
        super().deserialize(data)
        SCHEMA_UPDATES.execute(data)
        self.backpack_hack = BackpackHack()
        self.backpack_hack.deserialize(data.get('backpack-hack', {}))

    def serialize(self) -> dict:
        ds = {'version':self.VERSION}
        if self.backpack_hack.is_hacked:
            ds['backpack-hack'] = self.backpack_hack.serialize()
        return ds

    def show(self) -> None:
        log.info('Schema version: %d', self.VERSION)
        self.backpack_hack.show()
        return
