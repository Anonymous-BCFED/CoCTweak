from typing import TypeVar,Union,Generic,Type,Optional
from enum import IntEnum
from abc import ABC, abstractmethod

from coctweak.utils import clamp

from coctweak.logsys import getLogger
log = getLogger(__name__)

K = TypeVar('K', bound=IntEnum) # KFlag Type

class IBreastStore(object):
    ENUM_CUPSIZE: Type[IntEnum] = None
    ENUM_LACTATIONLEVEL: Type[IntEnum] = None

    GLOBAL_MIN_CUP_SIZE: IntEnum = None
    GLOBAL_MAX_CUP_SIZE: IntEnum = None

    GLOBAL_MIN_LACTATION_LEVEL: IntEnum = None
    GLOBAL_MAX_LACTATION_LEVEL: IntEnum = None

    def __init__(self, save, isMale: bool) -> None:
        self.save = save
        self.isMale: bool = isMale

        self.min_cup_size: Optional[IntEnum] = self.GLOBAL_MIN_CUP_SIZE
        self.max_cup_size: Optional[IntEnum] = self.GLOBAL_MAX_CUP_SIZE

        self.min_lactation_level: Optional[IntEnum] = self.GLOBAL_MIN_LACTATION_LEVEL
        self.max_lactation_level: Optional[IntEnum] = self.GLOBAL_MAX_LACTATION_LEVEL

    def reset(self) -> None:
        self.rows = -1
        self.cupSize = 0
        self.lactationLevel = 0
        self.nippleLength = 0
        self.fullness = 0
        self.timesMilked = 0
        self.preventLactationIncrease = 0
        self.preventLactationDecrease = 0

    def set_cup_size_bounds(self, _min: Union[int, IntEnum], _max: Union[int, IntEnum]) -> 'IBreastStore':
        self.min_cup_size = _min if isinstance(_min, IntEnum) else self.ENUM_CUPSIZE(_min)
        self.max_cup_size = _max if isinstance(_max, IntEnum) else self.ENUM_CUPSIZE(_max)
        return self

    def set_lactation_level_bounds(self, _min: Union[int, IntEnum], _max: Union[int, IntEnum]) -> 'IBreastStore':
        self.min_lactation_level = _min if isinstance(_min, IntEnum) else self.ENUM_LACTATIONLEVEL(_min)
        self.max_lactation_level = _max if isinstance(_max, IntEnum) else self.ENUM_LACTATIONLEVEL(_max)
        return self

    @property
    def rows(self) -> int:
        pass
    @rows.setter
    def rows(self, value: int) -> None:
        pass

    @property
    def cupSize(self) -> IntEnum:
        pass
    @cupSize.setter
    def cupSize(self, value: Union[int, IntEnum]) -> None:
        pass

    @property
    def lactationLevel(self) -> IntEnum:
        pass
    @lactationLevel.setter
    def lactationLevel(self, value: Union[int, IntEnum]) -> None:
        pass

    @property
    def nippleLength(self) -> float:
        pass
    @nippleLength.setter
    def nippleLength(self, value: float) -> None:
        pass

    @property
    def fullness(self) -> int:
        pass
    @fullness.setter
    def fullness(self, value: int) -> None:
        pass

    @property
    def timesMilked(self) -> int:
        pass
    @timesMilked.setter
    def timesMilked(self, value: int) -> None:
        pass

    @property
    def preventLactationIncrease(self) -> int:
        pass
    @preventLactationIncrease.setter
    def preventLactationIncrease(self, value: int) -> None:
        pass

    @property
    def preventLactationDecrease(self) -> int:
        pass
    @preventLactationDecrease.setter
    def preventLactationDecrease(self, value: int) -> None:
        pass

    def display(self) -> None:
        log.info(f'Rows:            {self.rows}')
        log.info(f'Cup size:        {self.cupSize.value} ({self.cupSize.name})')
        log.info(f'Lactation Level: {self.lactationLevel.value} ({self.lactationLevel.name})')
        log.info(f'Nipple Length:   {self.nippleLength}')
        log.info(f'Fullness:        {self.fullness}')
        log.info(f'# Times Milked:  {self.timesMilked}')
        log.info(f'Lactation min:   {self.preventLactationDecrease}')
        log.info(f'Lactation max:   {self.preventLactationIncrease}')
        with log.info('Calculated Fields:'):
            log.info(f'Can Tit-fuck: {self.canTitFuck()}')
            log.info(f'Has Breasts:  {self.hasBreasts()}')
            log.info(f'Is Lactating: {self.isLactating()}')
        with log.info('Description:'):
            log.info(self.description(useAdj=True))

    def breastDescript(self) -> str:
        return ''
    def adj(self) -> str:
        return ''
    def canTitFuck(self) -> bool:
        return False
    def cup(self) -> str:
        return ''
    def description(self, useAdj: bool = False) -> str:
        return ''
    def breastDesc(self) -> str:
        return ''
    def hasBreasts(self) -> bool:
        return False
        
    def isLactating(self) -> bool:
        return False
    def milkIsFull(self) -> bool:
        return False
    def milkIsOverflowing(self) -> bool:
        return False
    def milkQuantity(self) -> float:
        return 0.0
    def nippleDescript(self, tiny: str = 'tiny', small: str='prominent', large:str='large', huge:str='elongated', massive:str='massive') -> bool:
        return ''

class BaseSingleFlagBreastStore(IBreastStore):
    '''
    This is currently only used for Kath's boobs.

    Fen, if you're reading this: *headslap*
    '''
    LATEST_READER_VERSION: int = 1
    EARLIEST_PERMISSIBLE_READER_VERSION: int = 1

    def __init__(self, save, isMale: bool, dataFlag: Union[K, int]) -> None:
        super().__init__(save, isMale)

        self.dataFlag: Union[K, int] = dataFlag

        self.version: int = self.LATEST_READER_VERSION

        self.orig: str = ''

        self._rows: int = 1
        self._cupSize: int = 0
        self._lactationLevel: int = 0
        self._nippleLength: float = 0
        self._fullness: int = 0
        self._timesMilked: int = 0
        self._preventLactationIncrease: int = 0
        self._preventLactationDecrease: int = 0

        self._read: bool = False
        self._readCleanly: bool = True

    def reset(self) -> None:
        super().reset()
        self._read = False
        self._readCleanly = True

    def _loadFromSave(self) -> None:
        self.deserialize(self.save.flags.get(self.dataFlag, ''))

    def deserialize(self, raw: str) -> None:
        if self._read:
            return
        self._read = True
        self.orig = raw
        if raw == '':
            return

        self._readCleanly = True
        if '^' not in raw:
            with log.error(f'BreastController: Failed to read flag {self.flag} (does not contain "^").'):
                log.error(f'Data for this flag will not be processed or modified, in order to prevent save corruption.')
            self._readCleanly = False
            return

        chunks = raw.split('^')
        if len(chunks) > 9:
            with log.error(f'BreastController: Failed to read flag {self.flag} (len(chunks) == {len(chunks)}, must be 9).'):
                log.error(f'Data for this flag will not be processed or modified, in order to prevent save corruption.')
            self._readCleanly = False
            return
        if int(chunks[0]) < self.EARLIEST_PERMISSIBLE_READER_VERSION:
            with log.error(f'BreastController: Failed to read flag {self.flag} (version == {chunks[0]}).'):
                log.error(f'Data for this flag will not be processed or modified, in order to prevent save corruption.')
            self._readCleanly = False
            return

        self._rows = int(chunks[1])
        self._cupSize = int(chunks[2])
        self._lactationLevel = int(chunks[3])
        self._nippleLength = float(chunks[4])
        self._fullness = int(chunks[5])
        self._timesMilked = int(chunks[6])
        self._preventLactationIncrease = int(chunks[7])
        self._preventLactationDecrease = int(chunks[8])

    def serialize(self):
        if not self._readCleanly:
            return self.orig
        else:
            o=[
                self.LATEST_READER_VERSION,
                self._rows,
                self._cupSize,
                self._lactationLevel,
                f'{self._nippleLength:g}',
                self._fullness,
                self._timesMilked,
                self._preventLactationIncrease,
                self._preventLactationDecrease
            ]
            return '^'.join([str(x) for x in o])

    def _saveToSave(self) -> None:
        if not self._readCleanly:
            return
        self.save.flags.set(self.dataFlag, self.serialize())

    @property
    def rows(self) -> int:
        self._loadFromSave()
        return self._rows
    @rows.setter
    def rows(self, value: int) -> None:
        self._rows = value
        self._saveToSave()

    @property
    def cupSize(self) -> IntEnum:
        self._loadFromSave()
        return self.ENUM_CUPSIZE(self._cupSize)
    @cupSize.setter
    def cupSize(self, value: Union[int, IntEnum]) -> None:
        if isinstance(value, IntEnum):
            value = value.value
        value = clamp(value, self.min_cup_size.value, self.max_cup_size.value)
        self._cupSize = value
        self._saveToSave()

    @property
    def lactationLevel(self) -> IntEnum:
        self._loadFromSave()
        return self.ENUM_LACTATIONLEVEL(self._lactationLevel)
    @lactationLevel.setter
    def lactationLevel(self, value: Union[int, IntEnum]) -> None:
        if isinstance(value, IntEnum):
            value = value.value
        value = clamp(value, self.min_lactation_level.value, self.max_lactation_level.value)
        self._lactationLevel = value
        self._saveToSave()

    @property
    def nippleLength(self) -> float:
        self._loadFromSave()
        return self._nippleLength
    @nippleLength.setter
    def nippleLength(self, value: float) -> None:
        self._nippleLength = value
        self._saveToSave()

    @property
    def fullness(self) -> int:
        self._loadFromSave()
        return self._fullness
    @fullness.setter
    def fullness(self, value: int) -> None:
        self._fullness = value
        self._saveToSave()

    @property
    def timesMilked(self) -> int:
        self._loadFromSave()
        return self._timesMilked
    @timesMilked.setter
    def timesMilked(self, value: int) -> None:
        self._timesMilked = value
        self._saveToSave()

    @property
    def preventLactationIncrease(self) -> int:
        self._loadFromSave()
        return self._preventLactationIncrease
    @preventLactationIncrease.setter
    def preventLactationIncrease(self, value: int) -> None:
        self._preventLactationIncrease = value
        self._saveToSave()

    @property
    def preventLactationDecrease(self) -> int:
        self._loadFromSave()
        return self._preventLactationDecrease
    @preventLactationDecrease.setter
    def preventLactationDecrease(self, value: int) -> None:
        self._preventLactationDecrease = value
        self._saveToSave()
