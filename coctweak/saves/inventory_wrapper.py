from coctweak.logsys import getLogger
log = getLogger(__name__)
class InventoryWrapper(object):
    def __init__(self, save, invkey):
        self.save = save
        self.invkey = invkey

        self.slots = self.save._getInventory(self.invkey)

    def _getMaxQuantityFor(self, item_id: str) -> int:
        item = self.save.ALL_ITEMS.get(item_id, None)
        if item is None:
            return self.save.SLOT_QUANTITY_HARD_CAP
        return item['maxStackSize']

    def _commit(self):
        #self.save._setInventory(self.invkey, self.slots)
        return

    def add(self, item_id: str, count: int=1, quiet: bool=False) -> None:
        i=0
        maxq = self._getMaxQuantityFor(item_id)
        for slot in self.slots:
            i+=1
            if slot.id == item_id:
                if slot.quantity == maxq:
                    continue
                max_delta = maxq - slot.quantity
                delta = min(count, max_delta)
                count -= delta
                if not quiet:
                    log.info(f'Slot {i}: {slot.quantity} -> {slot.quantity+delta}')
                slot.quantity += delta
                if count == 0:
                    self._commit()
                    return
            elif slot.is_empty():
                slot.id = item_id
                slot.quantity = 0
                max_delta = maxq - slot.quantity
                delta = min(count, max_delta)
                count -= delta
                if not quiet:
                    log.info(f'Slot {i}: (empty) -> {delta}')
                slot.quantity = delta
                if count == 0:
                    self._commit()
                    return
        if count > 0:
            log.warning(f'Could not place {count} items.')
        self._commit()

    def count(self, item_id: str) -> int:
        o = 0
        for slot in self.slots:
            if slot.id != item_id:
                continue
            o += slot.quantity
        return o

    def remove(self, item_id: str, count: int=-1, quiet: bool=False) -> None:
        if count == -1:
            count = self.count(item_id)
        i=0
        for slot in self.slots:
            i += 1
            if slot.id == item_id:
                delta = min(count, slot.quantity)
                count -= delta
                if not quiet:
                    result = slot.quantity - delta
                    result = '(empty)' if result == 0 else result
                    log.info(f'Slot {i}: {slot.quantity} -> {result}')
                slot.quantity -= delta
                if slot.quantity <= 0:
                    slot.set_empty()
                if count == 0:
                    self._commit()
                    return
        if count > 0:
            log.warning(f'Could not remove {count} items.')
        self._commit()

    def clear(self, quiet: bool=False) -> None:
        i=0
        for slot in self.slots:
            i += 1
            if not slot.is_empty():
                if not quiet:
                    log.info(f'Slot {i}: {slot.id} x{slot.quantity} -> (empty)')
                slot.set_empty()
        self._commit()
