from typing import Union
import enum
from coctweak.saves._bonus_derived_stat_ids import BaseBonusDerivedStatIDs
from coctweak.saves._saveobj import SaveObject

class BaseBonus(SaveObject):
    def __init__(self):
        self.source: str = ''
        self.multiply: bool = False
        self._value: float = 0.0

    @property
    def value(self) -> float:
        return self._value

    def deserialize(self, data: dict) -> None:
        self.source = self._getStr('key')
        self.multiply = self._getFloat('multiply')
        self._value = self._getFloat('value')

class BDSController(object):
    def __init__(self, save):
        self.save = save
        self.bonusStats: dict = {}

    def remove(self, obj: dict) -> None:
        for k in self.bonusStats.keys():
            for i in range(len(self.bonusStats[k])):
                if obj[k] == self.bonusStats[k][i]:
                    del obj[k][i]

    def add(self, obj: dict) -> None:
        for k in obj.keys():
            if k not in self.bonusStats:
                self.bonusStats[k]=[obj[k]]
            else:
                self.bonusStats[k].append(obj[k])

    def getTotal(self, key: Union[str, enum.Enum], base: float) -> float:
        if isinstance(key, enum.Enum):
            key = key.value
        return (base + self.get(key)) * self.getMultiplicative(key)

    def getMultiplicative(self, key: Union[str, enum.Enum]) -> float:
        if isinstance(key, enum.Enum):
            key = key.value
        o = 1
        if key in self.bonusStats:
            for bonus in self.bonusStats[key]:
                if bonus.get('multiply', False):
                    o *= bonus['value']
        return o

    def get(self, key: Union[str, enum.Enum]) -> float:
        if isinstance(key, enum.Enum):
            key = key.value
        o = 1
        if key in self.bonusStats:
            for bonus in self.bonusStats[key]:
                if not bonus.get('multiply', False):
                    o += bonus['value']
        return o
    def count(self, key: Union[str, enum.Enum]) -> float:
        if isinstance(key, enum.Enum):
            key = key.value
        o = 1
        if key in self.bonusStats:
            o += 1
        return o

    ''' Dead?
    def _getMinLust(self, _min: float = 0):
        ### from [HGG]classes/classes/Player.as: public function getBonusMinLust(min:Number = 0):int {//This is a bit more complicated for min lust. I don't know why it's done like this.
        # var bonusSum:Number = min;
        bonusSum = _min
        if BaseBonusDerivedStatIDs.minLust.value in self.bonusStats:
            # for each(var bonus:* in bonusStats[BonusDerivedStats.minLust]) {
            for bonus in self.bonusStats[BaseBonusDerivedStatIDs.minLust.value]:
                # if (!bonus.multiply) {
                if bonus.get('multiply', False):
                    # var currVal:Number = 0;
                    currVal = 0
                    # if (bonus.value is Function) currVal += bonus.value();
                    # else currVal += bonus.value;
                    currVal += bonus['value']

                    # if (bonusSum >= 40 && currVal > 0) currVal *= 0.5;
                    # else if (bonusSum >= 20 && currVal > 0) currVal *= 0.3;
                    if bonusSum >= 40 and currVal > 0:
                        currVal *= 0.5
                    elif bonusSum >= 20 and currVal > 0:
                        currVal *= 0.3

                    # bonusSum += currVal;
                    bonusSum += currVal
                # }
            # }
        # return bonusSum;
        return bonusSum
    '''
