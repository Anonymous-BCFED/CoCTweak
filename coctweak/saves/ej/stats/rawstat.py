import math
from coctweak.saves._stat import Stat

class EJRawStat(Stat):
    def __init__(self):
        self.min = -math.inf
        self.max = math.inf
        self.value = 0.0
    def serialize(self):
        return {'value': self.value}
    def deserialize(self, data):
        self.value = float(data['value'])
