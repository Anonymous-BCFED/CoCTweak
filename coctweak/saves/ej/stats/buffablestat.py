from enum import IntEnum
import math
from coctweak.saves._stat import Stat
from coctweak.saves.ej.stats.rawstat import EJRawStat
from coctweak.saves.ej.stats.buff import EJBuff

class EAggregateType(IntEnum):
	#public static const AGGREGATE_SUM:int  = EnumValue.add(AggregateTypes, 0, 'AGGREGATE_SUM', {short: 'sum'});
    SUM = 0
	#public static const AGGREGATE_MAX:int  = EnumValue.add(AggregateTypes, 1, 'AGGREGATE_MAX', {short: 'max'});
    MAX = 1
	#public static const AGGREGATE_MIN:int  = EnumValue.add(AggregateTypes, 2, 'AGGREGATE_MIN', {short: 'min'});
    MIN = 2
	#public static const AGGREGATE_PROD:int = EnumValue.add(AggregateTypes, 3, 'AGGREGATE_PROD', {short: 'prod'});
    PROD = 3

class EJBuffableStat(Stat):
    def __init__(self, _id, name, **kwargs):
        super().__init__(_id, name, 0, lambda x: 0, lambda x: 100)
        self._base = kwargs.get('base', 0.0) if 'base' in kwargs and not callable(kwargs.get('base')) else 0.0
        self._baseFn = kwargs.get('base', None) if 'base' in kwargs and callable(kwargs.get('base')) else None
        self.aggregate = kwargs.get('aggregate', EAggregateType.SUM)
        self._min = kwargs.get('min', -math.inf)
        self._max = kwargs.get('max', math.inf)

        if self.aggregate == EAggregateType.PROD and self._base == 0.0:
            self._base = 1.0

        self.buffs = []

    def get_min(self):
        return self._min
    def set_min(self, value):
        self._min = value
    min = property(get_min, set_min)

    def get_max(self):
        return self._max
    def set_max(self, value):
        self._max = value
    max = property(get_max, set_max)

    @property
    def base(self):
        return self._baseFn() if self._baseFn is not None else self._base

    @property
    def value(self):
        return min(max(self._aggregateStep(self.base, self._value), self.min), self.max)

    def _aggregateBase(self) -> float:
        return 1 if self.aggregate == EAggregateType.PROD else 0

    def _aggregateStep(self, accumulator: float, value: float) -> float:
        if self.aggregate == EAggregateType.SUM:
            accumulator += value
        elif self.aggregate == EAggregateType.MAX:
            accumulator = max(accumulator, value)
        elif self.aggregate == EAggregateType.MIN:
            accumulator = min(accumulator, value)
        elif self.aggregate == EAggregateType.PROD:
            accumulator *= value
        return accumulator

    def calculate(self) -> float:
        o = self._aggregateBase()
        for buff in self.buffs:
            o = self._aggregateStep(o, buff.value)
        return o

    def recalculate(self) -> None:
        self._value = self.calculate()

    def serialize(self) -> dict:
        return {'effects': [x.serialize() for x in self.buffs]}

    def deserialize(self, data):
        newv = self._aggregateBase()
        for buffData in data.get('effects', []):
            buff = EJBuff()
            buff.deserialize(buffData)
            newv = self._aggregateStep(newv, buff.value)
            self.buffs.append(buff)
        self._value = newv
