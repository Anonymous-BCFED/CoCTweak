# @GENERATED 2019-06-21T13:50:55.996891
from coctweak.saves._saveobj import SaveObject

class EJBuff(SaveObject):
    def __init__(self):
        self.raw = None

        self.stat = None
        self.tag = ''
        self.rawValue = 0.0

        # Options
        self.save = False
        self.text = ''
        self.show = False
        self.rate = 0
        self.tick = 0

    @property
    def options(self) -> dict:
        return {
            'save': self.save,
            'show': self.show,
            'text': self.text,
            'rate': self.rate,
            'tick': self.tick
        }

    @options.setter
    def set_options(self, value: dict) -> None:
        if value is None:
            return
        self.save = bool(value.get('save', self.save))
        self.show = bool(value.get('show', self.show))
        self.text = str(value.get('text', self.text))
        self.rate = float(value.get('rate', self.rate))
        self.tick = float(value.get('tick', self.tick))

    @property
    def value(self) -> float:
        return self.rawValue

    @value.setter
    def value(self, value:float) -> None:
        self.rawValue = value
        self.stat.recalculate()

    def serialize(self):
        return [self.rawValue, self.tag, self.options]

    def deserialize(self, data):
        self.raw = data

        self.rawValue, self.tag, self.options = data
