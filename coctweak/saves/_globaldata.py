# This represents CoC_Main.sol.

from coctweak.saves._saveobj import SaveObject
from coctweak.saves._kflagstore import KFlagStore

from coctweak.logsys import getLogger
log = getLogger(__name__)

class GlobalData(SaveObject):
    FLAGS_ENUM = None
    ACHIEVEMENTS_ENUM = None
    PERM_OBJ_VERSION = -1
    MIN_PERM_OBJECT_ID = 0
    MAX_PERM_OBJECT_ID = 0
    BASEFILENAME = 'CoC_Main.sol'

    def __init__(self):
        super().__init__()

        self.host = ''

        self.permObjectVersion = self.PERM_OBJ_VERSION
        self.version = None

        self.flags: KFlagStore = KFlagStore(self.FLAGS_ENUM) if self.FLAGS_ENUM is not None else None
        self.achievements: KFlagStore = KFlagStore(self.ACHIEVEMENTS_ENUM) if self.ACHIEVEMENTS_ENUM is not None else None

        self.__loadedSafely = True

    def displayAchievements(self):
        with log.info('Achievements: (Global)'):
            o=[]
            maxlen = 0
            values = set(v for v in self.ACHIEVEMENTS_ENUM)
            for aID, aValue in self.achievements.items():
                aName = self.ACHIEVEMENTS_ENUM(int(aID)).name if int(aID) in values else f'_UNKNOWN_ACHIEVEMENT_{aID}'
                maxlen=max(maxlen, len(f'{aName} ({aID})'))
                if aValue == 1:
                    o += [(aName, aID)]
            for aName, aID in sorted(o, key = lambda i: i[0]):
                aKey = f'{aName} ({aID})'
                log.info(f'* {aKey:{maxlen}}')



    def deserialize(self, data: dict):
        self.raw = data
        self.permObjectVersion = self._getInt('permObjVersionID', self.PERM_OBJ_VERSION)
        self.version = data['version']

        if self.permObjectVersion < self.MIN_PERM_OBJECT_ID:
            with log.error(f'{self.BASEFILENAME} is so outdated that we cannot interpret it. (permObjVersionID={self.permObjectVersion}, MIN_PERM_OBJECT_ID={self.MIN_PERM_OBJECT_ID})'):
                log.error('It has been loaded as raw data and will simply be dumped back into the save as-is.  Modifications will not affect it, in order to prevent save corruption.')
            self.__loadedSafely = False
            return
        if self.permObjectVersion > self.MAX_PERM_OBJECT_ID:
            with log.error(f'!!! coctweak cannot read {self.BASEFILENAME}, as it\'s too new. (permObjVersionID={self.permObjectVersion}, MAX_PERM_OBJECT_ID={self.MAX_PERM_OBJECT_ID})'):
                log.error('It has been loaded as raw data and will simply be dumped back into the save as-is.  Modifications will not affect it, in order to prevent save corruption.')
            self.__loadedSafely = False
            return

        if self.ACHIEVEMENTS_ENUM is not None and 'achievements' in self.raw:
            self.achievements = KFlagStore(self.ACHIEVEMENTS_ENUM)
            self.achievements.deserialize(self.raw, 'achievements')

        if self.FLAGS_ENUM is not None and 'flags' in self.raw:
            self.flags = KFlagStore(self.FLAGS_ENUM)
            self.flags.deserialize(self.raw, 'flags')

    def serialize(self):
        if not self.__loadedSafely:
            return self.raw

        data = {}
        data['exists'] = True
        data['permObjVersionID'] = self.permObjectVersion
        data['version'] = self.version

        if self.ACHIEVEMENTS_ENUM is not None:
            data.update(self.achievements.serialize('achievements'))
        if self.FLAGS_ENUM is not None:
            data.update(self.flags.serialize('flags'))

        return data
