from typing import List, Dict, Any
from enum import Enum, IntEnum, auto
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._perk import Perk

from coctweak.logsys import getLogger
log = getLogger(__name__)

class EAilmentType(IntEnum):
    DISEASE = auto()
    PARASITE = auto()
    ADDICTION = auto()

class Ailment(object):
    '''
    Representation of an ailment (disease, parasite, addiction).

    Mostly used for parasites and addictions, since they're generally a clusterfuck of variables, flags, perks, and SFX.
    '''

    ID: str = ''
    NAME: str = ''
    TYPE: EAilmentType = EAilmentType.DISEASE

    INFLICTED_ADJ: str = 'INFLICTED' # See inflictedWith()
    INFLICTED_PREPOSITION: str = 'with' # See inflictedWith()

    STATUS_EFFECTS: List[str] = []
    PERKS: List[str] = []
    KFLAGS: List[int] = []
    SELFSAVEKEYS: List[str] = []

    def __init__(self):
        self.save = None
        self.indicating_sfx: List[StatusEffect] = []
        self.indicating_perks: List[Perk] = []
        self.indicating_kflags: Dict[int, Any] = {}
        self.indicating_selfsavers: Dict[str, Any] = {}

    def checkSFX(self, se:StatusEffect) -> bool:
        if se.id in self.STATUS_EFFECTS:
            self.indicating_sfx += [se]
            return True
        return False

    def checkPerk(self, p:Perk) -> bool:
        if p.id in self.PERKS:
            self.indicating_perks += [p]
            return True
        return False

    def checkKFlag(self, k: int, v: Any) -> bool:
        if k in self.KFLAGS:
            self.indicating_kflags[k] = v
            return True
        return False

    def check(self, save) -> bool:
        self.save = save
        o = False
        for sfxid in self.STATUS_EFFECTS:
            if save.hasStatusEffect(sfxid):
                self.indicating_sfx.append(save.statusEffects[sfxid])
                o = True
        for pid in self.PERKS:
            if save.hasPerk(pid):
                self.indicating_perks.append(save.perks[pid])
                o = True
        for k in self.KFLAGS:
            if save.flags.has(k):
                self.indicating_kflags[k] = save.flags.get(k)
                o = True
        return o

    def inflictedWith(self) -> str:
        return f'{self.INFLICTED_ADJ} {self.INFLICTED_PREPOSITION} {self.NAME}!'

    def displayDiagnosis(self, save, header_suffix='') -> None:
        self.save = save
        with log.warning(self.inflictedWith()+header_suffix):
            with log.info('Indicators:'):
                for se in self.indicating_sfx:
                    log.info(' * Status effect %r - %s', se.id, se.displayValues())
                for p in self.indicating_perks:
                    log.info(' * Perk %r - %s', p.id, p.displayValues())
                for k in self.indicating_kflags:
                    log.info(' * kFlag %s (%d) - %s', save.ENUM_FLAGS(k).name, k, save.flags.get(k))
            with log.info('Additional Information:'):
                self.displayState()

    def addStatusEffect(self, se, values: List[int] = None):
        if values is None:
            values = [0, 0, 0, 0]
        seid = None
        if isinstance(se, StatusEffect):
            seid = se.id
        if isinstance(se, Enum):
            seid = se.value
        if isinstance(se, str):
            seid = se
        if not self.save.hasStatusEffect(seid):
            log.info('Adding status effect %r with values %r...', seid, values)
            self.save.addStatusEffect(se).values = values

    def removeStatusEffect(self, se):
        seid = None
        if isinstance(se, StatusEffect):
            seid = se.id
        if isinstance(se, Enum):
            seid = se.value
        if isinstance(se, str):
            seid = se
        if self.save.hasStatusEffect(seid):
            log.info('Removed status effect %r...', seid)
            del self.save.statusEffects[seid]

    def addPerk(self, p, values: List[int] = None) -> None:
        if values is None:
            values = [0, 0, 0, 0]
        pid = None
        if isinstance(p, Perk):
            pid = p.id
        if isinstance(p, Enum):
            pid = p.value
        if isinstance(p, str):
            pid = p
        if not self.save.hasPerk(pid):
            log.info('Adding quadperk %r with values %r...', pid, values)
            self.save.createQuadPerk(pid, values)

    def removePerk(self, p):
        pid = None
        if isinstance(p, Perk):
            pid = p.id
        if isinstance(p, Enum):
            pid = p.value
        if isinstance(p, str):
            pid = p
        if self.save.hasPerk(pid):
            log.info('Removing perk %r...', pid)
            del self.save.perks[pid]

    def setKFlag(self, k: int, value: Any) -> None:
        oldvalue: Any = self.save.flags.get(k, 0)
        if oldvalue != value:
            log.info('Setting kFlag %s (%d): %r -> %r...', self.save.ENUM_FLAGS(k).name, k, oldvalue, value)
            self.save.flags.set(k, value)

    def removeKFlag(self, k: int) -> None:
        if self.save.flags.has(k):
            log.info('Unsetting kFlag %s (%d)...', self.save.ENUM_FLAGS(k).name, k)
            self.save.flags.remove(k)

    def addToPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        '''
        Things to do when infecting the PC.
        '''
        pass


    def displayState(self) -> None:
        return

    def removeFromPlayer(self, save, change_stats: bool = False, **kwargs) -> None:
        '''
        Things to do when uninfecting the PC.

        By default, this just nukes all of the indicating SFX, perks, and kFlags.
        '''
        if len(self.indicating_sfx) > 0:
            for se in self.indicating_sfx:
                self.removeStatusEffect(se)

        if len(self.indicating_perks) > 0:
            for p in self.indicating_perks:
                self.removePerk(p)

        if len(self.indicating_kflags.keys()) > 0:
            for k,v in self.indicating_kflags.items():
                self.removeKFlag(k)

class Disease(Ailment):
    INFLICTED_ADJ = 'INFECTED'
    TYPE = EAilmentType.DISEASE

class Parasite(Disease):
    INFLICTED_ADJ = 'INFECTED'
    TYPE = EAilmentType.PARASITE

class Addiction(Ailment):
    INFLICTED_ADJ = 'ADDICTED'
    INFLICTED_PREPOSITION = 'to'
    TYPE = EAilmentType.ADDICTION
