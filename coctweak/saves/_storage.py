from typing import List, Optional
from coctweak.saves.common.item import BaseItem
from ._saveobj import SaveObject

from coctweak.logsys import getLogger
log = getLogger(__name__)

class Storage(SaveObject):
    def __init__(self, save: 'BaseSave', name: str, category: Optional[str], keys: List[str]):
        self.save: 'BaseSave' = save
        self.name: str = name
        self.category: Optional[str] = category
        self.keys: List[str] = keys

        self.slots: List[BaseItem] = []
        self.hide_locked: bool = False

    def accepts(self, item: BaseItem) -> bool:
        return self.category is None or item.category == self.category

    def deserialize(self, rawslots) -> None:
        self.slots = []
        for rawslot in rawslots:
            slot = self.save.ITEM_TYPE()
            slot.deserialize(rawslot)
            self.save.loadSlot(slot) # metadata
            self.slots += [slot]

    def serialize(self) -> None:
        return [x.serialize() for x in self.slots]

    def _getMaxQuantityFor(self, item_id: str) -> int:
        item = self.save.getItemMeta(item_id)
        if item is None:
            return None
        return item['maxStackSize']

    def display(self):
        if len(self.slots) > 0:
            with log.info(f'{self.name}:'):
                i=0
                for item in self.slots:
                    i+=1
                    log.info(f'* {str(i).rjust(2)}: '+item.displayValues(hide_locked=self.hide_locked))

    def allocate(self, ct: int) -> None:
        for i in range(ct):
            self.slots += [self.save.ITEM_TYPE()]

    def add(self, item_id: str, count: int=1, quiet: bool=False) -> None:
        i=0
        maxq = self._getMaxQuantityFor(item_id)
        assert maxq is not None
        for slot in self.slots:
            i+=1
            if slot.id == item_id:
                if slot.quantity == maxq:
                    continue
                max_delta = maxq - slot.quantity
                delta = min(count, max_delta)
                count -= delta
                if not quiet:
                    log.info(f'Slot {i}: {slot.quantity} -> {slot.quantity+delta}')
                slot.quantity += delta
                if count == 0:
                    return
            elif slot.is_empty():
                slot.id = item_id
                slot.quantity = 0
                max_delta = maxq - slot.quantity
                delta = min(count, max_delta)
                count -= delta
                if not quiet:
                    log.info(f'Slot {i}: (empty) -> {delta}')
                slot.quantity = delta
                if count == 0:
                    return
        if count > 0:
            log.warning(f'Could not place {count} items.')

    def count(self, item_id: str) -> int:
        o = 0
        for slot in self.slots:
            if slot.id != item_id:
                continue
            o += slot.quantity
        return o

    def remove(self, item_id: str, count: int=-1, quiet: bool=False) -> None:
        if count == -1:
            count = self.count(item_id)
        i=0
        for slot in self.slots:
            i += 1
            if slot.id == item_id:
                delta = min(count, slot.quantity)
                count -= delta
                if not quiet:
                    result = slot.quantity - delta
                    result = '(empty)' if result == 0 else result
                    log.info(f'Slot {i}: {slot.quantity} -> {result}')
                slot.quantity -= delta
                if slot.quantity <= 0:
                    slot.set_empty()
                if count == 0:
                    return
        if count > 0:
            log.warning(f'Could not remove {count} items.')

    def clear(self, quiet: bool=False) -> None:
        i=0
        for slot in self.slots:
            i += 1
            if not slot.is_empty():
                if not quiet:
                    log.info(f'Slot {i}: {slot.id} x{slot.quantity} -> (empty)')
                slot.set_empty()

class GearStorage(Storage):
    def __init__(self, save: 'BaseSave', name: str, start: int, end: int, category: Optional[str], keys: List[str]):
        super().__init__(save, name, category, keys)
        self.start: int = start
        self.end: int = end

    def deserialize(self, rawslots) -> None:
        super().deserialize(rawslots[self.start:self.end])

    def serialize(self, rawslots) -> None:
        rawslots[self.start:self.end] = super().serialize()
