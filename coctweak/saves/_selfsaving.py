from coctweak.saves._saveobj import SaveObject
from typing import Dict, Optional, Tuple, Any

from coctweak.logsys import getLogger
log = getLogger(__name__)

class SelfSavingObject(SaveObject):
    DATA_KEY: str = 'data'
    def __init__(self, id: str) -> None:
        super().__init__()
        self.id: str = ''
        self.schema: Dict[str, dict] = {}

    def addBoolean(self, name: str, default: bool, desc: str) -> bool:
        self.schema[name] = {
            'type': 'b',
            'name': name,
            'default': default,
            'desc': desc,
        }
        return default

    def addStr(self, name: str, default: str, desc: str) -> str:
        self.schema[name] = {
            'type': 's',
            'name': name,
            'default': default,
            'desc': desc,
        }
        return default

    def addInt(self, name: str, default: int, desc: str, range: Optional[Tuple[int, int]] = None, bitflags: Optional[Dict[int, str]] = None) -> int:
        self.schema[name] = {
            'type': 'i',
            'name': name,
            'default': default,
            'desc': desc,
            'range': range,
        }
        return default

    def addFloat(self, name: str, default: float, desc: str, range: Optional[Tuple[float, float]] = None) -> float:
        self.schema[name] = {
            'type': 'f',
            'name': name,
            'default': default,
            'desc': desc,
            'range': range,
        }
        return default

    def addChoices(self, name: str, default: Any, desc: str, choices: Dict[Any, str] = None) -> Any:
        self.schema[name] = {
            'type': 'f',
            'name': name,
            'default': default,
            'desc': desc,
            'choices': choices,
        }
        return default

    def addArray(self, name: str, default: list, desc: str) -> Any:
        self.schema[name] = {
            'type': 'f',
            'name': name,
            'default': default,
            'desc': desc,
        }
        return default

    def addUnused(self, name: str, default: Any, desc: str) -> Any:
        self.schema[name] = {
            'type': '-',
            'name': name,
            'default': default,
            'desc': desc,
        }
        return default

    def addUnknown(self, name: str, default: Any, desc: str) -> Any:
        self.schema[name] = {
            'type': '?',
            'name': name,
            'default': default,
            'desc': desc,
        }
        return default
    def addObject(self, name: str, default: Any, desc: str, struct: dict = {}) -> Any:
        self.schema[name] = {
            'type': '?',
            'name': name,
            'default': default,
            'desc': desc,
            'struct': struct,
        }
        return default

    def reset(self) -> None:
        for k, v in self.schema.items():
            setattr(self, k, v['default'])

    def serialize(self) -> dict:
        meta = super().serialize()
        data = {}
        for k, v in self.schema.items():
            data[k] = getattr(self, k)
        meta[self.DATA_KEY] = data
        return meta

    def deserialize(self, meta: dict) -> None:
        super().deserialize(meta)
        data = meta[self.DATA_KEY]
        for k, v in self.schema.items():
            setattr(self, k, data.get(k, v['default']))

    def display(self) -> None:
        log.info(f'{self.id}:')
        for k, v in self.schema.items():
            desc = v['desc']
            val = getattr(self, k)
            log.info(f'  {k} ({desc}): {val!r}')
