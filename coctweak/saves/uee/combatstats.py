import math
from coctweak.saves._combatstats import BaseCombatStats
from coctweak.saves.uee.enums.kflags import UEEKFlags
from coctweak.saves._stat import Stat, StatHealType
from coctweak.saves.uee.enums.jewelrymodifiers import UEEJewelryModifiers
from coctweak.saves.uee.enums.itemlib import UEEItemLib
from coctweak.saves.uee.enums.perklib import UEEPerkLib
from coctweak.saves.uee.enums.statuslib import UEEStatusLib
from coctweak.saves.uee.bimboprogression import UEEBimboProgression

class UEECombatStats(BaseCombatStats):
    ## from [UEE]/classes/classes/Scenes/Places/TelAdre/UmasShop.as: public static const NEEDLEWORK_DEFENSE_EXTRA_HP:int = 50; @ dTk9wE4ta9IA5gpdN437NgCZ02lcC73wVeofg9M07x1D2bKG4eOgdK3va2bWgP5aKw80U8i2dcWas47hy0b8fQfbpufcneRQ
    NEEDLEWORK_DEFENSE_EXTRA_HP: int = 50

    def __init__(self, save: 'UEESave'):
        self.hunger: Stat = None
        super().__init__(save)

    def init(self):
        super().init()
        self.hunger = self._add(Stat('hunger', 'Hunger', 0.0, lambda: 0, self._get_hunger_max, healtype=StatHealType.MAX))

    def _get_hunger_max(self) -> float:
        ## from [UEE]/classes/classes/Character.as: public function maxHunger():Number @ gpq8SY20QcrvgIO8tPcf76WUfH8ccDbuWbKP97J3pE8lx9FagF1cNfcuk4Td4j3aNT63pdeKcYu6Ln8fyaAjfOB5VrfEHf1F
        return 100

    def _get_ovi_egg_count(self) -> int:
        '''
        Ovipositor egg count.
        '''
        ## from [UEE]/classes/classes/Creature.as: public function eggs():int @ 8HY1Vy9a295U7Id82m8vW1OCdPe8SI21p3emcZr6QP4ZK6VScFOd7t3gl9V87rR19x1RR44A15hbcc35I0Uae265TLbQTawZ
        spider_ovi = self.save.getPerk(UEEPerkLib.SpiderOvipositor)
        bee_ovi = self.save.getPerk(UEEPerkLib.SpiderOvipositor)
        #if (!hasPerk(PerkLib.SpiderOvipositor) && !hasPerk(PerkLib.BeeOvipositor)) return -1;
        if spider_ovi is None and bee_ovi is None:
            return -1
        #else if (hasPerk(PerkLib.SpiderOvipositor)) return perkv1(PerkLib.SpiderOvipositor);
        elif spider_ovi is not None:
                return spider_ovi.values[0]
        #else return perkv1(PerkLib.BeeOvipositor);
        return bee_ovi.values[0]

    def _get_lust_min(self) -> float:
        ## from [UEE]/classes/classes/Player.as: public override function minLust():Number @ 7DY02EgIO0wWfi97vM7Ez0430XvexIaWYbMz8f15deapOcImbzpdHE4at4IBcAn0aI7PwffD9Ez6dsfoj1BxebA64P7uu7rd

        #var min:Number = 0;
        _min: float = 0

        #var minCap:Number = 100;
        _minCap: float = 0

        #//Bimbo body boosts minimum lust by 40
        #if (hasStatusEffect(StatusEffects.BimboChampagne) || findPerk(PerkLib.BimboBody) >= 0 || findPerk(PerkLib.BroBody) >= 0 || findPerk(PerkLib.FutaForm) >= 0) {
        if self.save.hasStatusEffect(UEEStatusLib.BimboChampagne) or any([self.save.hasPerk(x) for x in (UEEPerkLib.BimboBody, UEEPerkLib.BroBody, UEEPerkLib.FutaForm)]):
            #if (min > 40) min += 10;
            if _min > 40:
                _min += 10
            #else if (min >= 20) min += 20;
            elif _min >= 20:
                _min += 20
            #else min += 40;
            else:
                _min += 40

            #if (armorName == "bimbo skirt") min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST] / 4;
            if self.save.armor.item.id == UEEItemLib.BIMBOSK:
                _min += self.save.flags.get(UEEKFlags.BIMBOSKIRT_MINIMUM_LUST, 0) / 4
        #}
        #else if (kGAMECLASS.bimboProgress.ableToProgress()) {
        elif UEEBimboProgression.ableToProgress(self.save):
            #if (min > 40) min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST] / 4;
            if _min > 40:
                _min += self.save.flags.get(UEEKFlags.BIMBOSKIRT_MINIMUM_LUST, 0) / 4
            #else if (min >= 20 ) min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST] / 2;
            elif _min >= 20:
                    _min += self.save.flags.get(UEEKFlags.BIMBOSKIRT_MINIMUM_LUST, 0) / 2
            #else min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST];
            else:
                _min += self.save.flags.get(UEEKFlags.BIMBOSKIRT_MINIMUM_LUST, 0) / 4
        #}
        #//Omnibus' Gift
        #if (hasPerk(PerkLib.OmnibusGift)) {
        if self.save.hasPerk(UEEPerkLib.OmnibusGift):
            #if (min > 40) min += 10;
            if _min > 40:
                _min += 10
            #else if (min >= 20) min += 20;
            elif _min >= 20:
                _min += 20
            #else min += 35;
            else:
                _min += 35
        #}
        #//Nymph perk raises to 30
        #if (hasPerk(PerkLib.Nymphomania)) {
        if self.save.hasPerk(UEEPerkLib.Nymphomania):
            #if (min >= 40) min += 10;
            if _min >= 40:
                _min += 10
            #else if (min >= 20) min += 15;
            elif _min >= 20:
                _min += 15
            #else min += 30;
            else:
                _min += 30
        #}
        #//Oh noes anemone!
        #if (hasStatusEffect(StatusEffects.AnemoneArousal)) {
        if self.save.hasStatusEffect(UEEStatusLib.AnemoneArousal):
            #if (min >= 40) min += 10;
            if _min >= 40:
                _min += 10
            #else if (min >= 20) min += 20;
            elif _min >= 20:
                _min += 20
            #else min += 30;
            else:
                _min += 30
        #}
        #//Hot blooded perk raises min lust!
        #if (hasPerk(PerkLib.HotBlooded)) {
        if self.save.hasPerk(UEEPerkLib.HotBlooded):
            perk = self.save.getPerk(UEEPerkLib.HotBlooded)
            #if (min > 0) min += perk(findPerk(PerkLib.HotBlooded)).value1 / 2;
            if _min > 0:
                _min += perk.value[0] / 2
            #else min += perk(findPerk(PerkLib.HotBlooded)).value1;
            else:
                _min += perk.value[0]
        #}
        #if (hasPerk(PerkLib.LuststickAdapted)) {
        if self.save.hasPerk(UEEPerkLib.LuststickAdapted):
            #if (min < 50) min += 10;
            if _min < 50:
                _min += 10
            #else min += 5;
            else:
                _min += 5
        #}
        #if (hasStatusEffect(StatusEffects.Infested)) {
        if self.save.hasStatusEffect(UEEStatusLib.Infested):
            #if (min < 50) min = 50;
            if _min < 50:
                _min = 50
        #}
        #//Add points for Crimstone
        #min += perkv1(PerkLib.PiercedCrimstone);
        _min += self.save.getPerk(UEEPerkLib.PiercedCrimstone).value[0] if self.save.hasPerk(UEEPerkLib.PiercedCrimstone) else 0
        #//Subtract points for Icestone!
        #min -= perkv1(PerkLib.PiercedIcestone);
        _min -= self.save.getPerk(UEEPerkLib.PiercedIcestone).value[0] if self.save.hasPerk(UEEPerkLib.PiercedIcestone) else 0
        #min += perkv1(PerkLib.PentUp);
        _min += self.save.getPerk(UEEPerkLib.PentUp).value[0] if self.save.hasPerk(UEEPerkLib.PentUp) else 0
        #//Cold blooded perk reduces min lust, to a minimum of 20! Takes effect after piercings. This effectively caps minimum lust at 80.
        #if (hasPerk(PerkLib.ColdBlooded)) {
        if self.save.hasPerk(UEEPerkLib.ColdBlooded):
            #if (min >= 20) {
            if _min >= 20:
                #if (min <= 40) min -= (min - 20);
                if _min <= 40:
                    _min -= _min - 20
                #else min -= 20;
                else:
                    _min -= 20
            #}
            #minCap -= 20;
            _minCap -= 20
        #}
        #//Harpy Lipstick status forces minimum lust to be at least 50.
        #if (min < 50 && hasStatusEffect(StatusEffects.Luststick)) min = 50;
        if _min < 50 and self.save.hasStatusEffect(UEEStatusLib.Luststick):
            _min = 50
        #//SHOULDRA BOOSTS
        #//+20
        #if (flags[kFLAGS.SHOULDRA_SLEEP_TIMER] <= -168 && flags[kFLAGS.URTA_QUEST_STATUS] != 0.75) {
        if self.save.flags.get(UEEKFlags.SHOULDRA_SLEEP_TIMER, 0) <= -168 and self.save.flags.get(UEEKFlags.URTA_QUEST_STATUS, 0) != 0.75:
            #min += 20;
            _min += 20
            #if (flags[kFLAGS.SHOULDRA_SLEEP_TIMER] <= -216)
            if self.save.flags.get(UEEKFlags.SHOULDRA_SLEEP_TIMER, 0) <= -216:
                #min += 30;
                _min += 30
        #}
        #//SPOIDAH BOOSTS
        #if (eggs() >= 20) {
        if self._get_ovi_egg_count() >= 20:
            #min += 10;
            _min += 10
            #if (eggs() >= 40) min += 10;
            if self._get_ovi_egg_count() >= 40:
                _min += 10
        #}
        #//Jewelry effects
        #if (jewelryEffectId == JewelryLib.MODIFIER_MINIMUM_LUST)
        #{
        if self.save.jewelry.effect.id == UEEJewelryModifiers.MINIMUM_LUST:
            jewelryEffectMagnitude = self.save.jewelry.effect.magnitude
            #min += jewelryEffectMagnitude;
            _min += jewelryEffectMagnitude
            #if (min > (minCap - jewelryEffectMagnitude) && jewelryEffectMagnitude < 0)
            #{
            if _min > (_minCap - jewelryEffectMagnitude) and jewelryEffectMagnitude < 0:
                #minCap += jewelryEffectMagnitude;
                _minCap += jewelryEffectMagnitude
            #}
        #}
        #if (min < 30 && armorName == "lusty maiden's armor") min = 30;
        if _min < 30 and self.save.armor.item.id == UEEItemLib.LMARMOR:
            _min = 30
        #if (min < 20 && armorName == "tentacled bark armor") min = 20;
        if _min < 20 and self.save.armor.item.id == UEEItemLib.TBARMOR:
            _min = 20
        #//Constrain values
        #if (min < 0) min = 0;
        #if (min > 95) min = 95;
        _min = min(max(_min, 0), 95)
        #if (min > minCap) min = minCap;
        #return min;
        return min(_min, _minCap)

    def _get_hp_max(self) -> float:
        ## from [UEE]/classes/classes/Creature.as: public function maxHP():Number @ 1mvbcn8oTg2s259gEEdNu1sMbt28sG4Yl8WK6bcbTa8647yL3VVc7U9aP7f95mDcMO5Lp47W0rygpkdAEbnse9n2uR0Oy3mN

        #var max:Number = 0;
        _max: float = 0
        #max += int(tou * 2 + 50);
        _max += (float(self.save.stats.toughness.value) * 2) + 50
        #if (findPerk(PerkLib.Tank) >= 0) max += 50;
        if self.save.hasPerk(UEEPerkLib.Tank):
            _max += 50
        #if (findPerk(PerkLib.Tank2) >= 0) max += Math.round(tou);
        if self.save.hasPerk(UEEPerkLib.Tank2):
            _max += round(self.save.stats.toughness.value)
        #if (findPerk(PerkLib.Tank3) >= 0) max += level * 5;
        if self.save.hasPerk(UEEPerkLib.Tank3):
            _max += self.level.value * 5
        #if (findPerk(PerkLib.ChiReflowDefense) >= 0) max += UmasShop.NEEDLEWORK_DEFENSE_EXTRA_HP;
        if self.save.hasPerk(UEEPerkLib.ChiReflowDefense):
            _max += self.NEEDLEWORK_DEFENSE_EXTRA_HP
        #if (flags[kFLAGS.GRIMDARK_MODE] >= 1)
        if self.save.flags.get(UEEKFlags.GRIMDARK_MODE, 0) >= 1:
            #max += level * 5;
            _max += self.level.value * 5
        #else
        else:
            #max += level * 15;
            _max += self.level.value * 15
        #if (jewelryEffectId == JewelryLib.MODIFIER_HP) max += jewelryEffectMagnitude;
        if self.save.jewelry.effect.id == UEEJewelryModifiers.HP:
            _max += self.save.jewelry.effect.magnitude
        #max *= 1 + (countCockSocks("green") * 0.02);
        _max *= 1 + (self.save.countCockSocks('green') * 0.02)
        #max = Math.round(max);
        #if (max < 50) max = 50;
        #if (max > 9999) max = 9999;
        #return max;
        return min(max(round(_max), 50), 9999)

    def _get_max_fatigue(self) -> float:
        ## from [UEE]/classes/classes/Creature.as: public function maxFatigue():Number @ 9j27mcgnl6ly4RhdSq8b7fUy3ie2IMguq7jN98D2GGcgb4Iadf85cu5CY38XaIj2RF2fv12Gdcd885ejSffI3m4g0M2tF24B

        #var max:Number = 100;
        _max: float = 100
        #if (findPerk(PerkLib.ImprovedEndurance) >= 0) max += 20;
        if self.save.hasPerk(UEEPerkLib.ImprovedEndurance):
            _max += 20
        #if (findPerk(PerkLib.ImprovedEndurance2) >= 0) max += 10;
        if self.save.hasPerk(UEEPerkLib.ImprovedEndurance2):
            _max += 10
        #if (findPerk(PerkLib.ImprovedEndurance3) >= 0) max += 10;
        if self.save.hasPerk(UEEPerkLib.ImprovedEndurance3):
            _max += 10
        #if (findPerk(PerkLib.AscensionEndurance) >= 0) max += perkv1(PerkLib.AscensionEndurance) * 5;
        if self.save.hasPerk(UEEPerkLib.AscensionEndurance):
            _max += self.save.getPerk(UEEPerkLib.AscensionEndurance).values[0] * 5
        #if (max > 999) max = 999;
        #return max;
        return min(_max, 999)
