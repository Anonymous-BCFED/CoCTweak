from coctweak.saves.uee.bodyparts._serialization.ears import UEERawEars
from coctweak.saves.uee.enums.eartypes import UEEEarTypes
from coctweak.saves.uee.piercing import UEEPiercing

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class UEEEars(UEERawEars):
    ENUM_TYPES = UEEEarTypes
    TYPE_PIERCING = UEEPiercing
