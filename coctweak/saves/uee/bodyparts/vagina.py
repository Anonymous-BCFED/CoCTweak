from coctweak.saves.uee.bodyparts._serialization.vagina import UEERawVagina
from coctweak.saves.uee.enums.vag_wetness import UEEVagWetness
from coctweak.saves.uee.enums.vag_looseness import UEEVagLooseness
from coctweak.saves.uee.enums.vag_types import UEEVagTypes
from coctweak.saves.uee.piercing import UEEPiercing

__ALL__ = ['UEEVagina']

class UEEVagina(UEERawVagina):
    TYPE_PIERCING  = UEEPiercing
    ENUM_WETNESS   = UEEVagWetness
    ENUM_LOOSENESS = UEEVagLooseness
    ENUM_TYPES     = UEEVagTypes

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.clitPiercing = self.TYPE_PIERCING.SimpleLoadFrom(data, 'clit')
        self.labiaPiercing = self.TYPE_PIERCING.SimpleLoadFrom(data, 'labia')

    def serialize(self) -> dict:
        data = super().serialize()
        (self.clitPiercing if self.clitPiercing is not None else self.TYPE_PIERCING()).simpleSerializeTo(data, 'clit')
        (self.labiaPiercing if self.labiaPiercing is not None else self.TYPE_PIERCING()).simpleSerializeTo(data, 'labia')
        return data
