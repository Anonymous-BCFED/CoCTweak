from coctweak.saves.uee.bodyparts._serialization.breastrow import UEERawBreastRow

class UEEBreastRow(UEERawBreastRow):
    def getDisplayEntries(self) -> dict:
        o = super().getDisplayEntries()
        if self.nippleCocks:
            o['Nipple Cocks'] = 'yes'
        return o
