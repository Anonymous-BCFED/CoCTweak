from coctweak.saves.uee.enums.tailtypes import UEETailTypes
from coctweak.saves.uee.bodyparts._serialization.tail import UEERawTail

class UEETail(UEERawTail):
    ENUM_TYPES = UEETailTypes
