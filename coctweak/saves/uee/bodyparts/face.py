from coctweak.saves.uee.bodyparts._serialization.face import UEERawFace
from coctweak.saves.uee.enums.facetypes import UEEFaceTypes


class UEEFace(UEERawFace):
    ENUM_TYPES = UEEFaceTypes
