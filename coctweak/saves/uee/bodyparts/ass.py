from typing import Dict
from coctweak.saves.uee.bodyparts._serialization.ass import UEERawAss
from coctweak.saves.uee.enums.ass_looseness import UEEAssLooseness
from coctweak.saves.uee.enums.ass_wetness import UEEAssWetness
from coctweak.saves.uee.enums.ass_ratings import UEEAssRatings

class UEEAss(UEERawAss):
    OBJECT_KEY = 'ass'
    ENUM_LOOSENESS = UEEAssLooseness
    ENUM_WETNESS = UEEAssWetness
    ENUM_RATINGS = UEEAssRatings

    def deserializeFrom(self, data: dict) -> None:
        super().deserialize(data[self.OBJECT_KEY])
        super().deserializeFrom(data)

    def serializeTo(self, data: dict) -> None:
        data[self.OBJECT_KEY] = self.serialize()
        data['buttRating'] = self.buttRating

    def getHelpers(self) -> Dict[str, str]:
        o = super().getHelpers()
        o['Rating'] = self.buttRating
        return o
    
    def analCapacity(self) -> float:
        return 0. #TODO