from coctweak.saves.uee.enums.lowerbodytypes import UEELowerBodyTypes
from coctweak.saves.uee.bodyparts._serialization.lowerbody import UEERawLowerBody

class UEELowerBody(UEERawLowerBody):
    ENUM_TYPES = UEELowerBodyTypes
