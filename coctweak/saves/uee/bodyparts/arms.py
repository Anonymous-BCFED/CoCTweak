from coctweak.saves.uee.bodyparts._serialization.arms import UEERawArms
from coctweak.saves.uee.enums.armtypes import UEEArmTypes

class UEEArms(UEERawArms):
    ENUM_TYPES = UEEArmTypes
