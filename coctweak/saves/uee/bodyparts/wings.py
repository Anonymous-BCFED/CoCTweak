from coctweak.saves.uee.enums.wingtypes import UEEWingTypes
from coctweak.saves.uee.bodyparts._serialization.wings import UEERawWings

class UEEWings(UEERawWings):
    ENUM_TYPES = UEEWingTypes
