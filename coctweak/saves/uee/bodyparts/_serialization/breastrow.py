# @GENERATED from coc/uee/classes/classes/BreastRow.as
from coctweak.saves.common.bodyparts.breastrow import BaseBreastRow
from coctweak.saves.uee.serialization import UEESerializationVersion

__ALL__=['UEERawBreastRow']

class UEERawBreastRow(BaseBreastRow):
    SERIALIZATION_STAMP = UEESerializationVersion('c862ee0a-5667-4fd3-a178-37a5e85c86d6', 0, 1)
    SERIALIZATION_VERSION: int = 0
    def __init__(self, save) -> None:
        super().__init__(save)
        self.nippleCocks: bool = False
    def serialize(self) -> dict:
        data = super().serialize()
        data["nippleCocks"] = self.nippleCocks
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.nippleCocks = self._getBool("nippleCocks", False)
