# @GENERATED from coc/uee/classes/classes/BodyParts/LowerBody.as
from coctweak.saves.common.bodyparts.lowerbody import BaseLowerBody

__ALL__=['UEERawLowerBody']

class UEERawLowerBody(BaseLowerBody):
    HUMAN: int = 0
    HOOFED: int = 0
    DOG: int = 0
    NAGA: int = 0
    CENTAUR: int = 0
    DEMONIC_HIGH_HEELS: int = 0
    DEMONIC_CLAWS: int = 0
    BEE: int = 0
    GOO: int = 0
    CAT: int = 0
    LIZARD: int = 0
    PONY: int = 0
    BUNNY: int = 0
    HARPY: int = 0
    KANGAROO: int = 0
    CHITINOUS_SPIDER_LEGS: int = 0
    DRIDER: int = 0
    FOX: int = 0
    DRAGON: int = 0
    RACCOON: int = 0
    FERRET: int = 0
    CLOVEN_HOOFED: int = 0
    RHINO: int = 0
    ECHIDNA: int = 0
    DEERTAUR: int = 0
    SALAMANDER: int = 0
    WOLF: int = 0
    IMP: int = 0
    COCKATRICE: int = 0
    RED_PANDA: int = 0
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
