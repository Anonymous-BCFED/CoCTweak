# @GENERATED from coc/uee/classes/classes/BodyParts/Tail.as
from coctweak.saves.common.bodyparts.tail import BaseTail

__ALL__=['UEERawTail']

class UEERawTail(BaseTail):
    NONE: int = 0
    HORSE: int = 0
    DOG: int = 0
    DEMONIC: int = 0
    COW: int = 0
    SPIDER_ABDOMEN: int = 0
    BEE_ABDOMEN: int = 0
    SHARK: int = 0
    CAT: int = 0
    LIZARD: int = 0
    RABBIT: int = 0
    HARPY: int = 0
    KANGAROO: int = 0
    FOX: int = 0
    DRACONIC: int = 0
    RACCOON: int = 0
    MOUSE: int = 0
    FERRET: int = 0
    BEHEMOTH: int = 0
    PIG: int = 0
    SCORPION: int = 0
    GOAT: int = 0
    RHINO: int = 0
    ECHIDNA: int = 0
    DEER: int = 0
    SALAMANDER: int = 0
    WOLF: int = 0
    SHEEP: int = 0
    IMP: int = 0
    COCKATRICE: int = 0
    RED_PANDA: int = 0
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
