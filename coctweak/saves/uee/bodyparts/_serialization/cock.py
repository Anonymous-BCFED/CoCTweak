# @GENERATED from coc/uee/classes/classes/Cock.as
from coctweak.saves.common.bodyparts.cock import BaseCock
from coctweak.saves.uee.serialization import UEESerializationVersion

__ALL__=['UEERawCock']

class UEERawCock(BaseCock):
    SERIALIZATION_STAMP = UEESerializationVersion('98367a43-6bf4-4b5c-b509-abea7e416416', 0, 1)
    SERIALIZATION_VERSION: int = 0
    OBJECT_NOT_FOUND: int = 0
    MAX_LENGTH: float = 0.0
    MAX_THICKNESS: float = 0.0
    KNOTMULTIPLIER_NO_KNOT: float = 0.0
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
