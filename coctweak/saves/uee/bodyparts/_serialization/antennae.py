# @GENERATED from coc/uee/classes/classes/BodyParts/Antennae.as
from coctweak.saves.common.bodyparts.antennae import BaseAntennae

__ALL__=['UEERawAntennae']

class UEERawAntennae(BaseAntennae):
    NONE: int = 0
    BEE: int = 0
    COCKATRICE: int = 0
    def __init__(self, save) -> None:
        super().__init__(save)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
