# @GENERATED from coc/uee/classes/classes/Ass.as
from coctweak.saves.common.bodyparts.ass import BaseAss
from coctweak.saves.uee.serialization import UEESerializationVersion

__ALL__=['UEERawAss']

class UEERawAss(BaseAss):
    SERIALIZATION_STAMP = UEESerializationVersion('0a3bd267-6ce0-4fed-a81b-53a4ccd6c17d', 0, 1)
    SERIALIZATION_VERSION: int = 0
    WETNESS_DRY: int = 0
    WETNESS_NORMAL: int = 0
    WETNESS_MOIST: int = 0
    WETNESS_SLIMY: int = 0
    WETNESS_DROOLING: int = 0
    WETNESS_SLIME_DROOLING: int = 0
    LOOSENESS_VIRGIN: int = 0
    LOOSENESS_TIGHT: int = 0
    LOOSENESS_NORMAL: int = 0
    LOOSENESS_LOOSE: int = 0
    LOOSENESS_STRETCHED: int = 0
    LOOSENESS_GAPING: int = 0
    def __init__(self, save) -> None:
        super().__init__(save)
        #self.virgin: bool = False
        self.buttRating: int = 0
    def serialize(self) -> dict:
        data = super().serialize()
        #data["virgin"] = self.virgin
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
        data["buttRating"] = self.buttRating
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        #self.virgin = self._getBool("virgin", False)
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
        self.buttRating = self._getInt("buttRating", 0, data)
