# @GENERATED from coc/uee/classes/classes/Vagina.as
from typing import Type
from typing import Type
from typing import Type
from coctweak.saves.common.bodyparts.vagina import BaseVagina
from enum import IntEnum
from coctweak.saves.uee.serialization import UEESerializationVersion

__ALL__=['UEERawVagina']

class UEERawVagina(BaseVagina):
    SERIALIZATION_STAMP = UEESerializationVersion('cfe61f89-7ab6-4e4b-83aa-33f738dd2f05', 0, 1)
    SERIALIZATION_VERSION: int = 0
    HUMAN: int = 0
    EQUINE: int = 0
    BLACK_SAND_TRAP: int = 0
    WETNESS_DRY: int = 0
    WETNESS_NORMAL: int = 0
    WETNESS_WET: int = 0
    WETNESS_SLICK: int = 0
    WETNESS_DROOLING: int = 0
    WETNESS_SLAVERING: int = 0
    LOOSENESS_TIGHT: int = 0
    LOOSENESS_NORMAL: int = 0
    LOOSENESS_LOOSE: int = 0
    LOOSENESS_GAPING: int = 0
    LOOSENESS_GAPING_WIDE: int = 0
    LOOSENESS_LEVEL_CLOWN_CAR: int = 0
    DEFAULT_CLIT_LENGTH: float = 0.0
    ENUM_WETNESS: Type[IntEnum] = None
    ENUM_LOOSENESS: Type[IntEnum] = None
    ENUM_TYPES: Type[IntEnum] = None
    def __init__(self, save) -> None:
        super().__init__(save)
        self.wetness: IntEnum = self.ENUM_WETNESS(0)
        self.looseness: IntEnum = self.ENUM_LOOSENESS(0)
        self.type: IntEnum = self.ENUM_TYPES(0)
        self.virgin: bool = False
        self.fullness: float = 0.0
        #self.labiaPierced: float = 0.0
        #self.labiaPShort: str = ''
        #self.labiaPLong: str = ''
        #self.clitPierced: float = 0.0
        #self.clitPShort: str = ''
        #self.clitPLong: str = ''
        self.clitLength: float = 0.0
        self.recoveryProgress: int = 0
    def serialize(self) -> dict:
        data = self.raw
        if self.SERIALIZATION_STAMP is not None:
            data = self.SERIALIZATION_STAMP.serialize(self, data)
        data["vaginalWetness"] = self.wetness.value
        data["vaginalLooseness"] = self.looseness.value
        data["type"] = self.type.value
        data["virgin"] = self.virgin
        data["fullness"] = self.fullness
        #data["labiaPierced"] = self.labiaPierced
        #data["labiaPShort"] = self.labiaPShort
        #data["labiaPLong"] = self.labiaPLong
        #data["clitPierced"] = self.clitPierced
        #data["clitPShort"] = self.clitPShort
        #data["clitPLong"] = self.clitPLong
        data["clitLength"] = self.clitLength
        data["recoveryProgress"] = self.recoveryProgress
        return data
    def deserialize(self, data: dict) -> None:
        self.raw = data
        if self.SERIALIZATION_STAMP is not None:
            self.SERIALIZATION_STAMP.deserialize(self, data)
        self.wetness = self.ENUM_WETNESS(self._getInt("vaginalWetness", 0))
        self.looseness = self.ENUM_LOOSENESS(self._getInt("vaginalLooseness", 0))
        self.type = self.ENUM_TYPES(self._getInt("type", 0))
        self.virgin = self._getBool("virgin", False)
        self.fullness = self._getFloat("fullness", 0.0)
        #self.labiaPierced = self._getFloat("labiaPierced", 0.0)
        #self.labiaPShort = self._getStr("labiaPShort", '')
        #self.labiaPLong = self._getStr("labiaPLong", '')
        #self.clitPierced = self._getFloat("clitPierced", 0.0)
        #self.clitPShort = self._getStr("clitPShort", '')
        #self.clitPLong = self._getStr("clitPLong", '')
        self.clitLength = self._getFloat("clitLength", 0.0)
        self.recoveryProgress = self._getInt("recoveryProgress", 0)
