# @GENERATED from coc/uee/classes/classes/ItemSlot.as
from coctweak.saves.common.item import BaseItem
from coctweak.saves.uee.serialization import UEESerializationVersion

__ALL__=['UEERawItem']

class UEERawItem(BaseItem):
    SERIALIZATION_STAMP = UEESerializationVersion('6c697f95-8c00-4082-9d28-39c1d6c147cd', 0, 2)
    def __init__(self):
        super().__init__()
        self.damage: int = 0
    def serialize(self) -> dict:
        data = super().serialize()
        data["damage"] = self.damage
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.damage = self._getInt("damage", 0)
