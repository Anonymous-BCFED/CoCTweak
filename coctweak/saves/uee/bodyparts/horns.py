from coctweak.saves.uee.bodyparts._serialization.horns import UEERawHorns
from coctweak.saves.uee.enums.horntypes import UEEHornTypes


class UEEHorns(UEERawHorns):
    ENUM_TYPES = UEEHornTypes
