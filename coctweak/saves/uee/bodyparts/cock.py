from coctweak.saves.common.bodyparts.cock import ECockTypeFlags
from coctweak.saves.uee.bodyparts._serialization.cock import UEERawCock
from coctweak.saves.uee.enums.cocksocktypes import UEECockSockTypes
from coctweak.saves.uee.enums.cocktypes import UEECockTypes
from coctweak.saves.uee.piercing import UEEPiercing

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class UEECock(UEERawCock):
    TYPE_PIERCING = UEEPiercing
    COCK_TYPES = UEECockTypes
    SOCK_TYPES = UEECockSockTypes
    # HAS_KNOT: See classes/classes/lists/GenitalLists.as.
    COCK_TYPE_FLAGS = {
        UEECockTypes.DOG:       ECockTypeFlags.HAS_KNOT,
        UEECockTypes.FOX:       ECockTypeFlags.HAS_KNOT,
        UEECockTypes.WOLF:      ECockTypeFlags.HAS_KNOT,
        UEECockTypes.DRAGON:    ECockTypeFlags.HAS_KNOT,
        UEECockTypes.DISPLACER: ECockTypeFlags.HAS_KNOT,
    }

    def __init__(self, save):
        super().__init__(save)

    def getCockTypeStr(self):
        return UEECockTypes(self.type).name

    def getKnotDiameter(self) -> float:
        return self.knotMultiplier * self.thickness

    def serialize(self) -> dict:
        return super().serialize()

    def hasKnot(self) -> bool:
        return self.hasTypeFlag(ECockTypeFlags.HAS_KNOT) and self.knotMultiplier > 1

    def hasSheath(self) -> bool:
        return self.hasTypeFlag(ECockTypeFlags.HAS_SHEATH)
