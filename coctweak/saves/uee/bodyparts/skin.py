from coctweak.saves.uee.enums.skintypes import UEESkinTypes
from coctweak.saves.uee.bodyparts._serialization.skin import UEERawSkin

class UEESkin(UEERawSkin):
    ENUM_TYPES = UEESkinTypes
