from coctweak.saves.uee.bodyparts._serialization.antennae import UEERawAntennae
from coctweak.saves.uee.enums.antennaetypes import UEEAntennaeTypes

class UEEAntennae(UEERawAntennae):
    ENUM_TYPES = UEEAntennaeTypes
