# @GENERATED from coc/uee/classes/classes/BodyParts/Skin.as
from enum import IntEnum

__all__ = ['UEESkinTypes']

class UEESkinTypes(IntEnum):
    PLAIN          = 0
    FUR            = 1
    LIZARD_SCALES  = 2
    GOO            = 3
    UNDEFINED      = 4
    DRAGON_SCALES  = 5
    FISH_SCALES    = 6
    WOOL           = 7
    FEATHERED      = 8
    BARK           = 9
