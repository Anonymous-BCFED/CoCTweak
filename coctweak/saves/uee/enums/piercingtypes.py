# @GENERATED from coc/uee/classes/classes/BodyParts/Piercing.as
from enum import IntEnum

__all__ = ['UEEPiercingType']

class UEEPiercingType(IntEnum):
    NONE    = 0
    STUD    = 1
    RING    = 2
    LADDER  = 3
    HOOP    = 4
    CHAIN   = 5
