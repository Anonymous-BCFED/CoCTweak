# @GENERATED from coc/uee/classes/classes/BodyParts/Horns.as
from enum import IntEnum

__all__ = ['UEEHornTypes']

class UEEHornTypes(IntEnum):
    NONE                      = 0
    DEMON                     = 1
    COW_MINOTAUR              = 2
    DRACONIC_X2               = 3
    DRACONIC_X4_12_INCH_LONG  = 4
    ANTLERS                   = 5
    GOAT                      = 6
    UNICORN                   = 7
    RHINO                     = 8
    SHEEP                     = 9
    RAM                       = 10
    IMP                       = 11
