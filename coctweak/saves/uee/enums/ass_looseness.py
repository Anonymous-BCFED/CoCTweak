# @GENERATED from coc/uee/classes/classes/Ass.as
from enum import IntEnum

__all__ = ['UEEAssLooseness']

class UEEAssLooseness(IntEnum):
    VIRGIN     = 0
    TIGHT      = 1
    NORMAL     = 2
    LOOSE      = 3
    STRETCHED  = 4
    GAPING     = 5
