# @GENERATED from coc/uee/classes/classes/BodyParts/Face.as
from enum import IntEnum

__all__ = ['UEEFaceTypes']

class UEEFaceTypes(IntEnum):
    HUMAN         = 0
    HORSE         = 1
    DOG           = 2
    COW_MINOTAUR  = 3
    SHARK_TEETH   = 4
    SNAKE_FANGS   = 5
    CATGIRL       = 6
    LIZARD        = 7
    BUNNY         = 8
    KANGAROO      = 9
    SPIDER_FANGS  = 10
    FOX           = 11
    DRAGON        = 12
    RACCOON_MASK  = 13
    RACCOON       = 14
    BUCKTEETH     = 15
    MOUSE         = 16
    FERRET_MASK   = 17
    FERRET        = 18
    PIG           = 19
    BOAR          = 20
    RHINO         = 21
    ECHIDNA       = 22
    DEER          = 23
    WOLF          = 24
    COCKATRICE    = 25
    BEAK          = 26
    RED_PANDA     = 27
    CAT           = 28
