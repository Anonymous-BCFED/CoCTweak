# @GENERATED from coc/uee/classes/classes/Vagina.as
from enum import IntEnum

__all__ = ['UEEVagWetness']

class UEEVagWetness(IntEnum):
    DRY        = 0
    NORMAL     = 1
    WET        = 2
    SLICK      = 3
    DROOLING   = 4
    SLAVERING  = 5
