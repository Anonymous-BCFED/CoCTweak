# @GENERATED from coc/uee/classes/classes/BodyParts/Ears.as
from enum import IntEnum

__all__ = ['UEEEarTypes']

class UEEEarTypes(IntEnum):
    HUMAN       = 0
    HORSE       = 1
    DOG         = 2
    COW         = 3
    ELFIN       = 4
    CAT         = 5
    LIZARD      = 6
    BUNNY       = 7
    KANGAROO    = 8
    FOX         = 9
    DRAGON      = 10
    RACCOON     = 11
    MOUSE       = 12
    FERRET      = 13
    PIG         = 14
    RHINO       = 15
    ECHIDNA     = 16
    DEER        = 17
    WOLF        = 18
    SHEEP       = 19
    IMP         = 20
    COCKATRICE  = 21
    RED_PANDA   = 22
