# @GENERATED from coc/uee/classes/classes/BodyParts/Antennae.as
from enum import IntEnum

__all__ = ['UEEAntennaeTypes']

class UEEAntennaeTypes(IntEnum):
    NONE        = 0
    BEE         = 2
    COCKATRICE  = 3
