from typing import Tuple, Optional, Any
from coctweak.saves._serialization import SerializationVersionInfo
from coctweak.logsys import getLogger
log = getLogger(__name__)

class UEESerializationVersion(SerializationVersionInfo):
    '''
    serializationVersionDictionary:
        0a3bd267-6ce0-4fed-a81b-53a4ccd6c17d: 1
    '''

    KEY = 'serializationVersionDictionary'
    OLD_KEY = 'serializationVersion'

    def __init__(self, uuid: str, oldest_ver: int, newest_ver: int):
        self.oldest_permitted_version: int = oldest_ver
        self.current_version: int = newest_ver
        self.uuid: str = uuid

    def serialize(self, obj: Any, data: dict) -> dict:
        if self.OLD_KEY in data:
            del data[self.OLD_KEY]
        data[self.KEY] = {self.uuid: self.current_version}
        return data

    def deserialize(self, obj: Any, data: dict, objname: Optional[str] = None) -> None:
        if objname is None:
            objname = obj.__class__.__name__
        if obj is not None:
            obj.version = 0
        if self.OLD_KEY not in data and self.KEY not in data and self.oldest_permitted_version > 0:
            log.warning(f'{objname}: Save serialization version stamp not present in this object.  We could break something by messing with this!')
            return
        uuid = None
        version = 0
        if self.OLD_KEY in data:
            log.warning(f'{objname}: Object using legacy version stamp!')
            version = data[self.OLD_KEY]
        elif self.KEY in data:
            uuid, version = next(iter(data[self.KEY].items()))
            assert uuid == self.uuid, f'{objname}: Cannot deserialize this object: save serialization UUID ({uuid}) does not equal ours ({self.uuid}). This generally means we\'re going to break shit by reading it. I think.'
        assert version >= self.oldest_permitted_version, f'{objname}: Cannot deserialize this object: save serialization version ({version}) < coctweak\'s oldest acceptable version ({self.oldest_permitted_version}), meaning we could corrupt things.'
        assert version <= self.current_version, f'{objname}: Cannot deserialize this object: save serialization version ({version}) > coctweak\'s newest acceptable version ({self.current_version}), meaning it\'s too new for us to read.'
        if obj is not None:
            obj.version = version
