#from .save import UEESave
from .enums.perklib import UEEPerkLib
from .enums.itemlib import UEEItemLib
from .enums.kflags import UEEKFlags

# I may move this into a utils file.
class UEEBimboProgression(object):
    @staticmethod
    def ableToProgress(save: 'UEESave') -> bool:
        ## from [UEE]/classes/classes/Scenes/BimboProgression.as: public 	function ableToProgress():Boolean { @ 0FNexfewi9WQbSy2qc8R26jp25mdcO8V5b1ieXc8aO33i6kLaZc6POgkp9HOdBI9Ye41m9sU7wv5QOfFT3gjakr1HeauRfMX
        #if ( (player.findPerk(PerkLib.BimboBrains) >= 0) && (player.findPerk(PerkLib.BimboBody) >= 0) ) return false;
        if save.hasPerk(UEEPerkLib.BimboBrains) and save.hasPerk(UEEPerkLib.BimboBody):
            return False
        #if (player.lowerGarment != UndergarmentLib.NOTHING) return false;
        if save.lowerGarment.item.id != UEEItemLib.NONE:
            return False
        #if (player.armorName == "bimbo skirt" && flags[kFLAGS.BIMBO_MINISKIRT_PROGRESS_DISABLED] == 0) return true;
        if save.armor.item.id == UEEItemLib.BIMBOSK and save.flags.get(UEEKFlags.BIMBO_MINISKIRT_PROGRESS_DISABLED, 0) == 0:
            return True
        #return false;
        return False
