from coctweak.saves.common.piercing import BasePiercing
from coctweak.saves.uee.enums.piercingtypes import UEEPiercingType
class UEEPiercing(BasePiercing):
    ENUM_TYPES = UEEPiercingType
