from coctweak.saves.uee.bodyparts._serialization.item import UEERawItem
from .serialization import UEESerializationVersion
class UEEItem(UEERawItem):
    DEFAULT_VALUE = 6
    MAX_INVENTORY_SLOTS = 10

    def __init__(self):
        super().__init__()
        self.bonus: str = ''

    def can_sort(self):
        return self.unlocked

    def cloneMutables(self, template):
        super().cloneMutables(template)
        #self.unlocked = template.unlocked
        self.bonus = template.bonus

    def cloneImmutables(self, template):
        super().cloneMutables(template)
        self.unlocked = template.unlocked
        #self.bonus = template.bonus

    def displayValues(self, extra=None, hide_locked=False):
        if extra is None:
            extra = {}
        if hide_locked and not self.unlocked:
            return '--LOCKED--'.center(14)
        #extra['Unlocked'] = self.unlocked
        return super().displayValues(extra, hide_locked)
