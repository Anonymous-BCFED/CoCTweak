from coctweak.saves.pregnancystore import FlagPregnancyStore, VarPregnancyStore
from coctweak.saves.uee.enums.kflags import UEEKFlags
from coctweak.saves.uee.enums.pregnancytypes import UEEPregnancyType
from coctweak.saves.uee.enums.incubationdurations import UEEIncubationDuration

class UEEFlagPregnancyStore(FlagPregnancyStore[UEEPregnancyType, UEEIncubationDuration]):
    PREGTYPE_ENUM = UEEPregnancyType
    INCUBATION_ENUM = UEEIncubationDuration

class UEEVarPregnancyStore(VarPregnancyStore[UEEPregnancyType, UEEIncubationDuration]):
    PREGTYPE_ENUM = UEEPregnancyType
    INCUBATION_ENUM = UEEIncubationDuration
