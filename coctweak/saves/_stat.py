from ._saveobj import SaveObject
from enum import IntEnum
from typing import Callable

class StatHealType(IntEnum):
    NONE = 0
    MIN = 1
    MAX = 2

class Stat(SaveObject):
    def __init__(self, _id: str, name: str, defaultValue: float, mincb: Callable[[], float], maxcb: Callable[[], float], healtype=StatHealType.NONE):
        super().__init__()
        self.id: str = _id
        self.name: str = name
        self._value: float = defaultValue

        self.mincb: Callable[[], float] = mincb
        self.maxcb: Callable[[], float] = maxcb

        self.healtype: StatHealType = healtype

    def get_value(self) -> float:
        return self._value

    def set_value(self, value: float) -> None:
        self._value = min(max(value, self.min), self.max)

    def set_value_raw(self, value: float) -> None:
        self._value = value

    value = property(get_value, set_value)

    @property
    def min(self) -> float:
        return self.mincb()

    @property
    def max(self) -> float:
        return self.maxcb()

    @property
    def percent(self) -> float:
        return (self.value / self.max) * 100.

    def heal(self) -> None:
        if self.healtype == StatHealType.NONE:
            return
        self._value = self.min if self.healtype == StatHealType.MIN else self.max

    def get_healed_value(self) -> None:
        if self.healtype == StatHealType.NONE:
            return self._value
        return self.min if self.healtype == StatHealType.MIN else self.max
