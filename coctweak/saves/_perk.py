from typing import List
from coctweak.saves._quartet import QuartetWithMeta

from coctweak.logsys import getLogger
log = getLogger(__name__)
class Perk(QuartetWithMeta):
    ID_KEY='id'
    def __init__(self) -> None:
        super().__init__()
        self.name:str = ''
        self.keepOnAscension:bool = False
        self.bonusStats:dict = {}

    def load_from_meta(self, data) -> None:
        assert data is not None, f'Perk {self.id!r} does not have metadata!'
        super().load_from_meta(data)
        self.name = data.get('name', self.id)
        self.keepOnAscension = data.get('keepOnAscension', False)
        self.bonusStats = data.get('bonusStats', {})

    def longPrintValues(self) -> None:
        super().longPrintValues()
        if self.bonusStats is not None:
            with log.info('Bonus Stats:'):
                for k, v in self.bonusStats.items():
                    log.info(f'{k}: {v!r}')

    @property
    def isPermanent(self) -> bool:
        return False
    @isPermanent.setter
    def set_isPermanent(self, value: bool) -> None:
        return
