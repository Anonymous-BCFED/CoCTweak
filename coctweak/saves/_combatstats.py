from typing import List, Optional
from coctweak.saves._stat_collection import StatCollection
from coctweak.saves._stat import Stat, StatHealType
from ._bonus_derived_stat_ids import BaseBonusDerivedStatIDs

from coctweak.logsys import getLogger
log = getLogger(__name__)
class BaseCombatStats(StatCollection):
    def __init__(self, save):
        self.hp: Stat = None
        self.xp: Stat = None
        self.level: Stat = None
        self.lust: Stat = None
        self.fatigue: Stat = None
        self.fertility: Stat = None
        self.femininity: Stat = None
        super().__init__(save)


    def init(self):
        self.hp      = self._add(Stat('HP',      'Health',     0.0, lambda: 0, self._get_hp_max, healtype=StatHealType.MAX))
        self.xp      = self._add(Stat('XP',      'Experience', 0.0, lambda: 0, self._get_xp_max))
        self.level   = self._add(Stat('level',   'Level',      1.0, lambda: 0, lambda: 100))
        self.lust    = self._add(Stat('lust',    'Lust',       0.0, self._get_lust_min, self._get_lust_max, healtype=StatHealType.MIN))
        self.fatigue = self._add(Stat('fatigue', 'Fatigue',    0.0, lambda: 0, self._get_max_fatigue, healtype=StatHealType.MIN))

        self.fertility = self._add(Stat('fertility', 'Fertility', 0., lambda: 0., lambda: 100., healtype=StatHealType.NONE))
        self.femininity = self._add(Stat('femininity', 'Femininity', 0., lambda: 0., lambda: 100., healtype=StatHealType.NONE))

    def heal(self, toHeal: Optional[List[str]] = None, diagnose_only: bool=False) -> dict:
        report = {}
        for stat in self.all:
            if (toHeal is None or stat.id in toHeal) and stat.healtype != StatHealType.NONE:
                if diagnose_only:
                    report[stat.name] = (stat.value, stat.get_healed_value())
                else:
                    before = stat.value
                    stat.heal()
                    after = stat.value
                    report[stat.name] = (before, after)
        return report

    def _get_hp_max(self) -> float:
        return 100 # TODO

    def _get_xp_max(self) -> float:
        return min(9999, self.level.value*100)

    def _get_lust_min(self) -> float:
        return 0
    def _get_lust_max(self) -> float:
        #var max:Number = 100;
        m = 100
        #if (this == game.player && game.player.demonScore() >= 4) max += 20;
        # skip for now
        #if (findPerk(PerkLib.ImprovedSelfControl) >= 0) max += 20;
        if self.save.hasPerk('Improved Self Control'):
            m += 20
        #if (hasPerk(PerkLib.ImprovedSelfControl2)) max += 20;
        if self.save.hasPerk('Improved Self Control 2'):
            m += 20
        #if (findPerk(PerkLib.BroBody) >= 0 || findPerk(PerkLib.BimboBody) >= 0 || findPerk(PerkLib.FutaForm) >= 0) max += 20;
        if self.save.hasPerk('Bro Body') or self.save.hasPerk('Bimbo Body') or self.save.hasPerk('Futa Form'):
            m += 20
        #if (findPerk(PerkLib.OmnibusGift) >= 0) max += 15;
        if self.save.hasPerk('Omnibus Gift'):
            m += 20
        #if (findPerk(PerkLib.AscensionDesires) >= 0) max += perkv1(PerkLib.AscensionDesires) * 5;
        p = self.save.perks.get('Ascension Desires')
        if p is not None:
            m += p.values[0] * 5
        #if (findPerk(PerkLib.NephilaArchQueen) >= 0) max += 40;
        #if (max > 999) max = 999;
        m = min(m, 999)
        return m

    def _get_max_fatigue(self) -> float:
        #var max:Number = 100;
        m = 100
        #if (findPerk(PerkLib.ImprovedEndurance) >= 0) max += 20;
        if self.save.hasPerk('Improved Endurance'):
            m += 20
        #if (findPerk(PerkLib.ImprovedEndurance2) >= 0) max += 10;
        if self.save.hasPerk('Improved Endurance 2'):
            m += 10
        #if (findPerk(PerkLib.AscensionEndurance) >= 0) max += perkv1(PerkLib.AscensionEndurance) * 5;
        if self.save.hasPerk('Ascension Endurance'):
            m += self.save.perks['Ascension Endurance'].values[0] * 5
        #max += getBonusStat(BonusDerivedStats.fatigueMax);
        m += self.save.bonuses.get(BaseBonusDerivedStatIDs.fatigueMax)
        #if (max > 999) max = 999;
        m = min(m, 999)
        #return max;
        return m
