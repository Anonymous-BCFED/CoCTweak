from abc import ABC, abstractmethod
from typing import Union
from enum import IntEnum

class IAppearance(ABC):
    def __init__(self, save) -> None:
        self.save = save
        super().__init__()

    @abstractmethod
    def breastCup(self, _cupSize: Union[int, IntEnum]) -> str:
        return ''

    @abstractmethod
    def breastSize(self, _cupSize: Union[int, IntEnum]) -> str:
        return ''
