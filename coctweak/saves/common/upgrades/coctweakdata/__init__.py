from .coctweakdataV2 import CoCTweakDataVersion2Upgrader
ALL_UPGRADES = [
    CoCTweakDataVersion2Upgrader() # 1 -> 2
]
