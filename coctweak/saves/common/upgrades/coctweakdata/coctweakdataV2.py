from coctweak.saves.common.upgrade import VersionedUpgrade
class CoCTweakDataVersion2Upgrader(VersionedUpgrade):
    NAME = 'Update Backpack Hack Schema to v2 Schema'
    VERSION_START = 1
    VERSION_END = 2

    def attemptFix(self, data: dict) -> bool:
        data['backpack-hack'] = {
            'is-hacked': data.get('unlock_inventory_slots', None) is not None,
            'had-backpack': data.get('backup_backpack_size', None) is not None,
            'old-data': [data.pop('backup_backpack_size', 0), 0, 0, 0],
            'desired-size': data.pop('unlock_inventory_slots', None),
        }
