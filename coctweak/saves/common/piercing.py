from enum import IntEnum
from typing import Type, Optional, Dict
from collections import OrderedDict
from coctweak.saves._saveobj import SaveObject

class BasePiercing(SaveObject):
    ENUM_TYPES: Type[IntEnum] = None

    SINGULAR_PIERCED_SUFFIX: str = 'pierced'
    SINGULAR_LONG_SUFFIX: str = 'pLongDesc'
    SINGULAR_SHORT_SUFFIX: str = 'pShortDesc'

    MULTIPLE_PIERCED_SUFFIX: str = 'Pierced'
    MULTIPLE_LONG_SUFFIX: str = 'PLong'
    MULTIPLE_SHORT_SUFFIX: str = 'PShort'

    @classmethod
    def SimpleLoadFrom(cls: Type['BasePiercing'], data: dict, prefix: Optional[str]) -> Optional['BasePiercing']:
        if prefix is not None:
            return cls.LoadFrom(data, f'{prefix}{cls.MULTIPLE_PIERCED_SUFFIX}', f'{prefix}{cls.MULTIPLE_LONG_SUFFIX}', f'{prefix}{cls.MULTIPLE_SHORT_SUFFIX}')
        else:
            return cls.LoadFrom(data, cls.SINGULAR_PIERCED_SUFFIX, cls.SINGULAR_LONG_SUFFIX, cls.SINGULAR_SHORT_SUFFIX)

    @classmethod
    def LoadFrom(cls: Type['BasePiercing'], data: dict, typeField: str, longField: str, shortField: str) -> Optional['BasePiercing']:
        piercing: Optional['BasePiercing'] = None
        if typeField in data and data[typeField] > 0:
            piercing = cls()
            piercing.deserializeFrom(data, typeField, longField, shortField)
        return piercing

    def __init__(self):
        self.type: IntEnum = self.ENUM_TYPES.NONE
        self.shortDesc: str = ''
        self.longDesc: str = ''

        self.fieldMapping = {}

    def simpleSerializeTo(self, data: dict, prefix: Optional[str]) -> None:
        if prefix is not None:
            self.serializeTo(data, f'{prefix}{self.MULTIPLE_PIERCED_SUFFIX}', f'{prefix}{self.MULTIPLE_LONG_SUFFIX}', f'{prefix}{self.MULTIPLE_SHORT_SUFFIX}')
        else:
            self.serializeTo(data, self.SINGULAR_PIERCED_SUFFIX, self.SINGULAR_LONG_SUFFIX, self.SINGULAR_SHORT_SUFFIX)

    def serializeTo(self, data: dict, typeField: Optional[str] = None, longField: Optional[str] = None, shortField: Optional[str] = None) -> None:
        typeField = typeField if typeField is not None else self.fieldMapping['typeField']
        longField = longField if longField is not None else self.fieldMapping['longField']
        shortField = shortField if shortField is not None else self.fieldMapping['shortField']

        data[typeField] = self.type.value
        data[longField] = self.longDesc
        data[shortField] = self.shortDesc

        #print(repr(data))

    def deserializeFrom(self, data: dict, typeField: str, longField: str, shortField: str) -> None:
        self.fieldMapping['type'] = typeField
        self.type = self.ENUM_TYPES(self._getInt(typeField, data))
        self.fieldMapping['shortDesc'] = shortField
        self.shortDesc = self._getStr(shortField, data)
        self.fieldMapping['longDesc'] = longField
        self.longDesc = self._getStr(longField, data)

    def getVars(self) -> Dict[str, str]:
        o = OrderedDict()
        o['Type'] = f'{self.type.name} ({self.type.value})'
        if self.type != self.ENUM_TYPES.NONE:
            o['Short Desc'] = self.shortDesc
            o['Long Desc'] = self.longDesc
        return o

    def __str__(self):
        return f'a {self.type.name} {self.short} ({self.long})'
