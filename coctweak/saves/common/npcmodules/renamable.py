import argparse
from coctweak.saves._selfsaving import SelfSavingObject
class RenamableNPC(object):
    def __init__(self, save) -> None:
        self.name_default = ''

        self.name_flag = ''
        # OR
        self.name_ssoname = ''
        self.name_ssoprop = ''

    def setupRenamable(self, default='', flag=None, ssoname=None, ssoprop=None) -> None:
        self.name_default = default
        self.name_flag = flag
        self.name_ssoname = ssoname
        self.name_ssoprop = ssoprop

    @property
    def name(self) -> str:
        if self.name_flag is not None:
            return self.save.flags.get(self.name_flag, self.name_default)
        if self.name_ssoprop is not None:
            sso: SelfSavingObject = self.save.selfSavingObjects.get(self.name_ssoname)
            if sso is None:
                return self.name_default
            return getattr(sso, self.name_ssoprop)
    @name.setter
    def name(self, value: str) -> None:
        if self.name_flag is not None:
            self.save.flags.set(self.name_flag, value)
        if self.name_ssoprop is not None:
            sso: SelfSavingObject = self.save.selfSavingObjects.get(self.name_ssoname)
            if sso is None:
                return
            setattr(sso, self.name_ssoprop, value)

    def register_cli_rename(self, subp: argparse.ArgumentParser) -> None:
        p_rename = subp.add_parser('rename', help=f'Rename {self.NAME}.')
        p_rename.add_argument('name', type=str, help='Their new name.')
        p_rename.set_defaults(npc_cmd=self.cmd_rename)

    def cmd_rename(self, args: argparse.Namespace) -> bool:
        print(f'Renaming {self.name} -> {args.name}...')
        self.name = args.name
        return True
