from coctweak.saves._statuseffect import StatusEffect
class CommonMarbleEffect(StatusEffect):
    KEY = 'Marble'
    def displayValues(self):
        mrblflags = int(self.values[2])
        isAddict = (mrblflags & 1) == 1
        isAshamed = (mrblflags & 2) == 2
        flagRepr = ['addict' if isAddict else 'not addict', 'ashamed' if isAshamed else 'not ashamed']
        return f'Affection: {self.values[0]}, Addiction: {self.values[1]}, Addiction: '+(', '.join(flagRepr)) + f' ({mrblflags}), Corruption: {self.values[3]}'
