from typing import Dict, Type
from enum import Enum

from coctweak.saves.common.bodyparts._serialization.antennae import RawAntennae
from coctweak.utils import display_enum

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)


class BaseAntennae(RawAntennae):
    CATEGORY = 'Antennae'

    ENUM_TYPES: Type[Enum] = None

    def getTypeStr(self):
        if self.ENUM_TYPES is not None:
            return self.ENUM_TYPES(self.type).name
        else:
            return f'type {self.type:g}'

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Type'] = display_enum(self.type)
        return o

    def getShortDesc(self) -> str:
        if self.type == self.ENUM_TYPES.NONE:
            return 'None'
        return f'{self.getTypeStr()} antennae'
