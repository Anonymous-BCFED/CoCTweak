from typing import Dict, Optional, Type

from coctweak.saves.common.bodyparts._serialization.balls import RawBalls
from coctweak.utils import display_bool, display_enum, display_length

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

from enum import Enum


class BaseBalls(RawBalls):
    CATEGORY = 'Balls'

    def getLongDesc(self,forcedSize: bool=True, plural: Optional[bool]=None, inclArticle: bool=False) -> str:
        return ''

    def getShortDesc(self) -> str:
        return f'{self.count}x balls, each {display_length(self.size)} around, cum multiplier of {self.cumMultiplier:g}'

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Count'] = f'{self.count:g}'
        o['Cum Multiplier'] = f'{self.cumMultiplier:g}'
        o['Size'] = f'{self.size:g}"'
        return o
