from enum import IntEnum
from typing import Dict, Type

from coctweak.saves._saveobj import SaveObject
from coctweak.saves.common.bodyparts._serialization.skin import RawSkin
from coctweak.utils import display_enum

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class BaseSkin(RawSkin):
    CATEGORY = 'Skin'
    ENUM_TYPES: Type[IntEnum] = None

    def __init__(self, save):
        super().__init__(save)
        self.type: IntEnum = self.ENUM_TYPES(0)
        self.tone: str = ''
        self.desc: str = ''
        self.adj: str = ''
        self.furColor: str = 'no'

    def getShortDesc(self) -> str:
        # PLAIN, light skin
        ## from [HGG]/classes/classes/BodyParts/Skin.as: public function description(noAdj:Boolean = false, noTone:Boolean = false):String { @ gOxdu80Qi3pn5ST8iw7Xq16Jfunc7R5BFcnDabP1mV2M62b94fRgXNdHO9DR7hg89C7SW0BQcZ9dRA11H6fqd0r69C45UfcO
        o = ''
        if self.tone not in ('rough gray',) and self.adj != '':
            o += self.adj + ', '
        o += self.tone + ' '
        o += self.desc
        ## end
        return o

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Type'] = display_enum(self.type)
        o['Tone'] = self.tone
        o['Descriptor'] = self.desc
        o['Adjective'] = self.adj
        o['Fur Color'] = self.furColor
        return o

    def getTypeStr(self) -> str:
        if self.ENUM_TYPES is not None:
            return self.ENUM_TYPES(self.type).name
        else:
            return f'type {self.type:g}'
