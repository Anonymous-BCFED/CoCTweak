from enum import IntEnum
from typing import Dict, Type

from coctweak.saves._saveobj import SaveObject
from coctweak.saves.common.bodyparts._serialization.wings import RawWings
from coctweak.utils import display_enum

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class BaseWings(RawWings):
    CATEGORY = 'Wings'
    ENUM_TYPES: Type[IntEnum] = None

    def getShortDesc(self) -> str:
        return self.getTypeStr()

    def getVars(self) -> Dict[str, str]:
        o =  super().getVars()
        o['Type'] = display_enum(self.type)
        o['Color 1'] = self.color1
        o['Color 2'] = self.color2
        return o

    def getTypeStr(self) -> str:
        if self.ENUM_TYPES is not None:
            return self.ENUM_TYPES(self.type).name
        else:
            return f'type {self.type:g}'
