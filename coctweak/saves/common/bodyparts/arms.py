from typing import Dict, Type
from enum import Enum, IntEnum

from coctweak.saves.common.bodyparts._serialization.arms import RawArms
from coctweak.utils import display_enum

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)


class BaseArms(RawArms):
    CATEGORY = 'Arms'

    ENUM_TYPES: Type[Enum] = None
    def getTypeStr(self):
        if self.ENUM_TYPES is not None:
            return self.ENUM_TYPES(self.type).name
        else:
            return f'type {self.type:g}'

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Type'] = display_enum(self.type)
        return o

    def getShortDesc(self) -> str:
        return f'{self.getTypeStr()} arms'
