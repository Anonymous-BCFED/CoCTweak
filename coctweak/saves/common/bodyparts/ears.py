from enum import IntEnum
from typing import Dict, Optional, Type

from coctweak.saves._saveobj import SaveObject
from coctweak.saves.common.bodyparts._serialization.ears import RawEars
from coctweak.saves.common.piercing import BasePiercing
from coctweak.utils import display_enum

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class BaseEars(RawEars):
    CATEGORY = 'Ears'

    TYPE_PIERCING: Type[BasePiercing] = None
    ENUM_TYPES: Type[IntEnum] = None

    def __init__(self, save):
        super().__init__(save)
        self.piercing: Optional[BasePiercing] = None

    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
        self.piercing = self.TYPE_PIERCING.SimpleLoadFrom(data, 'ears')

    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
        if self.piercing is None:
            self.piercing = self.TYPE_PIERCING()
        self.piercing.simpleSerializeTo(data, 'ears')

    def getShortDesc(self) -> str:
        return f'{self.value:g}x {self.type.name}'

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Type'] = display_enum(self.type)
        if self.piercing and self.piercing.type != self.piercing.ENUM_TYPES.NONE:
            o['Piercing'] = self.piercing.getVars()
        return o
