from enum import IntEnum
from typing import Dict, Type

from coctweak.saves._saveobj import SaveObject
from coctweak.saves.common.bodyparts._serialization.horns import RawHorns
from coctweak.utils import display_enum

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class BaseHorns(RawHorns):
    CATEGORY = 'Horns'
    ENUM_TYPES: Type[IntEnum] = None

    def __init__(self, save):
        super().__init__(save)
        self.type: IntEnum = self.ENUM_TYPES(0)
        self.count: int = 0

    def getShortDesc(self) -> str:
        return f'{self.count}x {self.getTypeStr()}'

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Type'] = display_enum(self.type)
        o['Count'] = f'{self.count:g}'
        return o

    def getTypeStr(self) -> str:
        if self.ENUM_TYPES is not None:
            return self.ENUM_TYPES(self.type).name
        else:
            return f'type {self.type:g}'
