from enum import IntEnum
from typing import Dict, Type

from coctweak.saves._saveobj import SaveObject
from coctweak.saves.common.bodyparts._serialization.lowerbody import RawLowerBody  # NOQA
from coctweak.utils import display_bool, display_enum

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class BaseLowerBody(RawLowerBody):
    CATEGORY = 'Lower Body'
    ENUM_TYPES: Type[IntEnum] = None
    HAS_HELPERS = True

    def __init__(self, save):
        super().__init__(save)
        self.type: IntEnum = self.ENUM_TYPES(0)
        self.count: int = 2
        self.incorporeal: bool = False

    def getShortDesc(self) -> str:
        adjlist = []
        if self.incorporeal:
            adjlist.append('incorporeal')
        adj = ''
        if len(adjlist) > 0:
            adj = ' '+', '.join(adj)
        return f'{self.count}x{adj} {self.getTypeStr()} legs'

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Type'] = display_enum(self.type)
        o['Leg Count'] = f'{self.count:g}'
        o['Incorporeal'] = display_bool(self.incorporeal)
        return o

    def getHelpers(self) -> Dict[str, str]:
        o = super().getHelpers()
        o['Is Drider'] = display_bool(self.isDrider())
        o['Is Taur'] = display_bool(self.isTaur())
        return o

    def getTypeStr(self) -> str:
        if self.ENUM_TYPES is not None:
            return self.ENUM_TYPES(self.type).name
        else:
            return f'type {self.type:g}'

    def isTaur(self) -> bool:
        return self.count > 2 and not self.isDrider()

    def isDrider(self) -> bool:
        return self.type.name == 'DRIDER'