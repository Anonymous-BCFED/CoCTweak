# @GENERATED from coc/vanilla/classes/classes/BreastRowClass.as
from coctweak.saves.common.bodyparts.bodypart import BodyPart

__ALL__=['RawBreastRow']

class RawBreastRow(BodyPart):
    def __init__(self, save) -> None:
        super().__init__(save)
        self.breasts: int = 2
        self.nipplesPerBreast: int = 1
        self.breastRating: float = 0.0
        self.lactationMultiplier: float = 0.0
        self.milkFullness: float = 0.0
        self.fullness: float = 0.0
        self.fuckable: bool = False
    def serialize(self) -> dict:
        data = super().serialize()
        data["breasts"] = self.breasts
        data["nipplesPerBreast"] = self.nipplesPerBreast
        data["breastRating"] = self.breastRating
        data["lactationMultiplier"] = self.lactationMultiplier
        data["milkFullness"] = self.milkFullness
        data["fullness"] = self.fullness
        data["fuckable"] = self.fuckable
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.breasts = self._getInt("breasts", 2)
        self.nipplesPerBreast = self._getInt("nipplesPerBreast", 1)
        self.breastRating = self._getFloat("breastRating", 0.0)
        self.lactationMultiplier = self._getFloat("lactationMultiplier", 0.0)
        self.milkFullness = self._getFloat("milkFullness", 0.0)
        self.fullness = self._getFloat("fullness", 0.0)
        self.fuckable = self._getBool("fuckable", False)
