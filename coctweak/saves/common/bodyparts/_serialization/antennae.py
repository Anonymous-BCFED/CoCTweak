# @GENERATED from coc/uee/classes/classes/BodyParts/Antennae.as
from typing import Type
from coctweak.saves.common.bodyparts.bodypart import BodyPart
from enum import IntEnum

__ALL__=['RawAntennae']

class RawAntennae(BodyPart):
    NONE: int = 0
    BEE: int = 0
    COCKATRICE: int = 0
    ENUM_TYPES: Type[IntEnum] = None
    def __init__(self, save) -> None:
        super().__init__(save)
        self.type: IntEnum = self.ENUM_TYPES(0)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
        data["antennae"] = self.type.value
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
        self.type = self.ENUM_TYPES(self._getInt("antennae", 0, data))
