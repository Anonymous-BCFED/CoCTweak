# @GENERATED 
from typing import Type
from coctweak.saves.common.bodyparts.bodypart import BodyPart
from enum import IntEnum

__ALL__=['RawTail']

class RawTail(BodyPart):
    ENUM_TYPES: Type[IntEnum] = None
    def __init__(self, save) -> None:
        super().__init__(save)
        self.type: IntEnum = self.ENUM_TYPES(0)
        self.venom: float = 0
        self.recharge: float = 0
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
        data["tailType"] = self.type.value
        data["tailVenum"] = self.venom
        data["tailRecharge"] = self.recharge
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
        self.type = self.ENUM_TYPES(self._getInt("tailType", 0, data))
        self.venom = self._getFloat("tailVenum", 0, data)
        self.recharge = self._getFloat("tailRecharge", 0, data)
