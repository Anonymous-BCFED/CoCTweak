# @GENERATED 
from typing import Type
from coctweak.saves.common.bodyparts.bodypart import BodyPart
from enum import IntEnum

__ALL__=['RawEars']

class RawEars(BodyPart):
    ENUM_TYPES: Type[IntEnum] = None
    def __init__(self, save) -> None:
        super().__init__(save)
        self.type: IntEnum = self.ENUM_TYPES(0)
        self.value: float = 0
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
        data["earType"] = self.type.value
        data["earValue"] = self.value
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
        self.type = self.ENUM_TYPES(self._getInt("earType", 0, data))
        self.value = self._getFloat("earValue", 0, data)
