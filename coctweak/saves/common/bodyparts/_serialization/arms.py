# @GENERATED from coc/uee/classes/classes/BodyParts/Arms.as
from typing import Type
from coctweak.saves.common.bodyparts.bodypart import BodyPart
from enum import IntEnum

__ALL__=['RawArms']

class RawArms(BodyPart):
    HUMAN: int = 0
    HARPY: int = 0
    SPIDER: int = 0
    BEE: int = 0
    PREDATOR: int = 0
    SALAMANDER: int = 0
    WOLF: int = 0
    COCKATRICE: int = 0
    RED_PANDA: int = 0
    FERRET: int = 0
    CAT: int = 0
    DOG: int = 0
    FOX: int = 0
    ENUM_TYPES: Type[IntEnum] = None
    def __init__(self, save) -> None:
        super().__init__(save)
        #self.creature: Any = 'None'
        self.type: IntEnum = self.ENUM_TYPES(0)
        #self.claws: Any = 'None'
    def serialize(self) -> dict:
        data = super().serialize()
        #data["_creature"] = self.creature
        #data["claws"] = self.claws
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
        data["armType"] = self.type.value
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        #self.creature = data.get("_creature", 'None')
        #self.claws = data.get("claws", 'None')
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
        self.type = self.ENUM_TYPES(self._getInt("armType", 0, data))
