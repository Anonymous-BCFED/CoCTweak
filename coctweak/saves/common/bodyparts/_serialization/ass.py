# @GENERATED from coc/vanilla/classes/classes/AssClass.as
from coctweak.saves.common.bodyparts.bodypart import BodyPart

__ALL__=['RawAss']

class RawAss(BodyPart):
    def __init__(self, save) -> None:
        super().__init__(save)
        self.wetness: float = 0.0
        self.looseness: float = 0.0
        self.fullness: float = 0.0
        self.rating: int = 0
    def serialize(self) -> dict:
        data = super().serialize()
        data["analWetness"] = self.wetness
        data["analLooseness"] = self.looseness
        data["fullness"] = self.fullness
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
        data["buttRating"] = self.rating
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.wetness = self._getFloat("analWetness", 0.0)
        self.looseness = self._getFloat("analLooseness", 0.0)
        self.fullness = self._getFloat("fullness", 0.0)
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
        self.rating = self._getInt("buttRating", 0, data)
