# @GENERATED 
from typing import Type
from coctweak.saves.common.bodyparts.bodypart import BodyPart
from enum import IntEnum

__ALL__=['RawWings']

class RawWings(BodyPart):
    ENUM_TYPES: Type[IntEnum] = None
    def __init__(self, save) -> None:
        super().__init__(save)
        self.type: IntEnum = self.ENUM_TYPES(0)
        self.color1: str = 'no'
        self.color2: str = 'no'
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
        data["wingType"] = self.type.value
        data["wingColor"] = self.color1
        data["wingColor2"] = self.color2
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
        self.type = self.ENUM_TYPES(self._getInt("wingType", 0, data))
        self.color1 = self._getStr("wingColor", 'no', data)
        self.color2 = self._getStr("wingColor2", 'no', data)
