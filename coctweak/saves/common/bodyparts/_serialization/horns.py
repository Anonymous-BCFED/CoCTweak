# @GENERATED 
from typing import Type
from coctweak.saves.common.bodyparts.bodypart import BodyPart
from enum import IntEnum

__ALL__=['RawHorns']

class RawHorns(BodyPart):
    ENUM_TYPES: Type[IntEnum] = None
    def __init__(self, save) -> None:
        super().__init__(save)
        self.count: int = 0
        self.type: IntEnum = self.ENUM_TYPES(0)
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
        data["horns"] = self.count
        data["hornType"] = self.type.value
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
        self.count = self._getInt("horns", 0, data)
        self.type = self.ENUM_TYPES(self._getInt("hornType", 0, data))
