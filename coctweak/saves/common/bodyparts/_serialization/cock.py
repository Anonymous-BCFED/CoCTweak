# @GENERATED from coc/vanilla/classes/classes/Cock.as
from typing import Type
from typing import Type
from coctweak.saves.common.bodyparts.bodypart import BodyPart
from enum import IntEnum, Enum

__ALL__=['RawCock']

class RawCock(BodyPart):
    COCK_TYPES: Type[IntEnum] = None
    SOCK_TYPES: Type[Enum] = None
    def __init__(self, save) -> None:
        super().__init__(save)
        self.length: float = 0.0
        self.thickness: float = 0.0
        self.type: IntEnum = self.COCK_TYPES(0)
        self.knotMultiplier: float = 0.0
        #self.isPierced: bool = False
        #self.pierced: float = 0.0
        #self.pierceType: Any = 'None'
        #self.pShortDesc: str = ''
        #self.pLongDesc: str = ''
        self.sock: Enum = self.SOCK_TYPES('')
    def serialize(self) -> dict:
        data = super().serialize()
        data["cockLength"] = self.length
        data["cockThickness"] = self.thickness
        data["cockType"] = self.type.value
        data["knotMultiplier"] = self.knotMultiplier
        #data["_isPierced"] = self.isPierced
        #data["_pierced"] = self.pierced
        #data["_pierceType"] = self.pierceType
        #data["_pShortDesc"] = self.pShortDesc
        #data["_pLongDesc"] = self.pLongDesc
        data["sock"] = self.sock.value
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.length = self._getFloat("cockLength", 0.0)
        self.thickness = self._getFloat("cockThickness", 0.0)
        self.type = self.COCK_TYPES(self._getInt("cockType", 0))
        self.knotMultiplier = self._getFloat("knotMultiplier", 0.0)
        #self.isPierced = self._getBool("_isPierced", False)
        #self.pierced = self._getFloat("_pierced", 0.0)
        #self.pierceType = data.get("_pierceType", 'None')
        #self.pShortDesc = self._getStr("_pShortDesc", '')
        #self.pLongDesc = self._getStr("_pLongDesc", '')
        self.sock = self.SOCK_TYPES(data.get("sock", ''))
