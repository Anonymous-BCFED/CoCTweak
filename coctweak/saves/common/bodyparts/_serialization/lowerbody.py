# @GENERATED 
from typing import Type
from coctweak.saves.common.bodyparts.bodypart import BodyPart
from enum import IntEnum

__ALL__=['RawLowerBody']

class RawLowerBody(BodyPart):
    ENUM_TYPES: Type[IntEnum] = None
    def __init__(self, save) -> None:
        super().__init__(save)
        self.type: IntEnum = self.ENUM_TYPES(0)
        self.count: int = 2
        self.incorporeal: bool = False
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
        data["lowerBody"] = self.type.value
        data["legCount"] = self.count
        data["incorporeal"] = self.incorporeal
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
        self.type = self.ENUM_TYPES(self._getInt("lowerBody", 0, data))
        self.count = self._getInt("legCount", 2, data)
        self.incorporeal = self._getBool("incorporeal", False, data)
