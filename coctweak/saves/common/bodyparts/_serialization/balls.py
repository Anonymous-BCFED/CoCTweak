# @GENERATED 
from coctweak.saves.common.bodyparts.bodypart import BodyPart

__ALL__=['RawBalls']

class RawBalls(BodyPart):
    def __init__(self, save) -> None:
        super().__init__(save)
        self.count: int = 0
        self.size: int = 0
        self.cumMultiplier: float = 0.0
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def serializeTo(self, data: dict) -> None:
        super().serializeTo(data)
        data["balls"] = self.count
        data["ballSize"] = self.size
        data["cumMultiplier"] = self.cumMultiplier
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
    def deserializeFrom(self, data: dict) -> None:
        super().deserializeFrom(data)
        self.count = self._getInt("balls", 0, data)
        self.size = self._getInt("ballSize", 0, data)
        self.cumMultiplier = self._getFloat("cumMultiplier", 0.0, data)
