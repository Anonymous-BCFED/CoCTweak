# @GENERATED from coc/vanilla/classes/classes/VaginaClass.as
from typing import Type
from typing import Type
from typing import Type
from coctweak.saves.common.bodyparts.bodypart import BodyPart
from enum import IntEnum

__ALL__=['RawVagina']

class RawVagina(BodyPart):
    ENUM_WETNESS: Type[IntEnum] = None
    ENUM_LOOSENESS: Type[IntEnum] = None
    ENUM_TYPES: Type[IntEnum] = None
    def __init__(self, save) -> None:
        super().__init__(save)
        self.wetness: IntEnum = self.ENUM_WETNESS(0)
        self.looseness: IntEnum = self.ENUM_LOOSENESS(0)
        self.type: IntEnum = self.ENUM_TYPES(0)
        self.virgin: bool = False
        self.fullness: float = 0.0
        #self.labiaPierced: float = 0.0
        #self.labiaPShort: str = ''
        #self.labiaPLong: str = ''
        #self.clitPierced: float = 0.0
        #self.clitPShort: str = ''
        #self.clitPLong: str = ''
    def serialize(self) -> dict:
        data = super().serialize()
        data["vaginalWetness"] = self.wetness.value
        data["vaginalLooseness"] = self.looseness.value
        data["vaginaType"] = self.type.value
        data["virgin"] = self.virgin
        data["fullness"] = self.fullness
        #data["labiaPierced"] = self.labiaPierced
        #data["labiaPShort"] = self.labiaPShort
        #data["labiaPLong"] = self.labiaPLong
        #data["clitPierced"] = self.clitPierced
        #data["clitPShort"] = self.clitPShort
        #data["clitPLong"] = self.clitPLong
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.wetness = self.ENUM_WETNESS(self._getInt("vaginalWetness", 0))
        self.looseness = self.ENUM_LOOSENESS(self._getInt("vaginalLooseness", 0))
        self.type = self.ENUM_TYPES(self._getInt("vaginaType", 0))
        self.virgin = self._getBool("virgin", False)
        self.fullness = self._getFloat("fullness", 0.0)
        #self.labiaPierced = self._getFloat("labiaPierced", 0.0)
        #self.labiaPShort = self._getStr("labiaPShort", '')
        #self.labiaPLong = self._getStr("labiaPLong", '')
        #self.clitPierced = self._getFloat("clitPierced", 0.0)
        #self.clitPShort = self._getStr("clitPShort", '')
        #self.clitPLong = self._getStr("clitPLong", '')
