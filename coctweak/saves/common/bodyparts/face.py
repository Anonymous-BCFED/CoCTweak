from collections import OrderedDict
from enum import IntEnum
from typing import Dict, Type

from coctweak.saves._saveobj import SaveObject
from coctweak.saves.common.bodyparts._serialization.face import RawFace
from coctweak.utils import display_enum

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class BaseFace(RawFace):
    CATEGORY = 'Face'
    ENUM_TYPES: Type[IntEnum] = None

    def __init__(self, save):
        super().__init__(save)
        self.type: IntEnum = self.ENUM_TYPES(0)

    def getShortDesc(self) -> None:
        return self.getTypeStr()

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Type'] = display_enum(self.type)
        return o

    def getTypeStr(self) -> str:
        if self.ENUM_TYPES is not None:
            return self.ENUM_TYPES(self.type).name
        else:
            return f'type {self.type:g}'
