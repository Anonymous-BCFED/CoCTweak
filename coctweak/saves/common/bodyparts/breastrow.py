from typing import Dict, List
from math import floor
from coctweak.saves._saveobj import SaveObject
from coctweak.saves.common.bodyparts._serialization.breastrow import RawBreastRow  # NOQA
from coctweak.utils import display_bool, display_enum, display_length

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class BaseBreastRow(RawBreastRow):
    CATEGORY = 'Breast Rows'
    CUP_NAMES: List[str] = [
        "flat", # 0
        "A-cup", "B-cup", "C-cup", "D-cup", "DD-cup", "big DD-cup", "E-cup", "big E-cup", "EE-cup",# 1-9
        "big EE-cup", "F-cup", "big F-cup", "FF-cup", "big FF-cup", "G-cup", "big G-cup", "GG-cup", "big GG-cup", "H-cup",#10-19
        "big H-cup", "HH-cup", "big HH-cup", "HHH-cup", "I-cup", "big I-cup", "II-cup", "big II-cup", "J-cup", "big J-cup",#20-29
        "JJ-cup", "big JJ-cup", "K-cup", "big K-cup", "KK-cup", "big KK-cup", "L-cup", "big L-cup", "LL-cup", "big LL-cup",#30-39
        "M-cup", "big M-cup", "MM-cup", "big MM-cup", "MMM-cup", "large MMM-cup", "N-cup", "large N-cup", "NN-cup", "large NN-cup",#40-49
        "O-cup", "large O-cup", "OO-cup", "large OO-cup", "P-cup", "large P-cup", "PP-cup", "large PP-cup", "Q-cup", "large Q-cup",#50-59
        "QQ-cup", "large QQ-cup", "R-cup", "large R-cup", "RR-cup", "large RR-cup", "S-cup", "large S-cup", "SS-cup", "large SS-cup",#60-69
        "T-cup", "large T-cup", "TT-cup", "large TT-cup", "U-cup", "large U-cup", "UU-cup", "large UU-cup", "V-cup", "large V-cup",#70-79
        "VV-cup", "large VV-cup", "W-cup", "large W-cup", "WW-cup", "large WW-cup", "X-cup", "large X-cup", "XX-cup", "large XX-cup",#80-89
        "Y-cup", "large Y-cup", "YY-cup", "large YY-cup", "Z-cup", "large Z-cup", "ZZ-cup", "large ZZ-cup", "ZZZ-cup", "large ZZZ-cup",#90-99
        #HYPER ZONE
        "hyper A-cup", "hyper B-cup", "hyper C-cup", "hyper D-cup", "hyper DD-cup", "hyper big DD-cup", "hyper E-cup", "hyper big E-cup", "hyper EE-cup",#100-109
        "hyper big EE-cup", "hyper F-cup", "hyper big F-cup", "hyper FF-cup", "hyper big FF-cup", "hyper G-cup", "hyper big G-cup", "hyper GG-cup", "hyper big GG-cup", "hyper H-cup",#110-119
        "hyper big H-cup", "hyper HH-cup", "hyper big HH-cup", "hyper HHH-cup", "hyper I-cup", "hyper big I-cup", "hyper II-cup", "hyper big II-cup", "hyper J-cup", "hyper big J-cup",#120-129
        "hyper JJ-cup", "hyper big JJ-cup", "hyper K-cup", "hyper big K-cup", "hyper KK-cup", "hyper big KK-cup", "hyper L-cup", "hyper big L-cup", "hyper LL-cup", "hyper big LL-cup",#130-139
        "hyper M-cup", "hyper big M-cup", "hyper MM-cup", "hyper big MM-cup", "hyper MMM-cup", "hyper large MMM-cup", "hyper N-cup", "hyper large N-cup", "hyper NN-cup", "hyper large NN-cup",#140-149
        "hyper O-cup", "hyper large O-cup", "hyper OO-cup", "hyper large OO-cup", "hyper P-cup", "hyper large P-cup", "hyper PP-cup", "hyper large PP-cup", "hyper Q-cup", "hyper large Q-cup",#150-159
        "hyper QQ-cup", "hyper large QQ-cup", "hyper R-cup", "hyper large R-cup", "hyper RR-cup", "hyper large RR-cup", "hyper S-cup", "hyper large S-cup", "hyper SS-cup", "hyper large SS-cup",#160-169
        "hyper T-cup", "hyper large T-cup", "hyper TT-cup", "hyper large TT-cup", "hyper U-cup", "hyper large U-cup", "hyper UU-cup", "hyper large UU-cup", "hyper V-cup", "hyper large V-cup",#170-179
        "hyper VV-cup", "hyper large VV-cup", "hyper W-cup", "hyper large W-cup", "hyper WW-cup", "hyper large WW-cup", "hyper X-cup", "hyper large X-cup", "hyper XX-cup", "hyper large XX-cup",#180-189
        "hyper Y-cup", "hyper large Y-cup", "hyper YY-cup", "hyper large YY-cup", "hyper Z-cup", "hyper large Z-cup", "hyper ZZ-cup", "hyper large ZZ-cup", "hyper ZZZ-cup", "hyper large ZZZ-cup",#190-199
        "jacques00-cup"
    ]
    def getShortDesc(self) -> str:
        o = f'{self.breasts:g}x w/ {self.nipplesPerBreast:g} nips per breast'
        return o +'; '+', '.join([f'{k}: {v}' for k,v in self.getVars(terse=True).items()])

    def getVars(self, terse: bool = False) -> Dict[str, str]:
        o = super().getVars()
        if not terse:
            o['Breast Count'] = f"{self.breasts:g}"
            o['Nipples per Breast'] = f'{self.nipplesPerBreast:g}'
        o['Rating'] = f'{self.breastRating:g}'
        if not terse or self.fullness > 0:
            o['Fullness'] = f'{self.fullness:g}'
        if not terse or self.milkFullness > 0.0:
            o['Milk Fullness'] = f'{self.milkFullness:g}'
        if not terse or self.lactationMultiplier > 0:
            o['Lactation Mult'] = f'{self.lactationMultiplier:g}'
        if not terse or self.fuckable:
            o['Fuckable'] = display_bool(self.fuckable)
        return o

    def cloneFrom(self, other: 'BaseBreastRow') -> None:
        self.deserialize(other.serialize())

    def flatten(self, verbose: bool = False) -> None:
        self.setProps({
            'breastRating': 0,
        }, quiet=not verbose)

    def fixForMen(self, verbose: bool = False) -> None:
        self.flatten()
        self.setProps({
            'lactationMultiplier': 0,
            'fuckable': False,
            #'fullness': 0.,
            'milkFullness': 0.,
        }, quiet=not verbose)

    def reset(self, verbose: bool = False) -> None:
        self.setProps({
            'breastRating': 0,
            'breasts': 2,
            'fuckable': False,
            'fullness': 0,
            'lactationMultiplier': 0,
            'milkFullness': 0,
            'nipplesPerBreast': 1,
        }, quiet=not verbose)

    def getCupSize(self) -> str:
        idx = min(floor(self.breastRating), len(self.CUP_NAMES)-1)
        return self.CUP_NAMES[idx]