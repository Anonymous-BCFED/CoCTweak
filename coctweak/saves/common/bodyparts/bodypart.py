from collections import OrderedDict
from typing import Any, Dict, Optional

from coctweak.saves._saveobj import SaveObject

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class BodyPart(SaveObject):
    CATEGORY: Optional[str] = None
    HAS_HELPERS: bool = False

    def __init__(self, save):
        super().__init__()
        self.save = save

    def __str__(self) -> str:
        return self.getShortDesc()

    def _dict2Log(self, data: Dict[str, Any]) -> None:
        for k, v in data.items():
            if isinstance(v, (dict, OrderedDict)):
                with log.info(f'{k}:'):
                    self._dict2Log(v)
            else:
                log.info(f'{k}: {v}')

    def display(self) -> None:
        log.info(self.getShortDesc())
        if (txt := self.getLongDesc()) != 'Not Implemented':
            with log.info('Description:'):
                log.info(txt)
        with log.info('Variables:'):
            self._dict2Log(self.getVars())
        if self.HAS_HELPERS:
            with log.info('Helpers:'):
                self._dict2Log(self.getHelpers())

    def getShortDesc(self) -> str:
        '''
        A short, helpful description.  Generally written specifically by and for coctweak.
        '''
        return 'Not Implemented'

    def getLongDesc(self) -> str:
        '''
        A long description meant to duplicate the game's Character.*Desc method as closely as possible.
        '''
        return 'Not Implemented'

    def getVars(self) -> Dict[str, str]:
        '''
        A dict of [Variable Display Name => Display Value]
        '''
        return OrderedDict()

    def getHelpers(self) -> Dict[str, str]:
        '''
        A dict of any helper methods that calculate certain values, such as cock area.
        [Display Name => Display Value]
        '''
        return OrderedDict()

    def setProp(self, attr: str, newval: Any, label: str = None, quiet: bool = False) -> None:
        label = label or attr
        if not quiet:
            oldval = None if quiet else getattr(self, attr)
            log.info('%s: %r -> %r', label, oldval, newval)
        setattr(self, attr, newval)

    def setProps(self, props: Dict[str, Any], quiet: bool=False) -> None:
        for k,v in props.items():
            self.setProp(k, v)
