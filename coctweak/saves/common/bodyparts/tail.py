from enum import IntEnum
from typing import Dict, Type

from coctweak.saves._saveobj import SaveObject
from coctweak.saves.common.bodyparts._serialization.tail import RawTail
from coctweak.utils import display_enum

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class BaseTail(RawTail):
    CATEGORY = 'Tails'
    ENUM_TYPES: Type[IntEnum] = None

    def __init__(self, save):
        super().__init__(save)
        self.type: IntEnum = self.ENUM_TYPES(0)
        self.venom: int = 0
        self.recharge: int = 0

    def getShortDesc(self) -> str:
        if self.type == self.ENUM_TYPES.NONE:
            return 'no tail'
        return self.getTypeStr() + ' tail'

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Type'] = display_enum(self.type)
        o['Venom'] = f'{self.venom:g}'
        o['Recharge'] = f'{self.recharge:g}'
        return o

    def getTypeStr(self) -> str:
        if self.ENUM_TYPES is not None:
            return self.ENUM_TYPES(self.type).name
        else:
            return f'type {self.type:g}'
