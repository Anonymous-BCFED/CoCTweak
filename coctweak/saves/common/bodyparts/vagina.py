from enum import IntEnum
import sys
from typing import Dict, Optional, Type

from coctweak.saves._saveobj import SaveObject
from coctweak.saves.common.bodyparts._serialization.vagina import RawVagina
from coctweak.saves.common.piercing import BasePiercing
from coctweak.utils import display_bool, display_enum, display_length

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class BaseVagina(RawVagina):
    CATEGORY = 'Vaginas'
    ENUM_LOOSENESS: Type[IntEnum] = None
    ENUM_WETNESS: Type[IntEnum] = None
    ENUM_TYPES: Type[IntEnum] = None
    TYPE_PIERCING: Type[BasePiercing] = None

    MIN_CLIT_LENGTH: float = 1.
    MAX_CLIT_LENGTH: float = sys.float_info.max

    def __init__(self, save):
        super().__init__(save)
        self.clitPiercing: Optional[BasePiercing] = None
        self.labiaPiercing: Optional[BasePiercing] = None

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.clitPiercing = self.TYPE_PIERCING.SimpleLoadFrom(data, 'clit')
        self.labiaPiercing = self.TYPE_PIERCING.SimpleLoadFrom(data, 'labia')

    def serialize(self) -> dict:
        data = super().serialize()
        (self.clitPiercing if self.clitPiercing is not None else self.TYPE_PIERCING()).simpleSerializeTo(data, 'clit')
        (self.labiaPiercing if self.labiaPiercing is not None else self.TYPE_PIERCING()).simpleSerializeTo(data, 'labia')
        return data

    def getMinClitLength(self, save) -> float:
        return self.MIN_CLIT_LENGTH
    def getMaxClitLength(self, save) -> float:
        return self.MAX_CLIT_LENGTH

    def getMinWetness(self, save) -> int:
        return 0
    def getMaxWetness(self, save) -> int:
        return max(map(int, self.ENUM_WETNESS))

    def getMinLooseness(self, save) -> int:
        return 0
    def getMaxLooseness(self, save) -> int:
        return max(map(int, self.ENUM_LOOSENESS))

    def getLoosenessStr(self):
        if self.ENUM_LOOSENESS is not None:
            return self.ENUM_LOOSENESS(self.looseness).name
        else:
            return f'{self.looseness:g} looseness'

    def getWetnessStr(self):
        if self.ENUM_WETNESS is not None:
            return self.ENUM_WETNESS(self.wetness).name
        else:
            return f'{self.wetness:g} wetness'


    def getShortDesc(self) -> str:
        desc = [
            self.getLoosenessStr(),
            self.getWetnessStr()
        ]
        subsentences = []
        if self.virgin:
            desc += ['virgin']
        desc = [', '.join(desc) + ' vagina']
        if self.fullness > 0.0:
            desc += [f'with fullness={self.fullness}']
        if self.labiaPiercing is not None:
            subsentences += [desc+[f'labia pierced with {self.labiaPiercing}']]
            desc = []
        desc += [f'with a clit {display_length(self.clitLength)} in length']
        if self.clitPiercing is not None:
            desc += [f'pierced with {self.clitPiercing}']
        subsentences += [desc]
        return '; '.join([', '.join(desc) for desc in subsentences])

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Type'] = display_enum(self.type)
        o['Fullness'] = f'{self.fullness:g}'
        o['Looseness'] = display_enum(self.looseness)
        o['Virgin'] = display_bool(self.virgin)
        o['Wetness'] = display_enum(self.wetness)
        return o
