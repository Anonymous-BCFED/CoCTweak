from collections import OrderedDict
from enum import auto, Enum, IntEnum, IntFlag
from typing import Dict, List, Optional, Type

from coctweak.saves._saveobj import SaveObject
from coctweak.saves.common.bodyparts._serialization.cock import RawCock
from coctweak.saves.common.piercing import BasePiercing
from coctweak.utils import display_bool, display_enum, display_length, in2feetinches  # NOQA

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

class ECockTypeFlags(IntFlag):
    HAS_KNOT = auto()
    HAS_SHEATH = auto()

class BaseCock(RawCock):
    CATEGORY = 'Cocks'
    HAS_HELPERS = True

    TYPE_PIERCING: Type[BasePiercing] = None
    COCK_TYPES: Type[IntEnum] = None
    COCK_SOCK_TYPES: Type[Enum] = None
    COCK_TYPE_FLAGS: Dict[IntEnum, ECockTypeFlags] = {}

    MIN_LENGTH: float = 0.01
    MAX_LENGTH: float = 9999.

    MIN_THICKNESS: float = 0.01
    MAX_THICKNESS: float = 999.

    MIN_KNOT_MULTIPLIER: float = 1.
    MAX_KNOT_MULTIPLIER: float = 9999.

    def __init__(self, save):
        super().__init__(save)
        self.piercing: Optional[BasePiercing] = None

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.piercing = self.TYPE_PIERCING.SimpleLoadFrom(data, None)

    def serialize(self) -> dict:
        data = super().serialize()
        if self.piercing is None:
            self.piercing = self.TYPE_PIERCING()
        self.piercing.simpleSerializeTo(data, None)
        return data

    def getTypeStr(self) -> str:
        return self.type.name

    def getSockStr(self) -> str:
        return self.type.name

    def hasSock(self) -> str:
        # Internal
        return self.sock != self.SOCK_TYPES.NONE

    def getTypeFlags(self) -> ECockTypeFlags:
        return self.COCK_TYPE_FLAGS.get(self.type, ECockTypeFlags(0))

    def hasTypeFlag(self, flag: ECockTypeFlags):
        return (self.getTypeFlags() & flag) == flag

    def hasKnot(self) -> bool:
        return False

    def hasSheath(self) -> bool:
        return False

    def setCockType(self, newType: IntEnum) -> None:
        '''
        Use for user-input shit.
        '''
        self.type = newType

    def getMinLength(self, save) -> float:
        return self.MIN_LENGTH
    def getMaxLength(self, save) -> float:
        return self.MAX_LENGTH

    def getMinThickness(self, save) -> float:
        return self.MIN_THICKNESS
    def getMaxThickness(self, save) -> float:
        return self.MAX_THICKNESS

    def getMinKnotMultiplier(self, save) -> float:
        return self.MIN_KNOT_MULTIPLIER
    def getMaxKnotMultiplier(self, save) -> float:
        return self.MAX_KNOT_MULTIPLIER

    def getKnotDiameter(self) -> float:
        return 0

    def getArea(self) -> float:
        ## from [HGG]/classes/classes/Cock.as: public function cArea():Number { @ bxn8eBfq36ikgkW9VW1qj1PfeoufOT19J1pL4Lgbuh0zt3vme6T0Kf2664Iz8Obaha2Hz6pb8698sX1Y341If9K3Ch0xTbcR
        return self.length * self.thickness

    def getShortDesc(self) -> str:
        desc = [f'{display_length(self.length)}L x {display_length(self.thickness)}W ({self.getArea():g}in² area) {self.getCockTypeStr()} cock']
        with_: str = 'with '
        if self.hasKnot():
            desc += [f'{with_} a knot {display_length(self.getKnotDiameter())} wide']
            with_ = ''
        if self.hasSock():
            desc += [f'{with_} a {self.sock.name} sock']
            with_ = ''
        if self.piercing is not None:
            desc += [f'{with_} a {self.piercing} piercing']
        return ', '.join(desc)

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Type'] = display_enum(self.type)
        o['Length'] = f'{self.length:g}"'
        o['Thickness'] = f'{self.thickness:g}"'
        o['Knot Multiplier'] = f'{self.knotMultiplier:g}"'
        o['Sock'] = display_enum(self.sock)
        if self.piercing is not None:
            o['Piercing'] = self.piercing.getVars()
        return o

    def getHelpers(self) -> Dict[str, str]:
        o = super().getVars()
        tfs: List[str] = []
        flags: ECockTypeFlags = self.getTypeFlags()
        for v in ECockTypeFlags:
            if (flags & v) == v:
                tfs.append(display_enum(v))
        o['Type Flags'] = ', '.join(tfs)
        o['Has Knot'] = display_bool(self.hasKnot())
        o['Has Sheath'] = display_bool(self.hasSheath())
        o['Has Sock'] = display_bool(self.hasSock())
        o['Knot Diameter'] = f'{self.getKnotDiameter():g}"'
        o['Area'] = f'{self.getArea():g}"'
        return o
