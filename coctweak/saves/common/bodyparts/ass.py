from typing import Dict, Type

from coctweak.saves.common.bodyparts._serialization.ass import RawAss
from coctweak.utils import display_bool, display_enum, display_length

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

from enum import Enum, IntEnum


class BaseAss(RawAss):
    CATEGORY = 'Asses'
    HAS_HELPERS = True

    ENUM_LOOSENESS: Type[Enum] = None
    ENUM_WETNESS: Type[Enum] = None
    ENUM_RATINGS: Type[Enum] = None

    def getLoosenessStr(self):
        if self.ENUM_LOOSENESS is not None:
            return self.ENUM_LOOSENESS(self.looseness).name
        else:
            return f'{self.looseness:g} looseness'

    def getWetnessStr(self):
        if self.ENUM_WETNESS is not None:
            return self.ENUM_WETNESS(self.wetness).name
        else:
            return f'{self.wetness:g} wetness'

    def getRatingFromVal(self) -> IntEnum:
        last = -1
        for e in self.ENUM_RATINGS:
            if self.rating <= e.value and self.rating > last:
                return e
            last = e.value
        return self.ENUM_RATINGS.BUTTLESS

    def getRatingStr(self, incl_value: bool = False):
        if self.ENUM_RATINGS is not None:
            e = self.getRatingFromVal()
            return f'{e.name} ({self.rating})' if incl_value else e.name
        else:
            return f'{self.rating:g} rating'

    def getVars(self) -> Dict[str, str]:
        o = super().getVars()
        o['Looseness'] = f'{self.looseness:g} ({self.getLoosenessStr()})'
        o['Rating'] = f'{self.rating:g} ({self.getRatingStr()})'
        o['Wetness'] = f'{self.wetness:g} ({self.getWetnessStr()})'
        o['Fullness'] = f'{self.fullness:g}'
        return o

    def getHelpers(self) -> Dict[str, str]:
        o = super().getHelpers()
        o['Anal Capacity'] = self.analCapacity()
        return o

    def getShortDesc(self) -> str:
        desc = [
            self.getLoosenessStr(),
            self.getWetnessStr(),
            self.getRatingStr(),
        ]
        desc = [', '.join(desc) + ' ass']
        if self.fullness > 0.0:
            desc += [f'with fullness={self.fullness}']
        return ', '.join(desc)

    def analCapacity(self) -> float:
        print(self.__class__.__name__)
        raise NotImplementedError()
