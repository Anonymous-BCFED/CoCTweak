from coctweak.saves._perk import Perk

class SmartPerk(Perk):
    def displayValues(self):
        return f'Bonus: {round(self.values[0]*100)}%'
