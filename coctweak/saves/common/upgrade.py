from typing import Optional, List, Dict, Any

from coctweak.logsys import getLogger

log = getLogger(__name__)

class Upgrade(object):
    '''
    An object that can upgrade a save, or part of a save, to a newer version.
    '''
    def __init__(self, name: str, priority: int = 0):
        self.name: str = name
        self.priority: int = priority

    def getName(self) -> str:
        return self.name

    def matches(self, data: dict) -> bool:
        return False

    def attemptFix(self, data: dict) -> bool:
        return False

    def onOK(self, data: dict) -> None:
        pass

    def onFailed(self, data: dict) -> None:
        pass

class VersionedUpgrade(Upgrade):
    NAME = 'Untitled'
    VERSION_START = 0
    VERSION_END = 0
    PRIORITY = 0

    def __init__(self):
        super().__init__(f'{self.NAME} ({self.VERSION_START} -> {self.VERSION_END})', self.PRIORITY)

    def getVersionIn(self, data: dict) -> Any:
        return data['version']
    def setVersionIn(self, data: dict, newVersion: Any) -> None:
        data['version'] = newVersion

    def matches(self, data: dict) -> bool:
        return self.getVersionIn(data) == self.VERSION_START

    def onOK(self, data: dict) -> None:
        self.setVersionIn(data, self.VERSION_END)

class UpgradeCollection(object):
    def __init__(self, subjName: str, upgrades: List[Upgrade]):
        self.subjectName: str = subjName
        self.all: List[Upgrade] = upgrades

    def execute(self, data: dict) -> bool:
        changes: int = 0
        sortedUpgrades = sorted(self.all, key=lambda x: x.priority)
        while True:
            changes = 0
            for upgrade in sortedUpgrades:
                if upgrade.matches(data):
                    with log.info('Running upgrade %s on %s...', upgrade.getName(), self.subjectName):
                        if upgrade.attemptFix(data):
                            log.info('OK')
                            self.onOK(data)
                            changes += 1
                        else:
                            log.critical('FAILED')
                            self.onFailed(data)
                            return False
            if changes == 0:
                break
        return True
