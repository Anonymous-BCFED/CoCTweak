import argparse
from enum import Enum, Flag
from typing import Any, Dict, List, Optional, Tuple, Type, Union

from colorama import Fore, Style
import tabulate

from coctweak.consts import TABULATE_TABLEFMT
from coctweak.logsys import getLogger
from coctweak.utils import ERoundingType, parsePropertyAdjustment

from ._breaststore import BaseSingleFlagBreastStore
from ._selfsaving import SelfSavingObject
from .pregnancystore import BasePregnancyStore

log = getLogger(__name__)

class NPCProperty(object):
    PREFIX = ''
    def __init__(self, save, name: str, type: Type, default: Any, **kwargs) -> None:
        super().__init__()
        self.save = save
        self.name: str = name
        self.type: Optional[Type] = type
        self.default: Any = default

        self.readonly: bool = kwargs.get('readonly', False)
        self.unused: bool = kwargs.get('unused', False)
        self.enumType: Optional[str] = kwargs.get('enumType', None)
        self.choices: Optional[Dict[Any, str]] = kwargs.get('choices', None)
        self.bitflags: Optional[Dict[int, str]] = kwargs.get('bitflags', None)
        self.min: Optional[Union[int, float]] = kwargs.get('min', None)
        self.max: Optional[Union[int, float]] = kwargs.get('max', None)
        self.notes: Optional[str] = kwargs.get('notes', None)
        self.intbool: bool = kwargs.get('intbool', False)

    @property
    def value(self) -> Any:
        return None

    @value.setter
    def value(self, value: Any) -> None:
        return

    @property
    def has_value(self) -> bool:
        return True

    def choices_to_str(self) -> str:
        o = []
        if isinstance(self.choices, dict):
            for k,v in self.choices.items():
                if isinstance(k, list):
                    k = '-'.join(k)
                o += [f'{k}={v}']
        if isinstance(self.choices, list):
            for v in self.choices:
                o += [str(v)]
        return ', '.join(o)

    def set(self, value, verbose: bool = False) -> None:
        if verbose:
            oldvalue = repr(self.value) if self.has_value else '(not set)'
            print(f'[{self.PREFIX}] {self.name}: {oldvalue} -> {value!r}')
        self.value = value

    def reset(self) -> None:
        print(f'Resetting {self.PREFIX} {self.name}: {self.value} -> {self.default}')
        self.value = self.default

    def display(self) -> None:
        print(f'TODO')

    def display_row(self) -> List[str]:
        value = repr(self.value)
        default = repr(self.default)
        color: str = None
        notes = [self.notes] if self.notes else []

        if self.has_value:
            if value != default:
                color = Fore.YELLOW
            else:
                color = Fore.GREEN
            color += Style.BRIGHT
        else:
            value = '(not set)'
            color = '\u001b[38;5;28m'
        if self.unused:
            notes += [Style.BRIGHT+'(UNUSED)'+Style.RESET_ALL]
            color = Fore.WHITE
        if self.intbool:
            value = 'True (1)' if self.value else 'False (0)'
            notes = ['(INTBOOL)'] + notes
        if self.readonly:
            notes = ['(READ-ONLY)'] + notes
        if self.choices is not None:
            notes += ['Choices: ' + self.choices_to_str()]
        if self.bitflags is not None:
            bf = [v for k, v in self.bitflags.items() if (k & self.value) > 0]
            notes += ['Bitflags: ' + (' + '.join(bf) if len(bf) > 0 else '(None)')]
        value = color + value + Style.RESET_ALL
        name = color + self.name + Style.RESET_ALL
        return [self.PREFIX, name, value, default, ' '.join(notes)]

class NPCKFlagProperty(NPCProperty):
    PREFIX = 'KFLAG'
    def __init__(self, save, name: str, type: Type, default: Any, kflag, **kwargs) -> None:
        super().__init__(save, name, type, default, **kwargs)
        self.kflag = kflag

    @property
    def value(self) -> Any:
        v =  self.save.flags.get(self.kflag, self.default)
        if self.enumType is not None:
            v = self.enumType(v)
        if self.type in (bool, int, float):
            if isinstance(v, (Enum, Flag)):
                v = v.value
            v = self.type(v)
        if self.intbool:
            v = bool(v)
        return v

    @value.setter
    def value(self, value: Any) -> None:
        if self.readonly:
            return
        if isinstance(value, (Enum, Flag)):
            value = value.value
        if self.intbool:
            value = 1 if bool(value) else 0
        self.save.flags.set(self.kflag, value)

    @property
    def has_value(self) -> bool:
        return self.save.flags.has(self.kflag)

    def display(self) -> None:
        value = repr(self.value) if self.has_value else '[not set]'
        print(f'  [{self.PREFIX}] {self.name}: {value} (default: {self.default!r})')

class NPCSelfSaveProperty(NPCProperty):
    PREFIX = 'SSO'
    def __init__(self, save, name: str, type: Type, default: Any, objid: str, attr: str, **kwargs) -> None:
        super().__init__(save, name, type, default, **kwargs)
        self.objID: str = objid
        self.attr: str = attr

    @property
    def value(self) -> Any:
        sso: SelfSavingObject = self.save.selfSavingObjects.get(self.objID)
        if sso is None:
            return None
        v = getattr(sso, self.attr)
        if self.enumType is not None:
            v = self.enumType(v)
        if self.type in (bool, int, float):
            v = self.type(v)
        if self.intbool:
            v = bool(v)
        return v

    @value.setter
    def value(self, value: Any) -> None:
        if self.readonly:
            return
        if isinstance(value, (Enum, Flag)):
            value = value.value
        sso: SelfSavingObject = self.save.selfSavingObjects.get(self.objID)
        if sso is None:
            return
        if self.intbool:
            value = 1 if bool(value) else 0
        setattr(sso, self.attr, value)

    def display(self) -> None:
        print(f'  [{self.PREFIX}] {self.name} ({self.objID}.{self.attr}): {self.value!r} (default: {self.default!r})')

class NPCStatusEffectProperty(NPCProperty):
    PREFIX = 'SFX'
    def __init__(self, save, name: str, type: Type, default: Any, sfxid: str, value_or_index: Optional[Union[str, int]], **kwargs) -> None:
        self.sfxid: str = sfxid
        super().__init__(save, name, type, default, **kwargs)
        self.val_or_idx: Optional[Union[str, int]] = value_or_index
        self._check_exists_only: bool = self.val_or_idx is None
        self._is_older_style: bool = isinstance(self.val_or_idx, int)

        assert self.genID()==name, f'{self.name} - Status Effect name set improperly, should be {self.genID()}!'

    @property
    def check_exists_only(self) -> bool:
        return self._check_exists_only
    @property
    def is_older_style(self) -> bool:
        return self._is_older_style

    @property
    def value(self) -> Any:
        if self.check_exists_only:
            return self.save.hasStatusEffect(self.sfxid)
        sfx = self.save.getStatusEffect(self.sfxid)
        if sfx is None:
            return self.default
        v = None
        if self._is_older_style:
            v = sfx.values[self.val_or_idx]
        else:
            v = sfx.dataStore[self.val_or_idx]
        if self.enumType is not None:
            v = self.enumType(v)
        if self.type in (bool, int, float):
            v = self.type(v)
        if self.intbool:
            value = 1 if bool(value) else 0
        return v

    @value.setter
    def value(self, value: Any) -> None:
        if self.readonly:
            return
        if self.check_exists_only:
            if value:
                self.save.addStatusEffect(self.sfxid, skip_meta=True)
            else:
                del self.save.statusEffects[self.sfxid]
            return
        if isinstance(value, (Enum, Flag)):
            value = value.value
        sfx = self.save.getStatusEffect(self.sfxid)
        if self.intbool:
            value = 1 if bool(value) else 0
        if sfx is None:
            if value is not None:
                sfx = self.save.addStatusEffect(self.sfxid, skip_meta=True)
        else:
            if value is None:
                del self.save.statusEffects[self.sfxid]
                return
        if self._is_older_style:
            sfx.values[self.val_or_idx]=value
        else:
            sfx.dataStore[self.val_or_idx]=value

    def genID(self) -> str:
        if self._check_exists_only:
            return self.sfxid.name
        return f'{self.sfxid.name}:{self.val_or_idx}'

    def display(self) -> None:
        sfxid = self.sfxid.name if isinstance(self.sfxid, Enum) else self.sfxid
        attr = f'values[{self.val_or_idx}]' if self._is_older_style else f'dataStore[{self.val_or_idx!r}]'
        print(f'  [{self.PREFIX}] {self.name} ({sfxid}.{attr}): {self.value!r} (default: {self.default!r})')

    def display_row(self) -> List[str]:
        value = repr(self.value)
        default = repr(self.default)
        color: str = None
        notes = [self.notes] if self.notes else []
        if self.has_value:
            if value != default:
                color = Fore.YELLOW
            else:
                color = Fore.GREEN
            color += Style.BRIGHT
        else:
            value = '(not set)'
            color = '\u001b[38;5;28m'
        if self.unused:
            notes += [Fore.LIGHTBLACK_EX+'(UNUSED)'+Style.RESET_ALL]
            color = Fore.LIGHTBLACK_EX
            default = color + default + Style.RESET_ALL

        if self.check_exists_only:
            notes += ['Value depends on whether the Status Effect exists.']
        if self.choices is not None:
            notes += ['Choices: ' + self.choices_to_str()]
        if self.bitflags is not None:
            bf = [v for k, v in self.bitflags.items() if (k & self.value) > 0]
            notes += ['Bitflags: '+(' + '.join(bf) if len(bf) > 0 else '(None)')]
        value = color + value + Style.RESET_ALL
        name = color + self.name + Style.RESET_ALL
        return [self.PREFIX, name, value, default, ' '.join(notes)]

class BaseNPC(object):
    #: Unique ID of this NPC (use lowercase).
    ID: str = ''
    #: Display name for this NPC.
    NAME: str = ''
    #: Any ID aliases to use.
    ALIASES = []

    def __init__(self, save) -> None:
        #super().__init__()
        self.save = save
        self.properties: Dict[str, NPCProperty] = {}
        self.sso: SelfSavingObject = None

        self.analPregnancy: Optional[BasePregnancyStore] = None
        self.vaginalPregnancy: Optional[BasePregnancyStore] = None
        self.breasts: Optional[BaseSingleFlagBreastStore] = None

        self.verbose_set: bool = False

        self._cli_reg_cmds: Dict[str, Callable] = {}

        self.debugPropIDs=False

    # Called when __getattribute__ can't find anything.
    def __getattr__(self, name: str) -> Any:
        if 'properties' in self.__dict__.keys():
            props = self.__dict__['properties']
            if name in props.keys():
                return props[name].value
        super().__getattr__(name)
    def __setattr__(self, name: str, value: Any) -> None:
        if 'properties' in self.__dict__.keys():
            props = self.__dict__['properties']
            if name in props.keys():
                props[name].set(value, verbose=self.verbose_set)
                return
        super().__setattr__(name, value)

    def multiset(self, key_values: Dict[str, Any], verbose: bool=False) -> None:
        self.verbose_set = verbose
        for k, v in key_values.items():
            self.properties[k] = v
        self.verbose_set = False

    def addKFlag(self, name: str, type: Type, default: Any, kflag, **kwargs) -> None:
        p = NPCKFlagProperty(self.save, name, type, default, kflag, **kwargs)
        self.properties[p.name] = p
        return p

    def addSSOProperty(self, name: str, type: Type, default: Any, objid: str, attr: str, **kwargs) -> None:
        p = NPCSelfSaveProperty(self.save, name, type, default, objid, attr, **kwargs)
        self.properties[p.name] = p
        return p

    def addSFXProperty(self, name: str, type: Type, default: Any, sfxid: Union[str, Enum], idx_or_key: Optional[Union[int, str]], **kwargs) -> None:
        p = NPCStatusEffectProperty(self.save, name, type, default, sfxid, idx_or_key, **kwargs)
        if self.debugPropIDs:
            print(p.genID())
        self.properties[p.genID()] = p
        return p

    def register_cli(self, subp: argparse.ArgumentParser) -> None:
        cmd_attrs=[]
        for attr in dir(self):
            if attr.startswith('register_cli_'):
                cmd_attrs += [attr]
        cmd_attrs.sort()
        for attr in cmd_attrs:
            method = getattr(self, attr)
            method(subp)

    def register_cli_reset(self, subp: argparse.ArgumentParser) -> None:
        resetp = subp.add_parser('reset', help='Reset variable')
        resetp.add_argument('--all', action='store_true', default=False, help='Target all properties.')
        resetp.add_argument('properties', nargs='*', type=str, default=[], help='Names of properties to target.')
        resetp.set_defaults(npc_cmd=self.cmd_reset)

    def register_cli_show(self, subp: argparse.ArgumentParser) -> None:
        showp = subp.add_parser('show', help='Show the state of this NPC')
        showp.add_argument('properties', nargs='*', type=str, default=[], help='Names of properties to target. Defaults to all.')
        showp.set_defaults(npc_cmd=self.cmd_show)

    def register_cli_set(self, subp: argparse.ArgumentParser) -> None:
        setp = subp.add_parser('set', help='Set the state of this NPC')
        setp.add_argument('property', type=str, help='Which property to target. See `npc ... show` command.')
        setp.add_argument('type', choices=['str', 'int', 'float'], help='How to interpret the next argument')
        setp.add_argument('value', type=str, help='How to interpret the next argument')
        setp.set_defaults(npc_cmd=self.cmd_set)

    def register_cli_preg(self, subp: argparse.ArgumentParser) -> None:
        if self.analPregnancy is not None or self.vaginalPregnancy is not None:
            p_preg = subp.add_parser('preg', help='Configure pregnancy data.')
            p_preg.add_argument('oriface', choices=['butt', 'vag', 'all'], help="Which hole to target")
            preg_subp = p_preg.add_subparsers()
            clearp = preg_subp.add_parser('clear', help='Clear pregnancy. WILL NOT REVERSE SIDE-EFFECTS.')
            clearp.set_defaults(npc_cmd=self.cmd_preg_clear)

            showp = preg_subp.add_parser('show', help='Show all pregnancystore values.')
            showp.set_defaults(npc_cmd=self.cmd_preg_show)

            adjustp = preg_subp.add_parser('adjust', help='Set or adjust values of a PregnancyStore')
            adjustp.add_argument('--type', '-t', type=str, default=None, help="Pregnancy type. See the enum for PregnancyType for your mod.")
            adjustp.add_argument('--last-notice', '-n', type=str, default=None, help="Time that you last noticed something. You can use the adjustment sublanguage.")
            adjustp.add_argument('--incubation', '-i', type=str, default=None, help="Incubation time. You can use the adjustment sublanguage.")
            adjustp.set_defaults(npc_cmd=self.cmd_preg_adjust)

    def cmd_reset(self, args: argparse.Namespace) -> bool:
        properties: List[str] = args.properties or []
        if args.all:
            properties = self.get_properties()
        self.reset(properties)
        return True # Yes, save.

    def cmd_show(self, args: argparse.Namespace) -> bool:
        properties: List[str] = args.properties or []
        if len(properties) == 0:
            properties = None
        self.show(properties)
        return False # Saving not needed.

    def _psFromArgs(self, args: argparse.Namespace) -> List[Tuple[str, BasePregnancyStore]]:
        o: List[Tuple[str, BasePregnancyStore]] = []
        part = args.oriface
        if part in ('butt', 'all'):
            o += [('Anal', self.analPregnancy)]
        if part in ('vag', 'all'):
            o += [('Vaginal', self.vaginalPregnancy)]
        return o
    def cmd_preg_show(self, args: argparse.Namespace) -> bool:
        for label, store in self._psFromArgs(args):
            with log.info(f'{label} Pregnancy:'):
                if store is None:
                    log.info('Skipped (Not supported on this NPC.)')
                    continue
                store.display()

    def cmd_preg_clear(self, args: argparse.Namespace) -> bool:
        for label, store in self._psFromArgs(args):
            with log.info(f'{label} Pregnancy:'):
                if store is None:
                    log.info('Skipped (Not supported on this NPC.)')
                    continue
                store.reset()
                log.info('Cleared!')
        return True # Yes, save.

    def cmd_preg_adjust(self, args: argparse.Namespace) -> bool:
        for label, store in self._psFromArgs(args):
            with log.info(f'{label} Pregnancy:'):
                if store is None:
                    log.info('Skipped (Not supported on this NPC.)')
                    continue
                if args.type is not None:
                    if args.type.isdigit():
                        store.type = store.PREGTYPE_ENUM(int(args.type))
                    else:
                        store.type = store.PREGTYPE_ENUM[args.type]
                if args.last_notice is not None:
                    store.last_notice = parsePropertyAdjustment(store.last_notice, args.last_notice, 0, 65535, ERoundingType.ROUND)
                if args.incubation is not None:
                    store.incubation = parsePropertyAdjustment(store.incubation, args.incubation, 0, 65535, ERoundingType.ROUND)
                log.info('Done!')
        return True # Yes, save.

    def register_cli_breasts(self, subp: argparse.ArgumentParser) -> None:
        if self.breasts is not None:
            p_breasts = subp.add_parser('breasts', help='Configure pregnancy data.')
            breasts_subp = p_breasts.add_subparsers()

            resetp = breasts_subp.add_parser('reset', help='Reset breasts. WILL NOT REVERSE SIDE-EFFECTS.')
            resetp.set_defaults(npc_cmd=self.cmd_breasts_reset)

            # clearp = breasts_subp.add_parser('clear', help='Remove breasts.')
            # clearp.set_defaults(npc_cmd=self.cmd_breasts_clear)

            showp = breasts_subp.add_parser('show', help='Show all breaststore values.')
            showp.set_defaults(npc_cmd=self.cmd_breasts_show)

            adjustp = breasts_subp.add_parser('adjust', help='Set or adjust values of a BreastStore')
            adjustp.add_argument('--rows', '-r', type=str, default=None, help="Row count. You can use the adjustment sublanguage.")
            adjustp.add_argument('--cup-size', '-c', type=str, default=None, help="Cup size. See BreastCup.as for your mod. You can use the adjustment sublanguage.")
            adjustp.add_argument('--lac-level', '-l', type=str, default=None, help="Lactation level. See BreastStore.as for your mod. You can use the adjustment sublanguage.")
            adjustp.add_argument('--nipple-length', '-n', type=str, default=None, help="Nipple length. You can use the adjustment sublanguage.")
            adjustp.add_argument('--fullness', '-f', type=str, default=None, help="How full the breasts are. You can use the adjustment sublanguage.")
            adjustp.add_argument('--times-milked', '-t', type=str, default=None, help="How many times the NPC has been milked. You can use the adjustment sublanguage.")
            adjustp.set_defaults(npc_cmd=self.cmd_breasts_adjust)

    def cmd_breasts_show(self, args: argparse.Namespace) -> bool:
        with log.info(f'Breasts:'):
            if self.breasts is None:
                log.info('Skipped (Not supported on this NPC.)')
                return
            self.breasts.display()

    def cmd_breasts_reset(self, args: argparse.Namespace) -> bool:
        with log.info(f'Breasts:'):
            if self.breasts is None:
                log.info('Skipped (Not supported on this NPC.)')
                return
            if self.reset_breasts():
                log.info('Reset successful!')
                return True # Yes, save.
            log.error('Reset failed!')
            return False # oh god oh fuck
        
    def reset_breasts(self) -> bool:
        self.breasts.reset()
        return True
        
    def cmd_breasts_adjust(self, args: argparse.Namespace) -> bool:
        with log.info(f'Breasts:'):
            if self.breasts is None:
                log.info('Skipped (Not supported on this NPC.)')
                return
            if args.cup_size is not None:
                if args.cup_size.isdigit():
                    self.breasts.cupSize = self.breasts.ENUM_CUPSIZE(int(args.cup_size))
                elif args.cup_size.isidentifier():
                    self.breasts.cupSize = self.breasts.ENUM_CUPSIZE[args.cup_size]
                else:
                    max_cupsize = max(list(map(int, self.breasts.ENUM_CUPSIZE)))
                    self.breasts.cupSize = self.breasts.ENUM_CUPSIZE(parsePropertyAdjustment(self.breasts.cupSize.value, args.cup_size, 0, max_cupsize, ERoundingType.ROUND))
            if args.lac_level is not None:
                if args.lac_level.isdigit():
                    self.breasts.lactationLevel = self.breasts.ENUM_LACTATIONLEVEL(int(args.lac_level))
                elif args.lac_level.isidentifier():
                    self.breasts.lactationLevel = self.breasts.ENUM_LACTATIONLEVEL[args.lac_level]
                else:
                    max_lactationLevel = max(list(map(int, self.breasts.ENUM_LACTATIONLEVEL)))
                    self.breasts.lactationLevel = self.breasts.ENUM_LACTATIONLEVEL(parsePropertyAdjustment(self.breasts.lactationLevel.value, args.lac_level, 0, max_lactationLevel, ERoundingType.ROUND))
            if args.rows is not None:
                self.breasts.rows = parsePropertyAdjustment(self.breasts.rows, args.rows, 0, 65535, ERoundingType.ROUND)
            if args.nipple_length is not None:
                self.breasts.nippleLength = parsePropertyAdjustment(self.breasts.nippleLength, args.nipple_length, 0., 65535., ERoundingType.NONE)
            if args.fullness is not None:
                self.breasts.fullness = parsePropertyAdjustment(self.breasts.fullness, args.fullness, 0, 65535, ERoundingType.ROUND)
            if args.times_milked is not None:
                self.breasts.timesMilked = parsePropertyAdjustment(self.breasts.timesMilked, args.times_milked, 0, 65535, ERoundingType.ROUND)
            log.info('Done!')
            return True # Yes, save.

    def cmd_set(self, args: argparse.Namespace) -> bool:
        propID: str = args.property
        prop = self.properties[propID]
        if prop.readonly:
            print(f'Cannot change {self.propID} (readonly).')
            return False # Don't save
        value: Any = args.value
        if args.type in ('str', 'string'):
            value = str(value)
        elif args.type in ('int', 'integer'):
            value = int(value)
        elif args.float in ('float', 'number'):
            value = float(value)
        old_value = prop.value if prop.has_value else '(not set)'
        self.properties[propID].value = value
        print(f'  [{prop.PREFIX}] Changing {prop.name}: {old_value!r} -> {value!r}')
        return True # Yes, save.

    def get_properties(self) -> List[str]:
        return list(self.properties.keys())

    def reset(self, properties) -> None:
        for prop in self.properties.values():
            if prop.name in properties:
                prop.reset()

    def show(self, properties) -> None:
        header: List[str] = ['Type', 'Name', 'Value', 'Default', 'Notes']
        dirs: List[str] = ['left', 'left', 'right', 'right', 'left']
        rows: List[List[Any]] = []
        for k, prop in sorted(self.properties.items()):
            if properties is None or prop.name in properties:
                rows += [prop.display_row()]
        print(tabulate.tabulate(
            tabular_data=rows,
            headers=header, 
            colalign=dirs,
            tablefmt=TABULATE_TABLEFMT
            ))

    def afterLoad(self) -> None:
        '''
        Called in Save.loadNPCs().

        Custom fields should load from properties here.
        '''
    def beforeSave(self) -> None:
        '''
        Called in Save.loadNPCs().

        Custom fields should save to properties here.
        '''
