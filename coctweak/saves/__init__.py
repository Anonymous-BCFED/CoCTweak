from __future__ import absolute_import, annotations

import logging
import os
from pathlib import Path
import platform
import random
import string
from typing import Optional

from miniamf import AMF3, sol
from miniamf.amf3 import Decoder, Encoder
from miniamf.util import BufferedByteStream

from coctweak.saves._globaldata import GlobalData
#from coctweak.saves.ej.save import EJSave # DROPPED
from coctweak.saves._save import BaseSave
from coctweak.saves.hgg.save import HGGSave
from coctweak.saves.uee.save import UEESave
from coctweak.saves.vanilla.save import VanillaSave
from coctweak.utils import _do_cleanup, sanitizeFilename

from coctweak.logsys import getLogger #isort: skip
log = getLogger(__name__)

from ruamel.yaml import YAML #isort: skip
yaml = YAML(typ='rt')

MOD_VERSION = 2066

# Enable to spam detection stuff.
DETECTION_DEBUG = False

__FDBDIR: str = None
def getFlashBaseDir():
    global __FDBDIR
    if __FDBDIR is None:
        flashDBDir = ''
        if 'COCTWEAK_SHARED_OBJECTS_DIR' in os.environ and os.environ['COCTWEAK_SHARED_OBJECTS_DIR'] != '':
            log.warning('COCTWEAK_SHARED_OBJECTS_DIR is set! This should only be the case if you are testing something.')
            if os.name == 'nt':
                cmd = 'set COCTWEAK_SHARED_OBJECTS_DIR='
            else:
                cmd = 'export -n COCTWEAK_SHARED_OBJECTS_DIR'
            log.warning('To clear this warning, enter `%s` into your current shell.', cmd)
            __FDBDIR = os.environ['COCTWEAK_SHARED_OBJECTS_DIR']
            return __FDBDIR
        if platform.system() == 'Windows':
            flashDBDir = os.path.join(os.environ['APPDATA'], 'Macromedia', 'Flash Player', '#SharedObjects')
        elif platform.system() == 'Linux':
            flashDBDir = os.path.expanduser('~/.macromedia/Flash_Player/#SharedObjects/')
        if os.path.isdir(flashDBDir):
            for subdir in os.listdir(flashDBDir):
                if subdir not in ('.','..'):
                    __FDBDIR = os.path.join(flashDBDir, subdir)
                    break
        if __FDBDIR is None:
            fdbpath = Path(flashDBDir) / ''.join(random.choice(string.ascii_uppercase) for _ in range(8))
            fdbpath.mkdir(parents=True, exist_ok=True)
            log.warning(f'Created {fdbpath}')
            __FDBDIR = str(fdbpath)
    return __FDBDIR


LSO_DIR = Path(getFlashBaseDir())

NSLOOKUP = {
    # Dropped #'ej': 'EndlessJourney'
}

def getSaveFilename(host: str, svID: str, ext: str='.sol') -> str:
    if host in ('@file', 'file'):
        return svID
    svID = int(svID)
    namespace = ''
    if '/' in host:
        host, namespace = host.split('/')
    basedir = os.path.join(LSO_DIR, host)
    if namespace != '':
        subdir = NSLOOKUP[namespace]
        basedir = os.path.join(basedir, '#CoC', subdir)
    return os.path.join(basedir, f'CoC_{svID}{ext}')

def loadSave(data: dict, isFile: bool) -> Optional[BaseSave]:
    #print(repr(data))
    if isFile:
        data = data['data']
        assert 'name' not in data.keys()
        data.name = ''
    saveLoader = detectModType(data, isFile)
    if saveLoader is None:
        return None
    #print(saveLoader.NAME)
    save = saveLoader()
    save.deserialize(data, isFile)
    return save

def saveToSOL(save, filename, slotID):
    fdata = sol.SOL(f'CoC_{slotID}')
    fdata.update(save.serialize())
    sol.save(fdata, filename, AMF3)

def loadFromSlot(hostname: str, slotID: int, quiet:bool = False, loglevel_file_not_found: int=logging.CRITICAL, for_backup:bool = False) -> BaseSave:
    namespace = ''
    orighost = hostname
    if for_backup:
        loglevel_file_not_found=logging.WARNING
    isFile = hostname in ('@file', 'file')
    if not isFile:
        if '/' in hostname:
            hostname, namespace = hostname.split('/')
        if not os.path.isdir(os.path.join(LSO_DIR, hostname)):
            log.arbitrary(loglevel_file_not_found, 'Flash does not have a directory for hostname {!r}.'.format(sanitizeFilename(os.path.join(LSO_DIR, hostname))))
            return None
        if namespace != '':
            subdir = NSLOOKUP[namespace]
            nsdir = os.path.join(LSO_DIR, hostname, '#CoC', subdir)
            if not os.path.isdir(nsdir):
                log.arbitrary(loglevel_file_not_found, 'Flash does not have a directory for namespace {!r} ({}).'.format(namespace, sanitizeFilename(nsdir)))
                return None

    filename = getSaveFilename(orighost, slotID)

    save = loadSaveFromFile(filename, quiet, loglevel_file_not_found=loglevel_file_not_found, is_file=isFile)
    if save is None:
        return None
    save.loading_for_backup = for_backup
    save.id = slotID
    save.host = hostname
    save.namespace = namespace
    save.global_data = None
    if not isFile:
        save.global_data = loadGlobalData(save.GLOBAL_DATA_TYPE, save.GLOBAL_DATA_FILENAME, hostname, quiet, loglevel_file_not_found=loglevel_file_not_found)
    return save

def loadSaveFromFile(filename: str, quiet:bool = False, loglevel_file_not_found: int=logging.CRITICAL, is_file:bool = False) -> BaseSave:
    try:
        if not quiet:
            log.info(f'Loading {sanitizeFilename(filename)}...').indent()
        if not os.path.isfile(filename):
            log.arbitrary(loglevel_file_not_found, '%r does not exist.', sanitizeFilename(filename))
            return None
        savedata: dict = sol.load(filename) if not is_file else loadCOCFile(filename)
        save = loadSave(savedata, is_file)
        if save is None:
            return None
        save.filename = filename
    finally:
        if not quiet:
            log.dedent()
    return save

def loadRawDataFromFile(filename: str, quiet:bool = False, loglevel_file_not_found: int=logging.CRITICAL, is_file:bool = False) -> BaseSave:
    try:
        if not quiet:
            log.info(f'Loading {sanitizeFilename(filename)}...').indent()
        if not os.path.isfile(filename):
            log.arbitrary(loglevel_file_not_found, '%r does not exist.', sanitizeFilename(filename))
            return None
        data: dict = sol.load(filename) if not is_file else loadCOCFile(filename)
    finally:
        if not quiet:
            log.dedent()
    return data

def loadCOCFile(filename) -> dict:
    with open(filename, 'rb') as f:
        data = Decoder()
        data.stream = BufferedByteStream(f)
        return data.readElement()

def saveCOCFile(filename: str, data: dict) -> None:
    with open(filename+'.tmp', 'wb') as f:
        data = Encoder()
        data.stream = BufferedByteStream(f)
        data.writeObject(data)
    os.replace(filename+'.tmp', filename)


def loadGlobalData(pObjType, pObjFilename:str, hostname:str, quiet:bool = False, loglevel_file_not_found: int=logging.CRITICAL) -> Optional[GlobalData]:
    if pObjType is None:
        return None
    if pObjFilename is None:
        return None
    filename = os.path.join(LSO_DIR, hostname, pObjFilename+'.sol')
    try:
        if not quiet:
            log.info('Loading %s...', sanitizeFilename(filename)).indent()
        if not os.path.isfile(filename):
            log.arbitrary(loglevel_file_not_found, '%r does not exist.', sanitizeFilename(filename))
            return None

        pobj = pObjType()
        pobj.deserialize(sol.load(filename))
        pobj.filename = filename
        pobj.host = hostname
    finally:
        if not quiet:
            log.dedent()
    return pobj

def saveToSlot(hostname: str, slotID: int, save:BaseSave, quiet:bool=False) -> None:
    filename = getSaveFilename(hostname, slotID)
    isFile = hostname in ('@file', 'file')
    try:
        if not quiet:
            log.info(f'Saving {sanitizeFilename(filename)}...').indent()
        if isFile:
            saveCOCFile(filename, save.serialize())
        else:
            saveToSOL(save, filename, slotID)
    finally:
        if not quiet:
            log.dedent()
def saveRawDataToFile(hostname: str, slotID: int, data: dict, quiet:bool=False) -> None:
    filename = getSaveFilename(hostname, slotID)
    isFile = hostname in ('@file', 'file')
    try:
        if not quiet:
            log.info(f'Saving {sanitizeFilename(filename)}...').indent()
        if isFile:
            saveCOCFile(filename, data)
        else:
            fdata = sol.SOL(f'CoC_{slotID}')
            fdata.update(data)
            sol.save(fdata, filename, AMF3)
    finally:
        if not quiet:
            log.dedent()

# I've talked to Mothman of HGG about a better way of detecting the mod, and apparently got some balls rolling.
# Until then:
def detectModType(data: sol.SOL, isFile: bool) -> Optional[BaseSave]:
    if isinstance(data, sol.SOL):
        log.debug(f'SOL name: {data.name!r}')
    else:
        log.debug(f'SOL name: N/A (not a SOL)')
    if 'version' in data:
        log.debug(f'Game version: {data["version"]!r}')
    else:
        log.debug('Game version: SOL["version"] missing!')

    save_version = data["flags"].get("2066", 'N/A')
    log.debug(f'Flag 2066 (MOD_SAVE_VERSION): {save_version!r}')

    if DETECTION_DEBUG:
        with open('last-save-raw.yml', 'w') as f:
            yaml.dump(data, f)
        with open('last-save-cleaned.yml', 'w') as f:
            yaml.dump(_do_cleanup(data), f)

    flags = data.get('flags', {})
    if str(MOD_VERSION) not in flags:
        log.debug(f'Flag {MOD_VERSION} not set.')
        log.debug('DETECTED AS VANILLA')
        return VanillaSave

    # Endless Journey nicely announces itself.
    if data['version'] == 'Endless Journey':
        log.debug('save["version"] is "Endless Journey".')
        log.debug('DETECTED AS EJ')
        #return EJSave
        return None # Support dropped

    # HGG version 1.4.11 finally announces itself.
    if data['version'] is not None and data['version'].startswith('hgg '):
        # Some version of HGG
        log.debug(f'save["version"] is present and starts with \'hgg \'.')
        log.debug(f'DETECTED AS HGG')
        return HGGSave

    if 'serializationVersionDictionary' in data:
        log.debug('Found UEE\'s weird serializationVersionDictionary shit')
        log.debug('DETECTED AS UEE')
        return UEESave

    # HGG has the selfSaving NPCs.
    # EJ has them under the npc key in a similar way.
    # Same with UEE, but UEE has a different serialization versioning thing.
    if 'selfSaving' in data:
        # Some version of HGG
        log.debug(f'save["selfSaving"] is present.')
        log.debug(f'DETECTED AS HGG')
        return HGGSave

    if isinstance(data['version'], str):
        # Avoiding regex for speed
        # EJ, UEE, and Revamp: 1.2.3_mod_4.5.6
        chunks = data['version'].split('_')
        if len(chunks) == 3:
            baseversion, mod, modversion = chunks
            if mod == 'mod': # Uncreative name? UEE!
                log.debug(f'save["version"] has "mod" as the mod ID.')
                log.debug(f'DETECTED AS UEE/REVAMP.')
                return UEESave

    return None
