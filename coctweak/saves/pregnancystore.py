from typing import TypeVar,Union,Generic,Type
from enum import IntEnum

from coctweak.logsys import getLogger
log = getLogger(__name__)

K = TypeVar('K', bound=IntEnum) # KFlag Type
T = TypeVar('T', bound=IntEnum) # Pregnancy Type
I = TypeVar('I', bound=IntEnum) # Incubation Time

class BasePregnancyStore(Generic[T, I]):
    PREGTYPE_ENUM: Type[IntEnum] = IntEnum
    INCUBATION_ENUM: Type[IntEnum] = IntEnum

    def __init__(self, save) -> None:
        self.save = save

    def reset(self) -> None:
        self.type = self.PREGTYPE_ENUM(0)
        self.last_notice = 0
        self.incubation = 0
        self.allowHerm = False

    @property
    def type(self) -> T:
        return T(0)

    @type.setter
    def type(self, value: Union[T, int]) -> None:
        pass

    @property
    def last_notice(self) -> int:
        return 0
    @last_notice.setter
    def last_notice(self, value: int) -> None:
        pass

    @property
    def incubation(self) -> int:
        return 0
    @incubation.setter
    def incubation(self, value: Union[I, int]) -> None:
        pass

    @property
    def allow_herm(self) -> bool:
        return False
    @allow_herm.setter
    def allow_herm(self, value: bool) -> None:
        pass

    @property
    def is_pregnant(self) -> bool:
        return self.type != 0

    def knockUp(self, newPregType: Union[T, int], newPregIncubation: Union[I, int]) -> None:
        return

class VarPregnancyStore(BasePregnancyStore[T, I]):
    '''
    Universal pregnancy object that mimics PregnancyStore, but uses vars instead.

    Used in player, mostly.
    '''

    #public function PregnancyStore(pregType:int, pregInc:int, buttPregType:int, buttPregInc:int) {
    def __init__(self, save, type_var, incubation_var) -> None:
        super().__init__(save)
        #print(repr(save), repr(self.PREGTYPE_ENUM))
        self._type: IntEnum = self.PREGTYPE_ENUM(0)
        self._last_notice: int = 0
        self._incubation: int = 0
        self._allowHerm: bool = False

        self.type_var: str = type_var
        self.incubation_var: str = incubation_var

    @property
    def type(self) -> T:
        return self._type
    @type.setter
    def type(self, value: Union[T, int]) -> None:
        if isinstance(value, int):
            value = self.PREGTYPE_ENUM(value)
        self._type = value

    @property
    def last_notice(self) -> int:
        return self._last_notice
    @last_notice.setter
    def last_notice(self, value: int) -> None:
        self._last_notice = value

    @property
    def incubation(self) -> int:
        return self._incubation
    @incubation.setter
    def incubation(self, value: int) -> None:
        self._incubation = value

    @property
    def allow_herm(self) -> bool:
        return self._allowHerm
    @allow_herm.setter
    def allow_herm(self, value: bool) -> None:
        self._allowHerm = value

    @property
    def is_pregnant(self) -> bool:
        return self.type != 0

    def serializeTo(self, data: dict) -> None:
        data[self.type_var] = self._type.value
        data[self.incubation_var] = self._incubation
        return

    def deserializeFrom(self, data: dict) -> None:
        self._type = self.PREGTYPE_ENUM(int(data[self.type_var]))
        self._incubation = int(data[self.incubation_var])
        return

    def display(self) -> None:
        if not self.is_pregnant:
            log.info('Not pregnant.')
        else:
            log.info(f'Type:        {self.type.name} ({self.type.value})')
            #log.info(f'Last Notice: {self.last_notice}')
            log.info(f'Incubation:  {self.incubation} hours')

    def knockUp(self, newPregType: Union[T, int], newPregIncubation: Union[I, int], allowHerm: bool) -> None:
        ## from [HGG]/classes/classes/Character.as: public function knockUpForce(type:int = 0, incubationDuration:int = 0, forceAllowHerm:Boolean = false):void { @ eRA8BnbqE8LXfUKaKOaG40RrcSO2OLfvw2PGcYu8RK86u73O8McgXpfE4e7U3b60Le8b36rwedbeqD2Lg0cZ3iObOW0IR4eo
        #//TODO push this down into player?
        #_pregnancyType = type;
        self.type = newPregType
        #_pregnancyIncubation = (type == 0 ? 0 : incubationDuration); //Won't allow incubation time without pregnancy type
        self.incubation = 0 if self.type.value == 0 else newPregIncubation
        #pregnancyAllowHerm = isHerm() || forceAllowHerm;
        self.allowHerm = self.save.isHerm() or allowHerm

class FlagPregnancyStore(BasePregnancyStore[T, I]):
    '''
    A fairly universal implementation of CoC's pregnancy store (without the simulation stuff).

    One difference: A separate store is needed for each oriface (vag and butt), but this
    gets rid of a bunch of redundancy.
    '''
    PREG_TYPE_MASK = 0x0000FFFF
    PREG_NOTICE_MASK = 0x7FFF0000

    #public function PregnancyStore(pregType:int, pregInc:int, buttPregType:int, buttPregInc:int) {
    def __init__(self, save, typeFlag: Union[K, int], incubationFlag: Union[K, int]) -> None:
        super().__init__(save)
        self.typeFlag: Union[K, int] = typeFlag
        self.incubationFlag: Union[K, int] = incubationFlag

    @property
    def type(self) -> T:
        return self.PREGTYPE_ENUM(self.save.flags.get(self.typeFlag, 0) & self.PREG_TYPE_MASK)
    @type.setter
    def type(self, value: Union[T, int]) -> None:
        if isinstance(value, IntEnum):
            value = value.value
        self.save.flags.set(self.typeFlag, (self.type.value & self.PREG_TYPE_MASK) | value)

    @property
    def last_notice(self) -> int:
        return (self.save.flags.get(self.typeFlag, 0) & self.PREG_NOTICE_MASK) >> 16
    @last_notice.setter
    def last_notice(self, value: int) -> None:
        self.save.flags.set(self.typeFlag, (self.type.value & self.PREG_NOTICE_MASK) | (value << 16))

    @property
    def incubation(self) -> int:
        return self.save.flags.get(self.incubationFlag, 0)
    @incubation.setter
    def incubation(self, value: int) -> None:
        return self.save.flags.set(self.incubationFlag, value)

    @property
    def allow_herm(self) -> bool:
        return self._allowHerm
    @allow_herm.setter
    def allow_herm(self, value: bool) -> None:
        self._allowHerm = value

    @property
    def is_pregnant(self) -> bool:
        return self.type != 0

    def knockUp(self, newPregType: Union[T, int], newPregIncubation: Union[I, int]) -> None:
        if isinstance(newPregType, IntEnum):
            newPregType = newPregType.value
        if isinstance(newPregIncubation, IntEnum):
            newPregIncubation = newPregIncubation.value
        ## from [HGG]/classes/classes/PregnancyStore.as: public function knockUpForce(newPregType:int = 0, newPregIncubation:int = 0, forceAllowHerm:Boolean = false):void { @ 3bT6b7g1UcRK0XK9PVaBr9KF5GzeSg3cq416afx8C81MW8Hw1LN5wz5qA4TEdsR6OkfH80fYd3jd5GbmY709ebQ6P7cKhaGN
        #if (_pregnancyTypeFlag == 0 || _pregnancyIncubationFlag == 0) return; //Check that these variables were provided by the containing class
        if self.typeFlag == 0 or self.incubationFlag == 0:
            return
        #if (newPregType != 0) newPregType = (game.flags[_pregnancyTypeFlag] & PREG_NOTICE_MASK) + newPregType;
        if newPregType != 0:
            newPregType = (self.save.flags.get(self.typeFlag, 0) & self.PREG_NOTICE_MASK) | newPregType
        #//If a pregnancy 'continues' an existing pregnancy then do not change the value for last noticed stage
        #game.flags[_pregnancyTypeFlag] = newPregType;
        self.save.flags.set(self.typeFlag, newPregType)
        #game.flags[_pregnancyIncubationFlag] = (newPregType == 0 ? 0 : newPregIncubation); //Won't allow incubation time without pregnancy type
        self.incubation = 0 if newPregType == 0 else newPregIncubation
        #if (forceAllowHerm || player.isHerm()) allowHerm = true;

    def display(self) -> None:
        if not self.is_pregnant:
            log.info('Not pregnant.')
        else:
            log.info(f'Type:        {self.type.name} ({self.type.value})')
            log.info(f'Last Notice: {self.last_notice}')
            log.info(f'Incubation:  {self.incubation} hours')
        typeval: int = self.save.flags.get(self.typeFlag, 0)
        flagName: str = self.save.flags.getNameOf(self.typeFlag)
        flagID: str = self.save.flags.getValueOf(self.typeFlag)
        nspaces =  23+len(str(flagName))+len(str(flagID))+len(str(typeval))

        with log.info(f'Type Flag: {flagName} ({flagID}) = {typeval} (0x{typeval:08X})'):
            raw_type_val = (typeval&self.PREG_TYPE_MASK)
            raw_notice_val = ((typeval&self.PREG_NOTICE_MASK)>>16)
            padding = ' '*(nspaces-17-2)
            log.info(f'Type:        {padding}0x    {raw_type_val:04X}')
            log.info(f'Last Notice: {padding}0x{raw_notice_val:04X}')
        incubationval: int = self.save.flags.get(self.incubationFlag, 0)
        log.info(f'Incubation Flag: {self.save.flags.getNameOf(self.incubationFlag)} ({self.save.flags.getValueOf(self.incubationFlag)}) = {incubationval} ({incubationval:X})')
