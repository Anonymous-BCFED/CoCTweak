import json # Sigh.
from coctweak.saves.common.item import BaseItem
from coctweak.saves._saveobj import SaveObject
class WearableSlot(SaveObject):
    def __init__(self, save: 'BaseSave', _id: str, label: str, defaultID: str):
        self.save: 'BaseSave' = save

        #: Slot ID, NOT THE ITEM ID!
        self.id: str = _id
        self.label: str = label
        self.default: str = defaultID

        self.item: BaseItem = self.save.ITEM_TYPE()
        self.bonus: dict = None

    def serialize(self) -> dict:
        o = {}
        o['id'] = self.item.id
        if self.bonus is not None:
            o['bonus'] = json.dumps(self.bonus)
        return o

    def deserialize(self, data: dict) -> None:
        self.bonus = None
        self.item = self.save.ITEM_TYPE()
        self.item.id = data.get('id', self.default)
        if self.item.id == self.default:
            return
        self.item.from_items_json(self.save.getItemMeta(data['id']))
        if 'bonus' in data:
            self.bonus = json.loads(self.bonus)
