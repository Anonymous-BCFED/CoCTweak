from __future__ import absolute_import

import argparse
from ast import Set
import atexit
import logging
import os
import random
import sys
from typing import Dict

import colorama
import tabulate

from coctweak.cmd import register_parsers
from coctweak.consts import GENERATED_AT, GENERATED_AT_SIMPLIFIED, GITHASH, GITREPO, MOD_SUPPORT, NAME, TABULATE_TABLEFMT, VERSION  # NOQA
from coctweak.logsys import defaultSetup, verboseSetup


class RootArgumentParser(argparse.ArgumentParser):
    def print_help(self, file=None):
        super().print_help(file)
        command = None
        if len(sys.argv) > 1:
            command = sys.argv[1]
        if command not in ('--help', '-h', None):
            return
        print('', file=file)
        print(f'Website: {GITREPO.getSiteURL()}', file=file)
        print(f'Git commit: {GITHASH}', file=file)
        print(f'Built at: {GENERATED_AT.isoformat()}', file=file)
        if hasattr(sys.modules[__name__], '__compiled__'):
            print(
                f'Compiled with Nuitka v{__compiled__.major}.{__compiled__.minor}.{__compiled__.micro}')
        print('Against the following mods:', file=file)
        for modID, modData in MOD_SUPPORT.items():
            print(f' - {modID} v{modData.version} ({modData.commit})', file=file)


def main():
    global TABULATE_TABLEFMT
    OUTPUT_PGODATA: bool = os.environ.get('COCTWEAK_OUTPUT_PGODATA')
    if OUTPUT_PGODATA:
        import inspect,json
        print('COCTWEAK_OUTPUT_PGODATA is set, gathering PGO data...')
        #print(f'{len(PGO_IMPORTS)} imports')
        # def _pgo_handle_imports(evtname: str, t: tuple) -> Any:
        #     if evtname == 'import':
        #         (module, filename, sys_path, sys_meta_path, sys_path_hooks) = t
        #         #print(module)
        #         PGO_IMPORTS.add(str(module))
        # sys.addaudithook(_pgo_handle_imports)
        def _pgo_handle_exit() -> None:
            # PGO_IMPORTS = 
            PGO_MODULES: set[str] = set()
            PGO_PACKAGES: set[str] = set()
            for modname, mod in sys.modules.items():
                if inspect.ismodule(mod):
                    PGO_MODULES.add(modname)
                else:
                    PGO_PACKAGES.add(modname)
            with open('.PGO_IMPORTS.json', 'w') as f:
                data = {
                    'modules': sorted(PGO_MODULES),
                    'packages': sorted(PGO_PACKAGES),
                }
                json.dump(data,f)
        atexit.register(_pgo_handle_exit)
    argp = RootArgumentParser(
        'coctweak', description=f'{NAME} Save Editor v{VERSION}-{GENERATED_AT_SIMPLIFIED}')

    argp.add_argument('--quiet', '-q', action='store_true',
                      default=False, help='Silence information-level messages.')
    argp.add_argument('--verbose', '-v', action='store_true',
                      default=False, help='More verbose logging.')
    argp.add_argument('--debug', action='store_true',
                      default=False, help='Display debug-level messages.')
    argp.add_argument('--no-colors', action='store_true',
                      default=False, help='Display debug-level messages.')
    argp.add_argument('--table-format', choices=tabulate.tabulate_formats,
                      default=TABULATE_TABLEFMT, help='Format tabulate uses to display tabular data.')
    argp.add_argument('--random-seed', type=int, default=None,
                      help='Force random seed to something. (Only used in body part descriptions and randomizers.) Also set when COCTWEAK_BUILDING_DOCS is set.')

    subp = argp.add_subparsers()

    register_parsers(subp)

    args = argp.parse_args()

    TABULATE_TABLEFMT = args.table_format

    if 'COCTWEAK_BUILDING_DOCS' in os.environ:
        args.random_seed = 1
        args.no_colors = True

    colorama.init(strip=True if args.no_colors else None)

    if getattr(args, 'cmd', None) is None:
        argp.print_help()
        sys.exit(1)

    if args.random_seed is not None:
        random.seed(args.random_seed)

    if not args.verbose:
        defaultSetup(logging.DEBUG if args.debug else logging.INFO)
    else:
        verboseSetup(logging.DEBUG if args.debug else logging.INFO)
    args.cmd(args)


if __name__ == '__main__':
    main()
