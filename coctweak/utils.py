import argparse
import binascii
import collections
from enum import Enum, IntEnum
import functools
import hashlib
import json
import math
import os
from pathlib import Path
import platform
import random
import shutil
import subprocess
import sys
from typing import Union
import warnings

import miniamf
from ruamel.yaml import YAML

from coctweak.flash_consts import FL_INT_MAX, FL_INT_MIN, FL_NUM_MAX, FL_NUM_MIN  # NOQA

yaml = YAML(typ='rt')

from coctweak.logsys import getLogger # isort: skip
log = getLogger(__name__)

from miniamf.amf3 import ByteArray

_NUMBER = Union[int, float]

HOST_TRANSLATIONS = {
    'localwithnet':  '#localWithNet',
    '@localwithnet': '#localWithNet',
    '@lwn':          '#localWithNet',
    '@file':         '@file',
}
def flash_host(hostname):
    if hostname is None:
        return None
    if hostname in ('@file', 'file'):
        print(f"NOTE: Hostname {hostname} indicates that you wish to read from a file directly.")
        return '@file'
    lhn = hostname.lower()
    if lhn in HOST_TRANSLATIONS:
        newhostname = HOST_TRANSLATIONS[lhn]
        print(f"NOTE: Hostname {hostname} has been interpreted as {newhostname}.")
        return newhostname
    return hostname

def cmd_deprecated(oldCommandName: str, newCommandName: str):
    def _decorator_deprecated(func):
        @functools.wraps(func)
        def _wrap(*args, **kwargs):
            log.warning(f'Command {oldCommandName!r} is deprecated! You should use {newCommandName!r} instead.')
            return func(*args, **kwargs)
        return _wrap
    return _decorator_deprecated


def deprecated(func):
    """This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emitted
    when the function is used."""
    @functools.wraps(func)
    def new_func(*args, **kwargs):
        warnings.simplefilter('always', DeprecationWarning)  # turn off filter
        warnings.warn("Call to deprecated function {}.".format(func.__name__),
                      category=DeprecationWarning,
                      stacklevel=2)
        warnings.simplefilter('default', DeprecationWarning)  # reset filter
        return func(*args, **kwargs)
    return new_func

def parseBoolean(new_value: str):
    new_value = new_value.lower()
    if new_value in ('y', 't', 'yes', 'true'):
        return True
    elif new_value in ('n', 'f', 'no', 'false'):
        return False
    else:
        raise ValueError('This argument only accepts y, n, yes, no, t, f, true, or false.')

class ERoundingType(IntEnum):
    NONE = 0
    ROUND = 1
    CEILING = 2
    FLOOR = 3

def parsePropertyAdjustment(orig_value: Union[float, int], adjustment: str, _min: Union[float, int], _max: Union[float, int], round_type: ERoundingType=ERoundingType.NONE) -> Union[float, int]:
    #print(repr(adjustment))
    assert isinstance(adjustment, str)
    adjustment = adjustment.strip()
    symbol: str = adjustment[0]
    adjustment = adjustment[1:]
    validSyms = '=-+/*'
    if symbol not in validSyms:
        raise ValueError(f"adjustment[0] needs to be one of "+(', '.join([repr(x) for x in validSyms])))
    forceFloat = False
    if adjustment.endswith('f'):
        adjustment = adjustment[:-1]
        forceFloat = True
    parse = float if forceFloat or '.' in adjustment else int
    new_value: Union[float, int] = parse(adjustment)
    if symbol == '=':
        pass
    elif symbol == '-':
        new_value = orig_value - new_value
    elif symbol == '+':
        new_value = orig_value + new_value
    elif symbol == '*':
        new_value = orig_value * new_value
    elif symbol == '/':
        new_value = orig_value / new_value

    if round_type == ERoundingType.ROUND:
        new_value = round(new_value)
    elif round_type == ERoundingType.CEILING:
        new_value = math.ceil(new_value)
    elif round_type == ERoundingType.FLOOR:
        new_value = math.floor(new_value)

    new_value = min(max(new_value, _min), _max)

    return new_value

def add_flash_host_arg(subp, varname='host', **kwargs):
    kwargs.setdefault('help', "Flash hostname the save will be stored in. Use @localWithNet instead of #localWithNet, and you can use @file to specify a static filename instead of a slot ID.")
    kwargs['type'] = flash_host
    subp.add_argument(varname, **kwargs)

def add_flash_id_arg(subp, varname='id', **kwargs):
    # subp.add_argument('id', type=str, help="ID of the save.")
    kwargs.setdefault('help', "Save slot ID, or, if you used @file as the host argument, the path to the file.")
    kwargs['type'] = str
    subp.add_argument(varname, **kwargs)

def dict_representer(dumper, data):
    return dumper.represent_dict(data.items())

def dict_constructor(loader, node):
    return collections.OrderedDict(loader.construct_pairs(node))

def remove_if_exists(path):
    if os.path.isdir(path):
        shutil.rmtree(path)
    elif os.path.isfile(path):
        os.remove(path)

def check_dir(path):
    if not os.path.isdir(path):
        os.makedirs(path)

def bool_from_boolargs(args, oldval, arg_true: str, arg_false: str) -> bool:
    if getattr(args, arg_true):
        return True
    if getattr(args, arg_false):
        return False
    return oldval


def in2feetinches(inches, footsuffix='\'', inchsuffix='"', divider='', maxprec=2):
    feet = round(inches // 12)
    inches = inches - (12 * feet)
    o = []
    if feet > 0:
        o += [f'{round(feet,maxprec):g}{footsuffix}']
    if inches > 0:
        o += [f'{round(inches,maxprec):g}{inchsuffix}']
    return divider.join(o)

def in2cm(inches):
    return inches * 2.54

def cm2metercm(cm, metersuffix='m', cmsuffix='cm', divider=', ', maxprec=2):
    meters = round(cm // 100)
    cm = cm - (100 * meters)
    o = []
    if meters > 0:
        o += [f'{round(meters,maxprec):g}{metersuffix}']
    if cm > 0:
        o += [f'{round(cm, maxprec):g}{cmsuffix}']
    return divider.join(o)

def display_length(inches, footsuffix='\'', inchsuffix='"', impdivider='', metersuffix='m', cmsuffix='cm', sidivider=', ', maxprec=1, systemdivider=' ', siwrap=True):
    impunits = in2feetinches(inches, footsuffix, inchsuffix, impdivider, maxprec)
    siunits = cm2metercm(in2cm(inches), metersuffix, cmsuffix, sidivider, maxprec)
    if siwrap:
        siunits = f'({siunits})'
    return f'{impunits}{systemdivider}{siunits}'

def display_bool(value: bool) -> str:
    return 'yes' if value else 'no'

def display_enum(value: Enum) -> str:
    return f'{value.name} ({value.value!r})'

def dump_to_yaml(input_filename, output_filename):
    if not os.path.isfile(input_filename):
        return
    sol = miniamf.sol.load(input_filename)
    data = _do_cleanup_object(sol)
    with open(output_filename, 'w') as f:
        yaml.dump(data, f)

def dump_to_json(input_filename, output_filename):
    if not os.path.isfile(input_filename):
        return
    sol = miniamf.sol.load(input_filename)
    data = _do_cleanup_object(sol)
    with open(output_filename, 'w') as f:
        json.dump(data, f, indent=2)

def is_int(inp:str) -> bool:
    try:
        return float(inp).is_integer()
    except:
        return False

def is_float(inp:str) -> bool:
    try:
        float(inp)
        return True
    except:
        return False

def _do_cleanup_object(obj):
    newobj = collections.OrderedDict()
    for k in sorted(obj.keys(), key=lambda x: str(x)):
        #for k in obj.keys():
        newobj[str(k)] = _do_cleanup(obj[k])
    return newobj


def _do_cleanup_list(lst):
    newlst = []
    for v in lst:
        newlst.append(_do_cleanup(v))
    return newlst


def _do_cleanup(thing):
    if isinstance(thing, dict):
        return _do_cleanup_object(thing)
    elif isinstance(thing, list):
        return _do_cleanup_list(thing)
    elif isinstance(thing, ByteArray):
        return _do_cleanup_bytearray(thing)
    else:
        return thing

def _do_cleanup_bytearray(ba):
    data = collections.OrderedDict()
    data['_type'] = 'amf3.ByteArray'
    data['compressed'] = ba.compressed
    data['data'] = binascii.hexlify(ba.getvalue())
    return data

if platform.system() == 'Windows':
    def sanitizeFilename(filename):
        return os.path.join('%APPDATA%', os.path.relpath(filename, os.environ['APPDATA']))
else:
    def sanitizeFilename(filename):
        home = os.path.expanduser('~')
        return os.path.join('~', os.path.relpath(filename, start=home))

def execute(cmd:list, display:bool=True, capture:bool=False) -> str:
    print('$ '+' '.join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False)
    o = b''
    while True:
        out = p.stdout.read(1)
        if out == b'' and p.poll() != None:
            break
        if out != b'':
            if capture:
                o += out
            if display:
                sys.stdout.write(out.decode('utf-8'))
                sys.stdout.flush()
    return o.decode('utf-8')

def add_hostid_args(subp: argparse.ArgumentParser, batch: bool) -> None:
    if not batch:
        add_flash_host_arg(subp)
        add_flash_id_arg(subp)

def clamp(value: _NUMBER, _min: _NUMBER, _max: _NUMBER) ->  _NUMBER:
    return min(max(value, _min), _max)

def rand(_max: int) -> int:
    ## from [HGG]/classes/classes/internals/Utils.as: public static function rand(max:Number):int { @ 4DL9yA0ni1130v3etV9Ot6Br43ZaWC2cY9Ww0t25G9gZp4avbn16bz392dnm8Qj7Ty6ze6Aq7Qw5ks8a6dvx7dN4TY4uhbSZ
    # return int(Math.random() * max);
    return math.floor(random.random() * _max)

def parseAndClampInt(input: str, lower: int=FL_INT_MIN, upper: int=FL_INT_MAX) -> int:
    return int(clamp(int(input), lower, upper))

def parseAndClampFloat(input: str, lower: float=FL_NUM_MIN, upper: float=FL_NUM_MAX) -> float:
    return float(clamp(float(input), lower, upper))

def _gethashsum(h, path: Path) -> bytes:
    with open(path, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b""):
            h.update(chunk)
    return h.digest()

def md5sum(path: Path) -> bytes:
    return _gethashsum(hashlib.md5(), path)

def sha256sum(path: Path) -> bytes:
    return _gethashsum(hashlib.sha256(), path)

def yaml_set_compact(yaml: YAML) -> None:
    yaml.default_flow_style = True
    yaml.compact(True, True)
