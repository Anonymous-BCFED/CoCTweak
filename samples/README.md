These files are exported saves used for testing.

## Uses

* Tracking save file changes over time
* Known-good saves for smoke tests
* Creating a quick and dirty save without jumping through 15 menus
* Examples of save file structures to examine for reverse-engineering purposes

## Recreating

In the event these saves become unreadable, or you wish to review the creation process, the script for making them is [here](../docs/development/samples.html).

You can also run the macros contained in (devtools/autogui-scripts/)[../devtools/autogui-scripts/].
