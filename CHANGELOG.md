# Changelog

## In Development (0.4.5?)

### Compatibility

<!-- devtools/updateSupportTable.py replaces this: -->
|                                                                     Mod | Version   | Git Commit                                                                                                                                              |
|------------------------------------------------------------------------:|:----------|:--------------------------------------------------------------------------------------------------------------------------------------------------------|
|                   [**HGG**](https://gitgud.io/BelshazzarII/CoCAnon_mod) | 1.6.17.1  | [`b56b7c4ca0372455298cffe272e648eaa8caf305`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/b56b7c4ca0372455298cffe272e648eaa8caf305)              |
|    [**UEE**](https://github.com/Kitteh6660/Corruption-of-Champions-Mod) | 1.4.19    | [`54fb4a179bcd4aea1cefada8d06dc0688d297657`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/54fb4a179bcd4aea1cefada8d06dc0688d297657) |
| [**Vanilla**](https://github.com/OXOIndustries/Corruption-of-Champions) | 1.0.2     | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00)  |
<!-- devtools/updateSupportTable.py END -->

### Big Changes

- Added `enable-encounters` command to `npc amily`

### Bugs Slain

- Releases now present the correct SHA256 hash instead of `None`
- Some NPCs were missing variables here and there.
- Minor tweaks to Amily

### Documentation

_TBD_

### Developer Stuff

- Fixes for packaging process.
- Generated RegexConsts lists use reduced attribute calls by setting `re.compile` and `re.MULTILINE` to module-level private variables.
- Generated RegexConsts lists now generate `__ALL__`.

## 0.4.4

This release is basically just [v0.4.3](https://gitlab.com/Anonymous-BCFED/CoCTweak/-/releases/v0.4.3) with an included README and FAQ.

See that release for a full list of changes since v0.4.2.

### Compatibility


|                                                                     Mod | Version   | Git Commit                                                                                                                                              |
|------------------------------------------------------------------------:|:----------|:--------------------------------------------------------------------------------------------------------------------------------------------------------|
|                   [**HGG**](https://gitgud.io/BelshazzarII/CoCAnon_mod) | 1.6.17.1  | [`b56b7c4ca0372455298cffe272e648eaa8caf305`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/b56b7c4ca0372455298cffe272e648eaa8caf305)              |
|    [**UEE**](https://github.com/Kitteh6660/Corruption-of-Champions-Mod) | 1.4.19    | [`54fb4a179bcd4aea1cefada8d06dc0688d297657`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/54fb4a179bcd4aea1cefada8d06dc0688d297657) |
| [**Vanilla**](https://github.com/OXOIndustries/Corruption-of-Champions) | 1.0.2     | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00)  |

### Bugs Slain

- Fixed automatic packaging script to include FAQ and README files.

## 0.4.3

### Compatibility

|                                                                     Mod | Version   | Git Commit                                                                                                                                              |
|------------------------------------------------------------------------:|:----------|:--------------------------------------------------------------------------------------------------------------------------------------------------------|
|                   [**HGG**](https://gitgud.io/BelshazzarII/CoCAnon_mod) | 1.6.17.1  | [`b56b7c4ca0372455298cffe272e648eaa8caf305`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/b56b7c4ca0372455298cffe272e648eaa8caf305)              |
|    [**UEE**](https://github.com/Kitteh6660/Corruption-of-Champions-Mod) | 1.4.19    | [`54fb4a179bcd4aea1cefada8d06dc0688d297657`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/54fb4a179bcd4aea1cefada8d06dc0688d297657) |
| [**Vanilla**](https://github.com/OXOIndustries/Corruption-of-Champions) | 1.0.2     | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00)  |

### Big Changes

* **BREAKING:** All instances of command-line flags '-m' changed to '-M' to comply with Nuitka weirdness.
* Python 3.11 support
* `*.gamedata.json` are cached to `*.pak` files using msgpack for speed.
* We use `tabulate` for generating tabular output now, instead of our crappy, home-rolled solution.

### Bugs Slain

* [#2](https://gitlab.com/Anonymous-BCFED/CoCTweak/-/issues/2) - Ceraph's piercings should no longer cause crashes.
* [#3](https://gitlab.com/Anonymous-BCFED/CoCTweak/-/issues/3) - KeyItems are now sorted by ID. This may not be the same as in-game sorting but it's all we have to go by, at the moment.
* ANSI character sequences not properly stripped from output
* Fixed some precision issues in HP calculation
* 

### Documentation

* Added an FAQ
* Added breadcrumbs

### Developer Stuff

* `poetryup` removed from buildscripts, since it's EOL.  [poetry-plugin-up](https://github.com/MousaZeidBaker/poetry-plugin-up) is used instead.
* Regular expressions pre-compiled with Pregex.

## 0.4.2

### Compatibility

| Mod | Version | Git Commit |
|---:|---|---|
| [**HGG**](https://gitgud.io/BelshazzarII/CoCAnon_mod) | 1.6.17.1 | [`d1c34ffc11da0f5a73705034e5447f8db1de4652`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/d1c34ffc11da0f5a73705034e5447f8db1de4652) |
| [**UEE**](https://github.com/Kitteh6660/Corruption-of-Champions-Mod) | 1.4.19 | [`54fb4a179bcd4aea1cefada8d06dc0688d297657`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/54fb4a179bcd4aea1cefada8d06dc0688d297657) |
| [**Vanilla**](https://github.com/OXOIndustries/Corruption-of-Champions) | 1.0.2 | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00) |

### Big Changes

* **Removed** `npc ... breasts clear` command, as I'm still not sure why the Hell you would use it, nor why I made it in the first place.

### Bugs Slain

* **Fixed** an ailments-related crash relating to me pasting `NAME` into the middle of a method like an idiot
* **Fixed** `npc ... breasts reset` not being found
* **Fixed** `npc ... breasts reset` not being correct for Kath.
* **Fixed** crash when `display`ing Vanilla saves.
* **Fixed** crash when `display`ing UEE saves.
* **Fixed** crash when loading characters with antennae.
* **Fixed** crash when loading malformed CoCTweak KFLAGs.

### Developer Stuff

*N/A*

## 0.4.1

### Compatibility

| Mod | Version | Git Commit |
|---:|---|---|
| [**HGG**](https://gitgud.io/BelshazzarII/CoCAnon_mod) | 1.6.17.1 | [`d1c34ffc11da0f5a73705034e5447f8db1de4652`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/d1c34ffc11da0f5a73705034e5447f8db1de4652) |
| [**UEE**](https://github.com/Kitteh6660/Corruption-of-Champions-Mod) | 1.4.19 | [`54fb4a179bcd4aea1cefada8d06dc0688d297657`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/54fb4a179bcd4aea1cefada8d06dc0688d297657) |
| [**Vanilla**](https://github.com/OXOIndustries/Corruption-of-Champions) | 1.0.2 | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00) |

### Big Changes

* **Added** `meditate` command
* **Switched** to a more secure YAML library

### Bugs Slain

*I forgor*

### Developer Stuff

* pyyaml is gone, long live ruamel.yaml.
* Big pybuildtool changes.
* Layout and formatting fixes.

### Known Issues

* **Development:** `devtools/build.py` does not generate documentation due to a weird dependency resolver issue. Use `devtools/buildDocs.py` instead.

## 0.4.0

### Compatibility


| Mod | Version | Git Commit |
|---:|---|---|
| [**HGG**](https://gitgud.io/BelshazzarII/CoCAnon_mod) | 1.6.9 | [`1b5af21128dc0da072d0f81ba7678f84629d57e2`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/1b5af21128dc0da072d0f81ba7678f84629d57e2) |
| [**UEE**](https://github.com/Kitteh6660/Corruption-of-Champions-Mod) | 1.4.19 | [`1843a31b6cad0c639cf5938b2d712dd9a8380592`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/1843a31b6cad0c639cf5938b2d712dd9a8380592) |
| [**Vanilla**](https://github.com/OXOIndustries/Corruption-of-Champions) | 1.0.2 | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00) |


### Big Changes

* **Added** initial support for Python 3.10, which is currently getting ready for release.
* **Added** support for fertility and femininity (treated as stats)
* **Added** `body ... breasts ...` commands
* **Added** `body ... face (set|show)` commands
* **Added** `body ... tail (set|show)` commands
* **Added** `get ... flags` (displays raw kFlag values)
* **Added** `npc` and `clean` to the `batch` command
* **Added** some filtering switches to `restore`
* **Added** support for some body parts in HGG and UEE.
  * Antennae (internal support only, for now)
  * Arms (internal support only, for now)
* **Removed** deprecated command `body ... balls add`
* **Removed** deprecated command `body ... balls deflate`
* **Removed** deprecated command `body ... balls inflate`
* **Removed** deprecated command `body ... balls remove`
* **Removed** deprecated command `body ... balls set-count`
* **Removed** deprecated command `body ... balls set-mult`
* **Removed** deprecated command `body ... balls set-size`

### Bugs Slain

* **Fixed** `restore` being broken.

### Developer Stuff

* `BodyPart`s now require a reference to the `BaseSave` in their constructor.
* Added `save.combat_stats.femininity` and `save.combat_stats.fertility`
* Broke `data/classes.yml` into `data/classes/**/*.yml`

## 0.3.1

### Compatibility


| Mod | Version | Git Commit |
|---:|---|---|
| [**HGG**](https://gitgud.io/BelshazzarII/CoCAnon_mod) | 1.6.9 | [`9a872a4a0f9b8e614fd31e9dec8be57493b89258`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/9a872a4a0f9b8e614fd31e9dec8be57493b89258) |
| [**UEE**](https://github.com/Kitteh6660/Corruption-of-Champions-Mod) | 1.4.19 | [`1843a31b6cad0c639cf5938b2d712dd9a8380592`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/1843a31b6cad0c639cf5938b2d712dd9a8380592) |
| [**Vanilla**](https://github.com/OXOIndustries/Corruption-of-Champions) | 1.0.2 | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00) |


### Big Changes

* **Completely overhauled** `backup` command. Works more consistently and logically. It also works faster.
  * **Added** `--lso-dir` command-line option to specify directory LSOs are kept in. (default: autodetected Flash dir)
  * **Added** `--to-dir` command-line option to specify the directory the backup is stored in. (default: `backup/`)

### Bugs Slain

* **Fixed** parse failures killing backups.
* **Fixed** `*.ssl` files killing backups on Windows (MUID.ssl in particular).
* **Fixed** unsupported EJ saves getting interpreted during backups somehow.

### Developer Stuff

* Better support for arrays in `genClasses.py`.
* `coctweak.saves.loadSaveFromFile()` returns None if it can't parse a save (instead of crashing).

## 0.3.0

### Compatibility


| Mod | Version | Git Commit |
|---:|---|---|
| [**HGG**](https://gitgud.io/BelshazzarII/CoCAnon_mod) | 1.6.7 | [`a6b8f22ec6b599d4da86d99faabdfc1fb33b2ed8`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/a6b8f22ec6b599d4da86d99faabdfc1fb33b2ed8) |
| [**UEE**](https://github.com/Kitteh6660/Corruption-of-Champions-Mod) | 1.4.19 | [`1843a31b6cad0c639cf5938b2d712dd9a8380592`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/1843a31b6cad0c639cf5938b2d712dd9a8380592) |
| [**Vanilla**](https://github.com/OXOIndustries/Corruption-of-Champions) | 1.0.2 | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00) |


### Big Changes
* **Binary release is now compiled to native code using Nuitka.** Unlike the versions that used pyinstaller, the executable *actually works* on Windows.
* **Added support for addictions.**
* **Backup directory structure changed.**
  * Backups now include a configuration file.
* Added `body ... skin show` so you can view your skin.
* Added `body ... skin set` to permit editing your skin.
* A great number of HGG NPCs and encounters are now supported.
* Underlying support for BreastStore. (Which is only used for Katherine, for some reason.)

### Bugs Slain
* Fixed some docs issues.
* Backup .gitignore file is no longer corrupt.

### Developer Stuff
* Overhauled PregnancyStores so they're a bit more standardized.
* Supports the new overhauled perks under development for HGG.
* Builds are tested on a private Jenkins server.
* Release builds are compiled with Jenkins for more consistent builds.

## 0.2.0

### Compatibility


| Mod | Version | Git Commit |
|---:|---|---|
| [**HGG**](https://gitgud.io/BelshazzarII/CoCAnon_mod) | 1.6.5.1 | [`dbc7ff67e97ea401f41e4b0fd5f51c22c94dac18`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/dbc7ff67e97ea401f41e4b0fd5f51c22c94dac18) |
| [**UEE**](https://github.com/Kitteh6660/Corruption-of-Champions-Mod) | 1.4.19 | [`1843a31b6cad0c639cf5938b2d712dd9a8380592`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/1843a31b6cad0c639cf5938b2d712dd9a8380592) |
| [**Vanilla**](https://github.com/OXOIndustries/Corruption-of-Champions) | 1.0.2 | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00) |


### Big Changes

* **`\*.coc` file support!** Just use `@file path/to/file.coc` instead of `hostname slot` when issuing a command.
* Added `npc` commands! This will allow you to see the KFlags and SSO properties of NPCs at a glance, as well as reset them and modify them.
  * Currently only supports HGG.
  * Use `npc-list hostname slot` to see a list of available NPCs.
* Added `body ... balls set`, which allows setting many testicle properties at once.
* Added `body ... balls adjust`, which allows adjusting many testicle properties at once. Uses a [new adjustment format](docs-src/includes/valueFormatting.template.md) to permit addition, subtraction, multiplication, and division of existing property values.
* Added `body ... cocks adjust`.
* Added `body ... vaginas adjust`.
* Added support for piercings.
* When displayed in `show`, the `Marble` status effect is now decoded into something that can be understood by a human.
* `inv` commands that take item IDs will fix the casing for you if you screw it up.
* `inv` will yell at you if you use an invalid item ID.
* `heal` now takes `--diseases` instead of `--parasites`.
* Added support for Nephilia slimes parasites in HGG.

### Bugs Slain

* CoC_Main.yml is now a permanent resident of backups, if it exists.

### Developer Stuff

* Added `coctweak.utils.parsePropertyAdjustment()`, which parses property adjustment language and applies it to a value.
* Added mechanisms for marking commands and methods as deprecated.
* Added Jenkinsfiles for CI builds and automating releases.

## 0.1.0

Significant changes, namely `BodyPart` support and code emulation
changes, and the ability to yell at developers about said changes when they
occur.  A few new commands have also been added, and `show` works better.

### Compatibility

| Mod | Version | Git Commit |
|---:|---|---|
| [**Vanilla**](https://github.com/OXOIndustries/Corruption-of-Champions) | 1.0.2 | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00) |
| [**HGG**](https://gitgud.io/BelshazzarII/CoCAnon_mod) | 1.6.4 | [`18686d1cbc46f291dfb7d45564b8121f19287aac`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/18686d1cbc46f291dfb7d45564b8121f19287aac) |
| [**UEE**](https://github.com/Kitteh6660/Corruption-of-Champions-Mod) | 1.4.19 | [`e06d15dcda96a9b565a7c3ebea1fed413584e141`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/e06d15dcda96a9b565a7c3ebea1fed413584e141) |

**Endless Journey support has been dropped** due to EJ ceasing development.

### Changes

* **Endless Journey support has been dropped** due to EJ ceasing development. Our support for it was never good in the first place, since its saves were an absolute nightmare to work with. **Feel free to submit merge requests if you feel like fixing it.** I left the code commented out so future archaeologists can revive it if needed.
* Added support for BodyParts (UEE and HGG only). Vanilla receives emulated support since it's less of a pain in the ass to use internally over the mess of variables Fen used.
* `show` command displays more body part stuff, and in a flexible way.
* `backup` command is faster and smarter.
* Added metadata for backpacks.
* Added `keyitem set` command.
* Added `set flag` command.
* Infections and status effects won't appear in the `heal` command unless summoned.

### Bugs Slain

* Inconsistencies with healing on HGG saves, caused by a few dumb typos and out of date code.
* `heal` will no longer kill you with starvation on UEE saves.
* Max HP is now correctly calculated on UEE.

### Developer stuff

* Massive changes to the buildsystem.
  * All building is done through devtools/build.py now.
  * Added preflight checks.
  * All CoC mods have been added as submodules
    * No longer spams GitHub/GitGud with HTTP requests
  * Added printSupportTable.py to print the changelog support table, which now includes commit SHA1s and links to everything.
  * Added *func-hashes*, which allow a developer to mark a section of code as being based on a particular ActionScript function or variable in a given file. The build process will then hash the contents of the subject, and then fail later builds if that original AS code is changed. See [func-hashes.md](docs/development/func-hashes.md).
  * Packaging is now done in Python.
  * Everything is BuildMaestro tasks now.
  * Added `portTool.py` to make porting crap from HGG to other mods easier.
* Added BodyParts.
* Added a *fuckload* more unit tests for BodyPart serialization.
* Big changes to `genEnums.py` and `genClasses.py`.
* Generated serialization code now includes more typing information and some static variables.
* BodyParts provide `serializeTo` and `serializeFrom`, so each body part can individually specify how to load stuff from the save `dict`.

### Caveats

* There's still a one-off with healing health in HGG saves, but since it gets clamped on load anyway, I'm not overly concerned. Probably rounding-related, or weirdness resulting from the differing implementations of floating point math. \*shrug\*
* Only really effects devs: My Base62 implementation is complete fucking garbage and will probably give someone hives. Currently, hash checks rely on the fact that I can simply compare the old garbage against the new garbage and if the input's the same, everything's cool. Unfortunately, the Base62 packages currently available assume that you're trying to convert an integer rather than turn an arbitrary bytestream from SHA512 into something legible, so this is probably as good as we're gonna get for a while.

## 0.0.4

### Compatibility

| Mod | Version |
| --- | --- |
| HGG | 1.6.3.1 |
| UEE | 1.0.2_mod_1.4.19 |

### Changes

* Added support for ass ratings (`buttRating` in the save file).
* `show` command is a little more readable.

### Bugs Slain

* Fixed EditorData using invalid function for deserializing integer data.
* Healing hunger in HGG no longer sets it to 0 (AKA dead).
* Fix crash caused by presence of CoC_Main-UEE file.
* Fix git crying about stuff not being in cache during backup.
* `inventory ... list` command works again.

## 0.0.3

### Compatibility

| Mod | Version |
| --- | --- |
| HGG | 1.6.3 |
| UEE | 1.0.2_mod_1.4.18b |

### Changes

* Added `get gems`
* Commands needing a hostname will accept `@localWithNet` or `@lwn` as `#localWithNet`.

### Bugs Slain

* Fixed outdated datafiles from HGG and UEE, fixed itemlibs as well.
* Each new file is scrubbed from backup individually prior to being added. Fixes a fatal.

## 0.0.2

First binary release, initial Windows support.

### Compatibility

| Mod | Version |
| --- | --- |
| HGG | 1.6.2 |
| UEE | 1.0.2_mod_1.4.18b |

### Changes

* Added `coctweak.cmd` to allow Windows users to run from source.
* Binary packages built for Windows and Linux, as well as a source tar.
* Shows version and date of build on `--help` screen.
* Buildsystem fixed to work on Windows.
* Buildsystem will now select latest available python 3.

### Bugs Slain
* Crashes in `body vagina` command tree fixed.
* Corrected documentation for `body vagina` command tree.
