import unittest

from _dev_regex import (
    PREG_AS3_BLOCKCOMMENT,
    PREG_AS3_LINECOMMENT,
    PREG_CLASSY_ENUM,
    PREG_CONST,
    PREG_CONSTS_PY_VERSION,
    PREG_CREATE_SFX,
    PREG_DEBUG_VARS,
    PREG_ENUM,
    PREG_FATAL_BAD_PACKAGE,
    PREG_HAS_SFX,
    PREG_HGG_SELF_SAVING_SIG,
    PREG_KFLAG_DEF,
    PREG_KFLAG_DEF_COMMENTED,
    PREG_KFLAG_REF,
    PREG_KFLAG_REF_BINARY_OP,
    PREG_KFLAG_REF_BREASTSTORE,
    PREG_KFLAG_REF_IFCHK,
    PREG_KFLAG_REF_PREGSTORE,
    PREG_KFLAG_REF_UNARY_OP,
    PREG_PUBLIC_STATIC_INT,
    PREG_RESET,
    PREG_RESET_VAR,
    PREG_RESET_VAR_COMMENTED,
    PREG_SER_FUNC,
    PREG_SERUUID_FUNC,
    PREG_SERVER_FUNC,
    PREG_SSO_ID,
    PREG_SSO_VERSION,
    PREG_STATIC_CONST,
    PREG_SUPPORT_TABLE_MARKERS,
    PREG_VAR,
    PREG_NPCPY_NPC_ID,
    PREG_NPCPY_NPC_ALIASES,
    PREG_NPCPY_NPC_CLS,
    PREG_NPCPY_SSO_ID,
    PREG_NPCPY_SSO_CLS,
)  # NOQA


class TestDevelopmentRegexPatterns(unittest.TestCase):
    def test_PREG_FATAL_BAD_PACKAGE(self) -> None:
        self.assertRegex(
            "FATAL: Error, failed to locate module 'pygit2.remote' you asked to include.",
            PREG_FATAL_BAD_PACKAGE,
        )

    def test_PREG_SUPPORT_TABLE_MARKERS(self) -> None:
        self.assertRegex(
            "<!-- devtools/updateSupportTable.py replaces this: -->",
            PREG_SUPPORT_TABLE_MARKERS,
        )
        self.assertRegex(
            "<!-- devtools/updateSupportTable.py END -->", PREG_SUPPORT_TABLE_MARKERS
        )

    def test_PREG_CONSTS_PY_VERSION(self) -> None:
        self.assertRegex(
            "VERSION: Final[Version] = Version('1.2.3')", PREG_CONSTS_PY_VERSION
        )

    def test_PREG_VAR(self) -> None:
        self.assertRegex("public var analLooseness:Number = 0;", PREG_VAR)

    def test_PREG_STATIC_CONST(self) -> None:
        self.assertRegex(
            "private static const SERIALIZATION_VERSION:int = 1;", PREG_STATIC_CONST
        )

    def test_PREG_SER_FUNC(self) -> None:
        self.assertRegex(
            "public function serialize(relativeRootObject:*):void {", PREG_SER_FUNC
        )

    def test_PREG_SERUUID_FUNC(self) -> None:
        self.assertRegex(
            "public function serializationUUID():String", PREG_SERUUID_FUNC
        )

    def test_PREG_SERVER_FUNC(self) -> None:
        self.assertRegex(
            "public function currentSerializationVerison():int {",
            PREG_SERVER_FUNC,
        )
        self.assertRegex(
            "public function currentSerializationVerison() {",
            PREG_SERVER_FUNC,
        )

    def test_PREG_PUBLIC_STATIC_INT(self) -> None:
        self.assertRegex(
            "public static const UNKNOWN_FLAG_NUMBER_00000:int                                   =    0;",
            PREG_PUBLIC_STATIC_INT,
        )

    def test_PREG_CLASSY_ENUM(self) -> None:
        self.assertRegex(
            'public static const HUMAN:CockTypesEnum = new CockTypesEnum("human");',
            PREG_CLASSY_ENUM,
        )

    def test_PREG_ENUM(self) -> None:
        self.assertRegex('public static const STR:String = "str";', PREG_ENUM)

    def test_PREG_HGG_SELF_SAVING_SIG(self) -> None:
        self.assertRegex("function get saveName()", PREG_HGG_SELF_SAVING_SIG)

    def test_PREG_KFLAG_DEF(self) -> None:
        self.assertRegex("// KFLAG_NAME:int = 123;", PREG_KFLAG_DEF)

    def test_PREG_KFLAG_DEF_COMMENTED(self) -> None:
        self.assertRegex(
            "// KFLAG_NAME:int = 123; // Some comment", PREG_KFLAG_DEF_COMMENTED
        )

    def test_PREG_KFLAG_REF(self) -> None:
        self.assertRegex("// kFLAGS.KFLAG_NAME", PREG_KFLAG_REF)

    def test_PREG_KFLAG_REF_BINARY_OP(self) -> None:
        self.assertRegex(
            "flags[kFLAGS.NEW_GAME_PLUS_LEVEL] == 0", PREG_KFLAG_REF_BINARY_OP
        )
        self.assertRegex(
            "flags[kFLAGS.AMILY_CAMP_CORRUPTION_FREAKED] = 1;", PREG_KFLAG_REF_BINARY_OP
        )

    def test_PREG_KFLAG_REF_UNARY_OP(self) -> None:
        self.assertRegex("flags[kFLAGS.NEW_GAME_PLUS_LEVEL]++", PREG_KFLAG_REF_UNARY_OP)
        self.assertRegex("flags[kFLAGS.NEW_GAME_PLUS_LEVEL]--", PREG_KFLAG_REF_UNARY_OP)

    def test_PREG_KFLAG_REF_IFCHK(self) -> None:
        self.assertRegex("if (flags[kFLAGS.CAMP_WALL_SKULLS])", PREG_KFLAG_REF_IFCHK)

    def test_PREG_KFLAG_REF_PREGSTORE(self) -> None:
        self.assertRegex(
            "new PregnancyStore(0, 0, kFLAGS.JOJO_BUTT_PREGNANCY_TYPE, kFLAGS.JOJO_EGGCUBATE_COUNT);",
            PREG_KFLAG_REF_PREGSTORE,
        )

    def test_PREG_KFLAG_REF_BREASTSTORE(self) -> None:
        self.assertRegex(
            "= new BreastStore(kFLAGS.KATHERINE_BREAST_SIZE);",
            PREG_KFLAG_REF_BREASTSTORE,
        )

    def test_PREG_HAS_SFX(self) -> None:
        self.assertRegex(
            "player.hasStatusEffect(StatusEffects.AIDS)",
            PREG_HAS_SFX,
        )

    def test_PREG_CREATE_SFX(self) -> None:
        self.assertRegex(
            "player.createStatusEffect(StatusEffects.Blind, 1 + rand(3), 0, 0, 0);",
            PREG_CREATE_SFX,
        )

    def test_PREG_RESET(self) -> None:
        self.assertRegex("public function reset():void {", PREG_RESET)

    def test_PREG_RESET_VAR(self) -> None:
        self.assertRegex("saveContent.shieldTaken = false;", PREG_RESET_VAR)

    def test_PREG_RESET_VAR_COMMENTED(self) -> None:
        self.assertRegex(
            "saveContent.state = 0; //-2 = abandoned; -1 = dead; 0 = unencountered; 1 = encounterable;",
            PREG_RESET_VAR_COMMENTED,
        )

    def test_PREG_DEBUG_VARS(self) -> None:
        sample = """private var debugVars:Object = {
    nephila: ["boolean", "Very low-quality, lore-breaking content that doesn't meet the mod's standards but ended up slipping through the cracks. It's left in the game only for historical reasons, and disabled by default.\n\nContains parasites, hyperpregnancy, silliness, a large number of typos, grammar issues, and likely bugs. Should not be considered canon. Requires parasites to also be enabled.\n\nEnabling this is not recommended."]
};
"""
        self.assertRegex(sample, PREG_DEBUG_VARS)

    def test_PREG_SSO_ID(self) -> None:
        sample = """public function get saveName():String {
    return "lumberjack";
}
"""
        self.assertRegex(sample, PREG_SSO_ID)

    def test_PREG_SSO_VERSION(self) -> None:
        sample = """public function get saveVersion():int {
    return 2;
}
"""
        self.assertRegex(sample, PREG_SSO_VERSION)

    def test_PREG_CONST(self) -> None:
        self.assertRegex("public const ASS:int = 0;", PREG_CONST)

    def test_PREG_AS3_LINECOMMENT(self) -> None:
        self.assertRegex("//a", PREG_AS3_LINECOMMENT)
        self.assertRegex("// bc", PREG_AS3_LINECOMMENT)

    def test_PREG_AS3_BLOCKCOMMENT(self) -> None:
        self.assertRegex("/*a*/", PREG_AS3_BLOCKCOMMENT)
        self.assertRegex("/* b*/", PREG_AS3_BLOCKCOMMENT)
        self.assertRegex("/* cd */", PREG_AS3_BLOCKCOMMENT)

    # REG_NPC_ID: Pattern = re.compile(r"ID *= *'([^']+)'")
    def test_PREG_NPCPY_NPC_ID(self)->None:
        self.assertRegex("ID = 'lumberjack'", PREG_NPCPY_NPC_ID)
    # REG_NPC_ALIASES: Pattern = re.compile(r"ALIASES *= *\[([^\]]+)\]")
    def test_PREG_NPCPY_NPC_ALIASES(self)->None:
        self.assertRegex("ALIASES = ['phylla']",PREG_NPCPY_NPC_ALIASES)
    # REG_NPC_CLS: Pattern = re.compile(r"class ([^\(]+)\(BaseNPC(,.+?)?\):")
    def test_PREG_NPCPY_NPC_CLS(self)->None:
        self.assertRegex("class AikoNPC(BaseNPC):", PREG_NPCPY_NPC_CLS)
    # REG_SSO_ID: Pattern = re.compile(r"super\(\)\.__init__\('([^']+)'\)")
    def test_PREG_NPCPY_SSO_ID(self)->None:
        self.assertRegex("super().__init__('aiko')", PREG_NPCPY_SSO_ID)
    # REG_SSO_CLS: Pattern = re.compile(r"class ([^\(]+)\(SelfSavingObject\):")
    def test_PREG_NPCPY_SSO_CLS(self)->None:
        self.assertRegex("class AikoSSO(SelfSavingObject):", PREG_NPCPY_SSO_CLS)

def run_my_tests(test_case):
    from rich.console import Console
    from rich.table import Table

    console = Console()
    case = unittest.TestLoader().loadTestsFromTestCase(test_case)
    result = unittest.TestResult()
    case(result)
    if result.wasSuccessful():
        return True
    else:
        console.print("Some tests failed!")
        t = Table("Test", "Result", title="Failed tests")
        for test, err in result.failures + result.errors:
            t.add_row(str(test), str(err))
        console.print(t)
        return False


if __name__ == "__main__":
    # run_my_tests(TestDevelopmentRegexPatterns)
    unittest.main()
