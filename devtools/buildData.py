import datetime
import os
from typing import List, Optional

from buildtools import log, os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.convert_data import EDataType
from buildtools.maestro.fileio import ReplaceTextTarget
from ruamel.yaml import YAML as Yaml

from coctweak_build.mods import getModVersions
from coctweak_build.targets.merge import EMergeDataType, MergeDataTarget
from coctweak_build.utils import get_files_in

yaml = Yaml(typ='rt')

def getCIBuildNumber() -> Optional[int]:
    if 'BUILD_NUMBER' in os.environ:
        return int(os.environ['BUILD_NUMBER'])
    return None

def add_data_buildtargets(bm: BuildMaestro, env: BuildMaestro, dependencies: List[str] = []) -> List[str]:
    ts = datetime.datetime.now(datetime.timezone.utc)
    tssimp = ts.strftime('%d%m%Y%H%M%S')
    modVersionData = getModVersions()
    modVersions = '{\n'
    for modID, modData in modVersionData.items():
        miargsv = [
            f'id={modID!r}',
        ]
        for k, v in modData.items():
            miargsv.append(f'{k}={v!r}')
        miargs = ', '.join(miargsv)
        modVersions += f'    {modID!r}: ModInfo({miargs}),\n'
    modVersions += '}'
    githash = os_utils.cmd_output(['git', 'rev-parse', 'HEAD'])[0].decode('utf-8').strip()
    cibuildnumber: int = getCIBuildNumber()
    bm.add(ReplaceTextTarget(
            outfile=os.path.join('coctweak', 'consts.py'),
            origfile=os.path.join('coctweak', 'consts.py.in'),
            replacements={
                '@@DTINFO@@': f'{ts!r} # {ts.isoformat()}',
                '@@DTINFOSIMP@@': f'{tssimp!r}',
                '@@MODVER@@': f'{modVersions}',
                '@@GITHASH@@': f'{githash!r}',
                '@@CIBUILDNUMBER@@': f'{cibuildnumber!r}',
            }))

    generated_targets = []
    for modid in [x.lower() for x in getModVersions().keys()]:
        for subj in ('statuseffects', 'perks', 'items', 'keyitems'):
            files = []
            gamedata = os.path.join('data', modid, f'{subj}.gamedata.json')
            if os.path.isfile(gamedata):
                log.info('Found Game Data at %s...', gamedata)
                files += [gamedata]
            coctweakdata = os.path.join('data', modid, f'{subj}.d')
            if os.path.isdir(coctweakdata):
                dfiles = get_files_in(coctweakdata)
                log.info('Found %d files in %s...', len(dfiles), coctweakdata)
                files += dfiles
            generated_targets.append(bm.add(MergeDataTarget(
                target=os.path.join('data', modid, f'{subj}-merged.min.json'),
                files=files,
                datatype=EMergeDataType.JSON,
                dependencies=dependencies)).target)
            # generated_targets.append(bm.add(MergeDataTarget(
            #     target=os.path.join('data', modid, f'{subj}-merged.pak'),
            #     files=files,
            #     datatype=EMergeDataType.MSGPACK,
            #     dependencies=dependencies)).target)

    return generated_targets


def main():
    env = os_utils.ENV.clone()
    bm = BuildMaestro(hidden_build_dir='.build/buildData_py/')
    add_data_buildtargets(bm, env)
    bm.as_app()

if __name__ == '__main__':
    main()
