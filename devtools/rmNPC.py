import filecmp
import glob
import os
from pathlib import Path
import sys
from typing import List

from buildtools import log, os_utils
from ruamel.yaml import YAML

MELD: str = os_utils.assertWhich('meld')

FLAVORS: List[str] = ['hgg']

MODULE_DIR = Path('coctweak')
SAVES_MOD_DIR = MODULE_DIR / 'saves'

DATA_DIR = Path('data')

def main():
    import argparse

    argp = argparse.ArgumentParser()
    argp.add_argument('flavor', choices=FLAVORS+['all'], default='all')
    argp.add_argument('npcids', type=str, nargs='*', default=['all'])
    args: argparse.Namespace = argp.parse_args()

    flavors = [args.flavor]
    if args.flavor.lower() in ('*', 'all'):
        flavors = FLAVORS

    npcids = []
    if len(args.npcids) > 0 and ('all' not in args.npcids):
        npcids += args.npcids
    else:
        for flavor in flavors:
            with log.info(f'{flavor}:'):
                for npcid in find_npcids(flavor):
                    nukeNPC(flavor, npcid)
        return

    with log.info(f'{args.flavor}:'):
        for npcid in npcids:
            nukeNPC(args.flavor, npcid)

def find_npcids(flavor: str) -> List[str]:
    npcids = set()
    for filepath in (SAVES_MOD_DIR / flavor.lower() / 'npcs').glob('*.py'):
        if filepath.name in ('__INDEX', '__init__'):
            continue
        if filepath.stem.endswith('.new'):
            continue
        suffix = ''.join(filepath.suffixes)
        if suffix in ('.py', '.new.py'):
            npcids.add(filepath.stem)
    return list(npcids)

def nukeNPC(flavor: str, npcid: str) -> None:
    yaml = YAML(typ='rt')
    with log.info(f'{npcid}:'):
        FLAVOR_DIR = SAVES_MOD_DIR / flavor.lower()
        NPCS_DIR = FLAVOR_DIR / 'npcs'
        oldnpcfile = NPCS_DIR / f'{npcid}.py'
        newnpcfile = NPCS_DIR / f'{npcid}.new.py'
        failure = False
        if oldnpcfile.is_file():
            log.info(f'RM {oldnpcfile}')
            oldnpcfile.unlink()
        if newnpcfile.is_file():
            log.info(f'RM {newnpcfile}')
            newnpcfile.unlink()
        SSO_DIR = FLAVOR_DIR / 'sso'
        oldssofile = SSO_DIR / f'{npcid}.py'
        newssofile = SSO_DIR / f'{npcid}.new.py'
        failure = False
        if oldssofile.is_file():
            log.info(f'RM {oldssofile}')
            oldssofile.unlink()
        if newssofile.is_file():
            log.info(f'RM {newssofile}')
            newssofile.unlink()

        FLAVOR_DATA_DIR = DATA_DIR / 'npcs' / flavor.lower()
        INDEX_YML = FLAVOR_DATA_DIR / f'__INDEX.yml'
        INDEX_YML_NEW = FLAVOR_DATA_DIR / f'__INDEX.yml.new'
        index_data: dict
        with INDEX_YML.open('r') as f:
            index_data = yaml.load(f)
        npcname = ''
        try:
            npcname = next(iter([x for x in index_data.keys() if x.lower() == npcid]))
        except StopIteration:
            log.info(f'{npcid} is not in {INDEX_YML}')
            return

        NPC_YML = FLAVOR_DATA_DIR / f'{npcname}.yml'
        if NPC_YML.is_file():
            log.info(f'RM {NPC_YML}')
            NPC_YML.unlink()

        del index_data[npcname]
        with INDEX_YML_NEW.open('w') as w, INDEX_YML.open('r') as f:
            n = 0
            for line in f:
                n += 1
                if line.startswith(f'{npcname}:'):
                    log.info(f'Discarded line {n} in {INDEX_YML}: {line!r}')
                    continue
                w.write(line)
        os.replace(INDEX_YML_NEW, INDEX_YML)
if __name__ == '__main__':
    main()
