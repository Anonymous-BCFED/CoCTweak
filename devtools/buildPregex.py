import ast
from pathlib import Path
import py_compile
import re
from typing import Dict, Final, List

from buildtools.maestro.base_target import SingleBuildTarget
from pregex.core.assertions import NotPrecededBy
from pregex.core.classes import Any as PregexAny
from pregex.core.classes import AnyBetween, AnyButFrom, AnyDigit, AnyFrom, AnyWhitespace
from pregex.core.groups import Backreference, Capture, Group
from pregex.core.operators import Either
from pregex.core.pre import Pregex
from pregex.core.quantifiers import Indefinite, OneOrMore
from pregex.core.quantifiers import Optional as PregexOptional
from regast.reparse import reparse_regex

from _common import writeGeneratedPythonWarningHeader
from _consts import SUPPORT_TABLE_END, SUPPORT_TABLE_START
from coctweak_build.modulebuilder import ModuleBuilder


class RegexConsts:
    FLAG_OPTIMIZE = 4096

    def __init__(self) -> None:
        self.vars: Dict[str, (Pregex, re.RegexFlag)] = {}

    def addExpr(
        self, constid: str, p: Pregex, multiline: bool = False, optimize: bool = True
    ) -> None:
        assert (
            constid not in self.vars.keys()
        ), f"{constid!r} already exists in RegexConsts."
        f = re.RegexFlag(0)
        if multiline:
            f |= re.MULTILINE
        if optimize:
            f |= self.FLAG_OPTIMIZE
        self.vars[constid] = (p, f)

    def to_file(
        self,
        filename: Path,
        purpose_header: str = "Pregenerated regular expressions",
        varname_re_compile: str = "__re_compile",
        varname_re_multiline: str = "__re_MULTILINE",
    ) -> None:
        mod = ModuleBuilder()
        mod.addImport(["re"])
        mod.addImportFrom("typing", ["Pattern"])
        all_ = ast.List(elts=[])
        for constid in sorted(self.vars.keys()):
            all_.elts.append(ast.Constant(constid))
        mod.addVariableDecl("__ALL__", None, all_)
        mod.addVariableDecl(
            varname_re_compile,
            None,
            ast.Attribute(
                value=ast.Name(id="re", ctx=ast.Load()),
                attr="compile",
            ),
        )
        mod.addVariableDecl(
            varname_re_multiline,
            None,
            ast.Attribute(value=ast.Name("re"), attr="MULTILINE"),
        )

        for constid, p_ in sorted(self.vars.items()):
            pattern, flags = p_
            f = ast.BoolOp(op=ast.Or(), values=[])
            if (flags & re.RegexFlag.MULTILINE) == re.RegexFlag.MULTILINE:
                f.values.append(ast.Name(varname_re_multiline))
            pstr = pattern.get_pattern()
            if (flags & self.FLAG_OPTIMIZE) == self.FLAG_OPTIMIZE:
                print('->',constid,repr(pstr))
                pstr = reparse_regex(pstr)
            args: List[ast.AST] = [ast.Constant(pstr)]
            if len(f.values) > 0:
                if len(f.values) == 1:
                    f = f.values[0]
                args.append(f)
            mod.addVariableDecl(
                name=constid,
                annotation=ast.Name(id="Pattern", ctx=ast.Load()),
                value=ast.Call(
                    func=ast.Name(varname_re_compile),
                    args=args,
                    keywords=[],
                ),
            )
        with filename.open("w") as f:
            writeGeneratedPythonWarningHeader(f, purpose_header, Path(__file__))
            f.write(mod.generate(False))
        py_compile.compile(filename)


class BuildGameRegexPy(SingleBuildTarget):
    BT_LABEL = "PREGEX"

    def __init__(self, targetfile: Path) -> None:
        self.targetpath = targetfile
        super().__init__(target=str(self.targetpath), files=[])

    def build(self) -> None:
        r = RegexConsts()
        # r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])'
        r.addExpr(
            "REG_ANSI_ESCAPE",
            Pregex("\x1B")
            + Either(
                (AnyBetween("@", "Z") | AnyBetween("\\", "_")),
                (
                    Pregex("[")
                    + Indefinite(AnyBetween("0", "?"))
                    + Indefinite(AnyFrom(" ", "/"))
                    + AnyBetween("@", "~")
                ),
            ),
        )

        r.to_file(
            self.targetpath,
            purpose_header="Pre-generated regular expression patterns for CoCTweak",
        )


class BuildDevRegexPy(SingleBuildTarget):
    BT_LABEL = "PREGEX"

    def __init__(self, targetfile: Path) -> None:
        self.targetpath = targetfile
        super().__init__(target=str(self.targetpath), files=[])

    def build(self) -> None:

        ws = OneOrMore(AnyWhitespace())
        iws = Indefinite(AnyWhitespace())

        keyword_class = Pregex("class")
        keyword_const = Pregex("const")
        keyword_function = Pregex("function")
        keyword_get = Pregex("get")
        keyword_if = Pregex("if")
        keyword_new = Pregex("new")
        keyword_private = Pregex("private")
        keyword_protected = Pregex("protected")
        keyword_public = Pregex("public")
        keyword_return = Pregex("return")
        keyword_static: Final[Pregex] = Pregex("static")
        keyword_var = Pregex("var")
        keyword_void = Pregex("void")

        const_kflags = Pregex("kFLAGS")
        type_string = Pregex("String")
        type_int = Pregex("int")

        hex_letters = AnyBetween("A", "F") | AnyBetween("a", "f") | AnyBetween("0", "9")

        privacy_modifier = Either(
            keyword_private,
            keyword_protected,
            keyword_public,
        )

        identifier_start = AnyBetween("a", "z") | AnyBetween("A", "Z") | "_"
        identifier_body = Indefinite(
            # Pregex("[0-9a-zA-Z_]", escape=False)
            AnyBetween("a", "z")
            | AnyBetween("A", "Z")
            | AnyBetween("0", "9")
            | "_"
        )
        identifier = identifier_start + identifier_body

        const_identifier_start = AnyBetween("A", "Z") | "_"
        const_identifier_body = Indefinite(
            # Pregex("[0-9A-Z_]", escape=False)
            AnyBetween("A", "Z")
            | AnyBetween("0", "9")
            | "_"
        )
        const_identifier = const_identifier_start + const_identifier_body

        tspec_start = AnyBetween("A", "Z") | AnyBetween("a", "z") | "_"
        tspec_end = Indefinite(
            # Pregex("[0-9A-Za-z_\.]", False)
            AnyBetween("0", "9")
            | AnyBetween("A", "Z")
            | AnyBetween("a", "z")
            | AnyFrom("_", ".")
        )
        typespec = tspec_start + tspec_end

        r = RegexConsts()

        # Not used currently (PGO)
        # FATAL: Error, failed to locate module 'pygit2.remote' you asked to include.
        r.addExpr(
            "PREG_FATAL_BAD_PACKAGE",
            Pregex("FATAL: Error, failed to locate module '")
            + Capture(OneOrMore(AnyButFrom("'")))
            + "' you asked to include.",
        )

        r.addExpr(
            "PREG_SUPPORT_TABLE_MARKERS",
            Either(Pregex(SUPPORT_TABLE_START), Pregex(SUPPORT_TABLE_END)),
        )

        r.addExpr(
            "PREG_CONSTS_PY_VERSION",
            Pregex("VERSION:")
            + iws
            + "Final[Version]"
            + iws
            + "="
            + iws
            + "Version('"
            + Capture(OneOrMore(AnyButFrom("'")))
            + "')",
        )

        # public var analLooseness:Number = 0;
        # REG_VAR = re.compile(r'(public|private|protected) var ([A-Za-z0-9_]+):([A-Za-z0-9\.\*]+)(?: *= *([^;]+))?;')
        r.addExpr(
            "PREG_VAR",
            Capture(privacy_modifier)
            + ws
            + keyword_var
            + ws
            + Capture(identifier)
            + ":"
            + Capture(typespec)
            + PregexOptional(Group(ws + "=" + ws + Capture(OneOrMore(AnyButFrom(";")))))
            + ";",optimize=False
        )
        # private static const SERIALIZATION_VERSION:int = 1;
        # REG_STATIC_CONST = re.compile(r"(public|private|protected) static const ([A-Z0-9_]+):([A-Za-z0-9\.\*]+)(?: *= *([^;]+))?;")
        r.addExpr(
            "PREG_STATIC_CONST",
            Capture(privacy_modifier)
            + ws
            + keyword_static
            + ws
            + keyword_const
            + ws
            + Capture(identifier)
            + ":"
            + Capture(typespec)
            + PregexOptional(Group(ws + "=" + ws + Capture(OneOrMore(AnyButFrom(";")))))
            + ";",
        )
        # public function serialize(relativeRootObject:*):void {
        # REG_SER_FUNC = re.compile(r"public function (serialize|saveToObject)\(([a-zA-Z0-9_]+):\*\):void \{")
        r.addExpr(
            "PREG_SER_FUNC",
            keyword_public
            + ws
            + keyword_function
            + ws
            + Capture(Either("serialize", "saveToObject"))
            + "("
            + Capture(identifier)
            + ":*):"
            + keyword_void
            + ws
            + "{",
        )
        # HGG/UEE (sic) public function currentSerializationVerison():int {
        # REG_SERVER_FUNC = re.compile(r"public function currentSerializationVerison\(\):int(?: {)?")
        r.addExpr(
            "PREG_SERVER_FUNC",
            keyword_public
            + ws
            + keyword_function
            + ws
            + "currentSerializationVerison()"
            + iws
            + PregexOptional(Pregex(":") + iws + type_int)
            + iws
            + PregexOptional("{"),
        )
        # UEE: public function serializationUUID():String
        # REG_SERUUID_FUNC = re.compile(r"public function serializationUUID\(\):String")
        r.addExpr(
            "PREG_SERUUID_FUNC",
            keyword_public
            + ws
            + keyword_function
            + ws
            + "serializationUUID()"
            + iws
            + ":"
            + iws
            + PregexOptional(Pregex(":") + iws + type_string)
            + iws
            + PregexOptional("{"),
        )

        # genEnums.py
        # public static const UNKNOWN_FLAG_NUMBER_00000:int                                   =    0;
        # common: public static (?:const|var) ([A-Za-z0-9_]+):([A-Za-z0-9_]+) += +
        public_static_constorvar = (
            keyword_public
            + ws
            + keyword_static
            + ws
            + Either(keyword_const, keyword_var)
            + ws
            + Capture(identifier)
            + iws
            + ":"
            + iws
            + Capture(typespec)
            + iws
            + "="
            + iws
        )

        # REG_CONST       = re.compile(r'public static (?:const|var) ([A-Za-z0-9_]+):([A-Za-z0-9_]+) += +(\d+);')
        r.addExpr(
            "PREG_PUBLIC_STATIC_INT",
            public_static_constorvar + Capture(OneOrMore(AnyDigit())) + ";",
        )

        # REG_CLASSY_ENUM = re.compile(r'public static (?:const|var) ([A-Za-z0-9_]+):([A-Za-z0-9_]+) += +new +\2\(([^\)]+)\);')
        r.addExpr(
            "PREG_CLASSY_ENUM",
            public_static_constorvar
            + keyword_new
            + ws
            + Backreference(2)
            + "("
            + Capture(OneOrMore(AnyButFrom(")")))
            + ");",
            optimize=False,  # BUG: UNHANDLED _unparse_pattern OP GROUPREF 2
        )
        # REG_ENUM        = re.compile(r'public static (?:const|var) ([A-Za-z0-9_]+):([A-Za-z0-9_]+) += +([^;]+);')
        r.addExpr(
            "PREG_ENUM",
            public_static_constorvar + Capture(OneOrMore(AnyButFrom(";"))) + ";",
        )

        # genNPCYAML.py
        # HGG_SELF_SAVING_SIG: Pattern = re.compile(r'function get saveName\(\)')
        r.addExpr(
            "PREG_HGG_SELF_SAVING_SIG",
            keyword_function + ws + keyword_get + ws + "saveName()",
        )

        # prebuildNPCs.py
        ##################
        initial_indent = Indefinite(AnyFrom(" ", "\t"))
        # // KFLAG_NAME:int = 123;
        # REG_KFLAG_DEF: Pattern = re.compile(r'^[ \t]*// +(?P<kflag>[A-Z][A-Z0-9_]+):int *= *(?P<value>\d+); *$')
        r.addExpr(
            "PREG_KFLAG_DEF",
            (
                initial_indent
                + "//"
                + iws
                + Capture(const_identifier, name="kflag")
                + ":"
                + iws
                + "int"
                + iws
                + "="
                + iws
                + Capture(OneOrMore(AnyDigit()), name="value")
                + ";"
                + iws
            )
            .match_at_line_start()
            .match_at_line_end(),
        )
        # // KFLAG_NAME:int = 123; // Some comment
        # REG_KFLAG_DEF_COMMENTED: Pattern = re.compile(r'^[ \t]*// +(?P<kflag>[A-Z][A-Z0-9_]+):int *= *(?P<value>\d+); *//(?P<comment>.*)$')
        r.addExpr(
            "PREG_KFLAG_DEF_COMMENTED",
            (
                initial_indent
                + "//"
                + iws
                + Capture(const_identifier, name="kflag")
                + ":int"
                + iws
                + "="
                + iws
                + Capture(OneOrMore(AnyDigit()), name="value")
                + ";"
                + iws
                + "//"
                + Capture(Indefinite(PregexAny()), name="comment")
            )
            .match_at_line_start()
            .match_at_line_end(),
        )
        # // kFLAGS.KFLAG_NAME
        # REG_KFLAG_REF: Pattern = re.compile(r"kFLAGS\.(?P<kflag>[A-Z][A-Z0-9_]+)")
        r.addExpr(
            "PREG_KFLAG_REF",
            const_kflags + "." + Capture(const_identifier, name="kflag"),
        )

        # REG_KFLAG_REF_BINARY_OP: Pattern = re.compile(r'flags\[kFLAGS\.(?P<kflag>[A-Z][A-Z0-9_]+)\] *(?P<op>(==|!=|>=|<=|\+=|-=|=|>|<)) *(?P<value>([0-9\.]+|0x[A-F0-9]+|"[^"]*"|true|false))')
        r.addExpr(
            "PREG_KFLAG_REF_BINARY_OP",
            Pregex("flags[")
            + const_kflags
            + "."
            + Capture(const_identifier, name="kflag")
            + "]"
            + iws
            + Capture(
                Either("==", "!=", ">=", "<=", "+=", "-=", "=", ">", "<", "===", "!=="),
                name="op",
            )
            + iws
            + Capture(
                Either(
                    OneOrMore(AnyDigit() | "."),
                    Pregex("0x") + OneOrMore(hex_letters),
                    Pregex('"') + Indefinite(AnyButFrom('"')) + '"',
                    Pregex("true"),
                    Pregex("false"),
                ),
                name="value",
            ),
        )
        # REG_KFLAG_REF_UNARY_OP: Pattern = re.compile(r"flags\[kFLAGS\.(?P<kflag>[A-Z][A-Z0-9_]+)\](?P<op>(\+\+|--))")
        r.addExpr(
            "PREG_KFLAG_REF_UNARY_OP",
            Pregex("flags[")
            + const_kflags
            + "."
            + Capture(const_identifier, name="kflag")
            + "]"
            + iws
            + Capture(Either("++", "--"), name="op"),
        )

        # REG_KFLAG_REF_IFCHK: Pattern = re.compile(r"if *\(flags\[kFLAGS\.(?P<kflag>[A-Z][A-Z0-9_]+)\]\)")
        r.addExpr(
            "PREG_KFLAG_REF_IFCHK",
            keyword_if
            + iws
            + "("
            + iws
            + "flags["
            + const_kflags
            + "."
            + Capture(const_identifier, name="kflag")
            + "]"
            + ")",
        )
        # new PregnancyStore(0, 0, kFLAGS.JOJO_BUTT_PREGNANCY_TYPE, kFLAGS.JOJO_EGGCUBATE_COUNT);
        # REG_KFLAG_REF_PREGSTORE: Pattern = re.compile(r"new PregnancyStore\((?P<vag_type>[^,]+), (?P<vag_incub>[^,]+), (?P<butt_type>[^,]+), (?P<butt_incub>[^,]+)\);")
        r.addExpr(
            "PREG_KFLAG_REF_PREGSTORE",
            keyword_new
            + ws
            + "PregnancyStore("
            + Capture(OneOrMore(AnyButFrom(",")), name="vag_type")
            + ","
            + iws
            + Capture(OneOrMore(AnyButFrom(",")), name="vag_incub")
            + ","
            + iws
            + Capture(OneOrMore(AnyButFrom(",")), name="butt_type")
            + ","
            + iws
            + Capture(OneOrMore(AnyButFrom(",")), name="butt_incub")
            + iws
            + ");",
        )
        # new BreastStore(kFLAGS.KATHERINE_BREAST_SIZE);
        # REG_KFLAG_REF_BREASTSTORE: Pattern = re.compile(r"=\ new\ BreastStore\((?P<breasts_flag>[^,]+)\);")
        r.addExpr(
            "PREG_KFLAG_REF_BREASTSTORE",
            Pregex("=")
            + ws
            + keyword_new
            + ws
            + "BreastStore("
            + Capture(OneOrMore(AnyButFrom(",")), name="breasts_flag")
            + ");",
        )

        # REG_HAS_SFX: Pattern = re.compile(r"player\.hasStatusEffect\(StatusEffects\.(?P<attr>[A-Za-z0-9_]+)\)")
        r.addExpr(
            "PREG_HAS_SFX",
            Pregex("player.hasStatusEffect(StatusEffects.")
            + Capture(
                identifier,
                name="attr",
            )
            + ")",
        )

        # REG_CREATE_SFX: Pattern = re.compile(r"player\.createStatusEffect\(StatusEffects\.(?P<attr>[A-Za-z0-9_]+),")
        # player.createStatusEffect(StatusEffects.Blind, 1 + rand(3), 0, 0, 0);
        r.addExpr(
            "PREG_CREATE_SFX",
            Pregex("player.createStatusEffect(StatusEffects.")
            + Capture(
                OneOrMore(identifier),
                name="attr",
            )
            + ",",
        )
        # REG_RESET: Pattern = re.compile(r"public +function +reset\(\): *void +{")
        r.addExpr(
            "PREG_RESET",
            keyword_public
            + ws
            + keyword_function
            + ws
            + "reset():"
            + iws
            + keyword_void
            + iws
            + "{",
        )

        # saveContent.rapePlay = 0; //0 = never done, 1 = continued impregnation, 2 = impregnation disabled
        # REG_RESET_VAR: Pattern = re.compile( r'(?<!\.)saveContent\.(?P<key>[a-zA-Z_][a-zA-Z0-9_]+) *= *(?P<value>([0-9\.]+|0x[A-F0-9]+|"[^"]*"|true|false));$' )
        r.addExpr(
            "PREG_RESET_VAR",
            NotPrecededBy(
                (
                    Pregex("saveContent.")
                    + Capture(identifier, name="key")
                    + iws
                    + "="
                    + iws
                    + Capture(
                        Either(
                            OneOrMore(Pregex("[0-9\.]", False)),
                            Pregex("0x") + OneOrMore(hex_letters),
                            Pregex('"') + Indefinite(AnyButFrom('"')) + '"',
                            Pregex("true"),
                            Pregex("false"),
                            Pregex("[]"),
                        ),
                        name="value",
                    )
                    + ";"
                ),
                Pregex("."),
            ).match_at_line_end(),
            multiline=True,
        )
        # REG_RESET_VAR_COMMENTED: Pattern = re.compile( r'(?<!\.)saveContent\.(?P<key>[a-zA-Z_][a-zA-Z0-9_]+) *= *(?P<value>([0-9\.]+|0x[A-F0-9]+|"[^"]*"|true|false)); *//(?P<comment>.*)$' )
        r.addExpr(
            "PREG_RESET_VAR_COMMENTED",
            NotPrecededBy(
                (
                    Pregex("saveContent.")
                    + Capture(identifier, name="key")
                    + iws
                    + "="
                    + iws
                    + Capture(
                        Either(
                            OneOrMore(AnyDigit() | "."),
                            Pregex("0x") + OneOrMore(hex_letters),
                            Pregex('"') + Indefinite(AnyButFrom('"')) + '"',
                            Pregex("true"),
                            Pregex("false"),
                            Pregex("[]"),
                        ),
                        name="value",
                    )
                    + ";"
                    + iws
                    + "//"
                    + iws
                    + Capture(Indefinite(PregexAny()), name="comment")
                ),
                Pregex("."),
            ).match_at_line_end(),
        )

        # private var debugVars:Object = {
        #     nephila: ["boolean", "Very low-quality, lore-breaking content that doesn't meet the mod's standards but ended up slipping through the cracks. It's left in the game only for historical reasons, and disabled by default.\n\nContains parasites, hyperpregnancy, silliness, a large number of typos, grammar issues, and likely bugs. Should not be considered canon. Requires parasites to also be enabled.\n\nEnabling this is not recommended."]
        # };
        # REG_DEBUG_VARS: Pattern = re.compile( r"private var debugVars:Object = ({[^;]+});", re.MULTILINE )
        r.addExpr(
            "PREG_DEBUG_VARS",
            privacy_modifier
            + ws
            + keyword_var
            + ws
            + "debugVars"
            + iws
            + ":"
            + iws
            + "Object"
            + iws
            + "="
            + iws
            + Capture(Pregex("{") + Indefinite(AnyButFrom(";")) + "}")
            + iws
            + ";",
            multiline=True,
        )

        # public function get saveName():String {
        #    return "lumberjack";
        # }
        # REG_SSO_ID: Pattern = re.compile( r'public function get saveName\(\): *String[\r\n\t ]*{[\r\n\t ]*return +"(?P<savename>[^"]+)";[\r\n\t ]*}', re.MULTILINE, )
        r.addExpr(
            "PREG_SSO_ID",
            keyword_public
            + ws
            + keyword_function
            + ws
            + keyword_get
            + ws
            + "saveName()"
            + iws
            + ":"
            + iws
            + type_string
            + iws
            + "{"
            + iws
            + keyword_return
            + ws
            + '"'
            + Capture(OneOrMore(AnyButFrom('"')), name="savename")
            + '";'
            + iws
            + "}",
            multiline=True,
        )
        # public function get saveVersion():int {
        #     return 2;
        # }
        # REG_SSO_VERSION: Pattern = re.compile( r"public function get saveVersion\(\): *int[\r\n\t ]*{[\r\n\t ]*return (?P<version>[^;]+);[\r\n\t ]*}", re.MULTILINE, )
        r.addExpr(
            "PREG_SSO_VERSION",
            keyword_public
            + ws
            + keyword_function
            + ws
            + keyword_get
            + ws
            + "saveVersion()"
            + iws
            + ":"
            + iws
            + type_int
            + iws
            + "{"
            + iws
            + keyword_return
            + ws
            + Capture(OneOrMore(AnyDigit()), name="version")
            + ";"
            + iws
            + "}",
            multiline=True,
        )
        # REG_CONST: Pattern = re.compile( r"public const ([A-Z0-9_]+): *[^=]+= *([^ ;]+);", re.MULTILINE )
        r.addExpr(
            "PREG_CONST",
            keyword_public
            + ws
            + keyword_const
            + ws
            + Capture(const_identifier)
            + iws
            + ":"
            + iws
            + OneOrMore(AnyButFrom("="))
            + "="
            + iws
            + Capture(OneOrMore(AnyButFrom(" ", ";")))
            + ";",
            multiline=True,
            optimize=False,  # BUG: UNHANDLED _parse_in OP NEGATE None
        )

        # REG_AS3_LINECOMMENT: Pattern = re.compile(r"//(.*)$")
        r.addExpr(
            "PREG_AS3_LINECOMMENT",
            (Pregex("//") + Capture(Indefinite(PregexAny()))).match_at_line_end(),
            multiline=True,
        )
        # REG_AS3_BLOCKCOMMENT: Pattern = re.compile(r"/\*(.+?)\*/")
        r.addExpr(
            "PREG_AS3_BLOCKCOMMENT",
            Pregex("/*") + Capture(OneOrMore(PregexAny(), is_greedy=False)) + "*/",
            optimize=False,  # BUG: UNHANDLED _unparse_pattern OP MIN_REPEAT (1, MAXREPEAT, [(ANY, None)])
        )

        # coctweak_build/indices.py
        # REG_NPC_ID: Pattern = re.compile(r"ID *= *'([^']+)'")
        r.addExpr(
            "PREG_NPCPY_NPC_ID",
            Pregex("ID")
            + iws
            + "="
            + iws
            + Either(
                Pregex("'") + Capture(OneOrMore(AnyButFrom("'"))) + "'",
                Pregex('"') + Capture(OneOrMore(AnyButFrom('"'))) + '"',
            ),
        )
        # REG_NPC_ALIASES: Pattern = re.compile(r"ALIASES *= *\[([^\]]+)\]")
        r.addExpr(
            "PREG_NPCPY_NPC_ALIASES",
            Pregex("ALIASES")
            + iws
            + "="
            + iws
            + "["
            + Capture(OneOrMore(AnyButFrom("]")))
            + "]",
        )
        # REG_NPC_CLS: Pattern = re.compile(r"class ([^\(]+)\(BaseNPC(,.+?)?\):")
        r.addExpr(
            "PREG_NPCPY_NPC_CLS",
            keyword_class
            + ws
            + Capture(identifier)
            + "(BaseNPC"
            + PregexOptional(
                Capture(Pregex(",") + iws + OneOrMore(PregexAny(), is_greedy=False))
            )
            + "):",
            optimize=False,  # BUG: UNHANDLED _unparse_pattern OP MIN_REPEAT (1, MAXREPEAT, [(ANY, None)])
        )

        # REG_SSO_ID: Pattern = re.compile(r"super\(\)\.__init__\('([^']+)'\)")
        r.addExpr(
            "PREG_NPCPY_SSO_ID",
            Pregex("super().__init__(")
            + Either(
                Pregex("'") + Capture(OneOrMore(AnyButFrom("'"))) + "'",
                Pregex('"') + Capture(OneOrMore(AnyButFrom('"'))) + '"',
            )
            + ")",
        )
        # REG_SSO_CLS: Pattern = re.compile(r"class ([^\(]+)\(SelfSavingObject\):")
        r.addExpr(
            "PREG_NPCPY_SSO_CLS",
            keyword_class + ws + Capture(identifier) + "(SelfSavingObject):",
        )

        r.to_file(
            self.targetpath,
            purpose_header="Pre-generated regular expression patterns for CoCTweak's build system",
        )


def main():
    from buildtools.maestro import BuildMaestro

    bm = BuildMaestro(".build/pregex")
    bm.add(BuildDevRegexPy(Path("devtools") / "_dev_regex.py"))
    bm.as_app()


if __name__ == "__main__":
    main()
