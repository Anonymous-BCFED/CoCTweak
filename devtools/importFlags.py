import collections
import os
from pathlib import Path
import re

from _dev_regex import PREG_PUBLIC_STATIC_INT, PREG_CLASSY_ENUM, PREG_ENUM
from _common import writeGeneratedPythonWarningHeader

from buildtools import log, os_utils
from buildtools.config import YAMLConfig
from buildtools.indentation import IndentWriter
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget

from coctweak_build.utils import downloadIfChanged

# public static const UNKNOWN_FLAG_NUMBER_00000:int

assert (
    PREG_PUBLIC_STATIC_INT.match(
        "public static const UNKNOWN_FLAG_NUMBER_00000:int                                   =    0;"
    )
    is not None
)
assert (
    PREG_CLASSY_ENUM.match(
        'public static const HUMAN:CockTypesEnum = new CockTypesEnum("human");'
    )
    is not None
)
assert PREG_ENUM.match('public static const STR:String = "str";') is not None


def searchForConsts(content, args: dict) -> dict:
    flags = collections.OrderedDict()
    conflicts = {}
    ln = 0
    for bline in content.splitlines():
        ln += 1
        m = PREG_PUBLIC_STATIC_INT.search(bline)
        if m is not None:
            startswith = args.get("startswith")
            if startswith is None or m.group(1).startswith(startswith):
                s = 0 if startswith is None else len(startswith)
                k = m.group(1)[s:]
                v = m.group(2)
                if k in flags.keys():
                    log.warning(
                        "searchForConsts: CONFLICT: line=%d, k=%r, v=%r", ln, k, v
                    )
                flags[k] = v
    return flags


def searchForConstsEnum(content, args: dict) -> dict:
    flags = collections.OrderedDict()
    i = 0
    reg = PREG_ENUM if not args.get("type") == "classy-enum" else PREG_CLASSY_ENUM
    for bline in content.splitlines():
        m = reg.search(bline)
        if m is not None:
            name = m[1]
            startswith = args.get("startswith")
            if startswith is not None and name.startswith(startswith):
                s = len(startswith)
                k = name[s:]
                v = str(i) if args.get("ignore-value", False) else m[3]
                if k in flags.keys():
                    log.warning(
                        "searchForConsts: CONFLICT: line=%d, k=%r, v=%r", i, k, v
                    )
                flags[k] = v
                i += 1
                continue
            include = args.get("include")
            if include is not None and name in include:
                # print(m[1])
                k = name
                v = str(i) if args.get("ignore-value", False) else m[3]
                if k in flags.keys():
                    log.warning(
                        "searchForConsts: CONFLICT: line=%d, k=%r, v=%r", i, k, v
                    )
                flags[k] = v
                i += 1
                continue
            include_all = args.get("include-all", False)
            if include_all:
                # print(m[1])
                k = name
                v = str(i) if args.get("ignore-value", False) else m[3]
                if k in flags.keys():
                    log.warning(
                        "searchForConsts: CONFLICT: line=%d, k=%r, v=%r", i, k, v
                    )
                flags[k] = v
                i += 1
                continue
    return flags


class GenerateEnumsTarget(SingleBuildTarget):
    BT_LABEL = "GEN ENUMS"

    def __init__(self, target, dependencies=[]):
        enums_yml = os.path.join("data", "enums.yml")
        self.enums = YAMLConfig(enums_yml).cfg
        files = [enums_yml]
        for clsID, data in self.enums.items():
            uri = data["url"]
            if uri is not None and os.path.isfile(uri):
                files += [uri]

        super().__init__(target=target, files=files, dependencies=dependencies)

    def build(self):
        for clsID, data in self.enums.items():
            with log.info("%s:", clsID):
                url = data["url"]
                destfilename = data["dest"]
                content = downloadIfChanged(url, cache_content=True)
                log.info("Generating code...")
                flags = {}
                if data.get("type") in ("classy-enum", "enum"):
                    flags = searchForConstsEnum(content, data)
                else:
                    flags = searchForConsts(content, data)
                # print(repr(flags))
                for k, v in data.get("inject", {}).items():
                    flags[str(k)] = str(v)
                for k in data.get("ignore", []):
                    if k in flags:
                        del flags[str(k)]
                with open(destfilename, "w") as f:
                    w = IndentWriter(f, indent_chars="    ")
                    # w.writeline('# @GENERATED '+datetime.datetime.now().isoformat())
                    # w.writeline(f"# @GENERATED from {url}")
                    writeGeneratedPythonWarningHeader(f, 'Generated enum', Path(__file__), url)
                    superclsspec = "enum:Enum"
                    if data.get("ignore-value", False):
                        superclsspec = "enum:IntEnum"
                    superclsspec = data.get("superclass", superclsspec)
                    imports = data.get("imports", [])
                    for import_spec in [superclsspec] + imports:
                        entry = import_spec.split(":")
                        if len(entry) == 1:
                            imports += [f"import {entry[0]}"]
                        elif len(entry) >= 2:
                            if "," in entry[1]:
                                entry += [x.strip() for x in entry[1].split(",")]
                            if len(entry) > 2:
                                entry[1] = ", ".join(entry[1:])
                                entry[1] = f"({entry[1]})"
                            w.writeline(f"from {entry[0]} import {entry[1]}")
                    w.writeline("")
                    w.writeline(f"__all__ = ['{clsID}']")
                    w.writeline("")
                    supercls = superclsspec.split(":")[-1]
                    with w.writeline(f"class {clsID}({supercls}):"):
                        maxlen = max([len(x) for x in flags.keys()])
                        for k, v in flags.items():
                            padding = " " * (maxlen + 2 - len(k))
                            try:
                                int(v)
                                w.writeline(f"{k}{padding}= {v}")
                            except:
                                w.writeline(f"#{k}{padding}= {v}")
                                pass


def add_gen_enums_targets(bm: BuildMaestro, env, deps=[]):
    return [
        bm.add(
            GenerateEnumsTarget(
                os.path.join(bm.builddir, "GEN_ENUMS.tgt"), dependencies=deps
            )
        ).target
    ]


def main():
    env = os_utils.ENV.clone()
    bm = BuildMaestro(hidden_build_dir=".build/genEnums_py/")
    add_gen_enums_targets(bm, env, [])
    bm.as_app()


if __name__ == "__main__":
    main()
