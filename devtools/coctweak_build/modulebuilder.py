import ast
from typing import Any, Iterable, List, Optional, Union

import astor
import autopep8
import isort
from astor.source_repr import split_lines


def value2ast(value: Any) -> ast.AST:
    if isinstance(value, (int, float, str, bytes)):
        return ast.Constant(value=value, kind=None)
    if isinstance(value, ast.AST):
        return value
    return None


class ModuleBuilder:
    def __init__(self) -> None:
        self.imports: List[Union[ast.Import, ast.ImportFrom]] = []
        self.expressions: List[ast.AST] = []
        # self.module = ast.Module()

    def addImport(self, names: Iterable[str]) -> None:
        if isinstance(names, str):
            names = [names]
        self.imports.append(ast.Import(
            names=[ast.alias(name=x, asname=None) for x in names]))

    def addImportFrom(self, module: str, names: Iterable[str]) -> None:
        if isinstance(names, str):
            names = [names]
        self.imports.append(ast.ImportFrom(module=module, names=[
                            ast.alias(name=x, asname=None) for x in names], level=0))

    def addVariableDecl(self, name: str, annotation: Optional[ast.AST], value: Any) -> None:
        if annotation:
            self.expressions.append(
                ast.AnnAssign(target=ast.alias(name, asname=None),
                              value=value2ast(value),
                              annotation=annotation,
                              simple=0))
        else:
            self.expressions.append(
                ast.Assign(targets=[ast.alias(name, asname=None)],
                           value=value2ast(value),
                           type_comment=None,
                           simple=0))

    def generate(self, cleanup_code: bool = True) -> str:
        body = []
        for imp in self.imports:
            body.append(imp)
        for x in self.expressions:
            body.append(x)
        mod = ast.Module(body=body)
        # modcode = astunparse.unparse(mod)

        def pretty_source(source):
            return ''.join(split_lines(source, maxline=65535))
        modcode = astor.to_source(mod, pretty_source=pretty_source)
        if cleanup_code:
            isortconfig = isort.Config(line_length=200)
            modcode = isort.code(modcode, config=isortconfig)
            return autopep8.fix_code(modcode, options={
                'max_line_length': 200,
                'aggressive': 2,
            })
        return modcode
