import os, sys, hashlib, string, collections, re
from typing import Pattern
import requests
import pygit2
from typing import List

from buildtools import log, os_utils
ALPHABET = string.digits + string.ascii_letters
ALPHALEN = len(ALPHABET)

def b62e(number: int, pad=2) -> str:
    """Converts an integer to a base36 string."""
    if not isinstance(number, int):
        raise TypeError('number must be an integer')

    base62 = ''
    sign = ''

    if number < 0:
        sign = '-'
        number = -number

    if 0 <= number < ALPHALEN:
        base62 = sign + ALPHABET[number]
    else:
        while number != 0:
            number, i = divmod(number, ALPHALEN)
            base62 = ALPHABET[i] + base62

    if pad > 0:
        base62 = base62.rjust(pad, ALPHABET[0])

    return sign + base62

def b62d(number: str) -> int:
    o = 0
    for c in number:
        o *= ALPHALEN
        o += ALPHABET.index(c)
    return o

def base62encode(b: bytes) -> str:
    # 14776335 e1780f ZZZZ
    # So maxlen = 2
    o = ''
    for i in range(0, len(b), 2):
        chunk = ''.join([hex(x)[2:].zfill(2) for x in b[i:i+2]])
        o += b62e(int(chunk, 16), pad=3)
    return o

def base62decode(data: str) -> bytes:
    buff = []
    b = b''
    for i in range(0, len(data), 3):
        chunk = data[i:i+3]
        print(chunk)
        a = bytes(b62d(chunk))
        print(a)
        b += a
    return b

'''
print('0x0000', b46e(0))
print('0xFFFF', b46e(0xFFFF))
print('0', hex(b46d('0')))
print('h31', hex(b46d('h31')))
print(base62encode(bytes([0,1,2,3,4,5])))
print(base62decode('00108j0gB'))

i = 0
while True:
    b46 = base62encode(i)
    h = hex(i)[2:]
    print(i, h, b46)
    if b46 == ALPHABET[-1]*4:
        break
    i += 1
'''

def downloadIfChanged(uri, cache_content=False):
    urlmd5 = base62encode(hashlib.sha256(uri.encode('utf-8')).digest())
    lmpath = os.path.join('.cache', urlmd5+'.lastmod')
    etpath = os.path.join('.cache', urlmd5+'.etag')
    contentpath = os.path.join('.cache', urlmd5+'.content')

    lastmod = ''
    etag = ''
    if not os.path.isdir('.cache'):
        os.mkdir('.cache')
    headers = {}
    if os.path.isfile(lmpath) and (cache_content and os.path.isfile(contentpath)):
        with open(lmpath,'r') as f:
            headers['If-Modified-Since'] = lastmod = f.read().strip()
    elif os.path.isfile(etpath) and (cache_content and os.path.isfile(contentpath)):
        with open(etpath,'r') as f:
            headers['If-None-Match'] = etag = f.read().strip()
    with log.info('GET %s', uri):
        res = requests.get(uri, headers={'If-Modified-Since':lastmod})
        #with log.info('Response headers:'):
        #    for k,v in res.headers.items():
        #        log.info('%s: %s',k,v)
        if res.status_code == 304:
            log.info('304 - Not Modified')
            if not cache_content:
                return None
            with open(contentpath, 'r') as f:
                return f.read()
        else:
            res.raise_for_status()
    if os.path.isfile(lmpath):
        os.remove(lmpath)
    if os.path.isfile(etpath):
        os.remove(etpath)
    if cache_content:
        if 'Last-Modified' in res.headers.keys():
            with open(lmpath, 'w') as f:
                f.write(res.headers.get('Last-Modified',lastmod))
        if 'ETag' in res.headers.keys():
            with open(etpath, 'w') as f:
                f.write(res.headers.get('ETag',etag))
        with open(contentpath, 'w') as f:
            f.write(res.text)
    return res.text

def update(d, u):
    for k, v in u.items():
        dv = d.get(k, {})
        #if not isinstance(dv, collections.Mapping):
        if not isinstance(dv, dict):
            d[k] = v
        #elif isinstance(v, collections.Mapping):
        elif isinstance(v, dict):
            d[k] = update(dv, v)
        else:
            d[k] = v
    return d

def get_files_in(path: str) -> List[str]:
    return sorted(os_utils.get_file_list(path, prefix=path))

REG_HGG_VERSION = re.compile(r'public static const saveVersion:String = "hgg ([^"]+)";')
def getHGGVersion(rootpath: str) -> str:
    return _getVersion(rootpath, REG_HGG_VERSION)

REG_UEE_VERSION = re.compile(r'ver = "\d+.\d+.\d+_mod_([^"]+)";')
def getUEEVersion(rootpath: str) -> str:
    return _getVersion(rootpath, REG_UEE_VERSION)

REG_VANILLA_VERSION = re.compile(r'ver = "([^"]+)";')
def getVanillaVersion(rootpath: str) -> str:
    return _getVersion(rootpath, REG_VANILLA_VERSION)

def _getVersion(rootpath: str, reg_version: Pattern) -> str:
    content = ''
    with open(os.path.join(rootpath, 'classes','classes','CoC.as'), 'r') as f:
        content = f.read()
    for line in content.splitlines():
        m = reg_version.search(line)
        if m is not None:
            return m.group(1)
    return '???'


def getCommitOf(repopath: str) -> str:
    repo = pygit2.Repository(repopath)
    head = repo.head
    head = repo[head.target]
    return head.hex
