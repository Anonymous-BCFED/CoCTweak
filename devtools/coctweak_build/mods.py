import os
from typing import Any, Dict
from ruamel.yaml import YAML

yaml = YAML(typ='rt')

from coctweak_build.urlgenerators import getURLGeneratorByID
from coctweak_build.utils import getHGGVersion, getUEEVersion, getCommitOf, getVanillaVersion

def getModVersions() -> Dict[str, Any]:
    modVersions = {}
    versionTypes = {
        'vanilla': getVanillaVersion,
        'uee': getUEEVersion,
        'hgg': getHGGVersion,
    }

    with open(os.path.join('data', 'coc_mods.yml'), 'r') as f:
        for k,v in yaml.load(f).items():
            dirname = os.path.join(*v['submodule'])
            urlgen = getURLGeneratorByID(v['repo']['type'])()
            urlgen.load(v['repo'])
            modVersions[k] = {
                'version': versionTypes[v['version-type']](dirname),
                'commit': getCommitOf(dirname),
                'urlgen': urlgen,
            }
    return modVersions

PREFIXES: dict = None
def _loadPrefixes():
    global PREFIXES
    if PREFIXES is None:
        PREFIXES = {}
        with open(os.path.join('data', 'coc_mods.yml'), 'r') as f:
            for k,v in yaml.load(f).items():
                PREFIXES[k] = os.path.join(*v['submodule'])
def replacePathWithPrefix(path: str) -> str:
    _loadPrefixes()
    for modid, modpath in PREFIXES.items():
        if path.startswith(modpath):
            return f'[{modid}]'+path[len(modpath):]
    return path
