import enum
import json
from typing import List
import msgpack
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.convert_data import EDataType
from ruamel.yaml import YAML
import toml
yaml = YAML(typ='rt')

from coctweak_build.utils import update


class MergeStrategy(enum.IntEnum):
    ADD = enum.auto()
    REPLACE = enum.auto()

class EMergeDataType(enum.IntEnum):
    JSON=0
    YAML=1
    TOML=2
    MSGPACK=3
class MergeDataTarget(SingleBuildTarget):
    BT_LABEL = 'MERGE'

    def __init__(self, target: str, files: List[str], datatype: EMergeDataType, dependencies: List[str] = []):
        self.type: EMergeDataType = datatype
        super().__init__(target, files, dependencies)

    def mergeDict(self, a: dict, b: dict):
        strat = MergeStrategy[b.pop('@merge-strategy', 'add').upper()]
        if strat == MergeStrategy.REPLACE:
            return b
        return update(a, b)
        '''
        o = {}
        for k in sorted(list(set(list(a.keys())+list(b.keys())))):
            if k in b and k in a:
                if isinstance(a[k], dict) and isinstance(b[k], dict):
                    o[k] = self.mergeDict(a[k], b[k])
                    continue
                elif isinstance(a[k], list) and isinstance(b[k], list):
                    o[k] = self.mergeList(a[k], b[k])
                    continue
            elif k in a and k not in b:
                o[k]=a[k]
            elif k not in a and k in b:
                o[k]=b[k]
        return o
        '''
    def mergeList(self, a: list, b: list):
        strat = MergeStrategy.ADD
        dedupe = True
        sort = True
        newb = []
        for item in b:
            if isinstance(item, dict):
                k,v = list(item.items())[0]
                if k == '@merge-strategy':
                    strat = MergeStrategy(v.upper())
                    continue
                elif k == '@merge-duplicate':
                    dedupe = v
                    continue
                elif k == '@merge-sort':
                    sort = v
                    continue
            newb += [item]
        b = newb
        if strat == MergeStrategy.REPLACE:
            o = []
            alen = len(a)
            blen = len(b)
            maxlen = max(alen, blen)
            minlen = min(alen, blen)
            for i in range(maxlen):
                if minlen >= i:
                    break
                if isinstance(a[i], dict) and isinstance(b[i], dict):
                    o += [self.mergeDict(a[i], b[i])]
                    continue
                if isinstance(a[i], list) and isinstance(b[i], list):
                    o += [self.mergeList(a[i], b[i])]
                    continue
                o += [b[i]]
        elif  strat == MergeStrategy.ADD:
            o = (a+b)
        if dedupe:
            o = list(set(a+b))
        return sorted(o) if sort else o

    def build(self):
        o = {}
        for filename in self.files:
            data = {}
            with open(filename, 'r') as f:
                if filename.endswith('.json'):
                    data = json.load(f)
                if filename.endswith('.yml'):
                    data = yaml.load(f)
            assert isinstance(data, dict), filename
            o = self.mergeDict(o, data)
        match self.type:
            case EMergeDataType.YAML:
                with open(self.target, 'w') as f:
                    yaml.dump(o, f)
            case EMergeDataType.JSON:
                with open(self.target, 'w') as f:
                    json.dump(o, f, separators=(',', ':'))
            case EMergeDataType.TOML:
                with open(self.target, 'w') as f:
                    toml.dump(o, f)
            case EMergeDataType.MSGPACK:
                with open(self.target, 'wb') as f:
                    msgpack.dump(o, f)
            case _:
                raise Exception(f'Unknown target type {self.type}')
