import markdown, os, re

import jinja2

from buildtools.maestro.base_target import SingleBuildTarget

jenv = jinja2.Environment(loader=jinja2.FileSystemLoader('docs-src'))

class BuildMarkdown(SingleBuildTarget):
    BT_LABEL = 'MARKDOWN'
    def __init__(self, target, filename, title: str = 'Untitled', jsfile: str = '', cssfile: str = '', dependencies=[]):
        self.infile: str = filename
        self.outfile: str = target
        self.title: str = title

        self.jsfile = jsfile
        self.cssfile = cssfile

        self.currentSection=0
        self.currentInstruction=0

        super().__init__(target=target, files=[filename], dependencies=dependencies)

    def get_config(self) -> dict:
        return {
            'title': self.title
        }

    def handleCheckbox(self, m) -> str:
        chkid=f'sec_{self.currentSection}_{self.currentInstruction}'
        self.currentInstruction += 1
        return f'<li><input type="checkbox" id="{chkid}"><label for="{chkid}">{m.group(1)}</label>'+m.group(2)

    def handleStartScript(self, m) -> str:
        return f'<section class="script"><span class="script-controls"><button class="begin-script" data-section="{self.currentSection}">Begin</button></span>'
    def handleEndScript(self, m) -> str:
        return f'</section>'

    def build(self) -> None:
        mdcode = ''
        jscode = ''
        csscode = ''

        with open(self.infile, 'r') as f:
            mdcode = f.read()

        if self.jsfile != '':
            with open(self.jsfile, 'r') as f:
                jscode = f.read()
        if self.cssfile != '':
            with open(self.cssfile, 'r') as f:
                csscode = f.read()

        mdcode = markdown.markdown(mdcode, extensions=['toc'], output_format='html5', tab_length=2)

        newmdcode = ''
        self.currentSection = 0
        for line in mdcode.splitlines():
            m = re.match(r'^<h\d>.*', line)
            if m is not None:
                self.currentSection += 1
                self.currentInstruction=0
            line = re.sub(r'<li> *\[ \](.*)(<(/li|ul|ol)>)$', self.handleCheckbox, line)
            line = re.sub(r'<p>\{Start Script\}</p>', self.handleStartScript, line)
            line = re.sub(r'<p>\{End Script\}</p>', self.handleEndScript, line)
            newmdcode += line+'\n'
        mdcode=newmdcode
        with open(self.outfile, 'w') as f:
            f.write(jenv.get_template('rendered.jinja2.html').render(title=self.title, body=mdcode, js=jscode, css=csscode))

def renderDoc(bm, basename, title, jsfile='',cssfile='',deps=[]):
    return bm.add(BuildMarkdown(os.path.join('docs', basename+'.html'), os.path.join('docs-src', basename+'.md'), title,
                  dependencies=deps,
                  jsfile=jsfile,
                  cssfile=cssfile))
