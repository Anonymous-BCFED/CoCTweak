VARIABLES = {
    'HGG_IN_DIR':     'coc/hgg',
    'UEE_IN_DIR':     'coc/uee',
    'VANILLA_IN_DIR': 'coc/vanilla',

    'COMMON_BODYPARTS':  'coctweak/saves/common/bodyparts/_serialization',
    'HGG_BODYPARTS':     'coctweak/saves/hgg/bodyparts/_serialization',
    'UEE_BODYPARTS':     'coctweak/saves/uee/bodyparts/_serialization',
    'VANILLA_BODYPARTS': 'coctweak/saves/vanilla/bodyparts/_serialization',

    'COMMON_DIR':  'coctweak/saves/common/_serialization',
    'HGG_DIR':     'coctweak/saves/hgg/_serialization',
    'UEE_DIR':     'coctweak/saves/uee/_serialization',
    'VANILLA_DIR': 'coctweak/saves/vanilla/_serialization',
}
