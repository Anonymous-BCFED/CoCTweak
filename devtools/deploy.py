import json
import os
import re
from typing import Any
import gitlab
import argparse
from pathlib import Path

from ruamel.yaml import YAML
from buildtools import os_utils, http, utils
from mega import Mega

# from jenkins import Jenkins


PLATFORMS = [
    ("linux", "tar.gz"),
    ("windows", "zip"),
]

POSIX_DEFAULT_CFG = Path("/etc/python-gitlab.cfg")
NT_DEFAULT_CFG = Path(os.environ["HOME"]) / "python-gitlab.cfg"
DEFAULT_CFG = str(Path.home() if os.name == "nt" else POSIX_DEFAULT_CFG)

yaml = YAML(typ="safe", pure=True)

"""
FORMAT:

# VERSION - Subtitle

blah

## Compatibility

<!--...-->

## Big Stuff

<!--...-->
"""

with Path(".deploy.yml").open("r") as f:
    cfg = yaml.load(f)

argp = argparse.ArgumentParser()
argp.add_argument("version", type=str, help="Tag of the release")
argp.add_argument("subtitle", type=str, help="Subtitle of the release")
argp.add_argument(
    "--dry-run",
    action="store_true",
    default=False,
    help="Only go through the motions.  Do not actually deploy.",
)
args = argp.parse_args()

assert re.match(r"\d+\.\d+\.\d+", args.version) is not None, "version must be M.m.p"

with open("RELEASE_NOTES.md", "r") as f:
    RELEASE_NOTES = f.read()

JENKINS_SCHEME: str = cfg["jenkins"]["scheme"]
JENKINS_HOST: str = cfg["jenkins"]["hostname"]
JENKINS_PORT: int = int(cfg["jenkins"]["port"])
JENKINS_USERNAME: str = cfg["jenkins"]["username"]
JENKINS_PASSWORD: str = cfg["jenkins"]["password"]
JENKINS_JOB: str = cfg["jenkins"]["job"]
JENKINS_BASE: str = f"{JENKINS_SCHEME}://{JENKINS_HOST}:{JENKINS_PORT}"

MEGA_EMAIL = cfg["mega"]["email"]
MEGA_PASSWORD = cfg["mega"]["password"]

GITLAB_ID = cfg["gitlab"]["id"]
GITLAB_PROJECT_ID = int(cfg["gitlab"]["project"])

os_utils.ensureDirExists(os.path.join("tmp", "dist"))

mega = Mega()
if not args.dry_run:
    print("Logging into mega.nz...")
    mega.login(MEGA_EMAIL, MEGA_PASSWORD)

# print('Logging into jenkins...')
# jenkins = Jenkins(JENKINS_BASE, username=JENKINS_USERNAME, password=JENKINS_PASSWORD)

MEGA_COCTWEAK_FOLDER: Any
if not args.dry_run:
    MEGA_COCTWEAK_FOLDER = mega.find("coctweak")
files = []
# http://xxxxx:8080/job/coctweak-release/lastSuccessfulBuild/artifact/archives/coctweak-linux-amd64-0.2.2.tar.gz
for platform, ext in PLATFORMS:
    destfile = f"tmp/dist/coctweak-{platform}-amd64-{args.version}.{ext}"
    # http.DownloadFile(f'{JENKINS_BASE}/job/{JENKINS_JOB}/lastSuccessfulBuild/artifact/archives/coctweak-{platform}-amd64-{args.version}.{ext}', destfile, auth=(JENKINS_USERNAME, JENKINS_PASSWORD))
    uri = f"{JENKINS_BASE}/job/{JENKINS_JOB}/lastSuccessfulBuild/Agent={platform}/artifact/archives/coctweak-{platform}-amd64-{args.version}.{ext}"
    http.DownloadFile(uri, destfile, auth=(JENKINS_USERNAME, JENKINS_PASSWORD))
    sz = os.path.getsize(destfile)
    _hash = utils.sha256sum(destfile)
    assert _hash is not None, f"SHA256 of {destfile} is None!"
    if not args.dry_run:
        md = mega.find(f"coctweak/coctweak-{platform}-amd64-{args.version}.{ext}")
        if md:
            print("Deleting duplicate...")
            mega.delete(md[0])
    print(f"Uploading {destfile} to coctweak...")
    md = (
        mega.upload(destfile, dest=MEGA_COCTWEAK_FOLDER[0])
        if not args.dry_run
        else None
    )
    files.append(
        [
            f"coctweak-{platform}-amd64-{args.version}.{ext}",
            sz,
            _hash,
            mega.get_upload_link(md) if not args.dry_run else "about:invalid",
        ]
    )


RELEASE_NOTES += "\n## Binary Builds\n"
RELEASE_NOTES += (
    "\nUse these if you're a Windows pleb or someone who can't work Linux.\n"
)
RELEASE_NOTES += "| Filename | Size | SHA256 |\n"
RELEASE_NOTES += "|---:|---|---|\n"
links = []
for filename, sz, _hash, link in files:
    humansz = os_utils.sizeof_fmt(sz)
    RELEASE_NOTES += (
        f"| [{filename}]({link}) | {humansz} ({sz}B) | <code>{_hash}</code> |\n"
    )
    links += [{"name": filename, "url": link, "type": "other"}]

if args.dry_run:
    print(RELEASE_NOTES)
    print(json.dumps({"assets": {"links": links}}))
else:
    gl = gitlab.Gitlab.from_config(GITLAB_ID, [DEFAULT_CFG])
    print(f"Logging into gitlab...")
    gl.auth()

    print(f"Fetching project...")
    project = gl.projects.get(GITLAB_PROJECT_ID)
    print(f"Creating release...")
    project.releases.create(
        {
            "name": f"v{args.version} - {args.subtitle}",
            "tag_name": f"v{args.version}",
            "description": RELEASE_NOTES,
            "assets": {"links": links},
        }
    )
print(f"DONE")
