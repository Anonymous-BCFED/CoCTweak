import ast
import pickle
import json
from pathlib import Path
from pprint import pprint
import sys
from ruamel.yaml import YAML
import argparse

def existingPath(inp: str) -> Path:
    p=Path(inp)
    if not p.is_file():
        raise FileNotFoundError(p)
    return p


def yaml_set_compact(yaml: YAML) -> None:
    yaml.default_flow_style = True
    yaml.compact(True, True)

def main():
    argp = argparse.ArgumentParser()
    inp = argp.add_argument_group('input')
    inp.add_argument('--json-in', type=existingPath, nargs='?', default=None)
    inp.add_argument('--pickle-in', type=existingPath, nargs='?', default=None)
    inp.add_argument('--py-in', type=existingPath, nargs='?', default=None)
    inp.add_argument('--yaml-in', type=existingPath, nargs='?', default=None)

    outp = argp.add_argument_group('output')
    outp.add_argument('--to-json', type=Path, nargs='?', default=None)
    outp.add_argument('--to-pickle', type=Path, nargs='?', default=None)
    outp.add_argument('--to-py', type=Path, nargs='?', default=None)
    outp.add_argument('--to-yaml', type=Path, nargs='?', default=None)

    argp.add_argument('--compress', action='store_true', default=False)

    args = argp.parse_args()
    
    yaml = YAML(typ='rt', pure=True)

    data: dict = {}
    if args.json_in is not None:
        with open(args.json_in, 'r') as f:
            data = json.load(f)
    elif args.yaml_in is not None:
        with open(args.yaml_in, 'r') as f:
            data = yaml.load(f)
    elif args.py_in is not None:
        with open(args.py_in, 'r') as f:
            data = ast.literal_eval(f.read())
    elif args.pickle_in is not None:
        with open(args.pickle_in, 'rb') as f:
            data = pickle.load(f)
    else:
        print('Some sort of input is required.')
        return

    if args.to_json is not None:
        with open(args.to_json, 'w') as f:
            json.dump(data, f, indent=None if args.compress else 2, separators=(',',':') if args.compress else (', ', ': '))
    elif args.to_pickle is not None:
        with open(args.to_pickle, 'wb') as f:
            pickle.dump(data, f)
    elif args.to_py is not None:
        with open(args.to_py, 'w') as f:
            if args.compress:
                f.write(str(data))
            else:
                pprint(data, f)
    elif args.to_yaml is not None:
        with open(args.to_yaml, 'w') as f:
            if args.compress:
                yaml_set_compact(yaml)
            yaml.dump(data, f)
    else:
        print('Some sort of output is required.')
        return

if __name__ == '__main__':
    main()