import os
from typing import List

from buildtools import os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget

from coctweak_build.indices import buildNPCIndex, buildSSOIndex


class GenNPCIndexTarget(SingleBuildTarget):
    BT_LABEL = "GENERATE"

    def __init__(self, dirname: str, dependencies=[]) -> None:
        self.dirname: str = dirname
        super().__init__(
            target=os.path.join(dirname, "__init__.py"),
            files=os_utils.get_file_list(dirname),
            dependencies=dependencies,
        )

    def build(self) -> None:
        buildNPCIndex(self.dirname)


class GenSSOIndexTarget(SingleBuildTarget):
    BT_LABEL = "GENERATE"

    def __init__(self, dirname: str, dependencies=[]) -> None:
        self.dirname: str = dirname
        super().__init__(
            target=os.path.join(dirname, "__init__.py"),
            files=os_utils.get_file_list(dirname),
            dependencies=dependencies,
        )

    def build(self) -> None:
        buildSSOIndex(self.dirname)


def add_gen_indices_targets(bm: BuildMaestro, env, deps=[]) -> List[str]:
    return [
        bm.add(
            GenNPCIndexTarget(os.path.join("coctweak", "saves", "hgg", "npcs"))
        ).target,
        bm.add(
            GenSSOIndexTarget(os.path.join("coctweak", "saves", "hgg", "sso"))
        ).target,
    ]


def main():
    env = os_utils.ENV.clone()
    bm = BuildMaestro(hidden_build_dir=".build/genIndices_py/")
    add_gen_indices_targets(bm, env, [])
    bm.as_app()


if __name__ == "__main__":
    main()
