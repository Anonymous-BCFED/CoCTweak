import argparse
import os
import re
import sys
from pregex.core.operators import Either
from pregex import Pregex

from buildtools import cmd
from semantic_version import Version

sys.path.append("./devtools")

from _consts import SUPPORT_TABLE_END, SUPPORT_TABLE_START
from _dev_regex import PREG_SUPPORT_TABLE_MARKERS, PREG_CONSTS_PY_VERSION
from updateSupportTables import genSupportTable


def editInPlace(regex, replacement, filename):
    content = ""
    with open(filename, "r") as f:
        content = f.read()
    content = re.sub(regex, replacement, content)
    with open(filename, "w") as f:
        f.write(content)


argp = argparse.ArgumentParser()
argp.add_argument("version", type=str)
argp.add_argument("--no-new-entry", action="store_true", default=False)
args = argp.parse_args()
NEWVERSION = args.version
assert len(NEWVERSION) > 0
NEWVERSION = Version(NEWVERSION)

editInPlace(
    PREG_CONSTS_PY_VERSION,
    f"VERSION: Final[Version] = {NEWVERSION!r}",
    os.path.join("coctweak", "consts.py.in"),
)
if not args.no_new_entry:
    editInPlace(PREG_SUPPORT_TABLE_MARKERS, "", "CHANGELOG.md")
    ncl = f"# Changelog\n\n"
    ncl += f"## In Development ({NEWVERSION}?)\n\n"
    ncl += "### Compatibility\n\n"
    ncl += f"{SUPPORT_TABLE_START}\n{genSupportTable()}\n{SUPPORT_TABLE_END}\n\n"
    ncl += "### Big Changes\n\n"
    ncl += "*TBD*\n\n"
    ncl += "### Bugs Slain\n\n"
    ncl += "*TBD*\n\n"
    ncl += "### Documentation\n\n"
    ncl += "*TBD*\n\n"
    ncl += "### Developer Stuff\n\n"
    ncl += "*TBD*"
    editInPlace(r"# Changelog", ncl, "CHANGELOG.md")

cmd(["poetry", "version", str(NEWVERSION)], echo=True, show_output=True, critical=True)
cmd(["./BUILD.sh", "--rebuild"], echo=True, show_output=True, critical=True)
