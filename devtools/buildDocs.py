import collections
import json
import os
from pathlib import Path
import re
import subprocess
from typing import Any, Dict, List, Optional, Pattern, Set

from buildtools import BuildEnv, log, os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.fileio import CopyFilesTarget, CopyFileTarget
import jinja2
from ruamel.yaml import YAML

from _common import toYamlFlowList

yaml = YAML(typ="rt")

_env = os_utils.ENV  # .clone()
cwd = os.getcwd()
TEMP_DIR = Path("/tmp") if os_utils.is_linux() else Path(os.environ.get("TEMP"))
SANDBOX_DIR = TEMP_DIR / "coctweak-demo"
SOLLIB_DIR = (
    SANDBOX_DIR
    / ".macromedia"
    / "Flash_Player"
    / "#SharedObjects"
    / "AAAAAAAA"
    / "localhost"
)
FAKE_PROJ_DIR = SANDBOX_DIR / "coctweak"
FAKE_BACKUP_DIR = FAKE_PROJ_DIR / "backup"
COCTWEAK_EXE_NAME = "coctweak.exe" if os.name == "nt" else "coctweak"
COCTWEAK_EXE_DIST = (Path("dist") / COCTWEAK_EXE_NAME).absolute()
COCTWEAK_EXE_SANDBOX = SANDBOX_DIR / COCTWEAK_EXE_NAME
COCTWEAK: Path = None

ACTUAL_COCTWEAK_DIR = Path("coctweak").absolute()

_jenv = None
FAILED = False

REG_ERROR_LOG = re.compile(r"coctweak[\.a-z0-9_]*: error: ")

PROFILING_CMDS: List[List[str]] = []

L = toYamlFlowList


def _cmd(
    cmdline: List[str],
    echo: bool = False,
    lang: str = "",
    comment: Optional[str] = None,
    env: BuildEnv = _env,
    critical: bool = True,
    globbify: bool = False,
    replacements: Optional[Dict[Pattern[str], str]] = None,
):
    global FAILED
    o = ""
    if echo:
        if comment is not None:
            comment = f"# {comment}\n"
        else:
            comment = ""
        displayed_cmdline = []
        for i in range(len(cmdline)):
            chunk = cmdline[i]
            if chunk == str(COCTWEAK):
                chunk = "coctweak"
            displayed_cmdline += [chunk]
        o += f"```shell\n{comment}$ {os_utils._args2str(displayed_cmdline)}\n```\n```{lang}\n"
    stdout = ""
    with os_utils.Chdir(str(FAKE_PROJ_DIR)):
        env = os.environ.copy()
        env["HOME"] = str(SANDBOX_DIR)
        env["COCTWEAK_BUILDING_DOCS"] = "1"
        try:
            stdout = subprocess.check_output(
                cmdline, env=env, stderr=subprocess.STDOUT
            ).decode()
        except subprocess.CalledProcessError as cpe:
            with log.critical("CalledProcessException:"):
                log.critical(f"args={cpe.args!r}")
                log.critical(f"returncode={cpe.returncode!r}")
                log.critical(f"stdout={cpe.stdout!r}")
                log.critical(f"stderr={cpe.stderr!r}")
                log.critical(f"output={cpe.output!r}")
            raise cpe
        o += stdout

    PROFILING_CMDS.append(cmdline[1:])

    # Traceback (most recent call last):
    if "Traceback (most recent call last):" in stdout:
        log.critical("Traceback detected in output, aborting.")
        log.critical(stdout)
        raise Exception

    # coctweak: error: unrecognized arguments: -k=0
    if REG_ERROR_LOG.search(stdout) is not None:
        log.critical("Error detected in output, aborting.")
        log.critical(stdout)
        raise Exception

    if replacements is not None and len(replacements) > 0:
        for pattern, replacement in replacements.items():
            o = re.sub(pattern, replacement, o)
    if echo:
        o += "```\n"

    with open("data/pgo_cmds.yml", "w") as f:
        rejiggered = []
        written: Set[List[str]] = set()
        for x in PROFILING_CMDS:
            x = [y for y in x]
            t = tuple(x)
            if t in written:
                continue
            written.add(t)
            rejiggered.append(L(*x))
        rejiggered.sort()
        yaml.dump(rejiggered, f)

    return o


def _reset_slot(slot: Any) -> None:
    if slot == "ALL":
        os_utils.copytree(
            os.path.join(cwd, "sandbox-seed"), str(SOLLIB_DIR), verbose=True
        )
    else:
        os_utils.single_copy(
            os.path.join(cwd, "sandbox-seed", f"CoC_{slot}.sol"),
            str(SOLLIB_DIR),
            verbose=True,
        )


def _remove_slot(slot: Any, domain: Any = "localhost") -> None:
    (SOLLIB_DIR.parent / domain / f"CoC_{slot}.sol").unlink(missing_ok=True)


DOC_SRC_DIR = os.path.join(os.getcwd(), "docs-src")


class GenDocTarget(SingleBuildTarget):
    BT_LABEL = "GEN DOC"
    GLOBAL_DOCVARS: dict = None

    def __init__(
        self,
        srcdir,
        destdir,
        basedir,
        basename,
        dependencies=[],
        header: bool = True,
        strip_comments: bool = False,
    ):
        self.outfile = os.path.join(destdir, basedir, f"{basename}.md")
        self.infile = os.path.join(srcdir, basedir, f"{basename}.template.md")
        self.header = header
        self.vars = {}
        self.strip_comments: bool = strip_comments

        super().__init__(self.outfile, [self.infile])

    def build(self):
        global _jenv, _env, COCTWEAK

        if self.GLOBAL_DOCVARS is None:
            with open("data/docvars.yml", "r") as f:
                self.GLOBAL_DOCVARS = yaml.load(f)
            with open(ACTUAL_COCTWEAK_DIR / "consts.py", "r") as f:
                m = re.search(
                    r"VERSION: Final\[Version\] = Version\('([^']+)'", f.read()
                )
                assert m is not None
                self.GLOBAL_DOCVARS["VERSION"] = m[1]
            # print(repr(self.GLOBAL_DOCVARS))

        _jenv = jinja2.Environment(
            loader=jinja2.FileSystemLoader(DOC_SRC_DIR),
            extensions=["jinja2.ext.do"],
            autoescape=False,
        )

        if COCTWEAK is None:
            COCTWEAK = Path(
                "./coctweak.cmd" if os.name == "nt" else "./coctweak.sh"
            ).absolute()
            if COCTWEAK_EXE_DIST.is_file():
                COCTWEAK = COCTWEAK_EXE_DIST

        _jenv.globals.update(
            cmd=_cmd,
            reset_slot=_reset_slot,
            remove_slot=_remove_slot,
            COCTWEAK=str(COCTWEAK),
            **self.GLOBAL_DOCVARS,
        )

        if _env.get("HOME", "") != str(SANDBOX_DIR):
            _env.set("HOME", str(SANDBOX_DIR))  # hue

        if _env.get("COCTWEAK_BUILDING_DOCS", "") != "1":
            _env.set("COCTWEAK_BUILDING_DOCS", "1")

        rendered = ""
        if self.header:
            rendered += "<!--\n"
            rendered += "@GENERATED by devtools/buildDocs.py. DO NOT MANUALLY EDIT.\n"
            rendered += f"Edit {os.path.normpath(self.infile)} instead!\n"
            rendered += "-->\n"
        with open(self.infile, "r") as f:
            renderedtempl = _jenv.from_string(f.read()).render()
            if self.strip_comments:
                renderedtempl = re.sub(
                    r"(<!--.*?-->)", "", renderedtempl, flags=re.DOTALL
                )
                renderedtempl = re.sub(r"({#.*?#})", "", renderedtempl, flags=re.DOTALL)
            rendered += renderedtempl
        if FAILED:
            self.failed()
            return
        os_utils.ensureDirExists(os.path.dirname(self.outfile), noisy=True)
        with open(self.outfile, "w") as f:
            f.write(rendered)


class BuildItemTableTarget(SingleBuildTarget):
    BT_LABEL = "TABLE"

    def __init__(self, target, input_json, template_file, modname, columns):
        super().__init__(target, [input_json, template_file])
        self.input_json = input_json
        self.template_file = template_file
        self.columns = columns
        self.modname = modname

    def build(self):
        data = {}
        with open(self.input_json, "r") as f:
            data = json.load(f)

        output = {}
        for k in sorted(data.keys()):
            item = data[k]
            cat = item.get("category", "UNKNOWN")
            if cat not in output:
                output[cat] = {}
            output[cat][k] = []
            for column, opts in self.columns.items():
                stringed = "-"
                typeid = opts.get("type", "str")
                if typeid in ("str", "int", "float", "number"):
                    stringed = str(item[column])
                elif typeid in ("obj", "escape"):
                    stringed = json.dumps(item[column], indent=2)
                    if "format" not in opts:
                        opts["format"] = "pre"
                else:
                    stringed = f"Unknown column type {typeid!r}"

                if opts.get("prefix", "") != "":
                    stringed = opts["prefix"] + stringed
                if opts.get("suffix", "") != "":
                    stringed = stringed + opts["prefix"]

                formatting = opts.get("format", "")
                if formatting in ("pre",):
                    stringed = "<pre>" + stringed + "</pre>"
                elif formatting in ("code",):
                    stringed = "<code>" + stringed + "</code>"
                output[cat][k].append(stringed)
        jenv = jinja2.Environment(
            loader=jinja2.FileSystemLoader(["."]),
            autoescape=False,
        )
        template = jenv.get_template(self.template_file)
        os_utils.ensureDirExists(os.path.dirname(self.target))
        with open(self.target, "w") as f:
            f.write(
                template.render(
                    FILENAME=self.input_json,
                    MODNAME=self.modname,
                    COLUMNS=self.columns,
                    categories=output,
                )
            )


def add_create_sandbox_targets(bm: BuildMaestro, env, deps=[]) -> List[str]:
    BUILD_DIR = Path(bm.builddir).absolute()

    o = [
        bm.add(
            CopyFilesTarget(
                str(BUILD_DIR / "SND_SOLLIB_DIR.tgt"),
                "sandbox-seed",
                str(SOLLIB_DIR),
                dependencies=deps,
            )
        ).target,
        bm.add(
            CopyFilesTarget(
                str(BUILD_DIR / "SND_DATA_DIR.tgt"),
                "data",
                os.path.join(FAKE_PROJ_DIR, "data"),
                dependencies=deps,
            )
        ).target,
        # bm.add(CopyFilesTarget(str(BUILD_DIR / 'SND_PY_DIR.tgt'), 'coctweak', os.path.join(FAKE_PROJ_DIR, 'coctweak'), dependencies=deps)).target,
        bm.add(
            CopyFileTarget(
                str(FAKE_PROJ_DIR / "coctweak.sh"), "coctweak.sh", dependencies=deps
            )
        ).target,
        bm.add(
            CopyFileTarget(
                str(FAKE_PROJ_DIR / "coctweak.cmd"), "coctweak.cmd", dependencies=deps
            )
        ).target,
        bm.add(
            CopyFileTarget(
                destfile=os.path.join(FAKE_PROJ_DIR, COCTWEAK_EXE_NAME),
                sourcefile=os.path.join("dist", COCTWEAK_EXE_NAME),
                dependencies=deps,
            )
        ).target,
    ]
    return o


def add_docs_buildtargets(bm: BuildMaestro, env, deps=[]) -> List[str]:

    if not os.path.isdir("sandbox-seed"):
        # Sandbox seed dir is needed, and I still need to decide how I want to handle it.
        return []
    bm.other_dirs_to_clean.append(str(SANDBOX_DIR))
    doclist = {}
    with open("doclist.yml", "r") as f:
        doclist = yaml.load(f)
    sb_deps = add_create_sandbox_targets(bm, env, deps)
    generated_targets = [
        bm.add(
            GenDocTarget(
                os.path.join("docs-src", "dist"),
                "dist",
                ".",
                "README",
                header=False,
                strip_comments=True,
                dependencies=sb_deps,
            )
        ).target,
        bm.add(GenDocTarget(".", ".", ".", "README", dependencies=sb_deps)).target,
    ]
    for dirname, children in doclist.items():
        if isinstance(children, list):
            for child in children:
                generated_targets.append(
                    bm.add(
                        GenDocTarget(
                            "docs-src", "docs", dirname, child, dependencies=sb_deps
                        )
                    ).target
                )
        else:
            generated_targets.append(
                bm.add(
                    GenDocTarget(
                        "docs-src", "docs", dirname, children, dependencies=sb_deps
                    )
                ).target
            )

    hgg_items = bm.add(
        BuildItemTableTarget(
            target=os.path.join("docs", "mods", "hgg", "items.md"),
            input_json=os.path.join("data", "hgg", "items-merged.min.json"),
            template_file=os.path.join("docs", "templates", "items.tmpl.md"),
            modname="HGG",
            columns=collections.OrderedDict(
                {
                    # 'id': {},
                    "const": {"format": "code"},
                    "shortName": {},
                    "headerName": {},
                    "longName": {},
                    "value": {},
                    "durability": {},
                    "description": {"type": "escape"},
                    "degradable": {},
                    "degradesInto": {},
                    "bonusStats": {"type": "obj"},
                }
            ),
        )
    )
    uee_items = bm.add(
        BuildItemTableTarget(
            target=os.path.join("docs", "mods", "uee", "items.md"),
            input_json=os.path.join("data", "uee", "items-merged.min.json"),
            template_file=os.path.join("docs", "templates", "items.tmpl.md"),
            modname="UEE",
            columns=collections.OrderedDict(
                {
                    # 'id': {},
                    "const": {"format": "code"},
                    "shortName": {},
                    "longName": {},
                    "value": {},
                    "durability": {},
                    "description": {"type": "escape"},
                    "degradable": {},
                    "degradesInto": {},
                }
            ),
        )
    )
    o = generated_targets + [hgg_items.target, uee_items.target]
    # print(repr(o))
    return o


def main():
    env = os_utils.ENV  # .clone()
    bm = BuildMaestro(hidden_build_dir=".build/buildDocs_py/")
    add_docs_buildtargets(bm, env, [])
    bm.as_app()


if __name__ == "__main__":
    main()
