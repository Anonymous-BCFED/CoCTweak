import json
import os

from buildtools import os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from ruamel.yaml import YAML

yaml = YAML(typ='rt')
REQUIRED = ['id', 'const']

class MakeEnumTarget(SingleBuildTarget):
    BT_LABEL = 'GEN ENUM'

    def __init__(self, injson, outfile, enumName, cfg: dict, dependencies=[]):
        self.injson: str = injson
        self.outfile: str = outfile
        self.enumName: str = enumName
        self.cfg: dict = cfg

        super().__init__(outfile, [injson], dependencies=dependencies)
    def build(self):
        data = {}
        with open(self.injson, 'r') as f:
            data = json.load(f)
        flags = {}
        for k, val in data.items():
            for rk in REQUIRED:
                if rk not in val:
                    print(k, self.injson, f'Missing {rk!r}!')
                    val[rk] = k
            fk = val['const']
            if fk == '':
                fk = '__EMPTYSTR__'
            elif fk is None:
                fk = '__NONE__'
            flags[fk] = val['id']
        enumType: str = self.cfg.get('enumType', 'Enum')
        for k,v in self.cfg.get('force-vals', {}).items():
            if v is None:
                if k in flags:
                    del flags[k]
            else:
                flags[k]=v
        with open(self.outfile, 'w') as w:
            w.write('# @GENERATED\n')
            w.write(f'from enum import {enumType}\n')
            w.write('\n')
            w.write(f'class {self.enumName}({enumType}):\n')
            maxlen = max([len(x) for x in flags.keys()])
            for k in sorted(flags.keys()):
                v = flags[k]
                padding = ' ' * (maxlen + 2 - len(k))
                w.write(f'    {k}{padding}= {v!r}\n')

def add_mklibenums_targets(bm: BuildMaestro, env, deps=[]):
    generated_targets = []
    with open(os.path.join('data','libconfig.yml'), 'r') as f:
        for enumName, cfg in yaml.load(f).items():
            generated_targets += [
                bm.add(MakeEnumTarget(cfg['src'], cfg['dest'], enumName, cfg)).target
            ]
    return generated_targets
def main():
    env = os_utils.ENV.clone()
    bm = BuildMaestro(hidden_build_dir='.build/mkLibEnums_py/')
    add_mklibenums_targets(bm, env, [])
    bm.as_app()

if __name__ == '__main__':
    main()
