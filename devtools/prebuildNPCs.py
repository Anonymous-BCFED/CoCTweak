from collections import OrderedDict
from enum import Flag, IntEnum, IntFlag
from functools import reduce
from io import StringIO
import json
import operator
import os
from pathlib import Path
import re
import stat
import sys
from typing import (
    Any,
    Dict,
    List,
    Optional,
    Pattern,
    TextIO,
    Tuple,
    Type,
    Union,
)  # NOQA

from _dev_regex import (
    PREG_HAS_SFX,
    PREG_CREATE_SFX,
    PREG_RESET,
    PREG_SSO_ID,
    PREG_SSO_VERSION,
    PREG_CONST,
    PREG_DEBUG_VARS,
    PREG_RESET_VAR,
    PREG_RESET_VAR_COMMENTED,
    PREG_KFLAG_DEF_COMMENTED,
    PREG_KFLAG_DEF,
    PREG_KFLAG_REF,
    PREG_KFLAG_REF_BINARY_OP,
    PREG_KFLAG_REF_UNARY_OP,
    PREG_KFLAG_REF_PREGSTORE,
    PREG_KFLAG_REF_BREASTSTORE,
    PREG_KFLAG_REF_IFCHK,
    PREG_AS3_BLOCKCOMMENT,
    PREG_AS3_LINECOMMENT,
)

from buildtools import log
from buildtools.indentation import IndentWriter
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from ruamel.yaml import YAML, CommentedMap

from checkFuncHashes import parsePythonFile as hashPythonFile
from coctweak_build.mods import replacePathWithPrefix
from genIndices import GenNPCIndexTarget, GenSSOIndexTarget

yaml = YAML(typ="rt")


class EGender(IntEnum):
    UNKNOWN = 0
    MALE = 1
    FEMALE = 2
    HERM = 3


class ParseNPCFromAS(SingleBuildTarget):
    BT_LABEL = "PARSE"

    def __init__(self, target, ymlfile: str, dependencies=[]) -> None:
        self.ymlfile: str = ymlfile
        super().__init__(
            target=target, files=[ymlfile, __file__], dependencies=dependencies
        )

    def build(self) -> None:
        parseFromNPCDef(self.ymlfile, self.target)


class GenNPCFromYAML(SingleBuildTarget):
    BT_LABEL = "GEN NPC"

    def __init__(self, target, ymlfile: str, dependencies=[]) -> None:
        self.ymlfile: str = ymlfile
        super().__init__(
            target=target, files=[ymlfile, __file__], dependencies=dependencies
        )

    def build(self) -> None:
        genNPC(self.ymlfile, self.target)


class GenSSOFromYAML(SingleBuildTarget):
    BT_LABEL = "GEN SSO"

    def __init__(self, target, ymlfile: str, dependencies=[]) -> None:
        self.ymlfile: str = ymlfile
        super().__init__(
            target=target, files=[ymlfile, __file__], dependencies=dependencies
        )

    def build(self) -> None:
        genSSO(self.ymlfile, self.target)


assert (
    PREG_HAS_SFX.search("if (player.hasStatusEffect(StatusEffects.WandererDemon)) {")
    is not None
)
test_match = PREG_CREATE_SFX.search(
    "player.createStatusEffect(StatusEffects.Blind, 1 + rand(3), 0, 0, 0);"
)
assert test_match is not None
assert test_match["attr"] == "Blind"

assert PREG_RESET.search("public function reset():void {") is not None


def literal(pat: str):
    return re.compile(f"^{re.escape(pat)}$")


SHIT_TO_IGNORE = [
    re.compile(r"^ACHIEVEMENT_PROGRESS_"),
    re.compile(r"^CODEX_ENTRY_"),
    literal("CAMP_BUILT_CABIN"),
    literal("CAMP_CABIN_FURNITURE_BED"),
    literal("SLEEP_WITH"),
    literal("SHIFT_KEY_DOWN"),
    literal("IN_COMBAT_USE_PLAYER_WAITED_FLAG"),
]
DEFAULT_SFX_TO_IGNORE = [
    literal("Blind"),
    literal("Disarmed"),
    literal("Fear"),
    literal("Feeder"),
    literal("FirstAttack"),
    literal("Heat"),
    literal("Illusion"),
    literal("Infested"),
    literal("lustvenom"),
    literal("NoFlee"),
    literal("ParasiteSlug"),
    literal("Rut"),
    literal("Spar"),
    literal("WebSilence"),
]


def mkReadOnly(filepath: Path) -> None:
    umask = os.umask(0)
    os.umask(umask)

    perms: int = filepath.stat().st_mode
    # log.info(f'{filepath}: BEFORE: 0o{perms:o}')
    perms &= ~(stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH)
    # perms &= umask
    log.info(f"{filepath}: chmod: 0o{perms:o}")

    filepath.chmod(perms)


def mkWritable(filepath: Path) -> None:
    umask = os.umask(0)
    os.umask(umask)

    perms: int = filepath.stat().st_mode
    # log.info(f'{filepath}: BEFORE: 0o{perms:o}')
    perms |= stat.S_IWUSR | stat.S_IWGRP
    # perms &= umask
    log.info(f"{filepath}: chmod: 0o{perms:o}")

    filepath.chmod(perms)


class BaseModule(object):
    ALL: Dict[str, Type["BaseModule"]] = {}

    @classmethod
    def Register(cls, modid: str) -> None:
        def wrapper(childcls) -> None:
            print(repr(childcls))
            cls.ALL[modid] = childcls

        return wrapper

    def __init__(self, npc=None, sso=None) -> None:
        super().__init__()
        self.npc: Optional["NPCFile"] = npc
        self.sso: Optional["SSOFile"] = sso

    def deserialize(self, data: dict) -> None:
        pass

    def serialize(self) -> dict:
        {}

    def getNPCMixin(self) -> Optional[str]:
        return None

    def writeNPCImports(self, w: IndentWriter) -> None:
        return

    def writeNPCInit(self, w: IndentWriter, kflagsEnum: str) -> None:
        return


@BaseModule.Register("renamable")
class RenamableModule(BaseModule):
    def __init__(self, npc=None, sso=None) -> None:
        super().__init__(npc=npc, sso=sso)
        self.default_name: str = ""

        self.flag: Optional[str] = None
        self.ssoinfo: Optional[Dict[str, str]] = None

    def deserialize(self, data: dict) -> None:
        self.default_name = data["default"]
        self.flag = data.get("flag")
        self.ssoinfo = data.get("sso")

    def serialize(self) -> dict:
        return {
            "default": self.default_name,
            "flag": self.flag,
            "sso": self.ssoinfo,
        }

    def getNPCMixin(self) -> Optional[str]:
        return "RenamableNPC"

    def writeNPCImports(self, w: IndentWriter) -> None:
        w.writeline(
            "from coctweak.saves.common.npcmodules.renamable import RenamableNPC"
        )

    def writeNPCInit(self, w: IndentWriter, kflagsEnum: str) -> None:
        args = [f"default={self.default_name!r}"]
        if self.flag is not None:
            args += [f"flag={kflagsEnum}.{self.flag}"]
        else:
            ssoname = self.ssoinfo["name"]
            args += [f"ssoname={ssoname!r}"]
            ssoattr = self.ssoinfo["attr"]
            args += [f"ssoprop={ssoattr!r}"]
        args = ", ".join(args)
        w.writeline(f"self.setupRenamable({args})")


class EKFlagType(IntEnum):
    UNKNOWN = 0
    INT = 1
    FLOAT = 2
    STR = 3
    BOOL = 4
    ARRAY = 5
    UNUSED = 6  # Passthrough
    INTBOOL = 7
    OBJECT = 8


class EPropertyFlags(IntFlag):
    NONE = 0
    READONLY = 1
    NEW = 2
    UNUSED = 4


class KFlag(object):
    def __init__(self) -> None:
        self.id: int = -1
        self.name: str = ""
        self.type: EKFlagType = EKFlagType(0)
        self.array_type: EKFlagType = EKFlagType(0)
        self.default: Any = None
        self.comment: str = ""
        self.disabled: bool = False

        self.min: Optional[int] = None
        self.max: Optional[int] = None

        self.choices: Optional[dict] = None
        self.enum: Optional[str] = None
        self.notes: Optional[str] = None

        self.bitflags: Optional[Dict[int, str]] = None

        self.flags: EPropertyFlags = EPropertyFlags(0)

    @property
    def readonly(self) -> bool:
        return (self.flags & EPropertyFlags.READONLY) == EPropertyFlags.READONLY

    @property
    def new(self) -> bool:
        return (self.flags & EPropertyFlags.NEW) == EPropertyFlags.NEW

    @property
    def unused(self) -> bool:
        return (self.flags & EPropertyFlags.UNUSED) == EPropertyFlags.UNUSED

    @property
    def dictKey(self) -> str:
        return self.name

    def deserialize(self, data: dict) -> None:
        self.id = data.get("id", self.id)
        self.type = EKFlagType[data.get("type", self.type.name)]
        self.array_type = EKFlagType[data.get("array_type", self.type.name)]
        self.comment = data.get("comment", self.comment)
        self.disabled = data.get("disabled", self.disabled)
        self.choices = data.get("choices", self.choices)
        self.default = data.get("default", self.default)
        self.enum = data.get("enum", self.enum)
        self.notes = data.get("notes", self.notes)
        self.bitflags = data.get("bitflags", self.bitflags)
        self.min = data.get("min", self.min)
        self.max = data.get("max", self.max)
        if "flags" in data:
            if isinstance(data["flags"], list):
                self.flags = reduce(
                    operator.or_,
                    [EPropertyFlags[x.upper().replace("-", "")] for x in data["flags"]],
                    EPropertyFlags.NONE,
                )
            elif isinstance(data["flags"], int):
                self.flags = EPropertyFlags(data["flags"])

        # print(self.dictKey, repr(data.get('flags')), self.flags, self.readonly, self.unused)

        if self.type == EKFlagType.UNUSED:
            self.flags |= EPropertyFlags.UNUSED

        """
        if self.type == EKFlagType.INTBOOL:
            self.type = EKFlagType.INT
            self.choices = {
                0: 'False',
                1: 'True',
            }
        """

        # print(self.dictKey, repr(data.get('flags')), self.flags, self.readonly, self.unused)

        if self.unused:
            # Set type to UNUSED if we don't know the type.
            if self.type == EKFlagType.UNKNOWN:
                self.type = EKFlagType.UNUSED
                self.default = 0

        # print(self.dictKey, repr(data.get('flags')), self.flags, self.readonly, self.unused)

    def serialize(self) -> dict:
        o = {
            "id": self.id,
            "type": self.type.name,
        }
        if self.default is not None:
            o["default"] = self.default
        if self.array_type != EKFlagType.UNKNOWN:
            o["array_type"] = self.array_type.name
        if self.comment != "":
            o["comment"] = self.comment
        if self.disabled:
            o["disabled"] = True
        if self.choices is not None and len(self.choices) > 0:
            o["choices"] = self.choices
        if self.enum is not None:
            o["enum"] = self.enum
        if self.bitflags is not None:
            o["bitflags"] = self.bitflags
        if self.min is not None:
            o["min"] = self.min
        if self.max is not None:
            o["max"] = self.max
        if self.flags != EPropertyFlags.NONE:
            o["flags"] = [x.name for x in EPropertyFlags if x in self.flags]
        if self.notes is not None:
            o["notes"] = self.notes
        return o

    def getTypeHint(self) -> str:
        if self.type == EKFlagType.UNKNOWN:
            return "FIXME"
        elif self.type == EKFlagType.UNUSED:
            return "None"
        elif self.type == EKFlagType.INT:
            return "int"
        elif self.type == EKFlagType.FLOAT:
            return "float"
        elif self.type == EKFlagType.STR:
            return "str"
        elif self.type == EKFlagType.BOOL:
            return "bool"
        elif self.type == EKFlagType.ARRAY:
            return "list"
        elif self.type == EKFlagType.INTBOOL:
            return "int"
        elif self.type == EKFlagType.OBJECT:
            return "dict"

    def getDefault(self) -> str:
        if self.default is not None:
            if self.enum is not None:
                return f"{self.enum}({int(self.default)})"
            if isinstance(self.default, (OrderedDict, CommentedMap)):
                return str(dict(self.default))
            return repr(self.default)
        if self.type == EKFlagType.UNKNOWN:
            return "None"
        elif (
            self.type == EKFlagType.INT
            or self.type == EKFlagType.UNUSED
            or self.type == EKFlagType.INTBOOL
        ):
            return "0"
        elif self.type == EKFlagType.FLOAT:
            return "0."
        elif self.type == EKFlagType.STR:
            return "''"
        elif self.type == EKFlagType.BOOL:
            return "False"
        elif self.type == EKFlagType.ARRAY:
            return "[]"
        elif self.type == EKFlagType.OBJECT:
            return "{}"

    def write(self, w: IndentWriter, enumCls: str) -> None:
        hint: str = self.getTypeHint()
        default: str = self.getDefault()
        end = ")"
        kwargs: str = ""
        if self.readonly:
            kwargs += ", readonly=True"
        if self.unused:
            kwargs += ", unused=True"
        if self.enum is not None:
            kwargs += f", enumType={self.enum}"
        if self.min is not None:
            kwargs += f", min={self.min}"
        if self.max is not None:
            kwargs += f", max={self.max}"
        if self.notes is not None:
            kwargs += f", notes={self.notes!r}"
        if self.type == EKFlagType.INTBOOL:
            kwargs += f", intbool=True"
        if self.choices is not None:
            kwargs += ", choices={"
            end = ""
        if self.bitflags is not None and len(self.bitflags) > 0:
            kwargs += ", bitflags={"
            end = ""
        if self.comment != "":
            w.writeline(f"# {self.comment}")
        c = "#" if self.disabled else ""
        # w.writeline(f'{c}self.{self.name}: NPCKFlagProperty = self.addKFlag({self.name!r}, {hint}, {default}, {enumCls}.{self.name}{kwargs}{end}')
        w.writeline(
            f"{c}self.addKFlag({self.name!r}, {hint}, {default}, {enumCls}.{self.name}{kwargs}{end}"
        )
        if self.choices is not None:
            with w:
                cl = {}
                if isinstance(self.choices, list):
                    cl = {v: v for v in self.choices}
                if isinstance(self.choices, dict):
                    cl = self.choices
                for k, v in cl.items():
                    w.writeline(f"{k!r}: {v!r},")
            w.writeline("})")
        if self.bitflags is not None:
            with w:
                cl = {}
                if isinstance(self.bitflags, list):
                    cl = {1 << b: v for b, v in enumerate(self.bitflags)}
                if isinstance(self.bitflags, dict):
                    cl = self.bitflags
                for k, v in cl.items():
                    w.writeline(f"{k!r}: {v!r},")
            w.writeline("})")


class StatusEffectProperty(object):
    def __init__(self) -> None:
        self.sfxid: Any = None
        self.idx_or_key: Optional[Union[str, int]] = None
        self.type: EKFlagType = EKFlagType(0)
        self.default: Any = None
        self.comment: str = ""
        self.disabled: bool = False

        self.min: Optional[int] = None
        self.max: Optional[int] = None

        self.choices: Optional[dict] = None
        self.enum: Optional[str] = None
        self.notes: Optional[str] = None

        self.bitflags: Optional[Dict[int, str]] = None

        self.flags: EPropertyFlags = EPropertyFlags(0)

    @property
    def readonly(self) -> bool:
        return (self.flags & EPropertyFlags.READONLY) == EPropertyFlags.READONLY

    @property
    def new(self) -> bool:
        return (self.flags & EPropertyFlags.NEW) == EPropertyFlags.NEW

    @property
    def unused(self) -> bool:
        return (self.flags & EPropertyFlags.UNUSED) == EPropertyFlags.UNUSED

    @property
    def dictKey(self) -> str:
        if self.sfxid is not None and self.idx_or_key is not None:
            return f"{self.sfxid}:{self.idx_or_key}"
        return self.name

    def deserialize(self, data: dict) -> None:
        self.sfxid = data.get("id", self.sfxid)
        self.idx_or_key = data.get("key", self.idx_or_key)
        self.type = EKFlagType[data.get("type", self.type.name)]
        self.comment = data.get("comment", self.comment)
        self.disabled = data.get("disabled", self.disabled)
        self.choices = data.get("choices", self.choices)
        self.default = data.get("default", self.default)
        self.min = data.get("min", self.min)
        self.max = data.get("max", self.max)
        self.enum = data.get("enum", self.enum)
        self.bitflags = data.get("bitflags", self.bitflags)
        self.notes = data.get("notes", self.notes)
        if "flags" in data:
            if isinstance(data["flags"], list):
                self.flags = reduce(
                    operator.or_,
                    [EPropertyFlags[x.upper().replace("-", "")] for x in data["flags"]],
                    EPropertyFlags.NONE,
                )
            elif isinstance(data["flags"], int):
                self.flags = EPropertyFlags(data["flags"])

        if (self.idx_or_key and self.sfxid) or data.get("new", False):
            self.flags |= EPropertyFlags.NEW

        self.name = self.dictKey

        if self.type == EKFlagType.UNUSED:
            self.flags |= EPropertyFlags.UNUSED

        """
        if self.type == EKFlagType.INTBOOL:
            self.type = EKFlagType.INT
            self.choices = {
                0: 'False',
                1: 'True',
            }
        """

        if self.unused:
            # Set type to UNUSED if we don't know the type.
            if self.type == EKFlagType.UNKNOWN:
                self.type = EKFlagType.UNUSED
                self.default = 0

    def serialize(self) -> dict:
        o = {
            "id": self.sfxid,
            "key": self.idx_or_key,
            "type": self.type.name,
        }
        if self.default is not None:
            o["default"] = self.default
        if self.comment != "":
            o["comment"] = self.comment
        if self.disabled:
            o["disabled"] = True
        if self.choices is not None and len(self.choices) > 0:
            o["choices"] = self.choices
        if self.enum is not None:
            o["enum"] = self.enum
        if self.bitflags is not None:
            o["bitflags"] = self.bitflags
        if self.min is not None:
            o["min"] = self.min
        if self.max is not None:
            o["max"] = self.max
        if self.notes is not None:
            o["notes"] = self.notes
        if self.flags != EPropertyFlags.NONE:
            o["flags"] = [x.name for x in EPropertyFlags if x in self.flags]
        return o

    def getTypeHint(self) -> str:
        if self.type == EKFlagType.UNKNOWN:
            return "FIXME"
        elif self.type == EKFlagType.UNUSED:
            return "None"
        elif self.type == EKFlagType.INT:
            return "int"
        elif self.type == EKFlagType.FLOAT:
            return "float"
        elif self.type == EKFlagType.STR:
            return "str"
        elif self.type == EKFlagType.BOOL:
            return "bool"
        elif self.type == EKFlagType.ARRAY:
            return "list"
        elif self.type == EKFlagType.INTBOOL:
            return "int"
        elif self.type == EKFlagType.OBJECT:
            return "int"

    def getDefault(self) -> str:
        if self.default is not None:
            if self.enum is not None:
                return f"{self.enum}({int(self.default)})"
            if isinstance(self.default, (OrderedDict, CommentedMap)):
                return str(dict(self.default))
            return repr(self.default)
        if self.type == EKFlagType.UNKNOWN:
            return "None"
        elif (
            self.type == EKFlagType.INT
            or self.type == EKFlagType.UNUSED
            or self.type == EKFlagType.INTBOOL
        ):
            return "0"
        elif self.type == EKFlagType.FLOAT:
            return "0."
        elif self.type == EKFlagType.STR:
            return "''"
        elif self.type == EKFlagType.BOOL:
            return "False"
        elif self.type == EKFlagType.ARRAY:
            return "[]"
        elif self.type == EKFlagType.OBJECT:
            return "{}"

    def write(self, w: IndentWriter, enumCls: str) -> None:
        hint: str = self.getTypeHint()
        default: str = self.getDefault()
        end = ")"
        kwargs: str = ""
        if self.readonly:
            kwargs += f", readonly=True"
        if self.unused:
            kwargs += f", unused=True"
        if self.type == EKFlagType.INTBOOL:
            kwargs += f", intbool=True"
        if self.enum is not None:
            kwargs += f", enumType={self.enum}"
        if self.min is not None:
            kwargs += f", min={self.min}"
        if self.max is not None:
            kwargs += f", max={self.max}"
        if self.notes is not None:
            kwargs += f", notes={self.notes!r}"
        if self.choices is not None:
            kwargs += ", choices={"
            end = ""
        if self.bitflags is not None and len(self.bitflags) > 0:
            kwargs += ", bitflags={"
            end = ""
        if self.comment != "":
            w.writeline(f"# {self.comment}")
        c = "#" if self.disabled else ""
        # w.writeline(f'{c}self.{self.name}: NPCKFlagProperty = self.addKFlag({self.name!r}, {hint}, {default}, {enumCls}.{self.name}{kwargs}{end}')
        # def addSFXProperty(self, name: str, type: Type, default: Any, sfxid: Union[str, Enum], idx_or_key: Optional[Union[int, str]], **kwargs) -> None:
        w.writeline(
            f"{c}self.addSFXProperty({self.name!r}, {hint}, {default}, {enumCls}.{self.sfxid}, {self.idx_or_key!r}{kwargs}{end}"
        )
        if self.choices is not None:
            with w:
                cl = {}
                if isinstance(self.choices, list):
                    cl = {v: v for v in self.choices}
                if isinstance(self.choices, dict):
                    cl = self.choices
                for k, v in cl.items():
                    w.writeline(f"{k!r}: {v!r},")
            w.writeline("})")
        if self.bitflags is not None:
            with w:
                cl = {}
                if isinstance(self.bitflags, list):
                    cl = {1 << b: v for b, v in enumerate(self.bitflags)}
                if isinstance(self.bitflags, dict):
                    cl = self.bitflags
                for k, v in cl.items():
                    w.writeline(f"{k!r}: {v!r},")
            w.writeline("})")


class SSOProperty(object):
    def __init__(self) -> None:
        self.name: str = ""
        self.objid: str = ""
        self.default: Any = None
        self.type: EKFlagType = EKFlagType(0)
        self.comment: str = ""
        self.debugHelpText: str = ""
        self.disabled: bool = False
        self.enum: Optional[str] = None

        self.choices: Optional[dict] = None
        self.notes: Optional[str] = None

        self.bitflags: Optional[Dict[int, str]] = None

        self.flags: EPropertyFlags = EPropertyFlags(0)

    @property
    def readonly(self) -> bool:
        return (self.flags & EPropertyFlags.READONLY) == EPropertyFlags.READONLY

    @property
    def new(self) -> bool:
        return (self.flags & EPropertyFlags.NEW) == EPropertyFlags.NEW

    @property
    def unused(self) -> bool:
        return (self.flags & EPropertyFlags.UNUSED) == EPropertyFlags.UNUSED

    @property
    def dictKey(self) -> str:
        return self.name

    def deserialize(self, data: dict) -> None:
        self.objid = data.get("objid", self.objid)
        self.default = data.get("default", self.default)
        self.type = EKFlagType[data.get("type", self.type.name)]
        self.comment = data.get("comment", self.comment)
        self.debugHelpText = data.get("debug-text", self.debugHelpText)
        self.disabled = data.get("disabled", self.disabled)
        self.choices = data.get("choices", self.choices)
        self.enum = data.get("enum", self.enum)
        self.bitflags = data.get("bitflags", self.bitflags)
        self.notes = data.get("notes", self.notes)
        if "flags" in data:
            if isinstance(data["flags"], list):
                self.flags = reduce(
                    operator.or_,
                    [EPropertyFlags[x.upper().replace("-", "")] for x in data["flags"]],
                    EPropertyFlags.NONE,
                )
            elif isinstance(data["flags"], int):
                self.flags = EPropertyFlags(data["flags"])

        if data.get("new", False):
            self.flags |= EPropertyFlags.NEW

        if self.type == EKFlagType.UNUSED:
            self.flags |= EPropertyFlags.UNUSED

        """
        if self.type == EKFlagType.INTBOOL:
            self.type = EKFlagType.INT
            self.choices = {
                0: 'False',
                1: 'True',
            }
        """

        if self.unused:
            # Set type to UNUSED if we don't know the type.
            if self.type == EKFlagType.UNKNOWN:
                self.type = EKFlagType.UNUSED
                self.default = 0

    def serialize(self) -> dict:
        o = {
            "objid": self.objid,
            "default": self.default,
            "type": self.type.name,
        }
        if self.comment != "":
            o["comment"] = self.comment
        if self.debugHelpText != "":
            o["debug-text"] = self.debugHelpText
        if self.disabled:
            o["disabled"] = True
        if self.choices is not None and len(self.choices) > 0:
            o["choices"] = self.choices
        if self.enum is not None:
            o["enum"] = self.enum
        if self.bitflags is not None:
            o["bitflags"] = self.bitflags
        if self.notes is not None:
            o["notes"] = self.notes
        if self.flags != EPropertyFlags.NONE:
            o["flags"] = [x.name for x in EPropertyFlags if x in self.flags]
        return o

    def getTypeHint(self) -> str:
        if self.type == EKFlagType.UNKNOWN:
            return "FIXME"
        elif self.type == EKFlagType.UNUSED:
            return "None"
        elif self.type == EKFlagType.INT:
            return "int"
        elif self.type == EKFlagType.FLOAT:
            return "float"
        elif self.type == EKFlagType.STR:
            return "str"
        elif self.type == EKFlagType.BOOL:
            return "bool"
        elif self.type == EKFlagType.ARRAY:
            return "list"
        elif self.type == EKFlagType.INTBOOL:
            return "int"
        elif self.type == EKFlagType.OBJECT:
            return "dict"

    def getDefault(self) -> str:
        if self.default is not None:
            if self.default == "false":
                return "False"
            if self.default == "true":
                return "True"
            if self.type in (EKFlagType.STR, EKFlagType.ARRAY):
                return repr(self.default)
            if isinstance(self.default, (OrderedDict, CommentedMap)):
                return str(dict(self.default))
            return self.default
        if self.type == EKFlagType.UNKNOWN:
            return "None"
        elif self.type == EKFlagType.INT or self.type == EKFlagType.UNUSED:
            return "0"
        elif self.type == EKFlagType.FLOAT:
            return "0."
        elif self.type == EKFlagType.STR:
            return "''"
        elif self.type == EKFlagType.BOOL:
            return "False"
        elif self.type == EKFlagType.ARRAY:
            return "[]"
        elif self.type == EKFlagType.OBJECT:
            return "{}"

    def detectType(self, value: str) -> None:
        if self.type == EKFlagType.UNKNOWN:
            if value.isdigit():
                self.type = EKFlagType.INT
            elif value.isdecimal() and "." in value:
                self.type = EKFlagType.FLOAT
            elif value in ("true", "false"):
                self.type = EKFlagType.BOOL
            elif value.startswith('"'):
                self.type = EKFlagType.STR
            elif value.startswith("["):
                self.type = EKFlagType.ARRAY
                self.default=json.loads(value)
            elif value.startswith("{"):
                self.type = EKFlagType.OBJECT
                self.default=json.loads(value)
            else:
                self.type = EKFlagType.UNKNOWN
        elif self.type == EKFlagType.INT:
            if value.isdecimal() and "." in value:
                self.type = EKFlagType.FLOAT

    DEBUGVARS_TYPE_MAP: Dict[str, EKFlagType] = {
        "intlist": EKFlagType.INT,
        "stringlist": EKFlagType.STR,
        "numberlist": EKFlagType.FLOAT,
        "int": EKFlagType.INT,
        "boolean": EKFlagType.BOOL,
        "string": EKFlagType.STR,
        "number": EKFlagType.FLOAT,
        "array": EKFlagType.ARRAY,
        "object": EKFlagType.OBJECT,
        "bitflags": EKFlagType.INT,
    }

    def consumeDebugVars(self, data: list) -> None:
        ## from [HGG]classes/coc/view/selfDebug/DebugComponentFactory.as: public static function getComponentFor(name:String, desc:Array, content:* = null):DebugComponent {
        typeid: str = data[0].lower()
        self.type = self.DEBUGVARS_TYPE_MAP.get(typeid, EKFlagType.UNKNOWN)
        if len(data) > 1 and data[1] != "":
            self.notes = data[1]
            if isinstance(self.notes, list):
                if len(self.notes) == 0:
                    self.notes = ""
                else:
                    self.notes = self.notes[0]

        if typeid in ("intlist", "stringlist", "numberlist"):
            """
            ["IntList", "", [
                {label: "None", data: 0},
                {label: "Male", data: 1},
                {label: "Female", data: 2},
                {label: "Herm", data: 3}
            ]],
            """
            if isinstance(data[2][0], (str, int, float)):
                data[2] = [{"label": f"{x}", "data": x} for x in data[2]]
            self.choices = {x["data"]: x["label"] for x in data[2]}
        if typeid == "String":
            """
            ["String", ""]
            """
            assert len(data) == 2
        if typeid == "Boolean":
            """
            ["Boolean", ""],
            """
            assert len(data) == 2
        if typeid == "Int":
            """
            ["Int", ""],
            """
            assert len(data) == 2
        if typeid == "Number":
            """
            ["Number", ""],
            """
            assert len(data) == 2
        if typeid == "Array":
            """
            ["Array", "", ["Int", ""]],
            """
            assert len(data) == 3
            assert len(data[2]) == 2
        if typeid == "Object":
            """
            ["Object", "", {"debugSortOrder": [], ...}],
            """
            assert len(data) == 3
            # assert len(data[2]) == 2
            # TODO
        if typeid == "BitFlag":
            """
            ["BitFlag",
                        ["Tracks progress on her personal quest"],
                        ["Camp", "Marielle", "Bazaar", "Library", "Circe", "Sex", "Finished", "Bought Robe", "Freed Her", "Solved Riddle", "Killed Demon"]
                ],
            """
            assert len(data) == 3
            if isinstance(data[2][0], (str, int, float)):
                data[2] = [
                    {"label": f"{x}", "data": 1 << i} for i, x in enumerate(data[2])
                ]
            self.bitflags = {x["data"]: x["label"] for x in data[2]}

    def write(self, w: IndentWriter) -> None:
        hint: str = self.getTypeHint()
        default: str = self.getDefault()
        end = ")"
        endl = None
        kwargs: str = ""
        if self.readonly:
            kwargs += f", readonly=True"
        if self.unused:
            kwargs += f", unused=True"
        if self.enum is not None:
            kwargs += f", enumType={self.enum}"
        if self.notes is not None:
            kwargs += f", notes={self.notes!r}"
        if self.choices is not None:
            kwargs += ", choices="
            if isinstance(self.choices, list):
                kwargs += "["
                endl = "])"
            if isinstance(self.choices, dict):
                kwargs += "{"
                endl = "})"
            end = ""
        if self.bitflags is not None:
            kwargs += ", bitflags={"
            endl = "})"
            end = ""
        if self.comment != "":
            w.writeline(f"# {self.comment}")
        c = "#" if self.disabled else ""
        # w.writeline(f'{c}self.{self.name}: NPCSelfSaveProperty = self.addSSOProperty({self.name!r}, {hint}, {default}, {self.objid!r}, {self.name!r}{kwargs}{end}')
        w.writeline(
            f"{c}self.addSSOProperty({self.name!r}, {hint}, {default}, {self.objid!r}, {self.name!r}{kwargs}{end}"
        )
        if self.choices is not None:
            with w:
                cl = {}
                if isinstance(self.choices, list):
                    for v in self.choices:
                        w.writeline(f"{v!r},")
                if isinstance(self.choices, dict):
                    for k, v in self.choices.items():
                        w.writeline(f"{k!r}: {v!r},")
            w.writeline(endl)
        if self.bitflags is not None:
            with w:
                cl = {}
                if isinstance(self.bitflags, list):
                    cl = {1 << b: v for b, v in enumerate(self.bitflags)}
                if isinstance(self.bitflags, dict):
                    cl = self.bitflags
                for k, v in cl.items():
                    w.writeline(f"{k!r}: {v!r},")
            w.writeline(endl)

    def writeSSO(self, w: IndentWriter) -> None:
        hint: str = self.getTypeHint()
        default: str = self.getDefault()
        end = ")"
        kwargs: str = ""
        if self.comment != "":
            w.writeline(f"# {self.comment}")
        c = "#" if self.disabled else ""
        func: str = ""
        if self.bitflags is not None:
            kwargs += ", bitflags={"
            end = ""
        if self.choices is not None:
            kwargs += ", {"
            end = ""
            func = "addChoices"
        elif self.type == EKFlagType.INT:
            func = "addInt"
        elif self.type == EKFlagType.INTBOOL:
            func = "addIntBool"
        elif self.type == EKFlagType.STR:
            func = "addStr"
        elif self.type == EKFlagType.BOOL:
            func = "addBoolean"
        elif self.type == EKFlagType.FLOAT:
            func = "addFloat"
        elif self.type == EKFlagType.ARRAY:
            func = "addArray"
        elif self.type == EKFlagType.UNUSED:
            func = "addUnused"
        elif self.type == EKFlagType.OBJECT:
            func = "addObject"
        elif self.type == EKFlagType.UNKNOWN:
            func = "addUnknown"

        if self.debugHelpText is None or self.debugHelpText == "":
            self.debugHelpText = self.notes or ""

        w.writeline(
            f"{c}self.{func}({self.name!r}, {default}, {self.debugHelpText!r}{kwargs}{end}"
        )
        if self.choices is not None:
            with w:
                cl = {}
                if isinstance(self.choices, list):
                    cl = {v: v for v in self.choices}
                if isinstance(self.choices, dict):
                    cl = self.choices
                for k, v in cl.items():
                    w.writeline(f"{k!r}: {v!r},")
            w.writeline("})")
        if self.bitflags is not None:
            with w:
                cl = {}
                if isinstance(self.bitflags, list):
                    cl = {1 << b: v for b, v in enumerate(self.bitflags)}
                if isinstance(self.bitflags, dict):
                    cl = self.bitflags
                for k, v in cl.items():
                    w.writeline(f"{k!r}: {v!r},")
            w.writeline("})")


class FileScanner(object):
    def __init__(self) -> None:
        self.constants: Dict[str, Any] = OrderedDict()
        self.flags: Dict[str, KFlag] = OrderedDict()
        self.dropped_flags: Dict[str, KFlag] = OrderedDict()
        self.ssoprops: Dict[str, SSOProperty] = OrderedDict()
        self.dropped_ssoprops: Dict[str, SSOProperty] = OrderedDict()
        self.sfxprops: Dict[str, StatusEffectProperty] = OrderedDict()
        self.dropped_sfxprops: Dict[str, StatusEffectProperty] = OrderedDict()

        self.fingerprints: List[Tuple[str, str]] = []

        self.npcdata = {}
        self.ssodata = {}
        self.sfxdata = {}
        self.flavor = ""
        self.enums = {}

        self.debugVars: dict = None

        self.in_reset: bool = False
        self.ssoID: str = ""
        self.ssoVersion: int = 0

        self.pregstor: Tuple[str, str, str, str] = (0, 0, 0, 0)
        self.breaststore: Tuple[str] = (0, 0, 0, 0)
        self.ignored_kflags: List[Pattern] = [] + SHIT_TO_IGNORE
        self.whitelisted_kflags: List[Pattern] = []
        self.ignored_sfx: List[Pattern] = [] + DEFAULT_SFX_TO_IGNORE

        self.parse_debug = False

        self.modules: Dict[str, dict] = {}

    def serialize(self) -> dict:
        return {
            "flags": {k: v.serialize() for k, v in self.flags.items()},
            "dropped-flags": {k: v.serialize() for k, v in self.dropped_flags.items()},
            "sso": {k: v.serialize() for k, v in self.ssoprops.items()},
            "dropped-sso": {k: v.serialize() for k, v in self.dropped_ssoprops.items()},
            "sfx": {k: v.serialize() for k, v in self.sfxprops.items()},
            "dropped-sfx": {k: v.serialize() for k, v in self.dropped_sfxprops.items()},
            "fingerprints": self.fingerprints,
            "npcdata": self.npcdata,
            "ssodata": self.ssodata,
            "sfxdata": self.sfxdata,
            "flavor": self.flavor,
            "debug-vars": self.debugVars,
            "ssoID": self.ssoID,
            "ssoVersion": self.ssoVersion,
            "preg-store": self.pregstor,
            "breast-store": self.breaststore,
            "ignored-kflags": [x.pattern for x in self.ignored_kflags],
            "ignored-sfx": [x.pattern for x in self.ignored_sfx],
            "modules": self.modules,
            "enums": self.enums,
        }

    def deserialize(self, data) -> None:
        self.flags = self._loadFlagsFrom(data["flags"])
        self.dropped_flags = self._loadFlagsFrom(data["dropped-flags"])
        self.ssoprops = self._loadSSOPropsFrom(data["sso"])
        self.dropped_ssoprops = self._loadSSOPropsFrom(data["dropped-sso"])
        self.sfxprops = self._loadSFXPropsFrom(data["sfx"])
        self.dropped_sfxprops = self._loadSFXPropsFrom(data["dropped-sfx"])
        self.fingerprints = data["fingerprints"]
        self.npcdata = data["npcdata"]
        self.ssodata = data["ssodata"]
        self.sfxdata = data["sfxdata"]
        self.flavor = data["flavor"]
        self.debugVars = data["debug-vars"]
        self.ssoID = data["ssoID"]
        self.ssoVersion = data["ssoVersion"]
        self.pregstor = data["preg-store"]
        self.breaststore = data["breast-store"]
        self.ignored_kflags = [re.compile(x) for x in data["ignored-kflags"]]
        self.ignored_sfx = [re.compile(x) for x in data["ignored-sfx"]]
        self.modules = data["modules"]
        self.enums = data["enums"]

    def _loadFlagsFrom(self, data: dict) -> Dict[str, KFlag]:
        o = {}
        kf: KFlag
        for k, v in data.items():
            kf = KFlag()
            kf.name = k
            kf.deserialize(v)
            o[k] = kf
        return o

    def _loadSSOPropsFrom(self, data: dict) -> Dict[str, SSOProperty]:
        o = {}
        p: SSOProperty
        for k, v in data.items():
            p = SSOProperty()
            p.name = k
            p.deserialize(v)
            o[k] = p
        return o

    def _loadSFXPropsFrom(self, data: dict) -> Dict[str, StatusEffectProperty]:
        o = {}
        p: StatusEffectProperty
        for k, v in data.items():
            p = StatusEffectProperty()
            p.name = k
            p.deserialize(v)
            o[k] = p
        return o

    def addFile(self, filename: str) -> None:
        ln: int = 0
        self.in_reset = False
        gotClassLine = False
        nFlagsStart: int = len(self.flags)
        nSSOStart: int = len(self.ssoprops)
        nSFXStart: int = len(self.sfxprops)
        nFingerprintsStart: int = len(self.fingerprints)
        with open(filename, "r") as f:
            fc = f.read()
            m = PREG_SSO_ID.search(fc)
            # assert m is not None, f'Could not find SSO ID in {filename}!'
            if m is not None:
                self.handleSSOID(filename, m)
            m = PREG_SSO_VERSION.search(fc)
            # assert m is not None, f'Could not find SSO Version in {filename}!'
            if m is not None:
                self.handleSSOVersion(filename, m)
            for m in PREG_CONST.finditer(fc):
                # print(m)
                self.handleConst(filename, m)
            # print(repr(self.constants))
            for find, repl in self.constants.items():
                fc = fc.replace(find, str(repl))
            m = PREG_DEBUG_VARS.search(fc)
            if m is not None:
                self.handleDebugData(filename, m)
            fc = ""
            f.seek(0)
            for line in f:
                for find, repl in self.constants.items():
                    line = line.replace(find, str(repl))
                sline = line.strip()
                ln += 1
                if line.strip().startswith("public class"):
                    self.fingerprints += [(filename, line.strip())]
                m = PREG_RESET.search(line)
                if m is not None:
                    # print(f'{filename}:{ln}: '+line.strip())
                    self.in_reset = True
                    continue
                if self.in_reset:
                    if line.strip() == "}":
                        # print(f'{filename}:{ln}: '+line.strip())
                        self.in_reset = False
                        continue
                m = PREG_RESET_VAR_COMMENTED.search(line)
                if m is not None:
                    self._handleResetVarCommented(filename, ln, line, m)
                    continue
                m = PREG_RESET_VAR.search(line)
                if m is not None:
                    self._handleResetVar(filename, ln, line, m)
                    continue
                m = PREG_KFLAG_DEF_COMMENTED.match(line)
                if m is not None:
                    self._handleCommentedKFlagDef(filename, ln, line, m)
                    continue
                m = PREG_KFLAG_DEF.match(line)
                if m is not None:
                    self._handleKFlagDef(filename, ln, line, m)
                    continue
                for m in PREG_KFLAG_REF.finditer(line):
                    self._handleKFlagRef(filename, ln, line, m)
                for m in PREG_KFLAG_REF_BINARY_OP.finditer(line):
                    self._handleKFlagRefBinaryOp(filename, ln, line, m)
                for m in PREG_KFLAG_REF_UNARY_OP.finditer(line):
                    self._handleKFlagRefUnaryOp(filename, ln, line, m)
                for m in PREG_KFLAG_REF_IFCHK.finditer(line):
                    self._handleKFlagRefIfCheck(filename, ln, line, m)
                for m in PREG_KFLAG_REF_PREGSTORE.finditer(line):
                    self._handleKFlagRefPregStore(filename, ln, line, m)
                for m in PREG_KFLAG_REF_BREASTSTORE.finditer(line):
                    self._handleKFlagRefBreastStore(filename, ln, line, m)
                # for m in PREG_HAS_SFX.finditer(line):
                #    self._handleHasSFX(filename, ln, line, m)
                for m in PREG_CREATE_SFX.finditer(line):
                    self._handleCreateSFX(filename, ln, line, m)
        print("Found:")
        print(f"  KFlags: {len(self.flags)-nFlagsStart:,}")
        print(f"  SSO Properties: {len(self.ssoprops)-nSSOStart:,}")
        print(f"  Status Effects: {len(self.sfxprops)-nSFXStart:,}")
        print(f"  Fingerprints: {len(self.fingerprints)-nFingerprintsStart:,}")

    def _postprocIgnores(
        self,
        catname: str,
        itemname: str,
        itemdict: dict,
        droppeddict: dict,
        defs: dict,
        ignored: List[Pattern],
        whitelist: List[Pattern],
    ) -> None:
        print(f"{catname}:")
        oldflags = OrderedDict(itemdict)

        itemdict.clear()
        droppeddict.clear()

        ok: int = 0
        dropped: int = 0
        use_whitelist: bool = len(whitelist) > 0
        if use_whitelist:
            print("WHITELIST MODE")
        for k, kf in oldflags.items():
            drop: bool
            if not use_whitelist:
                drop = False
                for badregex in ignored:
                    if badregex.search(k) is not None:
                        print(f"  * Dropped {k!r} (matches {badregex.pattern!r})")
                        drop = True
                        break
            else:
                drop = True
                for goodregex in whitelist:
                    if goodregex.search(k) is not None:
                        drop = False
                        break
                if not drop:
                    print(f"  * Dropped {k!r} (not in whitelist)")
            if not drop:
                ok += 1
                itemdict[k] = kf
            else:
                dropped += 1
                kf.disabled = True
                droppeddict[k] = kf
        print(f"  Passed: {ok}")
        print(f"  Dropped: {dropped}")

    def _postprocDefs(self, itemdict: dict, defs: dict, p_type: Type[object]) -> None:
        nukeDefs = set()
        warnings = set()
        for k, data in defs.items():
            flags = set(x.upper() for x in data.get("flags", []))

            if data.get("new", False):
                flags.add("NEW")
                del data["new"]
                warnings.add(
                    "You are using the old form of `new: yes` instead of `flags: [NEW, ...]`."
                )
            if data.get("readonly", False):
                flags.add("READONLY")
                del data["readonly"]
                warnings.add(
                    "You are using the old form of `readonly: yes` instead of `flags: [READONLY, ...]`."
                )

            if data.get("id") is not None and data.get("key") is not None:
                flags.add("NEW")
                if ":" in k:
                    expectedKey = data["id"] + ":" + str(data["key"])
                    assert (
                        k == expectedKey
                    ), f"Wrong key for {k}: Expected {expectedKey}"
                else:
                    nukeDefs.add(data["id"])
            data["flags"] = list(flags)
            if len(warnings) > 0:
                for w in warnings:
                    log.warning(w)
                print("```")
                yaml.dump({k: data}, sys.stdout)
                print("```")
            if k not in itemdict.keys() and "NEW" in flags:
                p = p_type()
                p.name = k
                if isinstance(p, StatusEffectProperty):
                    p.sfxid = k
                itemdict[p.dictKey] = p
                p.deserialize(data)
                continue
            if k not in itemdict:
                log.error(f"Unable to find key {k} in itemdict.")
                log.error(f"Possibilities: {itemdict.keys()!r}")
            itemdict[k].deserialize(data)
        for k in nukeDefs:
            if k in itemdict:
                del itemdict[k]

    def postProcess(self, kflag_defs: dict, sso_defs: dict, sfx_defs: dict) -> None:
        self._postprocIgnores(
            "KFlags",
            "KFlag",
            self.flags,
            self.dropped_flags,
            kflag_defs,
            self.ignored_kflags,
            self.whitelisted_kflags,
        )
        self._postprocIgnores(
            "Status Effects",
            "Status Effect",
            self.sfxprops,
            self.dropped_sfxprops,
            sfx_defs,
            self.ignored_sfx,
            [],
        )

        for ssoprop in self.ssoprops.values():
            ssoprop.objid = self.ssoID
            if self.debugVars is not None and ssoprop.name in self.debugVars:
                ssoprop.consumeDebugVars(self.debugVars[ssoprop.name])

        self._postprocDefs(self.ssoprops, sso_defs, SSOProperty)
        self._postprocDefs(self.flags, kflag_defs, KFlag)
        self._postprocDefs(self.sfxprops, sfx_defs, StatusEffectProperty)

    def handleConst(self, filename: str, m) -> None:
        if self.parse_debug:
            print(f"{filename}: Const found")
        self.constants[m[1]] = m[2]

    def handleSSOID(self, filename, m) -> None:
        if self.parse_debug:
            print(f"{filename}: SSO ID found")
        self.ssoID = m.group(1)

    def handleSSOVersion(self, filename, m) -> None:
        if self.parse_debug:
            print(f"{filename}: SSO Version found (m={m[1]})")
        self.ssoVersion = int(m.group(1))

    def handleDebugData(self, filename, m) -> None:
        if self.parse_debug:
            print(f"{filename}: SSO Debug Vars found")
        data = m.group(1)

        # Remove block comments
        data = PREG_AS3_BLOCKCOMMENT.sub("", data)
        newlines = []
        for line in data.splitlines():
            # Remove line comments
            line = PREG_AS3_LINECOMMENT.sub("", line)
            line = line.strip()
            if line != "":
                newlines += [line]
        data = "".join(newlines)
        # print(data)
        """
        private var debugVars:Object = {
            flowerExplained: ["Boolean", ""],
            newbornGender: ["IntList", "", [
                {label: "None", data: 0},
                {label: "Male", data: 1},
                {label: "Female", data: 2},
                {label: "Herm", data: 3}
            ]],
            birthTime: ["Int", ""],
            learnedFeeding: ["Boolean", ""],
            eggArray: ["Array", "", ["int", ""]],
            hatchedToday: ["Int", ""],
            tuckedToday: ["Int", ""]
        };
        """
        s = self.strip_comments_from_obj(data)
        print(s)
        if (
            s
            == '{rimmingProgress: ["IntList", "", [{label: "Unavailable", data: 0},{label: "Available", data: 1},{label: "Done", data: 2}]],seenBody: ["Boolean", ""],encounterDay: ["Int", ""],talkedAboutCurse:["Boolean",""]}'
        ):
            s = '{"rimmingProgress":["IntList","",[{"label":"Unavailable","data":0},{"label":"Available","data":1},{"label":"Done","data":2}]],"seenBody":["Boolean",""],"encounterDay":["Int",""],"talkedAboutCurse":["Boolean",""]}'
        f = StringIO(s)
        self.debugVars = yaml.load(f)
        # print(repr(data))

    DELIM_L = "/*"
    DELIM_R = "*/"

    def _strip_comments_from_obj_inner(self, txt: str) -> str:
        "Strips first nest of block comments"
        out = ""
        if self.DELIM_L in txt:
            indx = txt.index(self.DELIM_L)
            out += txt[:indx]
            txt = txt[indx + len(self.DELIM_L) :]
            txt = self._strip_comments_from_obj_inner(txt)
            assert self.DELIM_R in txt, (
                "Cannot find closing comment delimiter in " + txt
            )
            indx = txt.index(self.DELIM_R)
            out += txt[(indx + len(self.DELIM_R)) :]
        else:
            out = txt
        return out

    def strip_comments_from_obj(self, txt: str) -> str:
        "Strips nests of block comments"
        while self.DELIM_L in txt:
            txt = self._strip_comments_from_obj_inner(txt)
        return txt

    def test_strip_comments_from_obj(self) -> None:
        inp: str = '{a: ["bool", ""], /*b: ["bool", ""]*/c: ["int", ""]}'
        expected: str = '{a: ["bool", ""], c: ["int", ""]}'
        actual: str = self.strip_comments_from_obj(inp)
        assert actual == expected

    def _handleKFlagRefPregStore(self, filename, ln, line, m):
        o = []
        for oriface in ("vag", "butt"):
            for datum in ("type", "incub"):
                key: str = m[f"{oriface}_{datum}"]
                if key.startswith("kFLAGS."):
                    key = key[7:]
                    kf: KFlag = None
                    if key in self.flags:
                        kf = self.flags[key]
                    else:
                        kf = KFlag()
                        kf.name = key
                        self.flags[key] = kf
                    kf.type = EKFlagType.INT
                    kf.default = 0
                    if kf.comment == "":
                        datumdesc = {
                            "type": "pregnancy type and notice counter",
                            "incub": "incubation counter",
                        }[datum]
                        kf.comment = f"PregnancyStore {oriface} {datumdesc}"
                    o += [key]
                else:
                    o += [None]
        self.pregstor = tuple(o)
        if self.parse_debug:
            print(f"{filename}:{ln}: Found pregnancy store!")

    def _handleKFlagRefBreastStore(self, filename, ln, line, m):
        o = []
        key: str = m["breasts_flag"]
        if key.startswith("kFLAGS."):
            key = key[7:]
            kf: KFlag = None
            if key in self.flags:
                kf = self.flags[key]
            else:
                kf = KFlag()
                kf.name = key
                self.flags[key] = kf
            kf.type = EKFlagType.INT
            kf.default = 0
            if kf.comment == "":
                kf.comment = f"BreastStore data"
            o += [key]
        else:
            o += [None]
        self.breaststore = tuple(o)
        if self.parse_debug:
            print(f"{filename}:{ln}: Found breast store!")

    def _handleResetVarCommented(self, filename, ln, line, m):
        key: str = m["key"]
        p: SSOProperty = None
        if key in self.ssoprops:
            p = self.ssoprops[key]
        else:
            p = SSOProperty()
            p.name = key
            self.ssoprops[key] = p
        if self.in_reset:
            p.default = m["value"]
            p.comment = m["comment"]
        p.detectType(m["value"])
        if self.parse_debug:
            print(f"{filename}:{ln}: SSO prop {key} defined")

    def _handleResetVar(self, filename, ln, line, m):
        key: str = m["key"]
        p: SSOProperty = None
        if key in self.ssoprops:
            p = self.ssoprops[key]
        else:
            p = SSOProperty()
            p.name = key
            self.ssoprops[key] = p
        if self.in_reset:
            p.default = m["value"]
        p.detectType(m["value"])
        if self.parse_debug:
            print(f"{filename}:{ln}: SSO prop {key} defined")

    def _handleHasSFX(self, filename, ln, line, m):
        key: str = m["attr"]
        p: StatusEffectProperty
        if key in self.sfxprops:
            p = self.sfxprops[key]
        else:
            p = StatusEffectProperty()
            p.name = key
            p.sfxid = key
            p.type = EKFlagType.BOOL
            p.idx_or_key = None
            self.sfxprops[key] = p
        # if self.in_reset:
        #    p.default = m['value']
        if p.idx_or_key is not None:
            p.idx_or_key = None
        if self.parse_debug:
            print(f"{filename}:{ln}: SFX {key} referenced")

    def _handleCreateSFX(self, filename, ln, line, m):
        key: str = m["attr"]
        p: StatusEffectProperty
        if key in self.sfxprops:
            p = self.sfxprops[key]
        else:
            p = StatusEffectProperty()
            p.name = key
            p.sfxid = key
            p.type = EKFlagType.BOOL
            p.idx_or_key = None
            self.sfxprops[key] = p
        # if self.in_reset:
        #    p.default = m['value']
        if p.idx_or_key is not None:
            p.idx_or_key = None
        if self.parse_debug:
            print(f"{filename}:{ln}: SFX {key} created")

    def _handleCommentedKFlagDef(self, filename, ln, line, m):
        key: str = m["kflag"]
        kf: KFlag = None
        if key in self.flags:
            kf = self.flags[key]
        else:
            kf = KFlag()
            kf.name = key
            self.flags[key] = kf
        kf.id = int(m["value"])
        kf.comment = m["comment"]
        if self.parse_debug:
            print(f"{filename}:{ln}: kflag {key} defined")

    def _handleKFlagDef(self, filename, ln, line, m):
        key: str = m["kflag"]
        kf: KFlag = None
        if key in self.flags:
            kf = self.flags[key]
        else:
            kf = KFlag()
            kf.name = key
            self.flags[key] = kf
        kf.id = int(m["value"])
        if self.parse_debug:
            print(f"{filename}:{ln}: kflag {key} defined")

    def _handleKFlagRef(self, filename, ln, line, m):
        key: str = m["kflag"]
        kf: KFlag = None
        if key not in self.flags:
            kf = KFlag()
            kf.name = key
            self.flags[key] = kf
        if self.parse_debug:
            print(f"{filename}:{ln}: kflag {key} referenced")

    def _handleKFlagRefBinaryOp(self, filename, ln, line, m):
        key: str = m["kflag"]
        kf: KFlag = None
        if key in self.flags:
            kf = self.flags[key]
        else:
            kf = KFlag()
            kf.name = key
            self.flags[key] = kf
        # m['op']
        val: str = m["value"]
        if val.startswith('"'):
            kf.type = EKFlagType.STR
        elif val.isdigit():
            kf.type = EKFlagType.INT
        elif val.isdecimal():
            kf.type = EKFlagType.FLOAT
        elif val in ("true", "false"):
            kf.type = EKFlagType.BOOL
        if self.parse_debug:
            print(f"{filename}:{ln}: kflag {key} referenced in binary op")

    def _handleKFlagRefUnaryOp(self, filename, ln, line, m):
        key: str = m["kflag"]
        kf: KFlag = None
        if key in self.flags:
            kf = self.flags[key]
        else:
            kf = KFlag()
            kf.name = key
            self.flags[key] = kf
        # m['op']
        kf.type = EKFlagType.INT
        if self.parse_debug:
            print(f"{filename}:{ln}: kflag {key} referenced in unary op")

    def _handleKFlagRefIfCheck(self, filename, ln, line, m):
        key: str = m["kflag"]
        kf: KFlag = None
        if key in self.flags:
            kf = self.flags[key]
        else:
            kf = KFlag()
            kf.name = key
            self.flags[key] = kf

        if kf.type == EKFlagType.UNKNOWN:
            kf.type = EKFlagType.INT  # Prrrrobably.
        if self.parse_debug:
            print(f"{filename}:{ln}: kflag {key} referenced in if()")


class NPCFile(object):
    def __init__(
        self, clsName: str, npcName: str, npcID: str, flavor: str, gender: str
    ) -> None:
        super().__init__()
        self.clsName: str = clsName
        self.npcName: str = npcName
        self.npcID: str = npcID
        self.aliases: List[str] = []
        self.imports: Dict[str, Optional[str]] = {}
        self.kFlags: List[KFlag] = []
        self.dropped_kFlags: List[KFlag] = []
        self.ssoprops: List[SSOProperty] = []
        self.dropped_ssoprops: List[SSOProperty] = []
        self.sfxprops: List[StatusEffectProperty] = []
        self.dropped_sfxprops: List[StatusEffectProperty] = []

        self.fingerprints: List[Tuple[str, str]] = []
        self.vaginalPregStore: Optional[Tuple[str, str]] = None
        self.analPregStore: Optional[Tuple[str, str]] = None
        self.breastStore: Optional[Tuple[str]] = None
        self.debugVars: dict = {}
        self.flavor: str = flavor
        self.clean: bool = False
        self.sort_by: Optional[str] = None
        self.modules: List[BaseModule] = []
        self.enums: Optional[Dict[str, Any]] = {}

        self.gender: EGender = EGender[gender]

    def sortMethod(self, kf: KFlag) -> Any:
        if self.sort_by == "name":
            return kf.name
        elif self.sort_by == "id":
            return kf.id
        return kf

    def write(self, f: TextIO) -> None:
        w = IndentWriter(f, indent_chars="    ")
        if len(self.enums) > 0:
            for clsID, data in self.enums.items():
                superclsspec = "enum:Enum"
                if data.get("ignore-value", False):
                    superclsspec = "enum:IntEnum"
                superclsspec = data.get("superclass", superclsspec)
                k, v = superclsspec.split(":")
                if k in self.imports:
                    if v not in self.imports[k]:
                        self.imports[k].append(v)
                else:
                    self.imports[k] = [v]
        npcImports: List[str] = ["BaseNPC"]
        if len(self.ssoprops) > 0:
            npcImports += ["NPCSelfSaveProperty"]
        if len(self.kFlags) > 0:
            npcImports += ["NPCKFlagProperty"]
        if len(self.sfxprops) > 0:
            npcImports += ["NPCStatusEffectProperty"]
        npcImports.sort()
        w.writeline("from coctweak.saves._npc import " + ", ".join(npcImports))
        for mod in self.modules:
            mod.writeNPCImports(w)
        kflagsEnum: str = ""
        pregstortype: str = ""
        assert self.flavor in ("hgg", "uee")
        if self.flavor == "hgg":
            kflagsEnum = "HGGKFlags"
            pregstoretype = "HGGFlagPregnancyStore"
            breaststoretype = "HGGBreastStore"
            sfxEnum = "HGGStatusLib"
            w.writeline("from coctweak.saves.hgg.enums.kflags import HGGKFlags")
            if len(self.sfxprops) > 0:
                w.writeline(
                    "from coctweak.saves.hgg.enums.statuslib import HGGStatusLib"
                )
            if self.analPregStore is not None or self.vaginalPregStore is not None:
                w.writeline(
                    "from coctweak.saves.hgg.pregnancystore import HGGFlagPregnancyStore"
                )
            if self.breastStore is not None:
                w.writeline("from coctweak.saves.hgg.breaststore import HGGBreastStore")
        elif self.flavor == "uee":
            kflagsEnum = "UEEKFlags"
            pregstoretype = "UEEFlagPregnancyStore"
            sfxEnum = "UEEStatusLib"
            w.writeline("from coctweak.saves.uee.enums.kflags import UEEKFlags")
            if len(self.sfxprops) > 0:
                w.writeline(
                    "from coctweak.saves.uee.enums.statuslib import UEEStatusLib"
                )
            if self.analPregStore is not None or self.vaginalPregStore is not None:
                w.writeline(
                    "from coctweak.saves.uee.pregnancystore import UEEFlagPregnancyStore"
                )
        for k, v in self.imports.items():
            if v is None:
                w.writeline(f"import {k}")
            else:
                if isinstance(v, list):
                    v = ", ".join(sorted(v))
                w.writeline(f"from {k} import {v}")

        # Setup logging
        w.writeline("from coctweak.logsys import getLogger # isort: skip")
        w.writeline("log = getLogger(__name__)")

        for clsID, enumdata in self.enums.items():
            superclsspec = "enum:Enum"
            if enumdata.get("ignore-value", False):
                superclsspec = "enum:IntEnum"
            superclsspec = enumdata.get("superclass", superclsspec)
            imports = enumdata.get("imports", [])
            supercls = superclsspec.split(":")[-1]
            flags = {}
            for k, v in enumdata.get("values", {}).items():
                flags[str(k)] = str(v)
            with w.writeline(f"class {clsID}({supercls}):"):
                maxlen = max([len(x) for x in flags.keys()])
                for k, v in flags.items():
                    padding = " " * (maxlen + 2 - len(k))
                    w.writeline(f"{k}{padding}= {v}")

        extends = ", ".join(
            ["BaseNPC"]
            + [x.getNPCMixin() for x in self.modules if x.getNPCMixin() is not None]
        )
        with w.writeline(f"class {self.clsName}({extends}):"):
            w.writeline(f"ID = {self.npcID!r}")
            if len(self.aliases) > 0:
                w.writeline(f"ALIASES = {self.aliases!r}")
            w.writeline(f"NAME = {self.npcName!r}")
            with w.writeline("def __init__(self, save) -> None:"):
                if len(self.fingerprints) > 0:
                    for filename, clsLine in sorted(self.fingerprints):
                        filename = os.path.relpath(filename, start=".")
                        if filename.startswith("." + os.sep):
                            filename = filename[2:]
                        filename = replacePathWithPrefix(filename)
                        filename = filename.replace(os.sep, "/")
                        w.writeline(f"## from {filename}: {clsLine}")
                w.writeline("super().__init__(save)")
                for mod in self.modules:
                    mod.writeNPCInit(w, kflagsEnum)
                if len(self.ssoprops) > 0:
                    w.writeline("#############")
                    w.writeline("# SSO Props")
                    w.writeline("#############")
                    if self.debugVars is not None:
                        w.writeline("'''")
                        for line in json.dumps(self.debugVars, indent=4).splitlines():
                            w.writeline(line)
                        w.writeline("'''")
                    ssoprops = self.ssoprops
                    if self.sort_by is not None:
                        ssoprops = sorted(ssoprops, key=lambda p: p.name)
                    for p in ssoprops:
                        p.write(w)
                    if len(self.dropped_ssoprops) > 0:
                        w.writeline("# Dropped")
                        ssoprops = self.dropped_ssoprops
                        if self.sort_by is not None:
                            ssoprops = sorted(ssoprops, key=self.sortMethod)
                        for p in ssoprops:
                            p.write(w)

                if len(self.kFlags) > 0:
                    w.writeline("##########")
                    w.writeline("# kFlags")
                    w.writeline("##########")
                    kflags = self.kFlags
                    if self.sort_by is not None:
                        kflags = sorted(kflags, key=self.sortMethod)
                    for kf in kflags:
                        kf.write(w, kflagsEnum)
                    if len(self.dropped_kFlags) > 0:
                        w.writeline("# Dropped")
                        kflags = self.dropped_kFlags
                        if self.sort_by is not None:
                            kflags = sorted(kflags, key=self.sortMethod)
                        for kf in kflags:
                            kf.write(w, kflagsEnum)
                if len(self.sfxprops) > 0:
                    w.writeline("##################")
                    w.writeline("# Status Effects")
                    w.writeline("##################")
                    props = self.sfxprops
                    if self.sort_by is not None:
                        props = sorted(props, key=lambda p: p.name)
                    for p in props:
                        p.write(w, sfxEnum)
                    if len(self.dropped_sfxprops) > 0:
                        w.writeline("# Dropped")
                        kflags = self.dropped_sfxprops
                        props = sorted(props, key=lambda p: p.name)
                        for p in props:
                            p.write(w, sfxEnum)
                if self.analPregStore is not None or self.vaginalPregStore is not None:
                    w.writeline("###################")
                    w.writeline("# PregnancyStores")
                    w.writeline("###################")
                    if self.analPregStore is not None:
                        typeflag, incubationflag = self.analPregStore
                        w.writeline(
                            f"self.analPregnancy = {pregstoretype}(save, {kflagsEnum}.{typeflag}, {kflagsEnum}.{incubationflag})"
                        )
                    if self.vaginalPregStore is not None:
                        typeflag, incubationflag = self.vaginalPregStore
                        w.writeline(
                            f"self.vaginalPregnancy = {pregstoretype}(save, {kflagsEnum}.{typeflag}, {kflagsEnum}.{incubationflag})"
                        )

                if self.breastStore is not None:
                    w.writeline("################")
                    w.writeline("# BreastStores")
                    w.writeline("################")
                    dataflag = self.breastStore[0]
                    isMale = self.gender == EGender.MALE
                    w.writeline(
                        f"self.breasts = {breaststoretype}(save, {isMale}, {kflagsEnum}.{dataflag})"
                    )


class SSOFile(object):
    def __init__(self, flavor: str, clsName: str, objID: str) -> None:
        super().__init__()
        self.flavor: str = flavor
        self.clsName: str = clsName
        self.ssoID: str = objID
        self.ssoprops: List[SSOProperty] = []
        self.dropped_ssoprops: List[SSOProperty] = []
        self.fingerprints: List[Tuple[str, str]] = []
        self.debugVars: dict = {}

        self.oldest_version: int = 0
        self.newest_version: int = 0
        self.uuid: str = ""
        self.clean: bool = False
        self.sort_by: Optional[str] = None

        self.modules: List[BaseModule] = []

    def write(self, f: TextIO) -> None:
        w = IndentWriter(f, indent_chars="    ")
        w.writeline("from coctweak.saves._selfsaving import SelfSavingObject")
        for mod in self.modules:
            mod.writeSSOImports(w)
        # from enum import IntEnum
        stamp: str = ""
        if self.flavor == "hgg":
            stamp = f"HGGSerializationVersion(({self.oldest_version!r}, {self.newest_version!r}))"
            w.writeline(
                "from coctweak.saves.hgg.serialization import HGGSerializationVersion"
            )
        elif self.flavor == "uee":
            stamp = f"UEESerializationVersion({self.uuid!r}, {self.oldest_version!r}, {self.newest_version!r}))"
            w.writeline(
                "from coctweak.saves.hgg.serialization import UEESerializationVersion"
            )
        with w.writeline(f"class {self.clsName}(SelfSavingObject):"):
            w.writeline(f"SERIALIZATION_STAMP = {stamp}")
            with w.writeline("def __init__(self) -> None:"):
                w.writeline(f"super().__init__({self.ssoID!r})")
                if len(self.fingerprints) > 0:
                    for filename, clsLine in sorted(self.fingerprints):
                        filename = os.path.relpath(filename, start=".")
                        if filename.startswith("." + os.sep):
                            filename = filename[2:]
                        filename = replacePathWithPrefix(filename)
                        filename = filename.replace(os.sep, "/")
                        w.writeline(f"## from {filename}: {clsLine}")
                if self.debugVars is not None:
                    w.writeline("'''")
                    for line in json.dumps(self.debugVars, indent=4).splitlines():
                        w.writeline(line)
                    w.writeline("'''")
                ssoprops = self.ssoprops
                if self.sort_by is not None:
                    ssoprops = sorted(ssoprops, key=lambda p: p.name)
                for p in ssoprops:
                    p.writeSSO(w)
                if len(self.dropped_ssoprops) > 0:
                    w.writeline("# Dropped")
                    ssoprops = self.dropped_ssoprops
                    if self.sort_by is not None:
                        ssoprops = sorted(ssoprops, key=self.sortMethod)
                    for p in ssoprops:
                        p.writeSSO(w)


def main():
    PY_SAVES_DIR: Path = Path("coctweak") / "saves"
    NPCS_DIR: Path = Path("data") / "npcs"

    HGG_PY_DIR: Path = PY_SAVES_DIR / "hgg"
    HGG_DATA_DIR: Path = NPCS_DIR / "hgg"
    HGG_NPC_YML_TMP = str(HGG_DATA_DIR / "{0}.yml")
    HGG_NPC_PY_TMP = str(HGG_PY_DIR / "npcs" / "{0}.new.py")
    HGG_SSO_PY_TMP = str(HGG_PY_DIR / "sso" / "{0}.new.py")

    bm = BuildMaestro(hidden_build_dir=".build/prebuildNPCs_py")
    if "--rebuild" in sys.argv or "--clean" in sys.argv:
        for targetfile in Path(HGG_NPC_PY_TMP.format("x")).parent.glob("*.new.py"):
            if bm.colors:
                log.info("<red>RM</red> %s", targetfile)
            else:
                log.info("RM %s", targetfile)
            os.remove(targetfile)
        for targetfile in Path(HGG_SSO_PY_TMP.format("x")).parent.glob("*.new.py"):
            if bm.colors:
                log.info("<red>RM</red> %s", targetfile)
            else:
                log.info("RM %s", targetfile)
            os.remove(targetfile)
    HGG_NPCs = []
    HGG_SSOs = []

    def addNPC(
        modid,
        name,
        npc_py_tmp,
        sso_py_tmp,
        npc_yml_tmp,
        NPCs: List[str],
        SSOs: List[str],
        npc: bool = True,
        sso: bool = False,
    ):
        datafile = os.path.join("tmp", "npcdata", modid, f"{name}.yml")
        datapath = Path(datafile)
        datapath.parent.mkdir(parents=True, exist_ok=True)
        bm.add(ParseNPCFromAS(datafile, npc_yml_tmp.format(name)))
        if npc:
            NPCs += [
                bm.add(
                    GenNPCFromYAML(
                        npc_py_tmp.format(name.lower()),
                        datafile,
                        dependencies=[datafile],
                    )
                ).target
            ]
        if sso:
            SSOs += [
                bm.add(
                    GenSSOFromYAML(
                        sso_py_tmp.format(name.lower()),
                        datafile,
                        dependencies=[datafile],
                    )
                ).target
            ]

    hgg_npc_index: Path = HGG_DATA_DIR / "__INDEX.yml"
    with hgg_npc_index.open("r") as f:
        for npcid, npccfg in yaml.load(f).items():
            addNPC(
                "hgg",
                npcid,
                HGG_NPC_PY_TMP,
                HGG_SSO_PY_TMP,
                HGG_NPC_YML_TMP,
                HGG_NPCs,
                HGG_SSOs,
                **npccfg,
            )

    bm.add(
        GenNPCIndexTarget(
            os.path.join("coctweak", "saves", "hgg", "npcs"), dependencies=HGG_NPCs
        )
    )
    bm.add(
        GenSSOIndexTarget(
            os.path.join("coctweak", "saves", "hgg", "sso"), dependencies=HGG_SSOs
        )
    )

    bm.as_app()


def parseFromNPCDef(inymlfile: str, outymlfile: str) -> None:
    data = {}
    with open(inymlfile, "r") as f:
        data = yaml.load(f)
        assert data["version"] == 1

    clean = data.get("clean", False)

    parseropts: Dict[str, Any] = data.get("parsing", {})

    fs = FileScanner()
    fs.constants = parseropts.get("constants", {})
    fs.ssoID = data.get("sso", {}).get("id")
    fs.npcdata = data.get("npc", None)
    fs.ssodata = data.get("sso", None)
    fs.sfxdata = data.get("sfx", None)
    fs.enums = data.get("enums", {})
    if "pregnancy-stores" in data:
        ps = data["pregnancy-stores"]
        if fs.pregstor is None:
            fs.pregstor = (None, None, None, None)
        fsps = list(fs.pregstor)
        if "vaginal" in ps:
            vag = ps["vaginal"]
            fsps[0] = vag.get("type")
            fsps[1] = vag.get("incubation")
        if "anal" in ps:
            anal = ps["anal"]
            fsps[2] = anal.get("type")
            fsps[3] = anal.get("incubation")
        fs.pregstor = tuple(fsps)
    fs.flavor = data["flavor"]
    for entry in data.get("kflags", {}).get("ignore", []):
        if all((c.isalnum() or c == "_") for c in entry):
            fs.ignored_kflags += [literal(entry)]
        else:
            fs.ignored_kflags += [re.compile(entry)]
    for entry in data.get("kflags", {}).get("whitelist", []):
        if all((c.isalnum() or c == "_") for c in entry):
            fs.whitelisted_kflags += [literal(entry)]
        else:
            fs.whitelisted_kflags += [re.compile(entry)]

    for entry in data.get("sfx", {}).get("ignore", []):
        if all((c.isalnum() or c == "_") for c in entry):
            fs.ignored_sfx += [literal(entry)]
        else:
            fs.ignored_sfx += [re.compile(entry)]

    for filename in data["files"]:
        print(f"Scanning {filename}...")
        fs.addFile(filename)

    fs.postProcess(
        data.get("kflags", {}).get("defs", {}),
        data.get("sso", {}).get("defs", {}),
        data.get("sfx", {}).get("defs", {}),
    )
    with open(outymlfile, "w") as f:
        yaml.dump(fs.serialize(), f)


def genNPC(inymlfile: str, outpyfile: str, sort_by: str = "name") -> None:
    fs = FileScanner()
    with open(inymlfile, "r") as f:
        fs.deserialize(yaml.load(f))
    npcdata = fs.npcdata
    gender = npcdata.get("gender", "unknown").upper()
    npc = NPCFile(npcdata["class"], npcdata["name"], npcdata["id"], fs.flavor, gender)
    npc.aliases = npcdata.get("aliases", [])
    npc.sort_by = sort_by
    npc.kFlags = fs.flags.values()
    npc.dropped_kFlags = []  # fs.dropped_flags.values() if not clean else []
    npc.ssoprops = fs.ssoprops.values()
    npc.dropped_ssoprops = []  # fs.dropped_ssoprops.values() if not clean else []
    npc.sfxprops = fs.sfxprops.values()
    npc.dropped_sfxprops = []  # fs.dropped_ssoprops.values() if not clean else []
    npc.debugVars = fs.debugVars
    npc.fingerprints = fs.fingerprints
    npc.vaginalPregStore = (
        (fs.pregstor[0], fs.pregstor[1])
        if fs.pregstor[0] not in (None, "0", 0)
        else None
    )
    npc.analPregStore = (
        (fs.pregstor[2], fs.pregstor[3])
        if fs.pregstor[2] not in (None, "0", 0)
        else None
    )
    npc.breastStore = (
        (fs.breaststore[0],) if fs.breaststore[0] not in (None, "0", 0) else None
    )
    npc.imports = npcdata.get("imports", {})
    npc.enums = fs.enums
    npc.modules = []
    for modname, moddata in npcdata.get("modules", {}).items():
        mod: BaseModule = BaseModule.ALL[modname](npc=npc)
        mod.deserialize(moddata)
        npc.modules.append(mod)

    if os.path.isfile(outpyfile):
        mkWritable(Path(outpyfile))

    with open(outpyfile, "w") as f:
        npc.write(f)
    hashPythonFile(outpyfile, rehash=True)


def genSSO(inymlfile: str, outpyfile: str, sort_by: str = "name") -> None:
    fs = FileScanner()
    with open(inymlfile, "r") as f:
        fs.deserialize(yaml.load(f))
    ssodata = fs.ssodata
    sso = SSOFile(fs.flavor, ssodata["class"], fs.ssoID)
    sso.newest_version = fs.ssoVersion
    sso.sort_by = sort_by
    sso.ssoprops = fs.ssoprops.values()
    sso.dropped_ssoprops = []  # fs.dropped_ssoprops.values() if not clean else []
    sso.debugVars = fs.debugVars
    sso.fingerprints = fs.fingerprints
    for modname, moddata in ssodata.get("modules", {}).items():
        mod: BaseModule = BaseModule.ALL[modname](sso=sso)
        mod.deserialize(moddata)
        sso.modules.append(mod)
    if os.path.isfile(outpyfile):
        mkWritable(Path(outpyfile))
    with open(outpyfile, "w") as f:
        sso.write(f)
    hashPythonFile(outpyfile, rehash=True)


if __name__ == "__main__":
    main()
