import os
from pathlib import Path

from ruamel.yaml import YAML

from _dev_regex import PREG_HGG_SELF_SAVING_SIG

yaml = YAML(typ='rt')

from buildtools.indentation import IndentWriter


def main():
    import argparse

    argp = argparse.ArgumentParser()
    argp.add_argument('--clobber', action='store_true', default=False, help="Overwrite existing YML.")
    argp.add_argument('--sso', action='store_true', default=False, help="Expect self-saving objects.")
    argp.add_argument('flavor', choices=['hgg', 'uee', 'vanilla'], help='Which mod/flavor of CoC you\'re working on.')
    argp.add_argument('name', type=str, help='NPC\'s name.  Should be capitalized. (Amily, HelSpawn)')
    argp.add_argument('files', metavar='/path/to/file.as', nargs='+', help="List of *.as files containing NPC info.")

    args = argp.parse_args()

    ymlfile = Path('data') / 'npcs' / args.flavor / (args.name+'.yml')
    if not args.clobber and ymlfile.is_file():
        print('Exists. Force overwrite via --clobber.')
        return

    foundSelfSaving: bool = False
    for filename in args.files:
        filepath = Path(filename)
        if not filepath.is_file():
            print(f'Input file {filename!r} does not exist!')
            return
        contents = filepath.read_text()
        if PREG_HGG_SELF_SAVING_SIG.search(contents) is not None:
            print('Found HGG SSO signature.')
            foundSelfSaving = True
            #break

    with ymlfile.open('w') as f:
        w = IndentWriter(f, indent_chars='  ')
        w.writeline('version: 1')
        w.writeline(f'flavor: {args.flavor!r}')
        with w.writeline('npc:'):
            w.writeline(f'id: {args.name.lower()}')
            w.writeline(f'name: {args.name}')
            w.writeline(f'class: {args.name}NPC')
        with w.writeline('sso:'):
            w.writeline(f'class: {args.name}SSO')
            w.writeline('defs: {}')
        with w.writeline('kflags:'):
            w.writeline('defs: {}')
            w.writeline('ignore: []')
        with w.writeline('sfx:'):
            w.writeline('defs: {}')
            w.writeline('ignore: []')
        with w.writeline('pregnancy-stores:'):
            with w.writeline('anal:'):
                w.writeline('type: null')
                w.writeline('incubation: null')
            with w.writeline('vaginal:'):
                w.writeline('type: null')
                w.writeline('incubation: null')
            w.writeline('ignore: []')
        w.writeline('enums: {}')
        w.writeline('files:')
        for filename in args.files:
            w.writeline('- '+os.path.relpath(os.path.abspath(str(filename)), start='.'))
    print('YAML written.')

    indexyml = Path('data') / 'npcs' / args.flavor / '__INDEX.yml'
    found = False
    with indexyml.open('r') as f:
        data = yaml.load(f)
        if args.name in data.keys():
            found = True
    if found:
        print('Already exists in __INDEX.yml, skipping addition.')
    else:
        with indexyml.open('a') as f:
            ssotf = 'true' if foundSelfSaving else 'false'
            f.write(f'{args.name}: {{sso: {ssotf}}}\n')
        print('Added to __INDEX.yml.')

    newpy = Path('coctweak') / 'saves' / args.flavor / 'npcs' / f'{args.name.lower()}.py'
    if newpy.is_file():
        print(f'New file expected at {newpy} exists.')
    else:
        with newpy.open('w') as f:
            f.write(f'# STUB for prebuildNPCs.py\n')
            f.write(f'from coctweak.saves._npc import BaseNPC\n')
            f.write(f'class {args.name}NPC(BaseNPC):\n')
            f.write(f'    ID = {args.name.lower()!r}\n')
            f.write(f'    NAME = {args.name!r}\n')
        print(f'Touched {newpy}.')

    if args.sso:
        newpy = Path('coctweak') / 'saves' / args.flavor / 'sso' / f'{args.name.lower()}.py'
        if newpy.is_file():
            print(f'New file expected at {newpy} exists.')
        else:
            with newpy.open('w') as f:
                f.write(f'# STUB for prebuildNPCs.py\n')
                f.write('from coctweak.saves._selfsaving import SelfSavingObject\n')
                f.write(f'class {args.name}SSO(SelfSavingObject):\n')
                f.write('    pass\n')
            print(f'Touched {newpy}.')
    print(f'Run `python devtools/prebuildNPCs.py && python devtools/cmpNPCs.py {args.flavor} {args.name.lower()}`')

if __name__ == '__main__':
    main()
