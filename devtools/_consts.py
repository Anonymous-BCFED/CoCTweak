from typing import Final, FrozenSet


START_YEAR: Final[int] = 2019  # Change if you are forking.

SUPPORT_TABLE_START: Final[str] = (
    "<!-- devtools/updateSupportTable.py replaces this: -->"
)
SUPPORT_TABLE_END: Final[str] = "<!-- devtools/updateSupportTable.py END -->"

# Run trimPackageOpts.py to get a new value for this.
NUITKA_FORBIDDEN_IMPORTS: FrozenSet[str] = frozenset(
    {
        "__main__",
        "_frozen_importlib",
        "_frozen_importlib_external",
        "cython_runtime",
        "importlib._bootstrap",
        "importlib._bootstrap_external",
        "pkg_resources.extern.appdirs",
        "pkg_resources.extern.jaraco",
        "pkg_resources.extern.jaraco.context",
        "pkg_resources.extern.jaraco.functools",
        "pkg_resources.extern.jaraco.text",
        "pkg_resources.extern.more_itertools",
        "pkg_resources.extern.packaging",
        "pkg_resources.extern.packaging._structures",
        "pkg_resources.extern.packaging.markers",
        "pkg_resources.extern.packaging.requirements",
        "pkg_resources.extern.packaging.specifiers",
        "pkg_resources.extern.packaging.tags",
        "pkg_resources.extern.packaging.utils",
        "pkg_resources.extern.packaging.version",
        "pkg_resources.extern.pyparsing",
        "pyexpat.errors",
        "pyexpat.model",
        "pygit2._libgit2.lib",
        "pygit2.remote",
        "pygit2.submodule",
        "six.moves",
        "xml.parsers.expat.errors",
        "xml.parsers.expat.model",
        "zipimport",
    }
)
