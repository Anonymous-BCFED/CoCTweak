import filecmp
import os
from pathlib import Path
import stat
from typing import List

from buildtools import log, os_utils

MELD: str = os_utils.assertWhich('meld')

FLAVORS: List[str] = ['hgg']

MODULE_DIR = Path('coctweak')
SAVES_MOD_DIR = MODULE_DIR / 'saves'

def main():
    import argparse

    argp = argparse.ArgumentParser()
    argp.add_argument('flavor', choices=FLAVORS+['all'], default='all')
    argp.add_argument('npcids', type=str, nargs='*', default=['all'])
    argp.add_argument('--remove-broken-ssos', action='store_true', default=False)
    args: argparse.Namespace = argp.parse_args()

    flavors = [args.flavor]
    if args.flavor.lower() in ('*', 'all'):
        flavors = FLAVORS

    npcids = []
    if len(args.npcids) > 0 and ('all' not in args.npcids):
        npcids += args.npcids
    else:
        for flavor in flavors:
            with log.info(f'{flavor}:'):
                for npcid in find_npcids(flavor):
                    compareNPC(flavor, npcid, remove_broken_ssos=args.remove_broken_ssos)
        return

    with log.info(f'{args.flavor}:'):
        for npcid in npcids:
            compareNPC(args.flavor, npcid,
                       remove_broken_ssos=args.remove_broken_ssos)

def find_npcids(flavor: str) -> List[str]:
    npcids = set()
    for filepath in (SAVES_MOD_DIR / flavor.lower() / 'npcs').glob('*.py'):
        if filepath.name in ('__INDEX', '__init__'):
            continue
        if filepath.stem.endswith('.new'):
            continue
        suffix = ''.join(filepath.suffixes)
        if suffix in ('.py', '.new.py'):
            npcids.add(filepath.stem)
    return list(npcids)

def mkReadOnly(filepath: Path) -> None:
    umask = os.umask(0)
    os.umask(umask)

    perms: int = filepath.stat().st_mode
    #log.info(f'{filepath}: BEFORE: 0o{perms:o}')
    perms &= ~(stat.S_IWUSR|stat.S_IWGRP|stat.S_IWOTH)
    #perms &= umask
    log.info(f'{filepath}: chmod: 0o{perms:o}')

    filepath.chmod(perms)

def mkWritable(filepath: Path) -> None:
    umask = os.umask(0)
    os.umask(umask)

    perms: int = filepath.stat().st_mode
    #log.info(f'{filepath}: BEFORE: 0o{perms:o}')
    perms |= (stat.S_IWUSR|stat.S_IWGRP)
    #perms &= umask
    log.info(f'{filepath}: chmod: 0o{perms:o}')

    filepath.chmod(perms)

def compareNPC(flavor: str, npcid: str, remove_broken_ssos: bool = False) -> None:
    with log.info(f'{npcid}:'):
        FLAVOR_DIR = SAVES_MOD_DIR / flavor.lower()
        NPCS_DIR = FLAVOR_DIR / 'npcs'
        oldnpcfile = NPCS_DIR / f'{npcid}.py'
        newnpcfile = NPCS_DIR / f'{npcid}.new.py'
        failure = False
        if not oldnpcfile.is_file() and not newnpcfile.is_file():
            log.info(f'{oldnpcfile} and {newnpcfile} do not exist. Skipping.')
            failure = True
        if not failure and not oldnpcfile.is_file() and newnpcfile.is_file():
            with log.info(f'{oldnpcfile} does not exist, but {newnpcfile} does. Copying new file.'):
                os_utils.single_copy(str(newnpcfile), str(oldnpcfile), verbose=True)
            failure = True
        if not failure and not newnpcfile.is_file():
            log.warning(f'{newnpcfile} does not exist. Not comparing NPC files.')
            failure = True
        if not failure and filecmp.cmp(oldnpcfile, newnpcfile):
            log.info(f'{oldnpcfile} and {newnpcfile} match.  Skipping.')
            failure = True
        if not failure:
            mkReadOnly(newnpcfile)
            os_utils.cmd([MELD, str(newnpcfile), str(oldnpcfile)], echo=True)
            mkWritable(newnpcfile)
        SSO_DIR = FLAVOR_DIR / 'sso'
        oldssofile = SSO_DIR / f'{npcid}.py'
        newssofile = SSO_DIR / f'{npcid}.new.py'
        failure = False
        if not oldssofile.is_file() and not newssofile.is_file():
            log.info(f'{oldssofile} and {newssofile} do not exist. Skipping.')
            failure = True
        if not failure and not oldssofile.is_file() and newssofile.is_file():
            with log.info(f'{oldssofile} does not exist, but {newssofile} does. Copying new file.'):
                os_utils.single_copy(str(newssofile), str(oldssofile), verbose=True)
            failure = True
        if not failure and not newssofile.is_file():
            if remove_broken_ssos:
                log.warning(f'Removing broken SSO {oldssofile}...')
                oldssofile.unlink()
            else:
                log.warning(f'{newssofile} does not exist. Not comparing SSO files.')
            failure = True
        if not failure and filecmp.cmp(oldssofile, newssofile):
            log.info(f'{oldssofile} and {newssofile} match.  Skipping.')
            failure = True
        if not failure:
            mkReadOnly(newssofile)
            os_utils.cmd([MELD, str(newssofile), str(oldssofile)], echo=True)
            mkWritable(newssofile)

if __name__ == '__main__':
    main()
