import os

from buildtools import BuildEnv, os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.web import DartSCSSBuildTarget, UglifyJSTarget

from coctweak_build.targets.markdown import renderDoc


def add_testscripts_buildtargets(bm: BuildMaestro, env: BuildEnv, dependencies=[]):
    env.prependTo("PATH", os.path.join("node_modules", ".bin"))
    SASS = env.assertWhich("sass")
    UGLIFYJS = env.assertWhich("uglifyjs")
    js = bm.add(
        UglifyJSTarget(
            os.path.join("tmp", "rendered.min.js"),
            os.path.join("docs-src", "rendered.js"),
            uglify_executable=UGLIFYJS,
            compress=True,
            mangle=True,
            dependencies=dependencies,
        )
    ).target
    css = bm.add(
        DartSCSSBuildTarget(
            os.path.join("tmp", "rendered.min.css"),
            [os.path.join("docs-src", "rendered.scss")],
            output_style="compressed",
            sass_path=SASS,
            dependencies=dependencies,
        )
    ).target
    renderDoc(
        bm,
        os.path.join("development", "samples"),
        "Sample Generation Scripts",
        deps=[js, css],
        jsfile=js,
        cssfile=css,
    )


def main():
    env = os_utils.ENV.clone()
    bm = BuildMaestro(hidden_build_dir=".build/buildDevScripts_py/")
    add_testscripts_buildtargets(bm, env)
    bm.as_app()


if __name__ == "__main__":
    main()
