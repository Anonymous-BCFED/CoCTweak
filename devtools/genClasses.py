import collections
from enum import auto, IntEnum, IntFlag
import os
import re
import sys
from typing import Any, Dict, List, Optional, Tuple, Union

from buildtools import log, os_utils
from buildtools.config import YAMLConfig
from buildtools.indentation import IndentWriter
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
import requests
from ruamel.yaml import YAML

from _dev_regex import *
from coctweak_build.classes_stuff import VARIABLES
from coctweak_build.utils import downloadIfChanged

yaml = YAML(typ="rt")


def fixYAMLBool(inp: Union[bool, str]) -> bool:
    if isinstance(inp, bool):
        return inp
    elif isinstance(inp, str):
        if inp in ("true", "True", "yes", "y"):
            return True
        elif inp in ("false", "False", "no", "n"):
            return False
        else:
            raise AttributeError(f"Unrecognized string {inp!r}")
    else:
        raise AttributeError(f"Oh shit what are you doing {inp!r}")


assert PREG_VAR.match("public var analLooseness:Number = 0;") is not None


def _join(posix_path: str) -> str:
    return os.path.join(*posix_path.split("/"))


class ESerializeTo(IntEnum):
    OBJECT = auto()
    ROOT = auto()

    @staticmethod
    def parse(flagname: str) -> "ESerializeTo":
        assert isinstance(flagname, str)
        return ESerializeTo[flagname.replace("-", "_").upper()]


ALL_CLASSES = {}


class Variable:
    def __init__(self, m:re.Match=None):
        self.attr: str = ""
        self.id: str = ""
        self.type: str = ""
        self.pytype: str = ""
        self.solname: str = ""
        self.default: str = ""
        self.deserializeMethod: str = ""
        self.value: str = ""
        self.ignore: bool = False
        self.enum: str = ""
        self.enumType: Optional[Tuple[str, str]] = None  # enum, Enum/IntEnum
        self.useenumval: bool = True
        self.serializeTo: ESerializeTo = ESerializeTo.OBJECT
        if m is not None:
            # print(repr(m.groups()))
            self.attr = m[1]
            self.id = m[2]
            self.type = m[3]
            self.value = m[4]
            self.solname = self.id
            self.updateTypeDependants()
            if len(m.groups()) > 4:
                self.default = m[4]

    def postProcess(self, rawFile: "RawFile")->None:
        if self.enum != "":
            self.enumType = ("enum", "IntEnum" if self.type == "int" else "Enum")
        if self.enum.startswith("self."):
            enumMdl, enumCls = self.enumType
            v = Variable()
            v.attr = v.id = self.enum[5:]
            v.pytype = f"Type[{self.enumType[1]}]"
            v.default = None
            rawFile.statics[v.id] = v
            found = False
            newImports = [{"typing": "Type"}]
            for entry in rawFile.imports:
                newImport = None
                if isinstance(entry, dict):
                    newImport = {}
                    for k, v in entry.items():
                        if k == enumMdl:
                            if isinstance(v, str):
                                if v == enumCls:
                                    found = True
                                    newImport[k] = v
                                    break
                                newImport[k] = [v, enumCls]
                                found = True
                                break
                            if isinstance(v, list):
                                if enumCls in v:
                                    found = True
                                    newImport[k] = v
                                    break
                                newImport[k] = v + [enumCls]
                                found = True
                                break
                        else:
                            newImport[k] = v
                else:
                    newImport = entry
                newImports += [newImport]
            if not found:
                newImports += [{enumMdl: enumCls}]
            rawFile.imports = newImports
            # log.info(repr(rawFile.imports))

    def updateTypeDependants(self)->None:
        self.pytype = self.type
        self.default = "None"
        self.deserializeMethod = "data.get"

        if self.type == "int":
            self.default = 0
            self.deserializeMethod = "self._getInt"
        elif self.type == "String":
            self.default = ""
            self.pytype = "str"
            self.deserializeMethod = "self._getStr"
        elif self.type == "float":
            self.default = 0.0
            self.pytype = "float"
            self.deserializeMethod = "self._getFloat"
        elif self.type == "Number":
            self.default = 0.0
            self.pytype = "float"
            self.deserializeMethod = "self._getFloat"
        elif self.type == "Boolean":
            self.default = False
            self.pytype = "bool"
            self.deserializeMethod = "self._getBool"
        elif self.type == "Array":
            self.default = []
            self.pytype = "list"
        elif self.type == "Function":
            self.ignore = True
            self.comment = "Cannot deserialize type {!r}".format(self.type)
        else:
            self.pytype = "Any"

    def canSerializeTo(self, value: ESerializeTo) -> bool:
        return (self.serializeTo & value) == value

    def deserialize(self, data: dict) -> None:
        self.attr = data.get("attr", self.attr)
        self.id = data.get("id", self.id)
        self.type = data.get("type", self.type)
        if "type" in data:
            self.updateTypeDependants()
        self.pytype = data.get("pytype", self.pytype)
        self.solname = data.get("solname", self.solname)
        self.default = data.get("default", self.default)
        self.deserializeMethod = data.get("deserializeMethod", self.deserializeMethod)
        self.value = data.get("value", self.value)
        self.ignore = data.get("ignore", self.ignore)
        self.useenumval = data.get("use-enum-value", self.useenumval)
        self.serializeTo = ESerializeTo.parse(
            data.get("serialize-to", self.serializeTo.name)
        )
        self.enum = data.get("enum", self.enum)

    def getInitStr(self)->str:
        c = "#" if self.ignore else ""
        t = self.pytype
        rhs = f"{self.default!r}"
        if self.enum != "":
            t = self.enum
            rhs = f"{t}({rhs})"
            if self.enum.startswith("self."):
                t = self.enumType[1]
        return f"{c}self.{self.id}: {t} = {rhs}"

    def getStaticInitStr(self)->str:
        c = "#" if self.ignore else ""
        t = self.pytype
        rhs = f"{self.default!r}"
        if self.enum != "":
            t = self.enum
            rhs = f"{t}({rhs})"
            if self.enum.startswith("self."):
                t = self.enumType[1]
        return f"{c}{self.id}: {t} = {rhs}"

    def getSerialize(self)->str:
        c = "#" if self.ignore else ""
        rhs = f"self.{self.id}"
        if self.enum != "":
            if self.useenumval:
                rhs = f"{rhs}.value"
            else:
                rhs = f"{rhs}.name"
        return f'{c}data["{self.solname}"] = {rhs}'

    def getSerializeTo(self)->str:
        return self.getSerialize()

    def getDeserialize(self)->str:
        c = "#" if self.ignore else ""
        rhs = f'{self.deserializeMethod}("{self.solname}", {self.default!r})'
        if self.enum != "":
            if self.useenumval:
                rhs = f"{self.enum}({rhs})"
            else:
                rhs = f"{self.enum}[{rhs}]"
        return f"{c}self.{self.id} = {rhs}"

    def getDeserializeFrom(self)->str:
        c = "#" if self.ignore else ""
        rhs = f'{self.deserializeMethod}("{self.solname}", {self.default!r}, data)'
        if self.enum != "":
            if self.useenumval:
                rhs = f"{self.enum}({rhs})"
            else:
                rhs = f"{self.enum}[{rhs}]"
        return f"{c}self.{self.id} = {rhs}"

    def getValue(self) -> Any:
        if self.type == "Number":
            return float(self.value)
        if self.type == "int":
            return int(self.value)
        if self.type == "float":
            return float(self.value)
        if self.type == "String":
            return self.value.strip()[1:-1]
        return self.value


def fetchURL(url:str) -> requests.Response:
    log.info("Fetching %s...", url)
    res = requests.get(url)
    res.raise_for_status()
    return res


class ERawFlags(IntFlag):
    NONE = 0
    # ENDLESS_JOURNEY = auto()
    NO_SER_STAMP = auto()
    USES_SER_VER = auto()
    USES_SER_UUID = auto()
    SERIALIZE_AT_ROOT = auto()
    UNINHERIT_SERIALIZATION = auto()
    ONE_OF_MANY = auto()

    @staticmethod
    def getMaxLength()->int:
        return 6

    @staticmethod
    def parselist(l: List[str]) -> "ERawFlags":
        o = ERawFlags.NONE
        for flagname in l:
            o |= ERawFlags[flagname.replace("-", "_").upper()]
        return o

    def toList(self) -> List[str]:
        o = []
        for i in range(ERawFlags.getMaxLength()):
            v = ERawFlags(1 << i)
            if (self.value & v.value) != 0:
                o.append(v.name.lower())
        return o


class RawFile:
    def __init__(self):
        self.id: str = ""
        self.extends: str = ""
        self.inherits_from: Optional[str] = None
        self.url: str = ""
        self.dest: str = ""
        self.imports: List[Union[str, Dict[str, str]]] = []
        self.replacements: Dict[str, str] = {}
        self.flags: ERawFlags = ERawFlags.NONE
        self.rename_vars: Dict[str, str] = collections.OrderedDict()
        self.oldest_allowable_version: int = 0
        self.ctor_args: List[str] = []

        self.inherited: List[str] = []
        self.ignore_vars: List[str] = []
        self.vars: Dict[str, Variable] = collections.OrderedDict()
        self.overwrite_vars: Dict[str, dict] = {}
        self.statics: Dict[str, Variable] = collections.OrderedDict()

    def deserialize(self, k: str, data: dict) -> None:
        self.id = k
        self.extends = data.get("extends", self.extends)
        self.inherits_from = data.get("inherits-from", None)
        self.url = data["url"]
        self.imports = data.get("imports", self.imports)
        self.replacements = data.get("replacements", {})
        self.flags = ERawFlags.parselist(data.get("flags", []))
        self.inherited = data.get("inherited", [])
        self.dest = data["dest"]
        self.rename_vars = data.get("rename-vars", {})
        self.oldest_allowable_version = data.get("oldest-version", 0)
        self.ctor_args = data.get("constructor-args", [])

        self.vars = collections.OrderedDict()
        self.overwrite_vars = data.get("vars", {})

        self.oneOfMany = (self.flags & ERawFlags.ONE_OF_MANY) == ERawFlags.ONE_OF_MANY

    def parse(self) -> bool:
        url = self.url
        res = ""
        if url != "":
            if url.startswith("http://") or url.startswith("https://"):
                res = downloadIfChanged(self.url, cache_content=True)
                with log.info("Parsing code..."):
                    self.searchForVars(res)
            else:
                with log.info("Parsing code..."):
                    with open(url, "r") as f:
                        self.searchForVars(f)
        else:
            log.info("Generating...")
        success = True
        for k, v in self.overwrite_vars.items():
            if k in self.vars or v.get("new", False):
                if k not in self.vars:
                    self.vars[k] = va = Variable()
                    va.id = va.attr = k
                self.vars[k].deserialize(v)
            else:
                log.error(f"{self.id}.vars: {k} not in vars. {self.vars.keys()!r}")
                success = False
                sys.exit()
        if success:
            for var in self.vars.values():
                var.postProcess(self)
        return success

    def searchForVars(self, res:Union[str, requests.Response]) -> None:
        self.vars = collections.OrderedDict()
        inSerializer = False
        inSerVersion = False
        inSerUUID = False
        REG_SER_ASSIGN = None

        def inner(line: str):
            nonlocal inSerializer, inSerUUID, inSerVersion
            nonlocal REG_SER_ASSIGN
            for needle, repl in self.replacements:
                line = line.replace(needle, repl)
            if inSerVersion:
                if "public function" in line:
                    inSerVersion = False
                    inner(line)
                    return
                if "return SERIALIZATION_VERSION;" in line:
                    self.flags |= ERawFlags.USES_SER_VER
                return
            elif inSerUUID:
                # print(line)
                if "public function" in line:
                    inSerUUID = False
                    inner(line)
                    return
                if "return SERIALIZATION_UUID;" in line:
                    self.flags |= ERawFlags.USES_SER_UUID
                return
            elif inSerializer:
                if "public function" in line:
                    inSerializer = False
                    inner(line)
                    return
                if REG_SER_ASSIGN is None:
                    return
                m = REG_SER_ASSIGN.search(line)
                if m is not None:
                    # print(m.re.pattern)
                    # print(repr(m.groups()))
                    # print(repr(self.vars.keys()))
                    for varid in (m[2].rstrip("_"), "_" + m[2].rstrip("_")):
                        if varid in self.vars:
                            self.vars[varid].solname = m[1]
                            self.vars[varid].ignore = False
                            return
                    return
            else:
                # public function serialize(relativeRootObject:*):void {
                m = PREG_SER_FUNC.search(line)
                if m is not None:
                    inSerializer = True
                    # We have a serializer, make sure all the vars we output are present in it.
                    for var in self.vars.values():
                        var.ignore = True
                    REG_SER_ASSIGN = re.compile(
                        re.escape(m[2])
                        + r"\.([a-zA-Z0-9_]+) *= *this\.([a-zA-Z0-9_]+);"
                    )
                    return

                m = PREG_SERVER_FUNC.search(line)
                if m is not None:
                    # log.info('FOUND currentSerializationVerison()!')
                    inSerVersion = True
                    return

                # print(line)
                m = PREG_SERUUID_FUNC.search(line)
                if m is not None:
                    # log.info('FOUND serializationUUID()!')
                    # log.info(line)
                    inSerUUID = True
                    return

                m = PREG_VAR.search(line)
                if m is not None:
                    with log.info(f"Found {m.group(2)}"):
                        _id = origid = m.group(2)
                        if _id.startswith("_"):
                            # Get rid of leading _, since it actually means something in Python.
                            _id = _id.lstrip("_")
                        if _id in self.rename_vars.keys():
                            _id = self.rename_vars[_id]
                        if _id != origid:
                            log.info("Python ID changed to %s", _id)
                        if _id not in self.vars:
                            self.vars[_id] = Variable(m)
                            self.vars[_id].id = _id
                    return

                m = PREG_STATIC_CONST.search(line)
                if m is not None:
                    with log.info(f"Found {m.group(2)} (static)"):
                        _id = m.group(2)
                        self.statics[_id] = Variable(m)
                    return

        if isinstance(res, str):
            for bline in res.splitlines():
                inner(bline)
        else:
            for line in res:
                inner(line)

    def write(self) -> None:
        global ALL_CLASSES
        if self.inherits_from is not None:
            inheritee = ALL_CLASSES[self.inherits_from]
            self.inherited += list(inheritee.vars.keys())
            self.rename_vars.update(inheritee.rename_vars)
            for old, new in self.rename_vars.items():
                if old in self.vars:
                    self.vars[new] = self.vars[old]
                    del self.vars[old]
                    self.vars[new].id = new

        with log.info("To %s...", self.dest):
            with open(self.dest, "w") as f:
                self.toFile(f)

    def toFile(self, f) -> None:
        replacements = []
        for k, v in self.replacements.items():
            replacements += [(k, v)]

        imports = []
        if self.imports is not None:
            for import_spec in self.imports:
                if isinstance(import_spec, dict):
                    assert len(import_spec) > 0, repr(self.imports)
                    # package: target
                    package, imp = next(iter(import_spec.items()))
                    if isinstance(imp, str):
                        imp = [imp]
                    imp = ", ".join(imp)
                    imports += [f"from {package} import {imp}"]
                else:
                    imports += [f"import {import_spec}"]

        serialization_stamp: Optional[str] = None
        if (self.flags & ERawFlags.NO_SER_STAMP) != ERawFlags.NO_SER_STAMP:
            if self.id.startswith("UEE"):
                if (
                    "SERIALIZATION_UUID" in self.statics
                    and (self.flags & ERawFlags.USES_SER_UUID)
                    == ERawFlags.USES_SER_UUID
                ):
                    imports += [
                        f"from coctweak.saves.uee.serialization import UEESerializationVersion"
                    ]
                    uuid: str = self.statics["SERIALIZATION_UUID"].getValue()
                    version: str = self.statics["SERIALIZATION_VERSION"].getValue()
                    serialization_stamp = f"SERIALIZATION_STAMP = UEESerializationVersion('{uuid}', {self.oldest_allowable_version}, {version})"
                    del self.statics["SERIALIZATION_UUID"]
            elif self.id.startswith("HGG"):
                if (
                    "SERIALIZATION_VERSION" in self.statics
                    and (self.flags & ERawFlags.USES_SER_VER) == ERawFlags.USES_SER_VER
                ):
                    imports += [
                        f"from coctweak.saves.hgg.serialization import HGGSerializationVersion"
                    ]
                    version: int = self.statics["SERIALIZATION_VERSION"].getValue()
                    serialization_stamp = f"SERIALIZATION_STAMP = HGGSerializationVersion(({self.oldest_allowable_version}, {version}))"
                    del self.statics["SERIALIZATION_VERSION"]

        w = IndentWriter(f, indent_chars="    ")
        # w.writeline('# @GENERATED '+datetime.datetime.now().isoformat())
        fromline = ""
        if self.url != "":
            fromline = f"from {self.url}"
        w.writeline(f"# @GENERATED {fromline}")
        # w.writeline('from enum import IntEnum')
        for import_spec in imports:
            w.writeline(import_spec)
        w.writeline("")
        w.writeline(f"__ALL__=['{self.id}']")
        w.writeline("")
        extends = ", ".join(self.extends)
        with w.writeline(f"class {self.id}({extends}):"):
            if serialization_stamp is not None:
                w.writeline(serialization_stamp)
            if len(self.statics) > 0:
                # w.writeline("'''")
                for varID, var in self.statics.items():
                    if varID in ("LOGGER",):
                        continue
                    w.writeline(var.getStaticInitStr())
                # w.writeline("'''")
            cargs = ""
            ccargs = ""
            if len(self.ctor_args) > 0:
                cargs = ", ".join(self.ctor_args)
                ccargs = ", " + cargs
            with w.writeline(f"def __init__(self{ccargs}) -> None:"):
                w.writeline(f"super().__init__({cargs})")
                for var in self.vars.values():
                    if var.id not in self.inherited:
                        w.writeline(var.getInitStr())
            with w.writeline("def serialize(self) -> dict:"):
                if (
                    self.flags & ERawFlags.UNINHERIT_SERIALIZATION
                ) == ERawFlags.UNINHERIT_SERIALIZATION:
                    w.writeline("data = self.raw")
                    with w.writeline("if self.SERIALIZATION_STAMP is not None:"):
                        w.writeline(
                            "data = self.SERIALIZATION_STAMP.serialize(self, data)"
                        )
                else:
                    w.writeline("data = super().serialize()")
                for var in self.vars.values():
                    if var.id not in self.inherited and var.canSerializeTo(
                        ESerializeTo.OBJECT
                    ):
                        w.writeline(var.getSerialize())
                w.writeline("return data")
            if (
                self.flags & ERawFlags.SERIALIZE_AT_ROOT
            ) == ERawFlags.SERIALIZE_AT_ROOT:
                with w.writeline("def serializeTo(self, data: dict) -> None:"):
                    if (
                        self.flags & ERawFlags.UNINHERIT_SERIALIZATION
                    ) != ERawFlags.UNINHERIT_SERIALIZATION:
                        w.writeline("super().serializeTo(data)")
                    else:
                        w.writeline("self._addSelfToRoot()")
                    for var in self.vars.values():
                        if var.id not in self.inherited and var.canSerializeTo(
                            ESerializeTo.ROOT
                        ):
                            w.writeline(var.getSerializeTo())
            with w.writeline("def deserialize(self, data: dict) -> None:"):
                if (
                    self.flags & ERawFlags.UNINHERIT_SERIALIZATION
                ) == ERawFlags.UNINHERIT_SERIALIZATION:
                    w.writeline("self.raw = data")
                    with w.writeline("if self.SERIALIZATION_STAMP is not None:"):
                        w.writeline("self.SERIALIZATION_STAMP.deserialize(self, data)")
                else:
                    w.writeline("super().deserialize(data)")
                for var in self.vars.values():
                    if var.id not in self.inherited and var.canSerializeTo(
                        ESerializeTo.OBJECT
                    ):
                        w.writeline(var.getDeserialize())
            if (
                self.flags & ERawFlags.SERIALIZE_AT_ROOT
            ) == ERawFlags.SERIALIZE_AT_ROOT:
                with w.writeline("def deserializeFrom(self, data: dict) -> None:"):
                    if (
                        self.flags & ERawFlags.UNINHERIT_SERIALIZATION
                    ) != ERawFlags.UNINHERIT_SERIALIZATION:
                        w.writeline("super().deserializeFrom(data)")
                    for var in self.vars.values():
                        if var.id not in self.inherited and var.canSerializeTo(
                            ESerializeTo.ROOT
                        ):
                            w.writeline(var.getDeserializeFrom())


class GenerateClassesTarget(SingleBuildTarget):
    BT_LABEL = "GEN CLASSES"

    def __init__(self, target, dependencies=[]):
        global ALL_CLASSES
        globalVars = dict(VARIABLES)
        globalVars["pathjoin"] = (_join,)
        ALL_CLASSES = {}
        all_files = set()
        for root, _, filenames in os.walk("data/classes"):
            for filename in filenames:
                absfilename = os.path.join(root, filename)
                all_files.add(absfilename)
                cfg = YAMLConfig(absfilename, variables=globalVars)
                for k, v in cfg.cfg.items():
                    raw = RawFile()
                    raw.deserialize(k, v)
                    if raw.url != "" and os.path.isfile(raw.url):
                        all_files.add(raw.url)
                    ALL_CLASSES[k] = raw

        super().__init__(
            target=target, files=list(all_files), dependencies=dependencies
        )

    def build(self):
        global ALL_CLASSES
        failed = False
        with log.info("Parsing..."):
            for raw in ALL_CLASSES.values():
                if not raw.parse():
                    failed = True
        if failed:
            self.failed = True

        with log.info("Writing..."):
            for raw in ALL_CLASSES.values():
                raw.write()


def add_gen_classes_targets(bm: BuildMaestro, env, deps=[]):
    return [
        bm.add(
            GenerateClassesTarget(
                os.path.join(bm.builddir, "GEN_CLASSES.tgt"), dependencies=deps
            )
        ).target
    ]


def main():
    env = os_utils.ENV.clone()
    bm = BuildMaestro(hidden_build_dir=".build/genClasses_py/")
    add_gen_classes_targets(bm, env, [])
    bm.as_app()


if __name__ == "__main__":
    main()
