import collections
import os
import re
import sys

assert len(sys.argv) == 3
assert os.path.isfile(sys.argv[1])
assert sys.argv[2] in ('UEE', 'Vanilla')
REPL = collections.OrderedDict({
    'HGG': sys.argv[2],
    'hgg': sys.argv[2].lower(),
})

content = ''
with open(sys.argv[1], 'r') as f:
    content = f.read()
for a,b in REPL.items():
    content = content.replace(a, b)
with open(sys.argv[1], 'w') as f:
    f.write(content)
