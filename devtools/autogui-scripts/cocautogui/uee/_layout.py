class Layout:
    def __init__(self, innerh: int, innerw: int) -> None:
        self.innerHeight = innerh
        self.innerWidth = innerw

        self.paddingLeft: int = 0
        self.paddingTop: int = 0
        self.paddingCenter: int = 0

        self.elements = []

    def add(self, *elements) -> None:
        self.elements += elements

    def apply(self, **kwargs) -> None:
        return

class GridLayout(Layout):
    def apply(self, **kwargs) -> None:
        #private function applyGridLayout():void {
        #var config:Object = _layoutConfig;
        #var ignoreHidden:Boolean = 'ignoreHidden' in config ? config['ignoreHidden'] : false;
        ignoreHidden: bool = kwargs.get('ignoreHidden', False)
        #var rows:Number = config["rows"] || 1;
        rows: int = kwargs.get('rows', 1)
        #var cols:Number = config["cols"] || 1;
        cols: int = kwargs.get('cols', 1)
        #var innerw:Number = innerWidth;
        innerw = self.innerWidth
        #var innerh:Number = innerHeight;
        innerh = self.innerHeight
        #var cellw:Number = innerw / cols;
        cellw = innerw / cols
        #var cellh:Number = innerh / rows;
        cellh = innerh / rows
        #var setcw:Boolean = config['setWidth'];
        #setcw = kwargs['setWidth']
        #var setch:Boolean = config['setHeight'];
        #setch = kwargs['setHeight']

        #var row:int = 0;
        #var col:int = 0;
        row = 0
        col = 0
        #for (var ci:int = 0, cn:int = _container.numChildren; ci < cn; ci++) {
            #var child:DisplayObject = _container.getChildAt(ci);
        for child in self.elements:
            #var hint:Object = _layoutHints[child] || {};
            #if (hint['ignore'] || !child.visible && ignoreHidden) continue;
            #var setw:Boolean = 'setWidth' in hint ? hint['setWidth'] : setcw;
            #var seth:Boolean = 'setHeight' in hint ? hint['setHeight'] : setch;
            #if ('row' in hint && 'col' in hint) {
            #    row = hint['row'];
            #    col = hint['col'];
            #}
            #child.x = paddingLeft + col * (paddingCenter + cellw);
            child.pos.x = self.paddingLeft + col * (self.paddingCenter + cellw)
            #child.y = paddingTop + row * (paddingCenter + cellh);
            child.pos.y = self.paddingTop + row * (self.paddingCenter + cellh)
            #if (setw) child.width = cellw;
            #if (seth) child.height = cellh;
            #col = col + 1;
            col += 1
            #if (col >= cols) {
            #    col = 0;
            #    row++;
            #}
            if col >= cols:
                col = 0
                row += 1
