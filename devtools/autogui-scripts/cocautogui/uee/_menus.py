import math
from swfautogui.controls.menu import BaseMenu, Button, Point
from cocautogui.uee._consts import *
from cocautogui.uee._layout import GridLayout

class UEEMainView(BaseMenu):
    def setupElement(self, button: Button) -> None:
        if button.idx < BOTTOM_BUTTON_COUNT:
            button.size = STANDARD_BUTTON_SIZE
            bi = button.idx
            r = (bi // BOTTOM_COLS) << 0;
            c = bi % BOTTOM_COLS;
            #button = new CoCButton({
            #    bitmapClass: ButtonBackgrounds[bi % 10],
            #    visible    : false,
            #    x          : BOTTOM_X + BOTTOM_HGAP + c * (BOTTOM_HGAP * 2 + BTN_W),
            #    y          : BOTTOM_Y + r * (GAP + BTN_H)
            #});
            button.pos.x = BOTTOM_X + BOTTOM_HGAP + c * (BOTTOM_HGAP * 2 + BTN_W)
            button.pos.y = BOTTOM_Y + r * (GAP + BTN_H)
            button.pos.y += 80
# Bottom buttons
UEE_MAIN_VIEW = UEEMainView()
for i in range(BOTTOM_BUTTON_COUNT):
    UEE_MAIN_VIEW.add(f'Button {i+1}')
# Top buttons
mvTopButtons = GridLayout(TOPROW_W, TOPROW_H)
mvTopButtons.paddingLeft = GAP
mvTopButtons.paddingTop = GAP+80
for lbl in ['Main Menu', 'Data', 'Stats', 'Level Up', 'Perks', 'Appearance']:
    btn = Button(-1, lbl, Point(0, 0), STANDARD_BUTTON_SIZE)
    UEE_MAIN_VIEW.addButton(btn, quiet=True)
    mvTopButtons.add(btn)
mvTopButtons.apply(cols=6)

class UEEMainMenu(BaseMenu):
    def setupElement(self, button: Button) -> None:
        button.size = STANDARD_BUTTON_SIZE
        button.pos.x = math.floor(SCREEN_W / 2) - 310 + ((button.idx % 4) * 155)
        button.pos.y = math.floor(SCREEN_H / 2) + 175 + (math.floor(button.idx / 4) * 45)
        button.pos.y += 80 # Offset from Flash Player stuff.
UEE_MAIN_MENU = UEEMainMenu()
UEE_MAIN_MENU.add('Continue')
UEE_MAIN_MENU.add('New Game')
UEE_MAIN_MENU.add('Data')
UEE_MAIN_MENU.add('Options')
UEE_MAIN_MENU.add('Achievements')
UEE_MAIN_MENU.add('Instructions')
UEE_MAIN_MENU.add('Credits')
UEE_MAIN_MENU.add('Mod Thread')

UEE_GAMEPLAY_SETTINGS = UEEMainView()
for i in range(BOTTOM_BUTTON_COUNT):
    UEE_GAMEPLAY_SETTINGS.add(f'Button {i+1}')
