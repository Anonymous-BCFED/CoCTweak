import pyautogui
from buildtools import os_utils, log
from ._consts import (UEE_NEW_GAME_POS,
                      UEE_TEXT_ENTRY_POS,
                      UEE_DATA_BUTTON_POS,
                      UEE_SAVE_NOTES_POS)

from swfautogui.swfautogui import SWFAutoGUI
def ueeFelicia(auto: SWFAutoGUI) -> None:
    auto.deleteSlotIfExists(14)

    # New Game
    auto.clickButtonAt('New Game', *UEE_NEW_GAME_POS)

    # Give the game a second or two to shit itself.
    auto.sleep(5)

    # Choose the name Felicia
    auto.useTextField(UEE_TEXT_ENTRY_POS, 'Felicia', 'name field')

    pyautogui.PAUSE=1 #1s between calls from here on

    # Confirm name
    auto.useButtonByHotkey('OK', '1')

    # The character is female
    auto.useButtonByHotkey('Woman', '2')

    # Slender body plan
    auto.useButtonByHotkey('Slender', '1')

    # Light skin
    auto.useButtonByHotkey('Light', '1')

    # Blonde hair
    auto.useButtonByHotkey('Blonde', '1')

    # Confirm
    auto.useButtonByHotkey('Done', '0')

    # Smarts perk
    auto.useButtonByHotkey('Smarts', '4')

    # Confirm
    auto.useButtonByHotkey('Yes', '1')

    # Schooling perk
    auto.useButtonByHotkey('Schooling', '6')

    # Confirm
    auto.useButtonByHotkey('Yes', '1')

    # Game Mode screen
    auto.useButtonByHotkey('Start!', '5')

    # Fuck Ignam
    auto.useButtonByHotkey('No', 'n')

    # blah blah blah blah blah fuck you
    for _ in range(5):
        auto.useButtonByHotkey('Next', '1')

    # Click Data button.
    auto.clickButtonAt('Data', *UEE_DATA_BUTTON_POS)

    # Open the Save menu
    auto.useButtonByHotkey('Save', '1')

    # Use the broke-ass save notes
    auto.useTextField(UEE_SAVE_NOTES_POS, 'UEE - Felicia [PRISTINE]', 'save notes')

    # Use slot 14
    auto.useButtonByHotkey('Slot 14', 'f')

    # ./coctweak.sh export --raw @lwn 13 samples/hgg/newfemale.raw.yml
    os_utils.cmd(['python', '-m', 'coctweak', 'export', '--raw', '@lwn', '14', 'samples/uee/newfemale.raw.yml'], echo=True, show_output=True, critical=True)
    os_utils.cmd(['python', '-m', 'coctweak', 'export', '@lwn', '14', 'samples/uee/newfemale.yml'], echo=True, show_output=True, critical=True)

    auto.done()
