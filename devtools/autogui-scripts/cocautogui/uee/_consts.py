from swfautogui.point import Point

UEE_NEW_GAME_POS    = (420, 675) #(345, 655) + (150, 40) / 2
UEE_DATA_BUTTON_POS = (241, 105) #(166, 85) + (150, 40) / 2
UEE_TEXT_ENTRY_POS  = (296, 332) #(213, 319) + (166, 26) / 2
UEE_SAVE_NOTES_POS  = (485, 713) #(210, 700) + (551, 26) / 2

UEE_BUTTON_KEYS = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '0',
    'A',
    'S',
    'D',
    'F',
    'G',
]

# (public|internal) static const ([A-Z0-9_]+):Number *= ([^;]+);(.*)$
# #$1 static const $2:Number = $3;$4\n$2: int = $3
#public static const GAP:Number = 4;
GAP: int = 4
#public static const BTN_W:Number = 150;
BTN_W: int = 150
#public static const BTN_H:Number = 40;
BTN_H: int = 40

STANDARD_BUTTON_SIZE = Point(BTN_W, BTN_H)

#public static const SCREEN_W:Number = 1000;
SCREEN_W: int = 1000
#public static const SCREEN_H:Number = 800;
SCREEN_H: int = 800

SCREEN_SIZE = Point(SCREEN_W, SCREEN_H)

#internal static const TOPROW_Y:Number = 0;
TOPROW_Y: int = 0
#internal static const TOPROW_H:Number = 50;
TOPROW_H: int = 50
#internal static const TOPROW_NUMBTNS:Number = 6;
TOPROW_NUMBTNS: int = 6

#internal static const STATBAR_W:Number = 205;
STATBAR_W: int = 205
#internal static const STATBAR_Y:Number = TOPROW_Y + TOPROW_H;
STATBAR_Y: int = TOPROW_Y + TOPROW_H
#internal static const STATBAR_H:Number = 602;
STATBAR_H: int = 602

#public static const TEXTZONE_X:Number = 208; // left = const
TEXTZONE_X: int = 208
#public static const TEXTZONE_Y:Number = 52; // top = const
TEXTZONE_Y: int = 52
#public static const TEXTZONE_W:Number = 769; // width = const
TEXTZONE_W: int = 769
#public static const TEXTZONE_H:Number = 602; // height = const
TEXTZONE_H: int = 602
#internal static const VSCROLLBAR_W:Number = 15;
VSCROLLBAR_W: int = 15

#internal static const SPRITE_W:Number = 80;
SPRITE_W: int = 80
#internal static const SPRITE_H:Number = 80;
SPRITE_H: int = 80
#internal static const SPRITE_X:Number = GAP;
SPRITE_X: int = GAP
#internal static const SPRITE_Y:Number = SCREEN_H - SPRITE_H - GAP;
SPRITE_Y: int = SCREEN_H - SPRITE_H - GAP

#internal static const CREDITS_X:Number = GAP;
CREDITS_X: int = GAP
#internal static const CREDITS_Y:Number = STATBAR_Y + STATBAR_H + GAP;
CREDITS_Y: int = STATBAR_Y + STATBAR_H + GAP
#internal static const CREDITS_W:Number = STATBAR_W - GAP;
CREDITS_W: int = STATBAR_W - GAP
#internal static const CREDITS_H:Number = SPRITE_Y - CREDITS_Y;
CREDITS_H: int = SPRITE_Y - CREDITS_Y

#internal static const TOPROW_W:Number = STATBAR_W + 2 * GAP + TEXTZONE_W;
TOPROW_W: int = STATBAR_W + 2 * GAP + TEXTZONE_W

#internal static const BOTTOM_X:Number = STATBAR_W + GAP;
BOTTOM_X: int = STATBAR_W + GAP
#internal static const BOTTOM_COLS:Number = 5;
BOTTOM_COLS: int = 5
#internal static const BOTTOM_ROWS:Number = 3;
BOTTOM_ROWS: int = 3
#internal static const BOTTOM_BUTTON_COUNT:int = BOTTOM_COLS * BOTTOM_ROWS;
BOTTOM_BUTTON_COUNT:int = BOTTOM_COLS * BOTTOM_ROWS
#internal static const BOTTOM_H:Number = (GAP + BTN_H) * BOTTOM_ROWS;
BOTTOM_H: int = (GAP + BTN_H) * BOTTOM_ROWS
#internal static const BOTTOM_W:Number = TEXTZONE_W;
BOTTOM_W: int = TEXTZONE_W
#internal static const BOTTOM_HGAP:Number = (BOTTOM_W - BTN_W * BOTTOM_COLS) / (2 * BOTTOM_COLS);
BOTTOM_HGAP: int = (BOTTOM_W - BTN_W * BOTTOM_COLS) / (2 * BOTTOM_COLS)
#internal static const BOTTOM_Y:Number = SCREEN_H - BOTTOM_H;
BOTTOM_Y: int = SCREEN_H - BOTTOM_H
