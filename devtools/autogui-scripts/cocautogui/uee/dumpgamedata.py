import pyautogui, pyperclip
from pathlib import Path
from buildtools import os_utils, log
from ._menus import (UEE_MAIN_MENU,
                     UEE_MAIN_VIEW)

from swfautogui.swfautogui import SWFAutoGUI
def ueeDumpGameData(auto: SWFAutoGUI, basedir: Path) -> None:

    pyautogui.PAUSE=1 #1s between calls from here on

    # Options
    UEE_MAIN_MENU.get('Options').click()
    # Engage Debug Mode
    auto.clickButtonAt('Debug Mode: On', 724, 327)
    # Back
    UEE_MAIN_VIEW.get('Button 15').click(name='Back')
    UEE_MAIN_MENU.get('New Game').click()
    # Debug menu
    with log.info('Entering DEBUG cheat code (ignore graphical errors)...'):
        text = 'debug'
        log.info('Typing %r', list(text))
        pyautogui.typewrite(text)
    # Lag
    auto.sleep(3)
    # DUMP
    UEE_MAIN_VIEW.get('Button 11').click(name='DUMP')

    # Get items json
    UEE_MAIN_VIEW.get('Button 1').click(name='Items')
    saveFileTo(auto, basedir / 'data' / 'uee' / 'items.gamedata.json')
    UEE_MAIN_VIEW.get('Button 15').click(name='Back')

    UEE_MAIN_VIEW.get('Button 2').click(name='Perks')
    saveFileTo(auto, basedir / 'data' / 'uee' / 'perks.gamedata.json')
    UEE_MAIN_VIEW.get('Button 15').click(name='Back')

    UEE_MAIN_VIEW.get('Button 3').click(name='StatusFX')
    saveFileTo(auto, basedir / 'data' / 'uee' / 'statuseffects.gamedata.json')
    UEE_MAIN_VIEW.get('Button 15').click(name='Back')

    auto.done()

def saveFileTo(auto: SWFAutoGUI, path: Path) -> None:
    # Serialization is hard mmkay
    auto.sleep(3)
    '''
    # Here's where shit gets tricky.
    # We need to find an X window with name=Select location for download by localhost
    xwid = auto.getWindowID('Select location for download')
    with log.info('Focusing window...'):
        os_utils.cmd([self.xdotool, 'windowfocus', '--sync', str(xwid)], echo=True, critical=True)
    # Then we need to move it to 0, 0
    with log.info('Moving window to <0,0>...'):
        os_utils.cmd([self.xdotool, 'windowmove', '--sync', str(xwid), str(x), str(y)], echo=True, critical=True)
    '''
    #OOOOR I could just type in the name of the file and mash enter.
    filename: str = str(path)
    with log.info('Saving file as %r...'):
        log.info('Selecting all by typing ^A...')
        pyautogui.hotkey('ctrl', 'a')
        log.info('Setting clipboard text contents to %r...', filename)
        pyperclip.copy(filename)
        log.info('Pasting with ^V...')
        pyautogui.hotkey('ctrl', 'v')
        log.info('Pressing enter to confirm.')
        pyautogui.typewrite(['enter'])
        log.info('Pressing enter to confirm overwrite.')
        pyautogui.typewrite(['enter'])
    auto.sleep(3)
