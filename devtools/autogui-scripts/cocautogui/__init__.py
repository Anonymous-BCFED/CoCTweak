from swfautogui import SWFAutoGUI
import os, platform
from buildtools import log
from pathlib import Path
__SHARED_OBJECTS_DIR: str = None
def getFlashBaseDir():
    global __SHARED_OBJECTS_DIR
    if __SHARED_OBJECTS_DIR is None:
        flashDBDir = ''
        if 'FLASH_SHARED_OBJECTS_DIR' in os.environ and os.environ['FLASH_SHARED_OBJECTS_DIR'] != '':
            log.warning('FLASH_SHARED_OBJECTS_DIR is set! This should only be the case if you are testing something.')
            if os.name == 'nt':
                cmd = 'set FLASH_SHARED_OBJECTS_DIR='
            else:
                cmd = 'export -n FLASH_SHARED_OBJECTS_DIR'
            log.warning('To clear this warning, enter `%s` into your current shell.', cmd)
            __SHARED_OBJECTS_DIR = os.environ['FLASH_SHARED_OBJECTS_DIR']
            return __SHARED_OBJECTS_DIR
        if platform.system() == 'Windows':
            flashDBDir = os.path.join(os.environ['APPDATA'], 'Macromedia', 'Flash Player', '#SharedObjects')
        elif platform.system() == 'Linux':
            flashDBDir = os.path.expanduser('~/.macromedia/Flash_Player/#SharedObjects/')
        if os.path.isdir(flashDBDir):
            for subdir in os.listdir(flashDBDir):
                if subdir not in ('.','..'):
                    __SHARED_OBJECTS_DIR = os.path.join(flashDBDir, subdir)
                    break
    return __SHARED_OBJECTS_DIR

NSLOOKUP = {
    # Dropped #'ej': 'EndlessJourney'
}

def getSaveFilename(host: str, svID: str, ext: str='.sol') -> str:
    if host in ('@file', 'file'):
        return svID
    svID = int(svID)
    basedir = os.path.join(getFlashBaseDir(), host)
    return os.path.join(basedir, f'CoC_{svID}{ext}')

class CoCAutoGUI(SWFAutoGUI):
    def deleteSlotIfExists(self, slotid: int) -> None:
        savepath = Path(getSaveFilename('#localWithNet', str(slotid)))
        if savepath.is_file():
            with log.info('Deleting %s...', str(savepath)):
                savepath.unlink()
