import pyautogui
from buildtools import os_utils, log
from ._consts import (HGG_NEW_GAME_POS,
                      HGG_TEXT_ENTRY_POS,
                      HGG_DATA_BUTTON_POS,
                      HGG_SAVE_NOTES_POS)

from swfautogui.swfautogui import SWFAutoGUI
def hggHarold(auto: SWFAutoGUI) -> None:
    auto.deleteSlotIfExists(13)
    auto.clickButtonAt('New Game', *HGG_NEW_GAME_POS)

    # Give the game a second or two to shit itself.
    auto.sleep(5)

    # Choose the name Harold.
    auto.useTextField(HGG_TEXT_ENTRY_POS, 'Harold', 'name field')

    pyautogui.PAUSE=1 #1s between calls from here on

    # Confirm name
    auto.useButtonByHotkey('OK', '1')

    # We're an adult
    auto.useButtonByHotkey('Adult', '3')

    # The character is male
    auto.useButtonByHotkey('Man', '1')

    # Lean body plan
    auto.useButtonByHotkey('Lean', '1')

    # Likes the girls
    auto.useButtonByHotkey('Females', '2')

    # Light skin
    auto.useButtonByHotkey('Light', '1')

    # Blonde hair
    auto.useButtonByHotkey('Blonde', '1')

    # Confirm
    auto.useButtonByHotkey('Done', '0')

    # Smarts perk
    auto.useButtonByHotkey('Smarts', '4')

    # Confirm
    auto.useButtonByHotkey('Yes', '1')

    # Schooling perk
    auto.useButtonByHotkey('Schooling', '6')

    # Confirm
    auto.useButtonByHotkey('Yes', '1')

    # Virgin
    auto.useButtonByHotkey('Yes', '1')

    # Confirm everything
    auto.useButtonByHotkey('Continue', 'g')

    # blah blah blah blah fuck you fen
    for _ in range(4):
        auto.useButtonByHotkey('Next', '1')

    # Click Data button.
    x, y = HGG_DATA_BUTTON_POS
    log.info(f'Clicking "Data" @ <{x}, {y}>')
    pyautogui.click(x, y)

    # Open the Save menu
    auto.useButtonByHotkey('Save', '1')

    # Use the broke-ass save notes
    auto.useTextField(HGG_SAVE_NOTES_POS, 'HGG - Harold [PRISTINE]', 'save notes')

    # Use slot 13
    auto.useButtonByHotkey('Slot 13', 'd')

    # ./coctweak.sh export --raw @lwn 13 samples/hgg/newmale.raw.yml
    os_utils.cmd(['python', '-m', 'coctweak', 'export', '--raw', '@lwn', '13', 'samples/hgg/newmale.raw.yml'], echo=True, show_output=True, critical=True)
    os_utils.cmd(['python', '-m', 'coctweak', 'export', '@lwn', '13', 'samples/hgg/newmale.yml'], echo=True, show_output=True, critical=True)

    auto.done()
