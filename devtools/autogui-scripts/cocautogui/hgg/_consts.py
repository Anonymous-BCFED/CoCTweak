from swfautogui.point import Point

HGG_NEW_GAME_POS    = (421, 675) #(346, 655) + (150, 40) / 2
HGG_DATA_BUTTON_POS = (243, 104)  #(168, 84) + (150, 40) / 2
HGG_TEXT_ENTRY_POS  = (296, 332) #(213, 319) + (166, 26) / 2
HGG_SAVE_NOTES_POS  = (485, 713) #(210, 700) + (551, 26) / 2

# public static const ([A-Z0-9_]+):Number = ([^;]+);(.*)$
# #public static const $1:Number = $2;$3\n$1: int = $2

#public static const GAP:Number = 4;
GAP: int = 4
#public static const BTN_W:Number = 150;
BTN_W: int = 150
#public static const BTN_H:Number = 40;
BTN_H: int = 40
#public static const BTN_MW:Number = 98 + 2 / 3;
BTN_MW: int = 98 + 2 / 3

STANDARD_BUTTON_SIZE = Point(BTN_W, BTN_H)

#public static const SCREEN_W:Number = 1000;
SCREEN_W: int = 1000
#public static const SCREEN_H:Number = 800;
SCREEN_H: int = 800

SCREEN_SIZE = Point(SCREEN_W, SCREEN_H)

#public static const TEXTZONE_X:Number = 208; // left = const
TEXTZONE_X: int = 208
#public static const TEXTZONE_Y:Number = 52; // top = const
TEXTZONE_Y: int = 52
#internal static const TEXTZONE_W:Number = 770; // width = const
TEXTZONE_W: int = 770
#internal static const VSCROLLBAR_W:Number = 15;
VSCROLLBAR_W: int = 15
#internal static const TEXTZONE_H:Number = 602; // height = const
TEXTZONE_H: int = 602

#internal static const TOPROW_Y:Number = 0;
TOPROW_Y: int = 0
#internal static const TOPROW_H:Number = 50;
TOPROW_H: int = 50
#internal static const TOPROW_NUMBTNS:Number = 6;
TOPROW_NUMBTNS: int = 6

#internal static const STATBAR_W:Number = 205;
STATBAR_W: int = 205
#internal static const STATBAR_Y:Number = TOPROW_Y + TOPROW_H + 2;
STATBAR_Y: int = TOPROW_Y + TOPROW_H + 2
#internal static const STATBAR_H:Number = 602;
STATBAR_H: int = 602
#internal static const TOPROW_W:Number = STATBAR_W + 2 * GAP + TEXTZONE_W;
TOPROW_W: int = STATBAR_W + 2 * GAP + TEXTZONE_W

#internal static const BOTTOM_X:Number = STATBAR_W + GAP;
BOTTOM_X: int = STATBAR_W + GAP
#internal static const BOTTOM_COLS:Number = 5;
BOTTOM_COLS: int = 5
#internal static const BOTTOM_ROWS:Number = 3;
BOTTOM_ROWS: int = 3
#internal static const BOTTOM_BUTTON_COUNT:int = BOTTOM_COLS * BOTTOM_ROWS;
BOTTOM_BUTTON_COUNT: int = BOTTOM_COLS * BOTTOM_ROWS
#internal static const BOTTOM_H:Number = (GAP + BTN_H) * BOTTOM_ROWS;
BOTTOM_H: int = (GAP + BTN_H) * BOTTOM_ROWS
#internal static const BOTTOM_W:Number = TEXTZONE_W;
BOTTOM_W: int = TEXTZONE_W
#internal static const BOTTOM_HGAP:Number = (BOTTOM_W - BTN_W * BOTTOM_COLS) / (2 * BOTTOM_COLS);
BOTTOM_HGAP: float = (BOTTOM_W - BTN_W * BOTTOM_COLS) / (2 * BOTTOM_COLS)
#internal static const BOTTOM_Y:Number = SCREEN_H - BOTTOM_H;
BOTTOM_Y: int = SCREEN_H - BOTTOM_H
