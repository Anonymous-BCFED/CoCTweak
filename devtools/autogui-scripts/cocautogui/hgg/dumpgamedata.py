import pyautogui, pyperclip
from pathlib import Path
from buildtools import os_utils, log
from cocautogui.hgg._menus import (HGG_MAIN_MENU,
                                   HGG_MAIN_VIEW)

from swfautogui.swfautogui import SWFAutoGUI
def hggDumpGameData(auto: SWFAutoGUI, basedir: Path) -> None:

    pyautogui.PAUSE=1 #1s between calls from here on

    # Options
    HGG_MAIN_MENU.get('Options').click()
    # Engage Debug Mode
    auto.clickButtonAt('Debug Mode: On', 727, 383)
    # Back
    HGG_MAIN_VIEW.get('Button 15').click(name='Back')
    HGG_MAIN_MENU.get('Data').click()
    HGG_MAIN_VIEW.get('Button 2').click(name='Load')
    # Debug menu
    with log.info('Entering DEBUG cheat code (ignore graphical errors)...'):
        auto._stepCheck()
        text = 'debug'
        log.info('Typing %r', list(text))
        pyautogui.typewrite(text)

    # Lag
    auto.sleep(3)

    # Enter the DUMP menu we hacked together.
    HGG_MAIN_VIEW.get('Button 13').click(name='DUMP')

    # Get items json
    HGG_MAIN_VIEW.get('Button 1').click(name='Items')
    saveFileTo(auto, basedir / 'data' / 'hgg' / 'items.gamedata.json')
    HGG_MAIN_VIEW.get('Button 15').click(name='Back')

    HGG_MAIN_VIEW.get('Button 2').click(name='Perks')
    saveFileTo(auto, basedir / 'data' / 'hgg' / 'perks.gamedata.json')
    HGG_MAIN_VIEW.get('Button 15').click(name='Back')

    HGG_MAIN_VIEW.get('Button 3').click(name='StatusFX')
    saveFileTo(auto, basedir / 'data' / 'hgg' / 'statuseffects.gamedata.json')
    HGG_MAIN_VIEW.get('Button 15').click(name='Back')

    auto.done()

def saveFileTo(auto: SWFAutoGUI, path: Path) -> None:
    # Serialization is hard mmkay
    auto.sleep(3)
    '''
    # Here's where shit gets tricky.
    # We need to find an X window with name=Select location for download by localhost
    xwid = auto.getWindowID('Select location for download')
    with log.info('Focusing window...'):
        os_utils.cmd([self.xdotool, 'windowfocus', '--sync', str(xwid)], echo=True, critical=True)
    # Then we need to move it to 0, 0
    with log.info('Moving window to <0,0>...'):
        os_utils.cmd([self.xdotool, 'windowmove', '--sync', str(xwid), str(x), str(y)], echo=True, critical=True)
    '''
    #OOOOR I could just type in the name of the file and mash enter.
    filename: str = str(path)
    with log.info('Saving file as %r...', filename):
        log.info('Selecting all by typing ^A...')
        pyautogui.hotkey('ctrl', 'a')
        log.info('Setting clipboard text contents to %r...', filename)
        pyperclip.copy(filename)
        log.info('Pasting with ^V...')
        pyautogui.hotkey('ctrl', 'v')
        log.info('Pressing enter to confirm.')
        pyautogui.typewrite(['enter'])
        # TODO: This works on KDE, not sure about GNOME.
        log.info('Pressing alt+y to confirm overwrite.')
        pyautogui.hotkey('alt', 'y')
    auto.sleep(3)
