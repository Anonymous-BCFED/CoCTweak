import pyautogui
from buildtools import os_utils, log
from ._consts import (HGG_NEW_GAME_POS,
                      HGG_TEXT_ENTRY_POS,
                      HGG_DATA_BUTTON_POS,
                      HGG_SAVE_NOTES_POS)

from swfautogui.swfautogui import SWFAutoGUI
def hggFelicia(auto: SWFAutoGUI) -> None:
    auto.deleteSlotIfExists(12)

    # New Game
    auto.clickButtonAt('New Game', *HGG_NEW_GAME_POS)

    # Give the game a second or two to shit itself.
    auto.sleep(5)

    # Choose the name Felicia
    auto.useTextField(HGG_TEXT_ENTRY_POS, 'Felicia', 'name field')

    pyautogui.PAUSE=1 #1s between calls from here on

    # Confirm name
    auto.useButtonByHotkey('OK', '1')

    # We're an adult
    auto.useButtonByHotkey('Adult', '3')

    # The character is female
    auto.useButtonByHotkey('Woman', '2')

    # Slender body plan
    auto.useButtonByHotkey('Slender', '1')

    # Likes the boys
    auto.useButtonByHotkey('Males', '1')

    # Light skin
    auto.useButtonByHotkey('Light', '1')

    # Blonde hair
    auto.useButtonByHotkey('Blonde', '1')

    # Confirm
    auto.useButtonByHotkey('Done', '0')

    # Smarts perk
    auto.useButtonByHotkey('Smarts', '4')

    # Confirm
    auto.useButtonByHotkey('Yes', '1')

    # Schooling perk
    auto.useButtonByHotkey('Schooling', '6')

    # Confirm
    auto.useButtonByHotkey('Yes', '1')

    # Virgin
    auto.useButtonByHotkey('Yes', '1')

    # Confirm everything
    auto.useButtonByHotkey('Continue', 'g')

    # blah blah blah blah fuck you fen
    for _ in range(4):
        auto.useButtonByHotkey('Next', '1')

    # Click Data button.
    auto.clickButtonAt('Data', *HGG_DATA_BUTTON_POS)

    # Open the Save menu
    auto.useButtonByHotkey('Save', '1')

    # Use the broke-ass save notes
    auto.useTextField(HGG_SAVE_NOTES_POS, 'HGG - Felicia [PRISTINE]', 'save notes')

    # Use slot 12
    auto.useButtonByHotkey('Slot 12', 's')

    # ./coctweak.sh export --raw @lwn 13 samples/hgg/newmale.raw.yml
    os_utils.cmd(['python', '-m', 'coctweak', 'export', '--raw', '@lwn', '12', 'samples/hgg/newfemale.raw.yml'], echo=True, show_output=True, critical=True)
    os_utils.cmd(['python', '-m', 'coctweak', 'export', '@lwn', '12', 'samples/hgg/newfemale.yml'], echo=True, show_output=True, critical=True)

    auto.done()
