import math
from swfautogui.controls.menu import BaseMenu, Button, Point
from cocautogui.hgg._consts import *
from cocautogui.hgg._layout import GridLayout

class HGGMainView(BaseMenu):
    def setupElement(self, button: Button) -> None:
        if button.idx < BOTTOM_BUTTON_COUNT:
            button.size = STANDARD_BUTTON_SIZE
            bi = button.idx
            r = (bi // BOTTOM_COLS) << 0
            c = bi % BOTTOM_COLS

            #button = new CoCButton({
            #    visible: false,
            #    x: BOTTOM_X + BOTTOM_HGAP + c * (BOTTOM_HGAP * 2 + BTN_W),
            #    y: BOTTOM_Y + r * (GAP + BTN_H),
            #    position: bi
            #});
            button.pos.x = BOTTOM_X + BOTTOM_HGAP + c * (BOTTOM_HGAP * 2 + BTN_W)
            button.pos.y = BOTTOM_Y + r * (GAP + BTN_H)
            button.pos.y += 80 # Offset from Flash Player stuff.
# Bottom buttons
HGG_MAIN_VIEW = HGGMainView()
for i in range(BOTTOM_BUTTON_COUNT):
    HGG_MAIN_VIEW.add(f'Button {i+1}')
# Top buttons
mvTopButtons = GridLayout(TOPROW_W, TOPROW_H)
mvTopButtons.paddingLeft = GAP
mvTopButtons.paddingTop = GAP+80
for lbl in ['Main Menu', 'Data', 'Stats', 'Level Up', 'Perks', 'Appearance']:
    btn = Button(-1, lbl, Point(0, 0), STANDARD_BUTTON_SIZE)
    HGG_MAIN_VIEW.addButton(btn, quiet=True)
    mvTopButtons.add(btn)
mvTopButtons.apply(cols=6)

class HGGMainMenu(BaseMenu):
    def setupElement(self, button: Button) -> None:
        button.size = STANDARD_BUTTON_SIZE
        button.pos.x = math.floor(SCREEN_W / 2) - 310 + ((button.idx % 4) * 155)
        button.pos.y = math.floor(SCREEN_H / 2) + 175 + (math.floor(button.idx / 4) * 45)
        button.pos.y += 80 # Offset from Flash Player stuff.
HGG_MAIN_MENU = HGGMainMenu()
HGG_MAIN_MENU.add('Continue')
HGG_MAIN_MENU.add('New Game')
HGG_MAIN_MENU.add('Data')
HGG_MAIN_MENU.add('Options')
HGG_MAIN_MENU.add('Achievements')
HGG_MAIN_MENU.add('Instructions')
HGG_MAIN_MENU.add('Credits')
HGG_MAIN_MENU.add('Quit')

class HGGGameplaySettings(BaseMenu):
    def setupElement(self, button: Button) -> None:
        button.size = STANDARD_BUTTON_SIZE
        button.pos.x = math.floor(SCREEN_W / 2) - 310 + ((button.idx % 4) * 155)
        button.pos.y = math.floor(SCREEN_H / 2) + 175 + (math.floor(button.idx / 4) * 45)
        button.pos.y += 80 # Offset from Flash Player stuff.
HGG_GAMEPLAY_SETTINGS = HGGMainView()
for i in range(BOTTOM_BUTTON_COUNT):
    HGG_GAMEPLAY_SETTINGS.add(f'Button {i+1}')
