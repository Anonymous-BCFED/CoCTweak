These scripts automate the production of [samples](/samples/) using an automation toolkit called pyautogui.  

They are intended to replace the HTML instructions.

## Prerequisites
* pyautogui (installed with requirements-dev.txt)
* toml (installed with requirements-dev.txt)
* psutil (installed with requirements-dev.txt)
* xdotool
  * sudo apt install xdotool
* Patched and compiled source code for HGG, UEE, and Vanilla.

## Setup

1. Copy `autogui-config.toml.example` to `autogui-config.toml`
1. Edit `autogui-config.toml` appropriately.
1. Run `python devtools/autogui-scripts/mkStandardChars.py hgg harold` **AND DON'T TOUCH ANYTHING** until it exits
1. Run `python devtools/autogui-scripts/mkStandardChars.py hgg felicia` **AND DON'T TOUCH ANYTHING** until it exits
1. Run `python devtools/autogui-scripts/mkStandardChars.py uee harold` **AND DON'T TOUCH ANYTHING** until it exits
1. Run `python devtools/autogui-scripts/mkStandardChars.py uee felicia` **AND DON'T TOUCH ANYTHING** until it exits
