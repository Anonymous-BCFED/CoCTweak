import pyautogui, time
from pathlib import Path
from buildtools import os_utils, log
import tqdm, math, signal, os
import toml
from typing import Tuple

from cocautogui import CoCAutoGUI
from cocautogui.hgg.dumpgamedata import hggDumpGameData
from cocautogui.uee.dumpgamedata import ueeDumpGameData
XDOTOOL = ''
FLASHPLAYER = ''

#pyautogui.PAUSE=0.25
INNER_WINDOW_OFFSET = (0, 80)

def swfclick(x, y, *args, **kwargs):
    return pyautogui.click(x+INNER_WINDOW_OFFSET[0], y+INNER_WINDOW_OFFSET[1], *args, **kwargs)

def main():
    global XDOTOOL, FLASHPLAYER
    XDOTOOL = os_utils.assertWhich('xdotool')
    FLASHPLAYER = os_utils.which('flashplayer')

    import argparse

    argp = argparse.ArgumentParser()
    argp.add_argument('flavor', choices=['hgg', 'uee'], help='CoC Mod Flavor')
    args = argp.parse_args()

    with open('autogui-config.toml', 'r') as f:
        cfg = toml.load(f)

    FLASHPLAYER = Path(cfg['paths']['executables']['flashplayer'])
    #FLASHPLAYERDEBUG = Path(cfg['paths']['executables']['flashplayerdebug'])
    if not Path(FLASHPLAYER).is_file():
        log.critical('paths.executables.flashplayer not found at %r.', FLASHPLAYER)
        return

    auto = CoCAutoGUI(xdotool=XDOTOOL, flashplayer=FLASHPLAYER)
    swf: Path
    if args.flavor == 'hgg':
        swf = Path(cfg['paths']['swfs']['hgg-patched'])
        assert swf.is_file(), 'HGG SWF missing'
        with auto.startFlashPlayer(str(swf), (1000, 851)):
            hggDumpGameData(auto, Path.cwd())
    elif args.flavor == 'uee':
        swf = Path(cfg['paths']['swfs']['uee-patched'])
        assert swf.is_file(), 'UEE SWF missing'
        with auto.startFlashPlayer(str(swf), (1000, 851), startupWait=30):
            ueeDumpGameData(auto, Path.cwd())



if __name__ == '__main__':
    main()
