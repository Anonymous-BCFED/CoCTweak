from pathlib import Path

from buildtools import log, os_utils
from cocautogui import CoCAutoGUI
from cocautogui.hgg.felicia import hggFelicia
from cocautogui.hgg.harold import hggHarold
from cocautogui.uee.felicia import ueeFelicia
from cocautogui.uee.harold import ueeHarold
import pyautogui
import toml

XDOTOOL = ''
FLASHPLAYER = ''

#pyautogui.PAUSE=0.25
INNER_WINDOW_OFFSET = (0, 80)

def swfclick(x, y, *args, **kwargs):
    return pyautogui.click(x+INNER_WINDOW_OFFSET[0], y+INNER_WINDOW_OFFSET[1], *args, **kwargs)

def main():
    global XDOTOOL, FLASHPLAYER
    XDOTOOL = os_utils.assertWhich('xdotool')
    FLASHPLAYER = os_utils.which('flashplayer')

    import argparse

    argp = argparse.ArgumentParser()
    argp.add_argument('flavor', choices=['hgg', 'uee'], help='CoC Mod Flavor')
    argp.add_argument('character', choices=['harold', 'felicia'], help='Full path to CoC HGG .swf file')
    args = argp.parse_args()

    '''
    with open('autogui-config.toml.example', 'w') as f:
        d={
            'paths': {
                'executables': {
                    'flashplayer': '/path/to/flashplayer',
                    'flashplayerdebug': '/path/to/flashplayerdebug',
                },
                'swfs': {
                    'hgg': '/path/to/hgg.swf',
                    'uee': '/path/to/uee.swf',
                    'vanilla': '/path/to/vanilla.swf',
                }
            }
        }
        toml.dump(d,f)
    '''

    with open('autogui-config.toml', 'r') as f:
        cfg = toml.load(f)

    FLASHPLAYER = Path(cfg['paths']['executables']['flashplayer'])
    #FLASHPLAYERDEBUG = Path(cfg['paths']['executables']['flashplayerdebug'])
    if not Path(FLASHPLAYER).is_file():
        log.critical('paths.executables.flashplayer not found at %r.', FLASHPLAYER)
        return

    auto = CoCAutoGUI(xdotool=XDOTOOL, flashplayer=FLASHPLAYER)
    swf: Path
    if args.flavor == 'hgg':
        swf = Path(cfg['paths']['swfs']['hgg'])
        assert swf.is_file(), 'HGG SWF missing'
        with auto.startFlashPlayer(str(swf), (1000, 851)):
            if args.character == 'harold':
                hggHarold(auto)
            elif args.character == 'felicia':
                hggFelicia(auto)
    elif args.flavor == 'uee':
        swf = Path(cfg['paths']['swfs']['uee'])
        assert swf.is_file(), 'UEE SWF missing'
        with auto.startFlashPlayer(str(swf), (1000, 851), startupWait=30):
            if args.character == 'harold':
                ueeHarold(auto)
            elif args.character == 'felicia':
                ueeFelicia(auto)



if __name__ == '__main__':
    main()
