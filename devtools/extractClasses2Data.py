from ruamel.yaml import YAML
from buildtools.config import YAMLConfig
from pathlib import Path
import os, sys

sys.path.append('./devtools')
from coctweak_build.classes_stuff import VARIABLES

FLAVORS = ['Common', 'Vanilla', 'HGG', 'UEE']

DATA_DIR: Path = Path.cwd() / 'data'
CLASSES_YML: Path = DATA_DIR / 'classes.yml'

def getFlavor(name: str) -> str:
    if name.startswith('Raw'):
        return 'Common'
    if name.startswith('Vanilla'):
        return 'Vanilla'
    if name.startswith('HGG'):
        return 'HGG'
    if name.startswith('UEE'):
        return 'UEE'

def getDataDirFor(flavor: str) -> Path:
    return DATA_DIR / 'classes' / flavor.lower()

yaml = YAML(typ='rt')
data = {}
cfg = YAMLConfig(str(CLASSES_YML))
data = cfg.cfg

for className, classData in data.items():
    o = {className: classData}
    flavor = getFlavor(className)
    filename = f'{className.lower()}.yml'
    #print(repr(filename))
    for _ in range(2):
        if filename.startswith('raw') or filename.startswith('uee') or filename.startswith('hgg'):
            filename=filename[3:]
        if filename.startswith('vanilla'):
            filename=filename[7:]
    path: Path = getDataDirFor(flavor) / filename
    parent: Path = path.parent
    if not parent.is_dir():
        print(f'Creating {parent}...')
        parent.mkdir(parents=True)
    print(f'Writing {path}...')
    with path.open('w') as f:
        yaml.dump(o, f)

    strdata = path.read_text()

    for k, needle in VARIABLES.items():
        strdata = strdata.replace(needle, f'{{{{ {k} }}}}')

    path.write_text(strdata)
