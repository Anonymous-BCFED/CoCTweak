from pathlib import Path
from typing import TYPE_CHECKING, Any, Optional, TextIO, Union
from ruamel.yaml import CommentedSeq

if TYPE_CHECKING:
    import pregex.core.classes


def toYamlFlowList(*l: Any) -> CommentedSeq:
    ret = CommentedSeq(l)
    ret.fa.set_flow_style()
    return ret


def toYamlBlockList(*l: Any) -> CommentedSeq:
    ret = CommentedSeq(l)
    ret.fa.set_block_style()
    return ret


def writeGeneratedPythonWarningHeader(
    f: TextIO, purpose: str, generator_filepath: Path, source_filepath: Optional[Path]=None
) -> None:
    lines = [
        "WARNING: @GENERATED CODE; DO NOT EDIT BY HAND",
        f"BY {generator_filepath.absolute().relative_to(Path.cwd()).as_posix()}",
    ]
    if source_filepath is not None:
        lines.append(
            f"FROM {generator_filepath.absolute().relative_to(Path.cwd()).as_posix()}"
        )
    lines.append(purpose)
    maxlen = max([len(x) for x in lines]) + 2  # padding
    startstop = "#" * (maxlen + 2) + "\n"
    f.write(startstop)
    for l in lines:
        f.write(f'#{l.center(maxlen," ")}#\n')
    f.write(startstop)
