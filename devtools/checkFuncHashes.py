# This script checks methods that start with '## from {filename}: {signature} [@HASH]'.
# If hash is not present, it adds it.

import hashlib
import os
import re
import shutil
import sys
from typing import Optional

from buildtools import log, os_utils

from coctweak_build.utils import base62encode

log.enableANSIColors()

HGG_DIR = os.path.join('coc', 'hgg')
UEE_DIR = os.path.join('coc', 'uee')
VANILLA_DIR = os.path.join('coc', 'vanilla')

REG_SIGREF = re.compile(r'^(?P<indent>[ \t]+)## from (?P<path>[^:]+): ?(?P<signature>[^@]+) ?(@ (?P<hash>.+))?$')

def getFunctionBySig(filename: str, signature: str) -> Optional[str]:
    o = []
    in_func = False
    buffer = ''
    block = 0
    found = False
    endAtEOL = False

    #print(repr(signature))
    signature = signature.replace('\t', ' '*4).replace('\r\n', '\n').replace('\r', '\n').strip()
    signature = re.sub(r'[\t ]{2,}', ' ', signature)
    #print(repr(signature))

    if 'static const' in signature:
        #log.info('Parser in endAtEOL mode.')
        endAtEOL = True

    def addToBuffer(c: str) -> None:
        nonlocal buffer
        buffer += c
    def commitBuffer(c: str) -> None:
        nonlocal buffer, o
        o += [buffer+c]
        buffer = ''
    with open(filename, 'r') as f:
        for line in f:
            l_stripped = line.strip()
            lsig = line.replace('\t', '    ').strip()
            lsig = re.sub(r'[\t ]{2,}', ' ', lsig)
            if not in_func:
                if endAtEOL:
                    if lsig.startswith(signature):
                        return lsig
                else:
                    if lsig == signature:
                        in_func = True
                        found = True
                        if lsig.endswith('{'):
                            block += 1
            else:
                buffer = ''
                for c in l_stripped:
                    if c == '{':
                        block += 1
                    elif c == '}':
                        block -= 1
                        if block <= 0:
                            commitBuffer(c)
                            return '\n'.join(o)
                    addToBuffer(c)
                commitBuffer('')
    if not found:
        return None
    return '\n'.join(o)

def _fixPathWith(envID: str, rootpath: str, path: str) -> str:
    prefix = f'[{envID}]'
    if path.startswith(prefix):
        subpath = path[len(prefix):]
        if subpath.startswith('/'):
            subpath = subpath[1:]
        path = os.path.join(rootpath, subpath)
    return path

def _unfixPathWith(envID: str, rootpath: str, path: str) -> str:
    prefix = f'[{envID}]'
    if path.startswith(rootpath):
        subpath = path[len(rootpath):]
        if subpath.startswith('/'):
            subpath = subpath[1:]
        path = os.path.join(prefix, subpath)
    return path

def parsePythonFile(filename: str, check_only=False, rehash=False) -> None:
    changes: int = 0
    errors: List[str] = []
    ln: int = 0
    with log.info(f'{filename}:'):
        with open(filename, 'r') as f:
            with open(filename+'.tmp', 'w') as w:
                for line in f:
                    ln += 1
                    m = REG_SIGREF.match(line.rstrip())
                    if m is None:
                        w.write(line)
                    else:
                        indent = m.group('indent')
                        path = m.group('path').strip()
                        signature = m.group('signature').strip()
                        knownhash = m.group('hash')

                        origpath = path

                        path = _fixPathWith('HGG', HGG_DIR, path)
                        path = _fixPathWith('UEE', UEE_DIR, path)
                        path = _fixPathWith('VANILLA', VANILLA_DIR, path)

                        fixedopath = _unfixPathWith('HGG', HGG_DIR, path)
                        fixedopath = _unfixPathWith('UEE', UEE_DIR, fixedopath)
                        fixedopath = _unfixPathWith('VANILLA', VANILLA_DIR, fixedopath)

                        logprefix = f'{origpath}: {signature}'

                        actualhash = ''
                        if not os.path.isfile(path):
                            with log.error(f'{logprefix}: <red>FAILED</red>'):
                                errors += [f'{logprefix}: Specified file {origpath!r} ({path!r}) is missing.']
                                log.error(f'Specified file {origpath!r} ({path!r}) is missing.')
                                w.write(f'{indent}## from {fixedopath}: {signature} @ !!!FILE MISSING!!!\n')
                                changes += 1
                                #errors += 1
                                continue

                        funcdata = getFunctionBySig(path, signature)
                        if funcdata is None:
                            with log.error(f'{logprefix}: <red>FAILED</red>'):
                                errors += [f'{logprefix}: Unable to find that signature in {path!r}.']
                                log.error(f'Unable to find that signature in %r.  Copy the *entire line* and try again.', path)
                                w.write(f'{indent}## from {fixedopath}: {signature} @ !!!FUNC MISSING!!!\n')
                                changes += 1
                                #errors += 1
                                continue

                        digest = hashlib.sha512(funcdata.encode('utf-8')).digest()
                        #hexhash = binascii.b2a_hex(digest).decode('ascii')
                        #print(repr(hexhash), len(hexhash))
                        actualhash = base62encode(digest)
                        #print(repr(actualhash), len(actualhash))

                        if rehash or knownhash is None:
                            w.write(f'{indent}## from {fixedopath}: {signature} @ {actualhash}\n')
                            if not check_only:
                                changes += 1
                                word = 'RE-hashed!' if rehash else 'Hashed!'
                                log.info(f'{logprefix}: <cyan>{word}</cyan>')
                            else:
                                with log.warning(f'{logprefix}: <yellow>NO HASH</yellow>'):
                                    log.warning('Run without --check-only to fix.')
                        else:
                            knownhash = knownhash.strip()
                            if actualhash == knownhash:
                                log.info(f'{logprefix}: <green>OK</green>')
                                w.write(f'{indent}## from {fixedopath}: {signature} @ {actualhash}\n')
                            else:
                                with log.error(f'{logprefix}: <red>FAILED</red>'):
                                    log.error('Known hash:  %s', knownhash)
                                    log.error('Actual hash: %s', actualhash)
                                errors += [f'{logprefix}: Hash mismatch!']
        if check_only:
            os.remove(filename+'.tmp')
            if len(errors) > 0:
                with log.error('%d errors.', len(errors)):
                    for e in errors:
                        log.error(e)
                sys.exit(1)
            return
        log.info('%d changes', changes)
        if len(errors) > 0:
            with log.error('%d errors, aborting overwrite.', len(errors)):
                for e in errors:
                    log.error(e)
            os.remove(filename+'.tmp')
            sys.exit(1)
        else:
            os.remove(filename)
            shutil.move(filename+'.tmp', filename)

def main():
    import argparse

    argp = argparse.ArgumentParser()
    argp.add_argument('--check-only', action='store_true', default=False, help='Only check if hashes are correct.  Do not overwrite.')
    argp.add_argument('--rehash', action='store_true', default=False, help='Overwrite all hashes with their new values.')
    args = argp.parse_args()

    targets = []
    for pyfile in os_utils.get_file_list('coctweak', start='.'):
        if pyfile.endswith('.py'):
            with open(pyfile, 'r') as f:
                if ' ## from ' in f.read():
                    targets += [pyfile]
    for pyfile in targets:
        parsePythonFile(pyfile, check_only=args.check_only, rehash=args.rehash)

if __name__ == '__main__':
    main()
