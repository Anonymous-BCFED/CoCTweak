import subprocess
from _consts import START_YEAR
import datetime
from pathlib import Path
import re
from pregex import Pregex
from pregex.core.quantifiers import Indefinite
from pregex.core.groups import Capture
from pregex.core.classes import AnyBetween

from _consts import NUITKA_FORBIDDEN_IMPORTS

from typing import List, Set, Tuple

from ruamel.yaml import YAML as Yaml
YAML=Yaml(typ='rt')

from buildtools import os_utils
from buildtools.maestro.nuitka import NuitkaTarget

DISABLED_INCLUDES: Set[str] = set()

PREG_FATAL_BAD_PACKAGE = (Pregex('FATAL: Error, failed to locate module \'')
                          + Capture(Indefinite(AnyBetween('a', 'z') |
                                    AnyBetween('0', '9') | '.' | '_'))
                          + '\' you asked to include.')


def main() -> None:
    NUITKA_PKG_NAME: str = '__main__'
    NUITKA_ENTRY_POINT: Path = Path('coctweak') / '__main__.py'
    NUITKA_OUT_DIR: Path = Path('tmp') / 'nuitka'
    NUITKA_EXECUTABLE: Path
    DIST_DIR = Path('dist')
    DIST_EXECUTABLE: Path

    # Windows-specific Nuitka opts
    major: int
    minor: int
    patch: int
    with open('coctweak/consts.py.in', 'r') as f:
        # VERSION = Version('0.3.0')
        m = re.search(r"Version\('(\d+)\.(\d+)\.(\d+)'\)", f.read())
        major = int(m[1])
        minor = int(m[2])
        patch = int(m[3])
    all_modules: List[str] = list()
    all_packages: List[str] = list()
    with open('data/pgo_modules.yml', 'r') as f:
        data = YAML.load(f)
        all_modules = data['modules']
        all_packages = data['packages']
    nuitka = NuitkaTarget(
        NUITKA_ENTRY_POINT,
        NUITKA_PKG_NAME,
        [str(NUITKA_ENTRY_POINT)])
    nuitka.company_name = 'CoCTweak Contributors'
    nuitka.file_description = 'Corruption of Champions Save Editor and Toolkit'
    nuitka.file_version = (major, minor, patch, 0)
    nuitka.product_name = 'CoCTweak'
    nuitka.product_version = (major, minor, patch, 0)
    nuitka.trademarks = (
        f"Copyright ©{START_YEAR}-{datetime.datetime.now().year} {nuitka.company_name}"
    )
    nuitka.enabled_plugins.add('anti-bloat')
    nuitka.enabled_plugins.add('pylint-warnings')
    # nuitka.included_packages.add('coctweak')
    # Required or we start getting errors about _cffi_backend
    nuitka.included_packages.add('pygit2')
    # ModuleNotFoundError: No module named 'miniamf.adapters._sets'
    nuitka.included_packages.add('miniamf')

    nuitka.nofollow_imports = True
    excluded: Set[str] = set(NUITKA_FORBIDDEN_IMPORTS)
    nuitka.included_modules = list(set(all_modules)-excluded)
    FILTERS:List[Pregex] = [
        PREG_FATAL_BAD_PACKAGE,
    ]
    i=0
    while True:
        i+=1
        print(f'[{i}] Invoking Nuitka...')
        try:
            o = nuitka.launchNuitkaWithNuitkaPlus(nuitka.getCommandLine())
        except subprocess.CalledProcessError as cpe:
            o = cpe.stdout.decode()
        captures:Set[Tuple[str,str]] = set()
        for l in o.splitlines():
            for filter_ in FILTERS:
                captures |= set(map(lambda x: (filter_.get_pattern(), x), filter_.get_captures(o)))

        if len(captures) == 0:
            # pprint.pprint(frozenset(excluded), indent=4)
            print('NUITKA_FORBIDDEN_IMPORTS: FrozenSet[str] = frozenset({')
            for imp in sorted(excluded):
                print('    '+repr(imp)+',')
            print('})')
            return
        for err, m in captures:
            m=m[0]
            print(f' - {m} ({err})')
            nuitka.included_modules.remove(m)
            excluded.add(m)

    # nuitka.included_packages=all_packages
if __name__ == "__main__":
    main()
