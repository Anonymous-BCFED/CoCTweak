import hashlib
import os
import re
import sys

import requests
from ruamel.yaml import YAML

from coctweak_build.utils import downloadIfChanged

yaml = YAML(typ='rt')


REG_ID=r'[a-zA-Z0-9]+'
REG_CONST=r'[A-Z0-9\_]+'
REG_STR=r'[^"]+'

class RegexBuilder(object):
    def __init__(self):
        self.regex = r''

    def addLiteral(self, part: str):
        self.regex += re.escape(part)
        return self

    def addIDMatch(self, groupKey: str):
        self.regex += r'"(?P<'+groupKey+'>'+REG_ID+r')"'
        return self

    def addStringMatch(self, groupKey: str):
        self.regex += r'"(?P<'+groupKey+'>'+REG_STR+r')"'
        return self

    def addConstMatch(self, groupKey: str):
        self.regex += r'(?P<'+groupKey+'>'+REG_CONST+r')'
        return self

    def addIntMatch(self, groupKey: str):
        self.regex += r'(?P<'+groupKey+r'>\d+)'
        return self

    def addFloatMatch(self, groupKey: str):
        self.regex += r'(?P<'+groupKey+r'>[0-9\.]+)'
        return self

    def addRegex(self, regex: str):
        self.regex += regex
        return self

    def addComma(self):
        self.regex += r', *'
        return self

    def asString(self) -> str:
        return self.regex

ALL_ITEM_FILES = {
    'https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/Items/JewelryLib.as': {
        'mod': 'hgg',
        'category': 'Jewelry',
        'md5sum': '3ffa9ca6c5f6afa8314c24c39548f0e5',
        'replace': {
            'MODIFIER_NONE': '0',
            'MODIFIER_MINIMUM_LUST': '1',
            'MODIFIER_FERTILITY': '2',
            'MODIFIER_CRITICAL': '3',
            'MODIFIER_REGENERATION': '4',
            'MODIFIER_HP': '5',
            'MODIFIER_ATTACK_POWER': '6',
            'MODIFIER_SPELL_POWER': '7',
            'PURITY': '8',
            'CORRUPTION': '9',
            'MODIFIER_FLAMESPIRIT': '10',
            'MODIFIER_ACCURACY': '11',
            'MODIFIER_ETHEREALBLEED': '12',
            'MODIFIER_SPECTRE': '13',
            'MODIFIER_FRENZY': '14',

            'DEFAULT_VALUE': '6'
        },
        'regexes': {
            # public const ACCRN2:Jewelry = new Jewelry("FocsRng2", "Focus Ring", "ring of focus", "an enchanted onyx ring of focus", 0, 15, 3000, "An enchanted ring topped with onyx. It will increase the wearer's focus, making them miss less often. ", "Ring");
            RegexBuilder()
            .addLiteral('public const ')
            .addConstMatch('constid')
            .addRegex(r':Jewelry *= *new Jewelry\(')
            .addIDMatch('id')
            .addComma()
            .addStringMatch('shortname')
            .addComma()
            .addStringMatch('name')
            .addComma()
            .addStringMatch('longname')
            .addComma()
            .addIntMatch('effect_id')
            .addComma()
            .addIntMatch('effect_mag')
            .addComma()
            .addIntMatch('value')
            .addComma()
            .addStringMatch('desc')
            .addComma()
            .addStringMatch('type')
            .addLiteral(');')
            .asString(): {}
        },
        'manual-entries': {
            # super("PatienceRing", "Patience Ring", "Ring of Patience", "a silver ring shaped like overlapping knots", 0, 0, 1, "An enchanted silver ring, shaped like two knots interlocking in a circle. The finishing on it is rough, as if crafted by a skilled artisan with poor tools. It soothes the mind and allows its wearer to act with patience and wisdom. ", "Ring");
            'PatienceRing': {
                'constid': 'PATIENCERING',
                'id': 'PatienceRing',
                'shortname': 'Patience Ring',
                'name': 'Ring of Patience',
                'longname': 'a silver ring shaped like overlapping knots',
                'effect_id': 0,
                'effect_mag': 0,
                'value': 1,
                'desc': "An enchanted silver ring, shaped like two knots interlocking in a circle. The finishing on it is rough, as if crafted by a skilled artisan with poor tools. It soothes the mind and allows its wearer to act with patience and wisdom. ",
                'type': 'Ring'
            },
            # super("SpectrRing", "Specter Ring", "Ring of the Specter", "a plain silver ring", 0, 1, 5000, "An enchanted plain-looking silver ring. It boosts the wearer's agility and critical precision, at the cost of reducing their health. It's likely that whoever crafted this wished to remain inconspicuous, and perhaps went too far in pursuing this end. ", "Ring");
            'SpectrRing': {
                'constid': 'RING_SPECTR',
                'id': 'SpectrRing',
                'shortname': 'Specter Ring',
                'name': 'Ring of the Specter',
                'longname': 'a plain silver ring',
                'effect_id': 0,
                'effect_mag': 0,
                'value': 1,
                'desc': "An enchanted silver ring, shaped like two knots interlocking in a circle. The finishing on it is rough, as if crafted by a skilled artisan with poor tools. It soothes the mind and allows its wearer to act with patience and wisdom. ",
                'type': 'Ring'
            }
        }
    },
    'https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/Items/ConsumableLib.as': {
        'mod': 'hgg',
        'category': 'Consumables',
        'md5sum': '5cf9116a61eb39c1fe013d6fdae9b468',
        'replace': {},
        'regexes': {
            #public const AUBURND:HairDye = new HairDye("AuburnD", "Auburn").setHeader("Auburn Hair Dye");
            RegexBuilder()
            .addLiteral('public const ')
            .addConstMatch('constid')
            .addRegex(r':HairDye *\= *new HairDye\(')
            .addIDMatch('id')
            .addComma()
            .addStringMatch('color')
            .addLiteral(').setHeader(')
            .addStringMatch('name')
            .addLiteral(');')
            .asString()
            : {
                'shortname': '{color} Dye',
                'longname': 'a vial of {color} hair dye',
                'value': 6,
                'description': "This bottle of dye will allow you to change the color of your hair. Of course if you don't have hair, using this would be a waste.",
            },
            #public const DARK_OL:SkinOil = new SkinOil("DarkOil", "Dark").setHeader("Dark Skin Oil");
            RegexBuilder()
            .addLiteral('public const ')
            .addConstMatch('constid')
            .addRegex(r':SkinOil *\= *new SkinOil\(')
            .addIDMatch('id')
            .addComma()
            .addStringMatch('color')
            .addLiteral(').setHeader(')
            .addStringMatch('name')
            .addLiteral(');')
            .asString()
            : {
                'shortname': '{color} Oil',
                'longname': 'a bottle of {color} oil',
                'value': 6,
                'description': "A small glass bottle filled with a smooth clear liquid. A label across the front says, \"{color} Skin Oil.\""
            }
        },
        'manual-entries': {}
    }
}

#orig = 'public const ACCRN2:Jewelry = new Jewelry("FocsRng2", "Focus Ring", "ring of focus", "an enchanted onyx ring of focus", 0, 15, 3000, "An enchanted ring topped with onyx. It will increase the wearer\'s focus, making them miss less often. ", "Ring");'
#assert re.match(ALL_ITEM_FILES['https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/Items/JewelryLib.as']['regexes'][0], orig) is not None

with open('itemfinder.yml', 'w') as f:
    yaml.dump(ALL_ITEM_FILES, f)

ITEMS = {}
for url, rules in ALL_ITEM_FILES.items():
    mod = rules['mod']
    if mod not in ITEMS:
        ITEMS[mod]={}

    content = downloadIfChanged(url, cache_content=True)

    digest = hashlib.md5(content.encode('utf-8')).hexdigest()
    print(f'MD5SUM: {digest}')
    assert rules['md5sum'] == digest

    if 'replace' in rules and len(rules['replace']) > 0:
        for needle,replace in rules['replace'].items():
            content = re.sub(needle, replace, content)

    for regex, values in rules['regexes'].items():
        for m in re.finditer(regex, content):
            print(repr(m.groupdict()))
            d = m.groupdict()
            for k,v in values.items():
                if isinstance(v, str):
                    d[k]=v.format(**d)
                else:
                    d[k]=v
            #d.update(values)
            ITEMS[mod][m['id']]=d

    for key, entry in rules['manual-entries'].items():
        print(repr(entry))
        ITEMS[mod][entry['id']]=entry

for modID, modData in ITEMS.items():
    dirpath = os.path.join('data', modID)
    if not os.path.isdir(dirpath):
        os.makedirs(dirpath)
    with open(os.path.join(dirpath, 'items.yml'), 'w') as f:
        yaml.dump(modData, f)
