from io import StringIO
import os
from pathlib import Path
from typing import Any, Dict, Generator, List
from ruamel.yaml import YAML
from natsort import natsort_keygen

def actual_stem(f:Path)->str:
    return f.name.split(".")[0]
def natural_value(f:Path)->Any:
    s = actual_stem(f)
    try:
        return float(s)
    except:
        try:
            return int(s)
        except:
            return s
natkey = natsort_keygen(key=lambda x: natural_value(x))
def findDocsIn(dir: Path) -> Generator[str, None, None]:
    if (dir / "index.template.md").is_file():
        yield "index"

    for file in sorted(dir.glob("*.template.md"), key=natkey):
        s = actual_stem(file)
        if s == "index":
            continue
        # print(s)
        yield s


def findDirsIn(dir: Path) -> Generator[Path, None, None]:
    for f in os.scandir(dir):
        if f.is_dir():

            yield f


def main() -> None:
    o: Dict[str, List[str]] = {".": list(findDocsIn(Path("docs-src")))}
    rootdir = Path.cwd() / "docs-src"
    for root, dirs, _ in os.walk(rootdir):
        for dir in dirs:
            d = (Path(root)/ dir)
            k = d.relative_to(rootdir).as_posix()
            o[k] = list(findDocsIn(d))
        # for file in files:
        #     print(Path('.') / (Path(root,file).relative_to(rootdir)))
    so = {}
    for k,v in sorted(o.items()):
        so[k]=v
    YAMLi = YAML(typ="safe", pure=True)
    YAMLi.default_flow_style=False
    YAMLi.indent()
    with open('doclist.yml.guess','w') as f:
        YAMLi.dump(so, f)


if __name__ == "__main__":
    main()
