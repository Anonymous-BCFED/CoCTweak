import json
import sys
from typing import Dict, List
from buildtools import os_utils
import os
from pathlib import Path
from ruamel.yaml import YAML as Yaml
import tqdm

YAML = Yaml(typ="rt")
commands: List[List[str]] = []
TEMP_DIR = Path("/tmp") if os_utils.is_linux() else Path(os.environ.get("TEMP"))
SANDBOX_DIR = TEMP_DIR / "coctweak-demo"
SOLLIB_DIR = (
    SANDBOX_DIR
    / ".macromedia"
    / "Flash_Player"
    / "#SharedObjects"
    / "AAAAAAAA"
    / "localhost"
)
FAKE_PROJ_DIR = SANDBOX_DIR / "coctweak"
COCTWEAK_EXE_NAME = "coctweak.exe" if os.name == "nt" else "coctweak"
COCTWEAK_EXE_DIST = (Path.cwd() / "dist" / COCTWEAK_EXE_NAME).absolute()


def main() -> None:
    modules = set()
    packages = set()
    os_utils.copytree(
        os.path.join(Path.cwd(), "sandbox-seed"), str(SOLLIB_DIR), verbose=False
    )
    os_utils.copytree(
        os.path.join(Path.cwd(), "data"), str(FAKE_PROJ_DIR / "data"), verbose=False
    )
    os_utils.copytree(
        os.path.join(Path.cwd(), "coctweak"),
        str(FAKE_PROJ_DIR / "coctweak"),
        verbose=False,
    )
    with open("data/pgo_cmds.yml", "r") as f:
        cmds: Dict[str, List[str]] = {}
        for cmd in YAML.load(f):
            k = os_utils._args2str(cmd)
            cmds[k] = cmd
        commands = list(cmds.values())
    _env = os_utils.ENV.clone()
    _env.set("COCTWEAK_OUTPUT_PGODATA", "1")
    _env.set("HOME", str(SANDBOX_DIR))  # hue
    _env.set("COCTWEAK_BUILDING_DOCS", "1")
    with tqdm.tqdm(commands, unit="command") as prog:
        for cmdline in prog:
            os_utils.copytree(
                os.path.join(Path.cwd(), "sandbox-seed"), str(SOLLIB_DIR), verbose=False
            )
            with os_utils.Chdir(str(FAKE_PROJ_DIR), quiet=True):
                cmdline = [str(sys.executable), "-m", "coctweak"] + cmdline
                prog.write(
                    "$ " + os_utils._args2str(os_utils._cmd_handle_args(cmdline, False))
                )
                os_utils.cmd(cmdline, echo=False, show_output=False, env=_env)
            with open(FAKE_PROJ_DIR / ".PGO_IMPORTS.json", "r") as f:
                data = json.load(f)
                modules |= set(data["modules"])
                packages |= set(data["packages"])

    for bmn in sys.builtin_module_names:
        if bmn in modules:
            modules.remove(bmn)
            print(f"- {bmn}")

    os_utils.copytree(
        os.path.join(Path.cwd(), "sandbox-seed"), str(SOLLIB_DIR), verbose=True
    )
    os_utils.ENV.set("COCTWEAK_OUTPUT_PGODATA", "0")
    with open("data/pgo_modules.yml", "w") as f:
        YAML.dump(
            {
                "modules": sorted(modules),
                "packages": sorted(packages),
            },
            f,
        )


if __name__ == "__main__":
    main()
