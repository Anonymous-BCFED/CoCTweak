import sys
from typing import List, Optional

from _consts import SUPPORT_TABLE_END, SUPPORT_TABLE_START

sys.path.append(".")

import tabulate

from coctweak.modinfo import ModInfo
from coctweak.consts import MOD_SUPPORT, TABULATE_TABLEFMT


def genSupportTable() -> str:
    # o = '| Mod | Version | Git Commit |\n'
    # o += '|---:|---|---|\n'
    modid: str
    modinfo: ModInfo
    headers: List[str] = ["Mod", "Version", "Git Commit"]
    colalign: List[str] = ["right", "left", "left"]
    rows: List[List[str]] = []
    for modid, modinfo in sorted(MOD_SUPPORT.items(), key=lambda x: x[0]):
        site = modinfo.urlgen.getSiteURL()
        commitURL = modinfo.urlgen.getCommitURL(modinfo.commit)
        # o += f'| [**{modid}**]({site}) | {modinfo.version} | [`{modinfo.commit}`]({commitURL}) |\n'
        rows.append(
            [
                f"[**{modid}**]({site})",
                f"{modinfo.version}",
                f"[`{modinfo.commit}`]({commitURL})",
            ]
        )
    return tabulate.tabulate(rows, headers=headers, tablefmt="pipe", colalign=colalign)


def genTextTable() -> str:
    headers: List[str] = ["Mod", "Site", "Version", "Git Commit"]
    colalign: List[str] = ["right", "left", "left", "left"]
    rows: List[List[str]] = []
    for modid, modinfo in sorted(MOD_SUPPORT.items(), key=lambda x: x[0]):
        site = modinfo.urlgen.getSiteURL()
        commitURL = modinfo.urlgen.getCommitURL(modinfo.commit)
        rows.append(
            [f"{modid}", site, modinfo.version, f"[`{modinfo.commit}`]({commitURL})"]
        )
    return tabulate.tabulate(
        rows, headers=headers, colalign=colalign, tablefmt=TABULATE_TABLEFMT
    )


def replaceSupportTableIn(
    filename: str,
    start: Optional[str] = None,
    end: Optional[str] = None,
    irreversible: bool = False,
    markdown: bool = True,
) -> None:
    if start is None:
        start = SUPPORT_TABLE_START
    startLen = len(start)
    if end is None:
        end = SUPPORT_TABLE_END
    endLen = len(end)
    contents = ""
    with open(filename, "r") as f:
        contents = f.read()
    startsAt = contents.find(start)
    if startsAt == -1:
        print(filename, "No START!")
        return
    endsAt = contents.index(end, startsAt) + endLen
    if endsAt == -1:
        print(filename, "No END!")
        return
    print(filename, "Found @", startsAt, endsAt)

    replacement: str = genSupportTable() if markdown else genTextTable()
    if not irreversible:
        replacement = f"{start}\n{replacement}\n{end}"

    contents = contents[:startsAt] + replacement + contents[endsAt:]
    with open(filename, "w") as f:
        f.write(contents)


def main():
    # import argparse
    # argp = argparse.ArgumentParser()
    # argp.add_arguments('--irreversible', action='store_true', default=False, help='Removes the START and END tags.')
    # argp.add_arguments('filename', type=str)

    replaceSupportTableIn("README.template.md")
    replaceSupportTableIn("docs-src/dist/README.template.md", markdown=False)
    replaceSupportTableIn("CHANGELOG.md")


if __name__ == "__main__":
    main()
