import datetime
import hashlib
import os
from pathlib import Path, PurePath
import re
import shutil
import tarfile
from typing import Optional, cast, List, TYPE_CHECKING
import zipfile

from buildtools import os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.fileio import CopyFilesTarget, CopyFileTarget  # NOQA
from buildtools.maestro.nuitka import NuitkaTarget
from ruamel.yaml import YAML as Yaml

from _consts import NUITKA_FORBIDDEN_IMPORTS, START_YEAR
from buildDocs import GenDocTarget

if TYPE_CHECKING:
    from hashlib import _Hash as HASH_TYPE

YAML = Yaml(typ="rt")


class MakeTarGZ(SingleBuildTarget):
    BT_LABEL = "TAR.GZ"

    def __init__(self, target, files=[], start=".", dependencies=[]):
        self.start = "."
        super().__init__(target, files, dependencies=dependencies)

    def build(self):
        with tarfile.open(self.target, "w:gz") as tar:
            for af in self.files:
                tar.add(af, arcname=os.path.relpath(af, start=self.start))


class MakeZip(SingleBuildTarget):
    BT_LABEL = "ZIP"

    def __init__(self, target, files=[], start=".", dependencies=[]):
        self.start = "."
        super().__init__(target, files, dependencies=dependencies)

    def build(self):
        with zipfile.ZipFile(self.target, "w", compression=zipfile.ZIP_DEFLATED) as z:
            flist: List[str] = []
            for af in self.files:
                if os.path.isfile(af):
                    flist += [af]
                elif os.path.isdir(af):
                    for root, _, files in os.walk(af):
                        for filename in files:
                            flist += [os.path.join(root, filename)]
            for af in flist:
                z.write(af, arcname=os.path.relpath(af, start=self.start))


class MakeHashSum(SingleBuildTarget):
    def __init__(self, target: str, dirToScan: str, dependencies=[]):
        self.dirToScan: str = dirToScan
        super().__init__(target, [], dependencies=dependencies)

    def mkHasher(self) -> "HASH_TYPE":
        return None

    def fmtLine(self, h: "HASH_TYPE", relpath: str) -> str:
        return f"\\{h.hexdigest()} *{relpath}"

    def build(self):
        self.files = [
            os.path.abspath(x)
            for x in os_utils.get_file_list(self.dirToScan, prefix=self.dirToScan)
            if not os.path.basename(x).startswith(".") and not x.endswith("SUM")
        ]
        with open(self.target, "w") as w:
            for filename in self.files:
                relpath = "/".join(
                    os.path.relpath(filename, start=self.dirToScan).split(os.sep)
                )
                h = self.mkHasher()
                with open(filename, "rb") as f:
                    while True:
                        b = f.read(1024)
                        if not b:
                            break
                        h.update(b)
                # w.write(f"\\{h.hexdigest()} *{relpath}\n")
                w.write(f"{self.fmtLine(h, relpath)}\n")


class MakeSHA512Sum(MakeHashSum):
    BT_LABEL = "SHA512SUM"

    def mkHasher(self) -> "HASH_TYPE":
        return hashlib.sha512()

    def fmtLine(self, h: "HASH_TYPE", relpath: str) -> str:
        return f"{h.hexdigest()}  {relpath}"


class MakeMD5Sum(MakeHashSum):
    BT_LABEL = "MD5SUM"

    def mkHasher(self) -> "HASH_TYPE":
        return hashlib.md5()

    def fmtLine(self, h: "HASH_TYPE", relpath: str) -> str:
        return f"{h.hexdigest()}  {relpath}"


class ExceptionalCopyFilesTarget(SingleBuildTarget):
    BT_TYPE = "MyCopyFiles"
    BT_LABEL = "COPYFILES"

    def __init__(
        self,
        vtarget: str,
        targetdir: Path,
        sourcedir: Path,
        dependencies=[],
        verbose=False,
        ignore=None,
        show_progress=False,
    ):
        self.sourcedir: Path = sourcedir
        self.targetdir: Path = targetdir
        self.verbose = verbose
        self.ignore = ignore
        self.show_progress = show_progress
        super().__init__(
            vtarget, dependencies=dependencies, files=[os.path.abspath(__file__)]
        )
        self.name = f"{sourcedir} -> {targetdir}"

    def is_stale(self):
        return True

    def serialize(self):
        data = super().serialize()
        return data

    def deserialize(self, data):
        super().deserialize(data)
        self.source, self.destination = data["files"]

    def get_config(self):
        return [str(self.sourcedir), str(self.destdir), self.ignore]

    def build(self):
        for entry in self.sourcedir.iterdir():
            relpath = PurePath(entry).relative_to(self.sourcedir)
            os_utils.single_copy(
                str(entry), str(self.targetdir / relpath.parent), verbose=self.verbose
            )


def add_nuitka_targets(bm: BuildMaestro, env, deps=[], version="nightly") -> List[str]:
    NUITKA_PKG_NAME: str = "__main__"
    NUITKA_ENTRY_POINT: Path = Path("coctweak") / "__main__.py"
    NUITKA_OUT_DIR: Path = Path("tmp") / "nuitka"
    NUITKA_EXECUTABLE: Path
    DIST_DIR = Path("dist")
    DIST_EXECUTABLE: Path

    # Windows-specific Nuitka opts
    major: int
    minor: int
    patch: int
    with open("coctweak/consts.py.in", "r") as f:
        # VERSION = Version('0.3.0')
        m = re.search(r"Version\('(\d+)\.(\d+)\.(\d+)'\)", f.read())
        major = int(m[1])
        minor = int(m[2])
        patch = int(m[3])
    # all_modules: List[str] = list()
    # all_packages: List[str] = list()
    # with open("data/pgo_modules.yml", "r") as f:
    #     data = YAML.load(f)
    #     all_modules = data["modules"]
    #     all_packages = data["packages"]
    nuitka = cast(
        NuitkaTarget,
        bm.add(
            NuitkaTarget(
                NUITKA_ENTRY_POINT,
                NUITKA_PKG_NAME,
                [str(NUITKA_ENTRY_POINT)],
                dependencies=deps,
            )
        ),
    )
    nuitka.company_name = "CoCTweak Contributors"
    nuitka.file_description = "Corruption of Champions Save Editor and Toolkit"
    nuitka.file_version = (major, minor, patch, 0)
    nuitka.product_name = "CoCTweak"
    nuitka.product_version = (major, minor, patch, 0)
    nuitka.copyright = (
        f"Copyright {START_YEAR}-{datetime.datetime.now().year} {nuitka.company_name}"
    )
    nuitka.trademarks = ""
    nuitka.enabled_plugins.add("anti-bloat")
    nuitka.enabled_plugins.add("pylint-warnings")
    # nuitka.included_packages.add('coctweak')
    nuitka.included_packages.add(
        "pygit2"
    )  # Required or we start getting errors about _cffi_backend
    nuitka.included_packages.add(
        "miniamf"
    )  # ModuleNotFoundError: No module named 'miniamf.adapters._sets'

    # nuitka.nofollow_imports = True
    # nuitka.follow_import_to = sorted(set(all_modules) - NUITKA_FORBIDDEN_IMPORTS)
    # nuitka.included_packages=all_packages

    # Error, the program tried to call itself with '-m' argument. Disable with '--no-deployment-flag=self-execution'
    nuitka.other_opts.append("--no-deployment-flag=self-execution")

    nuitka.legacy_launch = True  # NuitkaPlus is bork on windows.

    if os.name == "nt":
        DIST_EXECUTABLE = DIST_DIR / "coctweak.exe"
    else:
        DIST_EXECUTABLE = DIST_DIR / "coctweak"
    return [
        bm.add(
            CopyFileTarget(
                str(DIST_EXECUTABLE),
                str(nuitka.executable_mangled),
                dependencies=[nuitka.target],
            )
        ).target
    ]


def add_packaging_targets(
    bm: BuildMaestro, env, deps=[], version="nightly", deploy_dir="archives"
):
    (shutil.rmtree(x) for x in ("dist", "build"))

    os_utils.ensureDirExists("archives")

    copy_ops = []

    for basename in ("LICENSE", "CHANGELOG.md"):
        copy_ops += [
            bm.add(
                CopyFileTarget(
                    os.path.join("dist", basename), basename, dependencies=deps
                )
            ).target
        ]
    for basename in ("README", "FAQ"):
        copy_ops += [
            bm.add(
                GenDocTarget(
                    srcdir=str(Path("docs-src") / "dist"),
                    destdir=str(Path("dist")),
                    basedir="",
                    basename=basename,
                    dependencies=deps,
                )
            ).target
        ]

    copyBatches = bm.add(
        CopyFilesTarget(
            os.path.join(bm.builddir, "DIST_BATCHES.tgt"),
            os.path.join("batches"),
            os.path.join("dist", "batches"),
            verbose=False,
            dependencies=deps,
        )
    )
    copyBatches.ignore = "mine/"
    copy_ops += [copyBatches.target]

    for modID in ("hgg", "uee", "vanilla"):
        os_utils.ensureDirExists(os.path.join("dist", "data", modID))
        for typeID in ("items", "keyitems", "perks", "statuseffects"):
            copy_ops += [
                bm.add(
                    CopyFileTarget(
                        os.path.join(
                            "dist", "data", modID, f"{typeID}-merged.min.json"
                        ),
                        os.path.join("data", modID, f"{typeID}-merged.min.json"),
                        dependencies=deps,
                    )
                ).target
            ]
    ARCHIVE_DIR = Path("archives")

    archive_targets = []
    ext = ""
    platform = ""
    if os_utils.is_linux():
        ext = "tar.gz"
        platform = "linux"
        archive_targets = [
            bm.add(
                MakeTarGZ(
                    str(ARCHIVE_DIR / f"coctweak-linux-amd64-{version}.tar.gz"),
                    ["dist"],
                    start="dist",
                    dependencies=copy_ops,
                )
            ).target
        ]
    else:
        platform = "windows"
        ext = "zip"
        archive_targets = [
            bm.add(
                MakeZip(
                    str(ARCHIVE_DIR / f"coctweak-windows-amd64-{version}.zip"),
                    ["dist"],
                    start="dist",
                    dependencies=copy_ops,
                )
            ).target
        ]
    archive_sha512sum = ARCHIVE_DIR / "SHA512SUM"
    archive_md5sum = ARCHIVE_DIR / "MD5SUM"
    latest_archive_targets: Optional[List[str]] = None
    if "BUILD_NUMBER" in os.environ:
        latest_archive_targets = [
            bm.add(
                CopyFileTarget(
                    os.path.join("archives", f"coctweak-{platform}-amd64-latest.{ext}"),
                    archive_targets[0],
                    dependencies=archive_targets,
                )
            ).target
        ]

    checksums = [
        bm.add(
            MakeSHA512Sum(
                str(archive_sha512sum),
                deploy_dir,
                dependencies=latest_archive_targets or archive_targets,
            )
        ).target,
        bm.add(
            MakeMD5Sum(
                str(archive_md5sum),
                deploy_dir,
                dependencies=latest_archive_targets or archive_targets,
            )
        ).target,
    ]
    # basename = os.path.basename(archive_target)
    DEPLOY_PATH = Path(deploy_dir)
    DEPLOYED_MD5SUM = DEPLOY_PATH / "MD5SUM"
    DEPLOYED_SHA512SUM = DEPLOY_PATH / "SHA512SUM"
    if str(DEPLOY_PATH.absolute()) != str(ARCHIVE_DIR.absolute()):
        return [
            bm.add(
                CopyFileTarget(
                    str(DEPLOYED_MD5SUM),
                    str(archive_md5sum),
                    dependencies=checksums,
                )
            ).target,
            bm.add(
                CopyFileTarget(
                    str(DEPLOYED_SHA512SUM),
                    str(archive_sha512sum),
                    dependencies=checksums,
                )
            ).target,
        ]
    else:
        return checksums
