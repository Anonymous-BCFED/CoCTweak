from collections import OrderedDict
import hashlib
import json
import os

from buildtools import os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget


class SortJSONTarget(SingleBuildTarget):
    BT_LABEL = 'SORT JSON'
    def __init__(self, target, filename, dependencies=[]):
        self.filename = filename
        super().__init__(target, [filename], dependencies=dependencies)

    def build(self):
        data: dict = None
        with open(self.filename, 'r') as f:
            data = json.load(f)
        with open(self.filename, 'w') as f:
            json.dump(data, f, sort_keys=True, indent=2)

    def get_displayed_name(self):
        return self.filename

def add_fixjson_targets(bm: BuildMaestro, env, deps=[]):
    generated_targets = []
    for root, _, files in os.walk('data'):
        for basefilename in files:
            if basefilename.endswith('.gamedata.json'):
                pathhash = hashlib.md5(os.path.join(root, basefilename).encode(encoding='utf-8')).hexdigest()
                filename = os.path.join(bm.builddir, f'FIXJSON_{pathhash.upper()}.tgt')
                generated_targets += [bm.add(SortJSONTarget(filename, os.path.join(root, basefilename))).target]
    return generated_targets

def main():
    env = os_utils.ENV.clone()
    bm = BuildMaestro(os.path.join('.build', 'sortdata_py'))
    add_fixjson_targets(bm, env, [])
    bm.as_app()

if __name__ == '__main__':
    main()
