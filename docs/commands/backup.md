<!--
@GENERATED by devtools/buildDocs.py. DO NOT MANUALLY EDIT.
Edit docs-src/commands/backup.template.md instead!
-->
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Backup
# Backup Command

**This command requires [git](https://git-scm.com/) and [git lfs](https://git-lfs.com/) to be installed.**

`backup` goes through your Flash SharedObject library and backs up your saves, both in binary form, and in a decoded YAML format.

Backup also commits these saves to a local git repository so you can track changes, or revert to earlier saves.

**NOTE:** This will backup *all* `*.sol` files in your Flash shared object directory.

## Command Syntax

```shell
$ coctweak backup --help
```
```
usage: coctweak backup [-h] [--message MESSAGE] [--to-dir TO_DIR]
                       [--lso-dir LSO_DIR]

options:
  -h, --help         show this help message and exit
  --message MESSAGE  Set message.
  --to-dir TO_DIR    Set destination directory.
  --lso-dir LSO_DIR  Directory in which Flash Local Shared Object files are
                     stored.
```


## Example

```shell
$ coctweak backup --message 'Initial backup'
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

localhost/CoC_4_backup.sol:
  Ignored (backup).
localhost/CoC_11_backup.sol:
  Ignored (backup).
localhost/CoC_3.sol:
  -> backup/localhost/CoC_3/CoC_3.sol (Raw Copy)
  -> backup/localhost/CoC_3/CoC_3.raw.yml (Raw YAML)
  -> backup/localhost/CoC_3/CoC_3.yml (HGG vnull (BUG))
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
localhost/CoC_10.sol:
  -> backup/localhost/CoC_10/CoC_10.sol (Raw Copy)
  -> backup/localhost/CoC_10/CoC_10.raw.yml (Raw YAML)
  WARNING: UEECock: Object using legacy version stamp!
  -> backup/localhost/CoC_10/CoC_10.yml (UEE v1.0.2_mod_1.4.14)
localhost/CoC_5.sol:
  -> backup/localhost/CoC_5/CoC_5.sol (Raw Copy)
  -> backup/localhost/CoC_5/CoC_5.raw.yml (Raw YAML)
  -> backup/localhost/CoC_5/CoC_5.yml (HGG vnull (BUG))
localhost/CoC_2_backup.sol:
  Ignored (backup).
localhost/CoC_11.sol:
  -> backup/localhost/CoC_11/CoC_11.sol (Raw Copy)
  -> backup/localhost/CoC_11/CoC_11.raw.yml (Raw YAML)
  -> backup/localhost/CoC_11/CoC_11.yml (UEE v1.0.2_mod_1.4.17c)
localhost/CoC_4.sol:
  -> backup/localhost/CoC_4/CoC_4.sol (Raw Copy)
  -> backup/localhost/CoC_4/CoC_4.raw.yml (Raw YAML)
  -> backup/localhost/CoC_4/CoC_4.yml (HGG vnull (BUG))
localhost/CoC_5_backup.sol:
  Ignored (backup).
localhost/CoC_1.sol:
  -> backup/localhost/CoC_1/CoC_1.sol (Raw Copy)
  -> backup/localhost/CoC_1/CoC_1.raw.yml (Raw YAML)
  -> backup/localhost/CoC_1/CoC_1.yml (HGG v1.4.11)
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
localhost/CoC_6_backup.sol:
  Ignored (backup).
localhost/CoC_6.sol:
  -> backup/localhost/CoC_6/CoC_6.sol (Raw Copy)
  -> backup/localhost/CoC_6/CoC_6.raw.yml (Raw YAML)
  -> backup/localhost/CoC_6/CoC_6.yml (HGG vnull (BUG))
localhost/CoC_7.sol:
  -> backup/localhost/CoC_7/CoC_7.sol (Raw Copy)
  -> backup/localhost/CoC_7/CoC_7.raw.yml (Raw YAML)
  -> backup/localhost/CoC_7/CoC_7.yml (HGG v1.4.13)
localhost/CoC_9.sol:
  -> backup/localhost/CoC_9/CoC_9.sol (Raw Copy)
  -> backup/localhost/CoC_9/CoC_9.raw.yml (Raw YAML)
  -> backup/localhost/CoC_9/CoC_9.yml (Vanilla v1.0.2)
localhost/CoC_Main.sol:
  -> backup/localhost/CoC_Main/CoC_Main.sol (Raw Copy)
  -> backup/localhost/CoC_Main/CoC_Main.raw.yml (Raw YAML)
localhost/CoC_3_backup.sol:
  Ignored (backup).
localhost/CoC_10_backup.sol:
  Ignored (backup).
localhost/CoC_9_backup.sol:
  Ignored (backup).
localhost/CoC_1_backup.sol:
  Ignored (backup).
localhost/CoC_2.sol:
  -> backup/localhost/CoC_2/CoC_2.sol (Raw Copy)
  -> backup/localhost/CoC_2/CoC_2.raw.yml (Raw YAML)
  -> backup/localhost/CoC_2/CoC_2.yml (HGG vnull (BUG))
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
localhost/#CoC/EndlessJourney/CoC_10.sol:
  -> backup/localhost/#CoC/EndlessJourney/CoC_10/CoC_10.sol (Raw Copy)
  -> backup/localhost/#CoC/EndlessJourney/CoC_10/CoC_10.raw.yml (Raw YAML)
  ERROR: handleCOCSave(): Unknown save type.
localhost/#CoC/EndlessJourney/LastSaved.sol:
  -> backup/localhost/#CoC/EndlessJourney/LastSaved/LastSaved.sol (Raw Copy)
  -> backup/localhost/#CoC/EndlessJourney/LastSaved/LastSaved.raw.yml (Raw YAML)
localhost/#CoC/EndlessJourney/CoC_Main.sol:
  -> backup/localhost/#CoC/EndlessJourney/CoC_Main/CoC_Main.sol (Raw Copy)
  -> backup/localhost/#CoC/EndlessJourney/CoC_Main/CoC_Main.raw.yml (Raw YAML)
localhost/#CoC/EndlessJourney/CoC_10_backup.sol:
  Ignored (backup).
$ git init
hint: Using 'master' as the name for the initial branch. This default branch name
hint: is subject to change. To configure the initial branch name to use in all
hint: of your new repositories, which will suppress this warning, call:
hint: 
hint: 	git config --global init.defaultBranch <name>
hint: 
hint: Names commonly chosen instead of 'master' are 'main', 'trunk' and
hint: 'development'. The just-created branch can be renamed via this command:
hint: 
hint: 	git branch -m <name>
Initialized empty Git repository in /tmp/coctweak-demo/coctweak/backup/.git/
WARNING: [git config] Setting user.name to nobody, and user.email to devnull@localhost.
WARNING: [git config] This will prevent attaching potentially sensitive information to your CoC backups. If you plan to host the git repo remotely, though, you will have to change this.
WARNING: [git config] To change, edit config.yml.
$ git config --local user.name nobody
$ git config --local user.email devnull@localhost
$ git config --local user.signingkey 
$ git lfs install
Updated git hooks.
Git LFS initialized.
$ git lfs track *.sol *.ssl
Tracking "*.sol"
Tracking "*.ssl"
+ .gitattributes
+ .gitignore
+ localhost/#CoC/EndlessJourney/CoC_10/CoC_10.sol
+ localhost/#CoC/EndlessJourney/CoC_Main/CoC_Main.sol
+ localhost/#CoC/EndlessJourney/LastSaved/LastSaved.sol
+ localhost/CoC_1/CoC_1.sol
+ localhost/CoC_1/CoC_1.yml
+ localhost/CoC_10/CoC_10.sol
+ localhost/CoC_10/CoC_10.yml
+ localhost/CoC_11/CoC_11.sol
+ localhost/CoC_11/CoC_11.yml
+ localhost/CoC_2/CoC_2.sol
+ localhost/CoC_2/CoC_2.yml
+ localhost/CoC_3/CoC_3.sol
+ localhost/CoC_3/CoC_3.yml
+ localhost/CoC_4/CoC_4.sol
+ localhost/CoC_4/CoC_4.yml
+ localhost/CoC_5/CoC_5.sol
+ localhost/CoC_5/CoC_5.yml
+ localhost/CoC_6/CoC_6.sol
+ localhost/CoC_6/CoC_6.yml
+ localhost/CoC_7/CoC_7.sol
+ localhost/CoC_7/CoC_7.yml
+ localhost/CoC_9/CoC_9.sol
+ localhost/CoC_9/CoC_9.yml
+ localhost/CoC_Main/CoC_Main.sol
+ metadata.yml
$ git rm --cache .gitattributes
$ git add .gitattributes
$ git rm --cache .gitignore
$ git add .gitignore
$ git rm --cache localhost/#CoC/EndlessJourney/CoC_10/CoC_10.sol
$ git add localhost/#CoC/EndlessJourney/CoC_10/CoC_10.sol
$ git rm --cache localhost/#CoC/EndlessJourney/CoC_Main/CoC_Main.sol
$ git add localhost/#CoC/EndlessJourney/CoC_Main/CoC_Main.sol
$ git rm --cache localhost/#CoC/EndlessJourney/LastSaved/LastSaved.sol
$ git add localhost/#CoC/EndlessJourney/LastSaved/LastSaved.sol
$ git rm --cache localhost/CoC_1/CoC_1.sol
$ git add localhost/CoC_1/CoC_1.sol
$ git rm --cache localhost/CoC_1/CoC_1.yml
$ git add localhost/CoC_1/CoC_1.yml
$ git rm --cache localhost/CoC_10/CoC_10.sol
$ git add localhost/CoC_10/CoC_10.sol
$ git rm --cache localhost/CoC_10/CoC_10.yml
$ git add localhost/CoC_10/CoC_10.yml
$ git rm --cache localhost/CoC_11/CoC_11.sol
$ git add localhost/CoC_11/CoC_11.sol
$ git rm --cache localhost/CoC_11/CoC_11.yml
$ git add localhost/CoC_11/CoC_11.yml
$ git rm --cache localhost/CoC_2/CoC_2.sol
$ git add localhost/CoC_2/CoC_2.sol
$ git rm --cache localhost/CoC_2/CoC_2.yml
$ git add localhost/CoC_2/CoC_2.yml
$ git rm --cache localhost/CoC_3/CoC_3.sol
$ git add localhost/CoC_3/CoC_3.sol
$ git rm --cache localhost/CoC_3/CoC_3.yml
$ git add localhost/CoC_3/CoC_3.yml
$ git rm --cache localhost/CoC_4/CoC_4.sol
$ git add localhost/CoC_4/CoC_4.sol
$ git rm --cache localhost/CoC_4/CoC_4.yml
$ git add localhost/CoC_4/CoC_4.yml
$ git rm --cache localhost/CoC_5/CoC_5.sol
$ git add localhost/CoC_5/CoC_5.sol
$ git rm --cache localhost/CoC_5/CoC_5.yml
$ git add localhost/CoC_5/CoC_5.yml
$ git rm --cache localhost/CoC_6/CoC_6.sol
$ git add localhost/CoC_6/CoC_6.sol
$ git rm --cache localhost/CoC_6/CoC_6.yml
$ git add localhost/CoC_6/CoC_6.yml
$ git rm --cache localhost/CoC_7/CoC_7.sol
$ git add localhost/CoC_7/CoC_7.sol
$ git rm --cache localhost/CoC_7/CoC_7.yml
$ git add localhost/CoC_7/CoC_7.yml
$ git rm --cache localhost/CoC_9/CoC_9.sol
$ git add localhost/CoC_9/CoC_9.sol
$ git rm --cache localhost/CoC_9/CoC_9.yml
$ git add localhost/CoC_9/CoC_9.yml
$ git rm --cache localhost/CoC_Main/CoC_Main.sol
$ git add localhost/CoC_Main/CoC_Main.sol
$ git rm --cache metadata.yml
$ git add metadata.yml
$ git commit -a -m Initial backup
[master (root-commit) xxxxxxx] Initial backup
 27 files changed, 11712 insertions(+)
 create mode 100644 .gitattributes
 create mode 100644 .gitignore
 create mode 100644 localhost/#CoC/EndlessJourney/CoC_10/CoC_10.sol
 create mode 100644 localhost/#CoC/EndlessJourney/CoC_Main/CoC_Main.sol
 create mode 100644 localhost/#CoC/EndlessJourney/LastSaved/LastSaved.sol
 create mode 100644 localhost/CoC_1/CoC_1.sol
 create mode 100644 localhost/CoC_1/CoC_1.yml
 create mode 100644 localhost/CoC_10/CoC_10.sol
 create mode 100644 localhost/CoC_10/CoC_10.yml
 create mode 100644 localhost/CoC_11/CoC_11.sol
 create mode 100644 localhost/CoC_11/CoC_11.yml
 create mode 100644 localhost/CoC_2/CoC_2.sol
 create mode 100644 localhost/CoC_2/CoC_2.yml
 create mode 100644 localhost/CoC_3/CoC_3.sol
 create mode 100644 localhost/CoC_3/CoC_3.yml
 create mode 100644 localhost/CoC_4/CoC_4.sol
 create mode 100644 localhost/CoC_4/CoC_4.yml
 create mode 100644 localhost/CoC_5/CoC_5.sol
 create mode 100644 localhost/CoC_5/CoC_5.yml
 create mode 100644 localhost/CoC_6/CoC_6.sol
 create mode 100644 localhost/CoC_6/CoC_6.yml
 create mode 100644 localhost/CoC_7/CoC_7.sol
 create mode 100644 localhost/CoC_7/CoC_7.yml
 create mode 100644 localhost/CoC_9/CoC_9.sol
 create mode 100644 localhost/CoC_9/CoC_9.yml
 create mode 100644 localhost/CoC_Main/CoC_Main.sol
 create mode 100644 metadata.yml
rmdir backup/localhost/#CoC/EndlessJourney/CoC_10_backup (empty)
rmdir backup/localhost/CoC_5_backup (empty)
rmdir backup/localhost/CoC_3_backup (empty)
rmdir backup/localhost/CoC_4_backup (empty)
rmdir backup/localhost/CoC_6_backup (empty)
rmdir backup/localhost/CoC_2_backup (empty)
rmdir backup/localhost/CoC_9_backup (empty)
rmdir backup/localhost/CoC_1_backup (empty)
rmdir backup/localhost/CoC_11_backup (empty)
rmdir backup/localhost/CoC_10_backup (empty)
```
