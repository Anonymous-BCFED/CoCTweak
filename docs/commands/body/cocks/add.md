<!--
@GENERATED by devtools/buildDocs.py. DO NOT MANUALLY EDIT.
Edit docs-src/commands/body/cocks/add.template.md instead!
-->
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Breasts](/docs/commands/body/breasts/index.md)
/ Add
# Body > Cocks > Add Command

This subcommand adds the desired number of cocks (1 by default) to the player's character.

Note that this *dumb* feature will not recalculate perks or deactivate features like the game would in the same situation.

## Subcommand Syntax

```shell
$ coctweak body localhost 5 cocks add --help
```
```
usage: coctweak body host id cocks add [-h]
                                       [--knot-multiplier KNOT_MULTIPLIER]
                                       [--length LENGTH]
                                       [--piercing-type PIERCING_TYPE]
                                       [--piercing-short PIERCING_SHORT]
                                       [--piercing-long PIERCING_LONG]
                                       [--sock SOCK] [--type TYPE]
                                       [--width WIDTH]
                                       count

positional arguments:
  count                 How many to add.

options:
  -h, --help            show this help message and exit
  --knot-multiplier KNOT_MULTIPLIER, -k KNOT_MULTIPLIER
                        The knot multiplier (W*Km in most mods).
  --length LENGTH, -l LENGTH
                        The length of the cock. Will otherwise use the last
                        cock in the collection as a template.
  --piercing-type PIERCING_TYPE
                        What kind of penis piercing you have.
  --piercing-short PIERCING_SHORT
                        Short description of penis piercing. REQUIRED if
                        --piercing-type is set and is not NONE.
  --piercing-long PIERCING_LONG
                        Long description of penis piercing. REQUIRED if
                        --piercing-type is set and is not NONE.
  --sock SOCK, -s SOCK  Sock ID (none or null for no sock)
  --type TYPE, -t TYPE  Cock type to use. See your mod's CockType enumeration
                        for possible values. Will otherwise use the last cock
                        in the collection as a template.
  --width WIDTH, -w WIDTH
                        The width of the cock. Will otherwise use the last
                        cock in the collection as a template.
```


## Example

```shell
$ coctweak body localhost 5 cocks add 1 --type=HUMAN --length=5.5 --width=1 -k=0
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Added:
  1:
    5.5" (14cm)L x 1" (2.5cm)W (5.5in² area) HUMAN cock
    Variables:
      Type: HUMAN (0)
      Length: 5.5"
      Thickness: 1"
      Knot Multiplier: 0"
      Sock: NONE ('')
    Helpers:
      Type Flags: 
      Has Knot: no
      Has Sheath: no
      Has Sock: no
      Knot Diameter: 0"
      Area: 5.5"
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
```


