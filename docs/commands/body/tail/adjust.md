<!--
@GENERATED by devtools/buildDocs.py. DO NOT MANUALLY EDIT.
Edit docs-src/commands/body/tail/adjust.template.md instead!
-->
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Tail](/docs/commands/body/tail/index.md)
/ Adjust
# Body > Tail > Adjust Command

Note that this *dumb* feature will not recalculate perks or deactivate features like the game would in the same situation.

This subcommand tinkers a bunch of variables about the player character's tails at once. Each option is optional.

**IMPORTANT:** The `--type` command does NOT use Value Formatting.

## Value Formatting

| Prefix | Meaning                                           |
| -----: | ------------------------------------------------- |
|    `=` | Sets the property value equal to the given value. |
|    `+` | Adds to the property value.                       |
|    `-` | Subtracts from the property value.                |
|    `*` | Multiplies the property value.                    |
|    `/` | Divides the property value.                       |

| Suffix | Meaning                                              |
| -----: | ---------------------------------------------------- |
|    `f` | Force interpretation as a **f**loating point number. |

For example, `=1` sets the value to `1`, while `/2f` will divide it by `2.0`.

## Subcommand Syntax

```shell
$ coctweak body localhost 1 tail adjust --help
```
```
usage: coctweak body host id tail adjust [-h] [--type TYPE] [--venom VENOM]
                                         [--recharge RECHARGE]

options:
  -h, --help            show this help message and exit
  --type TYPE, -t TYPE  Tail type to use. See your mod's Tail.as constants for
                        possible values. (PLAIN, FUR, etc) DOES NOT USE
                        ADJUSTMENT LANGUAGE
  --venom VENOM, -v VENOM
  --recharge RECHARGE, -r RECHARGE
```


## Example

```shell
# Show our tail.
$ coctweak body localhost 1 tail show
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
DOG tail
Variables:
  Type: DOG (2)
  Venom: 100
  Recharge: 25
```

```shell
# Change to a spider abdomen with defaults
$ coctweak body localhost 1 tail adjust --type=SPIDER_ABDOMEN --venom =0 --recharge =0
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Tail type changed: 2 -> 5
SPIDER_ABDOMEN tail
Variables:
  Type: SPIDER_ABDOMEN (5)
  Venom: 0
  Recharge: 0
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
```

```shell
# Add 5 webbing.
$ coctweak body localhost 1 tail adjust --venom +5
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
SPIDER_ABDOMEN tail
Variables:
  Type: SPIDER_ABDOMEN (5)
  Venom: 5
  Recharge: 0
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
```

```shell
# Add another 5 webbing.
$ coctweak body localhost 1 tail adjust --venom +5
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
SPIDER_ABDOMEN tail
Variables:
  Type: SPIDER_ABDOMEN (5)
  Venom: 10
  Recharge: 0
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
```

```shell
# Halve our webbage.
$ coctweak body localhost 1 tail adjust --venom /2f
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
SPIDER_ABDOMEN tail
Variables:
  Type: SPIDER_ABDOMEN (5)
  Venom: 5
  Recharge: 0
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
```

```shell
# Double our webbage.
$ coctweak body localhost 1 tail adjust --venom '*2'
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
SPIDER_ABDOMEN tail
Variables:
  Type: SPIDER_ABDOMEN (5)
  Venom: 10
  Recharge: 0
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
```
