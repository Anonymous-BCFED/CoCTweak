<!--
@GENERATED by devtools/buildDocs.py. DO NOT MANUALLY EDIT.
Edit docs-src/commands/body/skin/show.template.md instead!
-->
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Skin](/docs/commands/body/skin/index.md)
/ Show
# Body > Skin > Show Command

The `show` subcommand allows you to show the state of your skin, as well as all variables available for manipulation.

**WARNING: This subcommand is still under active development.** It may change significantly as the feature is refined.

## Subcommand Syntax

```shell
$ coctweak body localhost 1 skin show --help
```
```
usage: coctweak body host id skin show [-h]

options:
  -h, --help  show this help message and exit
```


## Example

```shell
# Show our skin.
$ coctweak body localhost 1 skin show
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
smooth, light skin
Variables:
  Type: PLAIN (0)
  Tone: light
  Descriptor: skin
  Adjective: smooth
  Fur Color: midnight black
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
```


