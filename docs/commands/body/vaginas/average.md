<!--
@GENERATED by devtools/buildDocs.py. DO NOT MANUALLY EDIT.
Edit docs-src/commands/body/vaginas/average.template.md instead!
-->
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Vagina](/docs/commands/body/vagina/index.md)
/ Average
# Body > Vaginas > Average Command

This weird little command allows you to unify the size and shape of your clits without ten hours in Lumi's workshop.

## Subcommand Syntax
```shell
$ coctweak body localhost 7 vaginas average --help
```
```
usage: coctweak body host id vaginas average [-h]

options:
  -h, --help  show this help message and exit
```


## Example

```shell
$ coctweak body localhost 7 vaginas average
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_7.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Before:
  Vaginas:
    TIGHT, NORMAL, virgin vagina, with a clit 0.5" (1.3cm) in length
    Variables:
      Type: HUMAN (0)
      Fullness: 0
      Looseness: TIGHT (0)
      Virgin: yes
      Wetness: NORMAL (1)
After:
  Vaginas:
    TIGHT, NORMAL, virgin vagina, with a clit 0.5" (1.3cm) in length
    Variables:
      Type: HUMAN (0)
      Fullness: 0
      Looseness: TIGHT (0)
      Virgin: yes
      Wetness: NORMAL (1)
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_7.sol...
```


