<!--
@GENERATED by devtools/buildDocs.py. DO NOT MANUALLY EDIT.
Edit docs-src/commands/inventory/remove.template.md instead!
-->
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Inventory](/docs/commands/inventory/index.md)
/ Remove
# Inventory > Remove Command

This command will remove n items from your inventory of the given type.

## Command Syntax

```
usage: coctweak inventory host id remove [-h] inventory_key item_id count

positional arguments:
  inventory_key  Inventory key to modify. See inv list.
  item_id        Item ID. Invalid IDs will break your save!
  count          How many items to remove.

options:
  -h, --help     show this help message and exit

```

## Example

```shell
# Remove a single Akbal Saliva from all slots in your player's inventory:
$ coctweak inventory localhost 1 remove player AkbalSl 1
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Slot 1: 2 -> 1
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
```
