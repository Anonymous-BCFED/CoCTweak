<!--
@GENERATED by devtools/buildDocs.py. DO NOT MANUALLY EDIT.
Edit docs-src/commands/statuseffect/remove.template.md instead!
-->
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [StatusEffect](/docs/commands/statuseffect/index.md)
/ Remove
# Status Effect > Remove Command

Remove a given status effect.

## Command Syntax
```shell
$ coctweak statuseffect localhost 1 remove --help
```
```
usage: coctweak statuseffect host id remove [-h] sfx_id

positional arguments:
  sfx_id      Status Effect ID. See the listing of available status effects
              for your mod.

options:
  -h, --help  show this help message and exit
```



## Example
```shell
# Remove the ButtStretched effect.
$ coctweak statuseffect localhost 1 remove ButtStretched
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Removing 'ButtStretched'...
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
```



