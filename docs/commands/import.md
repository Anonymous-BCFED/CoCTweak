# Import Command

Import can import a given save from a YAML or JSON file to your SharedObject library.

## Command Syntax

```
usage: coctweak import [-h] [--format {yaml,json}] filename host id

positional arguments:
  filename              File to import from.
  host                  Hostname the save will be stored in
  id                    ID of the save, or Main for the Permanent Object file.

optional arguments:
  -h, --help            show this help message and exit
  --format {yaml,json}  Format to expect. YAML can usually parse JSON just fine.
```

## Example

```shell
$ ./coctweak.sh import ~/coctweak/save_1.yml localhost 1
```

```
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol...
```
