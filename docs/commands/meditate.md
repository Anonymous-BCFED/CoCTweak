<!--
@GENERATED by devtools/buildDocs.py. DO NOT MANUALLY EDIT.
Edit docs-src/commands/meditate.template.md instead!
-->

> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Meditate
# Heal Command

The meditate command simulates having one or more meditation sessions with Jojo.

**NOTE:** The code CoCTweak uses slightly differs from what's in CoC, due to floating-point number error and implementation differences.  Your Milage May Vary, so we recommend using `--dry-run` first to double-check everything before committing.

## Command Syntax

```shell
$ coctweak meditate --help
```
```
usage: coctweak meditate [-h] [--dry-run] [--diagnose-only] [--count COUNT]
                         [--ignore-corruption]
                         [--min-corruption MIN_CORRUPTION]
                         [--min-libido MIN_LIBIDO]
                         host id

positional arguments:
  host                  Flash hostname the save will be stored in. Use
                        @localWithNet instead of #localWithNet, and you can
                        use @file to specify a static filename instead of a
                        slot ID.
  id                    Save slot ID, or, if you used @file as the host
                        argument, the path to the file.

options:
  -h, --help            show this help message and exit
  --dry-run             Go through the motions, but do not actually save
                        changes.
  --diagnose-only       Don't fix anything, just show current health issues.
  --count COUNT         Conduct only a fixed number of sessions
  --ignore-corruption   Disregard corruption value when checking whether to
                        stop or not.
  --min-corruption MIN_CORRUPTION
                        Repeat until corruption is at or below this number.
  --min-libido MIN_LIBIDO
                        Repeat until libido is at or below this number.
```


## Example

```shell
$ coctweak meditate localhost 2 --dry-run
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_2.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Stats:
  Corruption: 9.0 (min: 0, max: 100)
  Lust......: 0.0 (min: 1, max: 100)
  Strength..: 26.5 (min: 0, max: 100)
  Speed.....: 27.75 (min: 0, max: 100)
  Libido....: 25.0 (min: 0, max: 100)
Meditating until corruption is below 0:
  0000: LUS: 0.0 -> 1, COR: 9.0 -> 7.0, STR: 26.5 -> 27.5, SPD: 27.75 -> 28.75, LIB: 25.0 -> 24.0
  0001: COR: 7.0 -> 5.0, STR: 27.5 -> 28.5, SPD: 28.75 -> 29.75, LIB: 24.0 -> 23.0
  0002: COR: 5.0 -> 3.0, STR: 28.5 -> 29.5, SPD: 29.75 -> 30.75, LIB: 23.0 -> 22.0
  0003: COR: 3.0 -> 1.0, STR: 29.5 -> 30.5, SPD: 30.75 -> 31.75, LIB: 22.0 -> 21.0
  0004: COR: 1.0 -> 0, STR: 30.5 -> 31.5, SPD: 31.75 -> 32.75, LIB: 21.0 -> 20.0
After 5 grueling sessions with Jojo, you feel purer.
WARNING: --dry-run enabled, so coctweak is not saving any changes.
```


