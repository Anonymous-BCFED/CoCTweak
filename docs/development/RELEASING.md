How to make a release:

## Preparation
1. Check TODO.md and the gitlab for any outstanding tasks.
1. Remove "In Development" from CHANGELOG.md
1. Run `git submodule update --init --remote -- coc/*` to update CoC mod submodules
1. Run `python devtools/updateSupportTables.py` and update CHANGELOG.md.
1. Double-check support table in [/README.template.md], [/docs-src/dist/README.md], and [/CHANGELOG.md]
1. Make sure there are no stacktraces in docs/.
1. Run `python devtools/buildDocs.py --rebuild` and check the output for errors. This will also rebuild PGO argument data.
1. Run `python devtools/profileModuleUsage.py` to generate PGO data.
1. Run `python devtools/trimPackageOpts.py` to remove bad PGO data. Paste the output over `NUITKA_FORBIDDEN_IMPORTS` in `devtools/_consts.py`.
1. Run `python devtools/build.py --rebuild --version=$VERSION` to test-build binaries.
1. Commit and push changes.

## Compiling

Run the Jenkins coctweak-release task, which will build for all platforms.

OR

Run `python devtools/build.py --rebuild --version=$VERSION` on all platforms.

## Deployment
1. If all goes well, commit any changes.
1. Run `git tag "v${VERSION}"`.
1. Run `git push --tags`
1. Copy/paste the latest CHANGELOG.md entry into a new file called RELEASE_NOTES.md
1. Run `python devtools/deploy.py`
1. Run `python devtools/setVersion.py` to set the dev version to the next patch version. (0.1.0 -> 0.1.1)
1. Honk
