This file describes how func-hashes work.

## Summary

*Func-hashes* ("function hashes") are a special comment with which a developer can mark a particular segment of python code as being derived from a function or variable in CoC code. By running `checkFuncHashes.py`, a hash of the contents of that function or variable will be appended to the comment. Builds will fail if later versions of CoC mods change the original AS code, due to a mismatch of the original hash and the current hash.

This means that a developer can cause a build to fail if a section of code in CoC changes.

## Adding a Func-Hash

To add a func-hash, simply add a comment to your code with the following format:

```python
## from [MODID]path/to/coc/code.as: public function signature() {
```
The function signature should be the exact line that starts the function, including bracket (if needed), but without leading or trailing whitespace. To make things easier, the Func Hash should precede the derivative section of code, and you should make a comment where it ends.

For example:

```python
    #...
    def _get_ovi_egg_count(self) -> int:
        '''
        Ovipositor egg count.
        '''
        ## from [HGG]/classes/classes/Creature.as: public function eggs():int {
        spider_ovi = self.save.getPerk(HGGPerkLib.SpiderOvipositor)
        # ...
```

To generate the hash, simply run:

```shell
python devtools/checkFuncHashes.py
```

The generated hash will be appended to the end of the line, like so:

```python
    #...
    def _get_ovi_egg_count(self) -> int:
        '''
        Ovipositor egg count.
        '''
        ## from [HGG]/classes/classes/Creature.as: public function eggs():int { @ gXbfem9ecfQVaCp7mq2hU3ov4pv9sv8bp5HTfXX48r9984d726IbpJ1yufln9hm9tV6Lae900aq8Bp1sF5UqbnuehC9DrdVR
        spider_ovi = self.save.getPerk(HGGPerkLib.SpiderOvipositor)
        # ...
```

## Oh no something changed

When something changes upstream (in HGG, for example), builds will automatically fail in order to notify developers to re-examine the
new code for any changes that need to be incorporated.  This will usually look like this:
```
02/05/2021 02:46:18 AM [INFO    ]: coctweak/saves/hgg/combatstats.py:
02/05/2021 02:46:18 AM [INFO    ]:   [HGG]/classes/classes/Scenes/Places/TelAdre/UmasShop.as: public static const NEEDLEWORK_DEFENSE_EXTRA_HP:int: OK
02/05/2021 02:46:18 AM [INFO    ]:   [HGG]/classes/classes/Character.as: public function maxHunger():Number {: OK
02/05/2021 02:46:18 AM [INFO    ]:   [HGG]/classes/classes/Creature.as: public function eggs():int {: OK
02/05/2021 02:46:18 AM [INFO    ]:   [HGG]/classes/classes/Creature.as: public function maxLust():Number {: OK
02/05/2021 02:46:18 AM [INFO    ]:   [HGG]/classes/classes/Player.as: private function minLustSoftCap(raw:Number):Number {: OK
02/05/2021 02:46:18 AM [ERROR   ]:   [HGG]/classes/classes/Player.as: public override function minLust():Number {: FAILED
02/05/2021 02:46:18 AM [ERROR   ]:     Known hash:  8Gg5GR62x0m09SRg5uej2ajVcAA8MU6zKfdOe0t8ni67R1Nh8i4aCY0pJ3xNc7tgavavLdpe8YT7c8dTWcvhaSs82uc3KatH
02/05/2021 02:46:18 AM [ERROR   ]:     Actual hash: eVp4oN23i247azq5u1eeUft09SV6hdcWS75xdTifxI2hNb8R2qy6M16u4fU68oBazT6H224edRd9W77VY7E7bWe0dKamJ2Pj
02/05/2021 02:46:18 AM [INFO    ]:   [HGG]/classes/classes/Creature.as: public function maxHPUnmodified():Number {: OK
02/05/2021 02:46:18 AM [INFO    ]:   [HGG]/classes/classes/Creature.as: public function maxHP():Number {: OK
02/05/2021 02:46:18 AM [INFO    ]:   [HGG]/classes/classes/Creature.as: public function maxFatigue():Number {: OK
02/05/2021 02:46:18 AM [INFO    ]:   0 changes
02/05/2021 02:46:18 AM [ERROR   ]:   1 errors, aborting overwrite.
02/05/2021 02:46:18 AM [ERROR   ]:     [HGG]/classes/classes/Player.as: public override function minLust():Number {: Hash mismatch!
```

To fix this:

1. Re-examine the code listed in the `## from` comment and integrate any necessary changes.
1. Re-create the `## from` comment.
1. Execute `python devtools/checkFuncHashes.py` and fix any remaining issues.
1. Execute `./BUILD.sh --rebuild` or `.\BUILD.cmd --rebuild` to ensure that all tests pass.

## Hashing variables

Variables are matched by the beginning of the line and hash the entire line. This way, you can check if the value changes.

```python
## from [HGG]/classes/classes/Scenes/Places/TelAdre/UmasShop.as: public static const NEEDLEWORK_DEFENSE_EXTRA_HP:int @ dTk9wE4ta9IA5gpdN437NgCZ02lcC73wVeofg9M07x1D2bKG4eOgdK3va2bWgP5aKw80U8i2dcWas47hy0b8fQfbpufcneRQ
NEEDLEWORK_DEFENSE_EXTRA_HP = 50
```

## Technical Information

### Hashing Methodology

The hash is SHA512, half-assedly "encoded" as Base62 using an alphanumeric alphabet. (`len(string.digits+string.ascii_letters) == 62`) A maximum of two bytes from the hash is encoded to a short integer at a time, then encoded to Base62.

### Format

```
## from [MODID]path.as: signature @ HASH
```
