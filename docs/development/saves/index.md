<!--
@GENERATED by devtools/buildDocs.py. DO NOT MANUALLY EDIT.
Edit docs-src/development/saves/index.template.md instead!
-->
<!-- @generated by devtools/createSaveIndexTemplates.py -->
> [CoCTweak](/README.md)
/ [Development](/docs/development)
/ Saves
# Save List

Sandbox saves:

```shell
$ coctweak ls localhost
```
```
CoCTweak v0.4.5
(c)2019-2023 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

localhost  1: Hank (No Notes Available.) - Lvl 30.0 Male, 20632G, 290d on Easy (HGG vhgg 1.4.11)
localhost  2: Hank (MOUNTAIN) - Lvl 4.0 Male, 21G, 23d on Easy (HGG vNone)
localhost  3: Hank (MOUNTAIN) - Lvl 4.0 Male, 86G, 28d on Easy (HGG vNone)
localhost  4: Hank (MOUNTAIN) - Lvl 4.0 Male, 262G, 13d on Easy (HGG vNone)
localhost  5: Hank (NEW SAVE) - Lvl 1.0 Male, 0G, 0d on Normal (HGG vNone)
localhost  6: Hank (No Notes Available.) - Lvl 30.0 Male, 19456G, 277d on Easy (HGG vNone)
localhost  7: Hilda (No Notes Available.) - Lvl 1.0 Female, 0G, 0d on Normal (HGG vhgg 1.4.13)
localhost  9: Bill (ORIGINAL COC) - Lvl 1.0 Male, 0G, 0d on N/A (Vanilla v1.0.2)
WARNING: UEECock: Object using legacy version stamp!
localhost  10: Dale (Revamp) - Lvl 1.0 Male, 0G, 0d on Normal (UEE v1.0.2_mod_1.4.14)
localhost  11: Dale (UEE) - Lvl 1.0 Male, 0G, 0d on Normal (UEE v1.0.2_mod_1.4.17c)
```


* [Slot 1](docs/development/saves/1.md)
* [Slot 2](docs/development/saves/2.md)
* [Slot 3](docs/development/saves/3.md)
* [Slot 4](docs/development/saves/4.md)
* [Slot 5](docs/development/saves/5.md)
* [Slot 6](docs/development/saves/6.md)
* [Slot 7](docs/development/saves/7.md)
* [Slot 9](docs/development/saves/9.md)
* [Slot 10](docs/development/saves/10.md)
* [Slot 11](docs/development/saves/11.md)