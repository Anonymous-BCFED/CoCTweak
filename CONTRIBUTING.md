Some stuff you should read before contributing code.

## Rules
First things first:

### Behaviour
In order to make this a welcoming community with a minimum of drama, the following rules are instated:

0. Due to the nature of the content of CoC and thereby CoCTweak's code, **you must be over the age of 18 to participate.** This is non-negotiable and will result in permanent bans.
  1. Jokes that result in you claiming that you are underage will be taken seriously, and result in permanent bans.
1. Don't be a dick&trade;
2. Don't cause unnecessary drama.  Examples include, but are not limited to:
  * Interpersonal troubles
  * Suicidal statements
  * Yelling about certain unconventional communities, either for or against.
  * Criticism of fetishes in the game or enjoyed by someone else.
  * Politics/societal issues, either for or against.
  * Raids, hacking, doxxing, harassment.
  * Unsolicited roleplay or private messages.
4. Follow GitLab rules.
5. We are currently required to follow the laws of the United States of America. Content with even dubious legality will be removed without warning.
5. If you do something that isn't listed here, but you need to be removed, we reserve the right to do so. (Anti-rule-lawyering provision)

### Issue Reports
1. Reports need to contain
  * A title accurately describing the issue
  * A description of the problem
  * How someone can reproduce the same problem (if possible)
  * What you expected to happen
  * What *actually* happens
  * What version/git commit you're on
2. Your report should contain a minimum of angst.

### Feature Requests
1. Requests shall contain:
  * A title accurately describing the content
  * What you want
  * How it will improve everyone's experience
2. Requests are subject to dismissal or stasis for a variety of reasons:
  * Time constraints
  * IRL things
  * Feasibility
  * Lack of knowledge
  * Scope issues
  * *etc.*

### Merge Requests
1. MRs shall contain:
  * A title accurately describing the content of the MR
  * A clear, concise description
  * Why this change was necessary
  * Any potential caveats, such as:
    * Community reaction
    * Backwards compatibility
    * Issues on other platforms
    * Testing problems

## Coding

Here are some things you should know.

### Coding Style
We don't have a code style, *yet* (mostly because my own code has evolved over time).  Just try to make it readable.  You will be asked to make changes if your code is not readable.

### FuncHashes
In some places in our code, we attempt to emulate CoC's code.  In order to ensure we're notified when the upstream code changes, we add in specially-formatted comments that are parsed by <code>[devtools/checkFuncHashes.py](devtools/checkFuncHashes.py)</code>.

The comment in question generally looks like:
```python
def _methodWithEmulatedCode(...):
  ## from [GAMEPREFIX]/path/to/file.as: public function signature():void {
  the = code.weAre(emulating)
  # [...]
```

When `checkFuncHashes.py` is next run, it will then find the file, find the specified line in the file, extract the method body, and add a 96-character SHA512 checksum of that method body (encoded in half-assed alphanumeric Base62) (Base16 would be too long, Base64 would be too ugly), prefixed with an  `@`, to the end of the `## from` comment.

```python
def _methodWithEmulatedCode(...):
  ## from [GAMEPREFIX]/path/to/file.as: public function signature():void { @ 7Rb09Ycm42cH1afcNTfi775t6S3bkaeRh6k28VyawTcf62dEfJhcMifhigQC0zxbZn4Rvgk36cVgRp5PP0s24NVbel2Bb6gD
  the = code.weAre(emulating)
  # [...]
```

When run afterwards, `checkFuncHashes.py` will then check that *known* hash with the current *actual* hash it gets from the newest "clean" game code.  If the two hashes match, the check passes.  Otherwise, it'll fail, telling you something changed.

```
06/17/2020 01:01:40 PM [INFO    ]: coctweak/path/to/emulated/code/file.py:
06/17/2020 01:01:40 PM [INFO    ]:   2: [GAMEPREFIX]/path/to/file.as: public function signature():void { @ 7Rb09Ycm42cH1afcNTfi775t6S3bkaeRh6k28VyawTcf62dEfJhcMifhigQC0zxbZn4Rvgk36cVgRp5PP0s24NVbel2Bb6gD
06/17/2020 01:01:40 PM [INFO    ]:     OK
06/17/2020 01:01:40 PM [INFO    ]:   0 changes
```
