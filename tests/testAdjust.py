from coctweak.utils import parsePropertyAdjustment, ERoundingType
import unittest
class TestAdjust(unittest.TestCase):
    def test_setting_value(self):
        orig = 0
        adj = '=9'
        expected = 9
        actual = parsePropertyAdjustment(orig, adj, 0, 100)
        self.assertEqual(actual, expected)
    def test_adding_value(self):
        orig = 1
        adj = '+9'
        expected = 10
        actual = parsePropertyAdjustment(orig, adj, 0, 100)
        self.assertEqual(actual, expected)
    def test_subtracting_value(self):
        orig = 1
        adj = '-1'
        expected = 0
        actual = parsePropertyAdjustment(orig, adj, -100, 100)
        self.assertEqual(actual, expected)
    def test_multiplying_value(self):
        orig = 5
        adj = '*10'
        expected = 50
        actual = parsePropertyAdjustment(orig, adj, -100, 100)
        self.assertEqual(actual, expected)
    def test_dividing_value(self):
        orig = 4
        adj = '/2'
        expected = 2
        actual = parsePropertyAdjustment(orig, adj, -100, 100)
        self.assertEqual(actual, expected)
    def test_rounding_value(self):
        orig = 1.0
        adj = '/2.0'
        expected = 0
        actual = parsePropertyAdjustment(orig, adj, -100, 100, ERoundingType.ROUND)
        self.assertEqual(actual, expected)
    def test_ceiling_value(self):
        orig = 1.
        adj = '/3.0'
        expected = 1
        actual = parsePropertyAdjustment(orig, adj, -100, 100, ERoundingType.CEILING)
        self.assertEqual(actual, expected)
    def test_floor_value(self):
        orig = 2.
        adj = '/3.0'
        expected = 0
        actual = parsePropertyAdjustment(orig, adj, -100, 100, ERoundingType.FLOOR)
        self.assertEqual(actual, expected)
