import unittest
from coctweak.saves.hgg.bodyparts.face import HGGFace
from tests._testSerializing import TestSerializationOf
class TestHGGFace(TestSerializationOf):
    '''
    Test last updated: February 10, 2021

    faceType: 0
    '''
    ## from [HGG]/classes/classes/BodyParts/Face.as: public class Face { @ 58z16ibCCcYl5mO4Xv8Kt8DxaEObF13MM4Av5FE4DT0jW0761bS8SC1hi8wu6az5oa3bxee8cWz6NBaL64dUfMV3kf1sp3bC
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'faceType',
    ]
    TEST_SUBJECT = HGGFace

if __name__ == '__main__':
    unittest.main()
