import unittest
from coctweak.saves.hgg.bodyparts.ears import HGGEars
from tests._testSerializing import TestSerializationOf

class TestHGGEars(TestSerializationOf):
    '''
    Test last updated: February 10, 2021
    
    earType: 0
    earValue: 0
    earsPLong: ''
    earsPShort: ''
    earsPierced: 0
    '''
    ## from [HGG]/classes/classes/BodyParts/Ears.as: public class Ears { @ djub98drk5oI6sT4RCe5QcKF072dcp2aN9Rv5Xm14SaTseOw8QTgmz66tgdw02K0Pi5DJ8ntbdg0ysfL69gwaYugL85fS49N
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'earType',
        'earValue',
        'earsPLong',
        'earsPShort',
        'earsPierced',
    ]
    TEST_SUBJECT = HGGEars

if __name__ == '__main__':
    unittest.main()
