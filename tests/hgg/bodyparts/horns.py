import unittest
from coctweak.saves.hgg.bodyparts.horns import HGGHorns
from tests._testSerializing import TestSerializationOf
class TestHGGHorns(TestSerializationOf):
    '''
    Test last updated: February 10, 2021

    hornType: 0
    horns: 0
    '''
    ## from [HGG]/classes/classes/BodyParts/Horns.as: public class Horns { @ 7br4Wic1CeumcVm1Uv53C3dp265d9f7cqcQAc2U67meaI3jNbJ61e38xU8dR4U78Xj5gi68f3hl2dc77H67R8q7gWK7Jf9GI
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'hornType',
        'horns',
    ]
    TEST_SUBJECT = HGGHorns

if __name__ == '__main__':
    unittest.main()
