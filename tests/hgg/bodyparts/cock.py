import unittest
from coctweak.saves.hgg.bodyparts.cock import HGGCock
from tests._testSerializing import TestSerializationOf
class TestHGGCock(TestSerializationOf):
    '''
    Test last updated: February 10, 2021

    cocks:
    - cockLength: 5.5
      cockThickness: 1
      cockType: 0
      knotMultiplier: 1
      pLongDesc: ''
      pShortDesc: ''
      pierced: 0
      serializationVersion: 1
      sock: ''
    '''
    ## from [HGG]/classes/classes/Cock.as: public class Cock implements Serializable { @ 6RlblMbyl6gZ05vgQu84o2bx564aOn2Jt9UB6WXa7BefU5xJ968bei9a5gYX9XC6K31HY1I3cdscfO6ck7XyfKJ3SQ2C2gj4
    EXPECTED_KEYS = [
        'cockLength',
        'cockThickness',
        'cockType',
        'knotMultiplier',
        'pierced',
        'pLongDesc',
        'pShortDesc',
        'serializationVersion',
        'sock',
    ]
    TEST_SUBJECT = HGGCock

if __name__ == '__main__':
    unittest.main()
