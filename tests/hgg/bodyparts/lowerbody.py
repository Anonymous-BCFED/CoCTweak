import unittest
from coctweak.saves.hgg.bodyparts.lowerbody import HGGLowerBody
from tests._testSerializing import TestSerializationOf
class TestHGGLowerBody(TestSerializationOf):
    '''
    Test last updated: February 10, 2021

    incorporeal: false
    legCount: 2
    lowerBody: 0
    '''
    ## from [HGG]/classes/classes/BodyParts/LowerBody.as: public class LowerBody { @ 3mn2X4gG260g00zaju70o8oDeqweWAfdl010fDC4IR7Lt7Vz91gaaC9vFbOrgDObex1pMa6LeIr8o79EM18H6QibLkbrfa8F
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'incorporeal',
        'legCount',
        'lowerBody',
    ]
    TEST_SUBJECT = HGGLowerBody

if __name__ == '__main__':
    unittest.main()
