import unittest
from coctweak.saves.hgg.bodyparts.ass import HGGAss
from tests._testSerializing import TestSerializationOf
class TestHGGAss(TestSerializationOf):
    '''
    Test last updated: February 10, 2021

    ass:
      # Bug due to [...].ass.push([]) in Saves.as.
      '0': []
      analLooseness: 0
      analWetness: 0
      fullness: 0
    buttRating: 2
    '''
    ## from [HGG]/classes/classes/Ass.as: public class Ass implements Serializable { @ 8Tx3xk0x82JFc2s2724ljcdp7HJ7vcfEjgCLb9m1LjgPS00Rgdt5l72s88pKfESelPe3Sd2j9i47ORfsVgN71h3eW77lp5rY
    EXPECTED_KEYS = [
        'analLooseness',
        'analWetness',
        'fullness',
    ]
    EXPECTED_KEYS_AT_ROOT = [
        'ass',
        'buttRating'
    ]
    TEST_SUBJECT = HGGAss

if __name__ == '__main__':
    unittest.main()
