import unittest
from coctweak.saves.hgg.bodyparts.vagina import HGGVagina
from tests._testSerializing import TestSerializationOf
class TestHGGVagina(TestSerializationOf):
    '''
    Test last updated: February 10, 2021

    vaginas:
    - clitLength: 0.5
      clitPierced: 0
      clitPLong: ''
      clitPShort: ''
      fullness: 0
      labiaPierced: 0
      labiaPLong: ''
      labiaPShort: ''
      recoveryProgress: 0
      serializationVersion: 1
      type: 0
      vaginalLooseness: 0
      vaginalWetness: 1
      virgin: true
    '''

    ## from [HGG]/classes/classes/Vagina.as: public class Vagina implements Serializable { @ 6So5Hh6t47HM5dmcYE88I7I2cmTd1o1GZ8Nm9J75yRdH34r08MS8Pl2F11H1gsR9ur5Mj22C4pN2Lo8wtcvB2th3gD9sQ6Zh
    EXPECTED_KEYS = [
        'clitLength',
        'clitPierced',
        'clitPLong',
        'clitPShort',
        'fullness',
        'labiaPierced',
        'labiaPLong',
        'labiaPShort',
        'recoveryProgress',
        'serializationVersion',
        'type',
        'vaginalLooseness',
        'vaginalWetness',
        'virgin'
    ]
    TEST_SUBJECT = HGGVagina

if __name__ == '__main__':
    unittest.main()
