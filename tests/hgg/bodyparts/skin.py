import unittest
from coctweak.saves.hgg.bodyparts.skin import HGGSkin
from tests._testSerializing import TestSerializationOf
class TestHGGSkin(TestSerializationOf):
    '''
    Test last updated: February 10, 2021

    furColor: 'no'
    skinAdj: ''
    skinDesc: skin
    skinTone: light
    skinType: 0
    '''
    ## from [HGG]/classes/classes/BodyParts/Skin.as: public class Skin { @ g2TaRbdkUeaq5djg9Pa5P4l82uQax29OO7uk4030jj27727p84FfRF99O6N92J16P281paJTaY2gnB3Z65Hg0T1gO9ee7cka
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'furColor',
        'skinAdj',
        'skinDesc',
        'skinTone',
        'skinType',
    ]
    TEST_SUBJECT = HGGSkin

if __name__ == '__main__':
    unittest.main()
