import unittest
from coctweak.saves.hgg.bodyparts.antennae import HGGAntennae
from tests._testSerializing import TestSerializationOf
class TestHGGAntennae(TestSerializationOf):
    '''
    Test last updated: March 14, 2021

    antennae: 0

    Serialization done in coc/hgg/classes/classes/Saves.as.
    saveFile.data.antennae = player.antennae.type;
    '''
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'antennae'
    ]
    TEST_SUBJECT = HGGAntennae

if __name__ == '__main__':
    unittest.main()
