import unittest
from coctweak.saves.hgg.bodyparts.tail import HGGTail
from tests._testSerializing import TestSerializationOf
class TestHGGTail(TestSerializationOf):
    '''
    Test last updated: February 10, 2021

    tailRecharge: 0
    tailType: 0
    tailVenum: 0
    '''
    ## from [HGG]/classes/classes/BodyParts/Tail.as: public class Tail { @ c3maxo8gGfYjaCLblp7Ei4sRcRe6UXbD4ga73ud7V52kAbZXfmpb3k6Vn3bKej78tw1i609h8Pr5ls1ly9uYeuY7Nzadbect
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'tailRecharge',
        'tailType',
        'tailVenum',
    ]
    TEST_SUBJECT = HGGTail

if __name__ == '__main__':
    unittest.main()
