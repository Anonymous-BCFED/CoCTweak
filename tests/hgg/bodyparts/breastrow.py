import unittest
from coctweak.saves.hgg.bodyparts.breastrow import HGGBreastRow
from tests._testSerializing import TestSerializationOf
class TestHGGBreastRow(TestSerializationOf):
    '''
    Test last updated: February 10, 2021

    breastRows:
    - breastRating: 0
      breasts: 2
      fuckable: false
      fullness: 0
      lactationMultiplier: 0
      milkFullness: 0
      nipplesPerBreast: 1
    '''
    ## from [HGG]/classes/classes/BreastRow.as: public class BreastRow { @ 4jg9WK7dn0PE6mY8oJ2JGbdR26ogBYchi8Lf5PL7E9anZ6fTbTJ9Rd3dh5XldEj7Qne9I8Kp9oFdb88ejbH1coh7TK46614Z
    EXPECTED_KEYS = [
        'breastRating',
        'breasts',
        'nippleCocks',
        'fuckable',
        'fullness',
        'lactationMultiplier',
        'milkFullness',
        'nipplesPerBreast',
    ]
    EXPECTED_KEYS_AT_ROOT = []
    TEST_SUBJECT = HGGBreastRow

if __name__ == '__main__':
    unittest.main()
