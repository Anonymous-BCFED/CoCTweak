import unittest
from coctweak.saves.hgg.bodyparts.wings import HGGWings
from tests._testSerializing import TestSerializationOf
class TestHGGWings(TestSerializationOf):
    '''
    Test last updated: June 30, 2020

    wingColor: 'no'
    wingColor2: 'no'
    wingType: 0
    '''
    ## from [HGG]/classes/classes/BodyParts/Wings.as: public class Wings extends BaseBodyPart { @ 9exgme97A3FxcuueDzacw5xG8yzamw1QSgge2vXfq5fnk0FIcE72x0aMKbpZ5Ci2dx5Uh6aD9UKg8laP8cfP3xG9lE4Wg66p
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'wingColor',
        'wingColor2',
        'wingType',
    ]
    TEST_SUBJECT = HGGWings

if __name__ == '__main__':
    unittest.main()
