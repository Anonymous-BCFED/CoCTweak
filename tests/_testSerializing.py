from typing import List, Type
import unittest
from coctweak.saves.common.bodyparts.bodypart import BodyPart
class TestSerializationOf(unittest.TestCase):
    EXPECTED_KEYS: List[str] = []
    EXPECTED_KEYS_AT_ROOT: List[str] = []
    TEST_SUBJECT:Type = None
    DEBUG: bool = False
    def _instantiate(self):
        if issubclass(self.TEST_SUBJECT, (BodyPart,)):
            return self.TEST_SUBJECT(None)
        return self.TEST_SUBJECT()

    def setUp(self):
        if self.DEBUG:
            a = self._instantiate().serialize()
            b = {}
            self._instantiate().serializeTo(b)
            print(self.TEST_SUBJECT.__name__)
            print('  '+repr(a))
            print('  '+repr(b))


    def test_contains_expected_keys(self):
        actual_keys = self._instantiate().serialize().keys()
        for k in self.EXPECTED_KEYS:
            self.assertIn(k, actual_keys)

    def test_doesnt_contain_anything_else(self):
        actual_keys = self._instantiate().serialize().keys()

        for k in actual_keys:
            self.assertIn(k, self.EXPECTED_KEYS)

    def test_contains_expected_keys_at_root(self):
        if len(self.EXPECTED_KEYS_AT_ROOT) == 0:
            self.skipTest('Object does not modify root.')
            return
        actualData: dict = {}
        self._instantiate().serializeTo(actualData)
        actual_keys: List[str] = list(actualData.keys())
        if self.TEST_SUBJECT.OBJECT_KEY is not None:
            if self.TEST_SUBJECT.OBJECT_KEY not in actual_keys:
                actual_keys += [self.TEST_SUBJECT.OBJECT_KEY]
        for k in self.EXPECTED_KEYS_AT_ROOT:
            self.assertIn(k, actual_keys)

    def test_doesnt_contain_anything_else_at_root(self):
        if len(self.EXPECTED_KEYS_AT_ROOT) == 0:
            self.skipTest('Object does not modify root.')
            return
        actualData: dict = {}
        self._instantiate().serializeTo(actualData)
        actual_keys: List[str] = list(actualData.keys())
        if self.TEST_SUBJECT.OBJECT_KEY is not None:
            if self.TEST_SUBJECT.OBJECT_KEY not in actual_keys:
                actual_keys += [self.TEST_SUBJECT.OBJECT_KEY]
        for k in actual_keys:
            self.assertIn(k, self.EXPECTED_KEYS_AT_ROOT)
