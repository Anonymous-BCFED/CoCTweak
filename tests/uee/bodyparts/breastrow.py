import unittest
from coctweak.saves.uee.bodyparts.breastrow import UEEBreastRow
from tests._testSerializing import TestSerializationOf
class TestUEEBreastRow(TestSerializationOf):
    '''
    breastRating: 0.0
    breasts: 2.0
    fuckable: false
    fullness: 0.0
    lactationMultiplier: 0.0
    milkFullness: 0.0
    nippleCocks: false
    nipplesPerBreast: 1.0
    serializationVersionDictionary:
      c862ee0a-5667-4fd3-a178-37a5e85c86d6: 1
    '''
    ## from [UEE]/classes/classes/BreastRow.as: public class BreastRow implements Serializable @ figbri6113OtdIN9lb8GqflY8uH5GpejddRfgro4yl0G9dDP3mg8Ck06l1eDgKf4iz09x9XkgTGbr3d503z9fOo8nwcVf3XP
    EXPECTED_KEYS = [
        'breastRating',
        'breasts',
        'fuckable',
        'fullness',
        'lactationMultiplier',
        'milkFullness',
        'nippleCocks',
        'nipplesPerBreast',
        'serializationVersionDictionary',
    ]
    EXPECTED_KEYS_AT_ROOT = []
    TEST_SUBJECT = UEEBreastRow

if __name__ == '__main__':
    unittest.main()
