import unittest
from coctweak.saves.uee.bodyparts.antennae import UEEAntennae
from tests._testSerializing import TestSerializationOf
class TestUEEAntennae(TestSerializationOf):
    '''
    Test last updated: March 14, 2021

    antennae: 0

    Serialization done in coc/uee/classes/classes/Saves.as.
    saveFile.data.antennae = player.antennae.type;
    '''
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'antennae'
    ]
    TEST_SUBJECT = UEEAntennae

if __name__ == '__main__':
    unittest.main()
