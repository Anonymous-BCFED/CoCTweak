import unittest
from coctweak.saves.uee.bodyparts.cock import UEECock
from tests._testSerializing import TestSerializationOf
class TestUEECock(TestSerializationOf):
    '''
    cocks:
    - cockLength: 5.5
      cockThickness: 1
      cockType: 0
      knotMultiplier: 1
      pLongDesc: ''
      pShortDesc: ''
      pierced: 0
      serializationVersionDictionary:
        98367a43-6bf4-4b5c-b509-abea7e416416: 1
      sock: ''
    '''
    ## from [UEE]/classes/classes/Cock.as: public class Cock implements Serializable @ 7Do6bLaZE5ZSf8vdnW1s2gyF6vJ4Lkduf3TlfefcNc0lFen47yo9uf7lT9yZclZefU4iX6jE96jfS2cCt0gW4Cj19TdC99v0
    EXPECTED_KEYS = [
        'cockLength',
        'cockThickness',
        'cockType',
        'knotMultiplier',
        'pLongDesc',
        'pShortDesc',
        'pierced',
        'serializationVersionDictionary',
        'sock',
    ]
    EXPECTED_KEYS_AT_ROOT = []
    TEST_SUBJECT = UEECock

if __name__ == '__main__':
    unittest.main()
