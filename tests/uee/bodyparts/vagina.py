import unittest
from coctweak.saves.uee.bodyparts.vagina import UEEVagina
from tests._testSerializing import TestSerializationOf
class TestUEEVagina(TestSerializationOf):
    '''
    vaginas:
    - clitLength: 0.5
      clitPLong: ''
      clitPShort: ''
      clitPierced: 0
      fullness: 0
      labiaPLong: ''
      labiaPShort: ''
      labiaPierced: 0
      recoveryProgress: 0
      serializationVersionDictionary:
        cfe61f89-7ab6-4e4b-83aa-33f738dd2f05: 1
      type: 0
      vaginalLooseness: 0
      vaginalWetness: 1
      virgin: true
    '''
    ## from [UEE]/classes/classes/Vagina.as: public class Vagina implements Serializable @ 3uo9VR41M33G40G3084na84dcenc2A8bXdEW4nUfr31SA6mjckXept1FI7BE0D17Skek09xw6tM4vO2wI8Kzexz8cEgU03wl
    EXPECTED_KEYS = [
        'clitLength',
        'clitPierced',
        'clitPLong',
        'clitPShort',
        'fullness',
        'labiaPierced',
        'labiaPLong',
        'labiaPShort',
        'recoveryProgress',
        'serializationVersionDictionary',
        'type',
        'vaginalLooseness',
        'vaginalWetness',
        'virgin',
    ]
    EXPECTED_KEYS_AT_ROOT = []
    TEST_SUBJECT = UEEVagina

if __name__ == '__main__':
    unittest.main()
