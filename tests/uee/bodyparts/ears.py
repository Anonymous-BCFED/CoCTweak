import unittest
from coctweak.saves.uee.bodyparts.ears import UEEEars
from tests._testSerializing import TestSerializationOf
class TestUEEEars(TestSerializationOf):
    '''
    Test last updated: July 3, 2020

    earType: 0
    earValue: 0
    earsPLong: ''
    earsPShort: ''
    earsPierced: 0
    '''
    ## from [UEE]/classes/classes/BodyParts/Ears.as: public class Ears @ ccZ6w9c7y1Mefym0xO9xE3C7c556BcfJx58zcgLeYH1fh9Pqd144OIed0eKF8yydMk9KVcvsf135B53NE0Ho54m0gsa6Q2oK
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'earType',
        'earValue',
        'earsPLong',
        'earsPShort',
        'earsPierced',
    ]
    TEST_SUBJECT = UEEEars

if __name__ == '__main__':
    unittest.main()
