import unittest
from coctweak.saves.uee.bodyparts.horns import UEEHorns
from tests._testSerializing import TestSerializationOf
class TestUEEHorns(TestSerializationOf):
    '''
    Test last updated: July 3, 2020
    hornType: 0
    horns: 0
    '''
    ## from [UEE]/classes/classes/BodyParts/Horns.as: public class Horns @ 20A1tH9K5cBJbzEejj5ysa465ZS6C67989y978X4XE4ZNdhG6gSh296WAeyc4tH3EK5mze8qdPn0op45f9eQbcu7zTaDi5m5
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'hornType',
        'horns',
    ]
    TEST_SUBJECT = UEEHorns

if __name__ == '__main__':
    unittest.main()
