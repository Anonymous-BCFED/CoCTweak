import unittest
from coctweak.saves.uee.bodyparts.face import UEEFace
from tests._testSerializing import TestSerializationOf
class TestUEEFace(TestSerializationOf):
    '''
    Test last updated: July 3rd, 2020
    faceType: 0
    '''
    ## from [UEE]/classes/classes/BodyParts/Face.as: public class Face @ 5yX9MraZL9ak46C0qw4urcvR9LU1o1ggtbUMdCz4tRezH2pbfZP5cHaOs6fGavccRx7Dr5MI9vsfhAbka3Fhe0B2u23vb3De
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'faceType',
    ]
    TEST_SUBJECT = UEEFace

if __name__ == '__main__':
    unittest.main()
