import unittest
from coctweak.saves.uee.bodyparts.lowerbody import UEELowerBody
from tests._testSerializing import TestSerializationOf
class TestUEELowerBody(TestSerializationOf):
    '''
    Test last updated: July 3, 2020

    incorporeal: false
    legCount: 2
    lowerBody: 0
    '''
    ## from [UEE]/classes/classes/BodyParts/LowerBody.as: public class LowerBody @ 1nw14m0Xn4pSbIl5MmaXP6of1EP6t29A0h1r9Wee6QcGN55A2LN13sfMA93W3V3gQw49ledC5gQ6zbd4ifUY3EM0Ia8Kt31c
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'incorporeal',
        'legCount',
        'lowerBody',
    ]
    TEST_SUBJECT = UEELowerBody

if __name__ == '__main__':
    unittest.main()
