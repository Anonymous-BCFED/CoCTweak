import unittest
from coctweak.saves.uee.bodyparts.skin import UEESkin
from tests._testSerializing import TestSerializationOf
class TestUEESkin(TestSerializationOf):
    '''
    Test last updated: July 3, 2020

    furColor: 'no'
    skinAdj: ''
    skinDesc: skin
    skinTone: light
    skinType: 0
    '''
    ## from [UEE]/classes/classes/BodyParts/Skin.as: public class Skin @ fPq5HV9cUcKzcLdatR2qq58Ne4xbqPaUnbNP24096FfQ860Pd565C63uLe4xgnv03g8i0cXdcfZ2txfNA0Tn2yT0xC18Jdib
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'furColor',
        'skinAdj',
        'skinDesc',
        'skinTone',
        'skinType',
    ]
    TEST_SUBJECT = UEESkin

if __name__ == '__main__':
    unittest.main()
