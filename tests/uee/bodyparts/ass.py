import unittest
from coctweak.saves.uee.bodyparts.ass import UEEAss
from tests._testSerializing import TestSerializationOf
class TestUEEAss(TestSerializationOf):
    '''
    ass:
      analLooseness: 0
      analWetness: 0
      fullness: 0
      serializationVersionDictionary:
        0a3bd267-6ce0-4fed-a81b-53a4ccd6c17d: 1
    '''
    ## from [UEE]/classes/classes/Ass.as: public class Ass implements Serializable @ a71bbUbuG5eY0il0pfbYgfpabPD0ME0fydiI2En1jheZR7OCgH01Q2bev2sh7wI5iNeLJgCX4vm1svf9335scS7fK13vT6Sa
    EXPECTED_KEYS = [
        'analLooseness',
        'analWetness',
        'fullness',
        'serializationVersionDictionary'
    ]
    EXPECTED_KEYS_AT_ROOT = [
        'ass',
        'buttRating'
    ]
    TEST_SUBJECT = UEEAss

if __name__ == '__main__':
    unittest.main()
