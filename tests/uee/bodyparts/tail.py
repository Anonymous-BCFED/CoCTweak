import unittest
from coctweak.saves.uee.bodyparts.tail import UEETail
from tests._testSerializing import TestSerializationOf
class TestUEETail(TestSerializationOf):
    '''
    Test last updated: July 3, 2020
    tailRecharge: 0
    tailType: 0
    tailVenum: 0
    '''
    ## from [UEE]/classes/classes/BodyParts/Tail.as: public class Tail @ ffP6tZ29PeOJ0eRc1R0Vucdu9z0erK9Joa3ieLja86eaO5VXbLKaeF7ghclXaVdesq3h8ai0g1T3o64AU8Ht7XU8TI8g7817
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'tailRecharge',
        'tailType',
        'tailVenum',
    ]
    TEST_SUBJECT = UEETail

if __name__ == '__main__':
    unittest.main()
