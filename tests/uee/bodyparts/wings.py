import unittest
from coctweak.saves.uee.bodyparts.wings import UEEWings
from tests._testSerializing import TestSerializationOf
class TestUEEWings(TestSerializationOf):
    '''
    Test last updated: July 3, 2020
    wingColor: 'no'
    wingColor2: 'no'
    wingType: 0
    '''
    ## from [UEE]/classes/classes/BodyParts/Wings.as: public class Wings extends BaseBodyPart @ 7fqbcffUc4bwbr05YZ6btbgjemCapfbuefs81j655r1ss6dbggh1FBdTs0CsbgPfAVfahbZu7JBh2I9jued36Pke927iA9Z9
    EXPECTED_KEYS = []
    EXPECTED_KEYS_AT_ROOT = [
        'wingColor',
        'wingColor2',
        'wingType',
    ]
    TEST_SUBJECT = UEEWings

if __name__ == '__main__':
    unittest.main()
