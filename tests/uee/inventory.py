import unittest

from ruamel.yaml import YAML

from coctweak.saves.uee.save import UEEKFlags, UEESave

yaml = YAML(typ='rt')
class TestUEEInventory(unittest.TestCase):
    EXAMPLE = '''
    inventory:
      itemStorage:
      - damage: 0
        id: B.Gossr
        quantity: 4
        unlocked: false
      serializationVersionDictionary:
        230d7e43-bbc6-4c25-bd76-1f1175a0c58e: 1
    '''
    ALL_KEYS = set(
        ['damage',
        'id',
        'quantity',
        'unlocked']
    )
    def test_chest_deserializes(self):
        save = UEESave()
        save.deserializeChestInventory(None, yaml.load(self.EXAMPLE))
        self.assertEqual(save.chestStorage.slots[0].id,       'B.Gossr')
        self.assertEqual(save.chestStorage.slots[0].quantity, 4)
        self.assertEqual(save.chestStorage.slots[0].damage,   0)
        self.assertEqual(save.chestStorage.slots[0].unlocked, False)

    def test_chest_serializes(self):
        save = UEESave()
        save.chestStorage.add('B.Gossr', 4)
        data = {
            'gearStorage': []
        }
        save.flags.set(UEEKFlags.MOD_SAVE_VERSION, 16) # Enable new save serialization
        save.saveInventory(data)
        #print(repr(data))
        self.assertIn('inventory', data.keys())
        self.assertIn('itemStorage', data['inventory'].keys())
        self.assertEqual(len(data['inventory']['itemStorage']), 10)
        # UEE locks all slots on save.
        self.assertEqual(sum(v['unlocked'] == True for v in data['inventory']['itemStorage']), 0)

        self.assertEqual(data['inventory']['itemStorage'][0]['id'],       'B.Gossr')
        self.assertEqual(data['inventory']['itemStorage'][0]['quantity'], 4)
        self.assertEqual(data['inventory']['itemStorage'][0]['damage'],   0)
        self.assertEqual(data['inventory']['itemStorage'][0]['unlocked'], False)


if __name__ == '__main__':
    unittest.main()
