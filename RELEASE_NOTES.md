# 0.4.5 - TBD

_TBD_

## Compatibility

<!-- devtools/updateSupportTable.py replaces this: -->

|                                                                     Mod | Version  | Git Commit                                                                                                                                              |
| ----------------------------------------------------------------------: | :------- | :------------------------------------------------------------------------------------------------------------------------------------------------------ |
|                   [**HGG**](https://gitgud.io/BelshazzarII/CoCAnon_mod) | 1.6.17.1 | [`b56b7c4ca0372455298cffe272e648eaa8caf305`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/b56b7c4ca0372455298cffe272e648eaa8caf305)              |
|    [**UEE**](https://github.com/Kitteh6660/Corruption-of-Champions-Mod) | 1.4.19   | [`54fb4a179bcd4aea1cefada8d06dc0688d297657`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/54fb4a179bcd4aea1cefada8d06dc0688d297657) |
| [**Vanilla**](https://github.com/OXOIndustries/Corruption-of-Champions) | 1.0.2    | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00)  |

<!-- devtools/updateSupportTable.py END -->

### Big Changes

- Added `enable-encounters` command to `npc amily`

### Bugs Slain

- Releases now present the correct SHA256 hash instead of `None`
- Some NPCs were missing variables here and there.
- Fix crash in `HGGHorns` when fetching `value`.  Thanks to Xana for the report!

### Documentation

_TBD_

### Developer Stuff

- Minor tweaks to Amily
- Fixes for packaging process.
- Generated RegexConsts lists use reduced attribute calls by setting `re.compile` and `re.MULTILINE` to module-level private variables.
- Generated RegexConsts lists now generate `__ALL__`.