| Prefix | Meaning                                           |
| -----: | ------------------------------------------------- |
|    `=` | Sets the property value equal to the given value. |
|    `+` | Adds to the property value.                       |
|    `-` | Subtracts from the property value.                |
|    `*` | Multiplies the property value.                    |
|    `/` | Divides the property value.                       |

| Suffix | Meaning                                              |
| -----: | ---------------------------------------------------- |
|    `f` | Force interpretation as a **f**loating point number. |

For example, `=1` sets the value to `1`, while `/2f` will divide it by `2.0`.
