> [CoCTweak](/README.md)
/ Guide
# Guide to CoCTweak

CoCTweak is a powerful save editor, but it can be a bit overwhelming, especially if you're just trying to do something simple or common.

Here are some common ways to use CoCTweak.

For *all* of these, you **must** first save your game, and you **should** back it up before running coctweak.

[[_TOC_]]


## Specifying which save slot to use

CoCTweak can find and use your save slots, but you have to tell it some additional information so it know *which* collection of slots to use, because they are stored based on website domain. We just use the term `domain` to refer to each collection of save slots, since they can actually be for a number of things.

For your PC (such as when you're using the Flash Projector or Lightspark), you tell CoCTweak to use `@lwn` (which becomes `#localWithNet`, which is tricky to put on the command line on Linux). Some older saves are under `localhost`.

```shell
# Show the state of slot #1 stored in the #localWithNet domain used by Adobe Flash Projector
$ coctweak show @lwn 1

# Show the state of slot #5 stored in static1.e621.net domain by any browser
$ coctweak show static1.e621.net 5
```

For exported `*.coc` files, use `@file` as the domain:

```shell
# Show the state of /home/user/exports/mysave.coc
$ coctweak show @file /home/gary/exports/mysave.coc

# Same, but for a Windows user.
$ coctweak show @file C:\Users\Gary\Documents\mysave.coc

# You can also use a relative path on Windows or Linux.
$ coctweak show @file ../exports/mysave.coc
```

## Common Cheats

### Healing

You got your ass kicked and need some HP.  CoCTweak offers a simple way to [heal](docs/commands/heal.md):

{%- set host = 'localhost'%}
{%- set slot = '1' %}
{{ cmd([COCTWEAK, 'heal', host, slot], echo=True)}}
{% do reset_slot(slot) %}

If you want to heal fatigue, hunger, and/or lust, add `--fatigue`, `--hunger`, and/or `--lust` to the command line.  If you want to heal all three, add `--all`.

### Adding Gems

You see a shiny sword in a shop, and want it.  However, you're a bit short.  Here's how to add 1,000 gems to your account using [set gems](docs/commands/set/gems.md).

{%- set host = 'localhost' %}
{%- set slot = '1' %}
{{ cmd([COCTWEAK, 'set', host, slot, 'gems', '+1000'], echo=True)}}
{% do reset_slot(slot) %}

### Meditating without the Fuzz

You managed to get a bit of corruption, but don't have access to Jojo for one reason or another.  You may also have so much corruption that you don't want to laboriously go through each meditation manually.

The [meditate](docs/commands/meditate.md) command can do what Jojo does, including the stat changes!

{%- set host = 'localhost' %}
{%- set slot = '2' %}
{{ cmd([COCTWEAK, 'meditate', host, slot], echo=True)}}
{% do reset_slot(slot) %}