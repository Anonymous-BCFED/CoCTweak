> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Set
# Set Command

This command permits setting certain specifics of the game state.

## Help Listing
{{ cmd([COCTWEAK, 'set', '--help'], echo=True) }}

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- flag --></inline-elem>[coctweak set &lt;host&gt; &lt;id&gt; flag ...](docs/commands/set/flag.md)
* <inline-elem><!-- gems --></inline-elem>[coctweak set &lt;host&gt; &lt;id&gt; gems ...](docs/commands/set/gems.md)
* <inline-elem><!-- notes --></inline-elem>[coctweak set &lt;host&gt; &lt;id&gt; notes ...](docs/commands/set/notes.md)
* <inline-elem><!-- stats --></inline-elem>[coctweak set &lt;host&gt; &lt;id&gt; stat ...](docs/commands/set/stat.md)
