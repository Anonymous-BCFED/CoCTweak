{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
{%- set flag = 'GAR_NAME' -%}
{%- set value = '--str=Gargoyle' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Set](/docs/commands/set/index.md)
/ Flag
# Set > Flag Command

This permits setting your save's KFlags directly.  Which KFlags are available depends upon your mod.

## Command Syntax
{{ cmd([COCTWEAK, 'set', host, slot, 'flag', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'set', host, slot, 'flag', flag, value], echo=True, comment="Set gargoyle's name to Gargoyle.") }}

{%- do reset_slot(slot) %}
