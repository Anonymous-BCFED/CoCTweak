{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Set](/docs/commands/set/index.md)
/ Stat
# Set > Stat Command

This permits setting your character's stats directly.  Which stats are available depends upon your mod.

## Command Syntax
{{ cmd([COCTWEAK, 'set', host, slot, 'stat', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'set', host, slot, 'stat', 'STR', '100'], echo=True, comment="Set player's STR to 100") }}

{%- do reset_slot(slot) %}
