{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Set](/docs/commands/set/index.md)
/ Gems
# Set > Gems Command

This command lets you change how many gems you have.

## Command Syntax
{{ cmd([COCTWEAK, 'set', host, slot, 'gems', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'set', host, slot, 'gems', '5000'], echo=True, comment="Set player gems to 5000.") }}
{{ cmd([COCTWEAK, 'set', host, slot, 'gems', '-2500'], echo=True, comment="Subtract 2500 gems.") }}
{{ cmd([COCTWEAK, 'set', host, slot, 'gems', '+2500'], echo=True, comment="Add 2500 gems.") }}

{%- do reset_slot(slot) %}
