> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Backup
# Backup Command

**This command requires [git](https://git-scm.com/) and [git lfs](https://git-lfs.com/) to be installed.**

`backup` goes through your Flash SharedObject library and backs up your saves, both in binary form, and in a decoded YAML format.

Backup also commits these saves to a local git repository so you can track changes, or revert to earlier saves.

**NOTE:** This will backup *all* `*.sol` files in your Flash shared object directory.

## Command Syntax

{{ cmd([COCTWEAK, 'backup', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'backup', '--message', 'Initial backup'], echo=True, replacements={
  '\\[master \\(root-commit\\) ([a-f0-9]{7})\\]': '[master (root-commit) xxxxxxx]'
}) }}
