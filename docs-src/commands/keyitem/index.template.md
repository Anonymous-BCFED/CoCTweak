> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ KeyItem
# Key Items

Key Items are permanently added to your game to signify that your character acquired a skill, item, or other highlight.

CoC also sometimes misuses them for flags.

CoC stores each keyitem as a dictionary containing four numeric properties, named `value1` through `value4`.  Most of the time, these values are left as 0 and are ignored by the game.  However, they can often store important information, like when a buff wears off, how many infections have taken place, etc.

## Help Listing

```
{{ cmd([COCTWEAK, 'keyitem', '--help']) }}
```

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
 * <inline-elem><!-- add --></inline-elem>[coctweak keyitem &lt;host&gt; &lt;id&gt; add ...](docs/commands/keyitem/add.md)
 * <inline-elem><!-- list --></inline-elem>[coctweak keyitem &lt;host&gt; &lt;id&gt; list ...](docs/commands/keyitem/list.md)
 * <inline-elem><!-- show --></inline-elem>[coctweak keyitem &lt;host&gt; &lt;id&gt; show ...](docs/commands/keyitem/show.md)
 * <inline-elem><!-- remove --></inline-elem>[coctweak keyitem &lt;host&gt; &lt;id&gt; remove ...](docs/commands/keyitem/remove.md)
 * <inline-elem><!-- set --></inline-elem>[coctweak keyitem &lt;host&gt; &lt;id&gt; set ...](docs/commands/keyitem/set.md)
