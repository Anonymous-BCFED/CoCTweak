{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
{%- set item = 'Dangerous Plants' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [KeyItem](/docs/commands/inventory/keyitem.md)
/ Set
# Key Items > Set Command

Set one of the values of a keyitem.

Each keyitem has four numeric values attached to it, making it a type of quartet.  Many datatypes in CoC act like this, despite the inefficiency.

In coctweak, each value is designated by a number, 0-3.

## Command Syntax
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'set', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'show', item], echo=True, comment="Show the initial state of the keyitem") }}
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'set', item, '0', '1', '--type=integer'], echo=True, comment="Set the first value of the "+item+" keyitem") }}
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'show', item], echo=True, comment="Show the state of the keyitem after we've screwed with it.") }}
{% do reset_slot(slot) %}
