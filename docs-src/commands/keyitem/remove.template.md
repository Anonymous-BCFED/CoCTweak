{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
{%- set item = 'Dangerous Plants' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [KeyItem](/docs/commands/inventory/keyitem.md)
/ Remove
# Key Items > Show Command

Shows a Key Item, plus any known interpretations of its values.

## Command Syntax
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'remove', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'remove', item], echo=True, comment="Remove the "+item+" keyitem") }}

{% do reset_slot(slot) %}
