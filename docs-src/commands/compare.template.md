> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Compare
# Compare Command

Compare converts two saves to YAML, and then executes `diff` in unified mode so you can peruse the differences.

## Command Syntax

{{ cmd([COCTWEAK, 'compare', '--help'], echo=True) }}

## Example

```shell
$ ./coctweak.sh compare localhost 2 localhost 3
```
```
$ diff -u --color=always /tmp/tmpl3xod50u/CoC_2.yml /tmp/tmpl3xod50u/CoC_3.yml
--- /tmp/tmpl3xod50u/CoC_2.yml  2019-06-30 19:11:54.791370782 -0700
+++ /tmp/tmpl3xod50u/CoC_3.yml  2019-06-30 19:11:54.851370277 -0700
@@ -1,5 +1,5 @@
-HP: 69.00000000000001
-XP: 951                                                                                                                                                                                                                                                                       
+HP: 170
+XP: 1061                                                                                                                                                                                                                                                                      
 a: 'a '
 age: 0
 antennae: 0
```
