
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Perk
# Perks

Perks are how the character is permanently modified in some form, without affecting base stats.  Examples include new storage chests, levelling perks, and more.

CoC also sometimes misuses them for flags.

CoC stores each perk as a dictionary containing four numeric properties, named `value1` through `value4`.  Most of the time, these values are left as 0 and are ignored by the game.  However, they can often store important information, like when a buff wears off, how many infections have taken place, etc.

## Help Listing
{{ cmd([COCTWEAK, 'perk', '--help'], echo=True) }}

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
 * <inline-elem><!-- add --></inline-elem>[coctweak perk &lt;host&gt; &lt;id&gt; add ...](docs/commands/perk/add.md)
 * <inline-elem><!-- list --></inline-elem>[coctweak perk &lt;host&gt; &lt;id&gt; list ...](docs/commands/perk/list.md)
 * <inline-elem><!-- show --></inline-elem>[coctweak perk &lt;host&gt; &lt;id&gt; show ...](docs/commands/perk/show.md)
 * <inline-elem><!-- remove --></inline-elem>[coctweak perk &lt;host&gt; &lt;id&gt; remove ...](docs/commands/perk/remove.md)
