{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
{%- set perk = 'Mage' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Perk](/docs/commands/perk/index.md)
/ Remove
# Perk > Show Command

Shows a perk, plus any known interpretations of its values.

## Command Syntax
{{ cmd([COCTWEAK, 'perk', host, slot, 'remove', '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'perk', host, slot, 'remove', perk], echo=True, comment="Remove the "+perk+" perk.") }}

{% do reset_slot(slot) %}
