{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Perk](/docs/commands/perk/index.md)
/ List
# Perk > List Command

Display a list of all active perks, plus value information.

## Command Syntax
{{ cmd([COCTWEAK, 'perk', host, slot, 'ls', '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'perk', host, slot, 'ls'], echo=True, comment="List all held perks.") }}
{{ cmd([COCTWEAK, 'perk', host, slot, 'ls', '--format=markdown'], echo=True, lang='markdown', comment="Same, but in markdown") }}
{{ cmd([COCTWEAK, 'perk', host, slot, 'ls', '--format=json'], echo=True, lang='json', comment="And in JSON") }}
{{ cmd([COCTWEAK, 'perk', host, slot, 'ls', '--format=yaml'], echo=True, lang='yaml', comment="And finally in YAML.") }}
