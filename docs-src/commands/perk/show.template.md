{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
{%- set perk = 'Mage' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Perk](/docs/commands/perk/index.md)
/ Show
# Perk > Show Command

Shows a perk, plus any known interpretations of its values.

## Command Syntax
{{ cmd([COCTWEAK, 'perk', host, slot, 'show', '--help'], echo=True, comment="") }}


## Example
{{ cmd([COCTWEAK, 'perk', host, slot, 'show', perk], echo=True, comment="Show information about the "+perk+" perk.") }}
