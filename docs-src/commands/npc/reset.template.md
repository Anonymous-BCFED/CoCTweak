{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
{%- set npcid = 'amily' -%}
{%- set npcname = 'Amily' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [NPC](/docs/commands/npc/index.md)
/ Reset
# NPC > Reset Command

Resets all known variables on an NPC, including counters and stats.

## Command Syntax
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'reset', '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'show'], echo=True, comment="List all variables for "+npcid) }}
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'reset', 'AMILY_BIRTH_TOTAL'], echo=True, comment="Reset birth count") }}
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'show'], echo=True, comment="List all variables for "+npcid) }}

{% do reset_slot(slot) %}
