{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
{%- set npcid = 'amily' -%}
{%- set npcname = 'Amily' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [NPC](/docs/commands/npc/index.md)
/ Set
# NPC > Set Command

Set the value of any known variable on an NPC, assuming it isn't set to `readonly`.

## Command Syntax
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'set', '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'show'], echo=True, comment="List all variables for "+npcid) }}
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'set', 'AMILY_MET', 'int', '1'], echo=True, comment="You've met Amily!") }}
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'show'], echo=True, comment="List all variables for "+npcid) }}

{% do reset_slot(slot) %}
