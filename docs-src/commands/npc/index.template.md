> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ NPC
# NPC

This set of commands provides high-level access to NPC variables and commands.

Each NPC in each mod has its own set of commands, but the common commands are listed below.

For other commands, use `--help`.

For a list of available NPCs in your save, use [`npc-list`](docs/commands/npc-list.md).

## Help Listing
{{ cmd([COCTWEAK, 'npc', '--help'], echo=True) }}

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
 * <inline-elem><!-- preg --></inline-elem>[coctweak npc &lt;host&gt; &lt;id&gt; &lt;npcid&gt; preg ...](docs/commands/npc/preg/index.md)
 * <inline-elem><!-- breasts --></inline-elem>[coctweak npc &lt;host&gt; &lt;id&gt; &lt;npcid&gt; breasts ...](docs/commands/npc/breasts/index.md)
 * <inline-elem><!-- reset --></inline-elem>[coctweak npc &lt;host&gt; &lt;id&gt; &lt;npcid&gt; reset ...](docs/commands/npc/reset.md)
 * <inline-elem><!-- set --></inline-elem>[coctweak npc &lt;host&gt; &lt;id&gt; &lt;npcid&gt; set ...](docs/commands/npc/set.md)
 * <inline-elem><!-- show --></inline-elem>[coctweak npc &lt;host&gt; &lt;id&gt; &lt;npcid&gt; show ...](docs/commands/npc/show.md)
