{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
{%- set npcid = 'katherine' -%}
{%- set npcname = 'Kath' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [NPC](/docs/commands/npc/index.md)
/ [Breasts](/docs/commands/npc/breasts/index.md)
/ Adjust
# NPC > Breasts > Adjust Command

Allows you to tinker with breast-related variables on an NPC, assuming they're known and available.

## Value Formatting
{% include 'includes/valueFormatting.template.md' %}

## Command Syntax
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'breasts', 'adjust', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'breasts', 'show'], echo=True, comment="List all variables for "+npcid) }}
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'breasts', 'adjust', '--cup-size=C', '--lac-level=+1', '--fullness==50'], echo=True, comment="Set breasts to C-cups with increased lactation and in need of milking") }}
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'breasts', 'show'], echo=True, comment="List all variables for "+npcid) }}

{% do reset_slot(slot) %}
