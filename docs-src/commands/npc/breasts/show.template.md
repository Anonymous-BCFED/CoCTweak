{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
{%- set npcid = 'katherine' -%}
{%- set npcname = 'Kath' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [NPC](/docs/commands/npc/index.md)
/ [Breasts](/docs/commands/npc/breasts/index.md)
/ Show
# NPC > Preg > Show Command

Displays all known pregnancy-related variables on an NPC.

## Command Syntax
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'breasts', 'show', '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'breasts', 'show'], echo=True, comment="List all variables for "+npcid) }}
