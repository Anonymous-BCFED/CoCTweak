> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [NPC](/docs/commands/npc/index.md)
/ Pregnancy
# NPC > Preg

This set of commands provides high-level access to NPC variables and commands relating to pregnancies, if data is available.

Each NPC in each mod has its own set of commands, but the common commands are listed below.

For other commands, use `--help`.

For a list of available NPCs in your save, use [`npc-list`](docs/commands/npc-list.md).

## Help Listing
{{ cmd([COCTWEAK, 'npc', 'localhost', '1', 'amily', 'preg', '--help'], echo=True) }}

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
 * <inline-elem><!-- adjust --></inline-elem>[coctweak npc &lt;host&gt; &lt;id&gt; &lt;npcid&gt; set ...](docs/commands/npc/preg/adjust.md)
 * <inline-elem><!-- clear --></inline-elem>[coctweak npc &lt;host&gt; &lt;id&gt; &lt;npcid&gt; reset ...](docs/commands/npc/preg/clear.md)
 * <inline-elem><!-- show --></inline-elem>[coctweak npc &lt;host&gt; &lt;id&gt; &lt;npcid&gt; preg show...](docs/commands/npc/preg/show.md)
