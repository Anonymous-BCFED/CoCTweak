{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
{%- set npcid = 'amily' -%}
{%- set npcname = 'Amily' -%}
{%- set oriface = 'all' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [NPC](/docs/commands/npc/index.md)
/ [Pregnancy](/docs/commands/npc/preg/index.md)
/ Adjust
# NPC > Preg > Adjust Command

Allows you to tinker with pregnancy-related variables on an NPC, assuming they're known and available.

## Value Formatting
{% include 'includes/valueFormatting.template.md' %}

## Command Syntax
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'preg', oriface, 'adjust', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'preg', oriface, 'adjust', '--type=IMP', '--last-notice==0', '--incubation==0'], echo=True, comment="Add Imp pregnancy") }}

{% do reset_slot(slot) %}
