{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
{%- set npcid = 'amily' -%}
{%- set npcname = 'Amily' -%}
{%- set oriface = 'all' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [NPC](/docs/commands/npc/index.md)
/ [Pregnancy](/docs/commands/npc/preg/index.md)
/ Clear
# NPC > Preg > Clear Command

Clears all pregnancies on the NPC, assuming the method they use is known.

## Command Syntax
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'preg', oriface, 'clear', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'preg', oriface, 'clear'], echo=True, comment="Clear pregnancies.") }}

{% do reset_slot(slot) %}
