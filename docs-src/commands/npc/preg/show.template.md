{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
{%- set npcid = 'amily' -%}
{%- set npcname = 'Amily' -%}
{%- set oriface = 'all' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [NPC](/docs/commands/npc/index.md)
/ [Pregnancy](/docs/commands/npc/preg/index.md)
/ Show
# NPC > Preg > Show Command

Displays all known pregnancy-related variables on an NPC.

## Command Syntax
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'preg', oriface, 'show', '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'preg', oriface, 'show'], echo=True, comment="List all variables for "+npcid) }}
