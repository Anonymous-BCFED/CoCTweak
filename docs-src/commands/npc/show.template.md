{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
{%- set npcid = 'amily' -%}
{%- set npcname = 'Amily' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [NPC](/docs/commands/npc/index.md)
/ Show
# NPC > Show Command

Displays all known variables on an NPC, including counters and stats.

## Command Syntax
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'show', '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'npc', host, slot, npcid, 'show'], echo=True, comment="List all variables for "+npcid) }}
