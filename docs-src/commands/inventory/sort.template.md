{%- set host = 'localhost' -%}
{%- set slot = '6' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Inventory](/docs/commands/inventory/index.md)
/ Set
# Inventory > Sort Command

This tool allows you to sort an inventory collection by ID or value.

Value for each item type is taken from `data/{MOD ID}/items-merged.min.json`, if it exists.

Human-readable versions are available for:
* [HGG](docs/hgg/items.md)

## Command Syntax
```
{{ cmd([COCTWEAK, 'inventory', host, slot, 'sort', '--help']) }}
```

## Example

{{ cmd([COCTWEAK, 'inventory', host, slot, 'sort', 'player'], echo=True) }}

{{ cmd([COCTWEAK, 'inventory', host, slot, 'sort', 'chest'], echo=True) }}

{%- do reset_slot(slot) %}
