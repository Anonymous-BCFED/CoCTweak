{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Inventory](/docs/commands/inventory/index.md)
/ Add
# Inventory > Add Command

This command will add a given item to the indicated inventory target. It will attempt to combine
items into stacks like the game does, but may go over stack limits for some objects due to the
dumb nature of the save editor and its ignorance of which items have what stack size.

## Command Syntax
```
{{ cmd([COCTWEAK, 'inventory', host, slot, 'add', '--help']) }}
```

## Example

{{ cmd([COCTWEAK, 'inventory', host, slot, 'add', 'player', 'Equinum', '1'], echo=True) }}

{%- do reset_slot(slot) %}
