{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Inventory](/docs/commands/inventory/index.md)
/ List
# Inventory > List Command

The `list` subcommand will list all available inventory keys for the given save.


## Command Syntax

```
{{ cmd([COCTWEAK, 'inventory', host, slot, 'list', '--help']) }}
```

## Example

{{ cmd([COCTWEAK, 'inventory', host, slot, 'list'], echo=True) }}

{%- do reset_slot(slot) %}
