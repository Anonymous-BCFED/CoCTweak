> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Inventory
# Inventory Commands
These commands permit interaction with the inventory of the player and their furniture.

**This commandset is still in highly active development and WILL change frequently.**

## Help Listing

```
{{ cmd([COCTWEAK, 'inventory', 'localhost', '1', '--help']) }}
```

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- add --></inline-elem>[coctweak inventory &lt;host&gt; &lt;id&gt; add &lt;invkey&gt; &lt;item_id&gt; &lsqb;count&rsqb;](docs/commands/inventory/add.md)
* <inline-elem><!-- clear --></inline-elem>[coctweak inventory &lt;host&gt; &lt;id&gt; (clear|nuke) &lt;invkey&gt;](docs/commands/inventory/clear.md)
* <inline-elem><!-- list --></inline-elem>[coctweak inventory &lt;host&gt; &lt;id&gt; list](docs/commands/inventory/list.md)
* <inline-elem><!-- remove --></inline-elem>[coctweak inventory &lt;host&gt; &lt;id&gt; (remove|rm) &lt;invkey&gt; &lt;item_id&gt; &lsqb;count&rsqb;](docs/commands/inventory/remove.md)
* <inline-elem><!-- restash --></inline-elem>[coctweak inventory &lt;host&gt; &lt;id&gt; restash](docs/commands/inventory/restash.md)
* <inline-elem><!-- set --></inline-elem>[coctweak inventory &lt;host&gt; &lt;id&gt; set &lt;invkey&gt; &lt;slot_id&gt; &lt;item_id&gt;](docs/commands/inventory/set.md)
* <inline-elem><!-- sort --></inline-elem>[coctweak inventory &lt;host&gt; &lt;id&gt; sort &lt;invkey&gt;](docs/commands/inventory/set.md) --by=(id|value)
* <inline-elem><!-- unlock --></inline-elem>[coctweak inventory &lt;host&gt; &lt;id&gt; unlock &lt;invkey&gt; (&lsqb;count&rsqb;|auto|all)](docs/commands/inventory/unlock.md)
