{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Inventory](/docs/commands/inventory/index.md)
/ Unlock
# Inventory > Unlock Command

This powerful command permits you to change how many slots you've unlocked.

**WARNING:** This command currently only works for the pc/player inventory key. Chests will come later, if possible.

## How it works

`unlock` currently modifies your backpack's size so that, when added to the rest of the available slots, the desired number of slots are calculated.

For instance, if you have Strong Back and Strong Back 2, each of those perks counts for 1 slot. So, given that 3+1+1, you would have five slots available.
If you asked CoCTweak to ensure 10 slots were available, your backpack would be set to 10-(3+1+1), or 5. The end result is 3+1+1+5 = 10 character slots.

## Command Syntax
```
{{ cmd([COCTWEAK, 'inventory', host, slot, 'unlock', '--help']) }}
```
## Example

{{ cmd([COCTWEAK, 'inventory', host, slot, 'unlock', 'player', 'all'], echo=True) }}

{{ cmd([COCTWEAK, 'inventory', host, slot, 'unlock', 'player', '7'], echo=True) }}

{{ cmd([COCTWEAK, 'inventory', host, slot, 'unlock', 'player', 'auto'], echo=True) }}

{%- do reset_slot(slot) %}
