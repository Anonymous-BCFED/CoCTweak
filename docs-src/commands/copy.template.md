> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Copy
# Copy Command

The copy command allows you to duplicate a save to another slot, including on another hostname.

CoCTweak must be able to load the SOL file successfully, or the copy will fail.  The reason for a logical
load/save operation rather than a simple OS file copy is that flash will refuse to read the SOL unless the SOL file
content has the proper metadata.

## Command Syntax

{{ cmd([COCTWEAK, 'copy', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'copy', 'localhost', '1', '8ch.net', '5'], echo=True) }}
{% do remove_slot(5, '8ch.net') %}