{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ NPC-List
# NPC List Command

Displays a list of available NPC IDs for this save. Used for the `npc` command.

## Command Syntax
{{ cmd([COCTWEAK, 'npc-list', host, slot, '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'npc-list', host, slot], echo=True, comment="List all available NPCs.") }}
{{ cmd([COCTWEAK, 'npc-list', host, slot, '--format=yaml'], echo=True, comment="List all available NPCs in YAML.") }}
{{ cmd([COCTWEAK, 'npc-list', host, slot, '--format=json'], echo=True, comment="List all available NPCs in JSON.") }}
{{ cmd([COCTWEAK, 'npc-list', host, slot, '--format=json', '--compact'], echo=True, comment="List all available NPCs in compressed JSON.") }}
