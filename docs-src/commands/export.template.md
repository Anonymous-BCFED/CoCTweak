> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Export
# Export Command

`export` can export a given save from your SharedObject library to YAML or JSON.

## Command Syntax

{{ cmd([COCTWEAK, 'export', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'export', 'localhost', '1', 'save_1.yml'], echo=True) }}
