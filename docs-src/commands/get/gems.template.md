> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Get](/docs/commands/get/index.md)
/ Gems
# Get > Gems Command

This permits getting your character's gems directly.

## Command Syntax

{{ cmd([COCTWEAK, 'get', 'localhost', '1', 'gems', '-h'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'get', 'localhost', '1', 'gems'], echo=True) }}
