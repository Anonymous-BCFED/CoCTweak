> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Get](/docs/commands/get/index.md)
/ Flag

# Get > Flag Command

This command permits reading the value of a given kFlag.  **This is intended for advanced users.**

NOTE: To save people from having to make parsers, the header is automatically suppressed on `get` commands.

## Command Syntax

{{ cmd([COCTWEAK, 'get', 'localhost', '1', 'flag', '-h'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'get', 'localhost', '1', 'flag', 'ACHIEVEMENT_PROGRESS_DAMAGE_SPONGE'], echo=True, comment="A number") }}
{{ cmd([COCTWEAK, 'get', 'localhost', '1', 'flag', '1217'], echo=True, comment="A string") }}
{{ cmd([COCTWEAK, 'get', 'localhost', '1', 'flag', '_COCTWEAK_DATA'], echo=True, comment="A non-existent value") }}
