{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
{%- set effectID = 'Knows Charge' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [StatusEffect](/docs/commands/statuseffect/index.md)
/ Add
# Status Effects > Add Command

Add a status effect with this command.

**Note:** This command will not set flags or adjust stats like the game would.

## Command Syntax
{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'add', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'ls'], echo=True, comment="List active status effects.") }}
{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'add', effectID, '0', '0', '0', '0'], echo=True, comment="Add "+effectID+" status effect.") }}
{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'ls'], echo=True, comment="List effects again.") }}

{% do reset_slot(slot) %}
