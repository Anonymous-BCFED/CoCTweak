> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ StatusEffect
# Status Effects

Status effects are how many temporary effects are applied to the character.  Examples include combat buffs/debuffs, weapon effects, and diseases/parasites.

CoC also sometimes misuses them for flags and spell knowledge.

CoC stores each effect as a dictionary containing four numeric properties, named `value1` through `value4`.  Most of the time, these values are left as 0 and are ignored by the game.  However, they can often store important information, like when a buff wears off, how many infections have taken place, etc.

## Help Listing
{{ cmd([COCTWEAK, 'statuseffect', '--help'], echo=True) }}

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
 * <inline-elem><!-- add --></inline-elem>[coctweak statuseffect &lt;host&gt; &lt;id&gt; add ...](docs/commands/statuseffect/add.md)
 * <inline-elem><!-- list --></inline-elem>[coctweak statuseffect &lt;host&gt; &lt;id&gt; list ...](docs/commands/statuseffect/list.md)
 * <inline-elem><!-- show --></inline-elem>[coctweak statuseffect &lt;host&gt; &lt;id&gt; show ...](docs/commands/statuseffect/show.md)
 * <inline-elem><!-- remove --></inline-elem>[coctweak statuseffect &lt;host&gt; &lt;id&gt; remove ...](docs/commands/statuseffect/remove.md)
