> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ List
# List Command

The list command permits you to make a listing of all available Flash SharedObject library hostnames that contain CoC saves, and listings of the saves themselves.

## Command Syntax

```
{{ cmd([COCTWEAK, 'ls', '--help'], echo=True) }}
```

## Example

### Long format

{{ cmd([COCTWEAK, 'ls', 'localhost', '--format=long'], echo=True) }}

### CoC Format

{{ cmd([COCTWEAK, 'ls', 'localhost', '--format=coc'], echo=True) }}

### JSON Format

{{ cmd([COCTWEAK, 'ls', 'localhost', '--format=json'], echo=True) }}

### YAML Format

{{ cmd([COCTWEAK, 'ls', 'localhost', '--format=yaml'], echo=True) }}
