{%- set host = 'localhost' -%}
{%- set slot = '2' -%}{# Has 9 corruption #}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Meditate
# Heal Command

The meditate command simulates having one or more meditation sessions with Jojo.

**NOTE:** The code CoCTweak uses slightly differs from what's in CoC, due to floating-point number error and implementation differences.  Your Milage May Vary, so we recommend using `--dry-run` first to double-check everything before committing.

## Command Syntax

{{ cmd([COCTWEAK, 'meditate', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'meditate', host, slot, '--dry-run'], echo=True) }}

{% do reset_slot(slot) %}
