{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ Tail
# Body > Tail Commands

This set of subcommands allow you to modify every possible variable related to tails.

{% include 'includes/activeDevelopment.template.md' %}

## Save Variables

Skin variables are stored globally.

### Common

Vanilla and Revamp-based mods (UEE, HGG) use the following structure:

#### Variables
<table>
<thead>
<caption>Global Data Object Properties</caption>
<th>Name</th>
<th>Type</th>
<th>Default</th>
<th>Notes</th>
</thead>
<tbody>
<tr><th><code>tailType</code></th><td>Tail.* consts (int)</td><td><code>NONE (0)</code></td><td>Tail type.</td></tr>
<tr><th><code>tailVenum</code></th><td>int</td><td><code>0</code></td><td>[sic] Volume of venom/number of eggs/etc</td></tr>
<tr><th><code>tailRecharge</code></th><td>int</td><td>0</td><td>Recharge rate</td></tr>
</tbody>
</table>

## Command Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'tail', '--help'], echo=True) }}

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- adjust --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; tail (adjust|adj) ...](docs/commands/body/tail/adjust.md)
* <inline-elem><!-- set --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; tail set ...](docs/commands/body/tail/set.md)
* <inline-elem><!-- show --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; tail show](docs/commands/body/tail/show.md)
