{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Tail](/docs/commands/body/tail/index.md)
/ Show
# Body > Tail > Show Command

The set subcommand allows you to show the state of your tail, as well as all variables available for manipulation.

{% include 'includes/activeDevelopment.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'show', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'show'], echo=True, comment='Show our tail.') }}

{% do reset_slot(slot) %}
