{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Tail](/docs/commands/body/tail/index.md)
/ Adjust
# Body > Tail > Adjust Command

{% include 'includes/dumb.template.md' %}

This subcommand tinkers a bunch of variables about the player character's tails at once. Each option is optional.

**IMPORTANT:** The `--type` command does NOT use Value Formatting.

## Value Formatting

{% include 'includes/valueFormatting.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'adjust', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'show'], comment='Show our tail.', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'adjust', '--type=SPIDER_ABDOMEN', '--venom','=0', '--recharge','=0'], comment='Change to a spider abdomen with defaults', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'adjust', '--venom', '+5'], comment='Add 5 webbing.', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'adjust', '--venom', '+5'], comment='Add another 5 webbing.', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'adjust', '--venom', '/2f'], comment='Halve our webbage.', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'adjust', '--venom', '*2'], comment='Double our webbage.', echo=True)}}

{%- do reset_slot(slot) -%}
