{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Tail](/docs/commands/body/tail/index.md)
/ Set
# Body > Tail > Set Command

The set subcommand allows you to finely manipulate your character's tail on a variable-by-variable basis.

{% include 'includes/activeDevelopment.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'set', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'show'], echo=True, comment='Show our tail.') }}
{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'set', '--type=SPIDER_ABDOMEN', '--venom=0', '--recharge=5'], echo=True, comment='Grow a spider abdomen with default recharge rate and no webbing.') }}
{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'show'], echo=True, comment='Show our tail, again.') }}
{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'set', '--type=NONE'], echo=True, comment='Clear tails.') }}
{{ cmd([COCTWEAK, 'body', host, slot, 'tail', 'show'], echo=True, comment='Show our tail, again.') }}

{% do reset_slot(slot) %}
