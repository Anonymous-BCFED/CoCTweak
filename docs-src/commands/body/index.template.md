
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Body
# Body Commands

CoCTweak allows you to make a wide array of changes to your body with these subcommands.

{% include 'includes/activeDevelopment.template.md' %}

## Syntax

```
{{ cmd([COCTWEAK, 'body', '--help']) }}
```

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- balls --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls ...](docs/commands/body/balls/index.md)
* <inline-elem><!-- breasts --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; breasts ...](docs/commands/body/breasts/index.md)
* <inline-elem><!-- cocks --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; (cock|cocks|penis|penises) ...](docs/commands/body/cocks/index.md)
* <inline-elem><!-- face --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; face ...](docs/commands/body/face/index.md)
* <inline-elem><!-- skin --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; skin ...](docs/commands/body/skin/index.md)
* <inline-elem><!-- tail --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; tail ...](docs/commands/body/tail/index.md)
* <inline-elem><!-- vaginas --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; (vaginas|vagina|vag|vags) ...](docs/commands/body/vaginas/index.md)
