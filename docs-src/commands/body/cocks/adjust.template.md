{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Cocks](/docs/commands/body/cocks/index.md)
/ Adjust
# Body > Cocks > Adjust Command

This subcommand tinkers a bunch of variables about the player character's penises at once. Each option is optional.

{% include 'includes/dumb.template.md' %}

{% include 'includes/activeDevelopment.template.md' %}

## Value Formatting

{% include 'includes/valueFormatting.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'adjust', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'adjust', '1', '--length', '=5.5'], comment='The player\'s first penis shall have a length of 5.5".', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'adjust', '1', '--knot-multiplier=+2', '--width=-0.5'], comment='Add 2 to the knot multiplier, subtract half an inch from the width', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'adjust', '1', '--length=*2', '--width=/2f'], comment='Double the length, halve the width.', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'adjust', '1', '--type=HUMAN'], comment='Change into a human penis. Note what happens to the knot!', echo=True)}}

{%- do reset_slot(slot) -%}
