{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Cocks](/docs/commands/body/cocks/index.md)
/ Average
# Body > Cocks > Average Command

This weird little command allows you to unify the size and shape of your penises without ten hours in Lumi's workshop.

**Note:** The code currently doesn't average out anything other than length or width.

{% include 'includes/dumb.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'average', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'average'], echo=True) }}

{% do reset_slot(slot) %}
