{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ Cocks
# Body > Cocks Commands

These subcommands permit granular, specific modification of the player's penises.

## Save Variables

Cocks are stored in the `cocks` variable, as an array of objects.

### Cock Object

Vanilla and Revamp-based mods (UEE, HGG) use the following structure for each cock object:

<table>
<thead>
<caption>Cock Object Properties</caption>
<th>Name</th>
<th>Type</th>
<th>Default</th>
<th>Notes</th>
</thead>
<tbody>
<tr><th><code>cockLength</code></th><td>float</td><td>5.5</td><td>Length of the cock, in inches.</td></tr>
<tr><th><code>cockThickness</code></th><td>float</td><td>1</td><td>Diameter of the cock, in inches.</td></tr>
<tr><th><code>cockType</code></th><td>EnumCockTypes (int)</td><td>HUMAN (1)</td><td>The type of cock, taken from the EnumCockTypes enum, represented as an integer.</td></tr>
<tr><th><code>knotMultiplier</code></th><td>float</td><td>1</td><td>Diameter of the cock's knot is calculated as <code>(cockThickness*knotMultiplier)</code>.</td></tr>
<tr><th><code>pierced</code></th><td>int</td><td>NONE (0)</td><td>What type of piercing is used</td></tr>
<tr><th><code>pLongDesc</code></th><td>string</td><td><code>''</code></td><td>Long description of the cock (untested, not implemented).</td></tr>
<tr><th><code>pShortDesc</code></th><td>string</td><td><code>''</code></td><td>Short description of the cock (untested, not implemented).</td></tr>
<tr><th><code>serializationVersion</code></th><td colspan="3">Internal versioning used for upgrading saves, do not modify.</td></tr>
<tr><th><code>sock</code></th><td>string</td><td><code>''</code></td><td>What type of cock sock is used, if any.</td></tr>
</tbody>
</table>

## Command Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', '--help'], echo=True) }}

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- add --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; cocks add &lsqb;1-n&rsqb; ...](docs/commands/body/cocks/add.md)
* <inline-elem><!-- adjust --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; cocks adjust ...](docs/commands/body/cocks/adjust.md)
* <inline-elem><!-- average --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; cocks (average|avg) ...](docs/commands/body/cocks/average.md)
* <inline-elem><!-- remove --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; cocks remove &lsqb;1-n&rsqb;](docs/commands/body/cocks/remove.md)
* <inline-elem><!-- set --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; cocks set ...](docs/commands/body/cocks/set.md)
