{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Breasts](/docs/commands/body/breasts/index.md)
/ Add
# Body > Cocks > Add Command

This subcommand adds the desired number of cocks (1 by default) to the player's character.

{% include 'includes/dumb.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'add', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'add', '1', '--type=HUMAN', '--length=5.5', '--width=1', '-k=0'], echo=True) }}

{% do reset_slot(slot) %}
