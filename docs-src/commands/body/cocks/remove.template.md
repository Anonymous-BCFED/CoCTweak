{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Cocks](/docs/commands/body/cocks/index.md)
/ Remove
# Body > Cocks > Remove Command

This subcommand removes the indicated cock from the player's character.

{% include 'includes/dumb.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'remove', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'remove', '1'], echo=True, comment='Remove cock #1.') }}

{% do reset_slot(slot) %}
