{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Cocks](/docs/commands/body/cocks/index.md)
/ Set
# Body > Cocks > Set Command

The set subcommand allows you to finely manipulate your character's penises on a variable-by-variable basis.

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'set', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'set', '0', '--type=HUMAN', '--length=5.5', '--width=1', '-k=0'], echo=True, comment='Change penis #1 back into the default human cock.') }}

{% do reset_slot(slot) %}
