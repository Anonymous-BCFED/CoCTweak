{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Skin](/docs/commands/body/skin/index.md)
/ Set
# Body > Skin > Set Command

The `set` subcommand allows you to finely manipulate your character's skin on a variable-by-variable basis.

{% include 'includes/activeDevelopment.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'skin', 'set', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'skin', 'show'], echo=True, comment='Show our skin.') }}
{{ cmd([COCTWEAK, 'body', host, slot, 'skin', 'set', '--type=PLAIN', '--tone=ebony', '-a=greasy', '--desc=skin'], echo=True, comment='Change your skin to greasy, ebony skin.') }}
{{ cmd([COCTWEAK, 'body', host, slot, 'skin', 'show'], echo=True, comment='Show our skin, again.') }}

{% do reset_slot(slot) %}
