{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Skin](/docs/commands/body/skin/index.md)
/ Show
# Body > Skin > Show Command

The `show` subcommand allows you to show the state of your skin, as well as all variables available for manipulation.

{% include 'includes/activeDevelopment.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'skin', 'show', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'skin', 'show'], echo=True, comment='Show our skin.') }}

{% do reset_slot(slot) %}
