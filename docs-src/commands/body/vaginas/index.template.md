{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ Vagina
# Body > Vaginas Commands

This set of subcommands permits modification of the player's vaginas.

## Save Variables

Vaginas are stored in the `vaginas` variable, as an array of objects.

### Cock Object
Vanilla and Revamp-based mods (UEE, HGG) use the following structure for each cock object:
{% set EMPTY_STRING = '""' %}
{% set props = [
  {
    'name': 'vaginalWetness',
    'type': 'int',
    'default': 1,
    'notes': 'How wet the organ is. See <code>WETNESS_*</code> constants for values, but generally speaking, the higher the value, the wetter the puss.'
  },
  {
    'name': 'vaginalLooseness',
    'type': 'int',
    'default': 1,
    'notes': 'How loose the organ is. See <code>LOOSENESS_*</code> constants for values, but generally speaking, the higher the value, the looser the puss.'
  },
  {
    'name': 'type',
    'type': 'int',
    'default': 0,
    'notes': 'The type of vagina.  0 is normal, usually.'
  },
  {
    'name': 'virgin',
    'type': 'Boolean',
    'default': true,
    'notes': 'Self-explanatory.'
  },
  {
    'name': 'fullness',
    'type': 'Number',
    'default': 0,
    'notes': 'How much mass is in the organ. Unitless.'
  },
  {
    'name': 'labiaPierced',
    'type': 'Number',
    'default': 0,
    'notes': 'Are the labia pierced?  If so, with what type of material?'
  },
  {
    'name': 'labiaPShort',
    'type': 'string',
    'default': EMPTY_STRING,
    'notes': 'Short description of piercing? REQUIRED if labiaPierced &gt; 0.'
  },
  {
    'name': 'labiaPLong',
    'type': 'string',
    'default': EMPTY_STRING,
    'notes': 'Long description of piercing? REQUIRED if labiaPierced &gt; 0.'
  },
  {
    'name': 'clitPierced',
    'type': 'Number',
    'default': 0,
    'notes': 'Is the clit pierced?  If so, with what type of material?'
  },
  {
    'name': 'clitPShort',
    'type': 'string',
    'default': EMPTY_STRING,
    'notes': 'Short description of piercing? REQUIRED if clitPierced &gt; 0.'
  },
  {
    'name': 'clitPLong',
    'type': 'string',
    'default': EMPTY_STRING,
    'notes': 'Long description of piercing? REQUIRED if clitPierced &gt; 0.'
  },
  {
    'name': 'clitLength',
    'type': 'Number',
    'default': 'DEFAULT_CLIT_LENGTH',
    'notes': 'Length of clit (DEFAULT_CLIT_LENGTH=0.5 on most mods)'
  },
  {
    'name': 'recoveryProgress',
    'type': 'Number',
    'default': 0,
    'notes': 'Hours since last stretch(?)'
  }
  ]
%}
<table>
<thead>
<caption>Vagina Object Properties</caption>
<th>Name</th>
<th>Type</th>
<th>Default</th>
<th>Notes</th>
</thead>
<tbody>
{%- for prop in props %}
<tr><th><code>{{- prop.name -}}</code></th><td>{{- prop.type -}}</td><td><code>{{- prop.default -}}</code></td><td>{{- prop.notes -}}</td></tr>
{%- endfor %}
<tr><th><code>serializationVersion</code></th><td colspan="3">Internal versioning used for upgrading saves, do not modify.</td></tr>
</tbody>
</table>

## Command Syntax
{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', '--help'], echo=True) }}

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- add --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; vaginas add &lsqb;1-n&rsqb; ...](docs/commands/body/vaginas/add.md)
* <inline-elem><!-- adjust --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; vaginas (adjust|adj) ...](docs/commands/body/vaginas/adjust.md)
* <inline-elem><!-- average --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; vaginas (average|avg) ...](docs/commands/body/vaginas/average.md)
* <inline-elem><!-- remove --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; vaginas remove &lsqb;1-n&rsqb;](docs/commands/body/vaginas/remove.md)
* <inline-elem><!-- set --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; vaginas set ...](docs/commands/body/vaginas/set.md)
