{%- set host = 'localhost' -%}
{%- set slot = '7' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Vagina](/docs/commands/body/vagina/index.md)
/ Remove
# Body > Vaginas > Remove Command

This subcommand removes the desired vagina from the player's character.

{% include 'includes/dumb.template.md' %}

## Subcommand Syntax
{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'remove', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'remove', '0'], echo=True) }}

{% do reset_slot(slot) %}
