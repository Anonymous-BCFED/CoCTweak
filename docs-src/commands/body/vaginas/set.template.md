{%- set host = 'localhost' -%}
{%- set slot = '7' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Vagina](/docs/commands/body/vagina/index.md)
/ Set
# Body > Vaginas > Set Command

The set subcommand allows you to finely manipulate your character's vaginas on a variable-by-variable basis.

{% include 'includes/dumb.template.md' %}

## Subcommand Syntax
{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'set', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'set', '0', '--type=HUMAN', '--clit-length=5.5', '-f=1'], echo=True, comment="Give yourself a 5.5in clit.") }}
{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'set', '0', '--type=HUMAN', '--clit-piercing-type=STUD', '--clit-piercing-short=diamond clit-stud', '--clit-piercing-long=Diamond clit-stud'], echo=True, comment="Give yourself ceraph's clit peircings. Does not change fetish status (PC_FETISH kflag is what you're looking for).") }}

{% do reset_slot(slot) %}
