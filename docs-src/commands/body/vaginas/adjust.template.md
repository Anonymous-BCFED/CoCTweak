{%- set host = 'localhost' -%}
{%- set slot = '7' -%}
{%- set vagid = '0' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Vagina](/docs/commands/body/vagina/index.md)
/ Adjust
# Body > Vaginas > Adjust Command

This subcommand tinkers a bunch of variables about the player character's vaginas at once. Each option is optional.

{% include 'includes/dumb.template.md' %}

## Value Formatting

{% include 'includes/valueFormatting.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'adjust', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'adjust', vagid, '--clit-length==5.5'], echo=True, comment='Set clit length to 5.5"')}}
{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'adjust', vagid, '--wetness=+1'], echo=True, comment='Increase wetness by 1')}}

{%- do reset_slot(slot) -%}
