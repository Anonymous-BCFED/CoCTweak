{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Vagina](/docs/commands/body/vagina/index.md)
/ Add
# Body > Vaginas > Add Command

This subcommand adds the desired number of vaginas (1 by default) to the player's character.

{% include 'includes/dumb.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'add', '--help'], echo=True) }}


## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'add', '1', '--type=HUMAN', '--clit-length=0.5', '-f=0'], echo=True) }}

{% do reset_slot(slot) %}
