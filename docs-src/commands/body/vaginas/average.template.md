{%- set host = 'localhost' -%}
{%- set slot = '7' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Vagina](/docs/commands/body/vagina/index.md)
/ Average
# Body > Vaginas > Average Command

This weird little command allows you to unify the size and shape of your clits without ten hours in Lumi's workshop.

## Subcommand Syntax
{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'average', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'average'], echo=True) }}

{% do reset_slot(slot) %}
