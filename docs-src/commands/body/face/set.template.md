{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Face](/docs/commands/body/face/index.md)
/ Set
# Body > Face > Set Command

The set subcommand allows you to finely manipulate your character's face on a variable-by-variable basis.

{% include 'includes/activeDevelopment.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'face', 'set', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'face', 'show'], echo=True, comment='Show our face data.') }}
{{ cmd([COCTWEAK, 'body', host, slot, 'face', 'set', '--type=HORSE'], echo=True, comment='NEIGH.') }}
{{ cmd([COCTWEAK, 'body', host, slot, 'face', 'show'], echo=True, comment='Show our face, again.') }}

{% do reset_slot(slot) %}
