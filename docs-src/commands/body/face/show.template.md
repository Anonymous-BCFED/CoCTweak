{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Face](/docs/commands/body/face/index.md)
/ Show
# Body > Face > Show Command

The set subcommand allows you to show the state of your face, as well as all variables available for manipulation.

{% include 'includes/activeDevelopment.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'face', 'show', '--help'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'face', 'show'], echo=True, comment='Show your face.') }}

{% do reset_slot(slot) %}
