{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ Face
# Body > Face Commands

This set of subcommands allow you to modify every possible skin variable available to you.

{% include 'includes/activeDevelopment.template.md' %}

## Save Variables

There is only one known face variable.

### Common

Vanilla and Revamp-based mods (UEE, HGG) use the following structure:

#### Variables
<table>
<thead>
<caption>Global Data Object Properties</caption>
<th>Name</th>
<th>Type</th>
<th>Default</th>
<th>Notes</th>
</thead>
<tbody>
<tr><th><code>faceType</code></th><td><code>Face.*</code> consts (int)</td><td><code>HUMAN (0)</code></td><td>Facial structure</td></tr>
</tbody>
</table>

## Command Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'face', '--help'], echo=True) }}

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- set --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; face set ...](docs/commands/body/face/set.md)
* <inline-elem><!-- show --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; face show](docs/commands/body/face/show.md)
