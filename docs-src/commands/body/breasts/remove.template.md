{%- set host = 'localhost' -%}
{%- set slot = '7' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Breasts](/docs/commands/body/breasts/index.md)
/ Remove
# Body > Breasts > Remove Command

{% include 'includes/dumb.template.md' %}

This subcommand removes the specified row of breasts.

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'remove', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'remove', '0'], comment='Remove first row of breasts.', echo=True)}}

{%- do reset_slot(slot) -%}
