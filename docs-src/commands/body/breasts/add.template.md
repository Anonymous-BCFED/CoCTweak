{%- set host = 'localhost' -%}
{%- set slot = '7' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Breasts](/docs/commands/body/breasts/index.md)
/ Add
# Body > Breasts > Add Command

{% include 'includes/dumb.template.md' %}

This subcommand adds a new row of breasts.  By default, it either copies the last row, or makes a new row with 2 breasts of rating 1 if the list is empty.

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'add', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'add', '1', '--rating', '2'], comment='Add a row of B-cups.', echo=True)}}

{%- do reset_slot(slot) -%}
