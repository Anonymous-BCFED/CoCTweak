{%- set host = 'localhost' -%}
{%- set slot = '7' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Breasts](/docs/commands/body/breasts/index.md)
/ Remove-All
# Body > Breasts > Remove-All Command

{% include 'includes/dumb.template.md' %}

This subcommand removes all breast rows, and optionally the final nipple row as well.

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'remove-all', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'remove-all', '--flatten'], comment='Remove breasts by setting rating=0 on all of them', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'remove-all', '--literally'], comment='Remove all breast rows, including nipples.', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'remove-all', '--reset-to-human-female'], comment='Remove all breasts, and COMPLETELY reset the nipple row.', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'remove-all', '--reset-to-human-male'], comment='Remove all breasts, and COMPLETELY reset the nipple row.', echo=True)}}

{%- do reset_slot(slot) -%}
