{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Breasts](/docs/commands/body/breasts/index.md)
/ Show
# Body > Breasts > Show Command

This subcommand displays all variables related to your character's breasts.

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'show', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'show'], echo=True) }}

{%- do reset_slot(slot) -%}
