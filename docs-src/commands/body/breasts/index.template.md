> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ Breasts
# Body > Breasts Commands

This subcommand permits modification of the player's breasts.

Breasts, in CoC, are represented as a list of breast rows, each containing a set of variables
that define the breast rows' component breasts' appearance and behaviour.

Confusingly, the breast row with the largest `breastRating` (cup size) is the one that is 
used for appearance dialogue.

## Save Variables

### Common
For most mods (HGG, EJ, UEE) and Vanilla, the following variables represent the player's testes:

<table>
<thead>
<caption>Save Variables</caption>
<th>Variable</th>
<th>Type</th>
<th>Default (male)</th>
<th>Default (female)</th>
<th>Notes</th>
</thead>
<tbody>
<tr><th><code>breastRating</code></th><td>float</td><td>0.0</td><td>1.0</td><td>Size of breasts (cup size, 0=Flat).</td></tr>
<tr><th><code>breasts</code></th><td>int</td><td>2</td><td>2</td><td>Number of breasts in this row.</td></tr>
<tr><th><code>fuckable</code></th><td>bool</td><td>False</td><td>False</td><td>Whether breasts are fuckable.</td></tr>
<tr><th><code>fullness</code></th><td>float</td><td>0.0</td><td>0.0</td><td>How much semen each breast contains(?).</td></tr>
<tr><th><code>lactationMultiplier</code></th><td>float</td><td>0.0</td><td>0.0</td><td>Milk produced each tick.</td></tr>
<tr><th><code>milkFullness</code></th><td>float</td><td>0.0</td><td>0.0</td><td>Milk contained in this row.</td></tr>
<tr><th><code>nipplesPerBreast</code></th><td>int</td><td>1</td><td>1</td><td>How many nipples per breast</td></tr>
</tbody>
</table>

### HGG
Additionally, HGG contains the following variable:

<table>
<thead>
<caption>Save Variables</caption>
<th>Variable</th>
<th>Type</th>
<th>Default (male)</th>
<th>Default (female)</th>
<th>Notes</th>
</thead>
<tbody>
<tr><th><code>nippleCocks</code></th><td>bool</td><td>False</td><td>False</td><td>Whether nipple-cocks are present.</td></tr>
</tbody>
</table>

## Command Syntax

{{ cmd([COCTWEAK, 'body', 'localhost', '5', 'breasts', '--help'], echo=True) }}

## Subcommand Index
<!-- Used for sorting. -->
* <inline-elem><!-- add --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; breasts add ...](docs/commands/body/breasts/add.md)
* <inline-elem><!-- adjust --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; breasts adjust ...](docs/commands/body/breasts/adjust.md)
* <inline-elem><!-- remove --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; breasts adjust ...](docs/commands/body/breasts/remove.md)
* <inline-elem><!-- remove-all --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; breasts adjust ...](docs/commands/body/breasts/remove-all.md)
* <inline-elem><!-- set --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; breasts set ...](docs/commands/body/breasts/set.md)
* <inline-elem><!-- show --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; breasts show](docs/commands/body/breasts/show.md)
