{%- set host = 'localhost' -%}
{%- set slot = '7' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Breasts](/docs/commands/body/breasts/index.md)
/ Set
# Body > Breasts > Set Command

This subcommand sets a bunch of variables about the player character's breasts at once. Each option is optional.

{% include 'includes/dumb.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'set', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'set', '0', '--rating=2'], echo=True)}}

{%- do reset_slot(slot) -%}
