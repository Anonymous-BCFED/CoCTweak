{%- set host = 'localhost' -%}
{%- set slot = '7' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Breasts](/docs/commands/body/breasts/index.md)
/ Adjust
# Body > Breasts > Adjust Command

{% include 'includes/dumb.template.md' %}

This subcommand tinkers a bunch of variables about the player character's breasts at once. Each option is optional.

## Value Formatting

{% include 'includes/valueFormatting.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'adjust', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'adjust', '0', '--rating', '=1'], comment='The player shall have A-cups.', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'adjust', '0', '--rating=+1'], comment='Increase size to B-cups', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'breasts', 'adj', '0', '--lactation-multiplier==.5', '--rating=/2f'], comment='Increase lactation multiplier, halve rating', echo=True)}}

{%- do reset_slot(slot) -%}
