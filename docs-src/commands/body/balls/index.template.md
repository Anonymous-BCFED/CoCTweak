> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ Balls
# Body > Balls Commands

This subcommand permits modification of the player's testicles.

Be aware, however, that testicles are not very granular, and are represented in
most mods by merely 3 numbers, so only limited modifications are currently
available. More may become available as features are reverse-engineered.

## Save Variables

### Common
For most mods (HGG, EJ, UEE) and Vanilla, the following variables represent the player's testes:

<table>
<thead>
<caption>Save Variables</caption>
<th>Variable</th>
<th>Type</th>
<th>Default (male)</th>
<th>Default (female)</th>
<th>Notes</th>
</thead>
<tbody>
<tr><th><code>balls</code></th><td>int</td><td>2</td><td>0</td><td>Number of testes.</td></tr>
<tr><th><code>ballSize</code></th><td>float</td><td>1</td><td>0</td><td>Diameter of a testicle in inches.</td></tr>
<tr><th><code>cumMultiplier</code></th><td>float</td><td>1</td><td>1</td><td>Used in semen volume calculations (see <code>Creature.cumQ()</code>).</td></tr>
</tbody>
</table>

## Command Syntax

{{ cmd([COCTWEAK, 'body', 'localhost', '5', 'balls', '--help'], echo=True) }}

## Subcommand Index
<!-- Used for sorting. -->
* <inline-elem><!-- adjust --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls set ...](docs/commands/body/balls/adjust.md)
* <inline-elem><!-- set --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls set ...](docs/commands/body/balls/set.md)
* <inline-elem><!-- show --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls show](docs/commands/body/balls/show.md)
