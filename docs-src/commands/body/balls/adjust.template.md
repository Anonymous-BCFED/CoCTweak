{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Balls](/docs/commands/body/balls/index.md)
/ Adjust
# Body > Balls > Adjust Command

{% include 'includes/dumb.template.md' %}

This subcommand tinkers a bunch of variables about the player character's testicles at once. Each option is optional.

## Value Formatting

{% include 'includes/valueFormatting.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'balls', 'adjust', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'balls', 'adjust', '--count', '=1'], comment='The player shall have one nut.', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'balls', 'adjust', '--count=+1'], comment='Add another nut, for a total of 2.', echo=True)}}
{{ cmd([COCTWEAK, 'body', host, slot, 'balls', 'adj', '--mult=-.5', '--size=/2f'], comment='Remove some virility and halve size.', echo=True)}}

{%- do reset_slot(slot) -%}
