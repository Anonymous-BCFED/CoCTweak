{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Balls](/docs/commands/body/balls/index.md)
/ Show
# Body > Balls > Show Command

This subcommand displays all variables related to your character's balls.

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'balls', 'show', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'balls', 'show'], echo=True, replacements={
  '  (.+) hideously swollen and oversized (.+)': '  four hideously swollen and oversized balls'
})}}

{%- do reset_slot(slot) -%}
