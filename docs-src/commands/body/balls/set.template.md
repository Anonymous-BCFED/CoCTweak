{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ [Body](/docs/commands/body/index.md)
/ [Balls](/docs/commands/body/balls/index.md)
/ Set
# Body > Balls > Set Command

This subcommand sets a bunch of variables about the player character's testicles at once. Each option is optional.

{% include 'includes/dumb.template.md' %}

## Subcommand Syntax

{{ cmd([COCTWEAK, 'body', host, slot, 'balls', 'set', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'body', host, slot, 'balls', 'set', '--count=2', '--size=1', '--mult=1'], echo=True)}}

{%- do reset_slot(slot) -%}
