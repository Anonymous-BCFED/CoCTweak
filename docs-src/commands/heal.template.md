{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Heal
# Heal Command

The heal command repairs your HP, and optionally, your lust, fatigue, and hunger.

**NOTE:** The maximums you are healed to are _very_ rough guesses. CoCTweak will attempt to *overestimate* values, as that will cause the game to clamp them to what it expects.

## Command Syntax

{{ cmd([COCTWEAK, 'heal', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'heal', host, slot, '--all'], echo=True) }}
{% do reset_slot(slot) %}
