> [CoCTweak](/README.md)
/ [Commands](/docs/commands/list.md)
/ Restore
# Restore Command

This command will restore the <s>*entire*</s> backed-up Flash Shared Objects Library that's stored in backups/ to the correct directory.  Useful for when Flash mysteriously nukes it.

Note that this command will ONLY read the .sol binaries, unless told to use other sources directly.

## Command Syntax
{{ cmd([COCTWEAK, 'restore', '--help'], echo=True)}}

## Example

{{ cmd([COCTWEAK, 'restore', '--clobber'], echo=True, comment='Restore EVERYTHING, overwrites anything present.')}}
{{ cmd([COCTWEAK, 'restore', '--clobber', '--hostname=localhost', '--coc-save=1'], echo=True, comment='Restore coc save 1 from localhost.')}}

{% do reset_slot('ALL') %}
