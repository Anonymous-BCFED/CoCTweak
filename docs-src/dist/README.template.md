{#- This is what is released with the software. -#}
{%- set repo_uri = "https://gitlab.com/Anonymous-BCFED/CoCTweak" -%}
{%- set supported = [
  ['Vanilla',             'Fenoxo',           'Should be OK, for the most part.',  'https://github.com/OXOIndustries/Corruption-of-Champions'],
  ['HGG',                 'OCA/OtherCoCAnon', 'Has the best support.', 'https://gitgud.io/BelshazzarII/CoCAnon_mod'],
  ['UEE',                 'Kitteh6660' ,      'A bit behind HGG; **Development of mod on hiatus.**', 'https://github.com/Kitteh6660/Corruption-of-Champions-Mod']
] -%}
{%- set dropped = [
  ['Endless Journey',     'Oxdeception',      'Mod development ceased. Save structure is a clusterfuck.', 'https://github.com/Oxdeception/Corruption-of-Champions'],
] -%}
# CoCTweak {{VERSION}}

A thorough, mod-friendly save editor with a multitude of console commands. Built with Python 3.11 and mini-amf, so it works on Linux and Windows.

## A Note

As a save editor, CoCTweak is *not* supported by the developers of CoC or the game's modders.  By using CoCTweak, you can potentially break saves and trigger bugs.  **You use CoCTweak at your own risk.**

So, **don't bug CoC devs about bugs triggered by CoCTweak** in CoC.  They aren't responsible, and can't fix our problems anyway.

## Mod Versions

The versions of each mod supported by CoCTweak v{{ VERSION }} is listed below. Earlier versions may work, but this is not guaranteed. Using CoC on later versions may result in missing features or bugs.

Note: Git Commit is the specific state of the code in their repositories that the current version of CoCTweak was built against. This is useful for bugtracking information.

<!-- devtools/updateSupportTable.py replaces this: -->
|     Mod | Site                                                      | Version   | Git Commit                                                                                                                                              |
|---------|-----------------------------------------------------------|-----------|---------------------------------------------------------------------------------------------------------------------------------------------------------|
|     HGG | https://gitgud.io/BelshazzarII/CoCAnon_mod                | 1.6.17.1  | [`b56b7c4ca0372455298cffe272e648eaa8caf305`](https://gitgud.io/BelshazzarII/CoCAnon_mod/-/commit/b56b7c4ca0372455298cffe272e648eaa8caf305)              |
|     UEE | https://github.com/Kitteh6660/Corruption-of-Champions-Mod | 1.4.19    | [`54fb4a179bcd4aea1cefada8d06dc0688d297657`](https://github.com/Kitteh6660/Corruption-of-Champions-Mod/commit/54fb4a179bcd4aea1cefada8d06dc0688d297657) |
| Vanilla | https://github.com/OXOIndustries/Corruption-of-Champions  | 1.0.2     | [`429da7333799cc009ce88da3010061b88f5b8a00`](https://github.com/OXOIndustries/Corruption-of-Champions/commit/429da7333799cc009ce88da3010061b88f5b8a00)  |
<!-- devtools/updateSupportTable.py END -->

## More Info

For more information on how to use `coctweak`, please visit [our gitlab](https://gitlab.com/Anonymous-BCFED/CoCTweak/-/blob/master/README.md).


## Troubleshooting

**Please see the [Troubleshooting](https://gitlab.com/Anonymous-BCFED/CoCTweak/-/blob/master/docs/FAQ.md#troubleshooting) section of our FAQ** for the most up to date information.