> [CoCTweak](/README.md)
/ [Development](/docs/development)
/ Samples

Script for re-creating sample saves used for testing.

These names and choices carry no significance, and any resemblance is unintentional.  The choices and names are this way to enforce standard values and ensure we can track any encoding/obfuscation changes easily. Names were chosen at random.

To update this script:

1. Modify `docs-src/development/samples.md`
2. Install Node.js and `yarn` to your system
3. Run `python devtools/buildDevScripts.py --rebuild`

[TOC]

## Felicia (`newfemale`)

### HGG

{Start Script}

1. [ ] Select the button labelled **New Game**.
  * You will be asked to select a name.
1. [ ] Enter `Felicia`.
1. [ ] Select the button labelled **OK**.
  * A screen will appear asking you to select your age group.
1. [ ] Select the button labelled **Adult**.
  * You will be presented with a screen asking for your sex.
1. [ ] Select the button labelled **Woman**.
  * The next screen gives you a selection of body builds to choose from.
1. [ ] Select the button labelled **Slender**.
  * A screen will ask you for your sexuality.
1. [ ] Select the button labelled **Males**.
  * The next screen will give you a selection of complexions.
1. [ ] Select the button labelled **Light**.
  * Now, you will be asked about your hair color.
1. [ ] Select the button labelled **Blonde**.
  * A summary of your character, as it currently exists, will be given to you.
    * **Height:** 5' 7"
    * **Skin tone:** Light
    * **Hair color:** Blonde
    * **Breast size:** B-Cup
1. [ ] Select the button labelled **Done**.
  * A selection of "gifts" will appear.
1. [ ] Select the **Smarts** perk.
1. [ ] Confirm.
  * This time, you will be shown a list of histories to choose from
1. [ ] Select the **Schooling** history.
1. [ ] Confirm.
  * You will be asked if you're still a virgin.
1. [ ] Select the button labelled **Yes**.
  * A complex screen providing gameplay options will be presented to you.
1. [ ] Select the button labelled **Continue** in the lower right corner.
  * You'll be given some prose.
1. [ ] Hit **Next**.
  * More prose introducing you to things.
1. [ ] Hit **Next**.
  * You'll continue to see the story develop.
1. [ ] Hit **Next**.
  * You'll get more story development.
1. [ ] Hit **Next**.
  * You'll finally see the camp screen.
1. [ ] Select the **Data** button.
1. [ ] Select the **Save** button.
1. [ ] Enter `HGG - Felicia [PRISTINE]` into the notes textbox.
1. [ ] Select the button labelled **Slot 12**.
1. [ ] Open the console and `cd` to your COCTWEAK directory.
1. [ ] Paste in `./coctweak.sh export --raw @lwn 12 samples/hgg/newfemale.raw.yml` and press enter.
1. [ ] Paste in `./coctweak.sh export @lwn 12 samples/hgg/newfemale.yml` and press enter.

{End Script}

### UEE

{Start Script}

1. [ ] Select the button labelled **New Game**.
1. [ ] Enter `Felicia` as your name.
1. [ ] Select the button labelled **OK**.
1. [ ] Select the button labelled **Woman**.
1. [ ] Select the button labelled **Slender**.
1. [ ] Select the button labelled **Light**.
1. [ ] Select the button labelled **Blonde**.
  * A summary of your character, as it currently exists, will be given to you.
    * **Height:** 5' 7"
    * **Skin tone:** Light
    * **Hair color:** Blonde
    * **Breast size:** B-Cup
1. [ ] Select the button labelled **Done**.
1. [ ] Select the **Smarts** perk.
1. [ ] Confirm by pressing **Yes**.
1. [ ] Select the **Schooling** history.
1. [ ] Confirm by pressing **Yes**.
1. [ ] Press **Start!**
1. [ ] Press **No** when the game asks you whether to play the tutorial.
1. [ ] Hit **Next** to continue.
1. [ ] Hit **Next**.
1. [ ] Hit **Next**.
1. [ ] Hit **Next**.
  * You'll finally see the camp screen.
1. [ ] Select the **Data** button.
1. [ ] Select the **Save** button.
1. [ ] Enter `UEE - Felicia (PRISTINE)` into the notes textbox.
1. [ ] Select the button labelled **Slot 14**.
1. [ ] Open the console and `cd` to your COCTWEAK directory.
1. [ ] Paste in `./coctweak.sh export --raw @lwn 14 samples/uee/newfemale.raw.yml` and press enter.
1. [ ] Paste in `./coctweak.sh export @lwn 14 samples/uee/newfemale.yml` and press enter.

{End Script}

## Harold (`newmale`)

### HGG

{Start Script}

1. [ ] Select the button labelled **New Game**.
1. [ ] Enter `Harold`.
1. [ ] Select the button labelled **OK**.
1. [ ] Select the button labelled **Adult**.
1. [ ] Select the button labelled **Man**.
1. [ ] Select the button labelled **Lean**.
1. [ ] Select the button labelled **Females**.
1. [ ] Select the button labelled **Light**.
1. [ ] Select the button labelled **Blonde**.
1. [ ] Select the button labelled **Done**.
1. [ ] Select the **Smarts** perk.
1. [ ] Confirm.
1. [ ] Select the **Schooling** history.
1. [ ] Confirm.
1. [ ] Select the button labelled **Yes**.
1. [ ] Select the button labelled **Continue** in the lower right corner.
1. [ ] Hit **Next**.
1. [ ] Hit **Next**.
1. [ ] Hit **Next**.
1. [ ] Hit **Next**.
1. [ ] Select the **Data** button.
1. [ ] Select the **Save** button.
1. [ ] Enter `HGG - Harold [PRISTINE]` into the notes textbox.
1. [ ] Select the button labelled **Slot 13**.
1. [ ] Open the console and `cd` to your COCTWEAK directory.
1. [ ] Paste in `./coctweak.sh export --raw @lwn 13 samples/hgg/newmale.raw.yml` and press enter.
1. [ ] Paste in `./coctweak.sh export @lwn 13 samples/hgg/newmale.yml` and press enter.

{End Script}

### UEE

{Start Script}

1. [ ] Start a **New Game**.
1. [ ] Enter your name as `Harold`.
1. [ ] Press **OK** to confirm your name.
1. [ ] Choose **Man** at the prompt.
1. [ ] Select a **Lean** build.
1. [ ] You have a **Light** complexion.
1. [ ] Select **Blonde** from the available hair colors.
  * You will be presented with a character summary.  Ensure the values below match:
    * **Height**: 5’11"
    * **Skin tone**: light
    * **Hair color**: blonde
    * **Cock size**: 5.5" long, 1" thick
    * **Breast size**: flat
1. [ ] Press **Done**.
1. [ ] Choose the **Smarts** gift.
1. [ ] Confirm your choice by pressing **Yes**.
1. [ ] Select the **Schooling** history.
1. [ ] Confirm by pressing **Yes**.
1. [ ] Press **Start!**
1. [ ] Skip the prologue by selecting **No**.
1. [ ] Press **Next** until you hit the camp screen.
1. [ ] Select the **Data** button.
1. [ ] Select the **Save** button.
1. [ ] Enter `UEE - Harold (PRISTINE)` into the notes textbox.
1. [ ] Select the button labelled **Slot 11**.
1. [ ] Open the console and `cd` to your COCTWEAK directory.
1. [ ] Paste in `./coctweak.sh export --raw @lwn 11 samples/uee/newmale.raw.yml` and press enter.
1. [ ] Paste in `./coctweak.sh export @lwn 11 samples/uee/newmale.yml` and press enter.

{End Script}
