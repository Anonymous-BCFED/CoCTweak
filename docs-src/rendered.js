window.currentSection = '';
window.currentSectionTag = null;
window.currentSectionStartButton = null;
window.currentInstruction = '';
$(function() {
  $('input[type=checkbox]').on('change', function() {
    let id = $(this).attr('id');
    if (id.startsWith('sec_')) {
      if(!$(this).prop('checked'))
        return;
      var section, instruction;
      [_, section, instruction] = id.split('_');
      if (section+'' == window.currentSection) {
        $(this).prop('disabled', true).parent().removeClass('script-active').addClass('script-completed');
        let nextPart = $(this).parent().next();
        if(nextPart.length > 0) {
          nextPart.removeClass('script-todo').addClass('script-active');
          let nextCheckbox = nextPart.find('[type=checkbox]');
          nextCheckbox.prop('disabled', false);
        } else {
          window.currentSectionStartButton.prop('disabled', false);
          window.currentSection = '';
          window.currentSectionTag = null;
          window.currentSectionStartButton = null;
          window.currentInstruction = '';
          $('section.script input[type=checkbox]').prop('disabled', true).prop('checked', false).parent().removeClass('script-todo').removeClass('script-active').removeClass('script-completed');
        }
      }
    } else {
      $(this).parent().css('color', $(this).prop('checked') ? '#666666' : '#000000');
    }
  });
  $('button.begin-script').on('click', function(){
    if(!confirm('You are about to begin a script. You will be locked into this script until it is completed.\n\nDo you want to continue?'))
      return;
    $(this).prop('disabled', true);
    var section = $(this).parent().parent();
    window.currentSection=$(this).attr('data-section');
    window.currentSectionTag=section.first();
    window.currentSectionStartButton=$(this);
    window.currentInstruction='0';
    var nextPart = section.find('> ol li');
    console.assert(nextPart.length > 0, section.children());
    var checkboxes = nextPart.children('[type=checkbox]');
    console.assert(checkboxes.length > 0, nextPart.children());
    checkboxes.prop('disabled', true).prop('checked', false).parent().addClass('script-todo');
    checkboxes.first().prop('disabled', false).parent().removeClass('script-todo').addClass('script-active');
  });
  $('section.script input[type=checkbox]').prop('disabled', true);
});
