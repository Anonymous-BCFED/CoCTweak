These files are templates used to generate the documentation found in [../docs](../docs). They are not intended to be read by humans.
